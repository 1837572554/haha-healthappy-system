package com.healthappy;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import com.healthappy.annotation.AnonymousAccess;
import com.healthappy.config.HaHaHealthappyApplication;
import com.healthappy.utils.SpringContextHolder;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author hupeng
 * @date 2018/11/15 9:20:19
 */
@EnableAsync
@RestController
@SpringBootApplication(exclude = {DruidDataSourceAutoConfigure.class}) //排除原生Druid的快速配置类。
@EnableTransactionManagement
@MapperScan(basePackages = {"com.healthappy.config"})            //,"com.healthappy.modules.system.service.mapper"
public class AppRun {

	public static void main(String[] args) {
		HaHaHealthappyApplication.run(AppRun.class, args);
	}

	@Bean
	public SpringContextHolder springContextHolder() {
		return new SpringContextHolder();
	}

	@Bean
	public ServletWebServerFactory webServerFactory() {
		TomcatServletWebServerFactory fa = new TomcatServletWebServerFactory();
		fa.addConnectorCustomizers(connector -> connector.setProperty("relaxedQueryChars", "[]{}"));
		return fa;
	}

	/**
	 * 访问首页提示
	 * @return /
	 */
	@GetMapping("/")
	@AnonymousAccess
	public String index() {
		return "Backend service started successfully";
	}
}
