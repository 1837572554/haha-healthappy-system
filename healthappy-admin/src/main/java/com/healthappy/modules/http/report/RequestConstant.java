package com.healthappy.modules.http.report;

/**
 * @Author: FGQ
 * @Desc:
 * @Date: Created in 14:07 2022/3/17
 */
public interface RequestConstant {

	/**
	 * 条码
	 */
	String BAR_CODE = "/Report/GetBarcodeReport";

	/** 条码列表 */
	String BAR_CODE_LIST = "/Report/GetBarcodeReportByList";

	/**
	 * 指引单
	 */
	String GUIDE = "/Report/GetGuideReport";

	/**
	 * 信息表
	 */
	String INFO_REPORT = "/Report/GetPersonInfoReport";

	/**
	 * 预约信息表
	 */
	String APPOINT_INFO_REPORT = "/Report/GetAppointInfoReport";

	/**
	 * 个人体检报告
	 */
	String PERSON_REPORT = "/report/GetPersonReport";

	/**
	 * 单独体检项目报告生成
	 */
	String GET_ITEM_REPORT = "/report/GetItemReport";

	/**
	 * 单位报告
	 */
	String COMPANY_REPORT = "/report/GetCompanyProfessionReport";

	/**
	 * 单位健康分析报告
	 */
	String COMPANY_ANALYSIS = "/report/GetCompanyAnalysisReport";

	/**
	 * 健康证
	 */
	String HEALTH_CERTIFICATE = "/report/GetHealthCertificateReport";

	/** 收费票据 */
	String PAY_REPORRT = "/report/GetPayReport";
}
