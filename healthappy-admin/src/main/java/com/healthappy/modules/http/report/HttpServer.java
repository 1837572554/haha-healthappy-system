package com.healthappy.modules.http.report;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.my.queue.PrintRequestObject;
import com.healthappy.modules.system.domain.BReportConfig;
import com.healthappy.modules.system.domain.vo.AloneChargePrintVo;
import com.healthappy.modules.system.domain.vo.BarcodeVO;
import com.healthappy.utils.SecurityUtils;
import com.healthappy.utils.SpringBeanFactoryUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Map;

/**
 * @Author: FGQ
 * @Desc: http
 * @Date: Created in 11:10 2022/3/17
 */
@Slf4j
@Data
@Validated
@UtilityClass
public class HttpServer {


	/**
	 * timeout   超时时长，-1表示默认超时，单位毫秒
	 */
	@ApiModelProperty("timeout   超时时长，-1表示默认超时，单位毫秒")
	private final static int TIMEOUT = -1;

	/**
	 * 健康证生成
	 * @author YJ
	 * @date 2022/4/11 17:01
	 * @param beanOne
	 * @return java.lang.String
	 */
	public String getHealthCertificate(@Valid BeanOne beanOne) {
		return startReq(HttpMethod.POST, RequestConstant.HEALTH_CERTIFICATE, BeanUtil.beanToMap(beanOne));
	}

	/**
	 * 条码生成
	 *
	 * @param beanOne
	 * @return
	 */
	public String getBarcodeReport(@Valid BeanOne beanOne) {
		return startReq(HttpMethod.GET, RequestConstant.BAR_CODE, BeanUtil.beanToMap(beanOne));
	}

	/**
	 * 条码列表生成
	 *
	 * @param barcodeInfo
	 * @return
	 */
	public String getBarcodeReportList(@Valid BarcodeInfo barcodeInfo) {
		return startReq(HttpMethod.POST, RequestConstant.BAR_CODE_LIST, BeanUtil.beanToMap(barcodeInfo));
	}

	/**
	 * 指引单生成
	 *
	 * @param beanOne
	 * @return
	 */
	public String getGuideReport(@Valid BeanOne beanOne) {
		return startReq(HttpMethod.GET, RequestConstant.GUIDE, BeanUtil.beanToMap(beanOne));
	}

	/**
	 * 信息表生成
	 *
	 * @param personReport
	 * @return java.lang.String
	 * @author YJ
	 * @date 2022/3/18 17:05
	 */
	public String getInfoReport(@Valid PersonReport personReport) {
		return startReq(HttpMethod.POST, RequestConstant.INFO_REPORT, BeanUtil.beanToMap(personReport));
	}

	/**
	 * 预约信息表
	 *
	 * @param appointInfo
	 * @return java.lang.String
	 * @author YJ
	 * @date 2022/3/21 15:21
	 */
	public String getAppointInfoReport(@Valid AppointInfo appointInfo) {
		return startReq(HttpMethod.POST, RequestConstant.APPOINT_INFO_REPORT, BeanUtil.beanToMap(appointInfo));
	}

	/**
	 * 个人体检报告生成
	 *
	 * @return
	 */
	public String getPersonReport(@Valid PersonReport personReport) {
		//		PrintRequestObject object = new PrintRequestObject();
		//		object.setHttpMethod(HttpMethod.POST);
		//		object.setRequestMapping(RequestConstant.PERSON_REPORT);
		//		object.setParamMap(BeanUtil.beanToMap(personReport));
		//		MyQueue.Add(String.valueOf(SecurityUtils.getUserId()), object);
		//		return "";
		return startReq(HttpMethod.POST, RequestConstant.PERSON_REPORT, BeanUtil.beanToMap(personReport));
	}

	/**
	 * 单位体检报告生成
	 *
	 * @param companyReport
	 * @return
	 */
	public String getCompanyReport(@Valid CompanyReport companyReport) {
		//		PrintRequestObject object = new PrintRequestObject();
		//		object.setHttpMethod(HttpMethod.POST);
		//		object.setRequestMapping(RequestConstant.COMPANY_REPORT);
		//		object.setParamMap(BeanUtil.beanToMap(companyReport));
		//		new MyQueue().Add(String.valueOf(SecurityUtils.getUserId()), object);
		//		return "";
		return startReq(HttpMethod.POST, RequestConstant.COMPANY_REPORT, BeanUtil.beanToMap(companyReport));
	}

	/**
	 * 单位健康分析报告生成
	 *
	 * @param companyAnalysisReport
	 * @return java.lang.String
	 * @author YJ
	 * @date 2022/4/2 19:47
	 */
	public String getCompanyAnalysis(@Valid CompanyAnalysisReport companyAnalysisReport) {
		return startReq(HttpMethod.POST, RequestConstant.COMPANY_ANALYSIS, BeanUtil.beanToMap(companyAnalysisReport));
	}

	/**
	 * 单独体检项目报告生成
	 *
	 * @return
	 */
	public String getItemReport(@Valid GetItemReport getItemReport) {
		return startReq(HttpMethod.POST, RequestConstant.GET_ITEM_REPORT, BeanUtil.beanToMap(getItemReport));
	}

	/**
	 * 单独体检项目报告生成
	 *
	 * @return
	 */
	public String getItemReportQueue(@Valid GetItemReport getItemReport) {
		PrintRequestObject object = new PrintRequestObject();
		object.setHttpMethod(HttpMethod.POST);
		object.setRequestMapping(RequestConstant.GET_ITEM_REPORT);
		object.setParamMap(BeanUtil.beanToMap(getItemReport));
		object.setCommType("PrintReport");
		object.setUserName(SecurityUtils.getUsername());
		//		object.setUserId(String.valueOf(SecurityUtils.getUserId()));
		//		MyQueue.getInstance().Add(object);


		return "";
		//		return startReq(HttpMethod.POST, RequestConstant.GET_ITEM_REPORT, BeanUtil.beanToMap(getItemReport));
	}

	/**
	 * 生成收费单据
	 * @author YJ
	 * @date 2022/4/15 19:07
	 * @param aloneChargePrintVo
	 * @return java.lang.String
	 */
	public String getPayReport(AloneChargePrintVo aloneChargePrintVo) {
		return startReq(HttpMethod.POST, RequestConstant.PAY_REPORRT, BeanUtil.beanToMap(aloneChargePrintVo));
	}

	private String startReq(HttpMethod httpMethod, String requestMapping, Map<String, Object> paramMap) {
		String result;
		String requestUrl = SpringBeanFactoryUtil.resolve("${httpServer.reportServer}") + requestMapping;
		try {
			if (httpMethod.compareTo(HttpMethod.GET) == 0) {
				log.info("startReq get map:" + paramMap);
				result = HttpUtil.get(requestUrl, paramMap, TIMEOUT);
			} else if (httpMethod.compareTo(HttpMethod.POST) == 0) {
				String json = new JSONObject(paramMap).toString();
				log.info("startReq post json:" + json);
				result = HttpUtil.post(requestUrl, json, TIMEOUT);
			} else {
				throw new BadRequestException("请求类型异常");
			}
		} catch (Exception e) {
			throw new BadRequestException("请求接口异常");
		}
		log.info("startReq result:" + result);
		JSONObject resultObj = new JSONObject(result);
		if (!"0".equals(resultObj.getStr("code"))) {
			throw new BadRequestException(resultObj.getStr("msg"));
		}
		return resultObj.getStr("data");
	}

	public static String startReq(PrintRequestObject obj) {
		return startReq(obj.getHttpMethod(), obj.getRequestMapping(), obj.getParamMap());
	}

	public static void main(String[] args) {
		// BeanUtil.copyProperties()
	}

	/**
	 * 通用基础
	 */
	@Data
	public static class BeanOne {

		@ApiModelProperty("租户ID")
		@NotBlank(message = "租户ID不能为空")
		private String tenantId;

		@ApiModelProperty("流水号")
		@NotBlank(message = "流水号不能为空")
		private String patientId;

		@ApiModelProperty("文件类型 1=pdf , 2=word")
		@NotBlank(message = "文件类型不能为空")
		private Integer fileType;

		/**
		 * 初始化参数
		 *
		 * @author YJ
		 * @date 2022/3/18 9:43
		 */
		public void init() {
			this.setFileType(1);
			this.setTenantId(SecurityUtils.getTenantId());
		}
	}

	/**
	 * 条码列表对象
	 * @author YJ
	 * @date 2022/5/12 17:38
	 */
	@Data
	public static class BarcodeInfo extends BeanOne {
		/** 条码集合 */
		@ApiModelProperty("条码集合")
		private List<BarcodeVO> barcodes;
	}

	/**
	 * 预约信息表 对象
	 *
	 * @author YJ
	 * @date 2022/3/21 16:52
	 */
	@Data
	public static class AppointInfo {
		/**
		 * 租户ID
		 */
		@ApiModelProperty("租户ID")
		@NotBlank(message = "租户ID不能为空")
		private String tenantId;

		/**
		 * 预约号
		 */
		@ApiModelProperty("预约号")
		@NotBlank(message = "预约号不能为空")
		private String appointId;

		/**
		 * 文件类型 1=pdf , 2=word
		 */
		@ApiModelProperty("文件类型 1=pdf , 2=word")
		@NotBlank(message = "文件类型不能为空")
		private Integer fileType;

		/**
		 * 初始化参数
		 *
		 * @author YJ
		 * @date 2022/3/18 9:43
		 */
		public void init() {
			this.setFileType(1);
			this.setTenantId(SecurityUtils.getTenantId());
		}
	}

	/**
	 * 单项报告基础数据
	 */
	@Data
	public static class GetItemReport {

		@ApiModelProperty("租户ID")
		@NotBlank(message = "租户ID不能为空")
		private String tenantId;

		@ApiModelProperty("流水号")
		@NotBlank(message = "流水号不能为空")
		private String patientId;

		@ApiModelProperty("分组ID")
		@NotBlank(message = "分组ID不能为空")
		private String groupId;

		@ApiModelProperty("文件类型 1=pdf , 2=word")
		@NotBlank(message = "文件类型不能为空")
		private Integer fileType;

		/**
		 * 初始化参数
		 *
		 * @author YJ
		 * @date 2022/3/18 9:43
		 */
		public void init() {
			this.setFileType(1);
			this.setTenantId(SecurityUtils.getTenantId());
		}
	}

	/**
	 * 个人报告基础数据
	 */
	@Data
	public static class PersonReport {

		@ApiModelProperty("租户ID")
		@NotBlank(message = "租户ID不能为空")
		private String tenantId;

		@ApiModelProperty("流水号")
		@NotBlank(message = "流水号不能为空")
		private String patientId;

		@ApiModelProperty("模板路径或模板名称")
		@NotBlank(message = "模板路径或模板名称不能为空")
		private String templatePath;

		@ApiModelProperty("模板里需要的所有标签(如：[card],[items])")
		@NotBlank(message = "模板里需要的所有标签(如：[card],[items])")
		private String marks;

		@ApiModelProperty("文件类型 1=pdf , 2=word")
		@NotBlank(message = "文件类型不能为空")
		private Integer fileType;

		@ApiModelProperty("职业体检数据是否显示")
		@NotBlank(message = "职业体检数据是否显示")
		private boolean isShowZY = false;

		/**
		 * 是否只显示健康体检项目
		 */
		@ApiModelProperty("是否只显示健康体检项目")
		private Boolean isShowJK = false;

		@ApiModelProperty("是否复查")
		@NotBlank(message = "是否复查不能为空")
		private boolean isReview;

		/**
		 * 初始化参数-信息表
		 *
		 * @author YJ
		 * @date 2022/3/21 16:50
		 */
		public void initInfo() {
			this.setTemplatePath("guide/zinfo.rtf");
			this.setMarks("");
			this.setFileType(1);
			this.setShowZY(true);
			this.setReview(false);
			this.setTenantId(SecurityUtils.getTenantId());
		}

		/**
		 * 初始化参数-个人健康体检报告
		 *
		 * @author YJ
		 * @date 2022/3/18 9:43
		 */
		public void initHealth() {
			this.setTemplatePath("jk/person/person.rtf");
			this.setMarks("[card],[barcode],[group_tables],[conclusion_doc],[verify_doc],[seal],[depts]");
			this.setFileType(1);
			this.setShowZY(false);
			this.setReview(false);
			this.setTenantId(SecurityUtils.getTenantId());
		}

		/**
		 * 初始化参数-个人精简报告
		 *
		 * @author YJ
		 * @date 2022/3/18 17:17
		 */
		public void initHealthLite() {
			this.setTemplatePath("jk/person/person2.rtf");
			this.setMarks("[card],[barcode],[group_tables],[conclusion_doc],[verify_doc],[seal],[depts]");
			this.setFileType(1);
			this.setShowZY(false);
			this.setReview(false);
			this.setTenantId(SecurityUtils.getTenantId());
		}

		/**
		 * 初始化参数-职业健康检查表
		 *
		 * @author YJ
		 * @date 2022/3/18 9:43
		 */
		public void initOccupation() {
			this.setTemplatePath("zhi/person/person.rtf");
			this.setMarks(
					"[card],[camera],[barcode],[wz],[items],[doctor],[verify_doc],[seal],[guide],[depts],[group_tables]");
			this.setFileType(1);
			this.setShowZY(true);
			this.setReview(false);
			this.setTenantId(SecurityUtils.getTenantId());
		}

		/**
		 * 职业健康检查通知单
		 *
		 * @author YJ
		 * @date 2022/3/22 14:45
		 */
		public void initOccupationNotice() {
			this.setTemplatePath("zhi/person/person2.rtf");
			this.setMarks(
					"[card],[camera],[barcode],[wz],[items],[doctor],[verify_doc],[seal],[guide],[depts],[group_tables]");
			this.setFileType(1);
			this.setShowZY(true);
			this.setReview(false);
			this.setTenantId(SecurityUtils.getTenantId());
		}

		/**
		 * 儿童健康体检报告
		 *
		 * @author YJ
		 * @date 2022/3/22 14:56
		 */
		public void childHealth() {
			this.setTemplatePath("childrenReport/person.rtf");
			this.setMarks(
					"[barcode],[group_tables],[card],[camera],[wz],[items],[doctor],[verify_doc],[seal],[guide],[depts],[conclusion_doc]");
			this.setFileType(1);
			this.setShowZY(true);
			this.setReview(false);
			this.setTenantId(SecurityUtils.getTenantId());
		}

		/**
		 * 从业预防性体检报告
		 */
		public void practitionerPrevention() {
			this.setTemplatePath("cy/person/person3.rtf");
			this.setMarks(
					"[barcode],[group_tables],[conclusion_doc],[verify_doc],[seal],[items],[card],[wz],[guide],[doctor]");
			this.setFileType(1);
			this.setShowZY(true);
			this.setReview(false);
			this.setTenantId(SecurityUtils.getTenantId());
		}

		/**
		 * 放射健康检查表
		 */
		public void radiationHealth() {

		}

		/**
		 * 通过BReportConfig设置配置
		 *
		 * @param reportConfig
		 * @author YJ
		 * @date 2022/4/11 15:48
		 */
		public void setConfig(BReportConfig reportConfig) {
			this.setTemplatePath(reportConfig.getTemplatePath());
			this.setMarks(reportConfig.getMarks());
			this.setShowZY(reportConfig.getIsShowZy());
			this.setFileType(reportConfig.getFileType());
			this.setIsShowJK(reportConfig.getIsShowJk());
			this.setReview(false);
			this.setTenantId(SecurityUtils.getTenantId());
		}


	}

	/**
	 * 单位报告基础数据
	 */
	@Data
	public static class CompanyReport {
		/**
		 * 单位报告编号
		 */
		@ApiModelProperty("单位报告编号")
		@NotBlank(message = "单位报告编号不能为空")
		private String reportId;

		/**
		 * 租户ID
		 */
		@ApiModelProperty("租户ID")
		@NotBlank(message = "租户ID不能为空")
		private String tenantId;

		/**
		 * 模板路径或模板名称
		 */
		@ApiModelProperty("模板路径或模板名称")
		@NotBlank(message = "模板路径或模板名称不能为空")
		private String templatePath;

		/**
		 * 模板里需要的所有标签(如：[card],[items])
		 */
		@ApiModelProperty("模板里需要的所有标签(如：[card],[items])")
		@NotBlank(message = "模板里需要的所有标签(如：[card],[items])")
		private String marks;

		/**
		 * 文件类型 1=pdf , 2=word
		 */
		@ApiModelProperty("文件类型 1=pdf , 2=word")
		@NotBlank(message = "文件类型不能为空")
		private Integer fileType;

		/**
		 * 职业体检数据是否显示
		 */
		@ApiModelProperty("职业体检数据是否显示")
		@NotBlank(message = "职业体检数据是否显示")
		private boolean isShowZY;

		/**
		 * 是否只显示健康体检项目
		 */
		@ApiModelProperty("是否只显示健康体检项目")
		private Boolean isShowJK = false;

		/**
		 * 是否复查
		 */
		@ApiModelProperty("是否复查")
		@NotBlank(message = "是否复查不能为空")
		private boolean isReview;


		/**
		 * 初始化参数-健康
		 *
		 * @author YJ
		 * @date 2022/3/18 9:43
		 */
		public void initHealth() {
			this.setTemplatePath("analysis_report/analysis.rtf");
			this.setMarks(
					"[year]，[data_year],[time_min],[time_max],[time_count],[age_table],[package_table],[detection_table],[patient_table]");
			this.setFileType(1);
			this.setShowZY(false);
			this.setReview(false);
			this.setTenantId(SecurityUtils.getTenantId());
		}

		/**
		 * 初始化参数-职业
		 *
		 * @author YJ
		 * @date 2022/3/18 9:43
		 */
		public void initOccupation() {
			this.setTemplatePath("zhi/company/company.rtf");
			this.setMarks(
					"[user_table],[unusual_tables],[poisontype_tables],[poison_tables],[job_tables],[group_tables],[dept_tables],[chart_sex],[summary],[print_date],[data_year],[poisonCount],[compile_doc],[verify_doc],[radiate]");
			this.setFileType(1);
			this.setShowZY(false);
			this.setReview(false);
			this.setTenantId(SecurityUtils.getTenantId());
		}

		/**
		 * 通过BReportConfig设置配置
		 *
		 * @param reportConfig
		 * @author YJ
		 * @date 2022/4/11 15:48
		 */
		public void setConfig(BReportConfig reportConfig) {
			this.setTemplatePath(reportConfig.getTemplatePath());
			this.setMarks(reportConfig.getMarks());
			this.setShowZY(reportConfig.getIsShowZy());
			this.setFileType(reportConfig.getFileType());
			this.setIsShowJK(reportConfig.getIsShowJk());
			this.setReview(false);
			this.setTenantId(SecurityUtils.getTenantId());
		}
	}

	/**
	 * 单位报告基础数据
	 */
	@Data
	public static class CompanyAnalysisReport extends CompanyReport {

		/**
		 * 病种编号字符串集合，逗号分隔
		 */
		private String sicknessIds;

		/**
		 * 初始化参数
		 *
		 * @author YJ
		 * @date 2022/3/18 9:43
		 */
		public void init() {
			this.setTemplatePath("analysis_report/analysis.rtf");
			this.setMarks("[year],[data_year],[time_min],[time_max],[time_count],[detection_table],[age_table]");
			this.setFileType(1);
			this.setShowZY(false);
			this.setReview(false);
			this.setTenantId(SecurityUtils.getTenantId());
		}

		/**
		 * 通过BReportConfig设置配置
		 *
		 * @param reportConfig
		 * @author YJ
		 * @date 2022/4/11 15:48
		 */
		public void setConfig(BReportConfig reportConfig) {
			this.setTemplatePath(reportConfig.getTemplatePath());
			this.setMarks(reportConfig.getMarks());
			this.setShowZY(reportConfig.getIsShowZy());
			this.setIsShowJK(reportConfig.getIsShowJk());
			this.setFileType(reportConfig.getFileType());
			this.setReview(false);
			this.setTenantId(SecurityUtils.getTenantId());
		}
	}
}
