package com.healthappy.modules.http.report;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ZipUtil;
import com.healthappy.exception.BadRequestException;
import com.healthappy.utils.SpringBeanFactoryUtil;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.Arrays;

/**
 * @Author: FGQ
 * @Desc: 获取文件服务
 * @Date: Created in 10:51 2022/3/21
 */
@Slf4j
@UtilityClass
public class HttpFileServer {

    /**
     * 文件导出
     * @param compression 是否压缩
     * @param zipFileName zip文件名称
     * @param fileUrl 文件地址
     * @return 文件URL
     */
    public String fileZipByUrlRename(Boolean compression,String zipFileName,String... fileUrl){
        if(compression) {
            return buildFile(zipFileName, fileUrl);
        } else {
            return buildFile(fileUrl);
        }
    }

    /**
     * 文件压缩导出
     * @param fileUrl 文件地址
     * @return 文件URL
     */
    public String fileZipByUrl(String... fileUrl){
        return buildFile(IdUtil.simpleUUID(), fileUrl);
    }

    /**
     * 文件地址拼接
     * @author YJ
     * @date 2022/3/22 15:52
     * @param fileUrl
     * @return java.lang.String
     */
    public String fileByUrl(String... fileUrl){
        return buildFile(fileUrl);
    }

    private String buildFile(String[] fileUrl){
        log.debug("fileUrl:{1}",String.join(",",fileUrl));
        //如果是单个文件 默认拼接上URL,并返回
        String resolve = SpringBeanFactoryUtil.resolve("${httpServer.prodUrl}");
        return resolve + fileUrl[0];
    }

    private String buildFile(String zipFileName, String[] fileUrl) {
        log.debug("zipFileName:{0},fileUrl:{1}",zipFileName,String.join(",",fileUrl));
        //如果是单个文件 默认拼接上URL,并返回
        String resolve = SpringBeanFactoryUtil.resolve("${httpServer.prodUrl}");
//        if(fileUrl.length == 1){
//            return resolve + fileUrl[0];
//        }
        String prefixZip = SpringBeanFactoryUtil.resolve("${httpServer.zipUrl}");
        String zipName = zipFileName + ".zip";
        File zip = ZipUtil.zip(FileUtil.file(prefixZip + zipName), false,Arrays.asList(fileUrl).parallelStream().map(k -> FileUtil.file(prefixZip + k)).toArray(File[]::new));
        if(!zip.exists()){
            throw new BadRequestException("压缩失败,未找到文件。");
        }
        return resolve + zipName;
    }
}
