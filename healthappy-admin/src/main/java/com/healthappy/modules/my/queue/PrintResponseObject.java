package com.healthappy.modules.my.queue;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 * @author Jevany
 * @date 2022/4/25 17:48
 */
@Data
public class PrintResponseObject {
	/** 指令名称 */
	@ApiModelProperty("指令名称")
	private String comm;

	/** DownUrl */
	@ApiModelProperty("DownUrl")
	private String downUrl;
}
