package com.healthappy.modules.my.queue;

import cn.hutool.json.JSONUtil;
import com.healthappy.config.WebSocketConfig;
import com.healthappy.config.WebSocketServer;
import com.healthappy.modules.http.report.HttpFileServer;
import com.healthappy.modules.http.report.HttpServer;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 *
 * @author Jevany
 * @date 2022/4/25 23:29
 */
@Slf4j
@Component
@AllArgsConstructor
public class JQueueHandle {

	private final WebSocketServer webSocketServer;

	/**
	 * 生成报告
	 * @author YJ
	 * @date 2022/4/25 23:55
	 * @param queue
	 */
	public void generatePDF(JQueue<PrintRequestObject> queue) throws Exception {
		PrintRequestObject requestObject = null;
		PrintResponseObject responseObject = null;
		String fileName = null;
		String command = null;
		while (queue.size() > 0) {
			Thread.sleep(500);
			requestObject = queue.get();
			if (!Optional.ofNullable(requestObject).isPresent()) {
				return;
			}
			//			log.info("getValue:{}", requestObject);
			log.info("生成开始，参数：{}", requestObject.getParamMap());
			long startTime = System.currentTimeMillis();
			responseObject = new PrintResponseObject();

			try {
				fileName = HttpServer.startReq(requestObject);
				command = WebSocketConfig.PRINT_NOTIFICATION;
				responseObject.setDownUrl(HttpFileServer.fileByUrl(fileName));

			} catch (Exception ex) {
				fileName = "param:" + requestObject.getParamMap() + ",err:" + ex.getMessage();
				command = WebSocketConfig.PRINT_NOTIFICATION_FAILED;
				responseObject.setDownUrl(fileName);
			} finally {
				//WebScoket发送
				System.out.println(fileName);
				responseObject.setComm(requestObject.getCommType());

				webSocketServer.sendMessage(requestObject.getUserName(), JSONUtil.toJsonStr(responseObject), command);
				long times = System.currentTimeMillis() - startTime;
				log.info("参数：{},耗时：{}", requestObject.getParamMap(), String.valueOf(times));

			}
		}
	}
}
