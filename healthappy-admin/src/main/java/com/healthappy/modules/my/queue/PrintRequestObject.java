package com.healthappy.modules.my.queue;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.http.HttpMethod;

import java.util.Map;

/**
 * 报告生成对象
 * @author Jevany
 * @date 2022/4/25 11:44
 */
@Data
public class PrintRequestObject {
	/** HTTP请求类型  默认post */
	@ApiModelProperty("HTTP请求类型")
	private HttpMethod httpMethod=HttpMethod.POST;

	/** 请示对应URL路径 */
	@ApiModelProperty("请示对应URL路径")
	private String requestMapping;

	/** 请示具体Map参数 */
	@ApiModelProperty("请示具体Map参数")
	private Map<String, Object> paramMap;

	/** 指令类型 */
	@ApiModelProperty("指令类型")
	private String commType;

	/** 用户登录id-用于接收的Socket的用户编号 */
	@ApiModelProperty("用户登录id-用于接收的Socket的用户编号")
	private String userName;
}
