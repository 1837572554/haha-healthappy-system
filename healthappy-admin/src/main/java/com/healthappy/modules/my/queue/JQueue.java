package com.healthappy.modules.my.queue;

import java.util.LinkedList;

/**
 *
 * @author Jevany
 * @date 2022/4/25 23:17
 */
public class JQueue<T> {
	private LinkedList<T> list = null;
	int count = 0;

	public JQueue() {
		list = new LinkedList<T>();
	}

	public T get() {
		T obj = null;
		if (!list.isEmpty()) {
			// 第一个添加的元素在队列的尾部
			obj = list.removeLast(); // 删除并返回列表的最后一个元素（第一个被添加的元素）
		}
		return obj;
		// return list.getLast(); //没有删除操作，获取到的永远是最后一个
	}

	public void add(T obj) {
		list.addFirst(obj);
		this.count++;
	}

	public int size() {
		return count;
	}
	

}
