package com.healthappy.modules.redis.service;

import net.oschina.j2cache.CacheObject;

import java.util.Collection;

/**
 *
 * @author Jevany
 * @date 2022/5/12 14:01
 */
public interface RedisUtilService {

	/**
	 * 清除指定Key
	 * @author YJ
	 * @date 2022/5/12 10:05
	 * @param region
	 * @param key
	 */
	public void evic(String region, String key);

	/**
	 * 获取指定Key
	 * @author YJ
	 * @date 2022/5/12 10:05
	 * @param region
	 * @param key
	 */
	public CacheObject get(String region, String key);

	/**
	 * 设置指定Key对象
	 * @author YJ
	 * @date 2022/5/12 10:08
	 * @param region
	 * @param key
	 * @param value
	 */
	public void set(String region, String key, Object value);

	/**
	 * 清除指定区域
	 * @author YJ
	 * @date 2022/5/12 10:06
	 * @param region
	 */
	public void clear(String region);

	/**
	 * 获取指定区域所有的Keys
	 * @author YJ
	 * @date 2022/5/12 10:10
	 * @param region
	 * @return java.util.Collection〈java.lang.String〉
	 */
	public Collection<String> keys(String region);
}