package com.healthappy.modules.redis.service.impl;

import com.healthappy.modules.redis.service.RedisUtilService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.oschina.j2cache.CacheChannel;
import net.oschina.j2cache.CacheObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 *
 * @author Jevany
 * @date 2022/5/12 14:02
 */
@AllArgsConstructor
@Service
@Slf4j
public class RedisUtilServiceImpl implements RedisUtilService {

	@Autowired
	private CacheChannel cacheChannel;

	@Override
	public void evic(String region, String key) {
		try {
			cacheChannel.evict(region, key);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		log.info("[cache][evic] region:{},key:{}", region, key);
	}

	@Override
	public CacheObject get(String region, String key) {
		log.info("[cache][set] region:{},key:{}", region, key);
		return cacheChannel.get(region, key);
	}

	@Override
	public void set(String region, String key, Object value) {
		cacheChannel.set(region, key, value);
		log.info("[cache][set] region:{},key:{},value:{}", region, key, value);
	}

	@Override
	public void clear(String region) {
		cacheChannel.clear(region);
		log.info("[cache][clear] region:{}", region);
	}

	@Override
	public Collection<String> keys(String region) {
		log.info("[cache][keys] region:{}", region);
		return cacheChannel.keys(region);
	}
}