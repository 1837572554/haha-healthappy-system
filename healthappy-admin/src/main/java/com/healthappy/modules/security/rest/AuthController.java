package com.healthappy.modules.security.rest;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.healthappy.annotation.AnonymousAccess;
import com.healthappy.common.entity.TenantEntity;
import com.healthappy.config.TenantConstant;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.security.security.vo.AuthUser;
import com.healthappy.modules.security.service.OnlineUserService;
import com.healthappy.modules.security.service.UserDetailsServiceImpl;
import com.healthappy.modules.system.domain.Tenant;
import com.healthappy.modules.system.service.TenantService;
import com.healthappy.modules.system.service.UserService;
import com.healthappy.security.SecurityProperties;
import com.healthappy.security.TokenProvider;
import com.healthappy.utils.*;
import com.wf.captcha.ArithmeticCaptcha;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * @author hupeng
 * @date 2018-11-23
 * 授权、根据token获取用户详细信息
 */
@Slf4j
@RestController
@RequestMapping("/auth")
@Api(tags = "A系统：系统授权接口")
public class AuthController {

    @Value("${loginCode.expiration}")
    private Long expiration;
    @Value("${rsa.private_key}")
    private String privateKey;
    @Value("${single.login}")
    private Boolean singleLogin;
    private final SecurityProperties properties;
    private final RedisUtils redisUtils;
    private final UserDetailsServiceImpl userDetailsService;
    private final OnlineUserService onlineUserService;
    private final TokenProvider tokenProvider;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final UserService userService;
    private final TenantService tenantService;

    public AuthController(TenantService tenantService,UserService userService,SecurityProperties properties, RedisUtils redisUtils, UserDetailsServiceImpl userDetailsService, OnlineUserService onlineUserService, TokenProvider tokenProvider, AuthenticationManagerBuilder authenticationManagerBuilder) {
        this.properties = properties;
        this.redisUtils = redisUtils;
        this.userDetailsService = userDetailsService;
        this.onlineUserService = onlineUserService;
        this.tokenProvider = tokenProvider;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.userService = userService;
        this.tenantService = tenantService;
    }



    @ApiOperation("无验证码登录")
    @AnonymousAccess
    @PostMapping(value = "/nocodeLogin")
    public R nocodeLogin(@Validated @RequestBody AuthUser authUser, HttpServletRequest request) {
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(authUser.getUsername(), authUser.getPassword());
        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        // 生成令牌
        String token = tokenProvider.createToken(authentication);
        final JwtUser jwtUser = (JwtUser) authentication.getPrincipal();

        //查询是否冻结
        Tenant findByNoStatus = tenantService.getOne(Wrappers.<Tenant>lambdaQuery().eq(TenantEntity::getTenantId, jwtUser.getTenantId()).eq(Tenant::getIsEnable, false));
        if(null != findByNoStatus && ObjectUtil.isNotNull(findByNoStatus.getId()) && !findByNoStatus.getTenantId().toString().equals(TenantConstant.DEFAULT_TENANT_ID)){
            throw new BadRequestException("加盟商已经冻结");
        }
        RedisUtil.set(SecurityConstants.AUTH_USER_TENANT_ID+jwtUser.getUsername(),jwtUser);
        // 保存在线信息
        onlineUserService.save(jwtUser, token, request, SecurityConstants.SOURCE_PC);
        // 返回 token 与 用户信息
        Map<String, Object> authInfo = new HashMap<String, Object>(3) {{
            put("token", properties.getTokenStartWith() + token);
            put("user", jwtUser);
            put("accessTokenExpires",DateUtil.offsetSecond(DateTime.now(), Convert.toInt(redisUtils.getExpire(properties.getOnlineKey() + token))).toString());
        }};
        if (singleLogin) {
            //踢掉之前已经登录的token
            onlineUserService.checkLoginOnUser(authUser.getUsername(), token);
        }
        return R.ok(authInfo);
    }

    @Log("用户登录")
    @ApiOperation("登录授权")
    @AnonymousAccess
    @PostMapping(value = "/login")
    public R login(@Validated @RequestBody AuthUser authUser, HttpServletRequest request) {
        // 密码解密
        RSA rsa = new RSA(privateKey, null);
        String password = new String(rsa.decrypt(authUser.getPassword(), KeyType.PrivateKey));
//        String password = authUser.getPassword();
        // 查询验证码
        String code = (String) redisUtils.get(authUser.getUuid());
        // 清除验证码
        redisUtils.del(authUser.getUuid());
        if (StringUtils.isBlank(code)) {
            throw new BadRequestException("验证码不存在或已过期");
        }
        if (StringUtils.isBlank(authUser.getCode()) || !authUser.getCode().equalsIgnoreCase(code)) {
            throw new BadRequestException("验证码错误");
        }

        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(authUser.getUsername(), password);

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        // 生成令牌
        String token = tokenProvider.createToken(authentication);
        final JwtUser jwtUser = (JwtUser) authentication.getPrincipal();

        //查询是否冻结
//        Tenant findByNoStatus = tenantService.getOne(Wrappers.<Tenant>lambdaQuery().eq(TenantEntity::getTenantId, jwtUser.getTenantId()).eq(Tenant::getIsEnable, false));
//        if(null != findByNoStatus && ObjectUtil.isNotNull(findByNoStatus.getId()) && !findByNoStatus.getTenantId().toString().equals(TenantConstant.DEFAULT_TENANT_ID)){
//            throw new BadRequestException("加盟商已经冻结");
//        }

        String tenantName="";
        if(!jwtUser.getTenantId().equals(TenantConstant.DEFAULT_TENANT_ID)) {
            Tenant tenant = tenantService.getOne(Wrappers.<Tenant>lambdaQuery().eq(TenantEntity::getTenantId, jwtUser.getTenantId()));
            if (ObjectUtil.isEmpty(tenant) || ObjectUtil.isEmpty(tenant.getId())) throw new BadRequestException("加盟商不存在");
            if (tenant.getIsEnable().equals(false)) throw new BadRequestException("加盟商已经冻结");
            tenantName=tenant.getName();
        }

        RedisUtil.set(SecurityConstants.AUTH_USER_TENANT_ID+jwtUser.getUsername(),jwtUser);
        // 保存在线信息
        onlineUserService.save(jwtUser, token, request,SecurityConstants.SOURCE_PC);
        // 返回 token 与 用户信息
        Map<String, Object> authInfo = new HashMap<String, Object>(3) {{
            put("token", properties.getTokenStartWith() + token);
            put("user", jwtUser);
        }};
        if (singleLogin) {
            //踢掉之前已经登录的token
            onlineUserService.checkLoginOnUser(authUser.getUsername(), token);
        }
        authInfo.put("username", StrUtil.isNotBlank(tenantName) ?tenantName : "管理员");
        return R.ok(authInfo);
    }

    @ApiOperation("获取用户信息")
    @GetMapping(value = "/info")
    public R getUserInfo() {
        JwtUser jwtUser = (JwtUser) userDetailsService.loadUserByUsername(SecurityUtils.getUsername());
        return R.ok(jwtUser);
    }

    @AnonymousAccess
    @ApiOperation("获取验证码")
    @GetMapping(value = "/code")
    public R getCode() {
        // 算术类型 https://gitee.com/whvse/EasyCaptcha
        ArithmeticCaptcha captcha = new ArithmeticCaptcha(111, 36);
        // 几位数运算，默认是两位
        captcha.setLen(2);
        // 获取运算的结果
        String result = "";
        try {
            result = new Double(Double.parseDouble(captcha.text())).intValue() + "";
        } catch (Exception e) {
            result = captcha.text();
        }
        String uuid = properties.getCodeKey() + IdUtil.simpleUUID();
        // 保存
        redisUtils.set(uuid, result, expiration, TimeUnit.MINUTES);
        // 验证码信息
        Map<String, Object> imgResult = new HashMap<String, Object>(2) {{
            put("img", captcha.toBase64());
            put("uuid", uuid);
        }};
        return R.ok(imgResult);
    }

    @ApiOperation("退出登录")
    @AnonymousAccess
    @DeleteMapping(value = "/logout")
    public R logout(HttpServletRequest request) {
        onlineUserService.logout(tokenProvider.getToken(request));
        return R.ok();
    }
}
