package com.healthappy.modules.security.security.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * @author FGQ
 */
@Data
public class AppletLoginParam {

    private String sessionKey;

    private String encryptedData;

    private String iv;

    private String openId;
}
