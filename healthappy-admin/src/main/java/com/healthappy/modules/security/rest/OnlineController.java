package com.healthappy.modules.security.rest;

import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.security.service.OnlineUserService;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

/**
 * @author hupeng
 */
@RestController
@RequestMapping("/auth/online")
@Api(tags = "系统：在线用户管理")
public class OnlineController {

    private final OnlineUserService onlineUserService;

    public OnlineController(OnlineUserService onlineUserService) {
        this.onlineUserService = onlineUserService;
    }

    @ApiOperation("查询在线用户")
    @GetMapping
    @PreAuthorize("@el.check()")
    public R getAll(@RequestParam(value = "filter", defaultValue = "") String filter,
                                         @RequestParam(value = "type", defaultValue = "0") int type,
                                         Pageable pageable) {
        return R.ok(onlineUserService.getAll(filter, type, pageable));
    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check()")
    public void download(HttpServletResponse response,
                         @RequestParam(value = "filter", defaultValue = "") String filter,
                         @RequestParam(value = "type", defaultValue = "0") int type) throws IOException {
        onlineUserService.download(onlineUserService.getAll(filter, type), response);
    }


    @ApiOperation("踢出用户")
    @DeleteMapping
    @PreAuthorize("@el.check()")
    public R delete(@RequestBody Set<String> keys) throws Exception {

        for (String key : keys) {
            onlineUserService.kickOut(key);
        }
        return R.ok();
    }


    @ApiOperation("踢出移动端用户")
    @PostMapping("/delete")
    @PreAuthorize("@el.check()")
    public R deletet(@RequestBody Set<String> keys) throws Exception {

        for (String key : keys) {
            onlineUserService.kickOutT(key);
        }
        return R.ok();
    }
}
