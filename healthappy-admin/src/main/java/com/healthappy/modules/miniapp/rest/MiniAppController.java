package com.healthappy.modules.miniapp.rest;

import com.healthappy.annotation.AnonymousAccess;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.miniapp.service.MiniAppService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Jevany
 * @date 2022/5/17 14:59
 */
@Slf4j
@Api(tags = "Token")
@RestController
@AllArgsConstructor
@RequestMapping("/Token")
public class MiniAppController {
	private final MiniAppService miniAppService;

	/**
	 * 测试限流注解，下面配置说明该接口 60秒内最多只能访问 10次，保存到redis的键名为 limit_test，
	 */
	@Log("小哈健康：获取Token")
	@GetMapping("/getToken")
	@AnonymousAccess
	@ApiOperation("获取Token")
	public String getToken() {
		String token = miniAppService.getToken();
		
		return token;
	}

}
