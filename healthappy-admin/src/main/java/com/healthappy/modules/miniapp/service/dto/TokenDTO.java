package com.healthappy.modules.miniapp.service.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Token对象
 * @author Jevany
 * @date 2022/5/17 15:20
 */
@Data
public class TokenDTO implements Serializable {


	private static final long serialVersionUID = 1L;
}