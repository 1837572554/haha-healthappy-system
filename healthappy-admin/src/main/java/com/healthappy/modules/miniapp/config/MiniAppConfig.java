package com.healthappy.modules.miniapp.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 小哈健康-小程序配置
 * @author Jevany
 * @date 2022/5/17 15:00
 */
@Data
@Component
public class MiniAppConfig {

	/** 获得TOKEN连接 */
	public static String baseUrl;

	/** token 盐值 */
	public static String saltValue;

	/** token 登录用户名 */
	public static String tokenUserName;

	/** token 登录密码 */
	public static String tokenPassword;

	/** token 授权类型 */
	public static String tokenGrantType;


	/** token 连接 */
	public static String TokenUrl = "/auth/oauth/token?grant_type=";

	/** 获得TOKEN连接 */
	@Value("${miniappHealth.baseUrl}")
	private String baseUrlVal;

	/** token 盐值 */
	@Value("${miniappHealth.saltValue}")
	private String saltValueVal;

	/** token 登录用户名 */
	@Value("${miniappHealth.tokenUserName}")
	private String tokenUserNameVal;

	/** token 登录密码 */
	@Value("${miniappHealth.tokenPassword}")
	private String tokenPasswordVal;

	/** token 授权类型 */
	@Value("${miniappHealth.tokenGrantType}")
	private String tokenGrantTypeVal;

	@PostConstruct
	public void initConfig() {
		baseUrl = this.baseUrlVal;
		saltValue = this.saltValueVal;
		tokenUserName = this.tokenUserNameVal;
		tokenPassword = this.tokenPasswordVal;
		tokenGrantType = this.tokenGrantTypeVal;
	}
}
