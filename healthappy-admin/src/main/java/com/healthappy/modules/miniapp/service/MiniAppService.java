package com.healthappy.modules.miniapp.service;

/**
 *
 * @author Jevany
 * @date 2022/5/17 16:11
 */
public interface MiniAppService {

	/**
	 * 获得租户对应的
	 * @author YJ
	 * @date 2022/5/18 13:57
	 * @return java.lang.String
	 */
	String getToken();


}