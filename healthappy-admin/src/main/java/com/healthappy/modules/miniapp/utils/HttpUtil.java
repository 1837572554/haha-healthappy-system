package com.healthappy.modules.miniapp.utils;

import cn.hutool.json.JSONObject;
import com.healthappy.enums.OkHttpPostEnum;
import com.healthappy.utils.OkHttpUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;

/**
 * HTTP请求工具类
 * @author Jevany
 * @date 2022/5/17 16:18
 */
@Slf4j
@UtilityClass
public class HttpUtil {

	/**
	 * timeout   超时时长，-1表示默认超时，单位毫秒
	 */
	@ApiModelProperty("timeout   超时时长，-1表示默认超时，单位毫秒")
	private final static int TIMEOUT = -1;


	@SneakyThrows
	public String getToken(String url, String userName, String password, String saltValue) {
		Response response = OkHttpUtils.builder().url(url).addHeader("Authorization", saltValue)
				.addParam("username", userName).addParam("password", password).post(OkHttpPostEnum.MULTIPART_FORM)
				.execute();
		if (response.code() == 200) {
			JSONObject resultObj = new JSONObject(response.body().string());
			return resultObj.getStr("access_token");
		} else {
			return "";
		}

		//		OkHttpClient client = new OkHttpClient().newBuilder().build();
		//		MediaType mediaType = MediaType.parse("text/plain");
		//		RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart("username", userName)
		//				.addFormDataPart("password", password).build();
		//		Request request = new Request.Builder().url(url).method("POST", body).addHeader("Authorization", saltValue)
		//				.build();
		//		Response response = client.newCall(request).execute();
		//		if (response.code() == 200) {
		//
		//			JSONObject resultObj = new JSONObject(response.body().string());
		//			return resultObj.getStr("access_token");
		//		} else {
		//			return "";
		//		}
		//		return result;
	}


	public String ss() {
		OkHttpClient client = new OkHttpClient().newBuilder().build();
		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType, "]");
		Request request = new Request.Builder().url("http://10.99.1.153:9002/arrange/save").method("POST", body)
				.addHeader("tenantId", "625777")
				.addHeader("Authorization", "Bearer 837a3f9a-451b-4697-b2d5-8bdfb7802a66")
				.addHeader("Content-Type", "application/json").build();
		//		Response response = client.newCall(request).execute();
		return "";
	}


}
