package com.healthappy.modules.miniapp.service.impl;

import cn.hutool.core.util.StrUtil;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.miniapp.config.MiniAppConfig;
import com.healthappy.modules.miniapp.service.MiniAppService;
import com.healthappy.modules.miniapp.utils.HttpUtil;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 *
 * @author Jevany
 * @date 2022/5/17 16:52
 */
@AllArgsConstructor
@Service
public class TokenServiceImpl implements MiniAppService {
	@Override
	@Cacheable(value = "miniAppToken")
	public String getToken() {
		String token = HttpUtil.getToken(MiniAppConfig.baseUrl + MiniAppConfig.TokenUrl + MiniAppConfig.tokenGrantType,
				MiniAppConfig.tokenUserName, MiniAppConfig.tokenPassword, MiniAppConfig.saltValue);
		if (StrUtil.isBlank(token)) {
			throw new BadRequestException("Token获取失败");
		}
		return token;
		//		Map<String, Object> map = new HashMap<>();
		//		map.put("username", MiniAppConfig.tokenUserName);
		//		map.put("password", MiniAppConfig.tokenPassword);
		//		return HttpUtil.exec(HttpMethod.POST, MiniAppConfig.TokenUrl + MiniAppConfig.tokenGrantType,
		//				MiniAppConfig.saltValue, map);
	}

}