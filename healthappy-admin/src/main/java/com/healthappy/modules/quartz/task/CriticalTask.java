package com.healthappy.modules.quartz.task;

import com.healthappy.modules.system.service.TCriticalService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * @author Jevany
 * @date 2022/3/17 14:37
 */
@Component
@AllArgsConstructor
public class CriticalTask {
    private final TCriticalService criticalService;

    public synchronized void criticalNotification(){
        criticalService.listNeedNotificationCritical();
    }
}
