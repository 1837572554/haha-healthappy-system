package com.healthappy.modules.quartz.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.quartz.domain.QuartzLog;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author hupeng
 * @date 2020-05-13
 */
@Repository
@Mapper
public interface QuartzLogMapper extends CoreMapper<QuartzLog> {

}
