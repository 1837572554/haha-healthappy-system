package com.healthappy.modules.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BGroupHDWuJia;
import com.healthappy.modules.system.domain.BPoisonSymptom;
import com.healthappy.modules.system.service.BPoisonSymptomService;
import com.healthappy.modules.system.service.dto.BPoisonSymptomDto;
import com.healthappy.modules.system.service.dto.BPoisonSymptomQueryCriteria;
import com.healthappy.modules.system.service.mapper.BPoisonSymptomMapper;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BPoisonSymptomServiceImpl extends BaseServiceImpl<BPoisonSymptomMapper, BPoisonSymptom> implements BPoisonSymptomService {


    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BPoisonSymptomQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BPoisonSymptom> page = new PageInfo<BPoisonSymptom>(queryAll(criteria));//
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("content", generator.convert(page.getList(), BPoisonSymptomDto.class));
        map.put("totalElements", page.getTotal());

        return map;
    }

    @Override
    public List<BPoisonSymptom> queryAll(BPoisonSymptomQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BPoisonSymptomDto.class, criteria));
    }

    /**
     * 根据危害因素id删除
     * @param poisonId
     * @return
     */
    @Override
    public boolean deleteByPoId(String poisonId){
        return   this.lambdaUpdate().eq(BPoisonSymptom::getPoisonId,poisonId).remove();
    }
    /**
     * 根据危害因素id集合批量删除
     * @param poisonIds
     * @return
     */
    @Override
    public boolean deleteByPoIds(List<Long> poisonIds){
        return   this.lambdaUpdate().in(BPoisonSymptom::getPoisonId,poisonIds).remove();
    }
    /**
     * 根据症状id删除
     * @param symptomId
     * @return
     */
    @Override
    public boolean deleteBySymId(Long symptomId){
        return   this.lambdaUpdate().eq(BPoisonSymptom::getSymptomId,symptomId).remove();
    }

    /**
     * 根据症状id集合批量删除
     * @param symptomIds
     * @return
     */
    @Override
    public boolean deleteBySymIds(List<Long> symptomIds){
        return   this.lambdaUpdate().in(BPoisonSymptom::getSymptomId,symptomIds).remove();
    }
}
