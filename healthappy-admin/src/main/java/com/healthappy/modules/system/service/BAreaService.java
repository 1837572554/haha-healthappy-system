package com.healthappy.modules.system.service;

import cn.hutool.core.lang.tree.Tree;
import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BArea;
import com.healthappy.modules.system.service.dto.BAreaQueryCriteria;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.data.domain.Pageable;

/**
 * @description 区域表（阿斯利康）服务层
 * @author fang
 * @date 2021-06-17
 */
public interface BAreaService extends BaseService<BArea> {


    Map<String, Object> queryAll(BAreaQueryCriteria criteria, Pageable pageable);


    /**
     * 查询数据分页
     * @param criteria 条件
     * @return Map<String, Object>
     */
    List<BArea> queryAll(BAreaQueryCriteria criteria);

    List<Tree<String>> tree(BAreaQueryCriteria criteria);

    Integer checkExistHiCode(Set<Integer> ids);
}
