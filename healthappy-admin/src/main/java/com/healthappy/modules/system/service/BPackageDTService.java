package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BPackageDT;
import com.healthappy.modules.system.service.dto.BPackageDTQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


/**
 * @description 单位表 服务层
 * @author sjc
 * @date 2021-06-24
 */
public interface BPackageDTService extends BaseService<BPackageDT> {
    Map<String, Object> queryAll(BPackageDTQueryCriteria criteria, Pageable pageable);

    List<BPackageDT> queryAll(BPackageDTQueryCriteria criteria);

    List<BPackageDT> getGroupDispOrder(String packageId);
}
