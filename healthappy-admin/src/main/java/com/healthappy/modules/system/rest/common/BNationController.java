package com.healthappy.modules.system.rest.common;

import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BNation;
import com.healthappy.modules.system.service.BNationService;
import com.healthappy.modules.system.service.dto.BNationQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description 民族 控制器
 * @author FGQ
 * @date 2021-10-27
 */
@Slf4j
@Api(tags = "默认基础数据：民族")
@RestController
@AllArgsConstructor
@RequestMapping("/BNation")
public class BNationController {

    private final BNationService bNationService;

    @Log("民族")
    @ApiOperation("民族")
    @GetMapping
    public R listPage(BNationQueryCriteria criteria) {
        return R.ok(bNationService.list(QueryHelpPlus.getPredicate(BNation.class,criteria)));
    }


}
