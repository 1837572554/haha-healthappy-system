package com.healthappy.modules.system.service;

import com.healthappy.utils.R;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @Author: wukefei
 * @Date: Created in 2022/3/30 9:38
 * @Description:
 * @Version: 1.0
 *
 */
public interface RequestInterfaceService {

	public Map<String, String>  getList();

	public Boolean check(String key);

	public Boolean checkHostandPort( String host,  int port);

	public Boolean run();

	public Boolean shutdwon();
}
