package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.healthappy.config.SeedIdGenerator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 物价表
 * @author SJC
 * @date 2021-08-3
 */
@Data
@ApiModel("物价表")
@TableName("B_WuJia")
@SeedIdGenerator
public class BWuJia implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     *主键id
     */
    @ApiModelProperty("主键id")
    @TableId(type = IdType.INPUT)
    private String id;


    /**
     * 物价项目ID
     */
    @ApiModelProperty("物价项目ID")
    private String code;


    /**
     * 物价项目名称
     */
    @ApiModelProperty("物价项目名称")
    private String itemCnname;

    /**
     * 物价项目名称简拼
     */
    @ApiModelProperty("物价项目名称简拼")
    private String jp;
    /**
     * 价格
     */
    @ApiModelProperty("价格")
    private Double price;
    /**
     * 单位
     */
    @ApiModelProperty("单位")
    private String pkgUnit;
    /**
     * 规格
     */
    @ApiModelProperty("规格")
    private String pkgName;
    /**
     * 默认打折
     */
    @ApiModelProperty("默认打折")
    private String discountType;
    /**
     * 折扣比例，默认1(代表不打折)
     */
    @ApiModelProperty("折扣比例，默认1(代表不打折)")
    private Double discountValue;
    /**
     * 实际价格
     */
    @ApiModelProperty("实际价格")
    private Double fPrice;
    /**
     * 组合状态，默认为0，暂时系统不需要
     */
    @ApiModelProperty("组合状态，默认为0，暂时系统不需要")
    private String combineType;
    /**
     * 备注1
     */
    @ApiModelProperty("备注1")
    @TableField("remarks_1")
    private String remarks1;
    /**
     * 备注2
     */
    @ApiModelProperty("备注2")
    @TableField("remarks_2")
    private String remarks2;
    /**
     * 费别
     */
    @ApiModelProperty("费别")
    private String payOff;
    /**
     * 医疗机构id(u_hospital_info的id)
     */
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;

}
