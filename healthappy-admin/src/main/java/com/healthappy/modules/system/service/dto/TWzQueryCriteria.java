package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @description 问诊表
 * @author sjc
 * @date 2021-12-15
 */
@Data
public class TWzQueryCriteria {

    /**
     * 体检号
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("体检号")
    private String paId;


}
