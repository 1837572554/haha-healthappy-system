package com.healthappy.modules.system.rest.basic;

import cn.hutool.core.collection.CollUtil;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.vo.BGroupHDWuJiaVo;
import com.healthappy.modules.system.service.BGroupHDWuJiaService;
import com.healthappy.modules.system.service.dto.BGroupHDWuJiaQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @description 组合项目物价 控制器
 * @author sjc
 * @date 2021-08-3
 */
@Validated
@Slf4j
@Api(tags = "基础管理：组合项目物价设置")
@RestController
@AllArgsConstructor
@RequestMapping("/BGroupHDWuJia")
public class BGroupHDWuJiaController {


    private final BGroupHDWuJiaService bGroupHDWuJiaService;

    @Log("分页查询组合项目物价数据")
    @ApiOperation("分页查询组合项目物价数据，权限码：BGroupHDWuJia:list")
    @GetMapping()
    @PreAuthorize("@el.check('BGroupHDWuJia:list','BGroupHd:add')")
    public R listPage(BGroupHDWuJiaQueryCriteria criteria, Pageable pageable) {
        return R.ok(bGroupHDWuJiaService.queryAll(criteria, pageable));
    }

    @Log("新增|修改组合项目物价")
    @ApiOperation("新增|修改组合项目物价，权限码：BGroupHDWuJia:add")
    @PostMapping
    @PreAuthorize("@el.check('BGroupHDWuJia:add')")
    @Klock
    public R saveOrUpdate(@Valid @RequestBody List<BGroupHDWuJiaVo> resources) {
        if(CollUtil.isEmpty(resources)){
            throw new BadRequestException("参数不能为空");
        }
        bGroupHDWuJiaService.saveOrUpdate(resources);
        return R.ok();
    }

    @Log("根据组合项目id来删除组合项目物价")
    @ApiOperation("根据组合项目id来删除组合项目物价，权限码：BGroupHDWuJia:delete")
    @DeleteMapping("/delGroupId")
    @PreAuthorize("@el.check('BGroupHDWuJia:delete')")
    @Klock
    public R removeByGroupId(@RequestParam("groupId") String groupId) {
        if(bGroupHDWuJiaService.deleteByGroupId(groupId))
            return R.ok();
        return R.error(500,"方法异常");
    }

    @Log("根据id来删除组合项目物价")
    @ApiOperation("根据id来删除组合项目物价，权限码：BGroupHDWuJia:delete")
    @DeleteMapping("/delId")
    @PreAuthorize("@el.check('BGroupHDWuJia:delete')")
    @Klock
    public R removeById(@RequestParam("id") Long id) {
        if(bGroupHDWuJiaService.deleteById(id))
            return R.ok();
        return R.error(500,"方法异常");
    }
}
