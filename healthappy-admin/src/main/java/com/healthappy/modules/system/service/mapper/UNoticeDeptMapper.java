package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.UNoticeDept;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 公告对应科室表
 * @author sjc
 * @date 2021-09-3
 */
@Mapper
@Repository
public interface UNoticeDeptMapper extends CoreMapper<UNoticeDept> {

}
