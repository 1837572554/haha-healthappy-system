package com.healthappy.modules.system.service.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author FGQ
 * @date 2020-05-14
 */
@Data
public class BPeTypeJobDto implements Serializable {

    /**
     * id
     */
    private Long id;

    /**
     * 名称
     */
    private String name;

    /**
     * 排序
     */
    private Integer dispOrder;

    /**
     * 名称
     */
    private String code;

    /**
     * 类型(1代表职业；2代表从业)
     */
    private String type;

    private String typeText;
}
