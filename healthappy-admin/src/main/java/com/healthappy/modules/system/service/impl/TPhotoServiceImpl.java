package com.healthappy.modules.system.service.impl;

import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.TPhoto;
import com.healthappy.modules.system.service.TPhotoService;
import com.healthappy.modules.system.service.mapper.TPhotoMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class TPhotoServiceImpl extends BaseServiceImpl<TPhotoMapper, TPhoto> implements TPhotoService {

    //服务器端口
    @Value("${address.ip}")
    private String ip;

    /**
     * 查询人员照片数据
     * @return Object
     */
    public TPhoto query(String paId) {
        TPhoto tPhoto = this.getById(paId);
        if(tPhoto == null)
        {
            return null;
        }
        //给文件路径加上ip地址
//        tPhoto.setIdImage(ip+tPhoto.getIdImage());
//        tPhoto.setPortrait(ip+tPhoto.getPortrait());
        tPhoto.setIdImage(tPhoto.getIdImage());
        tPhoto.setPortrait(tPhoto.getPortrait());
        return tPhoto;
    }
}
