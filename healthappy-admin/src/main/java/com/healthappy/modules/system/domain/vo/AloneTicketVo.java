package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class AloneTicketVo {

    /** 类型 1:收据,2:发票 */
    @NotNull(message = "类型不能为空")
    @ApiModelProperty("类型 1:收据,2:发票")
    private Integer type;

    /** 票号 */
    @ApiModelProperty("票号")
    private String ticket;

    /** 增长序列 */
    @ApiModelProperty("增长序列")
    private List<Integer> addSequence;
}
