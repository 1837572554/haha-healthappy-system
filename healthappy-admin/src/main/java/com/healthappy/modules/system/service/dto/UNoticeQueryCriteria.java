package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

@Data
public class UNoticeQueryCriteria {

    @Query(type = Query.Type.BETWEEN)
    private List<Timestamp> time;
}
