package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @desc: 毒害精简类
 * @author: YJ
 * @date: 2021-12-07 18:23
 **/
@Data
public class BPoisonLiteVo {
    /** id */
    @ApiModelProperty("id")
    private String id;

    /** 毒害名称 */
    @NotBlank(message = "毒害名称不能为空")
    @ApiModelProperty("毒害名称")
    private String poisonName;

    /** 毒害简拼 */
    @ApiModelProperty("毒害简拼")
    private String searchCode;
}
