package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BPeTypeJobQueryCriteria {

    @Query(type = Query.Type.INNER_LIKE)
    private String name;

    /**
     * 类型(1代表职业；2代表从业)
     */
    @ApiModelProperty("类型(1代表职业；2代表从业)")
    @Query
    private String type;
}
