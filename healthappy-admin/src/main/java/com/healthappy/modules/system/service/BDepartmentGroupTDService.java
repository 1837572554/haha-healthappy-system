package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BDepartmentGroupTD;
import com.healthappy.modules.system.domain.vo.BDepartmentGroupTDSaveVo;
import com.healthappy.modules.system.service.dto.BDepartmentGroupTDQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 分组项目表 服务层
 * @author sjc
 * @date 2021-06-24
 */
public interface BDepartmentGroupTDService extends BaseService<BDepartmentGroupTD> {

    Map<String, Object> queryAll(BDepartmentGroupTDQueryCriteria criteria, Pageable pageable);

    Map<String, Object> list(BDepartmentGroupTDQueryCriteria criteria, Pageable pageable);

    /**
     * 查询分组下的项目数据
     * @return Object
     */
    List<BDepartmentGroupTD> queryAll(BDepartmentGroupTDQueryCriteria criteria);
    /**
     * 根据分组id删除所有的分组项目
     * @param id
     * @return
     */
    boolean deleteBDepartmentGroupTD(String id);
    /**
     * 保存分组项目
     * @param list
     * @return
     */
    boolean insertSelective(List<BDepartmentGroupTD> list);

    boolean addOrUpdate(BDepartmentGroupTDSaveVo saveVo);

    List<BDepartmentGroupTD> getBDepartmentGroup(BDepartmentGroupTDQueryCriteria criteria);
}
