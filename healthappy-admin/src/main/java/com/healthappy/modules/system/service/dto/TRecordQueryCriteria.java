package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @description 职业史表
 * @author sjc
 * @date 2021-12-15
 */
@Data
public class TRecordQueryCriteria  {

    /**
     * 体检号
     */
    @ApiModelProperty("体检号")
    @Query(type = Query.Type.EQUAL)
    private String paId;

}
