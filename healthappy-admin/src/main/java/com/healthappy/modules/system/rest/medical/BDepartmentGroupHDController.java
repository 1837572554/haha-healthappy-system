package com.healthappy.modules.system.rest.medical;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONObject;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BDepartmentGroupHD;
import com.healthappy.modules.system.domain.vo.BDepartmentGroupHDVo;
import com.healthappy.modules.system.service.BDepartmentGroupHDService;
import com.healthappy.modules.system.service.dto.BDepartmentGroupHDQueryCriteria;
import com.healthappy.utils.R;
import com.healthappy.utils.SecurityUtils;
import com.healthappy.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

/**
 * @description 分组项目管理 控制器
 * @author FGQ
 * @date 2021-06-24
 */
@Slf4j
@Api(tags = "体检管理：分组项目管理")
@RestController
@AllArgsConstructor
@RequestMapping("/BDepartmentGroupHD")
public class BDepartmentGroupHDController {

    private final BDepartmentGroupHDService bDepartmentGroupHDService;

    @Log("分页查询分组表")
    @ApiOperation("分页查询分组表，权限码：BDepartmentGroupHD:list")
    @GetMapping
    @PreAuthorize("@el.check('BDepartmentGroupHD:list')")
    public R list(BDepartmentGroupHDQueryCriteria criteria, Pageable pageable) {
        return R.ok(bDepartmentGroupHDService.queryAll(criteria,pageable));
    }

    @Log("查询全部分组表")
    @ApiOperation("查询全部分组表，权限码：BDepartmentGroupHD:list")
    @GetMapping("/list")
    @PreAuthorize("@el.check('BDepartmentGroupHD:list')")
    public R list(BDepartmentGroupHDQueryCriteria criteria) {
        return R.ok(bDepartmentGroupHDService.queryAll(criteria));
    }

    @Log("新增|修改分组表")
    @ApiOperation("新增|修改分组表，权限码：BDepartmentGroupHD:add")
    @PostMapping
    @PreAuthorize("@el.check('BDepartmentGroupHD:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BDepartmentGroupHDVo resources, HttpServletRequest request) {
        String ip = StringUtils.getIp(request);
        bDepartmentGroupHDService.saveGroupHd(resources,ip);
        return R.ok();
    }

    @Log("删除分组表")
    @ApiOperation("删除分组表，权限码：BDepartmentGroupHD:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('BDepartmentGroupHD:delete')")
    @Klock
    public R remove(@RequestBody Set<String> ids) {
        if(bDepartmentGroupHDService.removeByIds(ids))
            return R.ok();
        return R.error(500,"方法异常");
    }
}
