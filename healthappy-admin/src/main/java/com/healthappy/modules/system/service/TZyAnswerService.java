package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TZyAnswer;

import java.util.Map;

/**
 * @author Jevany
 * @date 2021/12/8 19:35
 * @desc 体检者问卷 答案 服务
 */
public interface TZyAnswerService extends BaseService<TZyAnswer> {
    /**
     * 计算题库1 分数数据
     * @author YJ
     * @date 2021/12/9 19:45
     * @param paId 体检号
     * @param bankId 库编号
     * @return java.lang.String
     */
    Map<String, String> calcScoreBank1(String paId,Integer bankId);

    /**
     * 计算题库2 分数数据
     * @author YJ
     * @date 2021/12/9 19:46
     * @param paId 体检号
     * @param bankId 库编号
     * @return java.lang.String
     */
    Map<String, String> calcScoreBank2(String paId, Integer bankId);
}
