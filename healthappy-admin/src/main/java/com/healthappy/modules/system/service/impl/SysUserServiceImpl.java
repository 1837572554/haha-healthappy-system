package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.config.databind.DataBind;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.exception.EntityExistException;
import com.healthappy.modules.system.domain.Role;
import com.healthappy.modules.system.domain.User;
import com.healthappy.modules.system.domain.UsersDepts;
import com.healthappy.modules.system.domain.UsersRoles;
import com.healthappy.modules.system.service.*;
import com.healthappy.modules.system.service.dto.UserDto;
import com.healthappy.modules.system.service.dto.UserQueryCriteria;
import com.healthappy.modules.system.service.mapper.RoleMapper;
import com.healthappy.modules.system.service.mapper.SysUserMapper;
import com.healthappy.utils.*;
import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

/**
 * @author hupeng
 * @date 2020-05-14
 */
@Service
//@AllArgsConstructor
@CacheConfig(cacheNames = CacheConstant.USER)
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class SysUserServiceImpl extends BaseServiceImpl<SysUserMapper, User> implements UserService {

    @Value("${file.avatar}")
    private String avatar;

    private final IGenerator generator;
    private final SysUserMapper userMapper;
    private final UserAvatarService userAvatarService;
    private final RoleMapper roleMapper;
    private final RedisUtils redisUtils;
    private final UsersRolesService usersRolesService;
    private final PasswordEncoder passwordEncoder;
    private final UsersDeptsService usersDeptsService;
    private final TagService tagService;

    public SysUserServiceImpl(IGenerator generator, SysUserMapper userMapper,
                              UserAvatarService userAvatarService, RoleMapper roleMapper, RedisUtils redisUtils,
                              UsersRolesService usersRolesService,
                              PasswordEncoder passwordEncoder,
                              UsersDeptsService usersDeptsService,
                              TagService tagService) {
        this.generator = generator;
        this.userMapper = userMapper;
        this.userAvatarService = userAvatarService;
        this.roleMapper = roleMapper;
        this.redisUtils = redisUtils;
        this.usersRolesService = usersRolesService;
        this.passwordEncoder = passwordEncoder;
        this.usersDeptsService = usersDeptsService;
        this.tagService = tagService;
    }

    @Override
    @Cacheable
    public Map<String, Object> queryAll(UserQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<User> page = new PageInfo<>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", generator.convert(page.getList(), UserDto.class));
        map.put("totalElements", page.getTotal());
        return map;
    }


    @Override
    @Cacheable
    public List<User> queryAll(UserQueryCriteria criteria) {
        List<User> userList = baseMapper.selectList(QueryHelpPlus.getPredicate(User.class, criteria));
        for (User user : userList) {
            user.setRoles(roleMapper.findByUsers_Id(user.getId()));
            Set<UsersDepts> byUserId = usersDeptsService.findByUserId(user.getId());
            if (!byUserId.isEmpty()) {
                Set<String> depts = new HashSet<>();
                for (UsersDepts usersDepts : byUserId) {
                    depts.add(usersDepts.getDeptId());
                }
                user.setDepts(depts);

            }

            user.setTagList(tagService.getUserIdByTag(user.getId()));
        }
        return userList;
    }


    @Override
    public void download(List<UserDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (UserDto user : all) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("邮箱", user.getEmail());
            map.put("状态：1启用、0禁用", user.getIsEnable());
            map.put("密码", user.getPassword());
            map.put("用户名", user.getUsername());
            map.put("手机号码", user.getPhone());
            map.put("创建日期", user.getCreateTime());
            map.put("最后修改密码的日期", user.getLastPasswordResetTime());
            map.put("昵称", user.getNickName());
            map.put("性别", user.getSex());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    /**
     * 根据用户名查询
     *
     * @param userName /
     * @return /
     */
    @Override
    @Cacheable
    public UserDto findByName(String userName) {
        User user = userMapper.findByName(userName);
        return generator.convert(user, UserDto.class);
    }

    @Override
    @Cacheable
    public UserDto findById(Long userId) {
        User user = userMapper.findById(userId);
        return generator.convert(user, UserDto.class);
    }

    /**
     * 修改密码
     *
     * @param username        用户名
     * @param encryptPassword 密码
     */
    @Override
    @CacheEvict(allEntries = true)
    public void updatePass(String username, String encryptPassword) {
        userMapper.updatePass(encryptPassword, DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"), username);
    }

    /**
     * 修改头像
     *
     * @param multipartFile 文件
     */
    @Override
    public void updateAvatar(MultipartFile multipartFile) {
//        User user = this.getOne(new LambdaQueryWrapper<User>()
//                .eq(User::getUsername, SecurityUtils.getUsername()));
//        UserAvatar userAvatar = userAvatarService.getOne(new LambdaQueryWrapper<UserAvatar>()
//                .eq(UserAvatar::getId, user.getAvatarId()));
//        String oldPath = "";
//        if (userAvatar != null) {
//            oldPath = userAvatar.getPath();
//        } else {
//            userAvatar = new UserAvatar();
//        }
//        File file = FileUtil.upload(multipartFile, avatar);
//        assert file != null;
//        userAvatar.setRealName(file.getName());
//        userAvatar.setPath(file.getPath());
//        userAvatar.setSize(FileUtil.getSize(multipartFile.getSize()));
//        userAvatarService.saveOrUpdate(userAvatar);
//       // user.setAvatarId(userAvatar.getId());
//        this.saveOrUpdate(user);
//        if (StringUtils.isNotBlank(oldPath)) {
//            FileUtil.del(oldPath);
//        }
    }

    /**
     * 修改邮箱
     *
     * @param username 用户名
     * @param email    邮箱
     */
    @Override
    public void updateEmail(String username, String email) {
        userMapper.updateEmail(email, username);
    }

    /**
     * 新增用户
     *
     * @param resources /
     * @return /
     */
    @Override
    @CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public boolean create(User resources) {
//        User userName = this.getOne(new LambdaQueryWrapper<User>()
//                .eq(User::getUsername, resources.getUsername()));
//        ;
        User userName = userMapper.getNotTenant(Wrappers.<User>lambdaQuery().eq(User::getUsername, resources.getUsername()).last("limit 1"));
        if (userName != null) {
            throw new EntityExistException(User.class, "username", resources.getUsername());
        }


//        User userEmail = this.getOne(new LambdaQueryWrapper<User>()
//                .eq(User::getEmail, resources.getEmail()));
        if (StrUtil.isNotBlank(resources.getEmail())) {
            User userEmail = userMapper.getNotTenant(Wrappers.<User>lambdaQuery().eq(User::getEmail, resources.getEmail()).last("limit 1"));
            if (userEmail != null) {
                throw new EntityExistException(User.class, "email", resources.getEmail());
            }
        }
        resources.setPassword(passwordEncoder.encode(resources.getPassword()));
        boolean result = this.save(resources);
        UsersRoles usersRoles = new UsersRoles();
        usersRoles.setUserId(resources.getId());
        Set<Role> set = resources.getRoles();
        for (Role roleIds : set) {
            usersRoles.setRoleId(roleIds.getId());
        }

        /**
         * 增加标识
         */
        Set<Long> tagList = resources.getTagList();
        if (CollUtil.isNotEmpty(tagList)) {
            tagService.bindUserAndTag(resources.getId(), tagList);
        }

        List<UsersDepts> usersDeptsList = new ArrayList<>();
        if (!resources.getDepts().isEmpty()) {
            Set<String> depts = resources.getDepts();
            for (String dept : depts) {
                UsersDepts usersDepts = new UsersDepts();
                usersDepts.setUserId(resources.getId());
                usersDepts.setDeptId(dept);
                usersDeptsList.add(usersDepts);
            }
        }

        if (result) {
            usersRolesService.save(usersRoles);
            if (!usersDeptsList.isEmpty()) {             //非空，则批量保存多选的科室
                usersDeptsService.saveBatch(usersDeptsList);
            }
        }
        return result;
    }

    /**
     * 编辑用户
     *
     * @param resources /
     */
    @Override
    @CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(User resources) {
        User user = this.getOne(new LambdaQueryWrapper<User>()
                .eq(User::getId, resources.getId()));
        ValidationUtil.isNull(user.getId(), "User", "id", resources.getId());
        User user1 = this.getOne(new LambdaQueryWrapper<User>()
                .eq(User::getUsername, resources.getUsername()));
        User user2 = this.getOne(new LambdaQueryWrapper<User>()
                .eq(User::getEmail, resources.getEmail()));

        if (user1 != null && !user.getId().equals(user1.getId())) {
            throw new BadRequestException("当前用户名已存在");
        }

        if (user2 != null && !user.getId().equals(user2.getId())) {
            throw new EntityExistException(User.class, "email", resources.getEmail());
        }
        user.setUsername(resources.getUsername());
        user.setEmail(resources.getEmail());
        user.setIsEnable(resources.getIsEnable());
        user.setType(resources.getType());
        user.setPhone(resources.getPhone());
        user.setNickName(resources.getNickName());
        user.setFsort(resources.getFsort());
        user.setSign(resources.getSign());

        if (resources.getNewPassword() != null && !resources.getNewPassword().trim().equals("")) {
            user.setPassword(passwordEncoder.encode(resources.getNewPassword()));
        }
        String tenantId = resources.getTenantId();
        if (StrUtil.isNotBlank(tenantId)) {
            user.setTenantId(tenantId);
        }
        user.setSex(resources.getSex());
        //修改权限
        if (ObjectUtil.isNull(resources.getRoles()) || resources.getRoles().isEmpty()) {
            throw new BadRequestException("角色不能为空");
        }

        boolean result = this.saveOrUpdate(user);

        /**
         * 标识
         */
        Set<Long> tagList = resources.getTagList();
        if (CollUtil.isNotEmpty(tagList)) {
            tagService.bindUserAndTag(user.getId(), tagList);
        }

        UsersRoles usersRoles = new UsersRoles();
        usersRoles.setUserId(resources.getId());
        Set<Role> set = resources.getRoles();
        for (Role roleIds : set) {
            usersRoles.setRoleId(roleIds.getId());
        }

        List<UsersDepts> usersDeptsList = new ArrayList<>();
        if (!resources.getDepts().isEmpty()) {
            Set<String> depts = resources.getDepts();
            for (String dept : depts) {
                UsersDepts usersDepts = new UsersDepts();
                usersDepts.setUserId(resources.getId());
                usersDepts.setDeptId(dept);
                usersDeptsList.add(usersDepts);
            }
        }

        usersRolesService.lambdaUpdate().eq(UsersRoles::getUserId, resources.getId()).remove();
        usersRolesService.save(usersRoles);

        if (!usersDeptsList.isEmpty()) {
            usersDeptsService.lambdaUpdate().eq(UsersDepts::getUserId, resources.getId()).remove();
            usersDeptsService.saveBatch(usersDeptsList);
        }


        // 如果用户的角色改变了，需要手动清理下缓存
        if (!resources.getRoles().equals(user.getRoles())) {
            String key = "role::loadPermissionByUser:" + user.getUsername();
            redisUtils.del(key);
            key = "role::findByUsers_Id:" + user.getId();
            redisUtils.del(key);
        }
    }

    @Override
    @CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void delete(Set<Long> ids) {
        for (Long id : ids) {
            usersRolesService.lambdaUpdate().eq(UsersRoles::getUserId, id).remove();
            usersDeptsService.lambdaUpdate().eq(UsersDepts::getUserId, id).remove();
        }
        this.removeByIds(ids);
    }

    @Override
    @Cacheable
    public List<User> getDockerList() {
        DataBind dataBind = SpringUtil.getBean(DataBind.class);
        BidiMap userTypeBidiMap = new DualHashBidiMap(dataBind.ENUM_MAP.get("userType")).inverseBidiMap();
        return this.lambdaQuery().eq(User::getType, userTypeBidiMap.get("医生")).list();
    }

    @Override
    @Cacheable
    public Boolean checkPermission(String userId, String permissionName) {
        if (userMapper.checkPermission(SecurityUtils.getUserId(), permissionName) > 0) {
            return true;
        }
        return false;
    }

    @Override
    @Cacheable
    public List<User> getPermissionUsers(String permissionName) {
        return userMapper.getPermissionUsers(permissionName);
    }

    @Override
    @Cacheable
    public List<User> getUsersByTagPermission(String tagPermission) {
        return baseMapper.getUsersByTagPermission(tagPermission);
    }
}
