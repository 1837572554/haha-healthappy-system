package com.healthappy.modules.system.service.dto;

import com.healthappy.modules.system.domain.BPackageHD;
import com.healthappy.modules.system.domain.BPoison;
import lombok.Data;

import java.util.List;

@Data
public class RegisterPoisonDto  extends BPoison {

    private List<BPackageHD> packageHDList;

}
