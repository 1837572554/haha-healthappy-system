package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TCompanyRepHd;
import com.healthappy.modules.system.service.dto.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 单位报告编制操作类
 * @author Jevany
 * @date 2022/2/17 15:56
 */
@Mapper
@Repository
public interface TCompanyRepHdMapper extends CoreMapper<TCompanyRepHd> {

    /**
     * 获得单位报告编制列表
     * @author YJ
     * @date 2022/2/17 15:58
     * @param criteria
     * @return java.util.List〈com.healthappy.modules.system.service.dto.CompanyReportDTO〉
     */
    List<CompanyReportDTO> getList(@Param("criteria") CompanyReportQueryCriteria criteria);

    /**
     * 获得单位报告编制列表 总数
     * @author YJ
     * @date 2022/2/17 17:29
     * @param criteria
     * @return java.lang.Integer
     */
    Integer getListCount(@Param("criteria") CompanyReportQueryCriteria criteria);

    /**
     * 根据单位报告编制Id,获取对应的病种名单数据
     * @author YJ
     * @date 2022/2/22 14:03
     * @param repId 单位报告编制Id
     * @param tenantId 租户Id
     * @return java.lang.Integer
     */
    @SqlParser(filter = true)
    List<CompanyRepSicknessDetailDTO> getSicknessDetail(@Param("repId") String repId, @Param("tenantId") String tenantId);

    /**
     * 根据单位报告编制Id,获取对应的病种数量
     * @author YJ
     * @date 2022/2/22 14:03
     * @param repId 单位报告编制Id
     * @param tenantId 租户Id
     * @return java.lang.Integer
     */
    @SqlParser(filter = true)
    List<CompanyRepSicknessListDTO> getSicknessTotalDate(@Param("repId") String repId,@Param("tenantId") String tenantId);

    /**
     * 根据单位报告编制Id,获取病种人员名单列表数量
     * @author YJ
     * @date 2022/2/23 16:43
     * @param criteria
     * @return java.lang.Integer
     */
    @SqlParser(filter = true)
    Integer getSicknessPersonListCount(@Param("criteria") CompanyReportSicknessPersonListQueryCriteria criteria);

    /**
     * 根据单位报告编制Id,获取病种人员名单列表
     * @author YJ
     * @date 2022/2/23 16:43
     * @param criteria
     * @return java.util.List〈com.healthappy.modules.system.service.dto.CompanyReportSicknessPersonListDTO〉
     */
    @SqlParser(filter = true)
    List<CompanyReportSicknessPersonListDTO> getSicknessPersonList(@Param("criteria") CompanyReportSicknessPersonListQueryCriteria criteria);
}
