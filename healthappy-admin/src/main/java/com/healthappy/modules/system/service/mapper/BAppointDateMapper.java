package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BAppointDate;
import com.healthappy.modules.system.domain.vo.AppointDateNumVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 * @author Jevany
 * @date 2022/5/7 0:05
 */
@Mapper
@Repository
public interface BAppointDateMapper extends CoreMapper<BAppointDate> {
	/**
	 * 根据预约日期编号获取对应的数据
	 * @author YJ
	 * @date 2022/5/7 1:11
	 * @param dateId 预约日期编号
	 * @param dateStr 日期字符串
	 * @return java.util.List〈com.healthappy.modules.system.domain.BAppointDateNum〉
	 */
	List<AppointDateNumVO> getListByDateIdAndDateStr(@Param("dateId") String dateId, @Param("dateStr") String dateStr);

	/**
	 * 获取日期的预约数 总的数
	 * @author YJ
	 * @date 2022/5/7 2:37
	 * @param dateStr
	 * @return java.lang.Integer
	 */
	Integer getAppointDateNum(@Param("dateStr") String dateStr);

	/**
	 * 预约号源数据清除
	 * @author YJ
	 * @date 2022/5/9 11:21
	 */
	void appointDateClean(@Param("appointDateId") String appointDateId);

	/**
	 * 获取今天指定单位的设置（限制）数
	 * @param companyId 单位编号
	 * @return 如果设置了会返回数值，否则为null
	 */
	Integer getCompanySetNumAppointToday(@Param("companyId") String companyId);

	/**
	 * 获取日期已经预约，登记的数-指定单位的
	 * @author YJ
	 * @date 2022/5/7 2:37
	 * @param dateStr
	 * @return java.lang.Integer
	 */
	Integer getTodayAppointNumByCompany(@Param("dateStr") String dateStr, @Param("companyId") String companyId);
}
