package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 申请类型
 * @author FGQ
 */
@Data
@TableName("B_Type_Apply")
@ApiModel("症状表")
public class BTypeApply implements Serializable {

    private Long id;

    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("排序")
    private Integer dispOrder;
}
