package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BMaritalStatus;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created with IntelliJ IDEA. Created by yutang 2021/9/2 0002  10:59 Description:
 */
@Mapper
public interface BMaritalStatusMapper extends CoreMapper<BMaritalStatus> {

}
