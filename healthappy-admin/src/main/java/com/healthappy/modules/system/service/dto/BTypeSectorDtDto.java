package com.healthappy.modules.system.service.dto;

import com.healthappy.modules.system.domain.BTypeSectorDt;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author FGQ
 * @date 2020-05-14
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BTypeSectorDtDto extends BTypeSectorDt {

}
