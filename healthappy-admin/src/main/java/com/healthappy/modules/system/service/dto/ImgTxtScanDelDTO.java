package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author Jevany
 * @date 2022/2/16 16:05
 */
@Data
public class ImgTxtScanDelDTO implements Serializable {
    /** 流水号 */
    @NotBlank(message = "流水号不能为空")
    @ApiModelProperty("流水号")
    private String paId;

    /**
     * 图文扫描类型（scanType）编号
     */
    @NotBlank(message = "图文扫描类型编号不能为空")
    @ApiModelProperty(value = "图文扫描类型（scanType）编号")
    private String imgTxtTypeId;

    /** 组合项编号,如imgTxtTypeId=4则必填 */
    @ApiModelProperty("组合项编号,如imgTxtTypeId=4则必填")
    private String groupId;


    /** 文件编号 */
    @ApiModelProperty("文件编号")
    private String fileId;
}
