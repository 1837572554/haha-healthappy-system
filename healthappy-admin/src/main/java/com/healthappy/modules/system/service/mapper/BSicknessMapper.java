package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BSickness;
import com.healthappy.modules.system.service.dto.BSicknessDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 健康疾病建议库
 * @author fang
 * @date 2021-06-17
 */
@Mapper
@Repository
public interface BSicknessMapper extends CoreMapper<BSickness> {

    List<BSicknessDto> all(@Param("name") String name,@Param("jp") String jp,@Param("tenantId") String tenantId);
}
