package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BGroupHDWuJia;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;


/**
 * @description 组合项目物价
 * @author sjc
 * @date 2021-08-3
 */
@Mapper
@Repository
public interface BGroupHDWuJiaMapper extends CoreMapper<BGroupHDWuJia> {
}
