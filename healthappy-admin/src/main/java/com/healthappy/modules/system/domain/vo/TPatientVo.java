package com.healthappy.modules.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.pattern.PatternIdCard;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author sjc
 * @date 2021-06-28
 */
@Data
public class TPatientVo  {

    /**
     *体检号
     */
    @ApiModelProperty("体检号")
    private String id;


    /**
     * 姓名
     */
    @NotBlank(message = "姓名不能为空")
    @ApiModelProperty("姓名")
    private String name;

    /**
     * 体检分类表id
     */
    @ApiModelProperty("体检分类表id")
    private Long peTypeHdId;

    /**
     * 体检类别表id
     */
    @ApiModelProperty("体检类别表id")
    private Long peTypeDtId;
    /**
     * 单位编号
     */
    @ApiModelProperty("单位编号")
    private String companyId;


    /**
     * 生日
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @ApiModelProperty("生日")
    private Timestamp birthday;
    /**
     * 性别表id
     */
    @ApiModelProperty("性别表id")
    private String sex;

    /**
     * 年龄
     */
    @ApiModelProperty("年龄")
    private Integer age;

    /**
     * 年龄
     */
    @ApiModelProperty("儿童年龄")
    private String ageDate;

    /**
     * 身份证
     */
    @NotBlank(message = "身份证不能为空")
    @ApiModelProperty("身份证")
    @PatternIdCard
    private String idNo;

    /**
     * 社保号
     */
    @ApiModelProperty("社保号")
    private String ssNo;

    /**
     * 电话
     */
    @ApiModelProperty("电话")
    private String phone;
    /**
     * 地址
     */
    @ApiModelProperty("地址")
    private String address;

    /**
     * 工种ID
     */
    @ApiModelProperty("工种ID")
    private String job;

    /**
     * 工种名称
     */
    @ApiModelProperty("工种名称")
    private String jobName;

    /**
     * 婚姻情况
     */
    @ApiModelProperty("婚姻情况")
    private String marital;
    /**
     * 部门/车间
     */
    @ApiModelProperty("部门/车间")
    private String workshop;

    /**
     * 工号
     */
    @ApiModelProperty("工号")
    private String code;

    /**
     * 总工龄
     */
    @ApiModelProperty("总工龄")
    private String totalYears;

    /**
     * 缴费方式  0自费、1统收
     */
    @ApiModelProperty("缴费方式 0自费、1统收")
    private Integer chargeType;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String mark;

    /**
     * 工作站
     */
    @ApiModelProperty("工作站")
    private String workstation;

    /**
     * 评价
     */
    @ApiModelProperty("评价")
    private String comment;

    /**
     * 建议
     */
    @ApiModelProperty("建议")
    private String suggest;

    /**
     * 主检医生
     */
    @ApiModelProperty("主检医生")
    private String conclusionDoc;

    /**
     * 打印医生
     */
    @ApiModelProperty("打印医生")
    private String printDoctor;

    /**
     * 审核医生
     */
    @ApiModelProperty("审核医生")
    private String verifyDoc;

    /**
     * 报告领取医生
     */
    @ApiModelProperty("报告领取医生")
    private String reportDoctor;

    /**
     * 套餐名称
     */
    @ApiModelProperty("套餐名称")
    private String packageName;
    /**
     * 套餐价格
     */
    @ApiModelProperty("套餐价格")
    private Double packagePrice;
    /**
     * 部门id
     */
    @ApiModelProperty("部门id")
    private String departmentId;

    /**
     * 统收限额
     */
    @ApiModelProperty("统收限额")
    private Double tsxe;

    /**
     * 分组id
     */
    @ApiModelProperty("分组id")
    private String departmentGroupId;



    /**
     * 合计价格
     */
    @ApiModelProperty("合计价格")
    private Double totalPrice;

    /**
     * 统收价格
     */
    @ApiModelProperty("统收价格")
    private Double tsxePric;
    /**
     * 人员来源
     */
    @ApiModelProperty("人员来源")
    private String personnelSource;
    /**
     * 任务来源
     */
    @ApiModelProperty("任务来源")
    private String charsi;
    /**
     * 结算归口
     */
    @ApiModelProperty("结算归口")
    private Integer settlement;
    /**
     * 职位
     */
    @ApiModelProperty("职位")
    private String duty;
    /**
     * 打印次数
     */
    @ApiModelProperty("打印次数")
    private Integer printNumer;
    /**
     * 打印份数-一次打印可能会打印多次
     */
    @ApiModelProperty("打印份数-一次打印可能会打印多次")
    private Integer printPart;
    /**
     * 文化程度
     */
    @ApiModelProperty("文化程度")
    private String culture;
    /**
     * 照射种类
     */
    @ApiModelProperty("照射种类")
    private String radiationType;
    /**
     * 邮编
     */
    @ApiModelProperty("邮编")
    private String postcode;
    /**
     * 出生地
     */
    @ApiModelProperty("出生地")
    private String birthplace;
    /**
     * 职普合一，职业结论
     */
    @ApiModelProperty("职普合一，职业结论")
    private String commentz;
    /**
     * 职普合一，职业建议
     */
    @ApiModelProperty("职普合一，职业建议")
    private String suggestz;
    /**
     * 民族代码
     */
    @ApiModelProperty("民族代码")
    private String nation;
    /**
     * 证件类型
     */
    @ApiModelProperty("证件类型")
    private String idType;

    /** 附加项目总价格 */
    @ApiModelProperty(value = "附加项目总价格")
    private Double additionPrice;

    //region 下面的字段是t_patient_z表中的字段*/
    @ApiModelProperty("接害工龄")
    private String workYears;

    @ApiModelProperty("毒害")
    private String poisonType;
    //endregion

    /** 毒害编号列表 */
    @ApiModelProperty("毒害编号列表")
    private List<String> poisonIdList;

    @ApiModelProperty("初检号")
    private Long historyNo;

    /**
     * 体检日期
     */
    @ApiModelProperty("体检日期")
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp peDate;


    @ApiModelProperty("身份证图片")
    private String idImage;

    @ApiModelProperty("人像")
    private String portrait;

}
