package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("单位缴费人员名单")
public class PayApplyPatientServiceCriteria {

    @ApiModelProperty("收费单号")
    private String payApplyId;

    @ApiModelProperty("体检号")
    private String paId;

    @ApiModelProperty("操作码：0：结账，1：撤销结账，2：锁定，3：解锁，4：结账+锁定")
    private Integer operate;
}
