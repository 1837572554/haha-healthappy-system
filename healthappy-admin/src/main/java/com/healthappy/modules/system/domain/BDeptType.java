package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 科室类型表
 * @author fang
 * @date 2021-06-22
 */
@Data
@TableName("B_Dept_Type")
@ApiModel("科室类型表")
public class BDeptType implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
     * id
     */
    @ApiModelProperty("id")
    private Long id;

    /**
     * 科室类别名
     */
    @ApiModelProperty("科室类别名")
    private String name;

    /**
     * 是否启用
     */
    @ApiModelProperty("是否启用")
    private Boolean isEnable;

    /**
     * 简拼
     */
    @ApiModelProperty("简拼")
    private String jp;
}
