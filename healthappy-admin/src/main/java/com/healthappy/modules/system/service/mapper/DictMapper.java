package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.Dict;
import com.healthappy.modules.system.domain.vo.DictEnumVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author hupeng
 * @date 2020-05-14
 */
@Repository
@Mapper
public interface DictMapper extends CoreMapper<Dict> {

    /**
     * 获取枚举需要的数据
     * @return
     */
	@SqlParser(filter = true)
    @Select("SELECT DISTINCT d.`name`,IFNULL(dd.label,'无') as label,IFNULL(dd.`value`,'无') as `value`,IFNULL(dd.sort,999) as sort FROM dict d LEFT JOIN dict_detail dd ON d.id = dd.dict_id ORDER BY IFNULL(dd.sort,999)  ASC")
    List<DictEnumVo> dictConvertEnum();

	List<DictEnumVo> reLoad();
}
