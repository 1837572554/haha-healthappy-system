package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TDctData;
import com.healthappy.modules.system.domain.vo.TDctDataVo;

/**
 * @author Jevany
 * @date 2021/12/2 15:43
 * @desc 电测听数据服务
 */
public interface TDctDataService extends BaseService<TDctData> {

    /**
     * 获得电测听数据
     * @author YJ
     * @date 2021/12/2 15:47
     * @param paId 体检号
     * @return com.healthappy.modules.system.service.dto.TDctDataDto
     */
    TDctDataVo getDctData(String paId);

    /**
     * 保存电测听数据
     * @author YJ
     * @date 2021/12/2 15:47
     * @param dto com.healthappy.modules.system.service.dto.TDctDataDto数据
     */
    void saveDctData(TDctDataVo vo);
}
