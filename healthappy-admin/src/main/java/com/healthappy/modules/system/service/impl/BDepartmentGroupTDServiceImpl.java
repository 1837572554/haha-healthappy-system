package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BDepartmentGroupTD;
import com.healthappy.modules.system.domain.BGroupHd;
import com.healthappy.modules.system.domain.vo.BDepartmentGroupTDSaveVo;
import com.healthappy.modules.system.domain.vo.BDepartmentGroupTDVo;
import com.healthappy.modules.system.service.BDepartmentGroupTDService;
import com.healthappy.modules.system.service.BGroupHdService;
import com.healthappy.modules.system.service.dto.BDepartmentGroupTDDto;
import com.healthappy.modules.system.service.dto.BDepartmentGroupTDQueryCriteria;
import com.healthappy.modules.system.service.mapper.BDepartmentGroupTDMapper;
import com.healthappy.utils.SecurityUtils;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author sjc
 * @date 2021-06-24
 */
@Service
@AllArgsConstructor//有自动注入的功能
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BDepartmentGroupTDServiceImpl extends BaseServiceImpl<BDepartmentGroupTDMapper, BDepartmentGroupTD>
		implements BDepartmentGroupTDService {

	private final IGenerator generator;

	private final BGroupHdService bGroupHdService;

	/**
	 * 分页查询
	 *
	 * @param criteria
	 * @param pageable
	 * @return
	 */
	@Override
	public Map<String, Object> queryAll(BDepartmentGroupTDQueryCriteria criteria, Pageable pageable) {
		getPage(pageable);
		PageHelper.orderBy("cast(IFNULL(bd.disp_order,999) as signed),CAST(IFNULL(bgh.disp_order,999) as signed)");
		List<BDepartmentGroupTD> bDepartmentGroupTDS = queryAll(criteria);
		PageInfo<BDepartmentGroupTD> page = new PageInfo<BDepartmentGroupTD>(bDepartmentGroupTDS);
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("content", generator.convert(page.getList(), BDepartmentGroupTDDto.class));
		map.put("totalElements", page.getTotal());
		return map;
	}


	/**
	 * 根据多个分组id查询对应的项目信息
	 *
	 * @param criteria
	 * @param pageable
	 * @return
	 */
	@Override
	public Map<String, Object> list(BDepartmentGroupTDQueryCriteria criteria, Pageable pageable) {
		getPage(pageable);
		PageHelper.orderBy("cast(IFNULL(bd.disp_order,999) as signed),CAST(IFNULL(bgh.disp_order,999) as signed)");
		List<BDepartmentGroupTD> bDepartmentGroupTDS = baseMapper.getBDepartmentGroupIn(criteria);
		PageInfo<BDepartmentGroupTD> page = new PageInfo<BDepartmentGroupTD>(bDepartmentGroupTDS);
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("content", generator.convert(page.getList(), BDepartmentGroupTDDto.class));
		map.put("totalElements", page.getTotal());
		return map;
	}

	/**
	 * 查询分组下的项目数据
	 *
	 * @return Object
	 */
	@Override
	public List<BDepartmentGroupTD> queryAll(BDepartmentGroupTDQueryCriteria criteria) {
		return baseMapper.getBDepartmentGroup(criteria);

		//        List<BDepartmentGroupTD> list = this.list(QueryHelpPlus.getPredicate(BDepartmentGroupTD.class, criteria));
		//        return loadingGroupName(list);
	}


	private List<BDepartmentGroupTD> loadingGroupName(List<BDepartmentGroupTD> list) {
		if (CollUtil.isEmpty(list)) {
			return Collections.emptyList();
		}
		List<String> permissions = list.stream().map(BDepartmentGroupTD::getGroupId).collect(Collectors.toList());

		LambdaUpdateWrapper<BGroupHd> updateLam = new LambdaUpdateWrapper();
		updateLam.in(BGroupHd::getId, permissions);
		List<BGroupHd> hdList = bGroupHdService.list(updateLam);


		for (BGroupHd bGroupHd : hdList) {
			for (BDepartmentGroupTD td : list) {
				if (td.getGroupId().equals(bGroupHd.getId())) {
					td.setGroupName(bGroupHd.getName());
					td.setSex(bGroupHd.getSex());
				}
			}
		}

		return list;
	}


	/**
	 * 根据分组id删除所有的分组项目
	 *
	 * @param id
	 * @return
	 */
	@Override
	public boolean deleteBDepartmentGroupTD(String id) {
		return baseMapper.deleteBDepartmentGroupTD(id);
	}

	/**
	 * 保存分组项目
	 *
	 * @param list
	 * @return
	 */
	@Override
	public boolean insertSelective(List<BDepartmentGroupTD> list) {
		return baseMapper.insertSelective(list);
	}

	@Override
	@Transactional
	public boolean addOrUpdate(BDepartmentGroupTDSaveVo saveVo) {
		baseMapper.deleteBDepartmentGroupTD(saveVo.getDepartmentGroupHdId());
		if (saveVo.getResources() != null && saveVo.getResources().size() > 0) {
			String tenantId = SecurityUtils.getTenantId();
			List<BDepartmentGroupTDVo> resources = saveVo.getResources();
			resources.forEach(r -> {
				r.setDepartmentGroupHdId(saveVo.getDepartmentGroupHdId());
				r.setTenantId(tenantId);
			});
			List<BDepartmentGroupTD> groupList = generator.convert(resources, BDepartmentGroupTD.class);

			groupList = groupList.stream().collect(Collectors.collectingAndThen(
					Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(BDepartmentGroupTD::getGroupId))),
					ArrayList::new));
			
			return baseMapper.insertSelective(groupList);
		}
		return true;
	}

	@Override
	public List<BDepartmentGroupTD> getBDepartmentGroup(BDepartmentGroupTDQueryCriteria criteria) {
		return baseMapper.getBDepartmentGroup(criteria);
	}
}
