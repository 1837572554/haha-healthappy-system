package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @description 个人缴费查询
 * @author FGQ
 * @date 2022-2-18
 */
@Data
@ApiModel("个人缴费查询")
public class PayCostAloneQueryCriteria {

    /** 流水号/档案号 模糊查询 */
    @ApiModelProperty("流水号/档案号")
    private String blurry;

    /** 1:登记时间,2:签到时间,2:总检时间 */
//    @NotNull(message = "时间类型不能为空")
    @ApiModelProperty("1:登记时间,2:签到时间,2:总检时间")
    private Integer timeType;

    /** 时间 */
//    @NotNull(message = "时间范围不能为空")
    @ApiModelProperty("时间")
    private List<String> time;

    /** 缴费状态,1:已缴费,0:未缴费 */
    @ApiModelProperty("缴费状态,1:已缴费,0:未缴费")
    private Integer payStatus;
}
