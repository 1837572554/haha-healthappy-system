package com.healthappy.modules.system.service.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author FGQ
 * @date 2020-05-14
 */
@Data
public class BCommentWzDto implements Serializable {

    /**
     * id
     */
    private String id;

    /**
     * 结论
     */
    private String comment;

    /**
     * 排序号
     */
    private String dispOrder;

    /**
     * 问诊模板类型  0既往病史 1现病史 2家族史 3手术史
     */
    private String type;

    private String typeText;

    /** 编号 */
    private Integer code;
}
