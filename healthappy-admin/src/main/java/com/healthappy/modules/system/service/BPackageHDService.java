package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BPackageHD;
import com.healthappy.modules.system.domain.vo.BPackageHDVo;
import com.healthappy.modules.system.service.dto.BPackageHDQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 单位表 服务层
 * @author sjc
 * @date 2021-06-24
 */
public interface BPackageHDService extends BaseService<BPackageHD> {


    Map<String, Object> queryAll(BPackageHDQueryCriteria criteria, Pageable pageable);

    List<BPackageHD> queryAll(BPackageHDQueryCriteria criteria);

    void saveOrUpdatePackage(BPackageHDVo resources);
}
