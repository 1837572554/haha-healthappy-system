package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BDepartmentQueryCriteria {
    /**
     * 单位id
     */
    @ApiModelProperty("单位id")
    @Query(type = Query.Type.EQUAL)
    private String comId;

    /**
     * 部门id
     */
    @ApiModelProperty("部门id")
    @Query(type = Query.Type.EQUAL)
    private String id;


    /**
     * 部门名称
     */
    @ApiModelProperty("部门名称")
    @Query(type = Query.Type.INNER_LIKE)
    private String name;


}
