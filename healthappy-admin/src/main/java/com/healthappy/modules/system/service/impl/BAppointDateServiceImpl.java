package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.BAppointDate;
import com.healthappy.modules.system.domain.BAppointDateNum;
import com.healthappy.modules.system.domain.vo.AppointDateNumVO;
import com.healthappy.modules.system.domain.vo.AppointDateVO;
import com.healthappy.modules.system.service.BAppointDateNumService;
import com.healthappy.modules.system.service.BAppointDateService;
import com.healthappy.modules.system.service.TenantService;
import com.healthappy.modules.system.service.dto.AppointDateDTO;
import com.healthappy.modules.system.service.dto.AppointDateOneDTO;
import com.healthappy.modules.system.service.dto.BAppointDateNumDTO;
import com.healthappy.modules.system.service.mapper.BAppointDateMapper;
import com.healthappy.utils.CacheConstant;
import com.healthappy.utils.DateUtils;
import com.healthappy.utils.StringUtils;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * @author Jevany
 * @date 2022/5/7 0:04
 */
@Service
@AllArgsConstructor
@CacheConfig(cacheNames = {CacheConstant.APPOINT_DATE_NUM})
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BAppointDateServiceImpl extends BaseServiceImpl<BAppointDateMapper, BAppointDate>
		implements BAppointDateService {

	private final IGenerator generator;
	private final BAppointDateNumService appointDateNumService;

	private final TenantService tenantService;

	@Override
	@Cacheable(value = CacheConstant.APPOINT_DATE_NUM, key = "#year +'_'+#month")
	public List<AppointDateVO> getMonthAppointDateList(Integer year, Integer month) {
		//根据年月，获取第一天和最后一天
		String[] monthFirstLast = DateUtils.getMonthFirstLast(year, month);
		//查询指定月份的数据
		List<BAppointDate> list = this.lambdaQuery()
				.between(BAppointDate::getDate, monthFirstLast[0], monthFirstLast[1]).orderByAsc(BAppointDate::getDate)
				.list();
		//转换类型
		List<AppointDateVO> AppointDateVOList = generator.convert(list, AppointDateVO.class);
		for (AppointDateVO appointDateVO : AppointDateVOList) {
			//获取子级数据
			appointDateVO.setAppointDateNumVOList(this.baseMapper.getListByDateIdAndDateStr(appointDateVO.getId(),
					DateUtils.dateTime(appointDateVO.getDate())));
			//获取预约、登记数据量
			appointDateVO.setAppointNum(this.baseMapper.getAppointDateNum(DateUtils.dateTime(appointDateVO.getDate())));
		}

		return AppointDateVOList;
	}

	@Override
	@CacheEvict(allEntries = true)
	public void batchAddition(AppointDateDTO appointDateDTO) {
		//获取时间段之间的日期
		List<String> betweenDate = new ArrayList<>();
		//检测时间是否已经设置
		List<BAppointDate> list = new ArrayList<>();

		if (appointDateDTO.getDateStart().equals(appointDateDTO.getDateEnd())) {
			betweenDate.add(appointDateDTO.getDateStart());
			list = this.lambdaQuery().apply("date_format(date,'%Y-%m-%d')='" + appointDateDTO.getDateStart() + "'")
					.list();
		} else {
			betweenDate = DateUtils.getBetweenDate(appointDateDTO.getDateStart(), appointDateDTO.getDateEnd());
			list = this.lambdaQuery()
					.between(BAppointDate::getDate, betweenDate.get(0), betweenDate.get(betweenDate.size() - 1)).list();
		}

		//如果有数据，说明时间段之间有设置过了
		StringBuilder sb = new StringBuilder();
		if (Optional.ofNullable(list).isPresent() && list.size() > 0) {
			if (betweenDate.size() == 1) {
				sb.append("已设置:");
			} else {
				sb.append(String.format("时间段：%s-%s中有日期已经设置：", betweenDate.get(0),
						betweenDate.get(betweenDate.size() - 1)));
			}
			list.forEach(e -> sb.append(DateUtils.dateTime(e.getDate()) + "、"));
			String err = StringUtils.trimFirstAndLastChar(sb.toString(), "、");
			throw new BadRequestException(err);
		}
		list.clear();
		//判断是否有0，全部
		long allCount = appointDateDTO.getAppointDateNumDTOList().stream().filter(dn -> dn.getCompanyId().equals("0"))
				.collect(Collectors.toList()).size();
		if (allCount > 0 && appointDateDTO.getAppointDateNumDTOList().size() > 1) {
			throw new BadRequestException("已有全部，无法再选其他公司");
		}

		//检测是否有重复单位
		List<String> collect = appointDateDTO.getAppointDateNumDTOList().stream().map(BAppointDateNumDTO::getCompanyId)
				.distinct().collect(Collectors.toList());

		if (appointDateDTO.getAppointDateNumDTOList().size() != collect.size()) {
			throw new BadRequestException("单位不能重复添加");
		}
		//计算总数
		Integer totalNum = 0;
		for (BAppointDateNumDTO bAppointDateNumDTO : appointDateDTO.getAppointDateNumDTOList()) {
			totalNum += bAppointDateNumDTO.getNum();
		}

		List<BAppointDateNum> AppointDateNumList = generator.convert(appointDateDTO.getAppointDateNumDTOList(),
				BAppointDateNum.class);

		//进行数据转换 companyId=0为全部
		for (String date : betweenDate) {
			BAppointDate appointDate = new BAppointDate();
			appointDate.setDate(DateUtils.dateTime(DateUtils.YYYY_MM_DD, date));
			appointDate.setTotalNum(totalNum);
			boolean saveSuccess = this.save(appointDate);
			if (!saveSuccess) {
				//记录插入失败的日期
				sb.append(date + "、");
				//跳过后面的操作
				continue;
			}
			AppointDateNumList.forEach(dn -> dn.setAppointDateId(appointDate.getId()));
			appointDateNumService.saveBatch(AppointDateNumList);
		}

		if (sb.length() > 0) {
			throw new BadRequestException("其中：" + StringUtils.trimFirstAndLastChar(sb.toString(), "、") + "保存失败");
		}
	}

	@Override
	@CacheEvict(allEntries = true)
	public void batchClean(String dateStart, String dateEnd) {
		//获取时间段之间的日期
		List<String> betweenDate = new ArrayList<>();
		//检测时间是否已经设置
		List<BAppointDate> list = new ArrayList<>();

		if (dateStart.equals(dateEnd)) {
			betweenDate.add(dateStart);
			list = this.lambdaQuery().apply("date_format(date,'%Y-%m-%d')='" + dateStart + "'").list();
		} else {
			betweenDate = DateUtils.getBetweenDate(dateStart, dateEnd);
			list = this.lambdaQuery()
					.between(BAppointDate::getDate, betweenDate.get(0), betweenDate.get(betweenDate.size() - 1)).list();
		}
		if (CollUtil.isEmpty(list)) {
			return;
		}
		StringBuilder sb = new StringBuilder();
		for (BAppointDate appointDate : list) {
			//循环判断是否有预约过了,登记过,
			String date = DateUtils.dateTime(appointDate.getDate());
			if (this.baseMapper.getAppointDateNum(date) > 0) {
				sb.append(date + "、");
			} else {
				this.baseMapper.appointDateClean(appointDate.getId());
			}
		}

		if (StringUtils.isNotBlank(sb.toString())) {
			throw new BadRequestException(
					"执行完成，以下日期已经有预约或登记，未清除：" + StringUtils.trimFirstAndLastChar(sb.toString(), "、"));
		}
	}

	@Override
	@CacheEvict(allEntries = true)
	public void addOrUpdate(AppointDateOneDTO appointDateOneDTO) {
		//新增还是修改
		if (StringUtils.isBlank(appointDateOneDTO.getId())) {
			//新增,直接走批量添加的接口
			AppointDateDTO appointDateDTO = new AppointDateDTO();
			appointDateDTO.setDateStart(appointDateOneDTO.getDate());
			appointDateDTO.setDateEnd(appointDateOneDTO.getDate());
			appointDateDTO.setAppointDateNumDTOList(appointDateOneDTO.getAppointDateNumDTOList());
			this.batchAddition(appointDateDTO);
		} else {
			//修改
			//判断是否有0，全部
			long allCount = appointDateOneDTO.getAppointDateNumDTOList().stream()
					.filter(dn -> dn.getCompanyId().equals("0")).collect(Collectors.toList()).size();
			if (allCount > 0 && appointDateOneDTO.getAppointDateNumDTOList().size() > 1) {
				throw new BadRequestException("已有全部，无法再选其他公司");
			}

			//检测是否有重复单位
			List<String> collect = appointDateOneDTO.getAppointDateNumDTOList().stream()
					.map(BAppointDateNumDTO::getCompanyId).distinct().collect(Collectors.toList());

			if (appointDateOneDTO.getAppointDateNumDTOList().size() != collect.size()) {
				throw new BadRequestException("单位不能重复添加");
			}

			//先获取原来的数据
			List<AppointDateNumVO> appointDateNumVOList = this.baseMapper.getListByDateIdAndDateStr(
					appointDateOneDTO.getId(), appointDateOneDTO.getDate());

			//当前提交的公司数据
			List<String> companyIds = appointDateOneDTO.getAppointDateNumDTOList().stream()
					.map(BAppointDateNumDTO::getCompanyId).collect(Collectors.toList());

			//过滤出来已经预约或登记的数据
			List<AppointDateNumVO> hasDateList = appointDateNumVOList.stream().filter(adn -> adn.getNum1() > 0)
					.collect(Collectors.toList());
			StringBuilder sb = new StringBuilder();
			for (AppointDateNumVO appointDateNumVO : hasDateList) {
				//这些数据必须存在，如果不存在就报错了。不让进行下一步
				if (!companyIds.contains(appointDateNumVO.getCompanyId())) {
					sb.append(appointDateNumVO.getCompany() + "、");
				}
			}

			if (StringUtils.isNotBlank(sb.toString())) {
				throw new BadRequestException("已经预约或登记的公司无法删除：" + StringUtils.trimFirstAndLastChar(sb.toString(), "、"));
			}

			//计算总数
			Integer totalNum = 0;
			for (BAppointDateNumDTO bAppointDateNumDTO : appointDateOneDTO.getAppointDateNumDTOList()) {
				totalNum += bAppointDateNumDTO.getNum();
			}

			List<BAppointDateNum> AppointDateNumList = generator.convert(appointDateOneDTO.getAppointDateNumDTOList(),
					BAppointDateNum.class);
			//更新总数
			this.update(Wrappers.<BAppointDate>lambdaUpdate().eq(BAppointDate::getId, appointDateOneDTO.getId())
					.set(BAppointDate::getTotalNum, totalNum));
			AppointDateNumList.forEach(dn -> dn.setAppointDateId(appointDateOneDTO.getId()));
			//删除原来的数据
			appointDateNumService.remove(Wrappers.<BAppointDateNum>lambdaUpdate()
					.eq(BAppointDateNum::getAppointDateId, appointDateOneDTO.getId()));
			//新增全部数据
			appointDateNumService.saveBatch(AppointDateNumList);
		}
	}

	@Override
	public Boolean companyCanRegister(String companyId, String appointId) {
		//取当天的预约数据
		String dateStr = DateUtils.getDate();
		BAppointDate appointDate = this.lambdaQuery().apply("date_format(date,'%Y-%m-%d')='" + dateStr + "'").one();
		if (!Optional.ofNullable(appointDate).isPresent() || StringUtils.isBlank(appointDate.getId())) {
			//没有设置预约数据
			throw new BadRequestException("未设置预约数据");
		} else {
			//今天是否又权限登记此单位数据
			Integer setCompanyNum = this.baseMapper.getCompanySetNumAppointToday(companyId);
			if (ObjectUtil.isNull(setCompanyNum)) {
				//今天没有设置此单位的预约数据
				throw new BadRequestException("今日未开放此单位");
			}
			Integer companyNum = this.baseMapper.getTodayAppointNumByCompany(
					DateUtils.dateTimeNow(DateUtils.YYYY_MM_DD), companyId);
			if (ObjectUtil.isNull(companyNum)) {
				throw new BadRequestException("未获取到单位预约、登记数");
			}
			if (companyNum >= setCompanyNum) {
				throw new BadRequestException("已达限制数,无法登记");
			}
		}
		return true;
	}
}