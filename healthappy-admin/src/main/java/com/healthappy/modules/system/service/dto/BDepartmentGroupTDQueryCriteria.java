package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class BDepartmentGroupTDQueryCriteria {

    /**
     * 分组id
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("分组id")
    private String departmentGroupHdId;

    /**
     * 分组id集合，用于批量查询
     */
    @ApiModelProperty("分组id集合")
    private List<String> departmentGroupHdIdList;
}
