package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @description 单位表
 * @author sqoy
 * @date 2022-03-03
 */

@Data
@ApiModel("单位收费结账人员名单表")
@TableName("T_Charge_COM_Apply_Patient")
@RenewLog
public class TChargeComApplyPatient implements Serializable {

    @ApiModelProperty("收费单号")
    @NotBlank(message = "收费单号不可为空")
    private String payApplyId;

    @ApiModelProperty("体检号")
    private String paId;

    @ApiModelProperty("租户号")
    private String tenantId;

    /** 操作码：0：结账，1：撤销结账,2：锁定，3：解锁，4：结账+锁定 */
    @NotNull
    @TableField(exist = false)
    @ApiModelProperty("操作码：0：结账，1：撤销结账,2：锁定，3：解锁，4：结账+锁定")
    private Integer operate;

    @ApiModelProperty("结账标志")
    private Integer payFlag;

    @ApiModelProperty("结账时间")
    @TableField(fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private String payTime;

    @ApiModelProperty("结账人")
    private String payDoctor;
}
