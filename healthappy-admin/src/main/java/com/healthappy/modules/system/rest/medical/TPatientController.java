package com.healthappy.modules.system.rest.medical;


import cn.hutool.core.util.ObjectUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.TPatientBaseinfo;
import com.healthappy.modules.system.service.TPatientBaseinfoService;
import com.healthappy.modules.system.service.TPatientService;
import com.healthappy.modules.system.service.dto.TPatientBaseinfoDto;
import com.healthappy.modules.system.service.dto.TPatientBaseinfoQueryCriteria;
import com.healthappy.modules.system.service.dto.TPatientQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * @author sjc
 * @description 体检人员表 控制器
 * @date 2021-06-28
 */
@Slf4j
@Api(tags = "体检管理：体检人员")
@RestController
@AllArgsConstructor
@RequestMapping("/TPatient")
public class TPatientController {

    private final TPatientService tPatientService;
    private final TPatientBaseinfoService baseinfoService;

    @Log("分页查询体检人员数据")
    @ApiOperation("分页查询体检人员数据，权限码：TPatient:list")
    @GetMapping
    @PreAuthorize("@el.check('TPatient:list')")
    public R listPage(TPatientQueryCriteria criteria, Pageable pageable) {
        return R.ok(tPatientService.queryAll(criteria, pageable));
    }

    @Log("人员档案信息获取")
    @ApiOperation("人员档案信息获取，权限码：MedicalRegistration:list")
    @GetMapping("/getBaseinfo")
    @PreAuthorize("@el.check('MedicalRegistration:list')")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "人员档案信息",response = TPatientBaseinfo.class)
    })
    public R getBaseinfo(TPatientBaseinfoQueryCriteria criteria) {
        TPatientBaseinfo baseinfo = baseinfoService.getBaseinfo(criteria);
        if(ObjectUtil.isNull(baseinfo.getPNo())) {
            return R.ok(null);
        }
        return R.ok(baseinfo);
    }

    @Log("人员档案信息保存、修改")
    @ApiOperation("人员档案信息保存、修改，权限码：MedicalRegistration:add")
    @PostMapping("/saveBaseinfo")
    @PreAuthorize("@el.check('MedicalRegistration:add')")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "人员档案编号",response = Long.class)
    })
    @Klock
    public R saveBaseinfo(@RequestBody TPatientBaseinfoDto dto) {
        return R.ok(baseinfoService.saveOrUpdateBaseinfo(dto));
    }
}
