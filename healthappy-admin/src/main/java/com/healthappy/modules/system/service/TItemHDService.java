package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TItemHD;
import com.healthappy.modules.system.domain.TPatient;
import com.healthappy.modules.system.service.dto.DataQueryCriteria;
import com.healthappy.modules.system.service.dto.DataQueryDto;
import com.healthappy.modules.system.service.dto.TItemHDQueryCriteria;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @author sjc
 * @description 体检人员项目表 服务层
 * @date 2021-06-29
 */
public interface TItemHDService extends BaseService<TItemHD> {
    Map<String, Object> queryAll(TItemHDQueryCriteria criteria, Pageable pageable);

    List<TItemHD> queryAll(TItemHDQueryCriteria criteria);

    List<DataQueryDto> getGroupPanelHd(DataQueryCriteria criteria);

    /**
     * 根据体检号获取对应的组合项目条码，用于条码的生成
     * @param paId
     * @return
     */
    List<TItemHD> getBarcodeGroup(String paId);

    /**
     * 更新指定条码名称对应的项目的条码
     * @param barcode 条码号
     * @param barcodeName 条码名称
     * @param type 组合项目类型
     * @return
     */
    boolean updateBarcode(String paId,String barcode, String barcodeName,String type);

    /**
     * 批量保存or新增
     *
     * @param resources
     * @return
     */
    boolean saveOrUpdate(List<TItemHD> resources);

    /**
     * 根据体检号删除该人员的组合项目
     *
     * @param paId
     * @return
     */
    boolean deleteByPaId(String paId);

    /**
     * 根据体检号和组合项目id进行删除
     *
     * @param paId
     * @param groupId
     * @return
     */
    void deleteByPaIdAndGroupId(String paId, String groupId);


    /**
     * 根据体检号和组合项目id进行弃检
     *
     * @param paId
     * @param groupId
     * @return
     */
    void abandon(String paId, String groupId);

    /**
     * 根据体检号和组合项目id进行撤销结果
     * @param paId
     * @param groupId
     * @return
     */
    void revocation(String paId, String groupId);

    /**
     * 组合项保存 实现
     *
     * @param hd TItemHD对象
     * @return
     */
    void saveItem(TItemHD hd, String sex);

    /**
     * 保存组合项目
     *
     * @param list TItemHD对象列表
     * @return 返回真假
     */
    void saveItems(TPatient tPatient, List<TItemHD> list) throws Exception;


//    /**
//     * 保存组合项目
//     * @param list TItemHD对象列表
//     * @return 返回真假
//     */
//    void saveItems(String paId, List<TItemHD> list) throws Exception;

    /**
     * 删除指定体检号下，指定组合编号外的数据
     *
     * @param paId          体检号
     * @param notInGroupIds 组合编号列表
     * @return
     */
    void delItemsNotInGroupIds(String paId, List<String> notInGroupIds);

    /**
     * 修改TypeJ、TypeZ和deptId
     * @author YJ
     * @date 2021/12/24 16:18
     * @param itemHd
     * @return java.lang.Integer 返回受影响的行数
     */
    Integer updateItemTypeAndDeptId(TItemHD itemHd);


    /**
     * 删除之前清条码
     * @author YJ
     * @date 2021/12/24 17:52
     * @param paId 体检号
     * @param groupIdList 组合编号列表 不能为空，空就没意义
     * @return java.lang.Integer 返回受影响的行数
     */
    Integer delBeforeClearBarcode(String paId, List<String> groupIdList);


    /**
     * 添加新项目，去除同条码名称的Barcode
     * @author YJ
     * @date 2021/12/27 10:02
     * @param paId 体检号
     * @return java.lang.Integer
     */
    Integer addItemClearWithNameBarcode(String paId);

    /**
     * 清除同一个PaId中相同GroupId
     * @param paIds
     */
    void repeatGroupId(List<String> paIds);
}
