package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@TableName("B_PeType_HD")
@ApiModel(value = "体检类型",discriminator = "体检类型：大类")
public class BPeTypeHD implements Serializable {

    private static final long SerialVersionUID = 111839700929517618L;

    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "主键id")
    private Long id;

    @ApiModelProperty("体检类型名称")
    @NotBlank(message = "体检类型名称不能为空")
    private String name;

    @ApiModelProperty("体检类型组ID")
    @NotBlank(message = "体检类型组ID不能为空")
    @TableField("group_id")
    private Integer groupID;

    @TableField("disp_order")
    @ApiModelProperty("排序")
    private Integer dispOrder;

    @TableField("is_enable")
    @ApiModelProperty("是否启用 1启用 0关闭")
    private Integer isEnable;

}
