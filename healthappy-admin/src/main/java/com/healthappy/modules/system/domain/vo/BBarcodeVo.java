package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @description 条码设置表
 * @author sjc
 * @date 2021-12-13
 */
@Data
public class BBarcodeVo {

    @ApiModelProperty(value = "id",position = 1)
    private String id;

    /**
     * 项目类型,来源字典表---项目组合主表类型
     */
    @NotBlank(message = "项目类型不能为空")
    @ApiModelProperty(value = "项目类型",position = 2)
    private String groupHdType;

    /**
     * 条码总长度
     */
    @NotNull(message = "条码总长度不能为空")
    @ApiModelProperty(value = "条码总长度",position = 3)
    private Integer barcodeLength;

    /**
     * 条码头内容
     */
    @NotBlank(message = "条码头内容不能为空")
    @ApiModelProperty(value = "条码头内容",position = 4)
    private String barcodeHead;

    /**
     * 种子，对应的条码在种子基础上+1
     */
    @ApiModelProperty(value = "条码种子",position = 5)
    private String seed;

}
