package com.healthappy.modules.system.service.dto;

import com.healthappy.modules.system.domain.Menu;
import io.swagger.annotations.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

/**
 * 角色菜单DTO
 * @author Jevany
 * @date 2022/3/4 15:52
 */
@Data
public class RoleMenuDTO implements Serializable {
    /** 菜单列表 */
    @ApiModelProperty("菜单列表")
    private Set<Menu> menus;

    /** 下面有未选的节点编号，逗号分隔 */
    @ApiModelProperty("下面有未选的节点编号，逗号分隔")
    private String menuJson;
}