package com.healthappy.modules.system.service;

import com.healthappy.modules.system.domain.TPatientImg;
import com.healthappy.modules.system.domain.vo.TPatientImageVo;
import com.healthappy.modules.system.service.dto.TPatientImgDto;

import java.util.List;

/**
 * @desc: 体检图片绑定服务
 * @author: YJ
 * @date: 2021-12-01 16:26
 **/
public interface TPatientImgService {

    /**
     * 绑定项目图片
     * @author YJ
     * @date 2021/12/1 16:28
     * @param tPatientImgs 体检图片绑定集合
     */
    void bindItemImg(List<TPatientImg> tPatientImgs);

    List<TPatientImageVo> getItemImg(TPatientImgDto tPatientImgDto);
}
