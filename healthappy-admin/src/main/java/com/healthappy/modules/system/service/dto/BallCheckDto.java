package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 团检备单
 */
@Data
public class BallCheckDto implements Serializable {

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private Long id;

    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("性别")
    private Integer sex;

    @ApiModelProperty("性别名称")
    private String sexName;

    @ApiModelProperty("年龄")
    private Integer age;

    @ApiModelProperty("身份证")
    private String idNo;

    /**
     * 婚姻状况
     */
    @ApiModelProperty("婚姻状况")
    private String marry;

    @ApiModelProperty("婚姻状况名称")
    private String marryName;

    /**
     * 单位名称
     */
    @ApiModelProperty("单位名称")
    private String companyName;

    /**
     * 单位id
     */
    @ApiModelProperty("单位id")
    private String companyId;

    /**
     * 部门名称
     */
    @ApiModelProperty("部门名称")
    private String departmentName;

    @ApiModelProperty("部门ID")
    private String departmentId;

    /**
     * 体检分类
     */
    @ApiModelProperty("体检分类")
    private String peType;

    @ApiModelProperty("体检分类名称")
    private String peTypeName;
    /**
     * 体检类别
     */
    @ApiModelProperty("体检类别")
    private String type;

    @ApiModelProperty("体检类别名称")
    private String typeName;

    @ApiModelProperty("危害因素")
    private String poisonType;

    @ApiModelProperty("危害因素名称")
    private String poisonTypeName;
}
