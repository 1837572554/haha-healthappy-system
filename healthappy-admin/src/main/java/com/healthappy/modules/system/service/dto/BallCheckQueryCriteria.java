package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class BallCheckQueryCriteria {

    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("性别")
    private Integer sex;

    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("婚姻状况")
    private Integer marry;

    @ApiModelProperty("体检日期开始时间")
    @Query(type = Query.Type.GREATER_THAN)
    private Timestamp appointDateStart;

    @ApiModelProperty("体检日期结束时间")
    @Query(type = Query.Type.LESS_THAN)
    private Timestamp appointDateEnd;
}
