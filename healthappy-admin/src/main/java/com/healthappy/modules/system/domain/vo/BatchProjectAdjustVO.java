package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 批量项目调整VO
 * @author Jevany
 * @date 2022/5/13 15:39
 */
@Data
@ApiModel("批量项目调整VO")
public class BatchProjectAdjustVO implements Serializable {
	/** 流水号 */
	@ApiModelProperty("流水号")
	private String paId;

	/** 姓名 */
	@ApiModelProperty("姓名")
	private String name;

	/** 单位名称 */
	@ApiModelProperty("单位名称")
	private String companyName;

	private static final long serialVersionUID = 1L;
}