package com.healthappy.modules.system.rest.medical;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BPackageDT;
import com.healthappy.modules.system.domain.BPackageHD;
import com.healthappy.modules.system.domain.vo.BPackageHDVo;
import com.healthappy.modules.system.service.BPackageDTService;
import com.healthappy.modules.system.service.BPackageHDService;
import com.healthappy.modules.system.service.dto.BPackageHDQueryCriteria;
import com.healthappy.modules.system.service.impl.BPackageHDServiceImpl;
import com.healthappy.utils.R;
import com.healthappy.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 套餐表 控制器
 * @author FGQ
 * @date 2021-07-2
 */
@Slf4j
@Api(tags = "体检管理：套餐")
@RestController
@AllArgsConstructor
@RequestMapping("/BPackageHD")
public class BPackageHDController {

    private final BPackageHDService bPackageHDService;
    private final BPackageDTService bPackageDTService;

    @Log("查询套餐")
    @ApiOperation("查询套餐，权限码：BPackageHD:list")
    @GetMapping
    public R list(BPackageHDQueryCriteria criteria,@PageableDefault(size = 2000) Pageable pageable) {
        return R.ok(bPackageHDService.queryAll(criteria,pageable));
    }

    @Log("新增|修改套餐")
    @ApiOperation("新增|修改套餐，权限码：BPackageHD:add")
    @PostMapping
    @PreAuthorize("@el.check('BPackageHD:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BPackageHDVo resources) {
        if(bPackageHDService.count(new LambdaQueryWrapper<BPackageHD>()
                .ne(StrUtil.isNotBlank(resources.getId()),BPackageHD::getId,resources.getId())
                .eq(BPackageHD::getName,resources.getName())
                .eq(BPackageHD::getTenantId, SecurityUtils.getTenantId())
                .ne(BPackageHD::getIsEnable,"0"))>0){
            throw new BadRequestException("编辑套餐【" + resources.getName() + "】名称已存在，名称不能重复");
        }
        bPackageHDService.saveOrUpdatePackage(resources);
        return R.ok();
    }

    @Log("删除套餐")
    @ApiOperation("删除套餐，权限码：BPackageHD:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('BPackageHD:delete')")
    @Klock
    public R remove(@RequestBody Set<Integer> ids) {
        if(bPackageHDService.removeByIds(ids)){
            bPackageDTService.remove(new LambdaUpdateWrapper<BPackageDT>().in(BPackageDT::getPackageId,ids));
            return R.ok();
        }
        return R.error(500,"方法异常");
    }
}
