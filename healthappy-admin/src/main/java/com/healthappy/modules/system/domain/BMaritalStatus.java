package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.sql.Timestamp;
import lombok.Data;

/**
 * Created with IntelliJ IDEA. Created by yutang 2021/9/2 0002  10:54 Description:
 */
@Data
@TableName("B_marital_status")
public class BMaritalStatus implements Serializable {

  private static final long serialVersionUID = 9188008984424269100L;

  @TableId(type = IdType.AUTO)
  private Long id;

  private String code;

  private String detail;

  @TableField(fill = FieldFill.INSERT)
  private Timestamp createTime;
}
