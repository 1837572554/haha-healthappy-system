package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BDepartmentGroupHD;
import com.healthappy.modules.system.domain.vo.BDepartmentGroupHDVo;
import com.healthappy.modules.system.service.dto.BDepartmentGroupHDQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 分组表 服务层
 * @author sjc
 * @date 2021-06-24
 */
public interface BDepartmentGroupHDService extends BaseService<BDepartmentGroupHD> {
	Map<String, Object> queryAll(BDepartmentGroupHDQueryCriteria criteria, Pageable pageable);

	List<BDepartmentGroupHD> queryAll(BDepartmentGroupHDQueryCriteria criteria);

	boolean isExistDepartmentGroup(String id);


	/**
	 * 获得单位下的部门分组编号
	 * @param companyId 单位编号
	 * @param tenantId 租户编号
	 * @return
	 */
	BDepartmentGroupHD getMaxIDDepartmentGroup(String companyId, String tenantId);

	/**
	 * 根据单位编号，生成部门分组信息
	 * @param companyId 单位编号
	 * @param tenantId 租户编号
	 * @return
	 */
	String generateDepartmentGroup(String companyId, String tenantId);

	/**
	 * 保存组合项目
	 * @author YJ
	 * @date 2022/1/15 13:30
	 * @param resources
	 * @param ip
	 */
	void saveGroupHd(BDepartmentGroupHDVo resources, String ip);

	/**
	 * 拷贝部门分组数据
	 * @author YJ
	 * @date 2022/4/29 17:19
	 * @param companyId
	 * @param departmentId
	 * @param departmentGroupId
	 */
	String copyDepartmentGroup(String companyId, String departmentId, String departmentGroupId);
}
