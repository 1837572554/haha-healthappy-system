package com.healthappy.modules.system.service.dto;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 预约表
 * @author sjc
 * @date 2021-09-14
 */
@Data
public class TAppointGroupQueryCriteria  {

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    @TableId(type = IdType.AUTO)
    private Long id;


    /**
     * 预约号
     */
    @ApiModelProperty("预约号")
    private String appointId;



    /**
     * 组合项目ID
     */
    @ApiModelProperty("组合项目ID")
    private String groupId;
}
