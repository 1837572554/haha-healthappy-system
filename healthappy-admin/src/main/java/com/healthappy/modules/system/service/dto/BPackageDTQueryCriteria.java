package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BPackageDTQueryCriteria {

    /**
     * 套餐编号
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("套餐编号")
    private String packageId;
}
