package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.modules.system.service.dto.MedicalRegistrationDto;
import com.healthappy.modules.system.service.dto.RegisterProDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 登记
 * @author fang
 * @date 2021-06-17
 */
@Mapper
@Repository
public interface MedicalRegistrationMapper {

    @Select("SELECT DISTINCT tp.*,bc.`name` as companyName,bs.`name` as sexName , bph.`name` \n" +
            "as peTypeHdName,bpd.`name` as peTypeDtName,bpj.`name` as jobName\n" +
            ",bd.`name` as departmentName,bdgh.`name` as departmentGroupName,\n" +
            "tpz.work_years as workYears,tpz.poison_type as poisonType\n" +
            " \n" +
            "FROM T_Patient as tp LEFT JOIN B_Company as bc ON tp.company_id = bc.id\n" +
            "\n" +
            "LEFT JOIN B_Sex as bs ON tp.sex = bs.id \n" +
            "\n" +
            "LEFT JOIN B_PeType_HD as bph on bph.id = tp.pe_type_hd_id\n" +
            "\n" +
            "LEFT JOIN B_PeType_DT AS bpd ON bpd.id = tp.pe_type_dt_id\n" +
            "\n" +
            "LEFT JOIN B_Petype_Job as bpj ON bpj.id = tp.job\n" +
            "\n" +
            "LEFT JOIN B_Department as bd ON bd.id = tp.department_id\n" +
            "\n" +
            "LEFT JOIN B_Department_Group_HD as bdgh ON bdgh.id = tp.department_group_id\n" +
            "\n" +
            "LEFT JOIN T_Patient_Z as tpz ON tpz.pa_id = tp.id\n" +
            "\n where tp.id = #{id}")
    MedicalRegistrationDto buildBean(@Param("id") String id);
    
    @Select("SELECT bdgt.*,tih.addition from B_Department_Group_TD as bdgt \n" +
            "LEFT JOIN  T_Item_HD AS tih  ON bdgt.group_id = tih.group_id " +
            "where department_group_hd_id = #{departmentGroupHdId}")
    List<RegisterProDto> buildProject(@Param("departmentGroupHdId") Long departmentGroupHdId);

    @SqlParser(filter = true)
    @Select("select tih.pa_id,tih.group_id,tih.group_name,tih.dept_id,tih.type_z,tih.type_j,tih.price,tih.discount,tih.cost,tih.addition,tih.choose,tih.pay_type,tih.free,tih.register_doc\n" +
            "from T_Item_HD tih \n" +
            "left join B_Dept bd on bd.id =tih.dept_id and bd.tenant_id =tih.tenant_id \n" +
            "left join B_Group_HD bgh on bgh.id =tih.group_id and bgh.tenant_id =tih.tenant_id \n" +
            "where tih.pa_id=#{paId} and tih.tenant_id=#{tenantId} \n" +
            "order by IFNULL(bd.disp_order,999),IFNULL(bgh.disp_order,999),IFNULL(tih.group_id,999)")
    List<RegisterProDto> getList(@Param("paId") String paId,@Param("tenantId") String tenantId);
}
