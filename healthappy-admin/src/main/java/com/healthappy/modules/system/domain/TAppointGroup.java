package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.healthappy.config.SeedIdGenerator;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 预约组合项目记录
 */
@Data
@TableName("T_Appoint_Group")
@SeedIdGenerator
@RenewLog
public class TAppointGroup implements Serializable {
    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    @TableId(type = IdType.INPUT)
    private String id;

    @ApiModelProperty("预约号")
    private String appointId;

    @ApiModelProperty("组合项目ID")
    private String groupId;

    @ApiModelProperty("自费方式")
    private Integer payType;

    @ApiModelProperty("价格")
    private BigDecimal price;

    @ApiModelProperty("其他ID，预留")
    private String otherId;

    @ApiModelProperty("是否放弃缴费  0否 1是 默认0")
    private Integer discard;

    @TableField(value = "tenant_id", fill = FieldFill.INSERT)
    private String tenantId;

    @ApiModelProperty("折扣")
    private BigDecimal discount;

    @ApiModelProperty("实际价格")
    private BigDecimal cost;
}
