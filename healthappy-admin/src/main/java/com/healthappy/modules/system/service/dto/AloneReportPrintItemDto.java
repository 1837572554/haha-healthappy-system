package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author sqoy
 * @date 2021/12/18 09:47
 * @desc
 */
@Data
public class AloneReportPrintItemDto  implements Serializable {

    /**
     * 科室编号
     */
    @ApiModelProperty("项目名称")
    private String itemName;

    /**
     * 科室编号
     */
    @ApiModelProperty("结果")
    private String result;

    /**
     * 科室编号
     */
    @ApiModelProperty("单位")
    private String unit;

    /**
     * 科室编号
     */
    @ApiModelProperty("范围")
    private String refValue;

}
