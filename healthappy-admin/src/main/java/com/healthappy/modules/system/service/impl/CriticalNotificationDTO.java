package com.healthappy.modules.system.service.impl;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 危急值通知类
 * @author Jevany
 * @date 2022/2/28 17:17
 */
@Data
public class CriticalNotificationDTO {
    /** 危急值自动编号 */
    @ApiModelProperty("危急值自动编号")
    private Long id;

    /** 流水号 */
    @NotBlank(message = "流水号不能为空")
    @ApiModelProperty("流水号")
    private String paId;

    /** 姓名 */
    @ApiModelProperty("姓名")
    private String name;

    /** 具体情况 */
    @ApiModelProperty("具体情况")
    private String detail;

    /** 租户编号 */
    @ApiModelProperty("租户编号")
    private String tenantId;
}