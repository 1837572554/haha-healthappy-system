package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 图文扫描录入类
 * @author YJ
 * @date 2022/2/16 10:05
 */
@Data
@TableName("T_IMGTXTSCAN")
@RenewLog
public class TImgTxtScan implements Serializable {
    /**
     * 体检号
     */
    @ApiModelProperty(value = "体检号")
    private String paId;

    /**
     * 图文扫描类型（scanType）编号
     */
    @ApiModelProperty(value = "图文扫描类型（scanType）编号")
    private String imgTxtTypeId;

    /**
     * 图片文件编号
     */
    @ApiModelProperty(value = "图片文件编号")
    private String fileId;

    /**
     * 添加时间
     */
    @ApiModelProperty(value = "添加时间")
    private Date createTime;

    /**
     * 操作医生
     */
    @ApiModelProperty(value = "操作医生")
    private String doctor;

    /**
     * 医疗机构ID(U_Hospital_Info的ID)
     */
    @ApiModelProperty(value = "医疗机构ID(U_Hospital_Info的ID)")
    private String tenantId;

    private static final long serialVersionUID = 1L;
}
