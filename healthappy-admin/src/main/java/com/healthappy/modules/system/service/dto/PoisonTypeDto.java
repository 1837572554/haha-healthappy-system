package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class PoisonTypeDto  implements Serializable {

    @ApiModelProperty("毒害ID")
    private Long id;

    @ApiModelProperty("毒害种类名称")
    private String poisonName;
}
