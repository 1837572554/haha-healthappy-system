package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 体检者条码
 * @author Jevany
 * @date 2022/5/12 16:04
 */
@Data
@ApiModel("体检者条码")
public class BarcodeVO implements Serializable {
	/** 流水号 */
	@ApiModelProperty("流水号")
	private String paId;

	/** 信息:姓名，性别，年龄 */
	@ApiModelProperty("信息:姓名，性别，年龄")
	private String infor;

	/** 条码头名称 */
	@ApiModelProperty("条码头名称")
	private String barcodeName;

	/** 项目名称 */
	@ApiModelProperty("项目名称")
	private String items;

	/** 申请类型 */
	@ApiModelProperty("申请类型")
	private String applyType;

	/** 条码 */
	@ApiModelProperty("条码")
	private String code;

	/** Items中是组合项ID字符串还是组合项名称字符串 1是ID，0是名称 ,默认0 */
	@ApiModelProperty("Items中是组合项ID字符串还是组合项名称字符串 1是ID，0是名称 ,默认0")
	private Integer isIds;


	private static final long serialVersionUID = 1L;
}