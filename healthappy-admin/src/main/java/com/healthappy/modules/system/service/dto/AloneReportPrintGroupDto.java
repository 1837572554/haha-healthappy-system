package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author sqoy
 * @date 2021/12/18 09:47
 * @desc
 */
@Data
public class AloneReportPrintGroupDto implements Serializable {


    /**
     * 科室编号
     */
    @ApiModelProperty("科室编号")
    private String deptId;

    /**
     * 科室名称
     */
    @ApiModelProperty("科室名称")
    private String deptName;

    /**
     * 组合编号
     */
    @ApiModelProperty("组合编号")
    private String groupId;

    /**
     * 组合名称
     */
    @ApiModelProperty("组合名称")
    private String groupName;

    /**
     * 报告医生
     */
    @ApiModelProperty("报告医生")
    private String doctor;

    /**
     * 结果时间
     */
    @ApiModelProperty("结果时间")
    private String resultDate;

    /**
     * 完成标志
     */
    @ApiModelProperty("完成标志：0：未完成；1：已完成")
    private String endflag;


}
