package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BTypeDevice;
import com.healthappy.modules.system.service.dto.BTypeDeviceQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 申请类型 服务层
 * @author sjc
 * @date 2021-08-3
 */
public interface BTypeDeviceService extends BaseService<BTypeDevice> {


    Map<String, Object> queryAll(BTypeDeviceQueryCriteria criteria, Pageable pageable);

    List<BTypeDevice> queryAll(BTypeDeviceQueryCriteria criteria);

    boolean deleteById(Long id);
}
