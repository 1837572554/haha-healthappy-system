package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class SearchSicknessTreeDto implements Serializable {

    @ApiModelProperty("部门id")
    private  String id;

    @ApiModelProperty("部门名称")
    private String deptName;

    @ApiModelProperty("项目集合")
    private List<ItemTreeDto> itemTreeList;
}

