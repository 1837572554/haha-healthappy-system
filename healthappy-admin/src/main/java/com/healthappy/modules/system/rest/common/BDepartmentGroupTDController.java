package com.healthappy.modules.system.rest.common;

import cn.hutool.core.collection.CollUtil;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BDepartmentGroupTD;
import com.healthappy.modules.system.domain.vo.BDepartmentGroupTDSaveVo;
import com.healthappy.modules.system.domain.vo.BDepartmentGroupTDVo;
import com.healthappy.modules.system.service.BDepartmentGroupTDService;
import com.healthappy.modules.system.service.dto.BDepartmentGroupTDQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author FGQ
 * @description 分组表 控制器
 * @date 2021-06-24
 */
@Validated
@Slf4j
@Api(tags = "默认基础配置：分组项目")
@RestController
@AllArgsConstructor
@RequestMapping("/BDepartmentGroupTD")
public class BDepartmentGroupTDController {


    private final BDepartmentGroupTDService bDepartmentGroupTDService;
    private final IGenerator generator;

    @Log("查询分组项目")
    @ApiOperation("查询分组项目，权限码：BDepartmentGroupTD:list")
    @GetMapping
    //sort = {"department_group_hd_id"},
    public R query(BDepartmentGroupTDQueryCriteria criteria, @PageableDefault(size = 2000) Pageable pageable) {
        return R.ok(bDepartmentGroupTDService.queryAll(criteria, pageable));
    }


    @Log("根据多个分组id查询分组项目")
    @ApiOperation("根据多个分组id查询分组项目，权限码：BDepartmentGroupTD:list")
    @PostMapping("/list")
    public R list(@RequestBody BDepartmentGroupTDQueryCriteria criteria, @PageableDefault(size = 2000) Pageable pageable) {
        return R.ok(bDepartmentGroupTDService.list(criteria, pageable));
    }


    @Log("新增|修改分组项目表")
    @ApiOperation("新增|修改分组项目表，权限码：BDepartmentGroupTD:add")
    @PostMapping
    @PreAuthorize("@el.check('BDepartmentGroupTD:add','BDepartmentGroupHD:add')")
    @Klock
    public R saveOrUpdate(@Valid @RequestBody List<BDepartmentGroupTDVo> resources) {
        if (CollUtil.isEmpty(resources)) {
            return R.error(500, "方法异常");
        }
        String id = resources.get(0).getDepartmentGroupHdId();
        bDepartmentGroupTDService.deleteBDepartmentGroupTD(id);

        if (bDepartmentGroupTDService.insertSelective(generator.convert(resources, BDepartmentGroupTD.class)))
            return R.ok();
        return R.error(500, "方法异常");
    }

    @Log("新增|修改分组项目表")
    @ApiOperation("新增|修改分组项目表，权限码：BDepartmentGroupTD:add")
    @PostMapping("/addOrUpdate")
    @PreAuthorize("@el.check('BDepartmentGroupTD:add','BDepartmentGroupHD:add')")
    @Klock
    public R addOrUpdate(@Validated @RequestBody BDepartmentGroupTDSaveVo saveVo) {
        if (bDepartmentGroupTDService.addOrUpdate(saveVo)) {
            return R.ok();
        }
        return R.error(500, "方法异常");
    }
}
