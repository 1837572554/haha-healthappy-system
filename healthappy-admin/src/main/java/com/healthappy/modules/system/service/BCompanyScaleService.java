package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BCompanyScale;
import com.healthappy.modules.system.service.dto.BCompanyScaleQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 企业规模表
 * @author sjc
 * @date 2021-09-2
 */
public interface BCompanyScaleService extends BaseService<BCompanyScale> {

    Map<String, Object> queryAll(BCompanyScaleQueryCriteria criteria, Pageable pageable);

    List<BCompanyScale> queryAll(BCompanyScaleQueryCriteria criteria);
}
