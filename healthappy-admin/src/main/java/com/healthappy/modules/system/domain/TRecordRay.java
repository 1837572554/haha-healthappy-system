package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description 职业史放射表
 * @author sjc
 * @date 2021-12-15
 */
@Data
@ApiModel("职业史放射表")
@TableName("T_Record_Ray")
@RenewLog
public class TRecordRay implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    @TableId(type = IdType.AUTO)
    private Long id;


    /**
     * 体检号
     */
    @ApiModelProperty("体检号")
    private String paId;

    /**
     * 起止日期
     */
    @ApiModelProperty("起止日期")
    private String startEnd;

    /**
     * 单位
     */
    @ApiModelProperty("单位")
    private String workUnit;

    /**
     * 部门
     */
    @ApiModelProperty("部门")
    private String department;

    /**
     * 工种
     */
    @ApiModelProperty("工种")
    private String job;

    /**
     * 放射线种类
     */
    @ApiModelProperty("放射线种类")
    private String radiationType;

    /**
     * 工作量
     */
    @ApiModelProperty("工作量")
    private String workLoad;

    /**
     * 过量照射史
     */
    @ApiModelProperty("过量照射史")
    private String overRadiation;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;

    /**
     * 累计受照剂量
     */
    @ApiModelProperty("累计受照剂量")
    private String cumulativeDose;

    /**
     * 医疗机构ID(U_Hospital_Info的ID)
     */
    @ApiModelProperty("医疗机构ID(U_Hospital_Info的ID)")
    private String tenantId;
}
