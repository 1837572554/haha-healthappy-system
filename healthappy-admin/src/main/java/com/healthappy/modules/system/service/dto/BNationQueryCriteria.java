package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BNationQueryCriteria {

    @Query
    @ApiModelProperty("code")
    private Long code;

    @Query(type = Query.Type.INNER_LIKE)
    @ApiModelProperty("名称")
    private String name;

    @Query
    @ApiModelProperty("拼音")
    private String ziMu;
}
