package com.healthappy.modules.system.service;

import com.healthappy.modules.system.domain.vo.GroupResultVo;
import com.healthappy.modules.system.domain.vo.ProjectCheckVo;
import com.healthappy.modules.system.service.dto.*;

import java.util.List;
import java.util.Map;

/**
 * @author FGQ
 * @description 项目检查
 * @date 2021-10-26
 */
public interface ProjectCheckService {


    Map<String, Object> queryAll(ProjectCheckQueryCriteria criteria);

    /**
     * 查询一个人的数据
     *
     * @param paId
     * @return com.healthappy.modules.system.service.dto.ProjectCheckDto 一个人的数据
     * @author YJ
     * @date 2021/11/26 18:46
     */
    ProjectCheckDto queryOne(String paId);

    /**
     * 检查项目和结果查询
     *
     * @param criteria
     * @return
     */
    List<ProejctCheckBItemDtDto> checkupItemsAndResultsQuery(DiagnosisRelatedDtQueryCriteria criteria);

    /**
     * 修改项目及结果
     *
     * @param projectCheckVo
     * @author YJ
     * @date 2022/2/9 17:06
     */
    void updProjectCheck(ProjectCheckVo projectCheckVo);

    /**
     * 生成小结
     *
     * @param groupResultVo
     */
    ItemSicknessDescDto generateResult(GroupResultVo groupResultVo);

    /**
     * 获取体检者的历史检测数据
     *
     * @param paId
     * @return java.util.List〈com.healthappy.modules.system.service.dto.ProjectCheckDto〉
     * @author YJ
     * @date 2021/12/13 17:45
     */
    List<ProjectCheckDto> getHistory(String paId);

    /**
     * 获取结果（已经保存的）对应的病种数据
     * @author YJ
     * @date 2022/3/10 11:10
     * @param paId
     * @param itemId
     * @return java.util.List〈com.healthappy.modules.system.service.dto.ItemSicknessMatchDto〉
     */
    List<ItemSicknessMatchDto> listResultSickness(String paId,String itemId);
}
