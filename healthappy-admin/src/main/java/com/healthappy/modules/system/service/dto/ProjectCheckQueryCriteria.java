package com.healthappy.modules.system.service.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

@Data
public class ProjectCheckQueryCriteria {

    /**
     * 主键
     */
    @ApiModelProperty("主键 / 流水号")
    @Query(type = Query.Type.INNER_LIKE)
    private String id;

    @ApiModelProperty(hidden = true , value = "主键集合")
    @Query(type = Query.Type.IN,propName = "id")
    private List<String> ids;

    /**
     * 姓名
     */
    @ApiModelProperty("姓名")
    @Query(type = Query.Type.INNER_LIKE)
    private String name;

    /**
     * 单位编号
     */
    @Query
    @ApiModelProperty("单位编号")
    private String companyId;

    /**
     * 体检分类
     */
    @Query
    @ApiModelProperty("体检分类")
    private Long peTypeHdId;

    /**
     * 体检类别
     */
    @Query
    @ApiModelProperty("体检类别")
    private Long peTypeDtId;

    /**
     * 体检时间类型
     */
    @Query
    @ApiModelProperty("体检时间类型")
    @TableField(exist = false)
    private Integer peDateType;

    /**
     * 科室编号
     */
    @Query
    @ApiModelProperty("科室编号")
    private String deptId;

    /**
     * 体检日期
     */
    @Query(type = Query.Type.BETWEEN)
    @ApiModelProperty("体检日期")
    private List<Timestamp> peDate;

    /**
     * 完成状态 true:已完成 | false:未完成
     */
    @ApiModelProperty("完成状态 true:已完成 | false:未完成")
    private Boolean resultStatus;

    /**
     * 是否仅自己
     */
    @ApiModelProperty("是否仅自己")
    private Boolean onlyOneself;

    /**
     * 是否仅自己(医生姓名，后端赋值使用)
     */
    @ApiModelProperty(value = "是否仅自己(医生姓名，后端赋值使用)", hidden = true)
    private String doctor;

    /**
     * 工作站code
     */
    @Query
    @ApiModelProperty("工作站code")
    private String workstation;

    /** 租户编号 */
    @ApiModelProperty(value = "租户编号",hidden = true)
    private String tenantId;
}
