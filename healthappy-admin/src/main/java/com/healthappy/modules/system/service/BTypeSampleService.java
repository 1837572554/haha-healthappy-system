package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BTypeSample;
import com.healthappy.modules.system.service.dto.BTypeSampleQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 标本类型 服务层
 * @author sjc
 * @date 2021-08-3
 */
public interface BTypeSampleService extends BaseService<BTypeSample> {

    Map<String, Object> queryAll(BTypeSampleQueryCriteria criteria, Pageable pageable);

    List<BTypeSample> queryAll(BTypeSampleQueryCriteria criteria);

    boolean deleteById(Long id);
}
