package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @description 单位属性
 * @author sjc
 * @date 2021-08-30
 */
@Data
public class BCompanyAttributeVo  {

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private Long id;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    @NotBlank(message = "名称不能为空")
    private String name;
    /**
     * 单位属性代码
     */
    @ApiModelProperty("单位属性代码")
    private String code;

    /**
     * 排序
     */
    @ApiModelProperty("排序")
    private Integer dispOrder;

}
