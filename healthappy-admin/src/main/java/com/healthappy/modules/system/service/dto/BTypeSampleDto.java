package com.healthappy.modules.system.service.dto;

import com.healthappy.modules.system.domain.BTypeSample;
import lombok.Data;

/**
 * @description 标本类型
 * @author sjc
 * @date 2021-08-3
 */
@Data
public class BTypeSampleDto extends BTypeSample {



}
