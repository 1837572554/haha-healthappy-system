package com.healthappy.modules.system.service.impl;

import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.TCompanyRepDt;
import com.healthappy.modules.system.service.TCompanyRepDtService;
import com.healthappy.modules.system.service.mapper.TCompanyRepDtMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 单位报告人员表 服务实现
 * @author Jevany
 * @date 2022/2/18 18:56
 */
@AllArgsConstructor
@Service
public class TCompanyRepDtServiceImpl extends BaseServiceImpl<TCompanyRepDtMapper, TCompanyRepDt> implements TCompanyRepDtService {
}