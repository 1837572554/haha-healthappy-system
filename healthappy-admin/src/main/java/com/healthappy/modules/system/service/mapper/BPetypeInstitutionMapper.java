package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BPetypeInstitution;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author FGQ
 * @date 2020-05-14
 */
@Repository
@Mapper
public interface BPetypeInstitutionMapper extends CoreMapper<BPetypeInstitution> {

}
