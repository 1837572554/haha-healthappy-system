package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BReportConfig;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Jevany
 * @date 2022/4/11 15:27
 */
@Mapper
@Repository
public interface BReportConfigMapper extends CoreMapper<BReportConfig> {
}
