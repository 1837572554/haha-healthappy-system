package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * id种子
 * @author FGQ
 */
@Data
@TableName("T_Id_Seed")
public class TidSeed implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("种子 / 版本号")
    @Version
    private Long version;

    @ApiModelProperty("工作站编号")
    private String workStationCode;

    @ApiModelProperty(value = "租户ID")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;

    @ApiModelProperty("表名")
    private String tableName;
}
