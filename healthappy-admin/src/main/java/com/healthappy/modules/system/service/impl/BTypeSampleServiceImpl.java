package com.healthappy.modules.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BTypeSample;
import com.healthappy.modules.system.service.BTypeSampleService;
import com.healthappy.modules.system.service.dto.BTypeSampleDto;
import com.healthappy.modules.system.service.dto.BTypeSampleQueryCriteria;
import com.healthappy.modules.system.service.mapper.BTypeSampleMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sjc
 * @date 2021-08-3
 */
@Service
@AllArgsConstructor//有自动注入的功能
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BTypeSampleServiceImpl extends BaseServiceImpl<BTypeSampleMapper, BTypeSample> implements BTypeSampleService {

    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BTypeSampleQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BTypeSample> page = new PageInfo<BTypeSample>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("content", generator.convert(page.getList(), BTypeSampleDto.class));
        map.put("totalElements", page.getTotal());

        return map;
    }

    @Override
    public List<BTypeSample> queryAll(BTypeSampleQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BTypeSampleDto.class, criteria));
    }

    @Override
    public boolean deleteById(Long id)
    {
        return baseMapper.deleteById(id)>0;
    }

}
