package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class DiagnosisRelatedDtQueryCriteria {

    @Query
    @NotBlank(message = "科室ID必填")
    @ApiModelProperty(value = "科室ID",required = true)
    private String deptId;

    @Query
    @NotNull(message = "项目ID必填")
    @ApiModelProperty(value = "项目ID",required = true)
    private Long id;

    @Query
    @NotBlank(message = "流水号必填")
    @ApiModelProperty(value = "流水号",required = true)
    private String paId;
}
