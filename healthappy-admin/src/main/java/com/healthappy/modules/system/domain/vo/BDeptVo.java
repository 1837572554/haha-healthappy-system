package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class BDeptVo {

    /**
     * id
     */
    @ApiModelProperty(value = "id",position = 1)
    private String id;

    /**
     * 科室名
     */
    @NotBlank(message = "科室名不能为空")
    @ApiModelProperty(value = "科室名称",required = true,position = 2)
    private String name;

    /**
     * 类型
     */
    @NotNull(message = "类型不能为空")
    @ApiModelProperty(value = "类型",required = true,position = 3)
    private Integer deptType;

    /**
     * 排序号
     */
    @NotNull(message = "排序号不能为空")
    @ApiModelProperty(value = "排序号",required = true,position = 4)
    private Integer dispOrder;

    /**B_ITEM
     * 是否启用
     */
    @NotNull(message = "状态不能为空")
    @ApiModelProperty(value = "启用状态",required = true,position = 5)
    private Boolean isEnable;

    /**
     * 描述
     */
    @ApiModelProperty(value = "描述",position = 6)
    private String description;

    /**
     * 简拼
     */
    @ApiModelProperty(value = "简拼",position = 7)
    private String jp;

    /**
     * 类别名称
     */
    @ApiModelProperty(value = "类别名称",position = 8)
    private String deptCode;

    /**
     * 科室类别名
     */
    @ApiModelProperty(value = "科室类别名",position = 9)
    private String categoryName;
}
