package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.Dict;
import com.healthappy.modules.system.domain.vo.DictEnumVo;
import com.healthappy.modules.system.service.dto.DictDto;
import com.healthappy.modules.system.service.dto.DictQueryCriteria;
import com.healthappy.modules.system.service.dto.DictTreeDto;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author hupeng
 * @date 2020-05-14
 */
public interface DictService extends BaseService<Dict> {

    /**
     * 查询数据分页
     * @param criteria 条件
     * @param pageable 分页参数
     * @return Map<String, Object>
     */
    Map<String, Object> queryAll(DictQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     * @param criteria 条件参数
     * @return List<DictDto>
     */
    List<Dict> queryAll(DictQueryCriteria criteria);

    /**
     * 导出数据
     * @param all 待导出的数据
     * @param response /
     * @throws IOException /
     */
    void download(List<DictDto> all, HttpServletResponse response) throws IOException;


    List<DictEnumVo>  dictConvertEnum();

    List<DictTreeDto> tree();

	List<DictEnumVo> reload();
}
