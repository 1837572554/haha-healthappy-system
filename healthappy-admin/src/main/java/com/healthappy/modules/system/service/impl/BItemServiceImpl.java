package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.BDept;
import com.healthappy.modules.system.domain.BItem;
import com.healthappy.modules.system.service.BDeptService;
import com.healthappy.modules.system.service.BItemService;
import com.healthappy.modules.system.service.dto.BItemDto;
import com.healthappy.modules.system.service.dto.BItemQueryCriteria;
import com.healthappy.modules.system.service.dto.BItemTreeDto;
import com.healthappy.modules.system.service.mapper.BItemMapper;
import com.healthappy.utils.CacheConstant;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
@CacheConfig(cacheNames = {CacheConstant.B_ITEM})
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BItemServiceImpl extends BaseServiceImpl<BItemMapper, BItem> implements BItemService {

	private final BDeptService bDeptService;

	@Override
	@Cacheable
	public Map<String, Object> queryAll(BItemQueryCriteria criteria, Pageable pageable) {
		getPage(pageable);
		PageInfo<BItem> page = new PageInfo<BItem>(queryAll(criteria));
		Map<String, Object> map = new LinkedHashMap<>(2);
		List<BItemDto> dtoList = new ArrayList<>();
		for (BItem item : page.getList()) {
			BItemDto itemDto = BeanUtil.copyProperties(item, BItemDto.class);
			BDept dept = bDeptService.getById(item.getDeptId());
			itemDto.setDeptName(Optional.ofNullable(dept).map(BDept::getName).orElse(""));
			dtoList.add(itemDto);
		}
		map.put("content", dtoList);
		map.put("totalElements", page.getTotal());
		return map;
	}

	@Override
	@Cacheable
	public List<BItem> queryAll(BItemQueryCriteria criteria) {
		return baseMapper.selectList(QueryHelpPlus.getPredicate(BItemDto.class, criteria));
	}

	@Override
	@Cacheable
	public List<BItemTreeDto> getTree() {
		List<BDept> deptList = bDeptService.list(
				Wrappers.<BDept>lambdaQuery().eq(BDept::getIsEnable, true).select(BDept::getId, BDept::getName));
		if (CollUtil.isEmpty(deptList)) {
			return new ArrayList<>();
		}
		List<BItem> itemList = this.list(Wrappers.<BItem>lambdaQuery().eq(BItem::getIsEnable, true)
				.select(BItem::getId, BItem::getName, BItem::getDeptId));
		Map<String, List<BItem>> deptMap = itemList.stream().collect(Collectors.groupingBy(BItem::getDeptId));
		return deptList.stream().map(x -> BItemTreeDto.builder().deptId(x.getId()).deptName(x.getName())
				.bItems(deptMap.getOrDefault(x.getId(), null)).build()).collect(Collectors.toList());
	}

	@Override
	public List<BItem> listByGroupId(String groupId) {
		if (StrUtil.isBlank(groupId)) {
			throw new BadRequestException("组合编号不能为空");
		}
		return this.baseMapper.listByGroupId(groupId);
	}


}
