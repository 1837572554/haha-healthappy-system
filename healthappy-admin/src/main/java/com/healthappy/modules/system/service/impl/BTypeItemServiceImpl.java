package com.healthappy.modules.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BTypeItem;
import com.healthappy.modules.system.domain.ZhToSickness;
import com.healthappy.modules.system.service.BTypeItemService;
import com.healthappy.modules.system.service.dto.BTypeItemDto;
import com.healthappy.modules.system.service.dto.BTypeItemQueryCriteria;
import com.healthappy.modules.system.service.dto.ZhToSicknessDto;
import com.healthappy.modules.system.service.dto.ZhToSicknessQueryCriteria;
import com.healthappy.modules.system.service.mapper.BTypeItemMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sjc
 * @date 2021-08-3
 */
@Service
@AllArgsConstructor//有自动注入的功能
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BTypeItemServiceImpl  extends BaseServiceImpl<BTypeItemMapper, BTypeItem> implements BTypeItemService {

    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BTypeItemQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BTypeItem> page = new PageInfo<BTypeItem>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("content", generator.convert(page.getList(), BTypeItemDto.class));
        map.put("totalElements", page.getTotal());

        return map;
    }

    @Override
    public List<BTypeItem> queryAll(BTypeItemQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BTypeItemDto.class, criteria));
    }

    @Override
    public boolean deleteById(Long id)
    {
        return baseMapper.deleteById(id)>0;
    }

}
