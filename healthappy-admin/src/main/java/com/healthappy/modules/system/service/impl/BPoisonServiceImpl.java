package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.BPackageDT;
import com.healthappy.modules.system.domain.BPackageHD;
import com.healthappy.modules.system.domain.BPoison;
import com.healthappy.modules.system.domain.vo.BPackageDTVo;
import com.healthappy.modules.system.domain.vo.BPoisonLiteVo;
import com.healthappy.modules.system.service.BPackageDTService;
import com.healthappy.modules.system.service.BPackageHDService;
import com.healthappy.modules.system.service.BPoisonService;
import com.healthappy.modules.system.service.dto.*;
import com.healthappy.modules.system.service.mapper.BPoisonMapper;
import com.healthappy.modules.system.service.wrapper.BPoisonWrapper;
import com.healthappy.utils.BeanCopyUtil;
import com.healthappy.utils.SecurityUtils;
import com.healthappy.utils.StringUtils;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;


/**
 * 有害因素服务层实现层
 *
 * @author sjc
 * @date 2021-08-9
 */
@Service
@AllArgsConstructor//有自动注入的功能
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BPoisonServiceImpl extends BaseServiceImpl<BPoisonMapper, BPoison> implements BPoisonService {

    private final BPackageHDService bPackageHDService;
    private final BPackageDTService bPackageDTService;

    @Override
    public Map<String, Object> queryAll(BPoisonQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        return BPoisonWrapper.build().pageVO(new PageInfo<BPoison>(queryAll(criteria)));
    }

    @Override
    public List<BPoison> queryAll(BPoisonQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BPoison.class, criteria));
    }

    /**
     * 根据体检类别获取对应的套餐信息
     *
     * @param id
     * @return
     */
    @Override
    public List<BPoisonDto> queryAllByPeTypeDt(String id, Integer sex) {
        List<BPoison> list = this.list();
        return queryByPeTypeDt(list, id, sex);
    }


    /**
     * 根据毒害列表和体检类别获取对应的套餐信息
     *
     * @param list 要查询的毒害列表
     * @param id   体检分类id
     * @return
     */
    @Override
    public List<BPoisonDto> queryByPeTypeDt(List<BPoison> list, String id, Integer sex) {
        List<BPoisonDto> BPoisonList = new ArrayList<BPoisonDto>();
        switch (id) {
            case "1"://上岗前
                BPoisonList = list.stream().filter(role -> ObjectUtil.isNotNull(role.getPackageBeforeId()))
                        .filter(v -> filterSex(v.getPackageBeforeId(), sex))
                        .map(
                                s -> {
                                    BPoisonDto bPoison = new BPoisonDto();
                                    bPoison.setPackageId(s.getPackageBeforeId());
                                    bPoison.setPackageName(s.getPackageBeforeName());
                                    return bPoison;
                                }
                        ).collect(Collectors.toList());
                break;
            case "2"://在岗期间
                BPoisonList = list.stream().filter(role -> ObjectUtil.isNotNull(role.getPackageInId()))
                        .filter(v -> filterSex(v.getPackageInId(), sex))
                        .map(
                                s -> {
                                    BPoisonDto bPoison = new BPoisonDto();
                                    bPoison.setPackageId(s.getPackageInId());
                                    bPoison.setPackageName(s.getPackageInName());
                                    return bPoison;
                                }
                        ).collect(Collectors.toList());
                break;
            case "3"://离岗前
            case "4"://离岗后
                BPoisonList = list.stream().filter(role -> ObjectUtil.isNotNull(role.getPackageAfterId()))
                        .filter(v -> filterSex(v.getPackageAfterId(), sex))
                        .map(
                                s -> {
                                    BPoisonDto bPoison = new BPoisonDto();
                                    bPoison.setPackageId(s.getPackageAfterId());
                                    bPoison.setPackageName(s.getPackageAfterName());
                                    return bPoison;
                                }
                        ).collect(Collectors.toList());//排除为空的数据
                break;
            case "5"://应急
                BPoisonList = list.stream().filter(role -> ObjectUtil.isNotNull(role.getPackageTempId()))
                        .filter(v -> filterSex(v.getPackageTempId(), sex))
                        .map(
                                s -> {
                                    BPoisonDto bPoison = new BPoisonDto();
                                    bPoison.setPackageId(s.getPackageTempId());
                                    bPoison.setPackageName(s.getPackageTempName());
                                    return bPoison;
                                }
                        ).collect(Collectors.toList());
                break;
        }
        return BPoisonList;
    }


    private boolean filterSex(String BPackageHdId, Integer sex) {
        if (null == sex) {
            return true;
        }
        return Optional.ofNullable(bPackageHDService.lambdaQuery().eq(BPackageHD::getId, BPackageHdId).eq(BPackageHD::getSex, sex).one()).isPresent();
    }

    @Override
    //@DS("click")
    public Object registerPoisonQueryAll(BRegisterPoisonQueryCriteria criteria, Pageable pageable) {
        List<BPoison> list = baseMapper.selectList(
                QueryHelpPlus.getPredicate(BPoison.class, criteria)
        );
        if (CollUtil.isEmpty(list)) {
            return Collections.emptyList();
        }
        if (ObjectUtil.isNull(criteria.getCode())) {
            return list;
        }
        return list.stream().map(v -> {
            RegisterPoisonDto registerPoisonDto = BeanUtil.copyProperties(v, RegisterPoisonDto.class);
            String id = getPoisonPackageId(criteria.getCode(), v);
            if(StringUtils.isNotBlank(id)){
                BPackageHDQueryCriteria queryCriteria = new BPackageHDQueryCriteria();
                queryCriteria.setId(id);
                registerPoisonDto.setPackageHDList((List<BPackageHD>) bPackageHDService.queryAll(queryCriteria, pageable).get("content"));
            }
            return registerPoisonDto;
        }).collect(Collectors.toList());
    }

    /**
     * 获取毒害下Code类型对应的套餐编号
     *
     * @param code
     * @param v
     * @return java.lang.String
     * @author YJ
     * @date 2021/12/7 19:25
     */
    private String getPoisonPackageId(Integer code, BPoison v) {
        if(ObjectUtil.isNull(code)){
            return "";
        }
        String id;
        switch (code) {
            case 1://上岗前
                id = v.getPackageBeforeId();
                break;
            //在岗期间
            case 2:
                id = v.getPackageInId();
                break;
            case 4://离岗后
                id = v.getPackageAfterId();
                break;
            case 5://应急
                id = v.getPackageTempId();
                break;
            default:
                throw new BadRequestException("参数有误");
        }
        return id;
    }

    @Override
    public List<BPoisonLiteVo> registerPoisonQueryAllLite(BRegisterPoisonQueryLiteCriteria criteria) {
        if(ObjectUtil.isNull(criteria.getCode())){
            criteria.setCode(-1);
        }
        criteria.setTenantId(SecurityUtils.getTenantId());
        return baseMapper.getPoisonIdNameJps(criteria);
//        List<BPoison> list = baseMapper.selectList(QueryHelpPlus.getPredicate(BPoison.class, criteria));
//        if (CollUtil.isEmpty(list)) {
//            return Collections.emptyList();
//        }
//        return BeanCopyUtil.copyList(list, BPoisonLiteVo.class);
    }

    @Override
    public List<BPackageHD> registerPoisonPackage(BRegisterPoisonQueryCriteria criteria) {
        List<BPoison> list = baseMapper.selectList(QueryHelpPlus.getPredicate(BPoison.class, criteria));
        if (CollUtil.isEmpty(list)) {
            return Collections.emptyList();
        }
        List<BPackageHD> packageHDS = new ArrayList<>();
        BPackageHDQueryCriteria queryCriteria = new BPackageHDQueryCriteria();
        for (BPoison bPoison : list) {
            String packageId = getPoisonPackageId(criteria.getCode(), bPoison);
            if(StringUtils.isNotBlank(packageId)){
                queryCriteria.setId(packageId);
                packageHDS.addAll(bPackageHDService.queryAll(queryCriteria));
            }

        }
        return packageHDS;
    }

    @Override
    public List<BPackageDT> registerPoisonPackageDt(BRegisterPoisonQueryLiteCriteria criteria) {
        if(ObjectUtil.isNull(criteria.getCode())){
            criteria.setCode(-1);
        }
        return baseMapper.getPackageGroups(criteria);



//        List<BPoison> list = baseMapper.selectList(QueryHelpPlus.getPredicate(BPoison.class, criteria));
//        if (CollUtil.isEmpty(list)) {
//            return Collections.emptyList();
//        }
//
//        List<BPackageDT> packageDTList = new ArrayList<>();
//        for (BPoison bPoison : list) {
//            String packageId = getPoisonPackageId(criteria.getCode(), bPoison);
//            if (StringUtils.isNotBlank(packageId)) {
//                packageDTList = bPackageDTService.lambdaQuery().eq(BPackageDT::getPackageId, packageId).list();
//                if (CollUtil.isNotEmpty(packageDTList)) {
//                    packageDTList = BPackageHDWrapper.build().loadGroupHdName(packageDTList);
//                }
//            }
//        }
//        return packageDTList;
    }

    @Override
    public List<BPackageDT> getPoisonGroups(RegisterPoisonQueryCriteria criteria) {
        if(ObjectUtil.isEmpty(criteria.getPoisonIdList()) || criteria.getPoisonIdList().size()==0 ){
            throw new BadRequestException("毒害编号不能为空");
        }
        if(ObjectUtil.isNull(criteria.getCode())){
            throw new BadRequestException("体检类别不能为空");
        }

        List<BPackageDTVo> list=new ArrayList<>();
        criteria.setTenantId(SecurityUtils.getTenantId());
        //循环查询每一个毒害，模拟用户每次加一个毒害的操作
        for (String poisonId : criteria.getPoisonIdList()) {
            if(list.size()>0){
                criteria.setNotGroupIds(list.stream().map(BPackageDTVo::getGroupId).collect(Collectors.toList()));
            }
            criteria.setPoisonId(poisonId);
            List<BPackageDTVo> poisonGroup = baseMapper.getPoisonGroup(criteria);
            if(CollUtil.isNotEmpty(poisonGroup) && poisonGroup.size()>0){
                list.addAll(poisonGroup);
            }
        }
        if(list.size()>2){
            Comparator<BPackageDTVo> byDeptDisp=Comparator.comparing(BPackageDTVo::getDeptDisp);
            Comparator<BPackageDTVo> byGroupDisp=Comparator.comparing(BPackageDTVo::getGroupDisp);
            list.sort(byDeptDisp.thenComparing(byGroupDisp));
        }

        return BeanCopyUtil.copyList(list,BPackageDT.class);

    }

    @Override
    public List<BPackageDT> getPackageList(List<String> poisonTypeList, String peType) {
        if(CollUtil.isEmpty(poisonTypeList) || StrUtil.isBlank(peType)){
            throw new BadRequestException("参数不能为空");
        }
        return baseMapper.getPackageList(poisonTypeList,peType);
    }


}
