package com.healthappy.modules.system.domain;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author yanjun
 * @description 用户绑定科室表
 * @date 2021/11/3
 */

@Data
@TableName("users_depts")
public class UsersDepts implements Serializable {
    /**
     * 用户编号
     */
    private Long userId;

    /**
     * 科室编号
     */
    private String deptId;

    public void copy(UsersDepts source) {
        BeanUtil.copyProperties(source, this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
