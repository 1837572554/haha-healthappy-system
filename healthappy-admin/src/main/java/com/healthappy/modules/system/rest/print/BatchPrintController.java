package com.healthappy.modules.system.rest.print;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.healthappy.annotation.AnonymousAccess;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.http.report.HttpServer;
import com.healthappy.modules.my.queue.JQueue;
import com.healthappy.modules.my.queue.JQueueHandle;
import com.healthappy.modules.my.queue.PrintRequestObject;
import com.healthappy.modules.system.constants.CommRequstPathEnum;
import com.healthappy.modules.system.domain.BReportConfig;
import com.healthappy.modules.system.domain.TPatient;
import com.healthappy.modules.system.domain.vo.PrintGroupVo;
import com.healthappy.modules.system.rest.print.dto.AppointInformationVo;
import com.healthappy.modules.system.rest.print.dto.BasePrintVo;
import com.healthappy.modules.system.rest.print.dto.CompanyAnalysisPrintVo;
import com.healthappy.modules.system.rest.print.dto.PersonPrintVo;
import com.healthappy.modules.system.service.BPeTypeHDService;
import com.healthappy.modules.system.service.BReportConfigService;
import com.healthappy.modules.system.service.TPatientService;
import com.healthappy.utils.R;
import com.healthappy.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * @author FGQ
 */
@Slf4j
@Api(tags = "批量-打印")
@RestController
@AllArgsConstructor
@RequestMapping("/print/batch")
public class BatchPrintController {

	private final BReportConfigService reportConfigService;
	private final TPatientService patientService;
	private final BPeTypeHDService peTypeHDService;

	private final JQueueHandle jQueueHandle;


	// TODO :健康证 不做批量


	@Log("批量--打印指引单")
	@ApiOperation("批量--打印指引单")
	@PostMapping("/guide")

	@ApiResponses(value = {@ApiResponse(code = 200, message = "通过WebSocket返回对应的地址")})
	@AnonymousAccess
	public R guide(@RequestBody BasePrintVo vo) {
		if (CollectionUtils.isEmpty(vo.getIds())) {
			return R.error("流水号不能为空");
		}
		if (StringUtils.isBlank(vo.getAcceptSocketUserId())) {
			return R.error("用于接收的Socket的用户编号不能为空");
		}
		//处理信息放入队列
		runBeanOneQueue(vo.getIds(), vo.getAcceptSocketUserId(), CommRequstPathEnum.GUIDE);
		return R.ok();
	}

	;


	@Log("批量--打印条码")
	@ApiOperation("批量--打印条码")
	@PostMapping("/barcode")
	@ApiResponses(value = {@ApiResponse(code = 200, message = "通过WebSocket返回对应的地址")})
	@AnonymousAccess
	public R barcode(@RequestBody BasePrintVo vo) {
		if (CollectionUtils.isEmpty(vo.getIds())) {
			return R.error("流水号不能为空");
		}
		if (StringUtils.isBlank(vo.getAcceptSocketUserId())) {
			return R.error("用于接收的Socket的用户编号不能为空");
		}
		//处理信息放入队列
		runBeanOneQueue(vo.getIds(), vo.getAcceptSocketUserId(), CommRequstPathEnum.BAR_CODE);
		return R.ok();
	}

	;


	@Log("批量--打印信息表")
	@ApiOperation("批量打印信息表")
	@PostMapping("/information")
	@ApiResponses(value = {@ApiResponse(code = 200, message = "通过WebSocket返回对应的地址")})
	@AnonymousAccess
	public R information(@RequestBody BasePrintVo vo) {
		if (CollectionUtils.isEmpty(vo.getIds())) {
			return R.error("流水号不能为空");
		}
		if (StringUtils.isBlank(vo.getAcceptSocketUserId())) {
			return R.error("用于接收的Socket的用户编号不能为空");
		}
		runPersonReportQueue(vo.getIds(), vo.getAcceptSocketUserId(), CommRequstPathEnum.INFO_REPORT, null);
		return R.ok();
	}

	;


	@Log("批量--打印预约信息表")
	@ApiOperation("批量--打印预约信息表")
	@PostMapping("/appointInformation")
	@ApiResponses(value = {@ApiResponse(code = 200, message = "返回信息表地址")})
	@AnonymousAccess
	public R appointInformation(@RequestBody AppointInformationVo vo) {
		if (CollectionUtils.isEmpty(vo.getAppointIds())) {
			return R.error("流水号不能为空");
		}
		if (StringUtils.isBlank(vo.getAcceptSocketUserId())) {
			return R.error("用于接收的Socket的用户编号不能为空");
		}
		//获取HttpServer 服务Bean
		runAppointInfoQueue(vo.getAppointIds(), vo.getAcceptSocketUserId(), CommRequstPathEnum.APPOINT_INFO_REPORT);
		return R.ok();
	}


	@Log("批量--打印项目报告")
	@ApiOperation("批量--打印项目报告")
	@PostMapping("/groupPrint")
	@ApiResponses(value = {@ApiResponse(code = 200, message = "通过WebSocket返回对应的地址")})
	@AnonymousAccess
	public R groupPrint(@RequestBody PrintGroupVo groupVos) {
		if (!Optional.ofNullable(groupVos).isPresent()) {
			return R.error("参数不能为空");
		}
		if (CollUtil.isEmpty(groupVos.getPaIdList())) {
			return R.error("流水号不能为空");
		}
		if (CollUtil.isEmpty(groupVos.getGroupIdList())) {
			return R.error("组合编号不能为空");
		}
		if (StringUtils.isBlank(groupVos.getAcceptSocketUserId())) {
			return R.error("用于接收的Socket的用户编号不能为空");
		}
		HttpServer.GetItemReport itemReport = new HttpServer.GetItemReport();
		//		runGetItemReportQueue(paIds,acceptSocketUserId,commRequstPathEnum);
		itemReport.init();
		JQueue<PrintRequestObject> jQueue = new JQueue<>();
		PrintRequestObject object = null;
		for (String pId : groupVos.getPaIdList()) {
			for (String gId : groupVos.getGroupIdList()) {
				itemReport.setPatientId(pId);
				itemReport.setGroupId(gId);
				object = new PrintRequestObject();
				object.setHttpMethod(HttpMethod.POST);
				object.setCommType(CommRequstPathEnum.GET_ITEM_REPORT.getComm());
				object.setRequestMapping(CommRequstPathEnum.GET_ITEM_REPORT.getRequstPath());
				object.setParamMap(BeanUtil.beanToMap(itemReport));
				object.setUserName(groupVos.getAcceptSocketUserId());
				jQueue.add(object);
			}
		}
		//运行线程后台消费 队列
		runQueue(jQueue);
		return R.ok();
	}

	//	@Log("导出预约信息表")
	//	@ApiOperation("导出预约信息表")
	//	@PostMapping("/exportAppointInfo")
	//	@ApiImplicitParams({@ApiImplicitParam(name = "appointIds", value = "预约号列表", dataType = "List", required = true)})
	//	@ApiResponses(value = {@ApiResponse(code = 200, message = "返回预约列表的压缩包")})
	//	@AnonymousAccess
	//	public R exportAppointInfo(@RequestParam List<String> appointIds,@RequestParam String acceptSocketUserId) {
	//		if (CollUtil.isEmpty(appointIds)) {
	//			return R.error("预约号列表不能为空");
	//		}
	//		List<String> reportInfoNames = new ArrayList<>();
	//		for (String appointId : appointIds) {
	//			try {
	//				AppointInfo appointInfo = new AppointInfo();
	//				appointInfo.init();
	//				appointInfo.setAppointId(appointId);
	//				appointInfo.setFileType(1);
	//				runAppointInfoQueue(appointIds,acceptSocketUserId, CommRequstPathEnum.APPOINT_INFO_REPORT);
	//			} catch (Exception e) {
	//				log.error("批量预约导出出错：{0}", e.getMessage());
	//			}
	//		}
	//		return R.ok();
	//	}


	@Log("批量--打印个人报告")
	@ApiOperation("批量--打印个人报告")
	@PostMapping("/personPrint")
	@ApiResponses(value = {@ApiResponse(code = 200, message = "通过WebSocket返回对应的地址")})
	@AnonymousAccess
	public R personPrint(@RequestBody PersonPrintVo vo) {
		if (CollectionUtils.isEmpty(vo.getPaIds())) {
			return R.error("流水号不能为空");
		}
		if (StrUtil.isBlank(vo.getReportName())) {
			return R.error("报告类型名称不能为空");
		}
		if (StringUtils.isBlank(vo.getAcceptSocketUserId())) {
			return R.error("用于接收的Socket的用户编号不能为空");
		}
		if ("健康证".equals(vo.getReportName())) {
			//			return R.ok(healthCertificateReport(paIds));
		}
		runPersonReportQueue(vo.getPaIds(), vo.getAcceptSocketUserId(), CommRequstPathEnum.INFO_REPORT,
				vo.getReportName());

		return R.ok();
	}

	@Log("批量--打印单位健康分析报告")
	@ApiOperation("批量--打印单位健康分析报告")
	@PostMapping("/companyAnalysisPrint")
	@ApiResponses(value = {@ApiResponse(code = 200, message = "返回单位报告地址")})
	@AnonymousAccess
	public R companyAnalysisPrint(@RequestBody CompanyAnalysisPrintVo vo) {
		if (CollectionUtils.isEmpty(vo.getReportIds())) {
			return R.error("单位报告编号不能为空");
		}
		if (StrUtil.isBlank(vo.getSicknessIds())) {
			return R.error("疾病编号不能为空");
		}
		runCompanyReportQueue(vo.getReportIds(), vo.getSicknessIds(), vo.getAcceptSocketUserId(),
				CommRequstPathEnum.COMPANY_ANALYSIS);
		return R.ok();
	}

	/** 指引单、条码 */
	private void runBeanOneQueue(List<String> paIds, String acceptSocketUserId, CommRequstPathEnum commRequstPathEnum) {
		//创建队列
		JQueue<PrintRequestObject> jQueue = new JQueue<>();

		PrintRequestObject object = null;
		for (String paId : paIds) {
			object = new PrintRequestObject();
			//获取HttpServer 服务Bean
			HttpServer.BeanOne beanOne = new HttpServer.BeanOne();
			beanOne.init();
			beanOne.setPatientId(paId);
			object.setParamMap(BeanUtil.beanToMap(beanOne));
			object.setCommType(commRequstPathEnum.getComm());
			object.setRequestMapping(commRequstPathEnum.getRequstPath());
			object.setUserName(acceptSocketUserId);
			object.setHttpMethod(HttpMethod.GET);
			jQueue.add(object);
		}
		//启动线程
		runQueue(jQueue);
	}

	private void runPersonReportQueue(List<String> paIds, String acceptSocketUserId,
			CommRequstPathEnum commRequstPathEnum, String reportName) {
		//创建队列
		JQueue<PrintRequestObject> jQueue = new JQueue<>();

		PrintRequestObject object = null;
		for (String paId : paIds) {
			object = new PrintRequestObject();
			//获取HttpServer
			HttpServer.PersonReport personReport = new HttpServer.PersonReport();
			if (StringUtils.isNotEmpty(reportName)) {
				personReport = getPersonReport(paId, reportName);
			} else {
				personReport.initInfo();
				personReport.setPatientId(paId);
			}
			object.setParamMap(BeanUtil.beanToMap(personReport));
			object.setCommType(commRequstPathEnum.getComm());
			object.setRequestMapping(commRequstPathEnum.getRequstPath());
			object.setUserName(acceptSocketUserId);
			jQueue.add(object);
		}

		//启动线程
		runQueue(jQueue);
	}

	private void runGetItemReportQueue(List<String> paIds, String acceptSocketUserId,
			CommRequstPathEnum commRequstPathEnum) {
		//创建队列
		JQueue<PrintRequestObject> jQueue = new JQueue<>();

		PrintRequestObject object = null;
		for (String paId : paIds) {
			object = new PrintRequestObject();
			//获取HttpServer
			HttpServer.PersonReport personReport = new HttpServer.PersonReport();
			personReport.initInfo();
			personReport.setPatientId(paId);
			object.setParamMap(BeanUtil.beanToMap(personReport));
			object.setCommType(commRequstPathEnum.getComm());
			object.setRequestMapping(commRequstPathEnum.getRequstPath());
			object.setUserName(acceptSocketUserId);
			jQueue.add(object);
		}
		//启动线程
		runQueue(jQueue);
	}

	private void runAppointInfoQueue(List<String> appointIds, String acceptSocketUserId,
			CommRequstPathEnum commRequstPathEnum) {
		//创建队列
		JQueue<PrintRequestObject> jQueue = new JQueue<>();

		PrintRequestObject object = null;
		for (String appointId : appointIds) {
			object = new PrintRequestObject();
			//获取HttpServer
			HttpServer.AppointInfo appointInfo = new HttpServer.AppointInfo();
			appointInfo.init();
			appointInfo.setAppointId(appointId);
			appointInfo.setFileType(1);
			object.setParamMap(BeanUtil.beanToMap(appointInfo));
			object.setCommType(commRequstPathEnum.getComm());
			object.setRequestMapping(commRequstPathEnum.getRequstPath());
			object.setUserName(acceptSocketUserId);
			jQueue.add(object);
		}
		//启动线程
		runQueue(jQueue);
	}

	private void runCompanyReportQueue(List<String> reportIds, String sicknessIds, String acceptSocketUserId,
			CommRequstPathEnum commRequstPathEnum) {
		//创建队列
		JQueue<PrintRequestObject> jQueue = new JQueue<>();

		PrintRequestObject object = null;
		for (String reportId : reportIds) {
			object = new PrintRequestObject();
			HttpServer.CompanyAnalysisReport companyAnalysisReport = new HttpServer.CompanyAnalysisReport();
			companyAnalysisReport.setReportId(reportId);
			companyAnalysisReport.setSicknessIds(sicknessIds);
			BReportConfig reportConfig = reportConfigService.getBReportConfig("单位健康报告", 1, 2);
			if (StrUtil.isNotBlank(reportConfig.getReportName())) {
				companyAnalysisReport.setConfig(reportConfig);
			} else {
				companyAnalysisReport.init();
			}
			object.setParamMap(BeanUtil.beanToMap(companyAnalysisReport));
			object.setCommType(commRequstPathEnum.getComm());
			object.setRequestMapping(commRequstPathEnum.getRequstPath());
			object.setUserName(acceptSocketUserId);
			jQueue.add(object);
		}
		//启动线程
		runQueue(jQueue);
	}

	/**
	 * 启动线程 执行 JQueue
	 * @param jQueue
	 */
	private void runQueue(JQueue<PrintRequestObject> jQueue) {
		//启动线程
		new Thread(() -> {
			try {
				jQueueHandle.generatePDF(jQueue);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
	}

	private HttpServer.PersonReport getPersonReport(String paId, String reportName) {
		HttpServer.PersonReport personReport = new HttpServer.PersonReport();
		personReport.setPatientId(paId);
		TPatient patient = patientService.getPatientInfo(paId);
		Integer peTypeGroupId = peTypeHDService.getPeTypeGroupId(patient.getPeTypeHdId());
		Integer reportType = 1;
		BReportConfig reportConfig;
		switch (reportName) {
			case "个人健康体检报告":
				reportConfig = reportConfigService.getBReportConfig("健康体检报告", peTypeGroupId, reportType);
				if (StrUtil.isNotBlank(reportConfig.getReportName())) {
					personReport.setConfig(reportConfig);
				} else {
					personReport.initHealth();
				}
				break;
			case "个人精简报告":
				reportConfig = reportConfigService.getBReportConfig("个人精简报告", peTypeGroupId, reportType);
				if (StrUtil.isNotBlank(reportConfig.getReportName())) {
					personReport.setConfig(reportConfig);
				} else {
					personReport.initHealthLite();
				}
				break;
			case "职业健康检查表":
				reportConfig = reportConfigService.getBReportConfig("职业体检报告", peTypeGroupId, reportType);
				if (StrUtil.isNotBlank(reportConfig.getReportName())) {
					personReport.setConfig(reportConfig);
				} else {
					personReport.initOccupation();
				}
				break;
			case "职业健康检查通知单":
				reportConfig = reportConfigService.getBReportConfig("健康检查通知单", peTypeGroupId, reportType);
				if (StrUtil.isNotBlank(reportConfig.getReportName())) {
					personReport.setConfig(reportConfig);
				} else {
					personReport.initOccupationNotice();
				}
				break;
			case "儿童健康体检报告":
				reportConfig = reportConfigService.getBReportConfig("儿童健康体检报告", peTypeGroupId, reportType);
				if (StrUtil.isNotBlank(reportConfig.getReportName())) {
					personReport.setConfig(reportConfig);
				} else {
					personReport.childHealth();
				}
				break;
			case "从业预防性体检报告":
				reportConfig = reportConfigService.getBReportConfig("从业体检报告", peTypeGroupId, reportType);
				if (StrUtil.isNotBlank(reportConfig.getReportName())) {
					personReport.setConfig(reportConfig);
				} else {
					personReport.practitionerPrevention();
				}
				break;
			case "健康证":
				//				return healthCertificateReport(paId);
			default:
				personReport.initHealth();
				break;
		}
		return personReport;
	}


}
