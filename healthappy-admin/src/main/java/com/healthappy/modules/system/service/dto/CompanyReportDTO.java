package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.*;
import lombok.Data;

import java.io.Serializable;

/**
 * 单位报告管理列表对象
 * @author Jevany
 * @date 2022/2/17 15:45
 */
@Data
public class CompanyReportDTO implements Serializable {
    /** 报告编号 */
    @ApiModelProperty("报告编号")
    private String id;

    /** 单位名称 */
    @ApiModelProperty("单位名称")
    private String companyName;

    /** 报告名称 */
    @ApiModelProperty("报告名称")
    private String reportName;

    /** 编制时间 */
    @ApiModelProperty("编制时间")
    private String compileTime;

    /** 编制人员 */
    @ApiModelProperty("编制人员")
    private String compileDoctor;

    /** 审核时间 */
    @ApiModelProperty("审核时间")
    private String verifyTime;

    /** 审核人员 */
    @ApiModelProperty("审核人员")
    private String verifyDoctor;

    /** 是否审核（0未审核，1已审核） */
    @ApiModelProperty("是否审核（0未审核，1已审核）")
    private Integer isVerify;
}