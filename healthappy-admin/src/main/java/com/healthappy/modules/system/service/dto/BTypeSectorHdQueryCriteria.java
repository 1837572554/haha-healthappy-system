package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import lombok.Data;

@Data
public class BTypeSectorHdQueryCriteria {

    @Query(type = Query.Type.INNER_LIKE)
    private String name;
}
