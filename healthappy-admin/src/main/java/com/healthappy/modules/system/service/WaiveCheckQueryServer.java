package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TPatient;
import com.healthappy.modules.system.service.dto.DataQueryCriteria;
import com.healthappy.modules.system.service.dto.DataQueryDto;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface WaiveCheckQueryServer  extends BaseService<TPatient> {
    List<DataQueryDto> queryAll(DataQueryCriteria criteria);

    Map<String, Object> queryAll(DataQueryCriteria criteria, Pageable pageable);

    void download(DataQueryCriteria criteria, HttpServletResponse response) throws IOException;
}
