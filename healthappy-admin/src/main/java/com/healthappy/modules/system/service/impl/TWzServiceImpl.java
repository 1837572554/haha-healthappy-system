package com.healthappy.modules.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.TRecord;
import com.healthappy.modules.system.domain.TWz;
import com.healthappy.modules.system.service.TRecordService;
import com.healthappy.modules.system.service.TWzService;
import com.healthappy.modules.system.service.dto.TRecordDto;
import com.healthappy.modules.system.service.dto.TRecordQueryCriteria;
import com.healthappy.modules.system.service.dto.TWzDto;
import com.healthappy.modules.system.service.dto.TWzQueryCriteria;
import com.healthappy.modules.system.service.mapper.TRecordMapper;
import com.healthappy.modules.system.service.mapper.TWzMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @description 职业史表
 * @author sjc
 * @date 2021-12-15
 */
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class TWzServiceImpl extends BaseServiceImpl<TWzMapper, TWz> implements TWzService {

    private final IGenerator generator;

    /**
     * 根据体检号获取对应的问诊信息
     * @param paId
     * @return
     */
    @Override
    public TWz query(String paId) {
        return baseMapper.selectOne(new LambdaQueryWrapper<TWz>().eq(TWz::getPaId,paId));
    }

    /**
     * 根据体检号删除问诊信息
     * @param paId
     * @return
     */
    @Override
    public boolean remove(String paId) {
        return baseMapper.delete(new LambdaQueryWrapper<TWz>().eq(TWz::getPaId,paId))>0;
    }
}
