package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.IdcardUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.enums.PeTypeGroupEnum;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.TPatientBaseinfo;
import com.healthappy.modules.system.service.BPeTypeHDService;
import com.healthappy.modules.system.service.TPatientBaseinfoService;
import com.healthappy.modules.system.service.dto.TPatientBaseinfoDto;
import com.healthappy.modules.system.service.dto.TPatientBaseinfoQueryCriteria;
import com.healthappy.modules.system.service.dto.TPatientBaseinfoSaveDtoDto;
import com.healthappy.modules.system.service.mapper.TPatientBaseinfoMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * @author Jevany
 * @date 2022/1/20 17:05
 * @desc
 */
@AllArgsConstructor
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class TPatientBaseinfoServiceImpl extends BaseServiceImpl<TPatientBaseinfoMapper, TPatientBaseinfo> implements TPatientBaseinfoService {

    private final BPeTypeHDService bPeTypeHDService;

    @Override
    public TPatientBaseinfo getBaseinfo(TPatientBaseinfoQueryCriteria criteria) {
        TPatientBaseinfo baseinfo = this.getOne(QueryHelpPlus.getPredicate(TPatientBaseinfo.class, criteria));
        if (!Optional.ofNullable(baseinfo).isPresent()) {
            return new TPatientBaseinfo();
        }
        return baseinfo;
    }

    @Override
    public Long saveOrUpdateBaseinfo(TPatientBaseinfoDto dto) {

        if(StrUtil.isBlank(dto.getIdNo())) {
            return 0L;
        }

        // 验证身份证
        if (ObjectUtil.isNotNull(dto.getPeTypeHdId())) {
            Integer peTypeGroupId = bPeTypeHDService.getPeTypeGroupId(dto.getPeTypeHdId());
            //传了体检分类，且不是儿童体检、健康体检的则需要进行身份证号验证
            if (!PeTypeGroupEnum.CHILD.getValue().equals(peTypeGroupId) && !PeTypeGroupEnum.HEALTH.getValue().equals(peTypeGroupId)
                    && !IdcardUtil.isValidCard(dto.getIdNo())) {
                throw new BadRequestException("身份证验证错误");
            }
        }

        //判断新增修改
        if (ObjectUtil.isNull(dto.getPNo())) {
            //新增
            //检测身份证号是否已经存在
            if (this.count(Wrappers.<TPatientBaseinfo>lambdaQuery().eq(TPatientBaseinfo::getIdNo, dto.getIdNo())) > 0) {
                throw new BadRequestException("身份证号已存在档案号");
            }
            TPatientBaseinfo baseinfo = BeanUtil.copyProperties(dto, TPatientBaseinfo.class);
            baseinfo.setBirthday(IdcardUtil.getBirthDate(dto.getIdNo()));
            baseinfo.setSex(IdcardUtil.getGenderByIdCard(dto.getIdNo()) == 1 ? 1 : 2);
            this.save(baseinfo);
            return baseinfo.getPNo();
        } else {
            //修改
            //检测档案号是否存在
            if (this.count(Wrappers.<TPatientBaseinfo>lambdaQuery().eq(TPatientBaseinfo::getPNo, dto.getPNo())) > 0) {
                if (this.count(Wrappers.<TPatientBaseinfo>lambdaQuery().eq(TPatientBaseinfo::getIdNo, dto.getIdNo()).ne(TPatientBaseinfo::getPNo, dto.getPNo())) > 0) {
                    throw new BadRequestException("身份证号已存在档案号");
                }
                TPatientBaseinfo baseinfo = BeanUtil.copyProperties(dto, TPatientBaseinfo.class);
                baseinfo.setBirthday(IdcardUtil.getBirthDate(dto.getIdNo()));
                baseinfo.setSex(IdcardUtil.getGenderByIdCard(dto.getIdNo()) == 1 ? 1 : 2);
                this.updateById(baseinfo);
                return dto.getPNo();
            } else {
                throw new BadRequestException("档案号不存在");
            }
        }
    }

    @Override
    public Long getPNo(TPatientBaseinfoSaveDtoDto dto) {
        if (StrUtil.isBlank(dto.getIdNo())) {
            return 0L;
        }
        if (ObjectUtil.isNotNull(dto.getPeTypeHdId())) {
            Integer peTypeGroupId = bPeTypeHDService.getPeTypeGroupId(dto.getPeTypeHdId());
            //传了体检分类，且不是儿童体检、健康体检的则需要进行身份证号验证
            if (!PeTypeGroupEnum.CHILD.getValue().equals(peTypeGroupId) && !PeTypeGroupEnum.HEALTH.getValue().equals(peTypeGroupId)
                    && !IdcardUtil.isValidCard(dto.getIdNo())) {
                throw new BadRequestException("身份证验证错误");
            }
        }

        //根据身份证号查询档案
        TPatientBaseinfo baseinfo = this.getOne(Wrappers.<TPatientBaseinfo>lambdaQuery().eq(TPatientBaseinfo::getIdNo, dto.getIdNo()));

        if (Optional.ofNullable(baseinfo).isPresent()) {
            //如果存在则返回档案号
            return baseinfo.getPNo();
        } else {
            //如果不存在则保存后返回档案号
            baseinfo = BeanUtil.copyProperties(dto, TPatientBaseinfo.class);
            baseinfo.setBirthday(IdcardUtil.getBirthDate(dto.getIdNo()));
            baseinfo.setSex(IdcardUtil.getGenderByIdCard(dto.getIdNo()) == 1 ? 1 : 2);
            this.save(baseinfo);
            return baseinfo.getPNo();
        }
    }


}