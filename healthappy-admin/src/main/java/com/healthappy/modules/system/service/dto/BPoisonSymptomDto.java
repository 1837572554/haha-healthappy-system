package com.healthappy.modules.system.service.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 毒害对应症状细表
 * @author sjc
 * @date 2021-08-10
 */
@Data
public class BPoisonSymptomDto implements Serializable {

    /**
     * 症状id
     */
    @ApiModelProperty("症状id")
    private Long symptomId;
    /**
     * 症状名称
     */
    @ApiModelProperty("症状名称")
    private String symptomName;

    /**
     * 体检类别
     */
    @ApiModelProperty("type")
    private String type;

    /**
     * 危害因素id
     */
    @ApiModelProperty("危害因素id")
    private String poisonId;


}
