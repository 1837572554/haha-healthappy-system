package com.healthappy.modules.system.service.dto;

import com.healthappy.modules.system.domain.BTypeGetResult;
import lombok.Data;

/**
 * @description 结果获取方式
 * @author sjc
 * @date 2021-08-3
 */
@Data
public class BTypeGetResultDto extends BTypeGetResult {



}
