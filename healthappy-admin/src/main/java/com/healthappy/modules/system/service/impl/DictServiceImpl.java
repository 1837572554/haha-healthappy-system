package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.Dict;
import com.healthappy.modules.system.domain.DictDetail;
import com.healthappy.modules.system.domain.vo.DictEnumVo;
import com.healthappy.modules.system.service.DictDetailService;
import com.healthappy.modules.system.service.DictService;
import com.healthappy.modules.system.service.dto.DictDto;
import com.healthappy.modules.system.service.dto.DictQueryCriteria;
import com.healthappy.modules.system.service.dto.DictTreeDto;
import com.healthappy.modules.system.service.dto.DictTreeNodeDto;
import com.healthappy.modules.system.service.mapper.DictMapper;
import com.healthappy.utils.CacheConstant;
import com.healthappy.utils.FileUtil;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

/**
 * @author hupeng
 * @date 2020-05-14
 */
@Service
@AllArgsConstructor
@CacheConfig(cacheNames = {CacheConstant.DICT})
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class DictServiceImpl extends BaseServiceImpl<DictMapper, Dict> implements DictService {

	private final IGenerator generator;
	private final DictDetailService dictDetailService;

	@Override
	@Cacheable
	public Map<String, Object> queryAll(DictQueryCriteria criteria, Pageable pageable) {
		getPage(pageable);
		PageInfo<Dict> page = new PageInfo<>(queryAll(criteria));
		Map<String, Object> map = new LinkedHashMap<>(2);
		map.put("content", generator.convert(page.getList(), DictDto.class));
		map.put("totalElements", page.getTotal());
		return map;
	}


	@Override
	@Cacheable
	public List<Dict> queryAll(DictQueryCriteria criteria) {
		return baseMapper.selectList(QueryHelpPlus.getPredicate(Dict.class, criteria));
	}


	@Override
	public void download(List<DictDto> all, HttpServletResponse response) throws IOException {
		List<Map<String, Object>> list = new ArrayList<>();
		for (DictDto dict : all) {
			Map<String, Object> map = new LinkedHashMap<>();
			map.put("字典名称", dict.getName());
			map.put("描述", dict.getRemark());
			map.put("创建日期", dict.getCreateTime());
			list.add(map);
		}
		FileUtil.downloadExcel(list, response);
	}

	/**
	 * 获取字典数据  ，首先从缓存中获取
	 * @return
	 */
	@Override
	@Cacheable(value = "dict:null", key = "0")
	public List<DictEnumVo> dictConvertEnum() {
		try {
			return baseMapper.dictConvertEnum();
		} catch (Exception ex) {
			//如果报错就返回空的，方式给前端显示
			return new ArrayList<>();
		}
	}

	/**
	 * 获取字典数据  ，不管有没有缓存 都从数据库中获取 然后跟新缓存
	 * @return
	 */
	@Override
	@CachePut(value = "dict:null", key = "0")
	public List<DictEnumVo> reload() {
		try {
			return baseMapper.reLoad();
		} catch (Exception ex) {
			//如果报错就返回空的，方式给前端显示
			return new ArrayList<>();
		}

	}


	@Override
	@Cacheable
	public List<DictTreeDto> tree() {
		List<Dict> dictList = this.list();
		if (CollUtil.isEmpty(dictList)) {
			return null;
		}
		List<DictDetail> dictDetailList = dictDetailService.getAll();
		return dictList.stream().map(dict -> {
			DictTreeDto treeDto = BeanUtil.copyProperties(dict, DictTreeDto.class);
			treeDto.setTreeNodes(
					dictDetailList.stream().filter(dictDetail -> dictDetail.getDictId().equals(dict.getId()))
							.sorted(Comparator.comparing(DictDetail::getSort))
							.map(dictDetail -> BeanUtil.copyProperties(dictDetail, DictTreeNodeDto.class))
							.collect(Collectors.toList())

			);
			return treeDto;
		}).collect(Collectors.toList());
	}
}
