package com.healthappy.modules.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BPetypeInstitution;
import com.healthappy.modules.system.service.BPetypeInstitutionService;
import com.healthappy.modules.system.service.dto.BPetypeInstitutionCriteria;
import com.healthappy.modules.system.service.mapper.BPetypeInstitutionMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BPetypeInstitutionServiceImpl extends BaseServiceImpl<BPetypeInstitutionMapper, BPetypeInstitution> implements BPetypeInstitutionService {


    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BPetypeInstitutionCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BPetypeInstitution> page = new PageInfo<BPetypeInstitution>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", page.getList());
        map.put("totalElements", page.getTotal());
        return map;
    }

    @Override
    public List<BPetypeInstitution> queryAll(BPetypeInstitutionCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BPetypeInstitution.class, criteria));
    }
}
