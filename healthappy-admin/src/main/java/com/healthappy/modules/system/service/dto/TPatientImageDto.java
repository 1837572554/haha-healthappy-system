package com.healthappy.modules.system.service.dto;/**
 * @author Jevany
 * @date 2021/12/2 9:54
 * @desc
 */

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @desc: 体检图片
 * @author: YJ
 * @date: 2021-12-02 09:54
 **/
@Data
public class TPatientImageDto implements Serializable {
    /** 体检编号 */
    @ApiModelProperty(value = "体检编号")
    private String paId;

    /** 组合项编号 */
    @ApiModelProperty(value = "组合项编号")
    private String groupId;

    /** 小项编号，可为空 */
    @ApiModelProperty(value = "小项编号，可为空")
    private String itemId;

    /** 图片编号id-长度20 */
    @ApiModelProperty(value = "主键id-长度20，上传成功才会有数据",hidden = true)
    private String id;

    /** 文件名称（上传时的文件名） */
    @ApiModelProperty(value ="文件名称（上传时的文件名）",hidden = true)
    private String fileName;

    /** 图片Url */
    @ApiModelProperty(value ="图片Url，如失败会在此处填失败原因",hidden = true)
    private String url;
}
