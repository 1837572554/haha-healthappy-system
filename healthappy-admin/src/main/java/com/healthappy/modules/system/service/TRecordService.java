package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TRecord;
import com.healthappy.modules.system.service.dto.TRecordQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @description 职业史表
 * @author sjc
 * @date 2021-12-15
 */
public interface TRecordService extends BaseService<TRecord> {


    Map<String, Object> queryAll(TRecordQueryCriteria criteria, Pageable pageable);

    List<TRecord> queryAll(TRecordQueryCriteria criteria);
}
