package com.healthappy.modules.system.rest.basic;

import cn.hutool.core.bean.BeanUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BCommentWz;
import com.healthappy.modules.system.domain.vo.BCommentWzVo;
import com.healthappy.modules.system.service.BCommentWzService;
import com.healthappy.modules.system.service.dto.BCommentWzQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 既往史模板设置 控制器
 * @author FGQ
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "基础管理：既往史模板设置")
@RestController
@AllArgsConstructor
@RequestMapping("/BCommentWz")
public class BCommentWzController {

    private final BCommentWzService bCommentWzService;

    @Log("查询既往史模板")
    @ApiOperation("查询既往史模板，权限码：BCommentWz:list")
    @GetMapping
    public R list(BCommentWzQueryCriteria criteria, Pageable pageable) {
        return R.ok(bCommentWzService.queryAll(criteria, pageable));
    }

    @Log("新增|修改既往史模板")
    @ApiOperation("新增|修改既往史模板，权限码：BCommentWz:add")
    @PostMapping
    @PreAuthorize("@el.check('BCommentWz:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BCommentWzVo resources) {
        return R.ok(bCommentWzService.saveOrUpdate(BeanUtil.copyProperties(resources, BCommentWz.class)));
    }

    @Log("删除既往史模板")
    @ApiOperation("删除既往史模板，权限码：BCommentWz:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('BCommentWz:delete')")
    @Klock
    public R remove(@RequestBody Set<Integer> ids) {
        return R.ok(bCommentWzService.removeByIds(ids));
    }
}
