package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author sqoy
 * @date 2021/12/17 17:47
 * @desc
 */
@Data
public class AloneReportPrintDto implements Serializable {
    /**
     * 体检号
     */
    @ApiModelProperty("体检号")
    private String id;

    /**
     * 体检者姓名
     */
    @ApiModelProperty("体检者姓名")
    private String PaName;

    /**
     * 体检编号
     */
    @ApiModelProperty("性别")
    private String sex;

}

