package com.healthappy.modules.system.rest.common;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BTypeSample;
import com.healthappy.modules.system.domain.vo.BTypeSampleVo;
import com.healthappy.modules.system.service.BTypeSampleService;
import com.healthappy.modules.system.service.dto.BTypeSampleQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description 标本类型 控制器
 * @author sjc
 * @date 2021-08-3
 */
@Slf4j
@Api(tags = "默认基础设置：标本类型")
@RestController
@AllArgsConstructor
@RequestMapping("/BTypeSample")
public class BTypeSampleController {

    private final BTypeSampleService bTypeSampleService;

    @Log("分页查询标本类型数据")
    @ApiOperation("分页查询标本类型数据，权限码：BTypeSample:list")
    @GetMapping
    public R listPage(BTypeSampleQueryCriteria criteria, Pageable pageable) {
        return R.ok(bTypeSampleService.queryAll(criteria, pageable));
    }

    @Log("新增|修改标本类型")
    @ApiOperation("新增|修改标本类型，权限码：admin")
    @PostMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BTypeSampleVo resources) {
        //如果名称重复
        if(checkBTypeSampleName(resources)){
            return R.error(500,"标本类型名称已经存在");
        }
        BTypeSample bTypeItem = new BTypeSample();
        BeanUtil.copyProperties(resources,bTypeItem);
        if(bTypeSampleService.saveOrUpdate(bTypeItem))
            return R.ok();
        return R.error(500,"方法异常");
    }

    @Log("删除项目类型")
    @ApiOperation("删除项目类型，权限码：admin")
    @DeleteMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R remove(@RequestParam("id") Long id) {
        if(bTypeSampleService.deleteById(id))
            return R.ok();
        return R.error(500,"方法异常");
    }

    /**
     * 用于判断名称是否存在
     * @param resources
     * @return
     */
    private Boolean checkBTypeSampleName(BTypeSampleVo resources) {
        //名称相同  id不同的情况下能查询到数据，那么就是名称重复
        return bTypeSampleService.lambdaQuery().eq(BTypeSample::getName,resources.getName())
                .ne(ObjectUtil.isNotNull(resources.getId()),BTypeSample::getId,resources.getId()).count() > 0;

    }
}
