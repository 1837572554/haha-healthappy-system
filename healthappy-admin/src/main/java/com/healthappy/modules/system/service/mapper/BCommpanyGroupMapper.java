package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BCommpanyGroup;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 集团
 * @author FGQ
 * @date 2021-10-15
 */
@Mapper
@Repository
public interface BCommpanyGroupMapper extends CoreMapper<BCommpanyGroup> {


}
