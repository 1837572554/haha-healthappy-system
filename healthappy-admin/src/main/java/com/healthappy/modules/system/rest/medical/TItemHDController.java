package com.healthappy.modules.system.rest.medical;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.TItemDT;
import com.healthappy.modules.system.domain.TItemHD;
import com.healthappy.modules.system.domain.TPatient;
import com.healthappy.modules.system.domain.vo.TItemHDVo;
import com.healthappy.modules.system.service.TItemDTService;
import com.healthappy.modules.system.service.TItemHDService;
import com.healthappy.modules.system.service.TPatientService;
import com.healthappy.modules.system.service.dto.TItemHDQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @description 体检人员项目表 控制器
 * @author shaojiachuan
 * @date 2021-06-29
 */
@Validated
@Slf4j
@Api(tags = "体检管理：体检人员项目")
@RestController
@AllArgsConstructor
@RequestMapping("/TItemHD")
public class TItemHDController {

    private final TItemHDService tItemHDService;
    private final TItemDTService tItemDTService;
    private final TPatientService tPatientService;
    private final IGenerator generator;

    @Log("查询体检人员数据")
    @ApiOperation("查询体检人员数据，权限码：ProjectCheck:list")
    @GetMapping("/paGroups")
    @PreAuthorize("@el.check('ProjectCheck:list')")
    public R list(TItemHDQueryCriteria criteria) {
        return R.ok(tItemHDService.queryAll(criteria));
    }

    @Log("分页查询体检人员数据")
    @ApiOperation("分页查询体检人员数据，权限码：TItemHD:list")
    @GetMapping
    @PreAuthorize("@el.check('ProjectCheck:list')")
    public R listPage(TItemHDQueryCriteria criteria, Pageable pageable) {
        return R.ok(tItemHDService.queryAll(criteria, pageable));
    }

    @Log("新增|修改体检人员项目表")
    @ApiOperation("新增|修改体检人员项目表，权限码：TItemHD:add")
    @PostMapping
    @PreAuthorize("@el.check('TItemHD:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody TItemHDVo resources) {
        return R.ok(tItemHDService.saveOrUpdate( BeanUtil.copyProperties(resources,TItemHD.class)));
    }

    @Log("批量新增|修改体检人员项目表")
    @ApiOperation("批量新增|修改体检人员项目表，权限码：TItemHD:adds")
    @PostMapping("/list")
    @PreAuthorize("@el.check('TItemHD:adds')")
    @Klock
    public R saveOrUpdate(@Valid @RequestBody List<TItemHDVo> resources) {
        if(CollUtil.isEmpty(resources)){
            throw new BadRequestException("参数不能为空");
        }
        return R.ok(tItemHDService.saveOrUpdate(generator.convert(resources,TItemHD.class)));
    }

    @Log("根据体检号和组合项目id进行删除")
    @ApiOperation("根据体检号和组合项目id进行删除，权限码：TItemHD:delete")
    @PostMapping("/deleteByPaIdAndGroupId")
    @PreAuthorize("@el.check('TItemHD:delete')")
    @Klock
    public R remove(@RequestParam String paId,@RequestParam String groupId) {
        tItemHDService.deleteByPaIdAndGroupId(paId,groupId);
        return R.ok();
    }



    @Log("根据体检号删除该人员的组合项目")
    @ApiOperation("根据体检号删除该人员的组合项目，权限码：TItemHD:delete")
    @PostMapping("/deleteByPaId")
    @PreAuthorize("@el.check('TItemHD:delete')")
    @Klock
    public R remove(@RequestBody String paId) {
        if(tItemHDService.deleteByPaId(paId)){
            LambdaUpdateWrapper <TItemDT> queryWrapper = new LambdaUpdateWrapper  ();
            queryWrapper.eq(TItemDT::getPaId,paId);
            tItemDTService.remove(queryWrapper);
            return R.ok();
        }
        return R.error(500,"方法异常");
    }



    @Log("批量新增|修改体检人员项目表")
    @ApiOperation("批量新增|修改体检人员项目表，权限码：TItemHD:adds")
    @PostMapping("/saveItems")
    @PreAuthorize("@el.check('TItemHD:adds')")
    @Klock
    public R saveItems(@Validated @RequestBody String paId, @RequestBody List<TItemHDVo> resources) throws Exception {
        if(CollUtil.isEmpty(resources)) {
            String msg="参数不能为空";
            throw new BadRequestException(msg);
        }
        List<TItemHD> list = resources.stream().map(h -> BeanUtil.copyProperties(h, TItemHD.class)).collect(Collectors.toList());
        TPatient tPatient = tPatientService.getById(paId);
        tItemHDService.saveItems(tPatient,list);
        return R.ok();
    }
}
