package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 从业
 */
@Data
@TableName("T_Patient_C")
@ApiModel("T_Patient_C")
@RenewLog
public class TPatientC implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
     * id
     */
    @ApiModelProperty("id")
    private Long id;

    /**
     * 体检编号
     */
    @ApiModelProperty("体检编号")
    private String paId;

    /**
     * 正常/合格
     */
    @ApiModelProperty("正常/合格")
    private String pjQualified;

    /**
     * 异常/不合格
     */
    @ApiModelProperty("异常/不合格")
    private String pjUnqualified;

    /**
     * 职业结论
     */
    @ApiModelProperty("职业结论")
    private String commentC;

    /**
     * 职业建议
     */
    @ApiModelProperty("职业建议")
    private String suggestC;

    /**
     * 修改时间
     */
    @ApiModelProperty("修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(value = "update_time",fill = FieldFill.INSERT_UPDATE)
    private Timestamp updateTime;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Timestamp createTime;

    /** 医疗机构ID(U_Hospital_Info的ID) */
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    @ApiModelProperty(value = "医疗机构ID",hidden = true)
    private String tenantId;
}
