package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BSicknessQueryCriteria {

    @Query(blurry = "name,jp")
    @ApiModelProperty("病种名称|模糊搜索")
    private String name;

    @Query
    @ApiModelProperty("主键")
    private String id;

    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("是否删除 空不限制,0：未删除，1：删除。默认0")
    private String delFlag="0";
}
