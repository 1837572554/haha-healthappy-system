package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TPatientBaseinfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Jevany
 * @date 2022/1/20 17:07
 * @desc 人员档案信息表 数据操作接口
 */
@Mapper
@Repository
public interface TPatientBaseinfoMapper extends CoreMapper<TPatientBaseinfo> {

}
