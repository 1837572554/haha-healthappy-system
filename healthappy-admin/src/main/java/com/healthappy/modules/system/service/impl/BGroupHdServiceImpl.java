package com.healthappy.modules.system.service.impl;

import cn.hutool.core.convert.Convert;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.modules.system.domain.BGroupDT;
import com.healthappy.modules.system.domain.BGroupHDWuJia;
import com.healthappy.modules.system.domain.BGroupHd;
import com.healthappy.modules.system.service.BGroupDTService;
import com.healthappy.modules.system.service.BGroupHDWuJiaService;
import com.healthappy.modules.system.service.BGroupHdService;
import com.healthappy.modules.system.service.dto.*;
import com.healthappy.modules.system.service.mapper.BGroupHdMapper;
import com.healthappy.modules.system.service.wrapper.BGroupWrapper;
import com.healthappy.utils.SecurityUtils;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BGroupHdServiceImpl extends BaseServiceImpl<BGroupHdMapper, BGroupHd> implements BGroupHdService {

    private BGroupDTService bGroupDTService;
    private BGroupHDWuJiaService bGroupHDWuJiaService;

    @Override
    public Map<String, Object> queryAll(BGroupHdQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        return BGroupWrapper.build().pageVO(new PageInfo<BGroupHd>(queryAll(criteria)));
    }

    @Override
    public List<BGroupHd> queryAll(BGroupHdQueryCriteria criteria) {
        //防止没有被调用到，而receiveSex的参数没有被执行？
        criteria.getSex();
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BGroupHdDto.class, criteria));
    }


    @Override
    public boolean removeByIds(Set<String> ids) {
        if (this.lambdaUpdate().in(BGroupHd::getId, ids).remove()) {
            bGroupDTService.lambdaUpdate().in(BGroupDT::getGroupId, ids).remove();
            bGroupHDWuJiaService.lambdaUpdate().in(BGroupHDWuJia::getItemGroupId, ids).remove();
        } else {
            return false;
        }
        return true;
    }

    @Override
    public List<DeptBusinessDTO> listDeptBusiness(String dateStr) {
        String tenantId = SecurityUtils.getTenantId();
        //所有需要统计的数据
        List<DeptGroupDTO> totalList = baseMapper.listDeptBusiness(dateStr, tenantId);
        List<DeptGroupDTO> registerList = baseMapper.listRegister(dateStr, tenantId);
        List<DeptGroupDTO> appointList = baseMapper.listAppoint(dateStr, tenantId);
        List<DeptGroupDTO> appointSignList = baseMapper.listAppointSign(dateStr, tenantId);

        List<DeptBusinessDTO> deptBusinessList = new ArrayList<>();
        LinkedHashMap<DeptBusinessDTO, List<DeptGroupDTO>> collect = totalList.stream().collect(Collectors.groupingBy(d -> new DeptBusinessDTO(d.getDeptId(), d.getDeptName(), new Integer[3], new ArrayList<>()), LinkedHashMap::new, Collectors.toList()));
        for (Map.Entry dept : collect.entrySet()) {
            DeptBusinessDTO deptBusinessDTO = (DeptBusinessDTO) dept.getKey();
            AppointRegisterTodayDTO appointDTO = new AppointRegisterTodayDTO();
            appointDTO.setRegisterNum(Convert.toInt(registerList.stream().filter(r -> deptBusinessDTO.getDeptId().equals(r.getDeptId())).collect(Collectors.toList()).stream().mapToInt(DeptGroupDTO::getCnt).sum()));
            appointDTO.setAppointNum(Convert.toInt(appointList.stream().filter(r -> deptBusinessDTO.getDeptId().equals(r.getDeptId())).collect(Collectors.toList()).stream().mapToInt(DeptGroupDTO::getCnt).sum()));
            appointDTO.setAppointSignNum(Convert.toInt(appointSignList.stream().filter(r -> deptBusinessDTO.getDeptId().equals(r.getDeptId())).collect(Collectors.toList()).stream().mapToInt(DeptGroupDTO::getCnt).sum()));
            deptBusinessDTO.getPersonNumList()[0] = appointDTO.getAppointNum();
            deptBusinessDTO.getPersonNumList()[1] = appointDTO.getAppointSignNum();
            deptBusinessDTO.getPersonNumList()[2] = appointDTO.getRegisterNum() - appointDTO.getAppointSignNum();

            List<DeptBusinessGroupDTO> groupDTOList=new ArrayList<>();
            List<DeptGroupDTO> deptGroupDTOList = (List<DeptGroupDTO>) dept.getValue();
            for (DeptGroupDTO deptGroupDTO : deptGroupDTOList) {
                DeptBusinessGroupDTO groupDTO=new DeptBusinessGroupDTO();
                groupDTO.setGroupName(deptGroupDTO.getGroupName());
                groupDTO.setAppointNum(Convert.toInt(appointList.stream().filter(r -> deptBusinessDTO.getDeptId().equals(r.getDeptId()) && deptBusinessDTO.getDeptId().equals(r.getDeptId())).collect(Collectors.toList()).stream().mapToInt(DeptGroupDTO::getCnt).sum()));
                groupDTO.setSignNum(Convert.toInt(appointSignList.stream().filter(r -> deptBusinessDTO.getDeptId().equals(r.getDeptId()) && deptBusinessDTO.getDeptId().equals(r.getDeptId())).collect(Collectors.toList()).stream().mapToInt(DeptGroupDTO::getCnt).sum()));
                groupDTOList.add(groupDTO);
            }
            deptBusinessDTO.setItemList(groupDTOList);
            deptBusinessList.add(deptBusinessDTO);
        }

        return deptBusinessList;
    }


}
