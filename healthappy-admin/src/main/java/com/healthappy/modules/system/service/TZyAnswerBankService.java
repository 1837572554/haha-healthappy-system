package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TZyAnswerBank;

/**
 * @author Jevany
 * @date 2021/12/8 19:34
 * @desc 体检者问卷答案库服务
 */
public interface TZyAnswerBankService extends BaseService<TZyAnswerBank> {

}
