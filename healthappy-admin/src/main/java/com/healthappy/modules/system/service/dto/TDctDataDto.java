package com.healthappy.modules.system.service.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.healthappy.config.databind.FieldBind;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @desc: 电测听数据Dto
 * @author: YJ
 * @date: 2021-12-02 15:41
 **/
@Data
public class TDctDataDto implements Serializable {
    /** 电测听项类型ID */
    @ApiModelProperty("电测听项类型ID")
    @FieldBind(target = "dctTypeName")
    private Integer dctType;

    /** 电测听项类型名称 */
    @ApiModelProperty("电测听项类型名称")
    @TableField(exist = false)
    private String dctTypeName;

    /** 125频率数值 */
    @ApiModelProperty("125频率数值")
    private String p125;

    /** 250频率数值 */
    @ApiModelProperty("250频率数值")
    private String p250;

    /** 500频率数值 */
    @ApiModelProperty("500频率数值")
    private String p500;

    /** 750频率数值 */
    @ApiModelProperty("750频率数值")
    private String p750;

    /** 1000频率数值 */
    @ApiModelProperty("1000频率数值")
    private String p1000;

    /** 1500频率数值 */
    @ApiModelProperty("1500频率数值")
    private String p1500;

    /** 2000频率数值 */
    @ApiModelProperty("2000频率数值")
    private String p2000;

    /** 3000频率数值 */
    @ApiModelProperty("3000频率数值")
    private String p3000;

    /** 4000频率数值 */
    @ApiModelProperty("4000频率数值")
    private String p4000;

    /** 6000频率数值 */
    @ApiModelProperty("6000频率数值")
    private String p6000;

    /** 8000频率数值 */
    @ApiModelProperty("8000频率数值")
    private String p8000;

    /** 12000频率数值 */
    @ApiModelProperty("12000频率数值")
    private String p12000;

    /** 16000频率数值 */
    @ApiModelProperty("16000频率数值")
    private String p16000;
}
