package com.healthappy.modules.system.service.dto;

import com.healthappy.modules.system.domain.BSicknessRule;
import io.swagger.annotations.*;
import lombok.Data;

@Data
public class BSicknessRuleDto extends BSicknessRule {

    /** 病种名称 */
    @ApiModelProperty("病种名称")
    private String bSicknessName;

    /** 病种建议 */
    @ApiModelProperty("病种建议")
    private String sicknessAdviceName;
}
