package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TDctData;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Jevany
 * @date 2021/12/2 15:50
 * @desc 电测听数据 Mapper
 */
@Repository
@Mapper
public interface TDctDataMapper extends CoreMapper<TDctData> {

}
