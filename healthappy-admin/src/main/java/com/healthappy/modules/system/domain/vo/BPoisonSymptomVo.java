package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @description 毒害对应症状细表
 * @author sjc
 * @date 2021-08-10
 */
@Data
public class BPoisonSymptomVo  {

    /**
     * 症状id
     */
    @ApiModelProperty("症状id")
    private Long symptomId;

    /**
     * 体检类别
     */
    @ApiModelProperty("type")
    private String type;

    /**
     * 危害因素id
     */
    @NotBlank(message = "危害因素id不能为空")
    @ApiModelProperty("危害因素id")
    private String poisonId;


}
