package com.healthappy.modules.system.rest.common;

import com.healthappy.annotation.AnonymousAccess;
import com.healthappy.modules.http.report.HttpFileServer;
import com.healthappy.modules.http.report.HttpServer;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: FGQ
 * @Desc:
 * @Date: Created in 13:34 2022/3/17
 */
@Slf4j
@Api(tags = "测试请求报告")
@RestController
@AllArgsConstructor
@RequestMapping("/http")
public class TestHttpServerController {

    @ApiOperation("条码生成")
    @GetMapping
    @AnonymousAccess
    public R returnSeedId() {
        HttpServer.BeanOne beanOne = new HttpServer.BeanOne();
        beanOne.setPatientId("220302010019");
        beanOne.setFileType(1);
        beanOne.setTenantId("625777");
        return R.ok(HttpServer.getBarcodeReport(beanOne));
    }


    @ApiOperation("文件批量生成ZIP导出")
    @GetMapping("/testZipReport")
    @AnonymousAccess
    public R testZipReport() {
        return R.ok(HttpFileServer.fileZipByUrl("202203211125235767574_758489_220309010009.pdf","202203211145114177340_625777_220321010001(测试01).pdf"));
    }
}
