package com.healthappy.modules.system.rest.medical;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.vo.BarcodeVO;
import com.healthappy.modules.system.domain.vo.TItemHDVo;
import com.healthappy.modules.system.domain.vo.TPatientAddVo;
import com.healthappy.modules.system.service.TPatientService;
import com.healthappy.modules.system.service.dto.MedicalRegistrationQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @description 体检登记 控制器
 * @author FGQ
 * @date 2021-09-06
 */
@Slf4j
@Api(tags = "体检管理：体检登记")
@RestController
@AllArgsConstructor
@RequestMapping("/medicalRegistration")
public class MedicalRegistrationController {

	private final TPatientService tPatientService;

	@Log("列表")
	@ApiOperation("列表，权限码：MedicalRegistration:list")
	@GetMapping("/list")
	@PreAuthorize("@el.check('MedicalRegistration:list')")
	public R list(MedicalRegistrationQueryCriteria criteria, Pageable pageable) {
		return R.ok(tPatientService.queryRegistrationAll(criteria, pageable));
	}

	@Log("列表精简")
	@ApiOperation("列表精简，权限码：MedicalRegistration:list")
	@GetMapping("/listLite")
	@PreAuthorize("@el.check('MedicalRegistration:list')")
	public R listLite(MedicalRegistrationQueryCriteria criteria, Pageable pageable) {
		return R.ok(tPatientService.queryRegistrationAllLite(criteria, pageable));
	}

	@Log("查询人员详细数据")
	@ApiOperation("查询人员详细数据，权限码：MedicalRegistration:list")
	@GetMapping("/getPatient")
	@PreAuthorize("@el.check('MedicalRegistration:list')")
	public R getPatient(@RequestParam String paId) {
		if (StrUtil.isBlank(paId)) {
			return R.error("体检号不能为空");
		}
		return R.ok(tPatientService.getPatient(paId));
	}


	@Log("新增|修改")
	@ApiOperation("新增|修改，权限码：MedicalRegistration:add")
	@PostMapping
	@PreAuthorize("@el.check('MedicalRegistration:add')")
	@Klock
	public R saveOrUpdate(@Validated @RequestBody TPatientAddVo resources) {
		if (CollUtil.isNotEmpty(resources.getItems())) {
			for (TItemHDVo item : resources.getItems()) {
				if (StrUtil.isBlank(item.getPayType())) {
					return R.error(item.getGroupName() + "项目未设置支付类型");
				}
			}
		}
		return R.ok(tPatientService.installVo(resources));
	}

	//    @Log("条码生成测试")
	//    @ApiOperation("条码生成测试，权限码：MedicalRegistration:add")
	//    @GetMapping("/testBarcode")
	//    @PreAuthorize("@el.check('MedicalRegistration:add')")
	//    @Klock
	//    public R testBarcode(@RequestParam String paId) {
	//        tPatientService.generateBarcode(paId);
	//        return R.ok();
	//    }


	@Log("删除")
	@ApiOperation("删除，权限码：MedicalRegistration:delete")
	@DeleteMapping
	@PreAuthorize("@el.check('MedicalRegistration:delete')")
	@Klock
	public R delete(@RequestParam("paId") String paId) {
		return R.ok(tPatientService.deleteById(paId));
	}

	@Log("获取体检时间类型")
	@ApiOperation("获取体检时间类型，权限码：MedicalRegistration:list")
	@GetMapping("/getPeDateTypes")
	public R getPeDateTypes(String pagePath) {
		return R.ok(tPatientService.getPeDateTypes(pagePath));
	}


	@Log("获取体检者条码列表")
	@ApiOperation("获取体检者条码列表，权限码：MedicalRegistration:list")
	@GetMapping("/listBarcodeByPaId")
	@ApiResponses(value = {@ApiResponse(code = 200, message = "体检者条码列表", response = BarcodeVO.class)})
	public R listBarcodeByPaId(String paId) {
		return R.ok(tPatientService.getBarcodeByPaId(paId));
	}
}
