package com.healthappy.modules.system.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@ApiModel("预约数据")
@Data
public class TAppointDto  implements Serializable {
    @ApiModelProperty("预约号")
    private String id;

    @ApiModelProperty("人员名称")
    private String name;

    @ApiModelProperty("性别")
    private String sex;

    private String sexText;

    @ApiModelProperty("年龄")
    private Integer age;

    @ApiModelProperty("身份证号")
    private String idNo;

    @ApiModelProperty("婚姻状况")
    private String marital;

    private String maritalText;

    @ApiModelProperty("手机号")
    private String mobile;

    @ApiModelProperty("部门名称")
    private String departmentName;

    @ApiModelProperty("工号")
    private String code;

    @ApiModelProperty("工龄")
    private String workYears;

    /** 工种编号-可能匹配不上则为空 */
    @ApiModelProperty("工种编号-可能匹配不上则为空")
    private String job;

    /**
     * 工种名称
     */
    @ApiModelProperty("工种名称")
    private String jobName;

    @ApiModelProperty("毒害种类")
    private String poisonType;

    @ApiModelProperty("毒害名称")
    private String poisonTypeName;

    @ApiModelProperty("套餐名称")
    private String packageName;

    @ApiModelProperty("附加套餐")
    private String addPackage;

    @ApiModelProperty("体检分类")
    private String peType;

    @ApiModelProperty("体检分类名称")
    private String peTypeName;

    @ApiModelProperty("体检类别")
    private String type;

    @ApiModelProperty("体检类别名称")
    private String typeName;

    @ApiModelProperty("社保号")
    private String ssNo;

    @ApiModelProperty("单位名称")
    private String companyName;

    @ApiModelProperty("单位id")
    private String companyId;

    @ApiModelProperty("预约类型")
    private String appointType;

    @ApiModelProperty("备注")
    private String mark;

    @ApiModelProperty("分组id")
    private String deptGroupId;

    @ApiModelProperty("分组名称")
    private String deptGroupName;

    @ApiModelProperty("人员来源")
    private String personnelSource;

    @ApiModelProperty("任务来源")
    private String charsi;

    @ApiModelProperty("结算归口")
    private String settlement;

    @ApiModelProperty("特殊减项目")
    private String subtractGroupItem;

    @ApiModelProperty("添加项目组合项编号")
    private String addGroupItemId;

    @ApiModelProperty("添加套餐编号")
    private String addPackageId;

    @ApiModelProperty("附加组合项目")
    private String addGroupItem;

    @ApiModelProperty("附加套餐ID")
    private String packageId;

    @ApiModelProperty("签到时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp signDate;

    @ApiModelProperty("总工龄")
    private String totalYears;

    @ApiModelProperty("预约日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp appointDate;

    @ApiModelProperty("预约时间开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp appointDateStart;

    @ApiModelProperty("预约时间结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp appointDateEnd;

    @ApiModelProperty("导入时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp updateTime;

    @ApiModelProperty("部门ID")
    private String departmentId;
}
