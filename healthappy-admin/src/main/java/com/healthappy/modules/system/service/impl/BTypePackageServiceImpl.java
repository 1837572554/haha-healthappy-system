package com.healthappy.modules.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BSetUp;
import com.healthappy.modules.system.domain.BTypePackage;
import com.healthappy.modules.system.service.BTypePackageService;
import com.healthappy.modules.system.service.dto.BSetUpDto;
import com.healthappy.modules.system.service.dto.BSetUpQueryCriteria;
import com.healthappy.modules.system.service.dto.BTypePackageDto;
import com.healthappy.modules.system.service.dto.BTypePackageQueryCriteria;
import com.healthappy.modules.system.service.mapper.BTypePackageMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @description 套餐类型
 * @author sjc
 * @date 2021-08-24
 */
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BTypePackageServiceImpl extends BaseServiceImpl<BTypePackageMapper, BTypePackage> implements BTypePackageService {


    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BTypePackageQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BTypePackage> page = new PageInfo<BTypePackage>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("content", generator.convert(page.getList(), BTypePackageDto.class));
        map.put("totalElements", page.getTotal());

        return map;
    }

    @Override
    public List<BTypePackage> queryAll(BTypePackageQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BTypePackageDto.class, criteria));
    }
}
