package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BDepartmentGroupHDQueryCriteria {
    /**
     * 单位id
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("单位id")
    private String companyId;

    /**
     * 部门id
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("部门id")
    private String departmentId;

    /**
     * 分组id
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("分组id")
    private String id;

    /**
     * 分组名称
     */
    @Query(type = Query.Type.INNER_LIKE)
    @ApiModelProperty("分组名称")
    private String name;
}
