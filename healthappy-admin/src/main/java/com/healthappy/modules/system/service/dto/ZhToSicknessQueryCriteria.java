package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * @description 组合判断设置
 * @author sjc
 * @date 2021-08-2
 */
@Data
public class ZhToSicknessQueryCriteria  {


    /**
     * 体检号
     */
    @ApiModelProperty("体检号")
    @Query(type = Query.Type.EQUAL)
    private Long sicknessId;

    /**
     * 组合名称
     */
    @ApiModelProperty("组合名称")
    @Query(blurry = "zhName,jp")
    private String zhName;


    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("是否删除 空不限制,0：未删除，1：删除。默认0")
    private String delFlag="0";

}
