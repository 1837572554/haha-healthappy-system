package com.healthappy.modules.system.service.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author Jevany
 * @date 2022/1/20 16:58
 * @desc 人员档案信息表 基础信息
 */
@Data
public class TPatientBaseinfoDto  implements Serializable {
    /**
     * 档案号,修改时必须带上，新增不需要
     */
    @ApiModelProperty(name = "pNo",value = "档案号,修改时必须带上，新增不需要")
    @JSONField(name="pNo")
    private Long pNo;

    /** 设置档案号 */
    @ApiModelProperty(name = "pNo",value = "档案号,修改时必须带上，新增不需要")
    @JsonProperty("pNo")
    public void setPNo(Long pNo){
        this.pNo=pNo;
    }

    /** 获得档案号 */
    @ApiModelProperty(name = "pNo",value = "档案号,修改时必须带上，新增不需要")
    @JsonProperty("pNo")
    public Long getPNo() {
        return this.pNo;
    }


    /**
     * 身份证
     */
    @NotBlank(message = "身份证不能为空")
    @ApiModelProperty(value = "身份证")
    private String idNo;

    /**
     * 姓名
     */
    @NotBlank(message = "姓名不能为空")
    @ApiModelProperty(value = " 姓名")
    private String name;


    /**
     * 体检分类表id
     */
    @ApiModelProperty("体检分类表id")
    private Long peTypeHdId;

}