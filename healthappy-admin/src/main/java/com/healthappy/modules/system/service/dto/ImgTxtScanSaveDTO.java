package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 图片扫描录入保存类
 * @author Jevany
 * @date 2022/2/16 11:55
 */
@Data
public class ImgTxtScanSaveDTO extends ImgTxtScanQueryDTO {
    /**
     * 图片文件编号
     */
    @ApiModelProperty(value = "图片文件编号,如项目类型，只做追加操作")
    private String fileId;
}