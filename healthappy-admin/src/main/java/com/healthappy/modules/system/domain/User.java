package com.healthappy.modules.system.domain;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.config.databind.FieldBind;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * @author hupeng
 * @date 2020-05-14
 */
@Data
@TableName("user")
public class User implements Serializable {

    /** 系统用户ID */
    @TableId(type = IdType.AUTO)
    private Long id;

    /** 邮箱 */
    private String email;


    /** 状态：1启用、0禁用 */
    private Boolean isEnable;

    /** 用户头像 */
    private String avatar;

    /** 用户角色 */
    @TableField(exist = false)
    private Set<Role> roles;

    /** 科室可多选 */
    @TableField(exist = false)
    private Set<String> depts;

    /** 密码 */
    private String password;

    @ApiModelProperty("新密码")
    @TableField(exist = false)
    private String newPassword;

    @ApiModelProperty(value = "租户ID",name = "tenant_id")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;

    /** 用户名 */
    @NotBlank(message = "请填写用户名称")
    private String username;


    /** 手机号码 */
    private String phone;

    /** 创建日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT)
    private Timestamp createTime;


    /** 最后修改密码的日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp lastPasswordResetTime;


    /** 昵称 */
    private String nickName;


    /** 性别 */
    @FieldBind(target = "sexText")
    private String sex;

    @TableField(exist = false)
    private String sexText;

    /**
     * 科室
     */
    //private Long deptId;

    /**
     * 签字
     */
    private String sign;

    /**
     * 医生类型
     */
    @FieldBind(type = "userType",target = "typeText")
    private String type;

    @TableField(exist = false)
    private String typeText;

    /**
     * 排序号
     */
    private Integer fsort;

    /**
     * ca证书标识
     */
    private String userCert;

    /**
     * 是否管理员
     */
    private String isAdmin;

    /**
     * 科室编码
     */
    private String deptCode;

    /**
     * 科室名称
     */
    private String deptName;

    /**
     * 最后更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp tLastUpTime;


    /**
     * 标识集合
     */
    @TableField(exist = false)
    private Set<Long> tagList;

    /**
     * 简拼
     */
    private String jp;

    public @interface Update {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(username, user.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username);
    }

    public void copy(User source) {
        BeanUtil.copyProperties(source, this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
