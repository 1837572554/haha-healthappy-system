package com.healthappy.modules.system.service.wrapper;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.healthappy.modules.system.domain.*;
import com.healthappy.modules.system.service.*;
import com.healthappy.modules.system.service.dto.BGroupHdDto;
import com.healthappy.modules.system.service.dto.BallCheckDto;
import com.healthappy.utils.SpringUtil;

import java.util.Optional;

/**
 * 包装类,返回视图层所需的字段
 *
 * @author FGQ
 */
public class BallCheckWrapper extends BaseEntityWrapper<TAppoint, BallCheckDto>{

    private final static BSexService sexService;
    private final static BPeTypeHDService peTypeHDService;
    private final static BPeTypeDTService peTypeDTService;
    private final static BPoisonService poisonService;
    private final static BMaritalStatusService maritalStatusService;

    static {
        sexService = SpringUtil.getBean(BSexService.class);
        peTypeHDService = SpringUtil.getBean(BPeTypeHDService.class);
        peTypeDTService = SpringUtil.getBean(BPeTypeDTService.class);
        poisonService = SpringUtil.getBean(BPoisonService.class);
        maritalStatusService = SpringUtil.getBean(BMaritalStatusService.class);
    }

    public static BallCheckWrapper build(){
        return new BallCheckWrapper();
    }

    @Override
    public BallCheckDto entityVO(TAppoint entity) {
        BallCheckDto ballCheckDto = BeanUtil.copyProperties(entity, BallCheckDto.class);
        if(ObjectUtil.isNotNull(ballCheckDto.getSex())){
            BSex sex = sexService.getById(ballCheckDto.getSex());
            ballCheckDto.setSexName(Optional.ofNullable(sex).map(BSex::getName).orElse(""));
        }
        if(ObjectUtil.isNotNull(ballCheckDto.getPeType())){
            BPeTypeHD peTypeHD = peTypeHDService.getById(ballCheckDto.getPeType());
            ballCheckDto.setPeTypeName(Optional.ofNullable(peTypeHD).map(BPeTypeHD::getName).orElse(""));
        }
        if(ObjectUtil.isNotNull(ballCheckDto.getType())){
            BPeTypeDT peTypeDT = peTypeDTService.getById(ballCheckDto.getType());
            ballCheckDto.setTypeName(Optional.ofNullable(peTypeDT).map(BPeTypeDT::getName).orElse(""));
        }
        if(ObjectUtil.isNotNull(ballCheckDto.getPoisonType())){
            BPoison bPoison = poisonService.getById(ballCheckDto.getPoisonType());
            ballCheckDto.setPoisonTypeName(Optional.ofNullable(bPoison).map(BPoison::getPoisonName).orElse(""));
        }
        if(ObjectUtil.isNotNull(ballCheckDto.getMarry())){
            BMaritalStatus maritalStatus = maritalStatusService.getById(ballCheckDto.getMarry());
            ballCheckDto.setMarryName(Optional.ofNullable(maritalStatus).map(BMaritalStatus::getDetail).orElse(""));
        }
        return ballCheckDto;
    }
}
