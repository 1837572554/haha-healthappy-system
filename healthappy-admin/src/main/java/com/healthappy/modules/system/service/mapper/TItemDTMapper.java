package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BItem;
import com.healthappy.modules.system.domain.TItemDT;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 体检人员子项目相关Mapper
 * @author sjc
 * @date 2021-07-20
 */
@Mapper
@Repository
public interface TItemDTMapper extends CoreMapper<TItemDT> {

    boolean saveItemDt(@Param("paId") String paId, @Param("groupId") String groupId,@Param("sex") String sexId,@Param("review") Integer review);


    @Select("SELECT bi.* FROM B_Group_DT as  bgd RIGHT JOIN  B_Item as bi ON bgd.item_id = bi.id WHERE  bgd.group_id = #{groupId}")
    List<BItem> getGroupIdList(@Param("groupId") String groupId);

    @SqlParser(filter = true)
    void repeatItemId(@Param("paIds") List<String> paIds,@Param("tenantId") String tenantId);
}
