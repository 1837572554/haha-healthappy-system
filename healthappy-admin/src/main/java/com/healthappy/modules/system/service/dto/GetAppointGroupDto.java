package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author sqoy
 * @date 2021/12/17 17:47
 * @desc
 */
@Data
public class GetAppointGroupDto implements Serializable {

    /**
     * 科室编号
     */
    @ApiModelProperty("科室编号")
    private String deptId;

    /**
     * 科室编号
     */
    @ApiModelProperty("科室名称")
    private String deptName;

    /**
     * 组合编号
     */
    @ApiModelProperty("组合编号")
    private String groupId;

    /**
     * 组合名称
     */
    @ApiModelProperty("组合名称")
    private String groupName;

    /**
     * 价格
     */
    @ApiModelProperty("价格")
    private String price;

    /**
     * 实价
     */
    @ApiModelProperty("实价")
    private String cost;

    /**
     * 折扣
     */
    @ApiModelProperty("折扣")
    private String discount;

    /**
     * 适用性别
     */
    @ApiModelProperty("适用性别")
    private String sex;

    /**
     * 职检
     */
    @ApiModelProperty("职检")
    private String typeJ;

    /**
     * 普检
     */
    @ApiModelProperty("普检")
    private String typeZ;

    /**
     * 普检
     */
    @ApiModelProperty("附加项目")
    private String addition;

    /** 收费方式 0自费 1统收 */
    @ApiModelProperty("收费方式 0自费 1统收")
    private Integer payType;

}
