package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BSicknessZH;
import com.healthappy.modules.system.service.dto.DeptItemSicknessDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author sjc
 * @description
 * @date 2021-08-3
 */
@Mapper
@Repository
public interface BSicknessZHMapper extends CoreMapper<BSicknessZH> {
    /**
     * 获取科室、明细项、病种数据
     *
     * @param tenantId 租户编号
     * @return java.util.List〈com.healthappy.modules.system.service.dto.DeptItemSicknessDTO〉
     * @author YJ
     * @date 2022/3/8 14:33
     */
    @SqlParser(filter = true)
    List<DeptItemSicknessDTO> listDeptItemSickness(@Param("simpleSpelling") String simpleSpelling, @Param("tenantId") String tenantId);
}
