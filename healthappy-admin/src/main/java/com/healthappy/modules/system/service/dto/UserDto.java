package com.healthappy.modules.system.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

/**
 * @author hupeng
 * @date 2020-05-14
 */
@Data
public class UserDto implements Serializable {


    @ApiModelProperty(hidden = true)
    private Long id;

    private String username;

    private String nickName;

    private String sex;

    private String avatar;

    private String email;

    private String phone;

    private String tenantId;

    private Boolean isEnable;

    @JsonIgnore
    private String password;

    private Timestamp lastPasswordResetTime;

    /**
     * 科室
     */
    //private Long deptId;

    private Set<String> depts;

    /**
     * 签字
     */
    private String sign;

    /**
     * 医生类型
     */
    private String type;

    private String typeText;

    /**
     * 排序号
     */
    private Integer fsort;

    /**
     * ca证书标识
     */
    private String userCert;

    /**
     * 是否管理员
     */
    private String isAdmin;

    /**
     * 科室编码
     */
    private String deptCode;

    /**
     * 科室名称
     */
    private String deptName;

    /**
     * 最后更新时间
     */
    private Timestamp tLastUpTime;


    /**
     * 简拼
     */
    private String jp;

    @ApiModelProperty(hidden = true)
    private Set<RoleSmallDto> roles;

    private Timestamp createTime;

    private Set<Long> tagList;
}
