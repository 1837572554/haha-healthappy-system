package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TZyAnswer;
import com.healthappy.modules.system.service.dto.ZyTzScoreSum;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Jevany
 * @date 2021/12/8 19:11
 * @desc 体检者问卷 答案 Mapper
 */
@Mapper
@Repository
public interface TZyAnswerMapper extends CoreMapper<TZyAnswer> {

    /**
     * 获得分数合计
     * @author YJ
     * @date 2021年12月9日 21:05:31
     * @param paId 体检号
     * @param bankId 库编号
     * @return java.util.List〈com.healthappy.modules.system.service.dto.ZyTzScoreSum〉
     */
    List<ZyTzScoreSum> getScoreSum(@Param("paId") String paId,@Param("bankId") Integer bankId);

    /**
     * 根据问题库编号和体检号 获取答案
     * @author YJ
     * @date 2021/12/10 14:09
     * @param paId  体检号 不能为空
     * @param bankId 问题库编号 1：30题，2：60题  不能为空
     * @param qIds  问题编号【List集合】
     * @return java.util.List〈com.healthappy.modules.system.domain.TZyAnswer〉
     */
    List<TZyAnswer> getAnswersByQuestionBankId(@Param("paId") String paId,@Param("bankId") Integer bankId,@Param("qIds") List<String> qIds);
}
