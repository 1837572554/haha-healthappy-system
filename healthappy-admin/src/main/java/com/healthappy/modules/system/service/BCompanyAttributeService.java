package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BCompanyAttribute;
import com.healthappy.modules.system.service.dto.BCompanyAttributeQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 单位属性
 * @author sjc
 * @date 2021-08-30
 */
public interface BCompanyAttributeService extends BaseService<BCompanyAttribute> {

    Map<String, Object> queryAll(BCompanyAttributeQueryCriteria criteria, Pageable pageable);

    List<BCompanyAttribute> queryAll(BCompanyAttributeQueryCriteria criteria);
}
