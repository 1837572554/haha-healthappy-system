package com.healthappy.modules.system.service.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author: YuTang
 * @Date: Created in 2022/3/31 11:53
 * @Description:
 * @Version: 1.0
 */
@Data
public class TChargeCOMApplyWujiaDTO {
    private String itemPriceId;
    private String itemCnname;
    private String pkgName;
    private BigDecimal price;
    private BigDecimal itemQuantity;
    private BigDecimal itemDiscountValue;
}
