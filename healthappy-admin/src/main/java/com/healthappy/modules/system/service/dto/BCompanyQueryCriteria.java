package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BCompanyQueryCriteria {

    /**
     * 单位名称
     */
    @Query(blurry = "pyCode,name")
    @ApiModelProperty("单位名称")
    private String name;
}
