package com.healthappy.modules.system.service.impl;


import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BCompany;
import com.healthappy.modules.system.service.BCompanyService;
import com.healthappy.modules.system.service.WorkStationService;
import com.healthappy.modules.system.service.dto.BCompanyQueryCriteria;
import com.healthappy.modules.system.service.mapper.BCompanyMapper;
import com.healthappy.modules.system.service.mapper.BDepartmentMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sjc
 * @date 2021-06-24
 */
@Service
@AllArgsConstructor//有自动注入的功能
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BCompanyServiceImpl  extends BaseServiceImpl<BCompanyMapper, BCompany> implements BCompanyService {

    private final IGenerator generator;
    private final WorkStationService workStationService;
    private final BDepartmentMapper bDepartmentMapper;

    @Override
    public Map<String, Object> queryAll(BCompanyQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BCompany> page = new PageInfo<BCompany>(queryAlls(criteria));
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("content", page.getList());
        map.put("totalElements", page.getTotal());

        return map;
    }
    /**
     * 查询单位全部数据
     * @return Object
     */
    @Override
    public List<BCompany> queryAlls(BCompanyQueryCriteria criteria) {
        return this.list(QueryHelpPlus.getPredicate(BCompany.class, criteria));
    }

    /**
     * 判断单位是否存在
     * @param companyId
     * @return
     */
    @Override
    public boolean isExistCompany(String companyId)
    {
        return this.getById(companyId)!=null;
    }
}
