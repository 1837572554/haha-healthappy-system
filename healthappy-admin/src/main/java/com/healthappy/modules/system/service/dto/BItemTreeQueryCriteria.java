package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BItemTreeQueryCriteria {

    @Query(blurry = "name,jp")
    @ApiModelProperty(name = "blurry",value = "项目名称 = name|简拼 = jp")
    private String blurry;


    @Query(type = Query.Type.NOT_NULL)
    @ApiModelProperty(value = "科室编号",hidden = true)
    private Long deptId = 1L;
}
