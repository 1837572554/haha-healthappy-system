package com.healthappy.modules.system.service.impl;

import cn.hutool.core.date.DateTime;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.TChargeComApplyPatient;
import com.healthappy.modules.system.domain.TPatient;
import com.healthappy.modules.system.service.PayApplyPatientService;
import com.healthappy.modules.system.service.TChargeCOMApplyWujiaService;
import com.healthappy.modules.system.service.dto.CheckoutListExportDTO;
import com.healthappy.modules.system.service.dto.CompanyPayApplyPatientCriteria;
import com.healthappy.modules.system.service.dto.CompanyReportPersonDTO;
import com.healthappy.modules.system.service.mapper.CompanyPayCostMapper;
import com.healthappy.modules.system.service.mapper.PayApplyPatientMapper;
import com.healthappy.utils.PageUtil;
import com.healthappy.utils.SecurityUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@AllArgsConstructor
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = false, rollbackFor = Exception.class)
public class PayApplyPatientServiceImpl extends BaseServiceImpl<PayApplyPatientMapper, TChargeComApplyPatient> implements PayApplyPatientService {

    private final CompanyPayCostMapper companyPayCostMapper;
    private final IGenerator generator;
    private final TPatientServiceImpl tPatientService;
    private final TChargeCOMApplyWujiaService wujiaService;

    @Override
    public Map<String, Object> getList(CompanyPayApplyPatientCriteria criteria) {
        String tenantId = SecurityUtils.getTenantId();
        Integer totalCount = companyPayCostMapper.getListByScreenCount(criteria, tenantId);
        Integer[] pageBase = PageUtil.toStartEnd(criteria.getPageNum(), criteria.getPageSize(), totalCount);
        criteria.setStartIndex(pageBase[0]);
        Map<String, Object> map = new LinkedHashMap<>();
//        if(StrUtil.isNotBlank(criteria.getIsPay()) && "2".equals(criteria.getIsPay())){
//            criteria.setIsPay("0");
//        }
//        if (ObjectUtil.isNotNull(criteria.getIsLock()) && 2 == criteria.getIsLock()) {
//            criteria.setIsLock(0);
//        }
        map.put("content", companyPayCostMapper.getListByScreen(criteria, tenantId));
        map.put("totalElements", totalCount);
        map.put("totalPage", pageBase[3]);
        map.put("currentPage", pageBase[2]);
        map.put("pageSize", criteria.getPageSize());
        return map;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void savePayPatient(TChargeComApplyPatient vo) {
        TChargeComApplyPatient applyPatient = this.lambdaQuery().eq(TChargeComApplyPatient::getPaId, vo.getPaId()).eq(TChargeComApplyPatient::getPayApplyId, vo.getPayApplyId())
                .eq(TChargeComApplyPatient::getTenantId, vo.getTenantId()).one();
        switch (vo.getOperate()) {
            case 0:
                billPlease(vo, applyPatient);
                break;
            case 1:
                //直接删除结账人员
                this.lambdaUpdate().eq(TChargeComApplyPatient::getPaId, vo.getPaId())
                        .eq(TChargeComApplyPatient::getPayApplyId, vo.getPayApplyId())
                        .eq(TChargeComApplyPatient::getTenantId, vo.getTenantId())
                        .remove();
                break;
            case 2:
                //如果是锁定
                locking(vo);
                break;
            case 3:
                //如果是解锁
                tPatientService.lambdaUpdate().eq(TPatient::getId, vo.getPaId()).set(TPatient::getIsLock, 0).update();
                break;
            case 4:
                billPlease(vo, applyPatient);
                locking(vo);
                break;
            default:
                throw new BadRequestException("参数异常");
        }

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void savePatient(Set<TChargeComApplyPatient> patients) {
        for (TChargeComApplyPatient tc:patients) {
            savePayPatient(tc);
        }
        //操作码：0：结账，1：撤销结账,2：锁定，3：解锁，4：结账+锁定
        //结账时需要将用户所作项目的物价保存到单位收费物价表，撤销时需要更新
        TChargeComApplyPatient next = patients.iterator().next();
        if(next.getOperate().equals(0) || next.getOperate().equals(4) || next.getOperate().equals(1)){
            wujiaService.saveByApply(patients.iterator().next().getPayApplyId());
        }
    }

    @Override
    public List<CheckoutListExportDTO> export(CompanyPayApplyPatientCriteria criteria) {
        String tenantId = SecurityUtils.getTenantId();
        Integer totalCount = companyPayCostMapper.getListByScreenCount(criteria, tenantId);
        Integer[] pageBase = PageUtil.toStartEnd(criteria.getPageNum(), criteria.getPageSize(), totalCount);
        criteria.setStartIndex(pageBase[0]);
//        if (StrUtil.isNotBlank(criteria.getIsPay()) && "2".equals(criteria.getIsPay())) {
//            criteria.setIsPay("0");
//        }
//        if (ObjectUtil.isNotNull(criteria.getIsLock()) && 2 == criteria.getIsLock()) {
//            criteria.setIsLock(0);
//        }
        List<CompanyReportPersonDTO> listByScreen = companyPayCostMapper.getListByScreen(criteria, tenantId);
        List<CheckoutListExportDTO> convert = generator.convert(listByScreen, CheckoutListExportDTO.class);
        convert.forEach(dto -> {
            dto.setPayApplyId(criteria.getPayApplyId());
        });
        return convert;
    }

    /**
     * @Author: FGQ
     * @Desc: 锁定
     * @Date: 2022/3/24
     */
    private void locking(TChargeComApplyPatient vo) {
        tPatientService.lambdaUpdate().eq(TPatient::getId, vo.getPaId()).set(TPatient::getIsLock, 1).update();
    }

    /**
     * 插入结账人员
     *
     * @Author: FGQ
     * @Desc: 结账
     * @Date: 2022/3/24
     */
    private void billPlease(TChargeComApplyPatient vo, TChargeComApplyPatient applyPatient) {
        TChargeComApplyPatient patient = this.lambdaQuery().eq(TChargeComApplyPatient::getPaId, vo.getPaId())
                .eq(TChargeComApplyPatient::getPayApplyId, vo.getPayApplyId())
                .eq(TChargeComApplyPatient::getTenantId, vo.getTenantId()).one();
        if (patient == null) {
            vo.setPayFlag(1);
            vo.setPayTime(DateTime.now().toTimestamp().toString());
            vo.setPayDoctor(SecurityUtils.getNickName());
            this.save(vo);
        }
    }
}
