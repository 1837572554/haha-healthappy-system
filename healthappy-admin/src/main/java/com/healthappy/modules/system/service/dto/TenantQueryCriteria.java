package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

@Data
public class TenantQueryCriteria {

    @Query(blurry = "name")
    private String blurry;

    @ApiModelProperty("医疗机构类型编码")
    @Query(type = Query.Type.EQUAL)
    private String instiType;

    @Query(type = Query.Type.BETWEEN)
    private List<Timestamp> createTime;
}
