package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @desc: 体检项目图片绑定Dto
 * @author: YJ
 * @date: 2021-12-01 16:48
 **/
@Data
public class TPatientImgDto implements Serializable {
    /** 体检编号 */
    @ApiModelProperty(value = "体检编号",required = true)
    @NotBlank(message = "体检编号不能为空")
    private String paId;

    /** 组合项编号 */
    @ApiModelProperty(value = "组合项编号")
    private String groupId;

    /** 小项编号，可为空 */
    @ApiModelProperty(value = "小项编号，可为空")
    private String itemId;
}

