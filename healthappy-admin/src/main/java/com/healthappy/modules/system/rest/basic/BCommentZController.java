package com.healthappy.modules.system.rest.basic;

import cn.hutool.core.bean.BeanUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BCommentZ;
import com.healthappy.modules.system.domain.vo.BCommentZVo;
import com.healthappy.modules.system.service.BCommentZService;
import com.healthappy.modules.system.service.dto.BCommentZQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 职业主检意见 控制器
 * @author FGQ
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "基础管理：职业主检意见")
@RestController
@AllArgsConstructor
@RequestMapping("/BCommentZ")
public class BCommentZController {

    private final BCommentZService BCommentZService;

    @Log("查询主检结论意见")
    @ApiOperation("查询主检结论意见，权限码：BCommentZ:list")
    @GetMapping
    public R list(BCommentZQueryCriteria criteria, Pageable pageable) {
        return R.ok(BCommentZService.queryAll(criteria, pageable));
    }

    @Log("新增|修改主检结论意见")
    @ApiOperation("新增|修改主检结论意见，权限码：BCommentZ:add")
    @PostMapping
    @PreAuthorize("@el.check('BCommentZ:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BCommentZVo resources) {
        return R.ok(BCommentZService.saveOrUpdate(BeanUtil.copyProperties(resources, BCommentZ.class)));
    }

    @Log("删除职业主检意见")
    @ApiOperation("删除职业主检意见，权限码：BCommentZ:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('BCommentZ:delete')")
    @Klock
    public R remove(@RequestBody Set<Integer> ids) {
        return R.ok(BCommentZService.removeByIds(ids));
    }
}
