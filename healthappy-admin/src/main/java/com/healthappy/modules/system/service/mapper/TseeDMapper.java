package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TseeD;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 种子
 * @author FGQ
 * @date 2021-06-17
 */
@Mapper
@Repository
public interface TseeDMapper extends CoreMapper<TseeD> {

}
