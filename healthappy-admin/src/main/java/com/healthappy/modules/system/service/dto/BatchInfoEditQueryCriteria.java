package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 批量信息调整-查询类
 * @author Jevany
 * @date 2022/5/12 19:30
 */
@Data
@ApiModel("批量信息调整-查询类")
public class BatchInfoEditQueryCriteria implements Serializable {
	/**
	 * 体检时间类型
	 */
	@ApiModelProperty(value = "体检时间类型 ,页面路径：batchInfoEdit", position = 0)
	private Integer dateType;

	/**
	 * 时间段
	 */
	@ApiModelProperty(value = "时间段")
	private List<String> timeList;

	/**
	 * 体检分类ID
	 */
	@ApiModelProperty("体检分类ID")
	private String peTypeHdId;

	/**
	 * 体检类别ID
	 */
	@ApiModelProperty("体检类别ID")
	private String peTypeDtId;

	/**
	 * 体检号
	 */
	@ApiModelProperty("体检号")
	private String paId;

	/**
	 * 体检者姓名
	 */
	@ApiModelProperty("体检者姓名")
	private String paName;

	/**
	 * 体检者身份证
	 */
	@ApiModelProperty("体检者身份证")
	private String idNo;

	/**
	 * 单位ID
	 */
	@ApiModelProperty("单位ID")
	private String companyId;

	/**
	 * 部门ID
	 */
	@ApiModelProperty("部门ID")
	private String deptId;

	/**
	 * 分组ID
	 */
	@ApiModelProperty("分组ID")
	private String deptGroupId;

	/**
	 * 审核状态 1：已完成 0：未完成 -1全部，默认-1
	 */
	@ApiModelProperty("审核状态 1：已完成 0：未完成 -1全部，默认-1")
	private Integer verify = -1;

	/**
	 * 打印状态  1：已完成 0：未完成 -1全部，默认-1
	 */
	@ApiModelProperty("打印状态  1：已完成 0：未完成 -1全部，默认-1")
	private Integer print = -1;

	/**
	 * 上传状态 1：已完成 0：未完成 -1全部，默认-1
	 */
	@ApiModelProperty("上传状态 1：已完成 0：未完成 -1全部，默认-1")
	private Integer upload = -1;

	private static final long serialVersionUID = 1L;
}