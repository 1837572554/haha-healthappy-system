package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.config.SeedIdGenerator;
import com.healthappy.config.databind.BindType;
import com.healthappy.config.databind.FieldBind;
import com.healthappy.modules.system.domain.vo.BPackageDTVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @description 套餐表
 * @author sjc
 * @date 2021-07-2
 */
@Data
@ApiModel("套餐表")
@TableName("B_Package_HD")
@SeedIdGenerator
public class BPackageHD implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     *套餐id
     */
    @ApiModelProperty("套餐id")
    @TableId(type = IdType.INPUT)
    private String id;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    @TableField("`name`")
    private String name;

    /**
     * 体检类型
     */
    @ApiModelProperty("体检类型")
    @FieldBind(type = "packageHdType",target = "typeText")
    private String type;

    @TableField(exist = false)
    private String typeText;

    /**
     * 适用性别
     */
    @ApiModelProperty("适用性别")
    @FieldBind(target = "sexText")
    private String sex;

    @ApiModelProperty("性别名称")
    @TableField(exist = false)
    private String sexText;

    /**
     * 是否启用
     */
    @ApiModelProperty("是否启用")
    private String isEnable;

    /**
     * 排序号
     */
    @ApiModelProperty("排序号")
    private Integer dispOrde;

    /**
     * 描述
     */
    @ApiModelProperty("描述")
    @TableField("`desc`")
    private String desc;

    /**
     * 套餐价格
     */
    @ApiModelProperty("套餐价格")
    private BigDecimal price;

    /**
     * 套餐折扣
     */
    @ApiModelProperty("套餐折扣")
    private BigDecimal discount;

    /**
     * 套餐应付价格
     */
    @ApiModelProperty("套餐应付价格")
    private BigDecimal cost;

    /**
     * 简拼
     */
    @ApiModelProperty("简拼")
    private String jp;

    /**
     * 图片
     */
    @ApiModelProperty("图片")
    private String iamge;

    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @TableField(fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp updateTime;

    /**
     * 更新医生
     */
    @ApiModelProperty("更新医生")
    @TableField(value = "update_by",fill = FieldFill.INSERT_UPDATE)
    private Long updateBy;

    /**
     * 服务费
     */
    @ApiModelProperty("服务费")
    private BigDecimal serviceFee;

    /**
     * 医疗机构ID(U_Hospital_Info的ID)
     */
    @ApiModelProperty("医疗机构ID")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;

    /**
     * 套餐详情
     */

    @TableField(exist = false)
    @ApiModelProperty("套餐详情")
    private List<BPackageDT> bPackageDt;

}
