package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 科室业务量统计
 * @author Jevany
 * @date 2022/3/14 17:23
 */
@Data
@AllArgsConstructor
@ApiModel("科室业务量统计")
public class DeptBusinessDTO implements Serializable  {
    /** 科室编号 */
    @ApiModelProperty("科室编号")
    private String deptId;

    /** 科室名称 */
    @ApiModelProperty("科室名称")
    private String deptName;

    /** 科室人员数据 0:预约体检人数 1:预约到检人数 2:新增登记签到人数*/
    @ApiModelProperty("科室人员数据 0:预约体检人数 1:预约到检人数 2:新增登记签到人数")
    private Integer[] personNumList= new Integer[3];

    /** 组合项目列表 */
    @ApiModelProperty("组合项目列表")
    private List<DeptBusinessGroupDTO> itemList=new ArrayList<>();

}