package com.healthappy.modules.system.rest.print.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Author: wukefei
 * @Date: Created in 2022/4/28 12:03
 * @Description:
 * @Version: 1.0
 *
 */
@ApiModel("打印单位健康分析")
@Data
public class CompanyAnalysisPrintVo {
	/** 流水号列表 */
	@ApiModelProperty("单位报告编号")
	private List<String> reportIds;
	/** 组合项目列表 */
	@ApiModelProperty("疾病编号不能为空")
	private String sicknessIds;

	/** 用于接收的Socket的用户编号 */
	@ApiModelProperty("用于接收的Socket的用户编号")
	private String acceptSocketUserId;

}
