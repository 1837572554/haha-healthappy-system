package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.Data;

/**
 * Created with IntelliJ IDEA. Created by yutang 2021/9/2 0002  14:34 Description:
 */
@Data
@TableName("B_Type_Pay")
@ApiModel(value = "缴费类型")
public class BTypePay implements Serializable {

  private static final long serialVersionUID = 111839700929517618L;

  @TableId(type = IdType.AUTO)
  @ApiModelProperty("主键id")
  private Long id;

  @ApiModelProperty("名称")
  private String name;

  @ApiModelProperty("排序")
  private Integer dispOrder;

}
