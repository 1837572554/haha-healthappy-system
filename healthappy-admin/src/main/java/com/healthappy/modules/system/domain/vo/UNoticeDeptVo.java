package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @description 公告对应科室表
 * @author sjc
 * @date 2021-09-3
 */
@Data
public class UNoticeDeptVo {
    /**
     * 公告id
     */
    @ApiModelProperty(value = "公告id",position = 1)
    @NotNull(message = "公告id不可为空")
    private String noticeId;

    /**
     * 科室id
     */
    @ApiModelProperty(value = "科室id",position = 2)
    @NotNull(message = "科室id不可为空")
    private String deptId;

}
