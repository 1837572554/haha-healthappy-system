package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNode;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BArea;
import com.healthappy.modules.system.service.BAreaService;
import com.healthappy.modules.system.service.dto.BAreaQueryCriteria;
import com.healthappy.modules.system.service.mapper.BAreaMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BAreaServiceImpl extends BaseServiceImpl<BAreaMapper, BArea> implements BAreaService {

    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BAreaQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BArea> page = new PageInfo<BArea>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", generator.convert(page.getList(), BArea.class));
        map.put("totalElements", page.getTotal());
        return map;
    }

    @Override
    public List<BArea> queryAll(BAreaQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BArea.class, criteria));
    }

    @Override
    public List<Tree<String>> tree(BAreaQueryCriteria criteria) {
        List<BArea> areaList = this.list(new LambdaQueryWrapper<BArea>().eq(BArea::getIsEnable,criteria.getIsEnable()));
        if(CollUtil.isEmpty(areaList)){
            return  new ArrayList<>();
        }
        if(StrUtil.isNotBlank(criteria.getBlurry()) || StrUtil.isNotBlank(criteria.getCode()) || ObjectUtil.isNotNull(criteria.getId())){
            BArea area = this.getOne(new LambdaQueryWrapper<BArea>().like(StrUtil.isNotBlank(criteria.getCode()),BArea::getCode,criteria.getCode())
                                                            .like(StrUtil.isNotBlank(criteria.getBlurry()),BArea::getName, criteria.getBlurry())
                                                            .last(" limit 1")
                                                            .eq(ObjectUtil.isNotNull(criteria.getId()),BArea::getId,criteria.getId()));
            List<BArea> bAreas = new ArrayList<>();
            buildReverseTree(bAreas,areaList,area.getHiCode());
            areaList = new ArrayList<>();
            areaList.add(area);
            areaList.addAll(bAreas);
        }
        List<TreeNode<String>> nodeList = CollUtil.newArrayList();
        for (BArea area:areaList) {
            TreeNode<String> treeNode = new TreeNode<String>(area.getCode(),area.getHiCode(),area.getName(),"0");
            Map<String,Object> map = new HashMap<>(2);
            map.put("code",area.getCode());
            map.put("hiCode",area.getHiCode());
            treeNode.setExtra(map);
            nodeList.add(treeNode);
        }
        return TreeUtil.build(nodeList,"0");
    }

    //递归查询 最顶级 Tree
    private void buildReverseTree(List<BArea> bAreas, List<BArea> areaList,String hiCode) {
        Optional<BArea> optional = areaList.stream().filter(x -> x.getCode().equals(hiCode)).findAny();
        if(optional.isPresent()){
            BArea bArea = optional.get();
            bAreas.add(bArea);
            this.buildReverseTree(bAreas,areaList,bArea.getHiCode());
        }
    }


    @Override
    public Integer checkExistHiCode(Set<Integer> ids) {
        return baseMapper.checkExistHiCode(ids);
    }
}
