package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TAppoint;
import com.healthappy.modules.system.domain.vo.TAppointVo;
import com.healthappy.modules.system.service.dto.*;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @description 预约表 服务层
 * @author sjc
 * @date 2021-06-28
 */
public interface TAppointService extends BaseService<TAppoint> {

	/**
	 * 根据身份证获取预约数据
	 * @param  id_no 身份证
	 * @return
	 */
	List<TAppoint> queryByIdNo(String id_no);

	Map<String, Object> queryAll(TAppointQueryCriteria criteria, Pageable pageable);

	boolean saveOrUpdate(TAppointVo resources);

	/**
	 * 读取上传的Excel文件中的数据并返回
	 * @param file
	 * @return
	 */
	List<TAppointDto> upload(MultipartFile file);

	/**
	 * 预约导入，将Excel文件读取出来的内容保存到预约表中
	 * @param list
	 */
	void importAppoint(List<TAppointVo> list);

	/**
	 * 导出数据为Excel文件
	 * @param id 所有需要导出的数据的id逗号拼接
	 * @param response
	 * @throws IOException
	 */
	void download(String id, HttpServletResponse response) throws IOException, IllegalAccessException;

	/**
	 * 查询单位全部数据
	 * @return Object
	 */
	List<TAppoint> queryAll(TAppointQueryCriteria criteria);

	/**
	 * 签到
	 * @return Object
	 */
	boolean sign(String id);

	/**
	 * 签到体检
	 * @author YJ
	 * @date 2022/1/25 14:34
	 * @param appointId
	 * @return java.lang.Boolean
	 */
	Boolean signPatient(String appointId, String paId);

	TAppointPrintDto detailForPrint(Integer id);

	Map<String, Object> ballCheckQueryAll(DeptWorkloadQueryCriteria criteria, Pageable pageable);

	List<TAppoint> queryAll(DeptWorkloadQueryCriteria criteria);

	Map<String, Object> queryAppointGroup(String appointId);

	/**
	 * 计算时间段内，有预约数据的，日期预约数量
	 * @author YJ
	 * @date 2022/3/16 12:53
	 * @param timeList
	 * @return java.util.List〈com.healthappy.modules.system.service.dto.AppointDateNumDTO〉
	 */
	List<AppointDateNumDTO> calcAppointDateNum(List<String> timeList);

	/**
	 * 预约号是否当天的
	 * @author YJ
	 * @date 2022/5/10 18:30
	 * @param appointId 预约号
	 * @return 当天的则大于0, 反之为0
	 */
	Integer appointToDayById(String appointId);
}

