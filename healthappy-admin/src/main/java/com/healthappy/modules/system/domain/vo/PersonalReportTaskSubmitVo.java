package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class PersonalReportTaskSubmitVo {

    @ApiModelProperty("体检编号集合")
    private List<String> ids;

    @ApiModelProperty("医生集合")
    private DockerNum dockerNum;


   @Data
   public static class DockerNum{

       private String conclusionDoc;

       private String conclusionCode;

       private String verifyDoc;

       private String verifyCode;
    }
}
