package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

@Data
public class MedicalRegistrationQueryCriteria {


    @ApiModelProperty("体检编号")
    @Query(type = Query.Type.EQUAL)
    private String id;

    @ApiModelProperty("体检者姓名")
    @Query(type = Query.Type.EQUAL)
    private String name;

    @ApiModelProperty("体检日期")
    @Query(type = Query.Type.BETWEEN)
    private List<Timestamp> peDate;


    @ApiModelProperty("单位ID")
    @Query(type = Query.Type.EQUAL)
    private String companyId;

    @ApiModelProperty("体检分类表id")
    @Query(type = Query.Type.EQUAL)
    private Long peTypeHdId;
}
