package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BGroupHDWuJia;
import com.healthappy.modules.system.domain.vo.BGroupHDWuJiaVo;
import com.healthappy.modules.system.service.dto.BGroupHDWuJiaQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 组合项目物价服务层
 * @author sjc
 * @date 2021-08-3
 */
public interface BGroupHDWuJiaService  extends BaseService<BGroupHDWuJia> {
    Map<String, Object> queryAll(BGroupHDWuJiaQueryCriteria criteria, Pageable pageable);

    List<BGroupHDWuJia> queryAll(BGroupHDWuJiaQueryCriteria criteria);

    void saveOrUpdate(List<BGroupHDWuJiaVo> resources);

    /**
     * 根据组合项目id删除物价数据
     * @param groupId
     * @return
     */
    boolean deleteByGroupId(String groupId);
    /**
     * 根据主键id删除物价数据
     * @param id
     * @return
     */
    boolean deleteById(Long id);
}
