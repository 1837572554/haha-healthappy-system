package com.healthappy.modules.system.service.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author sjc
 * @date 2022-01-20
 */
@Data
public class DataQueryDto extends TPatientDto {

    /**
     * 组装主键：流水号+组合项目编号
     */
    @ApiModelProperty("组装主键：流水号+组合项目编号")
    private String key;

    /**
     * 弃检项目名称
     */
    @ApiModelProperty("弃检项目名称")
    private String waiveItemName;

    /**
     * 弃检项目总金额
     */
    @ApiModelProperty("弃检项目总金额")
    private BigDecimal waiveCost;


    /**
     * 组合名称
     */
    @ApiModelProperty("工种名称")
    private String jobName;

    /**
     * 组合名称
     */
    @ApiModelProperty("组合名称")
    private String groupName;
    /**
     * 结果
     */
    @ApiModelProperty("结果")
    private String result;
    /**
     * 医生
     */
    @ApiModelProperty("医生")
    private String doctor;
    /**
     * 检查日期
     */
    @ApiModelProperty("检查日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date resultDate;


    @ApiModelProperty("接害工龄")
    private String workYears;

    @ApiModelProperty("接害种类")
    private String poisonType;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;
}
