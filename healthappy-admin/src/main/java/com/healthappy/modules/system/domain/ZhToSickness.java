package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @description 组合判断设置
 * @author sjc
 * @date 2021-08-2
 */
@Data
@ApiModel("组合判断设置")
@TableName("Zh_To_Sickness")
public class ZhToSickness implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    @TableId(type = IdType.AUTO)
    private String id;


    /**
     * 病种id
     */
    @ApiModelProperty("病种id")
    private String sicknessId;

    /**
     * 组合名称
     */
    @ApiModelProperty("组合名称")
    private String zhName;

    /**
     * jp
     */
    @ApiModelProperty("简拼")
    private String jp;

    /**
     * 医疗机构id(u_hospital_info的id)
     */
    @ApiModelProperty("医疗机构id(u_hospital_info的id)")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;

    @ApiModelProperty("不包含组合病种下对应的病种数据")
    @TableField(exist=false)
    private List<BSicknessZH> doesContainsicknessZHList;


    @ApiModelProperty("包含组合病种下对应的病种数据")
    @TableField(exist=false)
    private List<BSicknessZH> containSicknessZHList;
}
