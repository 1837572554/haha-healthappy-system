package com.healthappy.modules.system.rest.dataquery;

import cn.hutool.core.util.ObjectUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.service.GroupPanelServer;
import com.healthappy.modules.system.service.dto.DataQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @description 体检项目名单 控制器
 * @author sjc
 * @date 2022-1-21
 */
@Slf4j
@Api(tags = "数据查询：体检项目名单")
@RestController
@AllArgsConstructor
@RequestMapping("/groupPanel")
public class GroupPanelController {


    private GroupPanelServer groupPanelServer;

    @Log("分页查询")
    @ApiOperation("分页查询，权限码：TPatient:list")
    @PostMapping
    @PreAuthorize("@el.check('groupPanel:list')")
    public R list(@RequestBody DataQueryCriteria criteria, Pageable pageable) {
        if(ObjectUtil.isNotNull(criteria.getDateType())){
            if(ObjectUtil.isNull(criteria.getStartTime()) || ObjectUtil.isNull(criteria.getEndTime())){
                return R.error("查询时间条件有误");
            }
        }
        return R.ok(groupPanelServer.queryAll(criteria,pageable));
    }


    @Log("导出Excel数据")
    @ApiOperation("导出Excel数据")
    @PostMapping(value = "download")
    @PreAuthorize("@el.check('admin','groupPanel:upload')")
    @Klock
    public void downloadLog(HttpServletResponse response, @RequestBody DataQueryCriteria criteria) throws IOException {
        groupPanelServer.download(criteria, response);
    }

}
