package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("单位缴费人员名单")
public class CompanyPayApplyPatientCriteria {

    /**
     * 体检时间类型
     */
    @ApiModelProperty(value = "体检时间类型 ,页面路径：imgTxtScanManage", required = true, position = 0)
    private Integer dateType;

    /** 时间段（数组） */
    @ApiModelProperty(value = "时间段（数组） ", required = true, position = 1)
    @Query(type = Query.Type.BETWEEN)
    private List<String> timeList;

    /**
     * 体检分类ID
     */
    @ApiModelProperty("体检分类ID")
    private String peTypeHdId;

    /**
     * 体检类别ID
     */
    @ApiModelProperty("体检类别ID")
    private String peTypeDtId;

    /**
     * 体检号
     */
    @ApiModelProperty("体检号")
    private String paId;

    /**
     * 体检者姓名
     */
    @ApiModelProperty("体检者姓名")
    private String paName;

    /**
     * 体检者姓名
     */
    @ApiModelProperty("收费单号")
    private String payApplyId;

    /**
     * 单位ID
     */
    @ApiModelProperty("单位ID")
    private String companyId;

    /**
     * 部门ID
     * */
    @ApiModelProperty("部门ID")
    private String departmentId;

    /**
     * 分组ID
     * */
    @ApiModelProperty("分组ID")
    private String departmentGroupId;

    /**
     * 是否签到
     * */
    @ApiModelProperty("是否签到")
    private String isSign;

    /**
     * 是否审核
     * */
    @ApiModelProperty("是否审核")
    private String isVerify;

    /**
     * 是否结账
     * */
    @ApiModelProperty("是否结账 0全部， 1是，2否")
    private String isPay;

    /**
     * 是否锁定
     */
    @ApiModelProperty("是否锁定")
    private Integer isLock;

//    /**
//     * 是否上岗前 0：否，1：是 默认0
//     */
//    @ApiModelProperty("是否上岗前 0：否，1：是 默认0")
//    private Integer isBefore = 0;
//
//    /**
//     * 是否在岗期间 0：否，1：是 默认0
//     */
//    @ApiModelProperty("是否在岗期间 0：否，1：是 默认0")
//    private Integer isOnJob = 0;
//
//    /**
//     * 是否离岗时 0：否，1：是 默认0
//     */
//    @ApiModelProperty("是否离岗时 0：否，1：是 默认0")
//    private Integer isAfter = 0;
//
//    /**
//     * 是否应急 0：否，：1是 默认0
//     */
//    @ApiModelProperty("是否应急 0：否，：1是 默认0")
//    private Integer isEmergency = 0;

    /**
     * 页面显示条数
     */
    @ApiModelProperty("页面显示条数,默认为10")
    private Integer pageSize = 10;

    /**
     * 页数
     */
    @ApiModelProperty("页数,默认为1")
    private Integer pageNum = 1;

    /**
     * 是否导出，如果为True，分页功能不启用，默认False
     */
    @ApiModelProperty(value = "是否导出，如果为True，分页功能不启用，默认False",hidden = true)
    private Boolean isExport = false;


    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID", hidden = true)
    private String tenantId;

    /**
     * 开始条数
     */
    @ApiModelProperty(value = "开始条数", hidden = true)
    private Integer startIndex = -1;


}
