package com.healthappy.modules.system.service.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class TItemHDDto implements Serializable {


    /**
     * 体检号
     */
    private String paId;

    /**
     * 组合编号
     */
    private String groupId;

    /**
     * 科室编号
     */
    private String deptId;

    /**
     * 组合名称
     */
    private String groupName;
    /**
     * 结果
     */
    private String result;

    /**
     * 描述
     */
    private String resultDesc;
    /**
     * 结果判定
     */
    private String resultMark;

    /**
     * 检查日期
     */
    private Date resultDate;

    /**
     * 医生
     */
    private String doctor;

    /**
     * 审核医生
     */
    private String reportDoctor;

    /**
     * 是否是职业项目
     */
    private String typeZ;

    /**
     * 是否是健康项目
     */
    private String typeJ;

    /**
     * 类型
     */
    private String type;

    /**
     * 条码
     */
    private String barcode;

    /**
     * 收费
     */
    private BigDecimal price;

    /**
     * 折扣
     */
    private BigDecimal discount;

    /**
     * 优惠后价格
     */
    private BigDecimal cost;

    /**
     * 是否附加项目
     */
    private String addition;

    /**
     * 付费方式 0自费 1统收
     */
    private String payType;

    /**
     * 收费时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date chargeDate;

    /**
     * 添加项目的时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date addTime;

    /**
     * 复查
     */
    private Integer review;

    /**
     * 项目登记医生
     */
    private String registerDoc;

    /**
     * 修改小结医生
     */
    private String updateResultDoc;

    /**
     * 修改小结时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateResultDate;

    /**
     * 最后更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime;
}
