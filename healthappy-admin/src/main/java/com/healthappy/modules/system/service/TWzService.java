package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TWz;
import com.healthappy.modules.system.service.dto.TWzQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 职业史表
 * @author sjc
 * @date 2021-12-15
 */
public interface TWzService extends BaseService<TWz> {

    /**
     * 根据体检号获取对应的问诊信息
     * @param paId
     * @return
     */
    TWz query(String paId);

    boolean remove(String paId);
}
