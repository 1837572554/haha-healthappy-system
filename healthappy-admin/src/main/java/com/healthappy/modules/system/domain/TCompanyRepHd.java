package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.config.SeedIdGenerator;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;


/**
 * 单位报告编制主表
 * @author YJ
 * @date 2022/2/17 15:27
 */
@Data
@SeedIdGenerator
@TableName("T_COMPANY_REP_HD")
@RenewLog
public class TCompanyRepHd implements Serializable {
    /**
     * 报告编号
     */
    @ApiModelProperty(value = "报告编号")
    @TableId(type = IdType.INPUT)
    private String id;

    /**
     * 报告名称
     */
    @ApiModelProperty(value = "报告名称")
    private String reportName;

    /**
     * 检查编号
     */
    @ApiModelProperty(value = "检查编号")
    private String checkId;
    /**
     * 检查人数
     */
    @ApiModelProperty(value = "检查人数")
    private Integer checkNum;

    /**
     * 检查开始时间
     */
    @ApiModelProperty(value = "检查开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp peDateStart;

    /**
     * 检查结束时间
     */
    @ApiModelProperty(value = "检查结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp peDateEnd;

    /**
     * 单位编号
     */
    @ApiModelProperty(value = "单位编号")
    private String companyId;

    /**
     * 委托单位
     */
    @ApiModelProperty(value = "委托单位")
    private String entrustCompany;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remarks;

    /**
     * 总结（结论与意见）
     */
    @ApiModelProperty(value = "总结（结论与意见）")
    private String summary;

    /**
     * 编制时间
     */
    @ApiModelProperty(value = "编制时间")
    private Date compileTime;

    /**
     * 编制医生
     */
    @ApiModelProperty(value = "编制医生")
    private String compileDoctor;

    /**
     * 审核时间
     */
    @ApiModelProperty(value = "审核时间")
    private Date verifyTime;

    /**
     * 审核医生
     */
    @ApiModelProperty(value = "审核医生")
    private String verifyDoctor;

    /**
     * 报告时间
     */
    @ApiModelProperty(value = "报告时间")
    private Date reportTime;

    /**
     * 报告医生
     */
    @ApiModelProperty(value = "报告医生")
    private String reportDoctor;


    /**
     * 租户编号
     */
    @ApiModelProperty(value = "租户编号")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;

    /**
     * 报告类型 1：职业 2：健康
     */
    @ApiModelProperty(value = "报告类型 1：职业 2：健康")
    private Integer repType;

    private static final long serialVersionUID = 1L;
}
