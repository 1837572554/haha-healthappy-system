package com.healthappy.modules.system.service.dto;

import com.healthappy.modules.system.domain.BGroupDT;
import lombok.Data;


/**
 * @description 组合项目子项
 * @author sjc
 * @date 2021-08-4
 */
@Data
public class BGroupDTDto  extends BGroupDT {

}
