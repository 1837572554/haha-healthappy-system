package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.TImageDTO;
import com.healthappy.modules.system.domain.TImgTxtScan;
import com.healthappy.modules.system.domain.TImgTxtScanDTO;
import com.healthappy.modules.system.domain.TPatientImg;
import com.healthappy.modules.system.service.ImgTxtScanManageService;
import com.healthappy.modules.system.service.dto.ImgTxtScanDelDTO;
import com.healthappy.modules.system.service.dto.ImgTxtScanManageQueryCriteria;
import com.healthappy.modules.system.service.dto.ImgTxtScanQueryDTO;
import com.healthappy.modules.system.service.dto.ImgTxtScanSaveDTO;
import com.healthappy.modules.system.service.mapper.ImgTxtScanMapper;
import com.healthappy.modules.system.service.mapper.TPatientImgMapper;
import com.healthappy.utils.PageUtil;
import com.healthappy.utils.SecurityUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Jevany
 * @date 2022/2/15 17:15
 */
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class ImgTxtScanManageServiceImpl extends BaseServiceImpl<ImgTxtScanMapper, TImgTxtScan> implements ImgTxtScanManageService {

    private final TPatientImgMapper tPatientImgMapper;

    @Override
    public Map<String, Object> getList(ImgTxtScanManageQueryCriteria criteria) {
        //总条数
        Integer totalCount = baseMapper.getImgTxtScanCount(criteria);
        Integer[] pageBase = PageUtil.toStartEnd(criteria.getPageNum(), criteria.getPageSize(), totalCount);

        criteria.setStartIndex(pageBase[0]);

        Map<String, Object> map = new LinkedHashMap<>(5);

        map.put("content", baseMapper.getImgTxtScanManageList(criteria));
        map.put("totalElements", totalCount);
        map.put("totalPage", pageBase[3]);
        map.put("currentPage", pageBase[2]);
        map.put("pageSize", criteria.getPageSize());
        return map;
    }

    @Override
    public TImgTxtScanDTO getImgTxtScan(ImgTxtScanQueryDTO imgTxtScanQueryDTO) {
        /** 是否项目 */
        Boolean isItem = false;
        if ("4".equals(imgTxtScanQueryDTO.getImgTxtTypeId())) {
            isItem = true;
        }

        TImgTxtScanDTO tImgTxtScanDTO=new TImgTxtScanDTO();
        List<TImageDTO> imageDTOs=new ArrayList<>();
        if (isItem) {
            //项目类型
            imageDTOs = tPatientImgMapper.getImagesByPaIdGroupId(imgTxtScanQueryDTO.getPaId(), imgTxtScanQueryDTO.getGroupId());
        } else {
            TImgTxtScan one = baseMapper.selectOne(Wrappers.<TImgTxtScan>lambdaQuery()
                    .eq(TImgTxtScan::getPaId, imgTxtScanQueryDTO.getPaId())
                    .eq(TImgTxtScan::getImgTxtTypeId, imgTxtScanQueryDTO.getImgTxtTypeId()));
            if (ObjectUtil.isNull(one)) {
                return null;
            }
            tImgTxtScanDTO = BeanUtil.copyProperties(one, TImgTxtScanDTO.class);

            //指引单、信息表、信用承诺书
            TImageDTO imageDTO = tPatientImgMapper.getImageById(tImgTxtScanDTO.getFileId());
            if(ObjectUtil.isNotNull(imageDTO)) {
                imageDTOs.add(imageDTO);
            }
        }
        if(CollUtil.isNotEmpty(imageDTOs)){
            tImgTxtScanDTO.setImages(imageDTOs);
        } else {
            tImgTxtScanDTO.setImages(new ArrayList<>());
        }
        return tImgTxtScanDTO;
    }

    @Override
    public void saveImgTxtScan(ImgTxtScanSaveDTO imgTxtScanSaveDTO) {
        /** 是否项目 */
        Boolean isItem = false;
        if ("4".equals(imgTxtScanSaveDTO.getImgTxtTypeId())) {
            isItem = true;
        }
        if(isItem){
            //项目类型
            TPatientImg patientImg=new TPatientImg();
            patientImg.setPaId(imgTxtScanSaveDTO.getPaId());
            patientImg.setGroupId(imgTxtScanSaveDTO.getGroupId());
            patientImg.setFileId(imgTxtScanSaveDTO.getFileId());
            patientImg.setCreateTime(new Timestamp(System.currentTimeMillis()));
            tPatientImgMapper.insert(patientImg);
        } else {
            this.remove(Wrappers.<TImgTxtScan>lambdaUpdate().eq(TImgTxtScan::getPaId,imgTxtScanSaveDTO.getPaId())
                    .eq(TImgTxtScan::getImgTxtTypeId,imgTxtScanSaveDTO.getImgTxtTypeId()));
            TImgTxtScan imgTxtScan=new TImgTxtScan();
            imgTxtScan.setPaId(imgTxtScanSaveDTO.getPaId());
            imgTxtScan.setImgTxtTypeId(imgTxtScanSaveDTO.getImgTxtTypeId());
            imgTxtScan.setFileId(imgTxtScanSaveDTO.getFileId());
            imgTxtScan.setDoctor(SecurityUtils.getNickName());
            imgTxtScan.setCreateTime(new Timestamp(System.currentTimeMillis()));
            this.save(imgTxtScan);
        }

    }

    @Override
    public void delImgTxtScan(ImgTxtScanDelDTO imgTxtScanDelDTO) {
        /** 是否项目 */
        Boolean isItem = false;
        if ("4".equals(imgTxtScanDelDTO.getImgTxtTypeId())) {
            isItem = true;
        }
        if(isItem){
            //项目类型
            tPatientImgMapper.delete(Wrappers.<TPatientImg>lambdaUpdate().eq(TPatientImg::getPaId,imgTxtScanDelDTO.getPaId())
                    .eq(TPatientImg::getGroupId,imgTxtScanDelDTO.getGroupId()).eq(TPatientImg::getFileId,imgTxtScanDelDTO.getFileId()));
        } else {
            this.remove(Wrappers.<TImgTxtScan>lambdaUpdate().eq(TImgTxtScan::getPaId,imgTxtScanDelDTO.getPaId())
                    .eq(TImgTxtScan::getImgTxtTypeId,imgTxtScanDelDTO.getImgTxtTypeId()));

        }

    }
}