package com.healthappy.modules.system.rest.paycost;

import com.healthappy.dozer.service.IGenerator;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.TChargeComApplyPatient;
import com.healthappy.modules.system.service.PayApplyPatientService;
import com.healthappy.modules.system.service.dto.CheckoutListExportDTO;
import com.healthappy.modules.system.service.dto.CompanyPayApplyPatientCriteria;
import com.healthappy.utils.R;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@Api(tags = "缴费管理：单位缴费")
@RestController
@AllArgsConstructor
@RequestMapping("/payCostCompanyApply")
public class PayApplyPatientController {

    private final PayApplyPatientService payApplyPatientService;
    private final IGenerator generator;

    @Log("缴费管理：单位缴费——保存结账人员名单")
    @ApiOperation("缴费管理：单位缴费——保存结账人员名单，权限码：payPatient:insert")
    @PostMapping("/saveApplyPatient")
    @PreAuthorize("@el.check('payPatient:insert')")
    public R savePayPatient(@RequestBody @Valid Set<TChargeComApplyPatient> vo) {
//        for (TChargeComApplyPatient tc:vo) {
//            payApplyPatientService.savePayPatient(tc);
//        }
        payApplyPatientService.savePatient(vo);
        return R.ok();
    }

    @Log("缴费管理：单位缴费——按条件查找缴费人员（未生成表数据）")
    @ApiOperation("缴费管理：单位缴费——按条件查找缴费人员（未生成表数据），权限码：payPatient:getByScreen")
    @GetMapping("/getApplyPatientByScreen")
    @PreAuthorize("@el.check('payPatient:getByScreen')")
    public R getPayPatient(@Validated CompanyPayApplyPatientCriteria criteria) {
        return R.ok(payApplyPatientService.getList(criteria));
    }

    @Log("结账名单：结账名单导出")
    @ApiOperation("结账名单：结账名单导出，权限码：payPatient:getByScreen")
    @GetMapping("/export")
    @PreAuthorize("@el.check('payPatient:getByScreen')")
    @ResponseExcel(name = "结账名单")
    @Klock
    public List<CheckoutListExportDTO> export(@Validated CompanyPayApplyPatientCriteria criteria){
        return payApplyPatientService.export(criteria);
    }
}
