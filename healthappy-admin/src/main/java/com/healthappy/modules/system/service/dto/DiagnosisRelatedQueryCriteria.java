package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class DiagnosisRelatedQueryCriteria {

    @Query
    @ApiModelProperty("科室ID")
    private String deptId;

    @Query
    @ApiModelProperty("项目ID")
    private Long id;

    @Query
    @NotBlank(message = "流水号必填")
    @ApiModelProperty(value = "流水号",required = true)
    private String paId;
}
