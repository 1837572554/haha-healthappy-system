package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BTypeApplyQueryCriteria {

    @Query(type = Query.Type.INNER_LIKE)
    @ApiModelProperty(name = "name",value = "模糊")
    private String name;

    @Query
    private Long id;
}
