package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.healthappy.config.SeedIdGenerator;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 从业结论表
 * @author fang
 * @date 2021-06-21
 */
@Data
@TableName("B_Comment_C")
@SeedIdGenerator
public class BCommentC implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.INPUT)
    /**
     * id
     */
    private String id;

    /**
     * 结论
     */
    @ApiModelProperty("结论")
    private String comment;

    /**
     * 是否合格
     */
    @ApiModelProperty("是否合格")
    private String isQualify;

    /**
     * 排序号
     */
    @ApiModelProperty("排序号")
    private Integer dispOrder;

    /** 编号 */
    @ApiModelProperty("编号")
    private String code;

    /**
     * 医疗机构id(u_hospital_info的id)
     */
    @TableField(value = "tenant_id")//,fill = FieldFill.INSERT
    private String tenantId;
}
