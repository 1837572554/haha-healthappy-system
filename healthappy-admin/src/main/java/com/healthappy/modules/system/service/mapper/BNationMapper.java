package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BNation;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 民族
 * @author FGQ
 * @date 2021-06-17
 */
@Mapper
@Repository
public interface BNationMapper extends CoreMapper<BNation> {

}
