package com.healthappy.modules.system.service.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.TseeD;
import com.healthappy.modules.system.domain.WorkStation;
import com.healthappy.modules.system.service.TseeDService;
import com.healthappy.modules.system.service.WorkStationService;
import com.healthappy.modules.system.service.mapper.WorkStationMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author FGQ
 * @date 2021-09-08
 */
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class WorkStationServiceImpl extends BaseServiceImpl<WorkStationMapper, WorkStation> implements WorkStationService {

    private final TseeDService tseeDService;

    //region 体检号、条码号、项目申请号生成
    /**
     * 体检号、条码号、项目申请号生成
     * @param type  种子类型 1.体检号 2.条码号 3.项目申请号
     * @return
     */
    @Override
    public synchronized String generatePrimaryKey(Integer type) {
        DateTime dateTime = DateTime.now();
        TseeD tseeD = tseeDService.lambdaQuery().eq(TseeD::getSType,type).between(TseeD::getSDate,DateUtil.beginOfDay(dateTime),DateUtil.endOfDay(dateTime)).one();
        if(null == tseeD){
            tseeD = new TseeD();
            tseeD.setSDate(dateTime.toTimestamp());
            tseeD.setSType(type);
            tseeD.setSNum(0L);
            tseeDService.save(tseeD);
        }
        boolean flag =  tseeDService.updateById(tseeD);
        if(!flag){
            throw new BadRequestException("新增种子ID失败");
        }
        return createPk(tseeD.getSNum().intValue(),4,dateTime);
    }

    /**
     * 组装编号
     * @param pk
     * @param fillBeforeLen
     * @param tenantId
     * @param dateTime
     * @return
     */
    public String createPk(int pk,int fillBeforeLen,DateTime dateTime) {
        String code=this.getWorkStation();
        if(null!=dateTime){
            return dateTime.toString("yyMMdd") + code + StrUtil.fillBefore(pk + "", '0', 4);
        }
        else{
            return code+StrUtil.fillBefore(pk+"",'0',fillBeforeLen);
        }
    }

    /**
     * 获得指定tenantiID，启用的工作站编号
     * @param tenantId 租户ID
     * @return
     */
    @Override
    public String getWorkStation() {
        WorkStation workStation = this.lambdaQuery().eq(WorkStation::getIsEnable, true).one();
        if(null == workStation){
            throw new BadRequestException("工作站编号是空的!!!");
        }
        return workStation.getCode();
    }


}
