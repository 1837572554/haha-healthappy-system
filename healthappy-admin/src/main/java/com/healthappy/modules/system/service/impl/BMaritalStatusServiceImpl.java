package com.healthappy.modules.system.service.impl;

import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.BMaritalStatus;
import com.healthappy.modules.system.service.BMaritalStatusService;
import com.healthappy.modules.system.service.mapper.BMaritalStatusMapper;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA. Created by yutang 2021/9/2 0002  11:03 Description:
 */
@Service
public class BMaritalStatusServiceImpl extends BaseServiceImpl<BMaritalStatusMapper, BMaritalStatus>
    implements BMaritalStatusService {

}
