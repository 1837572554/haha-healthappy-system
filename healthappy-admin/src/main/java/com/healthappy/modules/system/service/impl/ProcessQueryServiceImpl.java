package com.healthappy.modules.system.service.impl;

import com.healthappy.modules.system.service.ProcessQueryService;
import com.healthappy.modules.system.service.dto.ProcessQueryDto;
import com.healthappy.modules.system.service.dto.ProcessQueryQueryCriteria;
import com.healthappy.modules.system.service.mapper.ProcessQueryMapper;
import com.healthappy.utils.FileUtil;
import com.healthappy.utils.PageUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * @author Jevany
 * @date 2022/1/26 10:41
 * @desc 体检进度查询 服务实现类
 */
@AllArgsConstructor
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class ProcessQueryServiceImpl implements ProcessQueryService {

    private final ProcessQueryMapper processQueryMapper;

    @Override
    public List<ProcessQueryDto> getProcessQueryList(ProcessQueryQueryCriteria criteria) {
        return processQueryMapper.getProcessQueryList(criteria);
    }

    @Override
    public Map<String, Object> getProcessQueryMap(ProcessQueryQueryCriteria criteria) {
        //总数据
        List<ProcessQueryDto> processQueryList = processQueryMapper.getProcessQueryList(criteria);
        //总条数
        Integer totalCount = processQueryList.size();

        //计算数据
        //未完成数
        Integer unFinishedNum = 0;
        //已完成数
        Integer finishedNum = 0;
        //已总检数
        Integer conclusionNum = 0;
        //已审核数
        Integer verifyNum = 0;
        //已打印数
        Integer printNum = 0;

        for (ProcessQueryDto dto : processQueryList) {
            if (dto.getFinished().equals(1)) {
                finishedNum++;
            } else {
                unFinishedNum++;
            }
            if (dto.getConclusion().equals(1)) {
                conclusionNum++;
            }
            if (dto.getVerify().equals(1)) {
                verifyNum++;
            }
            if (dto.getPrint().equals(1)) {
                printNum++;
            }
        }

        if(criteria.getIsExport().equals(false)){
            Integer[] pageBase= PageUtil.toStartEnd(criteria.getPageNum(), criteria.getPageSize(),totalCount);
            Map<String, Object> map = new LinkedHashMap<>(10);
            map.put("content", processQueryList.subList(pageBase[0],pageBase[1]));
            map.put("totalElements", totalCount);
            map.put("totalPage", pageBase[3]);
            map.put("currentPage", pageBase[2]);
            map.put("pageSize", criteria.getPageSize());

            map.put("unFinishedNum", unFinishedNum);
            map.put("finishedNum", finishedNum);
            map.put("conclusionNum", conclusionNum);
            map.put("verifyNum", verifyNum);
            map.put("printNum", printNum);

            return map;
        } else {
            Map<String, Object> map = new LinkedHashMap<>(6);
            map.put("content", processQueryList);

            map.put("unFinishedNum", unFinishedNum);
            map.put("finishedNum", finishedNum);
            map.put("conclusionNum", conclusionNum);
            map.put("verifyNum", verifyNum);
            map.put("printNum", printNum);
            return map;
        }


    }

    @Override
    public void download(List<ProcessQueryDto> all, HttpServletResponse response) throws IOException {
        List<Map<String,Object>> list=new ArrayList<>();
        for (ProcessQueryDto dto : all) {
            Map<String,Object> map=new HashMap<>();
            map.put("流水号",dto.getPaId());
            map.put("体检分类",dto.getPeTypeHdName());
            map.put("体检类别",dto.getPeTypeDtName());
            map.put("姓名",dto.getName());
            map.put("性别",dto.getSex());
            map.put("年龄",dto.getAge());
            map.put("单位",dto.getCompanyName());
            map.put("部门",dto.getDeptName());
            map.put("工种",dto.getJobName());
            list.add(map);
        }
        FileUtil.downloadExcel(list,response);
    }
}