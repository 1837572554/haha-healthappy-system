package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@TableName("T_CRITICAL")
@Data
/**
 * 危急值表对象
 * @author YJ
 * @date 2022/2/9 17:29
 */
@RenewLog
public class TCritical implements Serializable {
    /**
     * 自动编号
     */
    @ApiModelProperty(value = "自动编号")
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 体检编号
     */
    @ApiModelProperty(value = "体检编号")
    private String paId;

    /**
     * 身份证号
     */
    @ApiModelProperty(value = "身份证号")
    private String idNo;

    /**
     * 科室编号
     */
    @ApiModelProperty(value = "科室编号")
    private String deptId;

    /**
     * 组合项编号
     */
    @ApiModelProperty(value = "组合项编号")
    private String groupId;

    /**
     * 项目编号（子项）
     */
    @ApiModelProperty(value = "项目编号（子项）")
    private String itemId;

    /**
     * 病种编号
     */
    @ApiModelProperty(value = "病种编号")
    private String sicknessId;

    /**
     * 发生时间(科室保存时候、从LIS、PACS批量拉取数据后判断生成)
     */
    @ApiModelProperty(value = "发生时间(科室保存时候、从LIS、PACS批量拉取数据后判断生成)")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT)
    private Date occuTime;

    /**
     * 检查医生
     */
    @ApiModelProperty(value = "检查医生")
    private String checkDoctor;

    /**
     * 审核医生
     */
    @ApiModelProperty(value = "审核医生")
    private String verifyDoctor;

    /**
     * 处理医生
     */
    @ApiModelProperty(value = "处理医生")
    private String dealDoctor;

    /**
     * 处理情况
     */
    @ApiModelProperty(value = "处理情况")
    private String dealDetail;

    /**
     * 处理时间
     */
    @ApiModelProperty(value = "处理时间")
    private Date dealTime;

    /**
     * 确认状态（是否是危急值）1代表是危急值；0代表不是危急值
     */
    @ApiModelProperty(value = "确认状态（是否是危急值）1代表是危急值；0代表不是危急值")
    private String confirmState;

    /**
     * 通知方式
     */
    @ApiModelProperty(value = "通知方式")
    private String noticeWay;

    /**
     * 具体情况
     */
    @ApiModelProperty(value = "具体情况")
    private String detail;

    /**
     * 异常等级（类型）
     */
    @ApiModelProperty(value = "异常等级（类型）")
    private String affectLevel;

    /**
     * 转诊科室
     */
    @ApiModelProperty(value = "转诊科室")
    private String transferDept;

    /**
     * 租户编号
     */
    @ApiModelProperty(value = "租户编号")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;

//    /** 医生ID */
//    @ApiModelProperty(value = "医生ID",hidden = true)
//    @TableField(exist = false)
//    private Long checkDoctorId;

    private static final long serialVersionUID = 1L;
}
