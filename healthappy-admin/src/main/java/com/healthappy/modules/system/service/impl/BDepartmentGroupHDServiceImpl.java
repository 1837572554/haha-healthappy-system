package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.BDepartmentGroupHD;
import com.healthappy.modules.system.domain.vo.BDepartmentGroupHDVo;
import com.healthappy.modules.system.service.BCompanyService;
import com.healthappy.modules.system.service.BDepartmentGroupHDService;
import com.healthappy.modules.system.service.TidSeedService;
import com.healthappy.modules.system.service.dto.BDepartmentGroupHDDto;
import com.healthappy.modules.system.service.dto.BDepartmentGroupHDQueryCriteria;
import com.healthappy.modules.system.service.mapper.BDepartmentGroupHDMapper;
import com.healthappy.utils.SecurityUtils;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sjc
 * @date 2021-06-24
 */
@Service
@AllArgsConstructor//有自动注入的功能
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BDepartmentGroupHDServiceImpl extends BaseServiceImpl<BDepartmentGroupHDMapper, BDepartmentGroupHD>
		implements BDepartmentGroupHDService {

	//private final BDepartmentGroupHDMapper bDepartmentGroupHDMapper;
	private final BCompanyService bCompanyService;
	private final TidSeedService tidSeedService;
	private final IGenerator generator;

	/**
	 * 分页查询
	 *
	 * @param criteria
	 * @param pageable
	 * @return
	 */
	@Override
	public Map<String, Object> queryAll(BDepartmentGroupHDQueryCriteria criteria, Pageable pageable) {
		getPage(pageable);
		PageInfo<BDepartmentGroupHD> page = new PageInfo<BDepartmentGroupHD>(queryAll(criteria));
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("content", generator.convert(page.getList(), BDepartmentGroupHDDto.class));
		map.put("totalElements", page.getTotal());
		return map;
	}

	/**
	 * 查询分组全部数据
	 *
	 * @return Object
	 */
	@Override
	public List<BDepartmentGroupHD> queryAll(BDepartmentGroupHDQueryCriteria criteria) {
		return this.list(QueryHelpPlus.getPredicate(BDepartmentGroupHD.class, criteria));
	}

	/**
	 * 判断分组是否存在
	 *
	 * @param id 分组id
	 * @return
	 */
	@Override
	public boolean isExistDepartmentGroup(String id) {
		return this.getById(id) != null;
	}

	/**
	 * 获得单位下的部门分组编号
	 *
	 * @param companyId 单位编号
	 * @param tenantId  租户编号
	 * @return
	 */
	@Override
	public BDepartmentGroupHD getMaxIDDepartmentGroup(String companyId, String tenantId) {
		return baseMapper.getMaxDepartmentGroup(companyId, tenantId);
	}

	/**
	 * 根据单位编号，生成部门分组信息
	 *
	 * @param companyId 单位编号
	 * @param tenantId  租户编号
	 * @return
	 */
	@Override
	public String generateDepartmentGroup(String companyId, String tenantId) {
		if (!bCompanyService.isExistCompany(companyId)) {
			throw new BadRequestException("公司编号不存在");
		}
		String bDepartmentGroupId = tidSeedService.createASeendPrimaryKeyNoWorkStation("B_Department_Group_HD", 8);
		return companyId + bDepartmentGroupId;
	}

	@Override
	public void saveGroupHd(BDepartmentGroupHDVo resources, String ip) {
		BDepartmentGroupHD bDepartment = BeanUtil.copyProperties(resources, BDepartmentGroupHD.class);
		if (StrUtil.isBlank(resources.getId())) {
			if (this.count(
					new LambdaQueryWrapper<BDepartmentGroupHD>().eq(BDepartmentGroupHD::getName, resources.getName())
							.eq(BDepartmentGroupHD::getCompanyId, resources.getCompanyId())
							.eq(StrUtil.isNotEmpty(resources.getDepartmentId()), BDepartmentGroupHD::getDepartmentId,
									resources.getDepartmentId()).ne(BDepartmentGroupHD::getIsEnable, "0")) > 0) {
				throw new BadRequestException("新增部门分组【" + resources.getName() + "】名称已存在");
			}

			bDepartment.setId(this.generateDepartmentGroup(resources.getCompanyId(), SecurityUtils.getTenantId()));
		} else {
			if (this.count(new LambdaQueryWrapper<BDepartmentGroupHD>().ne(BDepartmentGroupHD::getId, resources.getId())
					.eq(BDepartmentGroupHD::getName, resources.getName())
					.eq(BDepartmentGroupHD::getCompanyId, resources.getCompanyId())
					.eq(StrUtil.isNotEmpty(resources.getDepartmentId()), BDepartmentGroupHD::getDepartmentId,
							resources.getDepartmentId()).ne(BDepartmentGroupHD::getIsEnable, "0")) > 0) {
				throw new BadRequestException("编辑部门分组【" + resources.getName() + "】名称已存在");
			}
		}

		bDepartment.setUpdateIp(ip);
		bDepartment.setUpdateDoc(SecurityUtils.getNickName());
		this.saveOrUpdate(bDepartment);
	}

	@Override
	public String copyDepartmentGroup(String companyId, String departmentId, String departmentGroupId) {
		/** 新分组id */
		String newDepartmentGroupId = this.generateDepartmentGroup(companyId, SecurityUtils.getTenantId());
		baseMapper.copyDepartmentGroup(companyId, departmentId, departmentGroupId, newDepartmentGroupId);
		return newDepartmentGroupId;
	}
}
