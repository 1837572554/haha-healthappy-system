package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @description 申请类型
 * @author sjc
 * @date 2021-08-3
 */
@Data
public class BTypeDeviceVo {


    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private Long id;


    /**
     * 排序
     */
    @ApiModelProperty("排序")
    private Integer dispOrder;

    /**
     * 名称
     */
    @NotBlank(message = "名称不能为空")
    @ApiModelProperty("名称")
    private String name;
}
