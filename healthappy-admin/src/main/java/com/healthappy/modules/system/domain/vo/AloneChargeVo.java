package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Data
public class AloneChargeVo {

    @NotBlank(message = "流水号不能为空")
    @ApiModelProperty("流水号")
    private String paId;

    /**
     * 现金
     */
    @ApiModelProperty(value = "现金")
    private BigDecimal money;

    /**
     * 银行卡
     */
    @ApiModelProperty(value = "银行卡")
    private BigDecimal card;

    /**
     * 微信
     */
    @ApiModelProperty(value = "微信")
    private BigDecimal weiXin;

    /**
     * 支付宝
     */
    @ApiModelProperty(value = "支付宝")
    private BigDecimal zhiFuBao;

    /**
     * 收据号
     */
    @NotBlank(message = "票据号不能为空")
    @ApiModelProperty(value = "票据号")
    private String receipt;

    /**
     * 票据类型
     */
    @NotNull(message = "票据类型不能为空")
    @ApiModelProperty(value = "票据类型")
    private Boolean receiptType;

    /**
     * 增长序列
     */
    @NotNull(message = "增长序列不能为空")
    @ApiModelProperty("增长序列")
    private List<Integer> addSequence;

    @NotNull(message = "标识不能为空")
    @ApiModelProperty("是否退费")
    private Boolean isRefund;
}
