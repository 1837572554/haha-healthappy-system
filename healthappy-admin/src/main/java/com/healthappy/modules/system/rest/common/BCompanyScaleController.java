package com.healthappy.modules.system.rest.common;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BCompanyScale;
import com.healthappy.modules.system.domain.vo.BCompanyScaleVo;
import com.healthappy.modules.system.service.BCompanyScaleService;
import com.healthappy.modules.system.service.dto.BCompanyScaleQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description 企业规模表 控制器
 * @author sjc
 * @date 2021-09-2
 */
@Slf4j
@Api(tags = "默认基础数据：企业规模")
@RestController
@AllArgsConstructor
@RequestMapping("/BCompanyScale")
public class BCompanyScaleController {

    private final BCompanyScaleService bCompanyScaleService;

    @Log("分页查询企业规模")
    @ApiOperation("分页查询企业规模，权限码：BCompanyScale:list")
    @GetMapping
    public R listPage(BCompanyScaleQueryCriteria criteria, Pageable pageable) {
        return R.ok(bCompanyScaleService.queryAll(criteria, pageable));
    }

    @Log("新增|修改企业规模")
    @ApiOperation("新增|修改企业规模，权限码：admin")
    @PostMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BCompanyScaleVo resources) {
        if(checkTypeIsRepeated(resources)){
            return R.error(500,"该企业规模类型已经存在");
        }

        BCompanyScale bTypeItem = new BCompanyScale();
        BeanUtil.copyProperties(resources,bTypeItem);
        if(bCompanyScaleService.saveOrUpdate(bTypeItem))
            return R.ok();
        return R.error(500,"方法异常");
    }

    private boolean checkTypeIsRepeated(BCompanyScaleVo resources) {
        return bCompanyScaleService.lambdaQuery().eq(BCompanyScale::getType,resources.getType())
                .ne(ObjectUtil.isNotNull(resources.getId()),BCompanyScale::getId,resources.getId()).count() > 0;
    }

    @Log("删除企业规模")
    @ApiOperation("删除企业规模，权限码：admin")
    @DeleteMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R remove(@RequestParam("id") Long id) {
        if(bCompanyScaleService.removeById(id))
            return R.ok();
        return R.error(500,"方法异常");
    }
}
