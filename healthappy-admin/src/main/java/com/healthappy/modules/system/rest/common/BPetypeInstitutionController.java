package com.healthappy.modules.system.rest.common;

import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BPetypeInstitution;
import com.healthappy.modules.system.service.BPetypeInstitutionService;
import com.healthappy.modules.system.service.dto.BPetypeInstitutionCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 医疗机构类型控制器
 * @author FGQ
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "默认基础配置：医疗机构类型")
@RestController
@ApiSort(40)
@AllArgsConstructor
@RequestMapping("/BPetypeInstitution")
public class BPetypeInstitutionController {

    private final BPetypeInstitutionService BPetypeInstitutionService;

    @Log("医疗机构类型")
    @ApiOperation("医疗机构类型，权限码：BPetypeInstitution:list")
    @GetMapping
    public R list(BPetypeInstitutionCriteria criteria, @PageableDefault(value = Integer.MAX_VALUE,sort = {"disp_order"},direction = Sort.Direction.ASC) Pageable pageable) {
        return R.ok(BPetypeInstitutionService.queryAll(criteria, pageable));
    }

    @Log("新增|修改医疗机构类型")
    @ApiOperation("新增|修改医疗机构类型，权限码：admin")
    @PostMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BPetypeInstitution resources) {
        return R.ok(BPetypeInstitutionService.saveOrUpdate(resources));
    }

    @Log("删除医疗机构类型")
    @ApiOperation("删除医疗机构类型，权限码：admin")
    @DeleteMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R remove(@RequestBody Set<Integer> ids) {
        return R.ok(BPetypeInstitutionService.removeByIds(ids));
    }
}
