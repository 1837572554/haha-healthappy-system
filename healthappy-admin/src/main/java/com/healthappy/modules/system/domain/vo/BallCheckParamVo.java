package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BallCheckParamVo {

    @ApiModelProperty("导入类型 (前端默认自动)")
    private Boolean importType;

    @ApiModelProperty("登记类型 (前端默认预约)")
    private Boolean registrationType;

    @ApiModelProperty("单位id")
    private String companyId;

    @ApiModelProperty("部门名称 (部门传回来名称)")
    private String departmentName;

    @ApiModelProperty("分组id")
    private String deptGroupId;
}
