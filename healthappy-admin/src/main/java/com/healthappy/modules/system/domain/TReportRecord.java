package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.config.SeedIdGenerator;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@ApiModel("报告发布记录表")
@TableName("T_REPORT_RECORD")
@Data
@SeedIdGenerator
@RenewLog
public class TReportRecord implements Serializable {
    /**
     * 记录编号
     */
    @ApiModelProperty(value = "记录编号")
    @TableId(type = IdType.INPUT)
    private String id;

    /**
     * 流水号
     */
    @ApiModelProperty(value = "流水号")
    private String paId;

    /**
     * 发布类型（操作类型）编号
     */
    @ApiModelProperty(value = "发布类型（操作类型）编号")
    private String releaseType;

    /**
     * 发布类型（操作类型）名称
     */
    @ApiModelProperty(value = "发布类型（操作类型）名称")
    @TableField(exist = false)
    private String releaseTypeName;

    /**
     * 报告类型
     */
    @ApiModelProperty(value = "报告类型")
    private String reportType;

    /**
     * 操作医生
     */
    @ApiModelProperty(value = "操作医生")
    private String doctor;

    /**
     * 撤销医生
     */
    @ApiModelProperty(value = "撤销医生")
    private String undoDoctor;

    /**
     * 撤销时间
     */
    @ApiModelProperty(value = "撤销时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date undoTime;

    /**
     * 领取医生
     */
    @ApiModelProperty(value = "领取医生")
    private String receiveDoctor;

    /**
     * 领取时间
     */
    @ApiModelProperty(value = "领取时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date receiveTime;

    /**
     * 撤销领取医生
     */
    @ApiModelProperty(value = "撤销领取医生")
    private String undoReceiveDoctor;

    /**
     * 撤销领取时间
     */
    @ApiModelProperty(value = "撤销领取时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date undoReceiveTime;

    /**
     * 租户编号
     */
    @ApiModelProperty(value = "租户编号")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    private static final long serialVersionUID = 1L;
}
