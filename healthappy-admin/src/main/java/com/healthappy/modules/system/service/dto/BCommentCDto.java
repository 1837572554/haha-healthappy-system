package com.healthappy.modules.system.service.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author FGQ
 * @date 2020-05-14
 */
@Data
public class BCommentCDto implements Serializable {
    /**
     * id
     */
    private String id;

    /**
     * 结论
     */
    private String comment;

    /**
     * 是否合格
     */
    private String isQualify;

    /** 编号 */
    private Integer code;

    /**
     * 排序号
     */
    private Integer dispOrder;
}
