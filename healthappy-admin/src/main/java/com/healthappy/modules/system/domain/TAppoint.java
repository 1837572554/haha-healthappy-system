package com.healthappy.modules.system.domain;


import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.config.SeedIdGenerator;
import com.healthappy.config.databind.FieldBind;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @description 预约表
 * @author sjc
 * @date 2021-06-28
 */
@Data
@ApiModel("预约表")
@TableName("T_Appoint")
@SeedIdGenerator
@RenewLog
public class TAppoint implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    @TableId(type = IdType.INPUT)
    private String id;


    /**
     * 人员名称
     */
    @ApiModelProperty("人员名称")
    private String name;

    /**
     * 性别
     */
    @ApiModelProperty("性别")
    @FieldBind(target = "sexText")
    private String sex;

    @TableField(exist = false)
    private String sexText;

    /**
     * 年龄
     */
    @ApiModelProperty("年龄")
    private Integer age;

    /**
     * 身份证号
     */
    @ApiModelProperty("身份证号")
    private String idNo;

    /**
     * 婚姻状况
     */
    @ApiModelProperty("婚姻状况")
    @FieldBind(target = "maritalText")
    private String marital;

    @TableField(exist = false)
    private String maritalText;

    /**
     * 手机号
     */
    @ApiModelProperty("手机号")
    private String mobile;

    /**
     * 部门名称
     */
    @ApiModelProperty("部门名称")
    private String departmentName;

    /**
     * 工号
     */
    @ApiModelProperty("工号")
    private String code;

    /** 工种编号-可能匹配不上则为空 */
    @ApiModelProperty("工种编号-可能匹配不上则为空")
    private String job;

    /**
     * 工种名称
     */
    @ApiModelProperty("工种名称")
    private String jobName;

    /**
     * 毒害种类
     */
    @ApiModelProperty("毒害种类")
    private String poisonType;


    /**
     * 套餐名称
     */
    @ApiModelProperty("套餐名称")
    private String packageName;

    /**
     * 附加套餐
     */
    @ApiModelProperty("附加套餐")
    private String addPackage;

    /**
     * 体检分类
     */
    @ApiModelProperty("体检分类")
    private String peType;

    /**
     * 体检类别
     */
    @ApiModelProperty("体检类别")
    private String type;

    /**
     * 工龄
     */
    @ApiModelProperty("工龄")
    private String workYears;

    /**
     * 社保号
     */
    @ApiModelProperty("社保号")
    private String ssNo;

    /**
     * 单位名称
     */
    @ApiModelProperty("单位名称")
    private String companyName;

    /**
     * 单位id
     */
    @ApiModelProperty("单位id")
    private String companyId;
    /**
     * 预约日期
     */
    @ApiModelProperty("预约日期")
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp appointDate;
    /**
     * 预约时间开始时间
     */
    @ApiModelProperty("预约时间开始时间")
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp appointDateStart;

    /**
     * 预约时间结束时间
     */
    @ApiModelProperty("预约时间结束时间")
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp appointDateEnd;

    /**
     * 签到日期
     */
    @ApiModelProperty("签到日期")
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp signDate;


    /**
     * 预约类型
     */
    @ApiModelProperty("预约类型")
    private String appointType;
    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String mark;
    /**
     * 付费状态 0否1是
     */
    @ApiModelProperty("付费状态")
    private String payStatus;
    /**
     * 付费类型
     */
    @ApiModelProperty("付费类型")
    private String payType;
    /**
     * 交易号
     */
    @ApiModelProperty("交易号")
    private String tradeOn;

    /**
     * 付费时间
     */
    @ApiModelProperty("付费时间")
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp payTime;


    /**
     * 付费日期
     */
    @ApiModelProperty("付费日期")
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp payReturnDate;

    @ApiModelProperty("")
    private String buyAccess;


    /**
     * 付费总价格
     */
    @ApiModelProperty("付费总价格")
    private Double totalPrice;

    @ApiModelProperty("部门ID")
    private String departmentId;

    /**
     * 总工龄
     */
    @ApiModelProperty("总工龄")
    private String totalYears;

    /**
     * 分组id
     */
    @ApiModelProperty("分组id")
    private String deptGroupId;

    /**
     * 分组名称
     */
    @ApiModelProperty("分组名称")
    private String deptGroupName;

    /**
     * 登记医生
     */
    @ApiModelProperty("登记医生")
    private String registerDoctor;

    /**
     * 收费类型
     */
    @ApiModelProperty("收费类型")
    private Integer chargeType;

    /**
     * 人员来源
     */
    @ApiModelProperty("人员来源")
    private String personnelSource;

    /**
     * 任务来源
     */
    @ApiModelProperty("任务来源")
    private String charsi;

    /**
     * 结算归口
     */
    @ApiModelProperty("结算归口")
    private String settlement;
    /**
     * 签到医生
     */
    @ApiModelProperty("签到医生")
    private String signDoctor;
    /**
     * 特殊减项目
     */
    @ApiModelProperty("特殊减项目")
    private String subtractGroupItem;
    /**
     * 微信对应id
     */
    @ApiModelProperty("微信对应id")
    private Long wxId;
    /**
     * 所属区域
     */
    @ApiModelProperty("所属区域")
    private String region;
    /**
     * 添加项目组合项编号
     */
    @ApiModelProperty("添加项目组合项编号")
    private String addGroupItemId;
    /**
     * 是否支付
     */
    @ApiModelProperty("是否支付")
    private String isPay;
    /**
     * 价格
     */
    @ApiModelProperty("价格")
    private String price;
    /**
     * 添加套餐编号
     */
    @ApiModelProperty("附加套餐")
    private String addPackageId;
    /**
     *
     */
    @ApiModelProperty("")
    private String post;
    /**
     * 附加组合项目
     */
    @ApiModelProperty("附加组合项目")
    private String addGroupItem;
    /**
     * 最后更新时间
     */
    @ApiModelProperty("最后更新时间")
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT)
    private Timestamp updateTime;
    /**
     * 业务员ID
     */
    @ApiModelProperty("业务员ID")
    private Long salesmanId;
    /**
     * 业务员姓名
     */
    @ApiModelProperty("业务员姓名")
    private String salesmanName;
    /**
     * 附加套餐ID
     */
    @ApiModelProperty("套餐ID")
    private String packageId;
    /**
     * 其他ID
     */
    @ApiModelProperty("其他ID")
    private Long otherId;
    /**
     * 微信上传状态,默认0，1为上传成功
     */
    @ApiModelProperty("微信上传状态")
    private String wxUploadStatus;
    /**
     * 交易号
     */
    @ApiModelProperty("微信上传时间")
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp wxUploadTime;
    /**
     * 微信上传者
     */
    @ApiModelProperty("微信上传者")
    private String wxUploadUser;
    /**
     * 医疗机构ID(U_Hospital_Info的ID)
     */
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;


    /** 体检编号，签到后反写到此 */
    @ApiModelProperty("体检编号，签到后反写到此")
    private String paId;
    
}
