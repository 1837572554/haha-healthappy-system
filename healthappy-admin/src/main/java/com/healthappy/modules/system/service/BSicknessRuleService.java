package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BSicknessRule;
import com.healthappy.modules.system.service.dto.*;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

/**
 * @description 病种规则表 服务层
 * @author sjc
 * @date 2021-06-22
 */
public interface BSicknessRuleService extends BaseService<BSicknessRule> {
    /**
     * 查询数据
     *
     * @return Object
     */
    Object queryAlls(BSicknessRuleQueryCriteria criteria);

    Object queryAll(BSicknessRuleQueryCriteria criteria, Pageable pageable);

    /**
     * 查询数据分页
     * @param criteria 条件
     * @return Map<String, Object>
     */
    List<BSicknessRule> queryAll(BSicknessRuleQueryCriteria criteria);

    List<BSicknessRuleDto> buildList(List<BSicknessRuleDto> ruleDtoList);

    Object tree(BItemTreeQueryCriteria criteria);

    Object search(SearchSicknessRuleCriteria criteria);

    /**
     * 文本类型的项目进行病种匹配
     * @param itemId 项目编号
     * @param result 项目结果
     * @param mark   项目判定
	 * @param sex 1.男 2.女
     * @return 项目结果与病种规则匹配后得到的病种结果集合
     */
    List<ItemSicknessMatchDto> txtItemSicknessMath(String itemId, String result,String mark,String sex);

    /**
     * 数值类型的项目进行病种匹配
     * @param itemId 项目编号
     * @param result 项目结果
     * @param mark   项目判定
	 * @param sex 1.男 2.女
     * @return 项目结果与病种规则匹配后得到的病种结果集合
     */
    List<ItemSicknessMatchDto> numItemSicknessMath(String itemId, String result, String mark,String sex);

    /**
     * 根据项目ID 获取病种规则，和病种名称
     * @param itemId
     * @return
     */
    List<SicknessByItemDto> getByItemId(String itemId);

    boolean removeByIds(Set<String> ids);
}
