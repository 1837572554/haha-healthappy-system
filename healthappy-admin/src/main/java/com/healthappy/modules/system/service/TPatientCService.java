package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TPatientC;

/**
 * @description 从业
 * @author fang
 * @date 2021-06-17
 */
public interface TPatientCService extends BaseService<TPatientC> {


}
