package com.healthappy.modules.system.rest.print.dto;

import com.healthappy.modules.system.domain.vo.BarcodeVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * 条码列表打印对象
 * @author Jevany
 * @date 2022/5/12 17:41
 */
@Data
@ApiModel("条码列表打印对象")
public class BarcodePrintVO implements Serializable {
	/** 流水号 */
	@NotBlank(message = "流水号不能为空")
	@ApiModelProperty("流水号")
	private String paId;

	/** 条码对象列表 */
	@NotEmpty(message = "条码对象列表不能为空")
	@ApiModelProperty("条码对象列表")
	private List<BarcodeVO> barcodes;

	private static final long serialVersionUID = 1L;
}