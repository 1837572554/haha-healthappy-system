package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BDept;
import com.healthappy.modules.system.domain.BTypeApply;
import com.healthappy.modules.system.service.dto.BDeptQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 部门 服务层
 * @author fang
 * @date 2021-06-17
 */
public interface BTypeApplyService extends BaseService<BTypeApply> {



}
