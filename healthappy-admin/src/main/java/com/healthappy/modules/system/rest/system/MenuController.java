package com.healthappy.modules.system.rest.system;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.Menu;
import com.healthappy.modules.system.domain.vo.MenuVo;
import com.healthappy.modules.system.service.MenuService;
import com.healthappy.modules.system.service.RoleService;
import com.healthappy.modules.system.service.UserService;
import com.healthappy.modules.system.service.dto.MenuDto;
import com.healthappy.modules.system.service.dto.MenuQueryCriteria;
import com.healthappy.modules.system.service.dto.UserDto;
import com.healthappy.utils.CacheConstant;
import com.healthappy.utils.R;
import com.healthappy.utils.RedisUtils;
import com.healthappy.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * @author hupeng
 * @date 2018-12-03
 */
@Api(tags = "系统：菜单管理")
@RestController
@RequestMapping("/api/menus")
@SuppressWarnings("unchecked")
public class MenuController {

	private final MenuService menuService;

	private final UserService userService;

	private final RoleService roleService;

	private final IGenerator generator;
	private final RedisUtils redisUtils;

	private static final String ENTITY_NAME = "menu";

	public MenuController(MenuService menuService, UserService userService, RoleService roleService,
			IGenerator generator, RedisUtils redisUtils) {
		this.menuService = menuService;
		this.userService = userService;
		this.roleService = roleService;
		this.generator = generator;
		this.redisUtils = redisUtils;
	}

	@ApiOperation("获取菜单权限按钮接口")
	@GetMapping(value = "/build/btn")
	public R buildBtn() {
		return R.ok(SecurityUtils.authorities());
	}

	@Log("导出菜单数据")
	@ApiOperation("导出菜单数据，权限码：menu:list")
	@GetMapping(value = "/download")
	@PreAuthorize("@el.check('menu:list')")
	@Klock
	public void download(HttpServletResponse response, MenuQueryCriteria criteria) throws IOException {
		menuService.download(generator.convert(menuService.queryAll(criteria), MenuDto.class), response);
	}

	@ApiOperation("获取前端所需菜单")
	@GetMapping(value = "/build")
	public R buildMenus() {
		List<MenuDto> menuDtoList = new ArrayList<>();
		Long userId = SecurityUtils.getUserId();
		//先获取缓存
		Object userMenu = redisUtils.hget(CacheConstant.USER_MENU, String.valueOf(userId));
		if (userMenu != null) {
			return R.ok((List<MenuVo>) userMenu);
		}
		if ("admin".equals(SecurityUtils.getUsername())) {
			menuDtoList = menuService.getAdminMenuList();
		} else {
			UserDto user = userService.findByName(SecurityUtils.getUsername());
			menuDtoList = menuService.findByRoles(roleService.findByUsersId(user.getId()));
		}
		List<MenuDto> menuDtos = (List<MenuDto>) menuService.buildTree(menuDtoList).get("content");
		List<MenuVo> menuVos = menuService.buildMenus(menuDtos);
		//写缓存
		redisUtils.hset(CacheConstant.USER_MENU, String.valueOf(userId), menuVos);
		return R.ok(menuVos);
	}


	@ApiOperation("返回全部的菜单，权限码：menu:list,roles:list")
	@GetMapping(value = "/tree")
	@PreAuthorize("@el.check('menu:list','roles:list')")
	public R getMenuTree() {
		return R.ok(menuService.getMenuTree(menuService.findByPid(0L)));
	}

	@Log("查询菜单")
	@ApiOperation("查询菜单，权限码：menu:list")
	@GetMapping
	@PreAuthorize("@el.check('menu:list')")
	public R getMenus(MenuQueryCriteria criteria) {
		List<MenuDto> menuDtoList = generator.convert(menuService.queryAll(criteria), MenuDto.class);
		return R.ok(menuService.buildTree(menuDtoList));
	}

	@Log("新增菜单")
	@ApiOperation("新增菜单，权限码：menu:add")
	@PostMapping
	@PreAuthorize("@el.check('menu:add')")
	@CacheEvict(cacheNames = {CacheConstant.MENU, CacheConstant.USER, CacheConstant.ROLE}, allEntries = true)
	@Klock
	public R create(@Validated @RequestBody Menu resources) {

		if (resources.getId() != null) {
			throw new BadRequestException("A new " + ENTITY_NAME + " cannot already have an ID");
		}

		return R.ok(menuService.create(resources));
	}

	@Log("检查组件名称是否重复")
	@ApiOperation("检查组件名称是否重复：menu:edit")
	@GetMapping("/checkComponentName/{id}/{componentName}")
	@PreAuthorize("@el.check('menu:edit')")
	public R checkComponentName(@PathVariable String id, @PathVariable String componentName) {
		Integer count = menuService.lambdaQuery().ne(Menu::getId, id).eq(Menu::getComponentName, componentName).count();
		if (count > 0) {
			return R.error("组件名称重复");
		}
		return R.ok();
	}

	@Log("修改菜单")
	@ApiOperation("修改菜单，权限码：menu:edit")
	@PutMapping
	@PreAuthorize("@el.check('menu:edit')")
	@CacheEvict(cacheNames = {CacheConstant.MENU, CacheConstant.USER, CacheConstant.ROLE}, allEntries = true)
	@Klock
	public R update(@Validated @RequestBody Menu resources) {
		menuService.update(resources);
		//清楚缓存
		redisUtils.hdel(CacheConstant.USER_MENU);
		return R.ok();
	}

	@Log("删除菜单")
	@ApiOperation("删除菜单，权限码：menu:delete")
	@DeleteMapping
	@PreAuthorize("@el.check('menu:delete')")
	@CacheEvict(cacheNames = {CacheConstant.MENU, CacheConstant.USER, CacheConstant.ROLE}, allEntries = true)
	@Klock
	public R delete(@RequestBody Set<Long> ids) {
		Set<Menu> menuSet = new HashSet<>();
		for (Long id : ids) {
			List<Menu> menuList = menuService.findByPid(id);
			Menu oneMenu = menuService.getOne(new LambdaQueryWrapper<Menu>().eq(Menu::getId, id));
			if (Optional.ofNullable(oneMenu).isPresent()) {
				menuSet.add(oneMenu);
				menuSet = menuService.getDeleteMenus(menuList, menuSet);
			}
		}
		if (Optional.ofNullable(menuSet).isPresent() && menuSet.size() > 0) {
			menuService.delete(menuSet);
		}
		//清楚缓存
		redisUtils.hdel(CacheConstant.USER_MENU);
		return R.ok();
	}

	@Log("清空用户菜单缓存")
	@ApiOperation("清空用户菜单缓存")
	@PostMapping("/clear")
	@Klock
	public R clear() {
		//删除用户菜单缓存
		redisUtils.hdel(CacheConstant.USER_MENU);
		//		redisUtils.hdel(CacheConstant.USER_MENU+"*");
		return R.ok();
	}
}
