package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BPackageDT;
import com.healthappy.modules.system.domain.BPoison;
import com.healthappy.modules.system.domain.vo.BPackageDTVo;
import com.healthappy.modules.system.domain.vo.BPoisonLiteVo;
import com.healthappy.modules.system.service.dto.BRegisterPoisonQueryLiteCriteria;
import com.healthappy.modules.system.service.dto.RegisterPoisonQueryCriteria;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 有害因素
 * @author sjc
 * @date 2021-08-9
 */
@Mapper
@Repository
public interface BPoisonMapper extends CoreMapper<BPoison> {

    @SqlParser(filter = true)
    List<BPoisonLiteVo> getPoisonIdNameJps(@Param("criteria") BRegisterPoisonQueryLiteCriteria criteria);

    List<BPackageDT> getPackageGroups(@Param("criteria") BRegisterPoisonQueryLiteCriteria criteria);

    /**
     * 查询有害因素集合下的组合项目
     * @author YJ
     * @date 2022/1/13 11:19
     * @param criteria
     * @return java.util.List〈com.healthappy.modules.system.domain.BPackageDT〉
     */
    List<BPackageDT> getPoisonGroups(@Param("criteria") RegisterPoisonQueryCriteria criteria);

    /**
     * 单个毒害查询
     * @author YJ
     * @date 2022/1/13 14:47
     * @param criteria
     * @return java.util.List〈com.healthappy.modules.system.domain.BPackageDT〉
     */
    List<BPackageDTVo> getPoisonGroup(@Param("criteria") RegisterPoisonQueryCriteria criteria);

    List<BPackageDT> getPackageList(@Param("poisonTypeList") List<String> poisonTypeList,@Param("peType") String peType);
}
