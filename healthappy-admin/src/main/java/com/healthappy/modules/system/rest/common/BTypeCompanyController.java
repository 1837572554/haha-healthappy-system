package com.healthappy.modules.system.rest.common;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BTypeCompany;
import com.healthappy.modules.system.domain.vo.BTypeCompanyVo;
import com.healthappy.modules.system.service.BTypeCompanyService;
import com.healthappy.modules.system.service.dto.BTypeCompanyQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description 企业经济类型 控制器
 * @author sjc
 * @date 2021-08-30
 */
@Slf4j
@Api(tags = "默认基础数据：企业经济类型")
@RestController
@AllArgsConstructor
@RequestMapping("/BTypeCompany")
public class BTypeCompanyController {

    private final BTypeCompanyService bTypeCompanyService;

    @Log("分页查询企业经济类型")
    @ApiOperation("分页查询企业经济类型，权限码：BTypeCompany:list")
    @GetMapping
    public R listPage(BTypeCompanyQueryCriteria criteria, Pageable pageable) {
        return R.ok(bTypeCompanyService.queryAll(criteria, pageable));
    }

    @Log("新增|修改企业经济类型")
    @ApiOperation("新增|修改企业经济类型，权限码：admin")
    @PostMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BTypeCompanyVo resources) {
        //如果名称重复
        if(checkBTypeCompanyName(resources)) {
            return R.error(500,"该企业经济类型名称已经存在");
        }
        BTypeCompany bTypeItem = new BTypeCompany();
        BeanUtil.copyProperties(resources,bTypeItem);
        if(bTypeCompanyService.saveOrUpdate(bTypeItem))
            return R.ok();
        return R.error(500,"方法异常");
    }

    @Log("删除企业经济类型")
    @ApiOperation("删除企业经济类型，权限码：admin")
    @DeleteMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R remove(@RequestParam("id") Long id) {
        if(bTypeCompanyService.removeById(id))
            return R.ok();
        return R.error(500,"方法异常");
    }

    /**
     * 用于判断名称是否存在
     * @param resources
     * @return
     */
    private Boolean checkBTypeCompanyName(BTypeCompanyVo resources) {
        //名称相同  id不同的情况下能查询到数据，那么就是名称重复
        return bTypeCompanyService.lambdaQuery().eq(BTypeCompany::getName,resources.getName())
                .ne(ObjectUtil.isNotNull(resources.getId()),BTypeCompany::getId,resources.getId()).count() > 0;

    }
}
