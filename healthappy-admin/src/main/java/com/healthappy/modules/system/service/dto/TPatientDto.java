package com.healthappy.modules.system.service.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.healthappy.modules.system.domain.TPatient;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
/**
 * @author sjc
 * @date 2021-06-28
 */
@Data
public class TPatientDto extends TPatient {

  @ApiModelProperty("部门名")
  private String depName;

  @ApiModelProperty("单位名")
  private String companyName;

  @ApiModelProperty("分组名")
  private String depGroupName;

  @ApiModelProperty("体检分类名称")
  private String peTypeHdName;

  @ApiModelProperty("体检类别名称")
  private String peTypeDtName;

  @ApiModelProperty("职业结论")
  private String commentZ;

  @ApiModelProperty("职业建议----处理意见")
  private String suggestZ;

}
