package com.healthappy.modules.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BTypeSectorHd;
import com.healthappy.modules.system.service.BTypeSectorHdService;
import com.healthappy.modules.system.service.dto.BTypeSectorHdDto;
import com.healthappy.modules.system.service.dto.BTypeSectorHdQueryCriteria;
import com.healthappy.modules.system.service.mapper.BTypeSectorHdMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BTypeSectorHdServiceImpl extends BaseServiceImpl<BTypeSectorHdMapper, BTypeSectorHd> implements BTypeSectorHdService {

    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BTypeSectorHdQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BTypeSectorHd> page = new PageInfo<BTypeSectorHd>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", generator.convert(page.getList(), BTypeSectorHdDto.class));
        map.put("totalElements", page.getTotal());
        return map;
    }

    @Override
    public List<BTypeSectorHd> queryAll(BTypeSectorHdQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BTypeSectorHdDto.class, criteria));
    }
}
