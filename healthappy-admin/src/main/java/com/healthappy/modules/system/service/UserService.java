package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.User;
import com.healthappy.modules.system.service.dto.UserDto;
import com.healthappy.modules.system.service.dto.UserQueryCriteria;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author hupeng
 * @date 2020-05-14
 */
public interface UserService extends BaseService<User> {

    /**
     * 查询数据分页
     *
     * @param criteria 条件
     * @param pageable 分页参数
     * @return Map<String, Object>
     */
    Map<String, Object> queryAll(UserQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria 条件参数
     * @return List<UserDto>
     */
    List<User> queryAll(UserQueryCriteria criteria);

    /**
     * 导出数据
     *
     * @param all      待导出的数据
     * @param response /
     * @throws IOException /
     */
    void download(List<UserDto> all, HttpServletResponse response) throws IOException;

    /**
     * 根据用户名查询
     *
     * @param userName /
     * @return /
     */
    UserDto findByName(String userName);

    /**
     * 根据用户ID获取数据
     * @author YJ
     * @date 2022/2/14 18:20
     * @param userId
     * @return com.healthappy.modules.system.service.dto.UserDto
     */
    UserDto findById(Long userId);



    /**
     * 修改密码
     *
     * @param username        用户名
     * @param encryptPassword 密码
     */
    void updatePass(String username, String encryptPassword);

    /**
     * 修改头像
     *
     * @param multipartFile 文件
     */
    void updateAvatar(MultipartFile multipartFile);

    /**
     * 修改邮箱
     *
     * @param username 用户名
     * @param email    邮箱
     */
    void updateEmail(String username, String email);

    /**
     * 新增用户
     *
     * @param resources /
     * @return /
     */
    boolean create(User resources);

    /**
     * 编辑用户
     *
     * @param resources /
     */
    void update(User resources);

    void delete(Set<Long> ids);

    List<User> getDockerList();

    /**
     * 用户权限判断 是否有对应的功能权限
     *
     * @param userId         用户ID
     * @param permissionName 权限名称
     * @return java.lang.Boolean 有权限为True
     * @author YJ
     * @date 2022/2/14 17:18
     */
    Boolean checkPermission(String userId, String permissionName);

    /**
     * 获得权限对应的用户列表
     *
     * @param permissionName
     * @return java.util.List〈com.healthappy.modules.system.domain.User〉
     * @author YJ
     * @date 2022/2/14 17:29
     */
    List<User> getPermissionUsers(String permissionName);

    /**
     * 根据标识权限获得对应的userId,userName,tenantId数据
     * @author YJ
     * @date 2022/3/17 13:59
     * @param tagPermission 标识权限名，如：critical_notification（危急值通知标识）
     * @return java.util.List〈com.healthappy.modules.system.domain.User〉userId,userName,tenantId数据
     */
    List<User> getUsersByTagPermission(String tagPermission);


}
