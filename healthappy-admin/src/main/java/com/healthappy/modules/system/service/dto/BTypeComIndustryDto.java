package com.healthappy.modules.system.service.dto;

import com.healthappy.modules.system.domain.BTypeComIndustry;
import lombok.Data;


/**
 * @description 企业从事行业类型表
 * @author sjc
 * @date 2021-08-30
 */
@Data
public class BTypeComIndustryDto extends BTypeComIndustry {
}
