package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BFeeTypeVo {

    /**
     * id
     */
    private String id;

    /**
     * 费别名称
     */
    @ApiModelProperty("费别名称")
    private String name;

    /**
     * 排序
     */
    @ApiModelProperty("排序")
    private Integer dispOrder;

    /**
     * 是否启用
     */
    @ApiModelProperty("是否启用")
    private String isEnable;


    /**
     * 删除标识
     */
    @ApiModelProperty("是否删除")
    private String delFlag="0";
}
