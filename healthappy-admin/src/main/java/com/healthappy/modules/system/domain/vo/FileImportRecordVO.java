package com.healthappy.modules.system.domain.vo;

import com.healthappy.modules.system.domain.FileImportRecord;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 *
 * @author Jevany
 * @date 2022/5/12 14:17
 */
@Data
public class FileImportRecordVO extends FileImportRecord implements Serializable {

	/** 创建者 */
	@ApiModelProperty("创建者")
	private String createName;

	private static final long serialVersionUID = 1L;
}