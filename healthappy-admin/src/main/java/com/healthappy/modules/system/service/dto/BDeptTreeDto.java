package com.healthappy.modules.system.service.dto;

import com.healthappy.modules.system.domain.BDept;
import com.healthappy.modules.system.domain.BItem;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class BDeptTreeDto implements Serializable {

    /**
     * id
     */
    @ApiModelProperty(value = "id",position = 1)
    private String id;

    /**
     * 科室名
     */
    @ApiModelProperty(value = "科室名",position = 2)
    private String name;


    @ApiModelProperty(value = "项目集合")
    private List<BItem> bItemList;
}
