package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 症状表
 * @author sjc
 * @date 2021-08-10
 */
@Data
@TableName("B_Symptom")
@ApiModel("症状表")
public class BSymptom implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 症状id
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("症状id")
    private Long id;

    /**
     * 症状名称
     */
    @ApiModelProperty("症状名称")
    private String name;

    /**
     * 正常结果
     */
    @ApiModelProperty("正常结果")
    private String value;

    /**
     * 是否启用
     */
    @ApiModelProperty("是否启用")
    private String isEnable;

    /**
     * 显示顺序
     */
    @ApiModelProperty("显示顺序")
    private Integer dispOrder;
}
