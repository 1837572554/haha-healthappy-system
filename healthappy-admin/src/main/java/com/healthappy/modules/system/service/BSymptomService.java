package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BSymptom;
import com.healthappy.modules.system.service.dto.BSymptomQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 症状表
 * @author sjc
 * @date 2021-08-10
 */
public interface BSymptomService extends BaseService<BSymptom> {
    Map<String, Object> queryAll(BSymptomQueryCriteria criteria, Pageable pageable);

    List<BSymptom> queryAll(BSymptomQueryCriteria criteria);
}
