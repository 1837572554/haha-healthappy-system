package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BSicknessRule;
import com.healthappy.modules.system.service.dto.ItemSicknessMatchDto;
import com.healthappy.modules.system.service.dto.SicknessByItemDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author sjc
 * @description 病种规则表
 * @date 2021-06-22
 */
@Mapper
@Repository
public interface BSicknessRuleMapper extends CoreMapper<BSicknessRule> {

    List<ItemSicknessMatchDto> txtItemSicknessMath(@Param("itemId") String itemId, @Param("result") String result, @Param("mark") String mark,@Param("sex") String sex);

    List<ItemSicknessMatchDto> numItemSicknessMath(@Param("itemId") String itemId, @Param("result") String result, @Param("mark") String mark,@Param("sex") String sex);

    List<SicknessByItemDto> getByItemId(@Param("itemId") String itemId);

    @Select("select * from B_Sickness_Rule ${ew.customSqlSegment}")
    List<BSicknessRule> getAll(@Param(Constants.WRAPPER) Wrapper<BSicknessRule> wrapper);
}
