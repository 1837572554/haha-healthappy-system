package com.healthappy.modules.system.rest.ballcheck;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.healthappy.annotation.AnonymousAccess;
import com.healthappy.config.ApplicationConstant;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.TAppoint;
import com.healthappy.modules.system.domain.vo.BallCheckUploadVo;
import com.healthappy.modules.system.service.BallCheckResetService;
import com.healthappy.modules.system.service.FileImportRecordService;
import com.healthappy.modules.system.service.TAppointService;
import com.healthappy.modules.system.service.TenantService;
import com.healthappy.modules.system.service.dto.BallCheckDto;
import com.healthappy.modules.system.service.dto.BallCheckExportDto;
import com.healthappy.modules.system.service.dto.DeptWorkloadQueryCriteria;
import com.healthappy.modules.system.service.wrapper.BallCheckWrapper;
import com.healthappy.utils.R;
import com.healthappy.utils.RedisUtil;
import com.healthappy.utils.SecurityConstants;
import com.healthappy.utils.SecurityUtils;
import com.pig4cloud.plugin.excel.annotation.RequestExcel;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @description 团检备单 控制器
 * @author FGQ
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "团检备单：团检备单")
@RestController
@AllArgsConstructor
@RequestMapping("/ballCheck")
public class BallCheckController {

	private final TAppointService tAppointService;
	private final BallCheckResetService ballCheckResetService;
	private final FileImportRecordService importRecordService;
	private final IGenerator generator;
	private final TenantService tenantService;

	@Log("团检备单")
	@ApiOperation("查询团检备单，权限码：BallCheck:list")
	@GetMapping
	@PreAuthorize("@el.check('BallCheck:list')")
	public R list(DeptWorkloadQueryCriteria criteria, Pageable pageable) {
		return R.ok(tAppointService.ballCheckQueryAll(criteria, pageable));
	}

	@Log("导出")
	@GetMapping("/export")
	@ApiOperation("导出：BallCheck:list")
	@AnonymousAccess
	@ResponseExcel(name = "团检备单")
	@Klock
	public List<BallCheckExportDto> list(DeptWorkloadQueryCriteria criteria) {
		List<TAppoint> appointList = tAppointService.queryAll(criteria);
		List<BallCheckDto> checkDtoList = BallCheckWrapper.build().listVO(appointList);
		return generator.convert(checkDtoList, BallCheckExportDto.class);
	}

	@Log("上传")
	@PostMapping("/upload/{fileName}")
	@ApiOperation("上传：BallCheck:list")
	@PreAuthorize("@el.check('BallCheck:list')")
	@Klock
	public R upload(
			//默认是预约,默认是自动
			@RequestParam(required = false, defaultValue = "1") Integer importType,
			@RequestParam(required = false, defaultValue = "1") Integer registrationType,
			@RequestParam(required = false) String companyId, @RequestParam(required = false) String departmentName,
			@RequestParam(required = false) String appointDateStart,
			@RequestParam(required = false) String appointDateEnd, @RequestParam(required = false) String deptGroupId,
			@RequestExcel List<BallCheckUploadVo> dataList, BindingResult bindingResult,
			@PathVariable String fileName) {
		//检测是否打开预约号源设置
		String tenantId = SecurityUtils.getTenantId();
		//获取数据前，先检测是否开启预约号源管理
		String appointSwitch = tenantService.getAppointSwitch(tenantId);
		if (ApplicationConstant.APPOINT_SWITCH.equals(appointSwitch) && registrationType == 0) {
			return R.error("开启预约号源管理，不支持登记功能");
		}
		//清空空的表格
		dataList = dataList.parallelStream().filter(k -> BeanUtil.isNotEmpty(k)).collect(Collectors.toList());
		//检查fileName是否重复
		fileName = importRecordService.checkFileName(fileName);
		if (StrUtil.isBlank(fileName)) {
			return R.error("文件正在上传，请勿重复上传");
		}
		importRecordService.saveImport(fileName, registrationType.toString());
		ballCheckResetService.upload(registrationType, appointDateStart, appointDateEnd,
				buildList(importType, companyId, departmentName, deptGroupId, dataList), fileName);
		return R.ok();
	}

	@Log("获取上传缓存")
	@GetMapping("/importCache")
	@ApiOperation("获取上传缓存：BallCheck:list")
	@PreAuthorize("@el.check('BallCheck:list')")
	private R getImportCache() {
		return R.ok(RedisUtil.get(SecurityConstants.BALL_CHECK_UPLOAD + SecurityUtils.getUsername()));
	}

	@Log("清空所有缓存")
	@DeleteMapping("/importCache")
	@ApiOperation("清空所有缓存：BallCheck:list")
	@PreAuthorize("@el.check('BallCheck:list')")
	private R delImportCache() {
		RedisUtil.del(SecurityConstants.BALL_CHECK_UPLOAD + SecurityUtils.getUsername());
		return R.ok();
	}

	private List<BallCheckUploadVo> buildList(Integer importType, String companyId, String departmentName,
			String deptGroupId, List<BallCheckUploadVo> dataList) {
		if (importType == 0) {
			if (StrUtil.isBlank(companyId)) {
				throw new BadRequestException("单位不能为空");
			}
			if (StrUtil.isBlank(departmentName)) {
				throw new BadRequestException("部门不能为空");
			}
			if (ObjectUtil.isNull(deptGroupId)) {
				throw new BadRequestException("分组不能为空");
			}
			return dataList.stream().peek(v -> {
				v.setCompanyId(companyId);
				v.setDepartmentName(departmentName);
				v.setDeptGroupId(deptGroupId);
			}).collect(Collectors.toList());
		}
		return dataList;
	}
}

