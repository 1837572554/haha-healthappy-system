package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.config.SeedIdGenerator;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import lombok.Data;

@ApiModel(value = "com-healthappy-modules-system-domain-TCharge")
@Data
@TableName("T_CHARGE")
@SeedIdGenerator
@RenewLog
public class TCharge implements Serializable {
    /**
     * 编号（用IdSeed生成）
     */
    @TableId(type = IdType.INPUT)
    @ApiModelProperty(value = "编号（用IdSeed生成）")
    private String id;

    /**
     * 流水号
     */
    @ApiModelProperty(value = "流水号")
    private String paId;

    /**
     * 现金
     */
    @ApiModelProperty(value = "现金")
    private BigDecimal money;

    /**
     * 银行卡
     */
    @ApiModelProperty(value = "银行卡")
    private BigDecimal card;

    /**
     * 微信
     */
    @ApiModelProperty(value = "微信")
    private BigDecimal weiXin;

    /**
     * 支付宝
     */
    @ApiModelProperty(value = "支付宝")
    private BigDecimal zhiFuBao;

    /**
     * 收费医生
     */
    @ApiModelProperty(value = "收费医生")
    private String chargeDoctor;

    /**
     * 收费时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(value = "收费时间")
    private Timestamp chargeTime;

    /**
     * 收据号
     */
    @ApiModelProperty(value = "票据号")
    private String receipt;

    /**
     * 票据类型
     */
    @ApiModelProperty(value = "票据类型")
    private Boolean receiptType;

    /**
     * 打印医生
     */
    @ApiModelProperty(value = "打印医生")
    private String printDoctor;

    /**
     * 打印时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(value = "打印时间")
    private Timestamp printTime;

    /**
     * 是否退费(不是退费就是收费,为空 作废)
     */
    @ApiModelProperty(value = "是否退费(不是退费就是收费)")
    private String refund;

    /**
     * 退费时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(value = "退费时间")
    private Timestamp refundTime;

    /**
     * 退费医生
     */
    @ApiModelProperty(value = "退费医生")
    private String refundDoctor;

    /**
     * 租户编号
     */
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    @ApiModelProperty(value = "租户编号")
    private String tenantId;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Timestamp createTime;

    /**
     * 已收价格
     */
    @ApiModelProperty(value = "已收价格")
    private BigDecimal alreadyPrice;

    /**
     * 应收价格
     */
    @ApiModelProperty(value = "应收价格")
    private BigDecimal shouldPrice;

    /**
     * 实收价格
     */
    @ApiModelProperty(value = "实收价格")
    private BigDecimal actualPrice;

    /**
     * 未收金额
     */
    @ApiModelProperty(value = "未收金额")
    private BigDecimal noPrice;


    private static final long serialVersionUID = 1L;
}
