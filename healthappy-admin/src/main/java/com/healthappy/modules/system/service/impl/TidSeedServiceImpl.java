package com.healthappy.modules.system.service.impl;

import cn.hutool.core.util.StrUtil;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.TidSeed;
import com.healthappy.modules.system.domain.WorkStation;
import com.healthappy.modules.system.service.TidSeedService;
import com.healthappy.modules.system.service.WorkStationService;
import com.healthappy.modules.system.service.mapper.TidSeedMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author FGQ
 * @date 2021-10-29
 */
@Service
@AllArgsConstructor
public class TidSeedServiceImpl extends BaseServiceImpl<TidSeedMapper, TidSeed> implements TidSeedService {

    private final WorkStationService workStationService;

    public TidSeed createASeedPrimaryKeyBase(String tableName) {
        WorkStation workStation = workStationService.lambdaQuery().eq(WorkStation::getIsEnable, true).select(WorkStation::getCode).one();
        if(null == workStation){
            throw new BadRequestException("工作站是空的");
        }
        String workStationCode = workStation.getCode();
        TidSeed tidSeed = baseMapper.selectByOne(workStationCode,tableName);
        if(null == tidSeed){
            tidSeed = new TidSeed();
            tidSeed.setTableName(tableName);
            tidSeed.setVersion(0L);
            tidSeed.setWorkStationCode(workStationCode);
            this.save(tidSeed);
        }
        boolean updResult = this.updateById(tidSeed);
        if(!updResult){
            throw new BadRequestException("新增种子ID失败");
        }
        return tidSeed;
    }

    @Override
    public synchronized String createASeedPrimaryKey(String tableName) {
        TidSeed tidSeed = createASeedPrimaryKeyBase(tableName);
        return tidSeed.getWorkStationCode()+StrUtil.fillBefore(tidSeed.getVersion() + "", '0',  baseMapper.getTableLen(tableName) - tidSeed.getWorkStationCode().length());
    }

    @Override
    public String createASeendPrimaryKeyNoWorkStation(String tableName,Integer keyLength)
    {
        TidSeed tidSeed = createASeedPrimaryKeyBase(tableName);
        return StrUtil.fillBefore(tidSeed.getVersion() + "", '0',  keyLength);
    }
}
