package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BCommentWzQueryCriteria {

    @Query
    @ApiModelProperty("问诊模板类型  0既往病史 1现病史 2家族史 3手术史")
    private String type;


    @Query(type = Query.Type.INNER_LIKE)
    @ApiModelProperty("结论")
    private String comment;
}
