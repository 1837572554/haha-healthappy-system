package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.healthappy.modules.system.domain.TChargeCOMApplyWujia;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @Author: YuTang
 * @Date: Created in 2022/3/29 9:03
 * @Description:
 * @Version: 1.0
 */
@Mapper
@Repository
public interface TChargeCOMApplyWujiaMapper extends BaseMapper<TChargeCOMApplyWujia> {

    /**
     * 根据体检编号查询出物价信息
     * @param tenantId 租户id
     * @param payApplyId 缴费申请单号
     * @return
     */
    void insertByApply(@Param("tenantId") String tenantId,@Param("payApplyId") String payApplyId);
}
