package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

/**
 * 体检报告发布 查询类
 *
 * @author Jevany
 * @date 2022/3/3 12:00
 */
@Data
public class ReportReleaseQueryCriteria {

    /**
     * 流水号
     */
    @ApiModelProperty("流水号")
    private String paId;

    /**
     * 姓名
     */
    @ApiModelProperty("姓名")
    private String name;

    /**
     * 体检时间类型Enum
     */
    @ApiModelProperty(value = "体检时间类型, 页面路径：reportRelease", position = 0)
    private Integer peDateType;

    /**
     * 时间段（数组）
     */
    @ApiModelProperty(value = "时间段（数组） ", position = 1)
    private List<Timestamp> timeList;

    /**
     * 体检分类表id
     */
    @ApiModelProperty("体检分类表id")
    private Long peTypeHdId;

    /**
     * 体检类别表id
     */
    @ApiModelProperty("体检类别表id")
    private Long peTypeDtId;

    /**
     * 打印选项 -1全部 0未打印 1已打印  默认-1（全部）
     */
    @ApiModelProperty("打印选项 -1全部 0未打印 1已打印  默认-1（全部）")
    private Integer print = -1;

    /**
     * 上传 -1全部 0未打印 1已打印  默认-1（全部）
     */
    @ApiModelProperty("上传 -1全部 0未打印 1已打印  默认-1（全部）")
    private Integer upload = -1;

    /**
     * 领取 -1全部 0未打印 1已打印  默认-1（全部）
     */
    @ApiModelProperty("领取 -1全部 0未打印 1已打印  默认-1（全部）")
    private Integer receive = -1;

    /**
     * 单位id
     */
    @ApiModelProperty("单位id")
    private String companyId;

    /**
     * 部门id
     */
    @ApiModelProperty("部门id")
    private String departmentId;

    /**
     * 分组id
     */
    @ApiModelProperty("分组id")
    private String departmentGroupId;

    /**
     * 套餐id
     */
    @ApiModelProperty("套餐id")
    private String packageId;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remarks;

//    /**
//     * 是否仅自己
//     */
//    @ApiModelProperty("是否仅自己")
//    private Boolean onlyOneself;

}