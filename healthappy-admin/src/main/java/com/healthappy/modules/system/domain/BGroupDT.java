package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @description 组合项目子项
 * @author sjc
 * @date 2021-08-4
 */
@Data
@TableName("B_Group_DT")
@ApiModel("组合项目子项")
public class BGroupDT implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 组合项目id
     */
    @ApiModelProperty("组合项目id")
    private String  groupId;

    /**
     * 项目编号
     */
    @ApiModelProperty("项目编号")
    private String itemId;


    /**
     * 最后更新时间
     */
    @ApiModelProperty("最后更新时间")
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp updateTime;

    /** 租户编号 */
    @ApiModelProperty("租户编号")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;
}
