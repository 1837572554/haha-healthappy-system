package com.healthappy.modules.system.service.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Jevany
 * @date 2022/3/4 16:24
 */
@Data
public class RoleMenuPidCountDTO implements Serializable {
    private Long pid;
    private Long menuCount;
}