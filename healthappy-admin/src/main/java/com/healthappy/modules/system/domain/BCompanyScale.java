package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 企业规模表
 * @author sjc
 * @date 2021-09-2
 */
@Data
@ApiModel("企业规模表")
@TableName("B_Company_Scale")
public class BCompanyScale implements Serializable {
    private static final long serialVersionUID = 1L;



    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 规模类型
     */
    @ApiModelProperty("规模类型")
    private String type;

    /**
     * 标准代码
     */
    @ApiModelProperty("标准代码")
    private String code;
}
