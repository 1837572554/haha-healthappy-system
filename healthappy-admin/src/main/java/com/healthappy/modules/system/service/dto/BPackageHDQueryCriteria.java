package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @description 套餐表
 * @author sjc
 * @date 2021-07-2
 */
@Data
public class BPackageHDQueryCriteria {

    /**
     *套餐id
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("套餐id")
    private String id;

    /**
     * 名称
     */
    @Query(blurry = "name,jp")
    @ApiModelProperty("名称")
    private String name;

    /**
     * 是否启用
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("是否启用")
    private String isEnable;

    /**
     * 体检类型
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("体检类型")
    private String type;

    /**
     * 适用性别
     */
    @ApiModelProperty("适用性别")
    private String sex;


    @ApiModelProperty(hidden = true , value = "性别集合String")
    @Query(type = Query.Type.IN,propName = "sex")
    private List<String> sexList;

}
