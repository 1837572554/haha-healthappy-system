package com.healthappy.modules.system.service;

import com.healthappy.modules.system.service.dto.ProcessQueryDto;
import com.healthappy.modules.system.service.dto.ProcessQueryQueryCriteria;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author Jevany
 * @date 2022/1/26 9:59
 * @desc 进度查询服务层
 */
public interface ProcessQueryService {

    /**
     * 根据条件查询体检进度数据,不分页
     * @author YJ
     * @date 2022/1/26 10:56
     * @param criteria
     * @return java.util.List〈com.healthappy.modules.system.service.dto.ProcessQueryDto〉
     */
    List<ProcessQueryDto> getProcessQueryList(ProcessQueryQueryCriteria criteria);

    /**
     * 根据条件查询体检进度数据，并分页
     * @author YJ
     * @date 2022/1/26 12:56
     * @param criteria
     * @return java.util.Map〈java.lang.String,java.lang.Object〉
     */
    Map<String, Object> getProcessQueryMap(ProcessQueryQueryCriteria criteria);

    /**
     * 导出下载
     * @author YJ
     * @date 2022/1/26 14:42
     * @param all
     * @param response
     */
    void download(List<ProcessQueryDto> all, HttpServletResponse response) throws IOException;
}