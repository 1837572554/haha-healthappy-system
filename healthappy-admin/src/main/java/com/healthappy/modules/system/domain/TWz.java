package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @description 问诊表
 * @author sjc
 * @date 2021-12-15
 */
@Data
@ApiModel("职业史放射表")
@TableName("T_Wz")
public class TWz implements Serializable {



    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    @TableId(type = IdType.AUTO)
    private Long id;


    /**
     * 体检号
     */
    @ApiModelProperty("体检号")
    private String paId;

    /**
     * 既往病史
     */
    @ApiModelProperty("既往病史")
    private String bsJwbs;

    /**
     * 病名
     */
    @ApiModelProperty("病名")
    private String bsBm;

    /**
     * 诊断时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty("诊断时间")
    private Timestamp bsZdsj;

    /**
     * 诊断单位
     */
    @ApiModelProperty("诊断单位")
    private String bsZddw;

    /**
     * 是否痊愈
     */
    @ApiModelProperty("是否痊愈")
    private String bsSfqy;

    /**
     * 初潮
     */
    @ApiModelProperty("初潮")
    private String bsCc;

    /**
     * 经期
     */
    @ApiModelProperty("经期")
    private String bsJq;

    /**
     * 周期
     */
    @ApiModelProperty("周期")
    private String bsZq;

    /**
     * 末次月经
     */
    @ApiModelProperty("末次月经")
    private String bsMcyj;

    /**
     * 现有子女
     */
    @ApiModelProperty("现有子女")
    private String bsXyzn;

    /**
     * 流产
     */
    @ApiModelProperty("流产")
    private String bsLc;

    /**
     * 早产
     */
    @ApiModelProperty("早产")
    private String bsZc;

    /**
     * 死产
     */
    @ApiModelProperty("死产")
    private String bsSc;

    /**
     * 异常胎
     */
    @ApiModelProperty("异常胎")
    private String bsYct;


    /**
     * 烟史
     */
    @ApiModelProperty("烟史  1从不吸烟  2偶尔吸烟   3经常吸烟")
    private String bsYs;

    /**
     * 酒史
     */
    @ApiModelProperty("酒史  1从不饮酒  2偶尔饮酒   3经常饮酒")
    private String bsJs;

    /**
     * 其他
     */
    @ApiModelProperty("其他")
    private String bsQt;

    /**
     * 症状
     */
    @ApiModelProperty("症状")
    private String zz;

    /**
     * 家族史
     */
    @ApiModelProperty("家族史")
    private String bsJzs;

    /**
     * 手术史
     */
    @ApiModelProperty("手术史")
    private String bsSss;

    /**
     * 睡眠情况
     */
    @ApiModelProperty("睡眠情况")
    private String bsSmqk;


    /**
     * 婚姻史-结婚日期
     */
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty("婚姻史-结婚日期")
    private Timestamp bsHysDate;

    /**
     * 婚姻史-放射历史
     */
    @ApiModelProperty("婚姻史-放射历史")
    private String bsHysFs;

    /**
     * 婚姻史-职业及健康状
     */
    @ApiModelProperty("婚姻史-职业及健康状")
    private String bsHysZy;

    /**
     * 烟史-烟龄
     */
    @ApiModelProperty("烟史-烟龄，每年吸的数量")
    private String ysYl;

    /**
     * 烟史-每天吸的数量
     */
    @ApiModelProperty("烟史-每天吸的数量")
    private String ysNs;

    /**
     * 酒史-酒龄
     */
    @ApiModelProperty("酒史-酒龄,每年喝的数量")
    private String jsJl;

    /**
     * 酒史-每天喝的数量
     */
    @ApiModelProperty("酒史-每天喝的数量")
    private String jsNs;

    /**
     * 操作医生
     */
    @ApiModelProperty("操作医生")
    private String doctor;

    /**
     * 医疗机构ID(U_Hospital_Info的ID)
     */
    @ApiModelProperty("医疗机构ID(U_Hospital_Info的ID)")
    private String tenantId;
}
