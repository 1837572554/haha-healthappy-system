package com.healthappy.modules.system.service.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CompanyPayApplyExportDto {

    @ColumnWidth(50)
    @ExcelProperty("收费单号")
    @ApiModelProperty("收费单号")
    private String id;

    @ColumnWidth(50)
    @ExcelProperty("申请时间")
    @ApiModelProperty("申请时间")
    private String applyTime;

    @ColumnWidth(50)
    @ExcelProperty("申请人")
    @ApiModelProperty("申请人")
    private String applyDoctor;

    @ColumnWidth(50)
    @ExcelProperty("发票抬头")
    @ApiModelProperty("发票抬头")
    private String invoiceTitle;

    @ColumnWidth(50)
    @ExcelProperty("税号")
    @ApiModelProperty("税号")
    private String taxNo;

    @ColumnWidth(50)
    @ExcelProperty("发票号")
    @ApiModelProperty("发票号")
    private String invoiceId;

    @ColumnWidth(50)
    @ExcelProperty("付款人")
    @ApiModelProperty("付款人")
    private String payRole;

    @ColumnWidth(50)
    @ExcelProperty("联系电话")
    @ApiModelProperty("联系电话")
    private String payPhone;

    @ColumnWidth(50)
    @ExcelProperty("备注")
    @ApiModelProperty("备注")
    private String mark;

}
