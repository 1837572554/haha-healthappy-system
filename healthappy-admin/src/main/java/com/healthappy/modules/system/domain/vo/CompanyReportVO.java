package com.healthappy.modules.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.sql.Timestamp;
import java.util.List;

/**
 * 单位报告提交类
 *
 * @author Jevany
 * @date 2022/2/17 17:50
 */
@Data
public class CompanyReportVO {

    /**
     * 报告编号,为空时新增
     */
    @ApiModelProperty(value = "报告编号,为空时新增")
    private String id;

    /**
     * 报告名称
     */
    @ApiModelProperty(value = "报告名称")
    private String reportName;

    /**
     * 检查编号
     */
    @ApiModelProperty(value = "检查编号")
    private String checkId;

    /**
     * 检查人数
     */
    @ApiModelProperty(value = "检查人数")
    private Integer checkNum;

    /**
     * 检查开始时间
     */
    @ApiModelProperty(value = "检查开始时间 yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp peDateStart;

    /**
     * 检查结束时间
     */
    @ApiModelProperty(value = "检查结束时间 yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Timestamp peDateEnd;

    /**
     * 单位编号
     */
    @ApiModelProperty(value = "单位编号")
    private String companyId;

    /**
     * 委托单位
     */
    @ApiModelProperty(value = "委托单位")
    private String entrustCompany;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remarks;

    /**
     * 总结（结论与意见）
     */
    @ApiModelProperty(value = "总结（结论与意见）")
    private String summary;

    /**
     * 流水号列表
     */
    @ApiModelProperty(value = "流水号列表")
    private List<String> paIdList;

    /**
     * 报告类型 1：职业 2：健康
     */
    @ApiModelProperty(value = "报告类型 1：职业 2：健康", hidden = true)
    private Integer repType;
}
