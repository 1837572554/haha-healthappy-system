package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 条码设置表
 * @author sjc
 * @date 2021-12-13
 */
@Data
public class BBarcodeDto implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 项目类型,来源字典表---项目组合主表类型
     */
    @ApiModelProperty(value = "项目类型",position = 2)
    private String groupHdType;

    /**
     * 条码总长度
     */
    @ApiModelProperty(value = "条码总长度",position = 3)
    private Integer barcodeLength;

    /**
     * 条码头内容
     */
    @ApiModelProperty(value = "条码头内容",position = 4)
    private Integer barcodeHead;

    /**
     * 种子，对应的条码在种子基础上+1
     */
    @ApiModelProperty(value = "条码种子",position = 6)
    private Integer seed;
}
