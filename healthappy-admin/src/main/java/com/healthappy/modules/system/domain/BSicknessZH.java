package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.healthappy.config.SeedIdGenerator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 病种组合子项
 * @author SJC
 * @date 2021-08-3
 */
@Data
@ApiModel("病种组合子项")
@TableName("B_Sickness_ZH")
@SeedIdGenerator
public class BSicknessZH implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     *主键id
     */
    @ApiModelProperty("主键id")
    @TableId(type = IdType.INPUT)
    private String id;


    /**
     * 病种组合id
     */
    @ApiModelProperty("病种组合id")
    private String sicknessZhId;

    /**
     * 病种id
     */
    @ApiModelProperty("病种id")
    private String sicknessId;

    /**
     * 是否包含
     */
    @ApiModelProperty("是否包含")
    private Long containsFlag;

    @ApiModelProperty("病种名称")
    @TableField(exist = false)
    private String sicknessName;

    /**
     * 医疗机构id(u_hospital_info的id)
     */
    @ApiModelProperty("医疗机构id(u_hospital_info的id)")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;

    @ApiModelProperty("病种规则ID")
    private String sicknessRuleId;
}
