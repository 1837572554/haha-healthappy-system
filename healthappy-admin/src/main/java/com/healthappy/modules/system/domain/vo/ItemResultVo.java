package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 小项结果集合
 *
 * @author yanjun
 * @date 2021/11/9
 */
@Data
@ApiModel("小项结果集合")
public class ItemResultVo {
    /**
     * 项目编号
     */
    @NotBlank(message = "项目编号必填")
    @ApiModelProperty(value = "项目编号", required = true)
    private String itemId;
    /**
     * 结果
     */
    @ApiModelProperty("结果")
    private String itemResult;
    /**
     * 判定
     */
    @ApiModelProperty("判定")
    private String itemMark;

    /**
     * 项目编码
     */
    @ApiModelProperty(value = "项目编码", hidden = true)
    private String itemCode;
}
