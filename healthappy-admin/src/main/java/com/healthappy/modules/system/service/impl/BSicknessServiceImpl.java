package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BSickness;
import com.healthappy.modules.system.domain.BSicknessAdvice;
import com.healthappy.modules.system.domain.BSicknessCause;
import com.healthappy.modules.system.domain.BSicknessScience;
import com.healthappy.modules.system.domain.vo.BSicknessVo;
import com.healthappy.modules.system.service.BSicknessAdviceService;
import com.healthappy.modules.system.service.BSicknessCauseService;
import com.healthappy.modules.system.service.BSicknessScienceService;
import com.healthappy.modules.system.service.BSicknessService;
import com.healthappy.modules.system.service.dto.BSicknessDto;
import com.healthappy.modules.system.service.dto.BSicknessQueryCriteria;
import com.healthappy.modules.system.service.mapper.BSicknessMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BSicknessServiceImpl extends BaseServiceImpl<BSicknessMapper, BSickness> implements BSicknessService {

    private final IGenerator generator;

    private final BSicknessAdviceService bSicknessAdviceService;

    private final BSicknessCauseService bSicknessCauseService;

    private final BSicknessScienceService bSicknessScienceService;

    @Override
    public Map<String, Object> queryAll(BSicknessQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BSickness> page = new PageInfo<>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        List<BSicknessDto> pageList = generator.convert(page.getList(), BSicknessDto.class);
        map.put("content", this.buildList(pageList));
        map.put("totalElements", page.getTotal());
        return map;
    }

    @Override
    public List<BSickness> queryAll(BSicknessQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BSicknessDto.class, criteria));
    }

    @Override
    public List<BSicknessDto> buildList(List<BSicknessDto> list) {
        for (BSicknessDto bSicknessDto:list) {
            String sicknessDtoId = bSicknessDto.getId();
            BSicknessAdvice sicknessAdvice = bSicknessAdviceService.getOne(Wrappers.<BSicknessAdvice>lambdaQuery().eq(BSicknessAdvice::getSicknessId, sicknessDtoId));
            if(Optional.ofNullable(sicknessAdvice).isPresent()){
                bSicknessDto.setSicknessAdviceName(StrUtil.isNotBlank(sicknessAdvice.getJy()) ? sicknessAdvice.getJy() : "");
                bSicknessDto.setSicknessAdviceId(ObjectUtil.isNotNull(sicknessAdvice.getId()) ? sicknessAdvice.getId() : null);
            }

            BSicknessCause sicknessCause = bSicknessCauseService.getOne(Wrappers.<BSicknessCause>lambdaQuery().eq(BSicknessCause::getSicknessId, sicknessDtoId));
            if(Optional.ofNullable(sicknessCause).isPresent()){
                bSicknessDto.setSicknessCauseName(StrUtil.isNotBlank(sicknessCause.getCause()) ? sicknessCause.getCause() : "");
                bSicknessDto.setSicknessCauseId(ObjectUtil.isNotNull(sicknessCause.getId()) ? sicknessCause.getId() : null);
            }

            BSicknessScience sicknessScience = bSicknessScienceService.getOne(Wrappers.<BSicknessScience>lambdaQuery().eq(BSicknessScience::getSicknessId, sicknessDtoId));
            if(Optional.ofNullable(sicknessScience).isPresent()){
                bSicknessDto.setSicknessScienceName(StrUtil.isNotBlank(sicknessScience.getScience()) ? sicknessScience.getScience() : "");
                bSicknessDto.setSicknessScienceId(ObjectUtil.isNotNull(sicknessScience.getId()) ? sicknessScience.getId() : null);
            }
        }
        return list;
    }
    @Override
    @Transactional
    public void saveOrUpdateSickness(BSicknessVo resources) {
        BSickness bSickness = new BSickness();
        BeanUtil.copyProperties(resources,bSickness);
        this.saveOrUpdate(bSickness);
        buildAdvice(bSickness.getId(),resources);
    }

    @Override
    @Transactional
    public void deleteSickness(Set<String> ids) {
        bSicknessAdviceService.remove(Wrappers.<BSicknessAdvice>lambdaUpdate().in(BSicknessAdvice::getSicknessId,ids));
        bSicknessCauseService.remove(Wrappers.<BSicknessCause>lambdaUpdate().in(BSicknessCause::getSicknessId,ids));
        bSicknessScienceService.remove(Wrappers.<BSicknessScience>lambdaUpdate().in(BSicknessScience::getSicknessId,ids));
        this.removeByIds(ids);
    }

    private void buildAdvice(String id, BSicknessVo resources) {
        String resourcesId = resources.getId();
        //建议
        String sicknessAdviceName = resources.getSicknessAdviceName();
//            bSicknessAdviceService.remove(Wrappers.<BSicknessAdvice>lambdaUpdate().eq(BSicknessAdvice::getSicknessId,resourcesId));
        BSicknessAdvice bSicknessAdvice = bSicknessAdviceService.getOne(Wrappers.<BSicknessAdvice>lambdaUpdate().eq(BSicknessAdvice::getSicknessId, resourcesId));
        if(!Optional.ofNullable(bSicknessAdvice).isPresent()) {
            bSicknessAdvice=new BSicknessAdvice();
            bSicknessAdvice.setSicknessId(id);
        }
        bSicknessAdvice.setJy(sicknessAdviceName);
        bSicknessAdviceService.saveOrUpdate(bSicknessAdvice);

        //常见原因
        String sicknessCauseName = resources.getSicknessCauseName();
//        bSicknessCauseService.remove(Wrappers.<BSicknessCause>lambdaUpdate().eq(BSicknessCause::getSicknessId,resourcesId));
        BSicknessCause bSicknessCause = bSicknessCauseService.getOne(Wrappers.<BSicknessCause>lambdaUpdate().eq(BSicknessCause::getSicknessId,resourcesId));
        if(!Optional.ofNullable(bSicknessCause).isPresent()) {
            bSicknessCause=new BSicknessCause();
            bSicknessCause.setSicknessId(id);
        }
        bSicknessCause.setCause(sicknessCauseName);
        bSicknessCauseService.saveOrUpdate(bSicknessCause);
        //医学解释
        String sicknessScienceName = resources.getSicknessScienceName();
        //bSicknessScienceService.remove(Wrappers.<BSicknessScience>lambdaUpdate().eq(BSicknessScience::getSicknessId,resourcesId));
        BSicknessScience bSicknessScience = bSicknessScienceService.getOne(Wrappers.<BSicknessScience>lambdaUpdate().eq(BSicknessScience::getSicknessId,resourcesId));
        if(!Optional.ofNullable(bSicknessScience).isPresent()) {
            bSicknessScience=new BSicknessScience();
            bSicknessScience.setSicknessId(id);
        }
        bSicknessScience.setScience(sicknessScienceName);
        bSicknessScienceService.saveOrUpdate(bSicknessScience);
    }
}
