package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import java.sql.Timestamp;
import java.util.List;
import lombok.Data;

/**
 * @description 体检人员查询
 * @author sjc
 * @date 2021-06-28
 */
@Data
public class TPatientQueryCriteria  {

    /**
     * 主键id
     */
    @Query(type = Query.Type.EQUAL)
    private String id;


    /**
     * 姓名
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("姓名")
    private String name;

    /**
     * 体检分类表id
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("体检分类表id")
    private Long peTypeHdId;

    /**
     * 体检类别表id
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("体检类别表id")
    private Long peTypeDtId;
    /**
     * 单位编号
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("单位编号")
    private String companyId;

    /**
     * 体检日期
     */
    @Query(type = Query.Type.BETWEEN)
    @ApiModelProperty("体检日期")
    private List<Timestamp> peDate;

    /**
     * 身份证
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("身份证")
    private String idNo;

    /**
     * 部门id
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("部门id")
    private String departmentId;

    /**
     * 分组id
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("分组id")
    private String departmentGroupId;

    /**
     * 档案号，一个身份证对应一个
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("档案号，一个身份证对应一个")
    private Long recordId;
}
