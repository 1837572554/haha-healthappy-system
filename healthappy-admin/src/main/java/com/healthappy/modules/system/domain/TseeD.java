package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 种子记录
 * @author FGQ
 */
@Data
@TableName("T_SEED")
public class TseeD implements Serializable {

    @TableId(type = IdType.AUTO)
    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("种子类型 1.体检号 2.条码号 3.项目申请号")
    private Integer sType;

    @ApiModelProperty("种子日期")
    private Timestamp sDate;

    @ApiModelProperty("种子数")
    @Version
    private Long sNum;

    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;
}
