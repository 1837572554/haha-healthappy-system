package com.healthappy.modules.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.BZyTz;
import com.healthappy.modules.system.domain.TZyAnswer;
import com.healthappy.modules.system.service.TZyAnswerService;
import com.healthappy.modules.system.service.dto.ZyTzScoreSum;
import com.healthappy.modules.system.service.mapper.BZyTzMapper;
import com.healthappy.modules.system.service.mapper.TZyAnswerMapper;
import com.healthappy.utils.StringUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @author Jevany
 * @date 2021/12/8 19:37
 * @desc 体检者问卷 答案 服务 实现类
 */
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class TZyAnswerServiceImpl extends BaseServiceImpl<TZyAnswerMapper, TZyAnswer> implements TZyAnswerService {

    private final String FILE_CONTENT_SPLIT_MARK = "\r\n";

    private final BZyTzMapper bZyTzMapper;

    @Override
    public Map<String, String> calcScoreBank1(String paId, Integer bankId) {
        Integer A = 0;//平和质
        Integer B = 0;//气虚质
        Integer C = 0;//阳虚质
        Integer D = 0;//阴虚质
        Integer E = 0;//痰湿质
        Integer F = 0;//湿热质
        Integer G = 0;//血瘀质
        Integer H = 0;//气郁质
        Integer I = 0;//特禀质

        //1,2,4,5,13          平和质 A 1下向，2、4、5、13 反向
        //2,3,4,14            气虚质 B
        //11,12,13,29         阳虚质 C
        //10，21，26，31      阴虚质 D
        //9，16，28，32       痰湿质 E
        //23，25，27，30     湿热质 F
        //19，22，24，33     血瘀质 G
        //5，6，7，8         气郁质 H
        //15，17，18，20     特禀质 I

        List<ZyTzScoreSum> scoreSum = baseMapper.getScoreSum(paId, bankId);

        String[] qIds = {"1", "2", "4", "5", "13"};
        List<TZyAnswer> TZyAnswerList = baseMapper.getAnswersByQuestionBankId(paId, bankId, Arrays.asList(qIds));

        for (TZyAnswer tZyAnswer : TZyAnswerList) {
            if (tZyAnswer.getQuestionId() == "1") {
                A += tZyAnswer.answerId;
            } else {
                A += negationScore(tZyAnswer.answerId);
            }
        }
        B = scoreSum.get(0).getScore();
        C = scoreSum.get(1).getScore();
        D = scoreSum.get(2).getScore();
        E = scoreSum.get(3).getScore();
        F = scoreSum.get(4).getScore();
        G = scoreSum.get(5).getScore();
        H = scoreSum.get(6).getScore();
        I = scoreSum.get(7).getScore();

        StringBuilder result = new StringBuilder();
        resultInit(A, B, C, D, E, F, H, result);

        StringBuilder resultDesc = new StringBuilder();
        StringBuilder tmp = new StringBuilder();
        StringBuilder diet = new StringBuilder();             //饮食
        StringBuilder living = new StringBuilder();           //起居
        StringBuilder movement = new StringBuilder();         //运动
        StringBuilder acupuncture = new StringBuilder();      //穴位
        StringBuilder emotion = new StringBuilder();          //情志调摄

        diet.append("饮食指导：" + FILE_CONTENT_SPLIT_MARK);
        living.append("起居指导：" + FILE_CONTENT_SPLIT_MARK);
        movement.append("运动指导：" + FILE_CONTENT_SPLIT_MARK);
        acupuncture.append("穴位指导：" + FILE_CONTENT_SPLIT_MARK);
        emotion.append("情志调摄指导：" + FILE_CONTENT_SPLIT_MARK);


        String mcName = "";     //类型名称

        mcName = "气虚质";
        if (B >= 11) {
            setContent(mcName, false, scoreSum.get(0).getContent(), tmp, resultDesc, diet, living, movement, acupuncture, emotion);
        } else if (B >= 9 && B < 11) {
            setContent(mcName, true, scoreSum.get(0).getContent(), tmp, resultDesc, diet, living, movement, acupuncture, emotion);
        }

        mcName = "阳虚质";
        if (C >= 11) {
            setContent(mcName, false, scoreSum.get(1).getContent(), tmp, resultDesc, diet, living, movement, acupuncture, emotion);
        } else if (C >= 9 && C < 11) {
            setContent(mcName, true, scoreSum.get(1).getContent(), tmp, resultDesc, diet, living, movement, acupuncture, emotion);
        }

        mcName = "阴虚质";
        if (D >= 11) {
            setContent(mcName, false, scoreSum.get(2).getContent(), tmp, resultDesc, diet, living, movement, acupuncture, emotion);
        } else if (D >= 9 && D < 11) {
            setContent(mcName, true, scoreSum.get(2).getContent(), tmp, resultDesc, diet, living, movement, acupuncture, emotion);
        }

        mcName = "痰湿质";
        if (E >= 11) {
            setContent(mcName, false, scoreSum.get(3).getContent(), tmp, resultDesc, diet, living, movement, acupuncture, emotion);
        } else if (E >= 9 && E < 11) {
            setContent(mcName, true, scoreSum.get(3).getContent(), tmp, resultDesc, diet, living, movement, acupuncture, emotion);
        }

        mcName = "湿热质";
        if (F >= 11) {
            setContent(mcName, false, scoreSum.get(4).getContent(), tmp, resultDesc, diet, living, movement, acupuncture, emotion);
        } else if (F >= 9 && F < 11) {
            setContent(mcName, true, scoreSum.get(4).getContent(), tmp, resultDesc, diet, living, movement, acupuncture, emotion);
        }

        mcName = "血瘀质";
        if (G >= 11) {
            setContent(mcName, false, scoreSum.get(5).getContent(), tmp, resultDesc, diet, living, movement, acupuncture, emotion);
        } else if (G >= 9 && G < 11) {
            setContent(mcName, true, scoreSum.get(5).getContent(), tmp, resultDesc, diet, living, movement, acupuncture, emotion);
        }

        mcName = "气郁质";
        if (H >= 11) {
            setContent(mcName, false, scoreSum.get(6).getContent(), tmp, resultDesc, diet, living, movement, acupuncture, emotion);
        } else if (H >= 9 && H < 11) {
            setContent(mcName, true, scoreSum.get(6).getContent(), tmp, resultDesc, diet, living, movement, acupuncture, emotion);
        }

        mcName = "特禀质";
        if (I >= 11) {
            setContent(mcName, false, scoreSum.get(7).getContent(), tmp, resultDesc, diet, living, movement, acupuncture, emotion);
        } else if (I >= 9 && I < 11) {
            setContent(mcName, true, scoreSum.get(7).getContent(), tmp, resultDesc, diet, living, movement, acupuncture, emotion);
        }


        if (A >= 17) {
            mcName = "平和质";

            if (B < 9 && C < 9 && D < 9 && E < 9 && F < 9 && G < 9 && H < 9 && I < 9) {
                tmp.delete(0, tmp.length());
                resultDesc.delete(0, resultDesc.length());

                setContent(mcName, false, getZyContent(mcName), tmp, resultDesc, diet, living, movement, acupuncture, emotion);
            } else if (B < 11 && C < 11 && D < 11 && E < 11 && F < 11 && G < 11 && H < 11 && I < 11) {
                tmp.delete(0, tmp.length());
                resultDesc.delete(0, resultDesc.length());

                tmp.append("基本是" + mcName);

                mcName = "气虚质";
                if (B >= 9) {
                    tmp.append(",有" + mcName + "倾向");
//                    resultDesc.append(mcName + FILE_CONTENT_SPLIT_MARK + scoreSum.get(0).getContent() + FILE_CONTENT_SPLIT_MARK);
                }

                mcName = "阳虚质";
                if (C >= 9) {
                    tmp.append(",有" + mcName + "倾向");
//                    resultDesc.append(mcName + FILE_CONTENT_SPLIT_MARK + scoreSum.get(1).getContent() + FILE_CONTENT_SPLIT_MARK);
                }

                mcName = "阴虚质";
                if (D >= 9) {
                    tmp.append(",有" + mcName + "倾向");
//                    resultDesc.append(mcName + FILE_CONTENT_SPLIT_MARK + scoreSum.get(2).getContent() + FILE_CONTENT_SPLIT_MARK);
                }

                mcName = "痰湿质";
                if (E >= 9) {
                    tmp.append(",有" + mcName + "倾向");
//                    resultDesc.append(mcName + FILE_CONTENT_SPLIT_MARK + scoreSum.get(3).getContent() + FILE_CONTENT_SPLIT_MARK);
                }

                mcName = "湿热质";
                if (F >= 9) {
                    tmp.append(",有" + mcName + "倾向");
//                    resultDesc.append(mcName + FILE_CONTENT_SPLIT_MARK + scoreSum.get(4).getContent() + FILE_CONTENT_SPLIT_MARK);
                }

                mcName = "血瘀质";
                if (G >= 9) {
                    tmp.append(",有" + mcName + "倾向");
//                    resultDesc.append(mcName + FILE_CONTENT_SPLIT_MARK + scoreSum.get(5).getContent() + FILE_CONTENT_SPLIT_MARK);
                }

                mcName = "气郁质";
                if (H >= 9) {
                    tmp.append(",有" + mcName + "倾向");
//                    resultDesc.append(mcName + FILE_CONTENT_SPLIT_MARK + scoreSum.get(6).getContent() + FILE_CONTENT_SPLIT_MARK);
                }

                mcName = "特禀质";
                if (I >= 9) {
                    tmp.append(",有" + mcName + "倾向");
//                    resultDesc.append(mcName + FILE_CONTENT_SPLIT_MARK + scoreSum.get(7).getContent() + FILE_CONTENT_SPLIT_MARK);
                }
                resultDesc.append(mcName + FILE_CONTENT_SPLIT_MARK + getZyContent(mcName) + FILE_CONTENT_SPLIT_MARK);
                setOtherContent(mcName, diet, living, movement, acupuncture, emotion);
            }
        }

        if (StringUtils.isBlank(tmp.toString())) {
            tmp.append("未检测到符合的体质!");
        }

        Map<String,String> map=new HashMap<>();
        map.put("result",tmp.toString().trim());
        map.put("desc",result.toString()
                + tmp.toString() + FILE_CONTENT_SPLIT_MARK
                + diet.toString() + FILE_CONTENT_SPLIT_MARK
                + living.toString() + FILE_CONTENT_SPLIT_MARK
                + movement.toString() + FILE_CONTENT_SPLIT_MARK
                + acupuncture.toString() + FILE_CONTENT_SPLIT_MARK
                + emotion);


        /*return result.toString()
                + tmp.toString() + FILE_CONTENT_SPLIT_MARK
                + diet.toString() + FILE_CONTENT_SPLIT_MARK
                + living.toString() + FILE_CONTENT_SPLIT_MARK
                + movement.toString() + FILE_CONTENT_SPLIT_MARK
                + acupuncture.toString() + FILE_CONTENT_SPLIT_MARK
                + emotion;*/
        return map;
    }


    @Override
    public Map<String, String> calcScoreBank2(String paId, Integer bankId) {
        List<ZyTzScoreSum> scoreSum = baseMapper.getScoreSum(paId, bankId);

        int A = GetScore(scoreSum.get(0));   //平和质
        int B = GetScore(scoreSum.get(1));   //气虚质
        int C = GetScore(scoreSum.get(2));   //阳虚质  气郁质
        int D = GetScore(scoreSum.get(3));   //阴虚质  湿热质
        int E = GetScore(scoreSum.get(4));   //痰湿质
        int F = GetScore(scoreSum.get(5));   //湿热质  特禀质
        int G = GetScore(scoreSum.get(6));   //血瘀质
        int H = GetScore(scoreSum.get(7));   //气郁质  阳虚质
        int I = GetScore(scoreSum.get(8));   //特禀质  阴虚质

        StringBuilder result = new StringBuilder();
        resultInit(A, B, C, D, E, F, H, result);

        StringBuilder resultDesc = new StringBuilder();
        StringBuilder tmp = new StringBuilder();

        String mcName = "气虚质";     //类型名称

        if (B >= 40) {
            setLiteContent(mcName, false, scoreSum.get(1).getContent(), tmp, resultDesc);
        } else if (B >= 30 && B < 40) {
            setLiteContent(mcName, true, scoreSum.get(1).getContent(), tmp, resultDesc);
        }

        mcName = "阳虚质";
        if (C >= 40) {
            setLiteContent(mcName, false, scoreSum.get(2).getContent(), tmp, resultDesc);
        } else if (C >= 30 && C < 40) {
            setLiteContent(mcName, true, scoreSum.get(2).getContent(), tmp, resultDesc);
        }

        mcName = "阴虚质";
        if (D >= 40) {
            setLiteContent(mcName, false, scoreSum.get(3).getContent(), tmp, resultDesc);
        } else if (D >= 30 && D < 40) {
            setLiteContent(mcName, true, scoreSum.get(3).getContent(), tmp, resultDesc);
        }

        mcName = "痰湿质";
        if (E >= 40) {
            setLiteContent(mcName, false, scoreSum.get(4).getContent(), tmp, resultDesc);
        } else if (E >= 30 && E < 40) {
            setLiteContent(mcName, true, scoreSum.get(4).getContent(), tmp, resultDesc);
        }

        mcName = "湿热质";
        if (F >= 40) {
            setLiteContent(mcName, false, scoreSum.get(5).getContent(), tmp, resultDesc);
        } else if (F >= 30 && F < 40) {
            setLiteContent(mcName, true, scoreSum.get(5).getContent(), tmp, resultDesc);
        }

        mcName = "血瘀质";
        if (G >= 40) {
            setLiteContent(mcName, false, scoreSum.get(6).getContent(), tmp, resultDesc);
        } else if (G >= 30 && G < 40) {
            setLiteContent(mcName, true, scoreSum.get(6).getContent(), tmp, resultDesc);
        }

        mcName = "气郁质";
        if (H >= 40) {
            setLiteContent(mcName, false, scoreSum.get(7).getContent(), tmp, resultDesc);
        } else if (H >= 30 && H < 40) {
            setLiteContent(mcName, true, scoreSum.get(7).getContent(), tmp, resultDesc);
        }

        mcName = "特禀质";
        if (I >= 40) {
            setLiteContent(mcName, false, scoreSum.get(8).getContent(), tmp, resultDesc);
        } else if (I >= 30 && I < 40) {
            setLiteContent(mcName, true, scoreSum.get(8).getContent(), tmp, resultDesc);
        }

        if(A>=60){
            mcName = "平和质";
            if (B < 30 && C < 30 && D < 30 && E < 30 && F < 30 && G < 30 && H < 30 && I < 30){
                tmp.delete(0,tmp.length());
                resultDesc.delete(0,resultDesc.length());
                setLiteContent(mcName, false, scoreSum.get(0).getContent(), tmp, resultDesc);
            } else if (B < 40 && C < 40 && D < 40 && E < 40 && F < 40 && G < 40 && H < 40 && I < 40) {
                tmp.delete(0,tmp.length());
                resultDesc.delete(0,resultDesc.length());
                tmp.append("基本是"+mcName);
                resultDesc.append(mcName + FILE_CONTENT_SPLIT_MARK + scoreSum.get(0).getContent() + FILE_CONTENT_SPLIT_MARK);

                mcName = "气虚质";
                if (B >= 30){
                    setLiteContent(mcName, true, scoreSum.get(1).getContent(), tmp, resultDesc);
                }

                mcName = "阳虚质";
                if (C >= 30){
                    setLiteContent(mcName, true, scoreSum.get(2).getContent(), tmp, resultDesc);
                }

                mcName = "阴虚质";
                if (D >= 30){
                    setLiteContent(mcName, true, scoreSum.get(3).getContent(), tmp, resultDesc);
                }

                mcName = "痰湿质";
                if (E >= 30){
                    setLiteContent(mcName, true, scoreSum.get(4).getContent(), tmp, resultDesc);
                }

                mcName = "湿热质";
                if (F >= 30){
                    setLiteContent(mcName, true, scoreSum.get(5).getContent(), tmp, resultDesc);
                }

                mcName = "血瘀质";
                if (G >= 30){
                    setLiteContent(mcName, true, scoreSum.get(6).getContent(), tmp, resultDesc);
                }

                mcName = "气郁质";
                if (H >= 30){
                    setLiteContent(mcName, true, scoreSum.get(7).getContent(), tmp, resultDesc);
                }

                mcName = "特禀质";
                if (I >= 30){
                    setLiteContent(mcName, true, scoreSum.get(8).getContent(), tmp, resultDesc);
                }
            }
        }

        Map<String, String> map=new HashMap<>();
        map.put("result",tmp.toString().trim());
        map.put("desc",result.toString()+tmp.toString()+FILE_CONTENT_SPLIT_MARK+resultDesc.toString());

//        return result.toString()+tmp.toString()+FILE_CONTENT_SPLIT_MARK+resultDesc.toString();
        return map;
    }

    private void resultInit(int a, int b, int c, int d, int e, int f, int h, StringBuilder result) {
        result.append("平和质：" + a + FILE_CONTENT_SPLIT_MARK);
        result.append("气虚质：" + b + "        气郁质：" + h + FILE_CONTENT_SPLIT_MARK);
        result.append("湿热质：" + f + "        痰湿质：" + e + FILE_CONTENT_SPLIT_MARK);
        result.append("阳虚质：" + c + "        阴虚质：" + d + FILE_CONTENT_SPLIT_MARK);
        result.append("结论：");
    }

    /**
     * 设置内容
     *
     * @param mcName      特质名称
     * @param isTendency  是否有倾向
     * @param tzContent
     * @param resultDesc
     * @param diet
     * @param living
     * @param movement
     * @param acupuncture
     * @param emotion
     * @author YJ
     * @date 2021/12/10 16:22
     */
    private void setContent(String mcName, Boolean isTendency, String tzContent, StringBuilder tmp, StringBuilder resultDesc, StringBuilder diet, StringBuilder living, StringBuilder movement, StringBuilder acupuncture, StringBuilder emotion) {
        setLiteContent(mcName, isTendency, tzContent, tmp, resultDesc);
        setOtherContent(mcName, diet, living, movement, acupuncture, emotion);
    }

    /**
     * 设置简单内容
     *
     * @param mcName
     * @param isTendency
     * @param tzContent
     * @param tmp
     * @param resultDesc
     * @author YJ
     * @date 2021/12/10 18:56
     */
    private void setLiteContent(String mcName, Boolean isTendency, String tzContent, StringBuilder tmp, StringBuilder resultDesc) {
        if (isTendency) {
            tmp.append("有" + mcName + "倾向 ");
        } else {
            tmp.append(mcName + " ");
        }
        resultDesc.append(mcName + FILE_CONTENT_SPLIT_MARK + tzContent + FILE_CONTENT_SPLIT_MARK);
    }


    /**
     * 设置其他内容
     *
     * @param mcName
     * @param diet
     * @param living
     * @param movement
     * @param acupuncture
     * @param emotion
     * @author YJ
     * @date 2021/12/10 19:00
     */
    private void setOtherContent(String mcName, StringBuilder diet, StringBuilder living, StringBuilder movement, StringBuilder acupuncture, StringBuilder emotion) {
        diet.append(getZyContent(mcName + "饮食") + FILE_CONTENT_SPLIT_MARK);
        living.append(getZyContent(mcName + "起居") + FILE_CONTENT_SPLIT_MARK);
        movement.append(getZyContent(mcName + "运动") + FILE_CONTENT_SPLIT_MARK);
        acupuncture.append(getZyContent(mcName + "穴位") + FILE_CONTENT_SPLIT_MARK);
        emotion.append(getZyContent(mcName + "情志调摄") + FILE_CONTENT_SPLIT_MARK);
    }

    /**
     * 获得中医体制特征
     *
     * @return java.lang.String
     * @author YJ
     * @date 2021/12/10 14:52
     */
    private String getZyContent(String name) {
        BZyTz bZyTz = bZyTzMapper.selectOne(new LambdaQueryWrapper<BZyTz>().eq(BZyTz::getName, name));
        if (Optional.ofNullable(bZyTz).isPresent() && StringUtils.isNotBlank(bZyTz.getContent())) {
            return bZyTz.getContent();
        }
        return "";
    }


    /**
     * 返回计分
     *
     * @param score 分数
     * @return java.lang.Integer
     * @author YJ
     * @date 2021/12/10 14:34
     */
    private Integer negationScore(Integer score) {
        switch (score) {
            case 1:
                return 5;
            case 2:
                return 4;
            case 3:
                return 3;
            case 4:
                return 2;
            case 5:
                return 1;
            default:
                return 0;
        }
    }

    private Integer GetScore(ZyTzScoreSum scoreSum) {
        Float tmp = ((scoreSum.getScore() - scoreSum.getCnt()) * 1f / (scoreSum.getCnt() * 4)) * 100;
        return Math.round(tmp);
    }


}
