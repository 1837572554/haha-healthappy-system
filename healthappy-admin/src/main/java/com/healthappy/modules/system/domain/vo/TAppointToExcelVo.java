package com.healthappy.modules.system.domain.vo;

import com.healthappy.modules.system.service.dto.TAppointDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * Created with IntelliJ IDEA. Created by yutang 2021/9/6 0006  16:52 Description:
 */
@Data
@ApiModel(value = "预约管理导出模型")
public class TAppointToExcelVo {

  @ApiModelProperty(value = "需要导出的列表")
  @NotNull(message = "列表数据不可为空")
  private List<TAppointDto> appoints;

}
