package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BDepartment;
import com.healthappy.modules.system.service.dto.BDepartmentQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 部门表 服务层
 * @author sjc
 * @date 2021-06-24
 */
public interface BDepartmentService extends BaseService<BDepartment> {

    Map<String, Object> queryAll(BDepartmentQueryCriteria criteria, Pageable pageable);

    /**
     * 查询全部数据
     * @return Object
     */
    List<BDepartment> queryAlls(BDepartmentQueryCriteria criteria);
    /**
     * 判断部门是否存在
     * @param department_id
     * @return
     */
    boolean isExistDepartment(String department_id);

    /**
     * 获得单位下最大的部门编号
     * @param companyId 单位编号
     * @param tenantId  租户编号
     * @return
     */
    BDepartment getMaxIDDepartment(String companyId,String tenantId);

    /**
     * 根据单位编号，生成部门编号
     * @param companyId 单位编号
     * @param tenantId  租户编号
     * @return
     */
    String generateDepartmentKey(String companyId,String tenantId);

    /**
     * 是否存在此租户和单位编号
     * @param companyId
     * @param tenantId
     * @return
     */
    boolean isExistCompanyID(String companyId,String tenantId);

}
