package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BItem;
import com.healthappy.modules.system.domain.TItemDT;
import com.healthappy.modules.system.service.dto.TItemDTQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;


/**
 * @description 体检人员子项目表 服务层
 * @author sjc
 * @date 2021-07-20
 */
public interface TItemDTService extends BaseService<TItemDT> {
    /**
     * 根据查询条件，获取体检子项分页数据
     * @param criteria 查询条件类
     * @param pageable 分页对象
     * @return
     */
    Map<String, Object> queryAll(TItemDTQueryCriteria criteria, Pageable pageable);

    /**
     * 根据查询条件，获取体检子项所有数据
     * @param criteria 查询条件类
     * @return
     */
    List<TItemDT> queryAll(TItemDTQueryCriteria criteria);


    /**
     * 保存体检子项
     * @param paId 体检号
     * @param groupId 组合项
     * @param sexId 性别Id
     * @param review 是否复查
     * @return 成功返回真，失败返回假
     */
    void saveItemDt(String paId, String groupId, String sexId,Integer review);

    /**
     * 通过体检号、组合项编号，删除体检子项数据
     * yanjun
     * @param paId 体检号
     * @param groupId 组合项编号
     * @return 返回真假
     */
    boolean delItemDtByPaIdGroupId(String paId, String groupId);

    /**
     * 删除指定体检号下，指定组合编号外的数据
     * @param paId
     * @param notInGroupIds
     * @return
     */
    boolean delItemDtNotInGroupIds(String paId,List<String> notInGroupIds);


    List<BItem> getGroupIdList(String groupId);

    /**
     * 清除同一个PaId中相同GroupId
     * @param paIds
     */
    void repeatItemId(List<String> paIds);
}
