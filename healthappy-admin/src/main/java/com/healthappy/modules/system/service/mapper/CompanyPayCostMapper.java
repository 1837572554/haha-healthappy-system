package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.annotation.IgnoreTenant;
import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TChargeComApply;
import com.healthappy.modules.system.service.dto.CompanyPayApplyPatientCriteria;
import com.healthappy.modules.system.service.dto.CompanyPayCostCriteria;
import com.healthappy.modules.system.service.dto.CompanyReportOccupationPersonQueryCriteria;
import com.healthappy.modules.system.service.dto.CompanyReportPersonDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 单位缴费
 * @author sqoy
 * @date 2022-02-25
 */
@Mapper
@Repository
public interface CompanyPayCostMapper extends CoreMapper<TChargeComApply> {

    /**
     * 查询列表
     * @param criteria
     * @return
     */
    List<TChargeComApply> getList(@Param("criteria") CompanyPayCostCriteria criteria);

    @SqlParser(filter = true)
    List<CompanyReportPersonDTO> getListByScreen(@Param("criteria") CompanyPayApplyPatientCriteria criteria, @Param("tenantId") String tenantId);

    @SqlParser(filter = true)
    Integer getListByScreenCount(@Param("criteria") CompanyPayApplyPatientCriteria criteria,@Param("tenantId") String tenantId);


}
