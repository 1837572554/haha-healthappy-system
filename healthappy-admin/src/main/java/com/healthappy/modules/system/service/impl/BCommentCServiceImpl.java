package com.healthappy.modules.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BCommentC;
import com.healthappy.modules.system.service.BCommentCService;
import com.healthappy.modules.system.service.dto.BCommentCDto;
import com.healthappy.modules.system.service.dto.BCommentCQueryCriteria;
import com.healthappy.modules.system.service.mapper.BCommentCMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BCommentCServiceImpl extends BaseServiceImpl<BCommentCMapper, BCommentC> implements BCommentCService {

    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BCommentCQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BCommentC> page = new PageInfo<BCommentC>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", generator.convert(page.getList(), BCommentCDto.class));
        map.put("totalElements", page.getTotal());
        return map;
    }

    @Override
    public List<BCommentC> queryAll(BCommentCQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BCommentCDto.class, criteria));
    }
}
