package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @description 有害因素
 * @author FGQ
 * @date 2021-10-21
 */
@Data
public class BRegisterPoisonQueryCriteria {

    /** 毒害编号 */
    @ApiModelProperty("毒害编号，带上code可查询单个毒害下的列表")
    @Query(type = Query.Type.EQUAL)
    private String id;

    /**
     * 毒害名称
     */
    @Query(type = Query.Type.BETWEEN_LIKE)
    @ApiModelProperty("毒害名称")
    private List<String> poisonName;

    /**
     * 体检类别
     */
    @ApiModelProperty("体检类别 1:上岗前，2：在岗期间，4：离岗后，5：应急")
    private Integer code;
}
