package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.*;
import lombok.Data;

import java.io.Serializable;

/**
 * 单位报告病种人员列表Dto
 *
 * @author Jevany
 * @date 2022/2/23 16:13
 */
@Data
public class CompanyReportSicknessPersonListDTO  implements Serializable {
    /**
     * 病种名称
     */
    @ApiModelProperty("病种名称")
    private String sicknessName;

    /**
     * 姓名
     */
    @ApiModelProperty("姓名")
    private String name;

    /**
     * 性别
     */
    @ApiModelProperty("性别")
    private String sex;

    /**
     * 年龄
     */
    @ApiModelProperty("年龄")
    private String age;

    /**
     * 电话
     */
    @ApiModelProperty("电话")
    private String phone;

    /**
     * 身份证
     */
    @ApiModelProperty("身份证")
    private String idNo;

}