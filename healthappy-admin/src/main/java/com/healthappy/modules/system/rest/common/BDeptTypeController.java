package com.healthappy.modules.system.rest.common;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BDeptType;
import com.healthappy.modules.system.service.BDeptTypeService;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 科室类型控制器
 * @author FGQ
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "默认基础数据：科室类型")
@ApiSort(2)
@RestController
@AllArgsConstructor
@RequestMapping("/BDeptType")
public class BDeptTypeController {

    private final BDeptTypeService BDeptTypeService;

    @Log("科室类型")
    @ApiOperation("科室类型，权限码：BDeptType:list")
    @GetMapping
    public R list() {
        return R.ok(BDeptTypeService.list(Wrappers.<BDeptType>lambdaQuery().eq(BDeptType::getIsEnable,true)));
    }

    @Log("新增|修改科室类型")
    @ApiOperation("新增|修改科室类型，权限码：admin")
    @PostMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BDeptType resources) {
        return R.ok(BDeptTypeService.saveOrUpdate(resources));
    }

    @Log("删除科室类型")
    @ApiOperation("删除科室类型，权限码：admin")
    @DeleteMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R remove(@RequestBody Set<Integer> ids) {
        return R.ok(BDeptTypeService.removeByIds(ids));
    }
}
