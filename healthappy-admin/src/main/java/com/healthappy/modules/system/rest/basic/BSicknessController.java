package com.healthappy.modules.system.rest.basic;

import cn.hutool.core.util.StrUtil;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.vo.BSicknessVo;
import com.healthappy.modules.system.service.BSicknessService;
import com.healthappy.modules.system.service.dto.BSicknessQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 病种设置 控制器
 * @author FGQ
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "基础管理：健康疾病建议库")
@RestController
@ApiSort(6)
@AllArgsConstructor
@RequestMapping("/BSickness")
public class BSicknessController {

    private final BSicknessService bSicknessService;

    @Log("查询病种")
    @ApiOperation("查询病种，权限码：BSickness:list")
    @GetMapping
    public R list(BSicknessQueryCriteria criteria,Pageable pageable) {
        return R.ok(bSicknessService.queryAll(criteria, pageable));
    }

    @Log("查询所有病种")
    @ApiOperation("查询所有病种，权限码：BSickness:list")
    @GetMapping("/all")
    public R all(BSicknessQueryCriteria criteria) {
        if(StrUtil.isBlank(criteria.getName())){
            throw new BadRequestException("查询条件不能为空");
        }
        return R.ok(bSicknessService.queryAll(criteria));
    }

    @Log("新增|修改病种")
    @ApiOperation("新增|修改病种，权限码：BSickness:add")
    @PostMapping
    @PreAuthorize("@el.check('BSickness:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BSicknessVo resources) {
        bSicknessService.saveOrUpdateSickness(resources);
        return R.ok();
    }

    @Log("删除病种")
    @ApiOperation("删除病种，权限码：BSickness:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('BSickness:delete')")
    @Klock
    public R remove(@RequestBody Set<String> ids) {
        bSicknessService.deleteSickness(ids);
        return R.ok();
    }
}
