package com.healthappy.modules.system.service.dto;

import cn.hutool.core.lang.tree.Tree;
import com.healthappy.modules.system.domain.BPetypeInstitution;
import com.healthappy.modules.system.domain.Tenant;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * @author hupeng
 * @date 2020-05-14
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class TenantDto extends Tenant {

    @ApiModelProperty("医疗机构编号")
    private BPetypeInstitution bPetypeInstitution;


    @ApiModelProperty("区域树")
    private List<Tree<String>> areaTree;
}
