package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BTypeSectorDt;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 从业体检类别
 * @author fang
 * @date 2021-06-17
 */
@Mapper
@Repository
public interface BTypeSectorDtMapper extends CoreMapper<BTypeSectorDt> {

}
