package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TReportRecord;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 体检报告发布记录 数据操作
 * @author Jevany
 * @date 2022/3/7 9:51
 */
@Mapper
@Repository
public interface TReportRecordMapper extends CoreMapper<TReportRecord> {

    List<TReportRecord> listReportRecord(@Param("paId") String paId);
}
