package com.healthappy.modules.system.service.dto;

import com.healthappy.modules.system.domain.BGroupHd;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author FGQ
 * @date 2020-05-14
 */
@Data
public class BGroupHdDto extends BGroupHd {

    @ApiModelProperty("项目数量")
    private Integer itemCount;
}
