package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.config.TenantConstant;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.exception.EntityExistException;
import com.healthappy.modules.system.domain.Menu;
import com.healthappy.modules.system.domain.Role;
import com.healthappy.modules.system.domain.RolesMenus;
import com.healthappy.modules.system.service.RoleService;
import com.healthappy.modules.system.service.RolesMenusService;
import com.healthappy.modules.system.service.dto.*;
import com.healthappy.modules.system.service.mapper.MenuMapper;
import com.healthappy.modules.system.service.mapper.RoleMapper;
import com.healthappy.utils.FileUtil;
import com.healthappy.utils.StringUtils;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

/**
 * @author hupeng
 * @date 2020-05-14
 */
@Service
@AllArgsConstructor//有自动注入的功能
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class RoleServiceImpl extends BaseServiceImpl<RoleMapper, Role> implements RoleService {


    private final IGenerator generator;
    private final RoleMapper roleMapper;
    private final MenuMapper menuMapper;
    private final RolesMenusService rolesMenusService;

    @Override
    public Map<String, Object> queryAll(RoleQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<Role> page = new PageInfo<>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", generator.convert(page.getList(), RoleDto.class));
        map.put("totalElements", page.getTotal());
		map.put("totalPage", page.getPages());
        return map;
    }

    /**
     * 查询数据分页
     *
     * @param pageable 分页参数
     * @return Object
     */
    @Override
    public Object queryAlls(RoleQueryCriteria criteria, Pageable pageable) {
        List<Role> roleList = baseMapper.selectList(QueryHelpPlus.getPredicate(Role.class, criteria));
        return roleList;
    }


    @Override
    public List<Role> queryAll(RoleQueryCriteria criteria) {
        List<Role> roleList = baseMapper.selectList(QueryHelpPlus.getPredicate(Role.class, criteria));
//        for (Role role : roleList) {
//            Set<Menu> menuList = menuMapper.findMenuByRoleId(role.getId());
//            //region 计算、修正角色的MenuJson数据
//            //子级没有全选的要把当前的菜单编号放到menuJson中去
//            List<Long> menuPidList = menuList.stream().filter(m-> !m.getPid().equals(0L)).map(Menu::getPid).distinct().collect(Collectors.toList());
//            List<Long> menuJsonList = new ArrayList<>();
//            String menuJson=role.getMenuJson();
//            if(StrUtil.isNotBlank(menuJson)) {
//                //menuJson转换成List
//                menuJsonList = Arrays.stream(menuJson.split(",")).map(Long::parseLong).collect(Collectors.toList());
//            }
//            menuPidList.removeAll(menuJsonList);    //去重重复的数据
//            for (Long pid : menuPidList) {
//                if(menuMapper.hasRoleMenuPidCountEqMenuPidCount(role.getId(),pid).equals(0)){
//                    menuJsonList.add(pid);
//                }
//            }
//            if(menuJsonList.size()>0){
//                role.setMenuJson(StringUtils.join(menuJsonList, ","));
//            }
//            //endregion 计算、修正角色的MenuJson数据
//            role.setMenus(menuList);
//        }
        return roleList;
    }


    @Override
    public void download(List<RoleDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (RoleDto role : all) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("名称", role.getName());
            map.put("备注", role.getRemark());
            map.put("数据权限", role.getDataScope());
            map.put("角色级别", role.getLevel());
            map.put("创建日期", role.getCreateTime());
            map.put("功能权限", role.getPermission());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    /**
     * 根据用户ID查询
     *
     * @param id 用户ID
     * @return /
     */
//    @Cacheable(key = "'findByUsers_Id:' + #p0")
    @Override
    public List<RoleSmallDto> findByUsersId(Long id) {
        List<Role> roles = roleMapper.selectListByUserId(id);
        return generator.convert(roles, RoleSmallDto.class);
    }

    /**
     * 根据角色查询角色级别
     *
     * @param roles /
     * @return /
     */
    @Override
    public Integer findByRoles(Set<Role> roles) {
        Set<RoleDto> roleDtos = new HashSet<>();
        for (Role role : roles) {
            roleDtos.add(findById(role.getId()));
        }
        return Collections.min(roleDtos.stream().map(RoleDto::getLevel).collect(Collectors.toList()));
    }

    /**
     * 根据ID查询
     *
     * @param id /
     * @return /
     */
    @Override
    public RoleDto findById(long id) {
        Role role = this.getById(id);
        role.setMenus(menuMapper.findMenuByRoleId(role.getId()));
        return generator.convert(role, RoleDto.class);
    }

    /**
     * 修改绑定的菜单
     *
     * @param resources /
     * @param roleDto   /
     */
    @Override
//    @CacheEvict(allEntries = true)
    public void updateMenu(Role resources, RoleDto roleDto) {
        if (resources.getMenus().size() > 0) {
            List<RolesMenus> rolesMenusList = resources.getMenus().stream().map(i -> {
                RolesMenus rolesMenus = new RolesMenus();
                rolesMenus.setRoleId(resources.getId());
                rolesMenus.setMenuId(i.getId());
                return rolesMenus;
            }).collect(Collectors.toList());
            rolesMenusService.remove(new LambdaQueryWrapper<RolesMenus>().eq(RolesMenus::getRoleId, resources.getId()));
            rolesMenusService.saveBatch(rolesMenusList);

            /** 新增menuJson字段方便前端使用 */
            this.lambdaUpdate().eq(Role::getId, resources.getId()).set(Role::getMenuJson, resources.getMenuJson()).update();
        }
    }


    @Override
//    @CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public RoleDto create(Role resources) {
        if (this.getOne(new LambdaQueryWrapper<Role>().eq(Role::getName, resources.getName())) != null) {
            throw new EntityExistException(Role.class, "username", resources.getName());
        }

        if (this.getOne(new LambdaQueryWrapper<Role>().eq(Role::getName, resources.getName())) != null) {
            throw new EntityExistException(Role.class, "username", resources.getName());
        }
        this.save(resources);
        return generator.convert(resources, RoleDto.class);
    }

    @Override
//    @CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(Role resources) {
        Role role = this.getById(resources.getId());

        Role role1 = this.getOne(new LambdaQueryWrapper<Role>().eq(Role::getName, resources.getName()));

        if (role1 != null && !role1.getId().equals(role.getId())) {
            throw new EntityExistException(Role.class, "username", resources.getName());
        }
        role1 = this.getOne(new LambdaQueryWrapper<Role>().eq(Role::getPermission, resources.getPermission()));
        if (role1 != null && !role1.getId().equals(role.getId())) {
            throw new EntityExistException(Role.class, "permission", resources.getPermission());
        }
        role.setName(resources.getName());
        role.setRemark(resources.getRemark());
        role.setDataScope(resources.getDataScope());
        role.setLevel(resources.getLevel());
        role.setPermission(resources.getPermission());
        this.saveOrUpdate(role);
    }

    /**
     * 获取用户权限信息
     *
     * @param user 用户信息
     * @return 权限信息
     */
    @Override
//    @Cacheable(key = "'loadPermissionByUser:' + #p0.username")
    public Collection<GrantedAuthority> mapToGrantedAuthorities(UserDto user) {
        //根据用户id获取权限等级
        Set<Role> roles = roleMapper.findByUsers_Id(user.getId());
        //获取权限等级对应的菜单权限
        for (Role role : roles) {
            Set<Menu> menuSet = menuMapper.findMenuByRoleId(role.getId());
            role.setMenus(menuSet);
        }

        Set<String> permissions = roles.stream().filter(role -> StringUtils.isNotBlank(role.getPermission())).map(Role::getPermission).collect(Collectors.toSet());

        permissions.addAll(
                roles.stream().flatMap(role -> role.getMenus().stream())//将权限对应的菜单内容单独提取出来形成一个流
                        .filter(menu -> StringUtils.isNotBlank(menu.getPermission()))//获取非空的菜单内容
                        .map(Menu::getPermission).collect(Collectors.toSet())//拼接为一个集合返回
        );

        if(TenantConstant.DEFAULT_TENANT_ID.equals(user.getTenantId())) {
            Set<Menu> adminMenuSet = menuMapper.getAdminMenu();
            permissions.addAll(adminMenuSet.stream().map(Menu::getPermission).collect(Collectors.toSet()));
        }

        if (permissions.size() == 0) {
            throw new BadRequestException("当前用户角色设置错误");
        }

        return permissions.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Set<Long> ids) {
        for (Long id : ids) {
            rolesMenusService.lambdaUpdate().eq(RolesMenus::getRoleId, id).remove();
        }
        this.removeByIds(ids);
    }

    @Override
    public Set<String> getComponentName(Long id) {
        List<Role> roles = baseMapper.selectListByUserId(id);
        Set<String> setAll = new HashSet<>();
        roles.forEach(role -> {
            setAll.addAll(menuMapper.findMenuByRoleId(role.getId()).stream().map(Menu::getComponentName).collect(Collectors.toSet()));
        });
        return setAll;
    }

    @Override
    public RoleMenuDTO getRolesMenuByRole(Long roleId) {

        RoleMenuDTO roleMenuDTO = new RoleMenuDTO();
        Role role = baseMapper.selectOne(Wrappers.<Role>lambdaQuery().eq(Role::getId, roleId));
        if (!Optional.ofNullable(role).isPresent()) {
            return roleMenuDTO;
        }
        //获取角色对应的权限数据
        Set<Menu> menuList = menuMapper.findMenuByRoleId(roleId);
        //region 计算、修正角色的MenuJson数据
        //子级没有全选的要把当前的菜单编号放到menuJson中去
        List<Long> menuPidList = menuList.stream().filter(m -> !m.getPid().equals(0L)).map(Menu::getPid).distinct().collect(Collectors.toList());
        List<Long> menuJsonList = new ArrayList<>();
        String menuJson = role.getMenuJson();
        if (StrUtil.isNotBlank(menuJson)) {
            //menuJson转换成List
            menuJsonList = Arrays.stream(menuJson.split(",")).map(Long::parseLong).collect(Collectors.toList());
        }
        menuPidList.removeAll(menuJsonList);    //去重重复的数据

        if (menuPidList.isEmpty()) {
            return roleMenuDTO;
        }

        //统计Pid下的个数
        Map<Long, Long> pidMenuNumMap = new HashMap<>();
        for (Long pid : menuPidList) {
            pidMenuNumMap.put(pid, menuList.stream().filter(m -> m.getPid().equals(pid)).count());
        }

        List<RoleMenuPidCountDTO> roleMenuPidCountDTOS = menuMapper.listPidMenuCount(menuPidList);
        for (Map.Entry<Long, Long> entry : pidMenuNumMap.entrySet()) {
            for (RoleMenuPidCountDTO roleMenuPidCountDTO : roleMenuPidCountDTOS) {
//                if(entry.getKey().equals(629L)){
//                    Integer a=1;
//                }
                if (entry.getKey().equals(roleMenuPidCountDTO.getPid())) {

                    if (!entry.getValue().equals(roleMenuPidCountDTO.getMenuCount())) {
                        menuJsonList.add(entry.getKey());
                    }
                    continue;
                }
            }
//            if(roleMenuPidCountDTOS.stream().filter(mc->mc.getPid().equals(entry.getKey()) && mc.getMenuCount().equals(entry.getValue())).count()==0) {
//                menuJsonList.add(entry.getKey());
//            }
        }
//        for (Long pid : menuPidList) {
//            if (menuMapper.hasRoleMenuPidCountEqMenuPidCount(roleId, pid).equals(0)) {
//                menuJsonList.add(pid);
//            }
//        }
        if (menuJsonList.size() > 0) {
            menuJsonList.addAll(getMenuIdByPid(menuJsonList));
            menuJsonList = menuJsonList.stream().distinct().collect(Collectors.toList());
            roleMenuDTO.setMenuJson(StringUtils.join(menuJsonList, ","));
        } else {
            roleMenuDTO.setMenuJson("");
        }

        //endregion 计算、修正角色的MenuJson数据
        roleMenuDTO.setMenus(menuList);

        return roleMenuDTO;
    }



	private List<Long> getMenuIdByPid(List<Long> menuJsonList) {
        if (CollUtil.isEmpty(menuJsonList)) {
            return Collections.emptyList();
        }
        //取所有菜单数据
        List<Menu> menus = menuMapper.selectList(Wrappers.<Menu>lambdaQuery().select(Menu::getId, Menu::getPid));
        List<Long> pids = new ArrayList<>();
        for (Long aLong : menuJsonList) {
            Long tmpPid = aLong;
            while (tmpPid != 0L) {
                Long finalTmpPid = tmpPid;
                Menu menu = menus.stream().filter(m -> m.getId().equals(finalTmpPid)).findFirst().orElse(null);
                if (ObjectUtil.isNotNull(menu)) {
                    tmpPid = menu.getPid();
                    if (!pids.contains(menu.getId()) && !aLong.equals(menu.getId())) {
                        pids.add(menu.getId());
                    }
                } else {
                    tmpPid = 0L;
                }
            }
        }
        return pids;
    }

	@Override
	public List<String> getUserNameByRoleId(Long roleId) {
		return this.baseMapper.getUserNameByRoleId(roleId);
	}
}
