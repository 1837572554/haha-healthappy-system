package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.modules.system.service.dto.ProcessQueryDto;
import com.healthappy.modules.system.service.dto.ProcessQueryQueryCriteria;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Jevany
 * @date 2022/1/26 10:42
 * @desc
 */
@Mapper
@Repository
public interface ProcessQueryMapper {

    /**
     * 根据条件查询体检进度数据
     * @author YJ
     * @date 2022/1/26 10:56
     * @param criteria
     * @return java.util.List〈com.healthappy.modules.system.service.dto.ProcessQueryDto〉
     */
    @SqlParser(filter = true)
    List<ProcessQueryDto> getProcessQueryList(@Param("criteria") ProcessQueryQueryCriteria criteria);

}
