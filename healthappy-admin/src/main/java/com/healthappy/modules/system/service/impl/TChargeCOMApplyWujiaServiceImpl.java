package com.healthappy.modules.system.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.healthappy.modules.system.domain.TChargeCOMApplyWujia;
import com.healthappy.modules.system.service.TChargeCOMApplyWujiaService;
import com.healthappy.modules.system.service.mapper.TChargeCOMApplyWujiaMapper;
import com.healthappy.utils.SecurityUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author: YuTang
 * @Date: Created in 2022/3/29 9:40
 * @Description:
 * @Version: 1.0
 */
@Service
@RequiredArgsConstructor
public class TChargeCOMApplyWujiaServiceImpl extends ServiceImpl<TChargeCOMApplyWujiaMapper, TChargeCOMApplyWujia>
        implements TChargeCOMApplyWujiaService {

    private final TChargeCOMApplyWujiaMapper tChargeCOMApplyWujiaMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveByApply(String payApplyId) {
        //先删除旧的
        this.remove(Wrappers.<TChargeCOMApplyWujia>lambdaQuery()
                .eq(TChargeCOMApplyWujia::getPayApplyId,payApplyId));
        //再新增
        String tenantId = SecurityUtils.getTenantId();
        tChargeCOMApplyWujiaMapper.insertByApply(tenantId, payApplyId);
    }
}
