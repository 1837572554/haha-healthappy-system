package com.healthappy.modules.system.service;

import com.healthappy.modules.system.domain.TPatient;
import com.healthappy.modules.system.service.dto.IsSubmitDto;
import com.healthappy.modules.system.service.dto.IsSubmitQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @Author: FGQ
 * @Desc: 指引单回交
 * @Date: Created in 15:37 2022/3/7
 */
public interface IsSubmitService {

    Map<String,Object> pageAll(IsSubmitQueryCriteria criteria, Pageable pageable);

    List<TPatient> getList(IsSubmitQueryCriteria criteria);

    IsSubmitDto detail(String id);

    void backSubmitRevocation(Set<String> itemIds,Boolean type,String id,Boolean status);
}
