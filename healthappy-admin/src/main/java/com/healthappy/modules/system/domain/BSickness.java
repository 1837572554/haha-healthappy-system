package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.healthappy.config.SeedIdGenerator;
import com.healthappy.config.databind.FieldBind;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 病种表
 * @author fang
 * @date 2021-06-21
 */
@Data
@TableName("B_Sickness")
@ApiModel("病种表")
@SeedIdGenerator
public class BSickness implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(type = IdType.INPUT)
    private String id;

    /**
     * 病种名称
     */
    @ApiModelProperty("病种名称")
    private String name;

    /**
     * 简拼
     */
    @ApiModelProperty("简拼")
    private String jp;

    /**
     * 是否删除
     */
    @ApiModelProperty("是否删除")
    @TableField(value = "del_flag",fill = FieldFill.INSERT)
    @TableLogic
    private String delFlag;

    /**
     * 显示顺序
     */
    @ApiModelProperty("显示顺序")
    private Integer dispOrder;

    /**
     * 是否是病种组合子项
     */
    @ApiModelProperty("是否是病种组合子项")
    private String isCombine;

    /**
     * 病种组合名称
     */
    @ApiModelProperty("病种组合名称")
    private String combineName;

    /**
     * 是否属于重大异常
     */
    @ApiModelProperty("是否属于重大异常")
    private String anomaly;

    /**
     * 病种组合名称
     */
    @ApiModelProperty("病种组合名称")
    private String zx;

    /**
     * 统计名称
     */
    @ApiModelProperty("统计名称")
    private String nameTj;

    /**
     * 病种等级
     */
    @ApiModelProperty("病种等级")
    @FieldBind(type = "sicknessGrade" , target = "gradeText")
    private Integer grade;

    @TableField(exist = false)
    private String gradeText;

    /**
     * 医疗机构id(u_hospital_info的id)
     */
    @ApiModelProperty("医疗机构id(u_hospital_info的id)")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;
}
