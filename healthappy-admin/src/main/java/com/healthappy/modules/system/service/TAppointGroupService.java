package com.healthappy.modules.system.service;


import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BDepartmentGroupHD;
import com.healthappy.modules.system.domain.TAppoint;
import com.healthappy.modules.system.domain.TAppointGroup;

import java.math.BigDecimal;
import java.util.List;

/**
 * @description 分组表 服务层
 * @author sjc
 * @date 2021-06-24
 */
public interface TAppointGroupService extends BaseService<TAppointGroup> {

    /**
     * 清除重复项目
     * @param appointIds
     */
    void repeatGroupId(List<String> appointIds);

    /**
     * 计算预约组合
     * @author YJ
     * @date 2022/3/10 18:21
     * @param id 预约id
     * @param groupId 组合项id
     * @param price 价格
     * @param payType 支付方式
     * @param discount 折扣
     * @param cost 实价
     * @return com.healthappy.modules.system.domain.TAppointGroup
     */
    TAppointGroup buildTAppointGroup(String id, String groupId, BigDecimal price, Integer payType, BigDecimal discount, BigDecimal cost);

    /**
     * @Author: FGQ
     * @Desc: 计算各项价格
     * @Date: 2022/3/8
     * @return  price,discount,cost
     */
    TAppointGroup calculateEachPrice(BigDecimal price,BigDecimal discount,BigDecimal cost);


    /**
     * 保存预约组合项
     * @author YJ
     * @date 2022/3/10 19:13
     * @param v 预约数据
     * @param bDepartmentGroupHDList 部门分组全部列表，没有的话传Null
     */
    void saveAppointGroup(TAppoint v, List<BDepartmentGroupHD> bDepartmentGroupHDList);
}

