package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 体检项目
 * @author fang
 * @date 2021-06-17
 */
@Mapper
@Repository
public interface BItemMapper extends CoreMapper<BItem> {
	/**
	 * 根据组合编号获取明细项
	 * @param groupId 组合编号
	 * @return 明细项列表
	 */
	List<BItem> listByGroupId(@Param("groupId") String groupId);
}
