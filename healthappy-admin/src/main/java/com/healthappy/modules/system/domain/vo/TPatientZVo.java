package com.healthappy.modules.system.domain.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @description 职业体检人员表
 * @author sjc
 * @date 2021-09-8
 */
@Data
public class TPatientZVo  {

    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty("主键id")
    private Long id;


    @NotNull(message = "体检号不能为空")
    @ApiModelProperty("体检号")
    private String paId;


    @ApiModelProperty("接害工龄")
    private String workYears;

    @ApiModelProperty("接害种类(B_Poison)")
    private String poisonType;

    @ApiModelProperty("是否职业禁忌证 ")
    private String pjTaboo;

    @ApiModelProperty("是否疑似职业病")
    private String pjSuspicion;

    @ApiModelProperty("是否其他复查")
    private String pjReview;

    @ApiModelProperty("是否调离")
    private String pjRemove;

    @ApiModelProperty("是否其他疾病或异常")
    private String pjUnusual;

    @ApiModelProperty("是否复查")
    private String pjRelate;

    @ApiModelProperty("职业结论")
    private String commentZ;

    @ApiModelProperty("职业建议")
    private String suggestZ;
}
