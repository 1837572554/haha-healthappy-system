package com.healthappy.modules.system.rest.medical;

import cn.hutool.core.bean.BeanUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.TRecord;
import com.healthappy.modules.system.domain.vo.TRecordVo;
import com.healthappy.modules.system.service.TRecordService;
import com.healthappy.modules.system.service.dto.TRecordQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 职业史表
 * @author sjc
 * @date 2021-12-15
 */
@Validated
@Slf4j
@Api(tags = "体检管理：职业史表")
@RestController
@AllArgsConstructor
@RequestMapping("/TRecord")
public class TRecordController {

    private final TRecordService tRecordService;

    @Log("查询职业史数据")
    @ApiOperation("查询职业史数据，权限码：ProjectCheck:list")
    @GetMapping
    @PreAuthorize("@el.check('ProjectCheck:list')")
    public R list(TRecordQueryCriteria criteria) {
        return R.ok(tRecordService.queryAll(criteria));
    }

    @Log("分页查询职业史数据")
    @ApiOperation("分页查询职业史数据，权限码：ProjectCheck:list")
    @GetMapping("/page")
    @PreAuthorize("@el.check('ProjectCheck:list')")
    public R listPage(TRecordQueryCriteria criteria, Pageable pageable) {
        return R.ok(tRecordService.queryAll(criteria, pageable));
    }

    @Log("新增|修改职业史")
    @ApiOperation("新增|修改职业史，权限码：TRecord:add")
    @PostMapping
    @PreAuthorize("@el.check('TRecord:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody TRecordVo resources) {
        return R.ok(tRecordService.saveOrUpdate( BeanUtil.copyProperties(resources, TRecord.class)));
    }


    @Log("根据id进行删除")
    @ApiOperation("根据id进行删除，权限码：TRecord:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('TRecord:delete')")
    @Klock
    public R remove(@RequestBody Set<Integer> ids) {
        return R.ok(tRecordService.removeByIds(ids));
    }
}
