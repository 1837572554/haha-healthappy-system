package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class BCommentWzVo {

    /**
     * id
     */
    private String id;

    /**
     * 结论
     */
    @NotBlank(message = "结论不能为空")
    @ApiModelProperty(value = "结论",required = true)
    private String comment;

    /**
     * 排序号
     */
    @ApiModelProperty("排序号")
    private String dispOrder;

    /**
     * 问诊模板类型  0既往病史 1现病史 2家族史 3手术史
     */
    @ApiModelProperty("问诊模板类型  0既往病史 1现病史 2家族史 3手术史")
    private String type;

    /** 编号 */
    @ApiModelProperty("编号")
    private Integer code;
}
