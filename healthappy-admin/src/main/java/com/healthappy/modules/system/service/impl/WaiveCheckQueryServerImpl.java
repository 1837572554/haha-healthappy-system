package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.*;
import com.healthappy.modules.system.service.*;
import com.healthappy.modules.system.service.dto.DataQueryCriteria;
import com.healthappy.modules.system.service.dto.DataQueryDto;
import com.healthappy.modules.system.service.dto.TPatientDto;
import com.healthappy.modules.system.service.mapper.TPatientMapper;
import com.healthappy.utils.FileUtil;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author sjc
 * @date 2022-1-20
 */
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class WaiveCheckQueryServerImpl extends BaseServiceImpl<TPatientMapper, TPatient> implements WaiveCheckQueryServer {

    private final IGenerator generator;
    private final TPatientService patientService;
    private final BCompanyService bCompanyService;
    private final BDepartmentService bDepartmentService;
    private final BDepartmentGroupHDService bDepartmentGroupHDService;
    private final BPeTypeHDService bPeTypeHDService;
    private final BPeTypeDTService bPeTypeDTService;
    private final TItemHDService tItemHDService;


    @Override
    public List<DataQueryDto> queryAll(DataQueryCriteria criteria) {
        return buildList(generator.convert(patientService.list(buildQueryCriteria(criteria)), DataQueryDto.class));
    }


    @Override
    public Map<String, Object> queryAll(DataQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<DataQueryDto> page = new PageInfo<DataQueryDto>(patientService.list(buildQueryCriteria(criteria)));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", buildList(generator.convert(page.getList(), DataQueryDto.class)));
        map.put("totalElements", page.getTotal());
        return map;
    }

    /**
     * 导出数据
     */
    @Override
    public void download(DataQueryCriteria criteria, HttpServletResponse response) throws IOException {
        if(ObjectUtil.isNotNull(criteria.getDateType())){
            if(ObjectUtil.isNull(criteria.getStartTime()) || ObjectUtil.isNull(criteria.getEndTime())){
                throw  new BadRequestException("查询时间条件有误");
            }
        }
        List<DataQueryDto> dtoList = queryAll(criteria);

        List<Map<String, Object>> list = new ArrayList<>();
        for (DataQueryDto dataQueryDto : dtoList) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put(" 流水号", dataQueryDto.getId());
            map.put(" 姓名", dataQueryDto.getName());
            map.put(" 性别", dataQueryDto.getSexText());
            map.put(" 年龄", dataQueryDto.getAge());
            map.put(" 联系电话", dataQueryDto.getPhone());
            map.put(" 弃检项目", dataQueryDto.getWaiveItemName());
            map.put(" 金额", dataQueryDto.getWaiveCost());
            map.put(" 体检分类", dataQueryDto.getPeTypeHdName());
            map.put(" 体检类别", dataQueryDto.getPeTypeDtName());
            map.put(" 单位", dataQueryDto.getCompanyName());
            map.put(" 部门", dataQueryDto.getDepName());
            map.put(" 分组", dataQueryDto.getDepGroupName());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    private List<DataQueryDto> buildList(List<DataQueryDto> list) {
        if(CollUtil.isEmpty(list)){
            return Collections.emptyList();
        }

        //单位
        List<String> comList = list.stream().map(DataQueryDto::getCompanyId).distinct().collect(Collectors.toList());
        List<BCompany> companyList = bCompanyService.lambdaQuery().in(CollUtil.isNotEmpty(comList),BCompany::getId, comList).list();
        //部门
        List<String> depList = list.stream().filter(c-> StrUtil.isNotEmpty(c.getDepartmentId())).map(DataQueryDto::getDepartmentId).distinct().collect(Collectors.toList());
        List<BDepartment> bDepartmentList = bDepartmentService.lambdaQuery().in(CollUtil.isNotEmpty(depList),BDepartment::getId,depList ).list();
        //分组
        List<String> depGroupList = list.stream().filter(c -> StrUtil.isNotEmpty(c.getDepartmentGroupId())).map(DataQueryDto::getDepartmentGroupId).distinct().collect(Collectors.toList());
        List<BDepartmentGroupHD> bDepartmentGroupHDList = bDepartmentGroupHDService.lambdaQuery().in(CollUtil.isNotEmpty(depGroupList),BDepartmentGroupHD::getId, depGroupList).list();

        //体检分类
        List<Long> typeHd = list.stream().filter(c -> c.getPeTypeHdId() != null).map(DataQueryDto::getPeTypeHdId).distinct().collect(Collectors.toList());
        List<BPeTypeHD> bPeTypeHDList = bPeTypeHDService.lambdaQuery().in(CollUtil.isNotEmpty(typeHd),BPeTypeHD::getId,typeHd).list();
        //体检类别
        List<Long> typeDt = list.stream().filter(c -> c.getPeTypeDtId() != null).map(DataQueryDto::getPeTypeDtId).distinct().collect(Collectors.toList());
        List<BPeTypeDT> bPeTypeDTList = bPeTypeDTService.lambdaQuery().in(CollUtil.isNotEmpty(typeDt),BPeTypeDT::getId, typeDt).list();

        List<TItemHD> tItemHDList = tItemHDService.lambdaQuery().in(TItemHD::getPaId, list.stream().map(DataQueryDto::getId).distinct().collect(Collectors.toList())).like(TItemHD::getResult,"放弃").list();


        list.stream().peek(e -> {
                e.setCompanyName(companyList.stream().filter(c->c.getId().equals(e.getCompanyId())).findFirst().orElse(new BCompany()).getName());
                e.setDepName(bDepartmentList.stream().filter(c->c.getId().equals(e.getDepartmentId())).findFirst().orElse(new BDepartment()).getName());
                e.setDepGroupName(bDepartmentGroupHDList.stream().filter(c->c.getId().equals(e.getDepartmentGroupId())).findFirst().orElse(new BDepartmentGroupHD()).getName());

                e.setPeTypeHdName(bPeTypeHDList.stream().filter(c->c.getId().equals(e.getPeTypeHdId())).findFirst().orElse(new BPeTypeHD()).getName());
                e.setPeTypeDtName(bPeTypeDTList.stream().filter(c->c.getId().equals(e.getPeTypeDtId())).findFirst().orElse(new BPeTypeDT()).getName());

                //弃检项目及价格
                List<String> collect = tItemHDList.stream().filter(c->c.getPaId().equals(e.getId())).map(TItemHD::getGroupName).collect(Collectors.toList());
                e.setWaiveItemName(StrUtil.join(",",collect.toArray()));
                e.setWaiveCost(tItemHDList.stream().filter(c->c.getPaId().equals(e.getId()) && c.getCost()!=null).map(TItemHD::getCost).reduce(BigDecimal.ZERO, BigDecimal::add));
        }).collect(Collectors.toList());

        return list;
    }


    private QueryWrapper buildQueryCriteria(DataQueryCriteria criteria) {
        QueryWrapper<TPatient> wrapper = QueryHelpPlus.getPredicate(TPatient.class,criteria);
        //查询的时间类型
        if(ObjectUtil.isNotNull(criteria.getDateType())){
            String startTime = criteria.getStartTime(),endTime = criteria.getEndTime();
            switch (criteria.getDateType()){
                case 1:
                    //登记
                    wrapper.between("pe_date", startTime,endTime);
                    break;
                case 2:
                    //签到
                    wrapper.between("sign_date",startTime,endTime);
                    break;
                case 3:
                    //总检
                    wrapper.between("conclusion_date",startTime,endTime);
                    break;
                default:
                    throw new BadRequestException("参数有误");
            }
        }
        String lastSql=" and id in (select pa_id from T_Item_HD where result like '%放弃%' and pa_id=T_Patient.id)";
        wrapper.last(lastSql);
        return wrapper;
    }
}
