package com.healthappy.modules.system.rest.manage;

import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.vo.BatchProjectAdjustVO;
import com.healthappy.modules.system.service.TPatientService;
import com.healthappy.modules.system.service.dto.BatchProjectAdjustDTO;
import com.healthappy.modules.system.service.dto.BatchProjectAdjustQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 批量项目调整-控制器
 * @author Jevany
 * @date 2022/5/13 15:31
 */
@Slf4j
@Api(tags = "体检管理：批量项目调整")
@RestController
@AllArgsConstructor
@RequestMapping("/batchProjectAdjust")
public class BatchProjectAdjustController {
	/** 体检人员服务 */
	private final TPatientService tPatientService;

	@Log("批量项目调整-列表查询")
	@ApiOperation("批量项目调整-列表查询，权限码：batchProjectAdjust:list")
	@GetMapping("/list")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "批量修改项目数据", response = BatchProjectAdjustVO.class, responseContainer = "Map")})
	@PreAuthorize("@el.check('batchProjectAdjust:list')")
	public R list(BatchProjectAdjustQueryCriteria criteria, Pageable pageable) {
		return R.ok(tPatientService.mapBatchProjectAdjust(criteria, pageable));
	}

	@Log("批量项目调整")
	@ApiOperation("批量项目调整，权限码：batchProjectAdjust:list")
	@PostMapping("/batchProjectAdjust")
	@PreAuthorize("@el.check('batchProjectAdjust:list')")
	public R batchProjectAdjust(@Validated @RequestBody BatchProjectAdjustDTO dto) {
		tPatientService.batchProjectAdjust(dto);
		return R.ok();
	}
}
