package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Jevany
 * @date 2022/1/21 17:10
 * @desc
 */
@Data
public class SicknessConclusionNameSexDto implements Serializable {
    /** 病种名称 */
    @ApiModelProperty("病种名称")
    private String sicknessName;

    /** 性别 */
    @ApiModelProperty("性别")
    private String sex;
}