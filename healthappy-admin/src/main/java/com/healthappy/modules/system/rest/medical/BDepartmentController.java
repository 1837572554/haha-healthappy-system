package com.healthappy.modules.system.rest.medical;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BDepartment;
import com.healthappy.modules.system.domain.BDepartmentGroupHD;
import com.healthappy.modules.system.domain.vo.BDepartmentVo;
import com.healthappy.modules.system.service.BDepartmentGroupHDService;
import com.healthappy.modules.system.service.BDepartmentService;
import com.healthappy.modules.system.service.dto.BDepartmentQueryCriteria;
import com.healthappy.utils.R;
import com.healthappy.utils.SecurityUtils;
import com.healthappy.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @author FGQ
 * @description 单位部门管理 控制器
 * @date 2021-06-24
 */
@Slf4j
@Api(tags = "体检管理：单位部门管理")
@RestController
@AllArgsConstructor
@RequestMapping("/BDepartment")
public class BDepartmentController {

    private final BDepartmentService bDepartmentService;

    private final BDepartmentGroupHDService bDepartmentGroupHDService;


    @Log("查询部门表")
    @ApiOperation("查询部门表，权限码：BDepartment:list")
    @GetMapping("/list")
    @PreAuthorize("@el.check('BDepartment:list')")
    public R list(BDepartmentQueryCriteria criteria) {
        return R.ok(bDepartmentService.queryAlls(criteria));
    }

    @Log("分页查询部门表")
    @ApiOperation("分页查询部门表，权限码：BDepartment:list")
    @GetMapping
    @PreAuthorize("@el.check('BDepartment:list')")
    public R list(BDepartmentQueryCriteria criteria, @PageableDefault(sort = {"disp_order"}) Pageable pageable) {
        return R.ok(bDepartmentService.queryAll(criteria, pageable));
    }

    @Log("新增|修改部门表")
    @ApiOperation("新增|修改部门表，权限码：BDepartment:add")
    @PostMapping
    @PreAuthorize("@el.check('BDepartment:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BDepartmentVo resources) {
        BDepartment bDepartment = new BDepartment();
        BeanUtil.copyProperties(resources, bDepartment);
        if (StringUtils.isBlank(resources.getId())) {
            //判断单位下是否有重名
            if (bDepartmentService.count(new LambdaQueryWrapper<BDepartment>().eq(BDepartment::getComId, resources.getComId())
                    .eq(BDepartment::getName, resources.getName())
                    .eq(BDepartment::getTenantId, SecurityUtils.getTenantId())
                    .ne(BDepartment::getIsEnable, "0")) > 0) {
                throw new BadRequestException("新增部门【" + resources.getName() + "】名称已存在");
            }

            bDepartment.setId(bDepartmentService.generateDepartmentKey(resources.getComId(), SecurityUtils.getTenantId()));
        } else {
            if (bDepartmentService.count(new LambdaQueryWrapper<BDepartment>().ne(BDepartment::getId, resources.getId())
                    .eq(BDepartment::getComId, resources.getComId())
                    .eq(BDepartment::getName, resources.getName())
                    .eq(BDepartment::getTenantId, SecurityUtils.getTenantId())
                    .ne(BDepartment::getIsEnable, "0")) > 0) {
                throw new BadRequestException("编辑部门【" + resources.getName() + "】名称已存在");
            }
        }

        bDepartmentService.saveOrUpdate(bDepartment);
        return R.ok();
    }

    @Log("删除部门表")
    @ApiOperation("删除部门表，权限码：BDepartment:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('BDepartment:delete')")
    @Klock
    public R remove(@RequestBody Set<String> ids) {
        checkIfItExists(ids);
        if (bDepartmentService.removeByIds(ids)) {
            return R.ok();
        } else {
            return R.error(500, "方法异常");
        }
    }

    private void checkIfItExists(Set<String> ids) {
        if (bDepartmentGroupHDService.lambdaQuery().in(BDepartmentGroupHD::getDepartmentId, ids).count() > 0) {
            throw new BadRequestException("分组设置中存在删除的部门,请先删除分组中的数据");
        }
    }
}
