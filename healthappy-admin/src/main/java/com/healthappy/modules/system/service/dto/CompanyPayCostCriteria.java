package com.healthappy.modules.system.service.dto;

import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Data
@ApiModel("单位缴费查询")
public class CompanyPayCostCriteria {

    /** 时间段（数组） */
    @NotNull(message = "时间段不能为空")
    @ApiModelProperty(value = "时间段（数组） ", required = true, position = 1)
    @Query(type = Query.Type.BETWEEN)
    private List<String> applyTime;

    @Query
    @ApiModelProperty("单位ID")
    private String companyId;

    @ApiModelProperty("结账标志，1：已结账 0：未结账  不传：全部")
    private Integer payFlag;

}
