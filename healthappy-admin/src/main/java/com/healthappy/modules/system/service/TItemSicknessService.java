package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TItemSickness;
import com.healthappy.modules.system.domain.vo.ProjectCheckVo;
import com.healthappy.modules.system.service.dto.ItemSicknessMatchDto;

import java.util.List;

/**
 * @description
 * @author FGQ
 * @date 2021-11-17
 */
public interface TItemSicknessService extends BaseService<TItemSickness> {

    /**
     * 组合项保存时，保存生成的病种规则
     */
    void installVo(ProjectCheckVo projectCheckVo);

    /**
     * 获取结果（已经保存的）对应的病种数据
     * @author YJ
     * @date 2022/3/10 11:38
     * @param paId 流水号
     * @param itemId 明细项
     * @return java.util.List〈com.healthappy.modules.system.service.dto.ItemSicknessMatchDto〉
     */
    List<ItemSicknessMatchDto> listResultSickness(String paId, String itemId);
}
