package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.BooleanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.config.databind.DataBind;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.enums.PeDateTypeEnum;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.*;
import com.healthappy.modules.system.domain.vo.AlwaysCheckVo;
import com.healthappy.modules.system.domain.vo.PersonalReportTaskSubmitVo;
import com.healthappy.modules.system.domain.vo.TPatientImageVo;
import com.healthappy.modules.system.service.*;
import com.healthappy.modules.system.service.dto.*;
import com.healthappy.modules.system.service.mapper.TPatientMapper;
import com.healthappy.modules.system.service.wrapper.ProjectCheckWrapper;
import com.healthappy.utils.SecurityUtils;
import com.healthappy.utils.SpringUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PersonalReportServiceImpl implements PersonalReportService {

    private final IGenerator generator;
    private final UserService userService;
    private final TPatientService patientService;
    private final TPatientZService patientZService;
    private final TPhotoService photoService;
    private final TItemHDService itemHDService;
    private final TItemDTService itemDtService;
    private final BCompanyService companyService;
    private final TPatientMapper patientMapper;
    private final TPatientImgService patientImgService;
    private final BPeTypeGroupService peTypeGroupService;
    private final TPatientCService patientCService;
    private final BDepartmentGroupHDService departmentGroupHDService;

    @Override
    public Map<String, Object> list(PersonalReportQueryCriteria criteria) {
        List list = buildList(patientService.list(buildQueryCriteria(criteria)));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", list);
        map.put("totalElements", list.size());
        return map;
    }


    private List<TPatientDto> buildList(List<TPatient> list) {
        if (CollUtil.isEmpty(list)) {
            return Collections.emptyList();
        }
        List<TPatientDto> dtos = generator.convert(list, TPatientDto.class);

        //获取t_patient_z下的结论和建议
        dtos.stream().peek(e -> {
            TPatientZ patientZ = patientZService.lambdaQuery().eq(TPatientZ::getPaId, e.getId()).one();
            if (null != patientZ) {
                e.setCommentZ(patientZ.getCommentZ());
                e.setSuggestZ(patientZ.getSuggestZ());
            }
        }).collect(Collectors.toList());


        List<TPhoto> photoList = photoService.lambdaQuery().in(TPhoto::getId, list.stream().map(TPatient::getId).collect(Collectors.toList())).list();
        if (CollUtil.isEmpty(photoList)) {
            return dtos;
        }
        return dtos.stream().peek(e -> {
            TPhoto photo = photoList.stream().filter(t -> t.getId().equals(e.getId())).findAny().orElse(null);
            if (null != photo) {
                e.setIdImage(photo.getIdImage());
                e.setPortrait(photo.getPortrait());
            }
        }).collect(Collectors.toList());
    }

    @Override
    public Object detail(String id) {
        return ProjectCheckWrapper.build().entityVO(patientService.getById(id));
    }

    @Override
    public Map<String, Object> task(PersonalReportTaskQueryCriteria criteria) {
        QueryWrapper<TPatient> wrapper = QueryHelpPlus.getPredicate(TPatient.class, criteria);

        if(ObjectUtil.isNotNull(criteria.getDateType()) && StrUtil.isNotBlank(criteria.getStartTime()) && StrUtil.isNotBlank(criteria.getEndTime())) {
            PeDateTypeEnum peDateTypeEnum=PeDateTypeEnum.toType(criteria.getDateType());
            switch (peDateTypeEnum){
                case REGISTRATION:
                    wrapper.between("pe_date",criteria.getStartTime(),criteria.getEndTime());
                    break;
                case SIGN:
                    wrapper.between("sign_date",criteria.getStartTime(),criteria.getEndTime());
                    break;
                default:
                    wrapper.between("pe_date",criteria.getStartTime(),criteria.getEndTime());
                    break;
            }
        }

        if (criteria.getIsLackItem() != null) {
            String tenantId = SecurityUtils.getTenantId();
            String LackSql = "  (select pa_id from T_Item_HD h inner join B_Dept d on d.id=h.dept_id  and d.tenant_id='" + tenantId + "' " +
                    "where h.pa_id =T_Patient.id and d.name not like '计费%' and  (h.result_date is null or h.result_date = '') and h.tenant_id='" + tenantId + "' " +
                    ")";
            //有缺项
            if (criteria.getIsLackItem()) {
                wrapper.exists(LackSql);
            } else {//无缺项
                wrapper.notExists(LackSql);
            }
        }
        //已分配才会使用总检医生、审核医生、是否总检、是否审核
        if(BooleanUtil.isTrue(criteria.getIsAssign())){
            if(BooleanUtil.isTrue(criteria.getIsConclusion())) {
                wrapper.isNotNull("conclusion_date");
            }

            if(BooleanUtil.isTrue(criteria.getIsVerify())) {
                wrapper.isNotNull("verify_date");
            }

            if(StrUtil.isNotBlank(criteria.getConclusionDoc())){
                wrapper.eq("conclusion_doc",criteria.getConclusionDoc());
            }
            if(StrUtil.isNotBlank(criteria.getVerifyDoc())){
                wrapper.eq("verify_doc",criteria.getVerifyDoc());
            }
			wrapper.and(wr->wr.isNotNull("conclusion_doc").or().isNotNull("verify_doc"));
        } else {
            wrapper.isNull("conclusion_doc").isNull("verify_doc");
        }


        if (StrUtil.isNotBlank(criteria.getCompanyId())) {
            if (criteria.getIsAssignCompany()) {
                wrapper.eq("company_id", criteria.getCompanyId());
            } else {
                wrapper.ne("company_id", criteria.getCompanyId());
            }
        }
        List list = buildTaskList(patientService.list(wrapper));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", list);
        map.put("totalElements", list.size());
        return map;
    }

    @Override
    public List<TaskDockerNumDto> conclusionDoc() {
        List<User> userList = userService.getDockerList();
        if (CollUtil.isEmpty(userList)) {
            return Collections.emptyList();
        }
        List<TPatient> patientList = patientService.query().isNull("conclusion_date")
                .isNotNull("conclusion_doc").groupBy("conclusion_doc")
                .select("conclusion_doc", "count(1) as doc_num").list();
        return userList.stream().map(user -> {
            TaskDockerNumDto dockerNumDto = new TaskDockerNumDto();
            dockerNumDto.setDocker(user.getNickName());
            dockerNumDto.setCode(user.getId());
            if (CollUtil.isEmpty(patientList)) {
                dockerNumDto.setNum(0L);
            } else {
                TPatient patient = patientList.stream().filter(pa -> pa.getConclusionDoc().equals(user.getNickName()))
                        .findAny().orElse(null);
                dockerNumDto.setNum(ObjectUtil.isNotNull(patient) ? patient.getDocNum() : 0L);
            }
            return dockerNumDto;
        }).collect(Collectors.toList());
    }

    @Override
    public List<TaskDockerNumDto> verifyDoc() {
        List<User> userList = userService.getDockerList();
        if (CollUtil.isEmpty(userList)) {
            return Collections.emptyList();
        }
        List<TPatient> patientList = patientService.query().isNull("verify_date")
                .isNotNull("verify_doc").groupBy("verify_doc")
                .select("verify_doc", "count(1) as doc_num").list();
        return userList.stream().map(user -> {
            TaskDockerNumDto dockerNumDto = new TaskDockerNumDto();
            dockerNumDto.setDocker(user.getNickName());
            dockerNumDto.setCode(user.getId());
            if (CollUtil.isEmpty(patientList)) {
                dockerNumDto.setNum(0L);
            } else {
                TPatient patient = patientList.stream().filter(pa -> pa.getVerifyDoc().equals(user.getNickName()))
                        .findAny().orElse(null);
                dockerNumDto.setNum(ObjectUtil.isNotNull(patient) ? patient.getDocNum() : 0L);
            }
            return dockerNumDto;
        }).collect(Collectors.toList());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void submit(List<PersonalReportTaskSubmitVo> taskSubmitVoList) {
        for (PersonalReportTaskSubmitVo vo : taskSubmitVoList) {
            PersonalReportTaskSubmitVo.DockerNum dockerNum = vo.getDockerNum();
            patientService.lambdaUpdate().set(TPatient::getConclusionDoc, dockerNum.getConclusionDoc())
                    .set(TPatient::getVerifyDoc, dockerNum.getVerifyDoc())
                    .set(TPatient::getConclusionDocCode, dockerNum.getConclusionCode())
                    .set(TPatient::getVerifyCode, SecurityUtils.getUserId())
                    .set(TPatient::getConclusionDocCode, dockerNum.getVerifyCode())
                    .in(TPatient::getId, vo.getIds()).update();
        }
    }

    @Override
    public PersonalReportSummaryDto summary(String id, String resultMark, Integer groupId, Integer type, Integer btn) {
        PersonalReportSummaryDto reportSummaryDto;
        if (resultMark != null) {
            reportSummaryDto = buildPatient(id, resultMark, type, btn);
        } else {
            TPatientZ patientZ = patientZService.lambdaQuery().eq(TPatientZ::getPaId, id).one();
            reportSummaryDto = buildPatient(id, null, type, btn);
            if (patientZ != null) {
                reportSummaryDto.setCommentZ(patientZ.getCommentZ());
                reportSummaryDto.setSuggestZ(patientZ.getSuggestZ());
            }
        }
        BPeTypeGroup peTypeGroup = peTypeGroupService.getById(groupId);
        switch (peTypeGroup.getName()) {
            case "从业":
                buildPatientC(id, reportSummaryDto);
                break;
            case "职业":
                buildPatientZ(id, reportSummaryDto);
                break;
            case "职普合一":
                buildPatientZC(id, reportSummaryDto,resultMark);
                break;
            default:
                break;
        }
        return reportSummaryDto;
    }


    private PersonalReportSummaryDto buildPatient(String id, String resultMark, Integer type, Integer btn) {
        PersonalReportSummaryDto reportSummaryDto = new PersonalReportSummaryDto();
        TPatient patient = patientService.getById(id);
        //if((btn  == null || btn != 1 ) && (StrUtil.isNotBlank(patient.getComment()) || StrUtil.isNotBlank(patient.getSuggest())))
        //已总检的话
        if ((btn == null || btn != 1) && patient.getConclusionDate() != null) {

            reportSummaryDto.setComment(patient.getComment());
            reportSummaryDto.setSuggest(patient.getSuggest());
            reportSummaryDto.setCommentZy(patient.getCommentZy());
            reportSummaryDto.setSuggestZy(patient.getSuggestZy());
            reportSummaryDto.setCommentPt(patient.getCommentPt());
            reportSummaryDto.setSuggestPt(patient.getSuggestPt());
            return reportSummaryDto;
        }
        List<PersonalReportSummaryDto> results = patientMapper.summary(id, resultMark, type);
        if (CollUtil.isEmpty(results)) {
            return reportSummaryDto;
        }

        String separator = System.getProperty("line.separator");
        reportSummaryDto.setComment(results.stream().filter(e -> StrUtil.isNotBlank(e.getComment())).map((x) -> "[" + x.getGroupName() + "]" + separator + x.getComment()).collect(Collectors.joining(separator)));
        reportSummaryDto.setSuggest(results.stream().filter(e -> StrUtil.isNotBlank(e.getSuggest())).map(PersonalReportSummaryDto::getSuggest).collect(Collectors.joining(separator)));

        //职普合一的职业
        reportSummaryDto.setCommentZy(results.stream().filter(e -> StrUtil.isNotBlank(e.getComment()) && "1".equals(e.getTypeZ())).map((x) -> "[" + x.getGroupName() + "]" + separator + x.getComment()).collect(Collectors.joining(separator)));
        reportSummaryDto.setSuggestZy(results.stream().filter(e -> (StrUtil.isNotBlank(e.getSuggest()) && "1".equals(e.getTypeZ()))).map(PersonalReportSummaryDto::getSuggest).collect(Collectors.joining(separator)));

		//职普合一的普通
        reportSummaryDto.setCommentPt(results.stream().filter(e -> StrUtil.isNotBlank(e.getComment()) && "1".equals(e.getTypeJ())).map((x) -> "[" + x.getGroupName() + "]" + separator + x.getComment()).collect(Collectors.joining(separator)));
        reportSummaryDto.setSuggestPt(results.stream().filter(e -> StrUtil.isNotBlank(e.getSuggest()) && "1".equals(e.getTypeJ())).map(PersonalReportSummaryDto::getSuggest).collect(Collectors.joining(separator)));


        return reportSummaryDto;
    }

    private void buildPatientZC(String id, PersonalReportSummaryDto reportSummaryDto,String resultMark) {
		//wkf 2022-03-3 说明：在resultMark是异常的情况下赋值会覆盖了原来正确的内容。
		if (resultMark == null) {
			TPatient patient = patientService.getById(id);
			reportSummaryDto.setCommentPt(patient.getCommentPt());
			reportSummaryDto.setSuggestPt(patient.getSuggestPt());
		}
        TPatientZ patientZ = patientZService.lambdaQuery().eq(TPatientZ::getPaId, id).one();
        if (null != patientZ) {
            BeanUtil.copyProperties(patientZ, reportSummaryDto);
        }
    }

    private void buildPatientZ(String id, PersonalReportSummaryDto reportSummaryDto) {
        TPatientZ patientZ = patientZService.lambdaQuery().eq(TPatientZ::getPaId, id).one();
        if (null != patientZ) {
            BeanUtil.copyProperties(patientZ, reportSummaryDto);
        }
    }

    private void buildPatientC(String id, PersonalReportSummaryDto reportSummaryDto) {
        TPatientC patientC = patientCService.lambdaQuery().eq(TPatientC::getPaId, id).one();
        if (null != patientC) {
            BeanUtil.copyProperties(patientC, reportSummaryDto);
            reportSummaryDto.setCommentZ(patientC.getCommentC());
            reportSummaryDto.setSuggestZ(patientC.getSuggestC());
        }
    }

    /**
     * @param id
     * @param type 1:职业,2:普通
     * @return
     */
    @Override
    public List<ProjectDetailsResultsDto> projectDetailsResults(String id, Integer type) {
        TItemHDQueryCriteria criteria = new TItemHDQueryCriteria();
        criteria.setPaId(id);
        List<TItemHD> tItemHDS = itemHDService.queryAll(criteria);
        if (type != null) {
            tItemHDS = tItemHDS.stream().filter(c -> {
                if (type == 1) {
                    return "1".equals(c.getTypeZ());
                } else if (type == 2) {
                    return "1".equals(c.getTypeJ());
                }
                return true;
            }).collect(Collectors.toList());
        }
        List<ProjectDetailsResultsDto> detailsResultsDtoList = generator.convert(tItemHDS, ProjectDetailsResultsDto.class);
//        List<ProjectDetailsResultsDto> detailsResultsDtoList = generator.convert(itemHDService.lambdaQuery()
//                .eq(type != null && type == 1,TItemHD::getTypeZ,1)
//                .eq(type != null && type == 2,TItemHD::getTypeJ,1)
//                .eq(TItemHD::getPaId, id)
//                .list(), ProjectDetailsResultsDto.class);
        if (CollUtil.isEmpty(detailsResultsDtoList)) {
            return Collections.emptyList();
        }
        return detailsResultsDtoList.stream().peek(detail -> {
            detail.setItemDetailList(generator.convert(itemDtService.lambdaQuery().eq(TItemDT::getGroupId, detail.getGroupId()).eq(TItemDT::getPaId, id).list(), ProjectDetailsResultsDto.ItemDt.class));
            TPatientImgDto tPatientImgDto = new TPatientImgDto();
            tPatientImgDto.setPaId(id);
            tPatientImgDto.setGroupId(detail.getGroupId());
            List<TPatientImageVo> itemImg = patientImgService.getItemImg(tPatientImgDto);
            detail.setTImageVoList(CollUtil.isNotEmpty(itemImg) ? itemImg.stream().flatMap(item -> item.getImages().stream()).collect(Collectors.toList()) : Collections.emptyList());
        }).collect(Collectors.toList());
    }

    @Override
    //@DS("click")
    public List<HistoricalComparisonDto> historicalComparison(String id) {
        List<HistoricalComparisonDto> comparisonDtoList = patientMapper.getDataPrimaryKeyByIdNo(id);
        if (CollUtil.isEmpty(comparisonDtoList)) {
            return Collections.emptyList();
        }
        DataBind dataBind = SpringUtil.getBean(DataBind.class);
        return comparisonDtoList.stream().peek(dto -> {
            dto.setSexText(dataBind.ENUM_MAP.get("sex").get(dto.getSex()));
            dto.setItemHdList(patientMapper.buildItemHdAndDtList(dto.getId()));
        }).collect(Collectors.toList());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void alwaysCheck(AlwaysCheckVo alwaysCheckVo) {
        //如果是撤销
        if (alwaysCheckVo.getType() == 2) {
            alwaysCheckVo.setSuggest(null);
            alwaysCheckVo.setComment(null);

            alwaysCheckVo.setSuggestZ(null);
            alwaysCheckVo.setCommentZ(null);
            alwaysCheckVo.setCommentZy(null);
            alwaysCheckVo.setCommentPt(null);
            alwaysCheckVo.setSuggestZy(null);
            alwaysCheckVo.setSuggestPt(null);
        }

        BPeTypeGroup peTypeGroup = peTypeGroupService.getById(alwaysCheckVo.getGroupId());
        Boolean isVerify = alwaysCheckVo.getType() == 1 && alwaysCheckVo.getOneClickVerify() == 1;
        String nickName = SecurityUtils.getNickName();
        Long userId = SecurityUtils.getUserId();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        patientService.lambdaUpdate()
                .set(TPatient::getComment, alwaysCheckVo.getComment())
                .set(TPatient::getSuggest, alwaysCheckVo.getSuggest())
                //撤销的时候不修改总检医生
                .set(alwaysCheckVo.getType() == 1, TPatient::getConclusionDoc, nickName)
                .set(TPatient::getConclusionDocCode, alwaysCheckVo.getType() == 1 ? userId : null)
                .set(TPatient::getIsStatus, alwaysCheckVo.getType() == 1 ? 2 : 4)//撤销总检时状态变为4---撤销总检
                .set(TPatient::getConclusionDate, alwaysCheckVo.getType() == 1 ? timestamp : null)
                //一键审核
                .set(isVerify, TPatient::getVerifyDoc, nickName)
                .set(isVerify, TPatient::getVerifyDate, timestamp)
                .set(isVerify, TPatient::getVerifyCode, userId)

                .eq(TPatient::getId, alwaysCheckVo.getId())
                .update();
        switch (peTypeGroup.getName()) {
            case "从业":
                TPatientC patientcOne = patientCService.lambdaQuery().eq(TPatientC::getPaId, alwaysCheckVo.getId()).one();
                if (null == patientcOne) {
                    patientcOne = new TPatientC();
                    patientcOne.setPaId(alwaysCheckVo.getId());
                }
                patientcOne.setPjQualified(alwaysCheckVo.getPjQualified().toString());
                patientcOne.setCommentC(alwaysCheckVo.getCommentZ());
                patientcOne.setPjUnqualified(alwaysCheckVo.getPjUnqualified().toString());
                patientcOne.setSuggestC(alwaysCheckVo.getSuggestZ());
                patientCService.saveOrUpdate(patientcOne);
                break;
            case "职业":
                buildUpd(alwaysCheckVo);
                break;

            case "职普合一":
                buildUpd(alwaysCheckVo);
                patientService.lambdaUpdate().eq(TPatient::getId, alwaysCheckVo.getId())
                        .set(TPatient::getCommentPt, alwaysCheckVo.getCommentPt())
                        .set(TPatient::getSuggestPt, alwaysCheckVo.getSuggestPt())
                        .set(TPatient::getCommentZy, alwaysCheckVo.getCommentZy())
                        .set(TPatient::getSuggestZy, alwaysCheckVo.getSuggestZy())
                        .update();
                break;
            default:
                break;
        }
    }

    private void buildUpd(AlwaysCheckVo alwaysCheckVo) {
        TPatientZ patientZ = patientZService.getByPaId(alwaysCheckVo.getId());
        if (null == patientZ) {
            patientZ = new TPatientZ();
            patientZ.setPaId(alwaysCheckVo.getId());
        }
        patientZ.setSuggestZ(alwaysCheckVo.getSuggestZ());
        patientZ.setCommentZ(alwaysCheckVo.getCommentZ());
        patientZ.setPjRelate(alwaysCheckVo.getPjRelate().toString());
        patientZ.setPjReview(alwaysCheckVo.getPjReview().toString());
        patientZ.setPjUnusual(alwaysCheckVo.getPjUnusual().toString());
        patientZ.setPjRemove(alwaysCheckVo.getPjRemove().toString());
        patientZ.setPjSuspicion(alwaysCheckVo.getPjSuspicion().toString());
        patientZ.setPjTaboo(alwaysCheckVo.getPjTaboo().toString());
        patientZService.saveOrUpdate(patientZ);
    }

    @Override
    public void check(String id, String type) {
        patientService.lambdaUpdate().eq(TPatient::getId, id)
                .set(TPatient::getIsStatus, "1".equals(type) ? 3 : 2)
                .set(TPatient::getVerifyDoc, "1".equals(type) ? SecurityUtils.getNickName() : null)
                .set(TPatient::getVerifyCode, "1".equals(type) ? SecurityUtils.getUserId() : null)
                .set(TPatient::getVerifyDate, "1".equals(type) ? new DateTime().toTimestamp() : null).update();
    }

    @Override
    public void returnAlwaysCheck(String id) {
        patientService.lambdaUpdate()
                .set(TPatient::getConclusionDate, null)
                .set(TPatient::getConclusionDocCode, null)
                .set(TPatient::getVerifyDoc, null)
                .set(TPatient::getVerifyDate, null)
                .set(TPatient::getVerifyCode, null)
                .set(TPatient::getIsStatus, 4)
                .eq(TPatient::getId, id)
                .update();
    }

    private List<PersonalReportTaskDto> buildTaskList(List<TPatient> list) {
        return list.stream().map(vo -> {
            PersonalReportTaskDto taskDto = BeanUtil.copyProperties(vo, PersonalReportTaskDto.class);
            taskDto.setCompanyName(StrUtil.isNotBlank(vo.getCompanyId()) ? Optional.ofNullable(companyService.lambdaQuery().eq(BCompany::getId, vo.getCompanyId()).select(BCompany::getName).one()).map(BCompany::getName).orElse("") : "");
            taskDto.setDepartmentGroupName(StrUtil.isNotBlank(vo.getDepartmentGroupId()) ? Optional.ofNullable(departmentGroupHDService.lambdaQuery().eq(BDepartmentGroupHD::getId, vo.getDepartmentGroupId()).select(BDepartmentGroupHD::getName).one()).map(BDepartmentGroupHD::getName).orElse("") : "");
            return taskDto;
        }).collect(Collectors.toList());
    }

    private QueryWrapper buildQueryCriteria(PersonalReportQueryCriteria criteria) {
        QueryWrapper<TPatient> wrapper = new QueryWrapper<>();

        wrapper = QueryHelpPlus.getPredicate(TPatient.class, criteria);

        String lastSql = "";
        if (criteria.getIsLackItem() != null) {
            String tenantId = SecurityUtils.getTenantId();

            String LackSql = "  (select pa_id from T_Item_HD h inner join B_Dept d on d.id=h.dept_id  and d.tenant_id='" + tenantId + "' " +
                    "where h.pa_id =T_Patient.id and d.name not like '计费%' and  (h.result_date is null or h.result_date = '') and h.tenant_id='" + tenantId + "' " +
                    ")";
            //有缺项
            if (criteria.getIsLackItem() == 1) {
                lastSql += " and EXISTS " + LackSql;
            } else {//无缺项
                lastSql += " and not EXISTS " + LackSql;
            }

        }


        //总检
        if (criteria.getType() == 1) {
            //String lastSql = "";
            String conclusionDoc = StrUtil.isBlank(criteria.getConclusionDoc()) ? "" : " and conclusion_doc ='" + criteria.getConclusionDoc() + "'";
            switch (criteria.getIsStatus()) {
                case "1":
                    lastSql += " and conclusion_date is null" + (StrUtil.isBlank(criteria.getConclusionDoc()) ? "" : " and (conclusion_doc is null or conclusion_doc = '" + criteria.getConclusionDoc() + "')");
                    lastSql += " and verify_date is null ";
                    break;
                case "2":
                    lastSql += " and conclusion_date is not null and conclusion_doc is not null " + (StrUtil.isBlank(criteria.getConclusionDoc()) ? "" : " and conclusion_doc ='" + criteria.getConclusionDoc() + "'");
                    break;
                case "4":
                    lastSql += " and is_status = 4 " + conclusionDoc;
                    lastSql += " and verify_date is null ";
                    break;
                default:
                    lastSql += StrUtil.isBlank(criteria.getConclusionDoc()) ? "" : " and (conclusion_doc is null or conclusion_doc = '" + criteria.getConclusionDoc() + "')";
                    lastSql += " and verify_date is null ";
                    break;
            }
            wrapper.last(lastSql);
//            wrapper.isNull("verify_date");
        } else {
            //String lastSql = "";
            String conclusionDoc = StrUtil.isBlank(criteria.getConclusionDoc()) ? "" : " and conclusion_doc ='" + criteria.getConclusionDoc() + "'";
            switch (criteria.getAcType()) {
                case 1:
                    lastSql += "and verify_date is null" + (StrUtil.isBlank(criteria.getVerifyDoc()) ? "" : " and (verify_doc is null or verify_doc = '" + criteria.getVerifyDoc() + "')") + conclusionDoc;
                    break;
                case 2:
                    lastSql += " and verify_date is not null and verify_doc is not null " + (StrUtil.isBlank(criteria.getVerifyDoc()) ? "" : " and verify_doc ='" + criteria.getVerifyDoc() + "'") + conclusionDoc;
                    break;
                default:
                    lastSql += (StrUtil.isBlank(criteria.getVerifyDoc()) ? "" : " and (verify_doc is null or verify_doc = '" + criteria.getVerifyDoc() + "')") + conclusionDoc;
                    break;
            }
            wrapper.last(lastSql);
            //总检时间不能为空
            wrapper.isNotNull("conclusion_date");
        }


        if (ObjectUtil.isNotNull(criteria.getDateType())) {
            String startTime = criteria.getStartTime(), endTime = criteria.getEndTime();
            switch (criteria.getDateType()) {
                case 1:
                    //登记
                    wrapper.between("pe_date", startTime, endTime);
                    break;
                case 2:
                    //签到
                    wrapper.between("sign_date", startTime, endTime);
                    break;
                case 3:
                    //体检
                    wrapper.between("conclusion_date", startTime, endTime);
                    break;
                case 4:
                    //总审
                    wrapper.between("verify_date", startTime, endTime);
                    break;
                default:
                    throw new BadRequestException("参数有误");
            }
        }
        return wrapper;
    }
}
