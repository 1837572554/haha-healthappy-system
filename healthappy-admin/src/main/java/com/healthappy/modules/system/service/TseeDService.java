package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TseeD;

/**
 * @description 种子 服务层
 * @author FGQ
 * @date 2021-06-17
 */
public interface TseeDService extends BaseService<TseeD> {


}
