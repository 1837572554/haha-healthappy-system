package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import com.healthappy.enums.PatientTimeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Author: FGQ
 * @Desc: 指引单 查询条件
 * @Date: Created in 16:17 2022/3/7
 */
@Data
@ApiModel("指引单")
public class IsSubmitQueryCriteria {

//    @NotNull(message = "时间类型不能为空")
    @ApiModelProperty("1:登记时间,2:签到时间,2:总检时间")
    private PatientTimeEnum timeType;

//    @NotNull(message = "时间范围不能为空")
    @ApiModelProperty("时间")
    private List<String> time;

    /** 体检分类 */
    @Query
    @ApiModelProperty("体检分类")
    private Long peTypeHdId;

    /** 体检类别 */
    @Query
    @ApiModelProperty("体检类别")
    private Long peTypeDtId;

    /** 是否回交 */
    @ApiModelProperty("是否回交")
    private Boolean isSubmit;

    /** 单位 */
    @Query
    @ApiModelProperty("单位")
    private String companyId;

    /** 流水号 */
    @Query
    @ApiModelProperty("流水号")
    private String id;

    /** 姓名 */
    @Query(type = Query.Type.INNER_LIKE)
    @ApiModelProperty("姓名")
    private String name;

    /** 身份证号 */
    @Query(type = Query.Type.INNER_LIKE)
    @ApiModelProperty("身份证号")
    private String idNo;
}
