package com.healthappy.modules.system.domain.vo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * @description 有害因素
 * @author sjctaboo_before
 * @date 2021-08-9
 */
@Data
public class BPoisonVo  {

    /**
     * id
     */
    @ApiModelProperty("id")
    private String id;



    /**
     * 毒害名称
     */
    @NotBlank(message = "毒害名称不能为空")
    @ApiModelProperty("毒害名称")
    private String poisonName;
    /**
     * 症状名称
     */
    @ApiModelProperty("症状名称")
    private String symptomName;
    /**
     * 上岗套餐ID
     */
    @ApiModelProperty("上岗套餐ID")
    private String packageBeforeId;

    /**
     * 上岗套餐名称
     */
    @ApiModelProperty("上岗套餐名称")
    private String packageBeforeName;

    /**
     *在岗套餐id
     */
    @ApiModelProperty("在岗套餐id")
    private String packageInId;

    /**
     * 在岗套餐名称
     */
    @ApiModelProperty("在岗套餐名称")
    private String packageInName;

    /**
     * 离岗套餐ID
     */
    @ApiModelProperty("离岗套餐ID")
    private String packageAfterId;

    /**
     * 离岗套餐名称
     */
    @ApiModelProperty("离岗套餐名称")
    private String packageAfterName;
    /**
     * 岗前体检禁忌症
     */
    @ApiModelProperty("岗前体检禁忌症")
    private String tabooBefore;
    /**
     * 在岗体检禁忌症
     */
    @ApiModelProperty("在岗体检禁忌症")
    private String tabooIn;
    /**
     * 应急套餐ID
     */
    @ApiModelProperty("应急套餐ID")
    private String packageTempId;

    /**
     * 应急套餐名称
     */
    @ApiModelProperty("应急套餐名称")
    private String packageTempName;

    /**
     * 标准
     */
    @ApiModelProperty("标准")
    private String standard;

    /**
     * 检索代码
     */
    @ApiModelProperty("检索代码")
    private String searchCode;

    /**
     * 岗前体检必检项目
     */
    @ApiModelProperty("岗前体检必检项目")
    private String mustBefore;

    /**
     * 岗前体检选检项目
     */
    @ApiModelProperty("岗前体检选检项目")
    private String chooseBefore;

    /**
     * 岗前体检职业病
     */
    @ApiModelProperty("岗前体检职业病")
    private String diseaseBefore;

    /**
     * 岗前体检体格检查
     */
    @ApiModelProperty("岗前体检体格检查")
    private String checkBefore;

    /**
     * 在岗体检必检项目
     */
    @ApiModelProperty("在岗体检必检项目")
    private String mustIn;

    /**
     * 在岗体检选检项目
     */
    @ApiModelProperty("在岗体检选检项目")
    private String chooseIn;

    /**
     * 在岗体检职业病
     */
    @ApiModelProperty("在岗体检职业病")
    private String diseaseIn;

    /**
     * 在岗体检体格检查
     */
    @ApiModelProperty("在岗体检体格检查")
    private String checkIn;

    /**
     * 离岗体检必检项目
     */
    @ApiModelProperty("离岗体检必检项目")
    private String mustAfter;

    /**
     * 离岗体检选检项目
     */
    @ApiModelProperty("离岗体检选检项目")
    private String chooseAfter;

    /**
     * 离岗体检职业病
     */
    @ApiModelProperty("离岗体检职业病")
    private String diseaseAfter;

    /**
     * 离岗体检禁忌症
     */
    @ApiModelProperty("离岗体检禁忌症")
    private String tabooAfter;

    /**
     * 离岗体检体格检查
     */
    @ApiModelProperty("离岗体检体格检查")
    private String checkAfter;

    /**
     * 应急体检必检项目
     */
    @ApiModelProperty("应急体检必检项目")
    private String mustOther;

    /**
     * 应急体检选检项目
     */
    @ApiModelProperty("应急体检选检项目")
    private String chooseOther;

    /**
     * 应急体检职业病
     */
    @ApiModelProperty("应急体检职业病")
    private String diseaseOther;

    /**
     * 应急体检禁忌症
     */
    @ApiModelProperty("应急体检禁忌症")
    private String tabooOther;

    /**
     * 应急体检体格检查
     */
    @ApiModelProperty("应急体检体格检查")
    private String checkOther;

    /**
     * 排序号
     */
    @ApiModelProperty("排序号")
    private Integer dispOrder;

    /**
     * 毒害种类
     */
    @ApiModelProperty("毒害种类")
    private String posionType;

    /**
     * 修改医生
     */
    @ApiModelProperty("修改医生")
    private String updateDoc;


    /**
     * 毒害对应症状细表
     */
    @ApiModelProperty("毒害对应症状细表")
    private List<BPoisonSymptomVo>  bPoisonSymptomList = new ArrayList<>();
}
