package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.healthappy.config.SeedIdGenerator;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 费别表
 * @author fang
 * @date 2021-06-17
 */
@Data
@TableName("B_Fee_Type")
@SeedIdGenerator
public class BFeeType implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * id
     */
    @TableId(type = IdType.INPUT)
    private String id;

    /**
     * 费别名称
     */
    private String name;

    /**
     * 排序
     */
    private Integer dispOrder;

    /**
     * 是否启用
     */
    private String isEnable;

    /**
     * 删除标识
     */
    @ApiModelProperty("是否删除")
    @TableField(value = "del_flag",fill = FieldFill.INSERT)
    @TableLogic
    private String delFlag;


    /**
     * 医疗机构id(u_hospital_info的id)
     */
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;
}
