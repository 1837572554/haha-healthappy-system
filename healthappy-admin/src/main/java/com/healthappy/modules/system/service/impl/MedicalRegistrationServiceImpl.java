package com.healthappy.modules.system.service.impl;

import com.healthappy.modules.system.service.MedicalRegistrationService;
import com.healthappy.modules.system.service.dto.MedicalRegistrationDto;
import com.healthappy.modules.system.service.dto.RegisterProDto;
import com.healthappy.modules.system.service.mapper.MedicalRegistrationMapper;
import com.healthappy.utils.SecurityUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
public class MedicalRegistrationServiceImpl implements MedicalRegistrationService {

    private final MedicalRegistrationMapper registrationMapper;

    @Override
    public MedicalRegistrationDto buildBean(String id) {
        return registrationMapper.buildBean(id);
    }

    @Override
    public List<RegisterProDto> buildProject(Long departmentGroupHdId) {
        return registrationMapper.buildProject(departmentGroupHdId);
    }

    @Override
    public List<RegisterProDto> getList(String paId){
        return registrationMapper.getList(paId, SecurityUtils.getTenantId());
    }
}
