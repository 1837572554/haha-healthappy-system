package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.UNotice;
import com.healthappy.modules.system.domain.vo.UNoticeVo;
import com.healthappy.modules.system.service.dto.UNoticeQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 区域表（阿斯利康）服务层
 * @author fang
 * @date 2021-06-17
 */
public interface UNoticeService extends BaseService<UNotice> {


    Map<String, Object> queryAll(UNoticeQueryCriteria criteria, Pageable pageable);


    /**
     * 查询数据分页
     * @param criteria 条件
     * @return Map<String, Object>
     */
    List<UNotice> queryAll(UNoticeQueryCriteria criteria);

    void saveOrUpdateUnotice(UNoticeVo resources);

    /**
     * 我的公告，有效范围内的公告
     * @author YJ
     * @date 2022/3/14 11:13
     * @return java.util.List〈com.healthappy.modules.system.domain.UNotice〉
     */
    List<UNotice> myNotice();
}
