package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.User;
import com.healthappy.modules.system.domain.UserTag;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 标识
 * @author FGQ
 * @date 2021-06-17
 */
@Mapper
@Repository
public interface UserTagMapper extends CoreMapper<UserTag> {


    /**
     * 根据标识值，获取对应的用户列表
     * @author YJ
     * @date 2022/2/28 18:19
     * @param per
     * @return java.util.List〈com.healthappy.modules.system.domain.User〉
     */
    @SqlParser(filter = true)
    List<User> getUsersByTagPer(@Param("per") String per);

    /**
     * 根据标识值，获取对应的用户列表
     * @author YJ
     * @date 2022/2/28 18:19
     * @param per
     * @param tenantId
     * @return java.util.List〈com.healthappy.modules.system.domain.User〉
     */
    @SqlParser(filter = true)
    List<User> getUsersByTagPerTenantId(@Param("per") String per,@Param("tenantId") String tenantId);

}
