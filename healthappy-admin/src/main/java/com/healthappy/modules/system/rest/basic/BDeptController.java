package com.healthappy.modules.system.rest.basic;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BDept;
import com.healthappy.modules.system.domain.BGroupHd;
import com.healthappy.modules.system.domain.BItem;
import com.healthappy.modules.system.domain.vo.BDeptVo;
import com.healthappy.modules.system.service.BDeptService;
import com.healthappy.modules.system.service.BGroupHdService;
import com.healthappy.modules.system.service.BItemService;
import com.healthappy.modules.system.service.dto.BDeptDelDto;
import com.healthappy.modules.system.service.dto.BDeptQueryCriteria;
import com.healthappy.utils.CacheConstant;
import com.healthappy.utils.R;
import com.healthappy.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 部门控制器
 * @author FGQ
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "基础管理：科室设置")
@ApiSort(value = 1)
@RestController
@AllArgsConstructor
@RequestMapping("/BDept")
public class BDeptController {

    private final BDeptService bDeptService;

    private final BItemService bItemService;

    private final BGroupHdService bGroupHdService;

    @Log("查询科室")
    @ApiOperation("查询科室，权限码：BDept:list")
    @GetMapping
    @PreAuthorize("@el.check('BDept:list')")
    public R list(BDeptQueryCriteria criteria, Pageable pageable) {
        return R.ok(bDeptService.queryAll(criteria, pageable));
    }

    @Log("查询所有科室")
    @ApiOperation("查询所有科室，权限码：BDept:list")
    @GetMapping("/all")
    @Cacheable(cacheNames = {CacheConstant.B_DEPT})
    public R list(BDeptQueryCriteria criteria) {
        QueryWrapper<BDept> wrapper = QueryHelpPlus.getPredicate(BDept.class, criteria);
        wrapper.orderByAsc("disp_order");
        return R.ok(bDeptService.list(wrapper));
    }

    @Log("新增|修改科室")
    @ApiOperation("新增|修改科室，权限码：BDept:add")
    @PostMapping
    @PreAuthorize("@el.check('BDept:add')")
    @Klock
    @CacheEvict(cacheNames = {CacheConstant.B_ITEM,CacheConstant.B_DEPT},allEntries = true)
    public R saveOrUpdate(@Validated @RequestBody BDeptVo resources) {
        if(checkNameRepeat(resources)){
         return R.error("科室名称不能重复");
        }
        return R.ok(bDeptService.saveOrUpdate(BeanUtil.copyProperties(resources, BDept.class)));
    }

    @Log("删除科室")
    @ApiOperation("删除科室，权限码：BDept:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('BDept:delete')")
    @Klock
    @CacheEvict(cacheNames = {CacheConstant.B_ITEM,CacheConstant.B_DEPT},allEntries = true)
    public R remove(@RequestBody Set<Integer> ids) {
        checkIfItExists(ids);
        return R.ok(bDeptService.removeByIds(ids));
    }

    @Log("删除科室")
    @ApiOperation("删除科室，权限码：BDept:delete")
    @PostMapping("/delDept")
    @PreAuthorize("@el.check('BDept:delete')")
    @Klock
    @CacheEvict(cacheNames = {CacheConstant.B_ITEM,CacheConstant.B_DEPT},allEntries = true)
    public R delDept(@RequestBody BDeptDelDto dto){
        if(!dto.getConfirm()) {
            String msg=bDeptService.checkExistsAssociate(dto.getIds());
            if(StringUtils.isNotBlank(msg)){
                return R.error(200,msg);
            }
        }
        return R.ok(bDeptService.lambdaUpdate().set(BDept::getDelFlag,"1").in(BDept::getId,dto.getIds()).update());
    }

    @Log("获取启用科室且下级存在细项")
    @ApiOperation("获取启用科室且下级存在细项，权限码：BDept:list")
    @GetMapping("/getItemSearchDept")
    @PreAuthorize("@el.check('BDept:list')")
    public R getDeptEnableAndExistsItem(){
        return R.ok(bDeptService.getDeptEnableAndExistsItem());
    }

    /**
     * 检测项目数据是否存在如果存在就抛异常
     * @author YJ
     * @date 2021/12/27 16:14
     * @param ids
     */
    private void checkIfItExists(Set<Integer> ids) {
        if(bItemService.lambdaQuery().in(BItem::getDeptId,ids).count() > 0){
            throw new BadRequestException("体检项目中存在删除的科室,请先删除体检项目");
        }
        if(bGroupHdService.lambdaQuery().in(BGroupHd::getDeptId,ids).count() > 0){
            throw new BadRequestException("组合项目中存在删除的科室,请先删除组合项目");
        }
    }

    private boolean checkNameRepeat(BDeptVo resources) {
        return bDeptService.lambdaQuery().eq(BDept::getName,resources.getName())
                .ne(ObjectUtil.isNotNull(resources.getId()),BDept::getId,resources.getId()).count() > 0;
    }
}
