package com.healthappy.modules.system.service.impl;

import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.TZyAnswerBank;
import com.healthappy.modules.system.service.TZyAnswerBankService;
import com.healthappy.modules.system.service.mapper.TZyAnswerBankMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Jevany
 * @date 2021/12/8 19:38
 * @desc 体检者问卷答案库 服务实现类
 */
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class TZyAnswerBankServiceImpl extends BaseServiceImpl<TZyAnswerBankMapper, TZyAnswerBank> implements TZyAnswerBankService {
}
