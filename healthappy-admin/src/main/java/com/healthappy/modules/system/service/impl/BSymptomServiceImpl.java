package com.healthappy.modules.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BSymptom;
import com.healthappy.modules.system.domain.BTypeApplyForm;
import com.healthappy.modules.system.service.BSymptomService;
import com.healthappy.modules.system.service.dto.BSymptomDto;
import com.healthappy.modules.system.service.dto.BSymptomQueryCriteria;
import com.healthappy.modules.system.service.dto.BTypeApplyFormDto;
import com.healthappy.modules.system.service.dto.BTypeApplyFormQueryCriteria;
import com.healthappy.modules.system.service.mapper.BSymptomMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor//有自动注入的功能
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BSymptomServiceImpl extends BaseServiceImpl<BSymptomMapper, BSymptom> implements BSymptomService {

    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BSymptomQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BSymptom> page = new PageInfo<BSymptom>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("content", generator.convert(page.getList(), BSymptomDto.class));
        map.put("totalElements", page.getTotal());

        return map;
    }

    @Override
    public List<BSymptom> queryAll(BSymptomQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BSymptomDto.class, criteria));
    }





}
