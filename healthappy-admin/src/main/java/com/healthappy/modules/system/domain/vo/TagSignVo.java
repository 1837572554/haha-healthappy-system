package com.healthappy.modules.system.domain.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
public class TagSignVo {

    private Set<Long> signIds;

    @NotNull(message = "标识不能为空")
    private Long tagId;
}
