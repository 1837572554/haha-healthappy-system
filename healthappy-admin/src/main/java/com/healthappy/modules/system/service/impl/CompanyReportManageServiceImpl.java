package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.TCompanyRepDt;
import com.healthappy.modules.system.domain.TCompanyRepHd;
import com.healthappy.modules.system.domain.vo.CompanyReportVO;
import com.healthappy.modules.system.service.CompanyReportManageService;
import com.healthappy.modules.system.service.TCompanyRepDtService;
import com.healthappy.modules.system.service.dto.*;
import com.healthappy.modules.system.service.mapper.TCompanyRepHdMapper;
import com.healthappy.modules.system.service.mapper.TPatientMapper;
import com.healthappy.utils.FileUtil;
import com.healthappy.utils.PageUtil;
import com.healthappy.utils.SecurityUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

/**
 * 单位报告管理 服务实现类
 *
 * @author Jevany
 * @date 2022/2/17 15:52
 */
@AllArgsConstructor
@Service
public class CompanyReportManageServiceImpl extends BaseServiceImpl<TCompanyRepHdMapper, TCompanyRepHd> implements CompanyReportManageService {

    private final TCompanyRepHdMapper companyRepHdMapper;
    private final TPatientMapper tPatientMapper;
    private final TCompanyRepDtService companyRepDtService;

    @Override
    public Map<String, Object> getList(CompanyReportQueryCriteria criteria) {
        //总条数
        Integer totalCount = companyRepHdMapper.getListCount(criteria);
        Map<String, Object> map = new LinkedHashMap<>(4);
        if (totalCount == 0) {
            map.put("content", new ArrayList<>());
            map.put("totalElements", totalCount);
            map.put("totalPage", 0);
            map.put("currentPage", 1);
            return map;
        }

        Integer[] pageBase = PageUtil.toStartEnd(criteria.getPageNum(), criteria.getPageSize(), totalCount);
        criteria.setStartIndex(pageBase[0]);
        map.put("content", companyRepHdMapper.getList(criteria));
        map.put("totalElements", totalCount);
        map.put("totalPage", pageBase[3]);
        map.put("currentPage", pageBase[2]);
        return map;
    }

    @Override
    public String saveReport(CompanyReportVO companyRepHdVO) {
        TCompanyRepHd tCompanyRepHd = BeanUtil.copyProperties(companyRepHdVO, TCompanyRepHd.class);
        tCompanyRepHd.setCompileTime(new Timestamp(System.currentTimeMillis()));
        tCompanyRepHd.setCompileDoctor(SecurityUtils.getNickName());
        this.saveOrUpdate(tCompanyRepHd);
        CompanyReportPersonSaveDTO saveDTO = new CompanyReportPersonSaveDTO();
        saveDTO.setRepId(tCompanyRepHd.getId());
        saveDTO.setPaIds(companyRepHdVO.getPaIdList());
        savePerson(saveDTO);
        return tCompanyRepHd.getId();
    }

    @Override
    public void savePerson(CompanyReportPersonSaveDTO saveDTO) {
        if (StrUtil.isBlank(saveDTO.getRepId())) {
            return;
        }
        //清空人员列表
        companyRepDtService.remove(Wrappers.<TCompanyRepDt>lambdaUpdate().eq(TCompanyRepDt::getRepId, saveDTO.getRepId()));

        if (CollUtil.isNotEmpty(saveDTO.getPaIds())) {
            List<TCompanyRepDt> companyRepDtList = new ArrayList<>();
            String repId = saveDTO.getRepId();
            for (String paId : saveDTO.getPaIds()) {
                TCompanyRepDt repDt = new TCompanyRepDt();
                repDt.setRepId(repId);
                repDt.setPaId(paId);
                companyRepDtList.add(repDt);
            }
            if (CollUtil.isNotEmpty(companyRepDtList)) {
                this.lambdaUpdate().eq(TCompanyRepHd::getId,saveDTO.getRepId()).set(TCompanyRepHd::getCheckNum,companyRepDtList.size()).update();
                companyRepDtService.saveBatch(companyRepDtList);
            }
        }
    }

    @Override
    public Map<String, Object> getPerson(CompanyReportPersonQueryCriteria criteria) {
        //总条数
        Integer totalCount = tPatientMapper.getPersonCount(criteria.getRepId());
        Map<String, Object> map = new LinkedHashMap<>(4);
        //如总条数是0则直接返回
        if (totalCount == 0) {
            map.put("content", new ArrayList<>());
            map.put("totalElements", 0);
            map.put("totalPage", 0);
            map.put("currentPage", 1);
            return map;
        }
        //计算列表
        Integer[] pageBase = PageUtil.toStartEnd(criteria.getPageNum(), criteria.getPageSize(), totalCount);
        criteria.setStartIndex(pageBase[0]);
        map.put("content", tPatientMapper.getPersons(criteria));
        map.put("totalElements", totalCount);
        map.put("totalPage", pageBase[3]);
        map.put("currentPage", pageBase[2]);
        return map;
    }

    @Override
    public Map<String, Object> getOccupationPersonList(CompanyReportOccupationPersonQueryCriteria criteria) {
        //总条数
        Integer totalCount = tPatientMapper.getCompanyOccupationReportPersonCount(criteria);
        Map<String, Object> map = new LinkedHashMap<>(4);
        //如总条数是0则直接返回
        if (totalCount == 0) {
            map.put("content", new ArrayList<>());
            map.put("totalElements", 0);
            map.put("totalPage", 0);
            map.put("currentPage", 1);
            return map;
        }
        //计算列表
        Integer[] pageBase = PageUtil.toStartEnd(criteria.getPageNum(), criteria.getPageSize(), totalCount);
        criteria.setStartIndex(pageBase[0]);
        map.put("content", tPatientMapper.getCompanyOccupationReportPerson(criteria));
        map.put("totalElements", totalCount);
        map.put("totalPage", pageBase[3]);
        map.put("currentPage", pageBase[2]);
        return map;
    }

    @Override
    public Map<String, Object> getHealthPersonList(CompanyReportHealthPersonQueryCriteria criteria) {
        //总条数
        Integer totalCount = tPatientMapper.getCompanyHealthReportPersonCount(criteria);
        Map<String, Object> map = new LinkedHashMap<>(4);
        //如总条数是0则直接返回
        if (totalCount == 0) {
            map.put("content", new ArrayList<>());
            map.put("totalElements", totalCount);
            map.put("totalPage", 0);
            map.put("currentPage", 1);
            return map;
        }
        //计算列表
        Integer[] pageBase = PageUtil.toStartEnd(criteria.getPageNum(), criteria.getPageSize(), totalCount);
        criteria.setStartIndex(pageBase[0]);
         map.put("content", tPatientMapper.getCompanyHealthReportPerson(criteria));
        map.put("totalElements", totalCount);
        map.put("totalPage", pageBase[3]);
        map.put("currentPage", pageBase[2]);
        return map;
    }

    @Override
    public Map<String, Object> getSicknessList(CompanyReportPersonQueryCriteria criteria) {
        criteria.setTenantId(SecurityUtils.getTenantId());
        //取总数据列表-过滤后的病种Id
        List<CompanyRepSicknessListDTO> sicknessTotalDate = companyRepHdMapper.getSicknessTotalDate(criteria.getRepId(), criteria.getTenantId());
        Map<String, Object> map = new LinkedHashMap<>(4);
        if (CollUtil.isEmpty(sicknessTotalDate)) {
            map.put("content", new ArrayList<>());
            map.put("totalElements", 0);
            map.put("totalPage", 0);
            map.put("currentPage", 1);
            return map;
        }

        //获取到全部数据
        List<CompanyRepSicknessDetailDTO> sicknessDetail = companyRepHdMapper.getSicknessDetail(criteria.getRepId(), criteria.getTenantId());
        //所有数据统计
        sicknessTotalDate.forEach(td -> {
            td.setSicknessName(sicknessDetail.stream().filter(sd -> td.getSicknessId().equals(sd.getSicknessId())).findAny().get().getSicknessName());
            td.setTotalCount(sicknessDetail.stream().filter(sd -> td.getSicknessId().equals(sd.getSicknessId())).count());
            //1男 2女
            td.setMaleCount(sicknessDetail.stream().filter(sd -> td.getSicknessId().equals(sd.getSicknessId()) && "1".equals(sd.getSex())).count());
            td.setFemaleCount(sicknessDetail.stream().filter(sd -> td.getSicknessId().equals(sd.getSicknessId()) && "2".equals(sd.getSex())).count());
        });
        //排序 倒序
        sicknessTotalDate.sort((t1, t2) -> t2.getTotalCount().compareTo(t1.getTotalCount()) * -1);

        if (criteria.getIsExport()) {
            map.put("content", sicknessTotalDate);
            return map;
        } else {
            //总条数
            Integer totalCount = sicknessTotalDate.size();
            //计算列表
            Integer[] pageBase = PageUtil.toStartEnd(criteria.getPageNum(), criteria.getPageSize(), totalCount);
            map.put("content", sicknessTotalDate.subList(pageBase[0], pageBase[1]));
            map.put("totalElements", totalCount);
            map.put("totalPage", pageBase[3]);
            map.put("currentPage", pageBase[2]);
            return map;
        }
    }

    @Override
    public void exportSicknessList(List<CompanyRepSicknessListDTO> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        Integer idx = 0;
        for (CompanyRepSicknessListDTO dto : all) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("序号", ++idx);
            map.put("疾病名称", dto.getSicknessName());
            map.put("人数", dto.getTotalCount());
            map.put("男", dto.getMaleCount());
            map.put("女", dto.getFemaleCount());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    @Override
    public Map<String, Object> getSicknessPersonList(CompanyReportSicknessPersonListQueryCriteria criteria) {
        criteria.setTenantId(SecurityUtils.getTenantId());
        //查询总条数
        Integer totalCount = companyRepHdMapper.getSicknessPersonListCount(criteria);
        Map<String, Object> map = new LinkedHashMap<>(4);
        if (totalCount == 0) {
            map.put("content", new ArrayList<>());
            map.put("totalElements", 0);
            map.put("totalPage", 0);
            map.put("currentPage", 1);
            return map;
        }

        if (!criteria.getIsExport()) {
            Integer[] pageBase = PageUtil.toStartEnd(criteria.getPageNum(), criteria.getPageSize(), totalCount);
            criteria.setStartIndex(pageBase[0]);
            map.put("content", companyRepHdMapper.getSicknessPersonList(criteria));
            map.put("totalElements", totalCount);
            map.put("totalPage", pageBase[3]);
            map.put("currentPage", pageBase[2]);
            return map;
        } else {
            map.put("content", companyRepHdMapper.getSicknessPersonList(criteria));
            return map;
        }
    }

    @Override
    public void exportSicknessPersonList(List<CompanyReportSicknessPersonListDTO> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        Integer idx = 0;
        for (CompanyReportSicknessPersonListDTO dto : all) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("序号", ++idx);
            map.put("疾病名称", dto.getSicknessName());
            map.put("姓名", dto.getName());
            map.put("性别", dto.getSex());
            map.put("年龄", dto.getAge());
            map.put("电话", dto.getPhone());
            map.put("身份证", dto.getIdNo());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    @Override
    public void verifyReport(CompanyReportVerifyDTO verifyDTO) {
        TCompanyRepHd tCompanyRepHd = companyRepHdMapper.selectById(verifyDTO.getRepId());
        if (!Optional.ofNullable(tCompanyRepHd).isPresent()) {
            throw new BadRequestException("单位报告数据不存在");
        }
        if (verifyDTO.getVerify()) {
            //需要审核
            if (ObjectUtil.isNotNull(tCompanyRepHd.getVerifyTime())) {
                throw new BadRequestException("已审核，无法再次审核");
            }
            this.lambdaUpdate().set(TCompanyRepHd::getVerifyTime, new Timestamp(System.currentTimeMillis()))
                    .set(TCompanyRepHd::getVerifyDoctor, SecurityUtils.getNickName())
                    .eq(TCompanyRepHd::getId, verifyDTO.getRepId()).update();
        } else {
            //取消审核
            if (ObjectUtil.isNull(tCompanyRepHd.getVerifyTime())) {
                throw new BadRequestException("未审核，不能取消审核");
            }
            this.lambdaUpdate().set(TCompanyRepHd::getVerifyTime, null).set(TCompanyRepHd::getVerifyDoctor, null)
                    .eq(TCompanyRepHd::getId, verifyDTO.getRepId()).update();
        }
    }
}