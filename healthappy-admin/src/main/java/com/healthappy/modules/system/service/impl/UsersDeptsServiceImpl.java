package com.healthappy.modules.system.service.impl;

import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.UsersDepts;
import com.healthappy.modules.system.service.UsersDeptsService;
import com.healthappy.modules.system.service.mapper.UsersDeptsMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
 * @author yanjun
 * @description
 * @date 2021/11/3
 */
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true,rollbackFor = Exception.class)
public class UsersDeptsServiceImpl extends BaseServiceImpl<UsersDeptsMapper, UsersDepts> implements UsersDeptsService {

    @Override
    public Set<UsersDepts> findByUserId(Long userId) {
        return baseMapper.findByUserId(userId);
    }
}
