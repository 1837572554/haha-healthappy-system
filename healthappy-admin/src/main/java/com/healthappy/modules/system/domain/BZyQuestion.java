package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @desc: 中医体质问卷问题
 * @author: YJ
 * @date: 2021-12-08 14:41
 **/
@Data
@ApiModel("中医体质问卷问题")
@TableName("B_ZY_QUESTION")
public class BZyQuestion implements Serializable {

    /** 问题编号 */
    @ApiModelProperty("问题编号")
    private String question_id;

    /** 库编号  1：33题 2：67题 */
    @ApiModelProperty("库编号  1：33题 2：67题")
    private Integer bankId;

    /** 体质所属编号 */
    @ApiModelProperty("体质所属编号")
    private String tzId;

    /** 问题 */
    @ApiModelProperty("问题")
    private String content;

    /** 适用性别 0：不限 1：男 2：女 */
    @ApiModelProperty("适用性别 0：不限 1：男 2：女")
    private String sex;

    /** 是否反向分值 0：正常 1：反向*/
    @ApiModelProperty("是否反向分值 0：正常 1：反向")
    private String change;

    /** 显示序号 */
    @ApiModelProperty("显示序号")
    private Integer dispOrder;
}
