package com.healthappy.modules.system.service.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BallCheckExportDto {

    @ColumnWidth(50)
    @ExcelProperty("姓名")
    @ApiModelProperty("姓名")
    private String name;

    @ColumnWidth(50)
    @ExcelProperty("性别")
    @ApiModelProperty("性别")
    private Integer sex;

    @ColumnWidth(50)
    @ExcelProperty("性别名称")
    @ApiModelProperty("性别名称")
    private String sexName;

    @ColumnWidth(50)
    @ExcelProperty("年龄")
    @ApiModelProperty("年龄")
    private Integer age;

    @ColumnWidth(50)
    @ExcelProperty("身份证")
    @ApiModelProperty("身份证")
    private String idNo;

    @ColumnWidth(50)
    @ExcelProperty("婚姻状况名称")
    @ApiModelProperty("婚姻状况名称")
    private String marryName;

    @ColumnWidth(50)
    @ExcelProperty("单位名称")
    @ApiModelProperty("单位名称")
    private String companyName;


    @ColumnWidth(50)
    @ExcelProperty("部门名称")
    @ApiModelProperty("部门名称")
    private String departmentName;


    @ColumnWidth(50)
    @ExcelProperty("体检分类名称")
    @ApiModelProperty("体检分类名称")
    private String peTypeName;

    @ColumnWidth(50)
    @ExcelProperty("体检类别名称")
    @ApiModelProperty("体检类别名称")
    private String typeName;

    @ColumnWidth(50)
    @ExcelProperty("危害因素名称")
    @ApiModelProperty("危害因素名称")
    private String poisonTypeName;
}
