package com.healthappy.modules.system.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

/**
 * 危急值状况查询类
 * @author Jevany
 * @date 2022/2/10 11:09
 */
@Data
public class CriticalQueryCriteria {
    /**
     * 体检时间类型
     */
//    @NotNull(message = "体检时间类型不能为空")
    @ApiModelProperty(value = "体检时间类型 ,页面路径：criticalStatusManage", position = 0)
    private Integer dateType;

    /** 时间段（数组） */
//    @NotEmpty(message = "时间段不能为空")
//    @Size(min = 2,max = 2,message = "必须2个对象")
    @ApiModelProperty(value = "时间段（数组） ", position = 1)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private List<Timestamp> timeList;

    /**
     * 体检分类ID
     */
    @ApiModelProperty("体检分类ID")
    private String peTypeHdId;

    /**
     * 体检类别ID
     */
    @ApiModelProperty("体检类别ID")
    private String peTypeDtId;

    /**
     * 体检号
     */
    @ApiModelProperty("体检号")
    private String paId;

    /**
     * 体检者姓名
     */
    @ApiModelProperty("体检者姓名")
    private String paName;

    /**
     * 体检者身份证
     */
    @ApiModelProperty("体检者身份证")
    private String idNo;

    /**
     * 单位ID
     */
    @ApiModelProperty("单位ID")
    private String companyId;

    /** 是否处理 */
    @ApiModelProperty("是否处理")
    private Boolean deal;

    /** 处理医生 */
    @ApiModelProperty("处理医生")
    private String dealDoctor;

    /** 异常类型 */
    @ApiModelProperty("异常类型")
    private String affectLevel;

    /**
     * 页面显示条数
     */
    @ApiModelProperty(value = "页面显示条数,默认为10", position = 101)
    private Integer pageSize = 10;

    /**
     * 页数
     */
    @ApiModelProperty(value = "页数,默认为1", position = 102)
    private Integer pageNum = 1;


    /**
     * 开始条数
     */
    @ApiModelProperty(value = "开始条数", hidden = true)
    private Integer startIndex = -1;

    /**
     * 是否导出，如果为True，分页功能不启用，默认False
     */
    @ApiModelProperty(value = "是否导出，如果为True，分页功能不启用，默认False", hidden = true)
    private Boolean isExport = false;
}