package com.healthappy.modules.system.rest.basic;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BPackageDT;
import com.healthappy.modules.system.domain.BPoison;
import com.healthappy.modules.system.domain.BPoisonSymptom;
import com.healthappy.modules.system.domain.vo.BPoisonSymptomVo;
import com.healthappy.modules.system.domain.vo.BPoisonVo;
import com.healthappy.modules.system.service.BPoisonService;
import com.healthappy.modules.system.service.BPoisonSymptomService;
import com.healthappy.modules.system.service.dto.BPoisonQueryCriteria;
import com.healthappy.modules.system.service.dto.BRegisterPoisonQueryCriteria;
import com.healthappy.modules.system.service.dto.BRegisterPoisonQueryLiteCriteria;
import com.healthappy.modules.system.service.dto.RegisterPoisonQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @description 有害因素   控制器
 * @author sjc
 * @date 2021-08-9
 */
@Slf4j
@Api(tags = "基础管理：有害因素设置")
@RestController
@AllArgsConstructor
@RequestMapping("/BPoison")
public class BPoisonController {
    private final IGenerator generator;
    private final BPoisonService bPoisonService;
    private final BPoisonSymptomService bPoisonSymptomService;

    @Log("体检登记-查询有害因素")
    @ApiOperation("体检登记-查询有害因素，权限码：BPoison:list")
    @GetMapping("/register")
    public R register(BRegisterPoisonQueryCriteria criteria,@PageableDefault(size = Integer.MAX_VALUE) Pageable pageable) {
        return R.ok(bPoisonService.registerPoisonQueryAll(criteria,pageable));
    }

    @Log("体检登记-查询有害因素（毒害编号、名称、简拼）")
    @ApiOperation("体检登记-查询有害因素（毒害编号、名称、简拼），权限码：BPoison:list")
    @GetMapping("/registerLite")
    public R registerLite(BRegisterPoisonQueryLiteCriteria criteria) {
        return R.ok(bPoisonService.registerPoisonQueryAllLite(criteria));
    }

    @Log("体检登记-查询有害因素-套餐数据")
    @ApiOperation("体检登记-查询有害因素-套餐数据，权限码：BPoison:list")
    @GetMapping("/poisonPackage")
    public R poisonPackage(BRegisterPoisonQueryCriteria criteria) {
        if (ObjectUtil.isNull(criteria.getCode())){
            throw new BadRequestException("体检类别不能为空");
        }
        return R.ok(bPoisonService.registerPoisonPackage(criteria));
    }

    @Log("体检登记-查询有害因素-组合项目")
    @ApiOperation("体检登记-查询有害因素-组合项目，权限码：BPoison:list")
    @GetMapping("/poisonPackageDt")
    public R poisonPackageDt(BRegisterPoisonQueryLiteCriteria criteria) {
        if (ObjectUtil.isNull(criteria.getCode())){
            throw new BadRequestException("体检类别不能为空");
        }
        return R.ok(bPoisonService.registerPoisonPackageDt(criteria));
    }

    @Log("体检登记-查询有害因素集合下的组合项目")
    @ApiOperation("体检登记-查询有害因素集合下的组合项目，权限码：BPoison:list")
    @PostMapping("/getPoisonGroups")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "返回列表中毒害IDList对应的组合项目", response = BPackageDT.class)
    })
    public R getPoisonGroups(@Validated @RequestBody RegisterPoisonQueryCriteria criteria){
        return R.ok(bPoisonService.getPoisonGroups(criteria));
    }

    @Log("查询有害因素数据")
    @ApiOperation("查询有害因素数据，权限码：BPoison:list")
    @GetMapping
    public R list(BPoisonQueryCriteria criteria, Pageable pageable) {
        return R.ok(bPoisonService.queryAll(criteria,pageable));
    }

    @Log("查询全部有害因素数据")
    @ApiOperation("查询全部有害因素数据，权限码：BPoison:list")
    @GetMapping("/list")
    public R all(BPoisonQueryCriteria criteria,@PageableDefault(size = Integer.MAX_VALUE) Pageable pageable) {
        return R.ok(bPoisonService.queryAll(criteria,pageable));
    }

    @Log("根据体检类别查询有害因素数据")
    @ApiOperation("根据体检类别查询有害因素数据，权限码：BPoison:list")
    @GetMapping("/peTypeDtId")
    public R list(@RequestParam("peTypeDtId") String peTypeDtId,@RequestParam(required = false)Integer sex) {
        return R.ok(bPoisonService.queryAllByPeTypeDt(peTypeDtId,sex));
    }

    @Log("新增|修改有害因素")
    @ApiOperation("新增|修改有害因素，权限码：BPoison:add")
    @PostMapping
    @PreAuthorize("@el.check('BPoison:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BPoisonVo resources) {
        if(checkNameRepeat(resources)){
            return R.error("毒害名称不能重复");
        }
        BPoison bPoison = new BPoison();
        //用于将对象属性拷贝到另一个对象（类型，名字一样即可）
        //第一个是数据源，第二个是赋值对象
        BeanUtil.copyProperties(resources,bPoison);
        if(bPoisonService.saveOrUpdate(bPoison))
        {
            List<BPoisonSymptomVo> list = bPoison.getBPoisonSymptomList();
            list.forEach(c->c.setPoisonId(bPoison.getId()));

            bPoisonSymptomService.deleteByPoId(bPoison.getId());
            bPoisonSymptomService.saveBatch(generator.convert(list, BPoisonSymptom.class));
            return R.ok();
        }
        return R.error(500,"方法异常");
    }

    @Log("删除有害因素")
    @ApiOperation("删除有害因素，权限码：BPoison:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('BPoison:delete')")
    @Klock
    public R remove(@RequestBody List<Long> ids) {
        if(bPoisonService.removeByIds(ids))
        {
            bPoisonSymptomService.deleteByPoIds(ids);
            return R.ok();
        }
        return R.error(500,"方法异常");
    }

    private boolean checkNameRepeat(BPoisonVo resources) {
        return bPoisonService.lambdaQuery().eq(BPoison::getPoisonName, resources.getPoisonName())
                .ne(ObjectUtil.isNotNull(resources.getId()),BPoison::getId,resources.getId()).count() > 0;
    }
}
