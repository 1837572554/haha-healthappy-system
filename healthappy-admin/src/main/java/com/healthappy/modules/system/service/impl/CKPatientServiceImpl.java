package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.BGroupHd;
import com.healthappy.modules.system.domain.BItem;
import com.healthappy.modules.system.domain.TItemDT;
import com.healthappy.modules.system.domain.TItemHD;
import com.healthappy.modules.system.service.*;
import com.healthappy.modules.system.service.dto.QueryCustomDTO;
import com.healthappy.modules.system.service.dto.QueryDataCriteria;
import com.healthappy.modules.system.service.mapper.CKPatientMapper;
import com.healthappy.utils.FileUtil;
import com.healthappy.utils.SecurityUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class CKPatientServiceImpl implements CKPatientService {

    private final CKPatientMapper mapper;

    private final TItemDTService itemDtService;

    private final TItemHDService tItemHdService;

    private final BItemService itemService;

    private final BGroupHdService groupHdService;

    @Override
    public void reportExecl(QueryDataCriteria criteria, HttpServletResponse response) {
        List<QueryCustomDTO> patList = this.queryAll(criteria);
        if(CollUtil.isEmpty(patList)){
            throw new BadRequestException("数据为空");
        }
        try {
            List<JSONObject> convertFieldData = convertFieldData(patList, criteria);
            FileUtil.exportExcel(new ArrayList<>(convertFieldData), response,"自定义查询");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<JSONObject> convertFieldData(List<QueryCustomDTO> patList,QueryDataCriteria criteria) {
        List<String> groupFields = Arrays.asList("groupName","resultDesc","resultMark");
        List<String> itemFields = Arrays.asList("itemName","itemResultMark");
        List<String> paIds = patList.stream().map(QueryCustomDTO::getId).collect(Collectors.toList());

        List<TItemDT> itemDtList = new ArrayList<>();
        List<TItemHD> itemHdList = new ArrayList<>();
        List<BGroupHd> groupHdList = new ArrayList<>();
        List<BItem> itemList = new ArrayList<>();
        if(CollUtil.isNotEmpty(criteria.getItemList())){
            itemList = itemService.lambdaQuery().in(BItem::getId,criteria.getItemList()).list();
            itemDtList.addAll(itemDtService.lambdaQuery().in(TItemDT::getPaId, paIds).in(TItemDT::getItemId, criteria.getItemList()).list());
        }
        if(CollUtil.isNotEmpty(criteria.getGroupList())){
            groupHdList = groupHdService.lambdaQuery().in(BGroupHd::getId, criteria.getGroupList()).list();
            itemHdList.addAll(tItemHdService.lambdaQuery().in(TItemHD::getPaId, paIds).in(TItemHD::getGroupId, criteria.getGroupList()).list());
        }
        if(CollUtil.isNotEmpty(itemList) && CollUtil.isNotEmpty(groupHdList)){
            List<String> groupNames = groupHdList.stream().map(BGroupHd::getName).collect(Collectors.toList());
            AtomicLong count = new AtomicLong(0);
            itemList = itemList.stream().peek(item-> item.setName(buildNameKey(item.getName(),groupNames,count))).collect(Collectors.toList());
        }

        List<BGroupHd> finalGroupHdList = groupHdList;
        List<BItem> finalItemList = itemList;
        return patList.parallelStream().map(i -> {
            JSONObject jsonObject = new JSONObject(i, false);
            JSONObject object = new JSONObject();
            //是否转换word-data
            criteria.getQueryField().forEach(e -> {
                if(!groupFields.contains(e) && !itemFields.contains(e)){
                    try {
                        Field field = i.getClass().getDeclaredField(e);
                        ApiModelProperty annotation = field.getAnnotation(ApiModelProperty.class);
                        if (annotation != null) {
                            object.put(annotation.value(), jsonObject.getStr(e, ""));
                        }
                    } catch (NoSuchFieldException noSuchFieldException) {
                        noSuchFieldException.printStackTrace();
                    }
                }
                if(groupFields.contains(e)){
                    criteria.getGroupList()
                            .forEach(c->{
                                Optional<TItemHD> optional = Optional.empty();
                                if(CollUtil.isNotEmpty(itemHdList)){
                                     optional = itemHdList.stream()
                                            .filter(y->y.getPaId().equals(i.getId()))
                                            .filter(k -> k.getGroupId().equals(c)).findAny();
                                }
                                if(optional.isPresent()){
                                    TItemHD itemHd = optional.get();
                                    object.put(itemHd.getGroupName(),StrUtil.blankToDefault(itemHd.getResult(),""));
                                    if(criteria.getQueryField().contains("resultDesc")){
                                        object.put(itemHd.getGroupName() +"判定", "1".equals(StrUtil.blankToDefault(itemHd.getResultMark(),"")) ? "异常":"正常");
                                    }
                                    if(criteria.getQueryField().contains("resultMark")){
                                        object.put(itemHd.getGroupName() +"描述",StrUtil.blankToDefault(itemHd.getResultDesc(),""));
                                    }
                                }else {
                                    BGroupHd groupHd = finalGroupHdList.stream().filter(g->g.getId().equals(c)).findAny().get();
                                    object.put(groupHd.getName(),"");
                                    if(criteria.getQueryField().contains("resultDesc")) {
                                        object.put(groupHd.getName() + "判定", "");
                                    }
                                    if(criteria.getQueryField().contains("resultMark")) {
                                        object.put(groupHd.getName() + "描述", "");
                                    }
                                }
                            });
                }
                if(itemFields.contains(e)){
                    criteria.getItemList()
                        .forEach(c->{
                            Optional<TItemDT> optional = Optional.empty();
                            if(CollUtil.isNotEmpty(itemDtList)){
                                 optional = itemDtList.stream()
                                        .filter(y->y.getPaId().equals(i.getId()))
                                        .filter(k -> k.getItemId().equals(c)).findAny();
                            }
                            BItem item = finalItemList.stream().filter(b -> b.getId().equals(c)).findAny().get();
                            if(optional.isPresent()){
                                TItemDT itemDT = optional.get();
                                object.put(item.getName(),StrUtil.blankToDefault(itemDT.getResult(),""));
                                if(criteria.getQueryField().contains("itemResultMark")){
                                    object.put(item.getName() + "判定",StrUtil.blankToDefault(itemDT.getResultMark(),""));
                                }
                            }else {
                                object.put(item.getName(),"");
                                if(criteria.getQueryField().contains("itemResultMark")){
                                    object.put(item.getName()+ "判定","");
                                }
                            }
                        });
                }
            });
            return object;
        }).collect(Collectors.toList());
    }

    public String buildNameKey(String key,List<String> groupNames,AtomicLong count){
        if(!groupNames.contains(key)){
            return key;
        }
        return key + count.incrementAndGet();
    }

    @Override
    public Map<String, Object> getPage(QueryDataCriteria criteria, Pageable pageable) {
        PageHelper.startPage(pageable.getPageNumber() , pageable.getPageSize());
        PageInfo<QueryCustomDTO> pageInfo = new PageInfo<>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", convertFieldData(pageInfo.getList(),criteria));
        map.put("totalElements",pageInfo.getTotal());
        return map;
    }

    @Override
    public List<QueryCustomDTO> queryAll(QueryDataCriteria criteria) {
        return mapper.getQueryAll(criteria, SecurityUtils.getTenantId());
    }


}
