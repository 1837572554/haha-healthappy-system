package com.healthappy.modules.system.service;

import com.healthappy.modules.system.service.dto.SicknessConclusionListDto;
import com.healthappy.modules.system.service.dto.SicknessConclusionListQueryCriteria;
import com.healthappy.modules.system.service.dto.SicknessConclusionNameDto;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author Jevany
 * @date 2022/1/21 14:36
 * @desc 阳性结论名单 服务层
 */
public interface SicknessConclusionService {

    /**
     * 根据条件获取病种名称
     * @author YJ
     * @date 2022/1/21 16:35
     * @param criteria 查询类
     * @return java.util.List〈java.lang.String〉
     */
    Map<String,Object> getSicknessNameList(SicknessConclusionListQueryCriteria criteria);

    /**
     * 导出数据
     * @param all 待导出的数据
     * @param response /
     * @throws IOException /
     */
    void SicknessNameDownload(List<SicknessConclusionNameDto> all, HttpServletResponse response) throws IOException;


    /**
     * 根据条件获取名单数据
     * @author YJ
     * @date 2022/1/21 17:48
     * @param criteria
     * @return java.util.List〈com.healthappy.modules.system.service.dto.SicknessConclusionListDto〉
     */
    Map<String,Object> getSicknessList(SicknessConclusionListQueryCriteria criteria);


    /**
     * 导出病种名单数据
     * @param all 待导出的数据
     * @param response /
     * @throws IOException /
     */
    void SicknessListDownload(List<SicknessConclusionListDto> all, HttpServletResponse response) throws IOException;
}