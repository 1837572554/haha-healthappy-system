package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BTypeTakeblood;

/**
 * @description 行业类别
 * @author fang
 * @date 2021-06-17
 */
public interface BTypeTakebloodService extends BaseService<BTypeTakeblood> {


}
