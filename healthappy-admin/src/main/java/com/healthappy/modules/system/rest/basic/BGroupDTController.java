package com.healthappy.modules.system.rest.basic;


import cn.hutool.core.collection.CollUtil;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.vo.BGroupDTVo;
import com.healthappy.modules.system.service.BGroupDTService;
import com.healthappy.modules.system.service.dto.BGroupDTQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

/**
 * @description 组合项目子项控制器
 * @author sjc
 * @date 2021-08-4
 */
@Validated
@Slf4j
@Api(tags = "基础管理：组合项目子项")
@ApiSort(4)
@RestController
@AllArgsConstructor
@RequestMapping("/BGroupDT")
public class BGroupDTController {

    private final BGroupDTService bGroupDTService;

    @Log("分页查询组合项目子项")
    @ApiOperation("分页查询组合项目子项，权限码：BGroupDT:list")
    @GetMapping
    @PreAuthorize("@el.check('BGroupDT:list','BGroupHd:add')")
    public R list(BGroupDTQueryCriteria criteria,@PageableDefault(sort = {"group_id"}) Pageable pageable) {
        return R.ok(bGroupDTService.queryAll(criteria, pageable));
    }

    @Log("新增|修改组合项目子项")
    @ApiOperation("新增|修改组合项目子项，权限码：BGroupDT:add")
    @PostMapping
    @PreAuthorize("@el.check('BGroupDT:add','BGroupHd:add')")
    @Klock
    public R saveOrUpdate(@Valid @RequestBody List<BGroupDTVo> resources) {
        if(CollUtil.isEmpty(resources)){
            throw new BadRequestException("参数不能为空");
        }
        bGroupDTService.saveOrUpdate(resources);
        return R.ok();
    }

    @Log("根据项目id删除子项")
    @ApiOperation("根据项目id删除子项，权限码：BGroupDT:delete")
    @DeleteMapping("delIds")
    @PreAuthorize("@el.check('BGroupDT:delete')")
    @Klock
    public R removeByIds(@RequestBody Set<Integer> ids) {
        return R.ok(bGroupDTService.removeByIds(ids));
    }

    @Log("根据组合项目id删除子项")
    @ApiOperation("根据组合项目id删除子项，权限码：BGroupDT:delete")
    @DeleteMapping("delGroupId")
    @PreAuthorize("@el.check('BGroupDT:delete')")
    @Klock
    public R removeBygroupId(@RequestParam("groupId") String groupId) {
        return R.ok(bGroupDTService.removeBygroupId(groupId));
    }
}
