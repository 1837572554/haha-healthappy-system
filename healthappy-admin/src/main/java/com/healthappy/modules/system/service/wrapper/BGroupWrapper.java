package com.healthappy.modules.system.service.wrapper;

import cn.hutool.core.bean.BeanUtil;
import com.healthappy.modules.system.domain.BGroupDT;
import com.healthappy.modules.system.domain.BGroupHd;
import com.healthappy.modules.system.service.BGroupDTService;
import com.healthappy.modules.system.service.dto.BGroupHdDto;
import com.healthappy.utils.SpringUtil;

/**
 * 包装类,返回视图层所需的字段
 *
 * @author FGQ
 */
public class BGroupWrapper  extends BaseEntityWrapper<BGroupHd, BGroupHdDto>{

    private final static BGroupDTService groupDtService;

    static {
        groupDtService = SpringUtil.getBean(BGroupDTService.class);
    }

    public static BGroupWrapper build(){
        return new BGroupWrapper();
    }

    @Override
    public BGroupHdDto entityVO(BGroupHd entity) {
        BGroupHdDto groupHdDto = BeanUtil.copyProperties(entity, BGroupHdDto.class);
        groupHdDto.setItemCount(groupDtService.lambdaQuery().eq(BGroupDT::getGroupId, entity.getId()).count());
        return groupHdDto;
    }
}
