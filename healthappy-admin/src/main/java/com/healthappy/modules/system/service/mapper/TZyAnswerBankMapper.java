package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TZyAnswerBank;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Jevany
 * @date 2021/12/8 19:13
 * @desc
 */
@Mapper
@Repository
public interface TZyAnswerBankMapper extends CoreMapper<TZyAnswerBank> {
}
