package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BTypeGetResult;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 结果获取方式
 * @author sjc
 * @date 2021-08-3
 */
@Mapper
@Repository
public interface BTypeGetResultMapper extends CoreMapper<BTypeGetResult> {
}
