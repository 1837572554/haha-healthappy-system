package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class UploadCacheVo {

    @ApiModelProperty("文件名称")
    private String fileName;

    @ApiModelProperty("历史记录")
    private List<String> history;
}
