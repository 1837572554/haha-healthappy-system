package com.healthappy.modules.system.rest.dataquery;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.service.SicknessConclusionService;
import com.healthappy.modules.system.service.dto.SicknessConclusionListDto;
import com.healthappy.modules.system.service.dto.SicknessConclusionListQueryCriteria;
import com.healthappy.modules.system.service.dto.SicknessConclusionNameDto;
import com.healthappy.utils.R;
import com.healthappy.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author Jevany
 * @date 2022/1/21 11:43
 * @desc 阳性（病种）结论名单
 */

@Slf4j
@Api(tags = "数据查询：阳性结论名单")
@RestController
@AllArgsConstructor
@RequestMapping("/sicknessConclusion")
public class SicknessConclusionController {

    private final SicknessConclusionService sicknessConclusionService;

    /**
     * 获得病种
     * @author YJ
     * @date 2022/1/21 14:45
     * @return com.healthappy.utils.R
     */
    @Log("查询病种")
    @ApiOperation("查询病种，权限码：sicknessConclusion:list")
    @PostMapping("/getSickness")
    @PreAuthorize("@el.check('sicknessConclusion:list')")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "病种名单",response = SicknessConclusionNameDto.class)
    })
    public R getSickness(SicknessConclusionListQueryCriteria criteria){
        if(StrUtil.isBlank(criteria.getTenantId())){
            criteria.setTenantId(SecurityUtils.getTenantId());
        }
        if(criteria.getIsExport().equals(false)){
            if (ObjectUtil.isEmpty(criteria.getPageNum()) || criteria.getPageNum() <= 0) {
                criteria.setPageNum(1);
            }
            if (ObjectUtil.isEmpty(criteria.getPageSize()) || criteria.getPageSize() <= 0) {
                criteria.setPageSize(10);
            }
        }
        return R.ok(sicknessConclusionService.getSicknessNameList(criteria));
    }

    @Log("导出病种")
    @ApiOperation("导出病种，权限码：sicknessConclusion:upload")
    @GetMapping(value = "/sicknessDownload")
    @PreAuthorize("@el.check('sicknessConclusion:upload')")
    @Klock
    public void sicknessDownload(HttpServletResponse response,SicknessConclusionListQueryCriteria criteria) throws IOException{
        if(StrUtil.isBlank(criteria.getTenantId())){
            criteria.setTenantId(SecurityUtils.getTenantId());
        }
        Map<String, Object> sicknessNameList = sicknessConclusionService.getSicknessNameList(criteria);
        sicknessConclusionService.SicknessNameDownload((List<SicknessConclusionNameDto>)sicknessNameList.get("content"),response);
    }


    /**
     * 获取名单
     * @author YJ
     * @date 2022/1/21 14:48
     * @param criteria
     * @return com.healthappy.utils.R
     */
    @Log("获取名单")
    @ApiOperation("获取名单，权限码：sicknessConclusion:list")
    @PostMapping("/getList")
    @PreAuthorize("@el.check('sicknessConclusion:list')")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "病种名单列表",response = SicknessConclusionListDto.class)
    })
    public R getList(SicknessConclusionListQueryCriteria criteria){
        if(StrUtil.isBlank(criteria.getTenantId())){
            criteria.setTenantId(SecurityUtils.getTenantId());
        }
        if(criteria.getIsExport().equals(false)){
            if (ObjectUtil.isEmpty(criteria.getPageNum()) || criteria.getPageNum() <= 0) {
                criteria.setPageNum(1);
            }
            if (ObjectUtil.isEmpty(criteria.getPageSize()) || criteria.getPageSize() <= 0) {
                criteria.setPageSize(10);
            }
        }
        return R.ok(sicknessConclusionService.getSicknessList(criteria));
    }

    @Log("导出病种名单")
    @ApiOperation("导出病种名单，权限码：sicknessConclusion:upload")
    @GetMapping(value = "/sicknessListDownload")
    @PreAuthorize("@el.check('sicknessConclusion:upload')")
    @Klock
    public void sicknessListDownload(HttpServletResponse response,SicknessConclusionListQueryCriteria criteria) throws IOException{
        if(StrUtil.isBlank(criteria.getTenantId())){
            criteria.setTenantId(SecurityUtils.getTenantId());
        }
        Map<String, Object> sicknessList = sicknessConclusionService.getSicknessList(criteria);
        sicknessConclusionService.SicknessListDownload((List<SicknessConclusionListDto>)sicknessList.get("content"),response);
    }


}
