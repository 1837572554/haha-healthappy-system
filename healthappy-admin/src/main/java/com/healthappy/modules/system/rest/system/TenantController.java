package com.healthappy.modules.system.rest.system;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.healthappy.common.entity.TenantEntity;
import com.healthappy.config.TenantConstant;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.redis.service.RedisUtilService;
import com.healthappy.modules.system.domain.Tenant;
import com.healthappy.modules.system.domain.vo.TenantVo;
import com.healthappy.modules.system.service.TenantService;
import com.healthappy.modules.system.service.dto.TenantQueryCriteria;
import com.healthappy.utils.CacheConstant;
import com.healthappy.utils.R;
import com.healthappy.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Api(tags = "系统：租户管理")
@RestController
@RequestMapping("/api/tenant")
@AllArgsConstructor
public class TenantController {

	private final TenantService tenantService;

	private static final String ENTITY_NAME = "tenant";

	private final RedisUtilService redisUtilService;

	@Log("查询租户")
	@ApiOperation("查询租户，权限码：admin,tenant:list")
	@GetMapping
	@PreAuthorize("@el.check('admin','tenant:list','user:add')")
	public R getTenants(TenantQueryCriteria criteria, Pageable pageable) {
		return R.ok(tenantService.queryAll(criteria, pageable));
	}

	@Log("查询当前租户信息")
	@ApiOperation("查询当前租户信息，权限码：admin,tenant:list")
	@GetMapping("/current")
	@PreAuthorize("@el.check('admin','tenant:list')")
	public R getTenant() {
		return R.ok(tenantService.getOne(
				Wrappers.<Tenant>lambdaQuery().eq(TenantEntity::getTenantId, SecurityUtils.getTenantId())));
	}

	@Log("新增租户")
	@ApiOperation("新增租户，权限码：admin")
	@PostMapping
	@PreAuthorize("@el.check('admin','BPetypeInstitution:add')")
	@Klock
	@CacheEvict(cacheNames = {CacheConstant.TENANT}, allEntries = true)
	public R create(@Validated @RequestBody Tenant resources) {
		if (resources.getId() != null) {
			throw new BadRequestException("A new " + ENTITY_NAME + " cannot already have an ID");
		}
		checkField(resources);
		buildTenant(resources);
		return R.ok(tenantService.save(resources));
	}

	@Log("修改租户")
	@ApiOperation("修改租户，权限码：admin")
	@PutMapping
	@PreAuthorize("@el.check('admin','BPetypeInstitution:edit')")
	@Klock
	@CacheEvict(cacheNames = {CacheConstant.TENANT}, allEntries = true)
	public R update(@Validated @RequestBody TenantVo resources) {
		Tenant tenant = BeanUtil.copyProperties(resources, Tenant.class);
		checkField(tenant);
		//如果将租户修改为冻结状态
		if (!resources.getIsEnable()) {
			kickOutTenant(resources.getId());
		}
		//清理预约号源开关状态
		String tenantId = SecurityUtils.getTenantId();


		redisUtilService.evic("appointSwitch" + StrUtil.COLON + tenantId, tenantId);
		boolean success = tenantService.saveOrUpdate(tenant);
		//清理预约号源开关状态 简单双删
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		redisUtilService.evic("appointSwitch" + StrUtil.COLON + tenantId, tenantId);
		return R.ok(success);
	}

	@Log("删除租户")
	@ApiOperation("删除租户，权限码：admin")
	@DeleteMapping
	@PreAuthorize("@el.check('admin','BPetypeInstitution:del')")
	@Klock
	@CacheEvict(cacheNames = {CacheConstant.TENANT}, allEntries = true)
	public void delete(@RequestBody Set<Long> ids) throws Exception {
		tenantService.removeByTenantIds(ids);
	}

	;

	private void buildTenant(Tenant resources) {
		String tenantId = tenantService.generateTenantId();
		resources.setTenantId(tenantId);
		resources.setCode(DateUtil.current(false) + "");
	}

	/**判断字段*/
	private void checkField(Tenant resources) {
		Tenant tenant = tenantService.getOne(Wrappers.<Tenant>lambdaQuery().eq(Tenant::getName, resources.getName()));
		if ((null != tenant && ObjectUtil.isNotNull(tenant.getId())) && ((ObjectUtil.isNull(resources.getId())) || (
				ObjectUtil.isNotNull(resources.getId()) && !resources.getId().equals(tenant.getId())))) {
			throw new BadRequestException("加盟商店铺名称重复");
		}
	}

	/**剔除加盟商下所有在线的租户*/
	private void kickOutTenant(Long id) {
		Tenant tenant = tenantService.getById(id);
		if (null == tenant || ObjectUtil.isNull(tenant.getId())) {
			throw new BadRequestException("加盟商为空");
		}
		if (tenant.getTenantId().equals(TenantConstant.DEFAULT_TENANT_ID)) {
			throw new BadRequestException("超级管理员不能冻结");
		}
		tenantService.kickOutTenant(tenant);
	}
}
