package com.healthappy.modules.system.rest.projectcheck;

import cn.hutool.core.util.ObjectUtil;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.vo.TDctDataVo;
import com.healthappy.modules.system.service.TDctDataService;
import com.healthappy.utils.R;
import com.healthappy.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * @desc: 电测听控制器
 * @author: YJ
 * @date: 2021-12-02 16:07
 **/
@Slf4j
@Api(tags = "体检流程：电测听")
@RestController
@AllArgsConstructor
@RequestMapping("/dct")
public class DctController {

    /** 电测听数据服务 */
    private final TDctDataService dctService;

    @Log("查询体检人员电测听数据")
    @ApiOperation("查询体检人员电测听数据，权限码：ProjectCheck:list")
    @GetMapping("/getDctData")
    @PreAuthorize("@el.check('ProjectCheck:list')")
    public R getDctData(String paId) {
        if(StringUtils.isBlank(paId)){
            new BadRequestException("体检号不能为空");
        }
        return R.ok(dctService.getDctData(paId));
    }

    @Log("保存体检人员电测听数据")
    @ApiOperation("保存体检人员电测听数据，权限码：ProjectCheck:update")
    @PostMapping("/saveDctData")
    @PreAuthorize("@el.check('ProjectCheck:list')")
    @Klock
    public R saveDctData(@RequestBody @Validated TDctDataVo vo) {
        if(!Optional.ofNullable(vo).isPresent()  || ObjectUtil.isNull(vo)){
            new BadRequestException("对象不能为空");
        }
        if(StringUtils.isBlank(vo.getPaId())){
            new BadRequestException("体检号不能为空");
        }
        if(!Optional.ofNullable(vo.getData()).isPresent()  || ObjectUtil.isNull(vo.getData()) || vo.getData().size()==0){
            new BadRequestException("正常数据不能为空");
        }
        if(!Optional.ofNullable(vo.getDataXz()).isPresent()  || ObjectUtil.isNull(vo.getDataXz()) || vo.getDataXz().size()==0){
            new BadRequestException("修正数据不能为空");
        }
        dctService.saveDctData(vo);
        return R.ok();
    }
}
