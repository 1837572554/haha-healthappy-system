package com.healthappy.modules.system.service.dto;

import com.healthappy.modules.system.domain.BCompanyAttribute;
import lombok.Data;


/**
 * @description 单位属性
 * @author sjc
 * @date 2021-08-30
 */
@Data
public class BCompanyAttributeDto extends BCompanyAttribute {


}
