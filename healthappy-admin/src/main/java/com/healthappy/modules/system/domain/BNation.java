package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 民族表
 * @author FGQ
 */
@Data
@TableName("B_Nation")
public class BNation implements Serializable {

    @ApiModelProperty("主键")
    private Long id;

    @ApiModelProperty("code")
    private Long code;

    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("拼音")
    private String ziMu;
}
