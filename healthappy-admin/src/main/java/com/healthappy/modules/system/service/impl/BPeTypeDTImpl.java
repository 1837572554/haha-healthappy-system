package com.healthappy.modules.system.service.impl;

import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.BPeTypeDT;
import com.healthappy.modules.system.service.BPeTypeDTService;
import com.healthappy.modules.system.service.mapper.BPeTypeDTMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @description 体检类型实现s
 * @author yanjun
 * @date 2021年9月3日 10:44:41
 */
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true, rollbackFor = Exception.class)
public class BPeTypeDTImpl extends BaseServiceImpl<BPeTypeDTMapper, BPeTypeDT> implements BPeTypeDTService {

}
