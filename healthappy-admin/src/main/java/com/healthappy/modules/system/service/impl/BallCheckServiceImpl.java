package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.IdcardUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.healthappy.config.ApplicationConstant;
import com.healthappy.config.databind.DataBind;
import com.healthappy.enums.PeTypeGroupEnum;
import com.healthappy.modules.system.domain.*;
import com.healthappy.modules.system.domain.vo.BallCheckUploadVo;
import com.healthappy.modules.system.service.*;
import com.healthappy.modules.system.service.dto.TPatientBaseinfoSaveDtoDto;
import com.healthappy.utils.AgeCalcUtil;
import com.healthappy.utils.SecurityUtils;
import lombok.AllArgsConstructor;
import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
public class BallCheckServiceImpl implements BallCheckService {

	private final DataBind dataBind;
	private final BDepartmentGroupHDService departmentGroupHDService;
	private final BDepartmentGroupTDService departmentGroupTDService;
	private final BGroupHdService bGroupHdService;
	private final TItemHDService itemHDService;
	private final TItemDTService itemDTService;
	private final TAppointGroupService tAppointGroupService;
	private final BGroupDTService bGroupDTService;
	private final BPackageDTService bPackageDTService;
	private final BPackageHDService bPackageHDService;
	private final TPatientZService patientZService;
	private final WorkStationService workStationService;
	private final TPatientService patientService;
	private final TAppointService appointService;
	private final BItemService itemService;
	private final TPatientBaseinfoService patientBaseinfoService;
	private final BPoisonService poisonService;
	private final BPeTypeHDService bPeTypeHDService;


	private List<BPackageDT> packageDTList;
	private List<BGroupHd> bGroupHdList;
	private List<BDepartmentGroupTD> bDepartmentGroupTDList;
	private List<BDepartmentGroupHD> bDepartmentGroupHDList;

	private final TenantService tenantService;

	@Override
	public void upload(Integer registrationType, String appointDateStart, String appointDateEnd,
			List<BallCheckUploadVo> manual) {
		//性别Map
		BidiMap sexPrimaryKeyMap = getSexPrimaryKey(
				manual.stream().filter(s -> StrUtil.isNotBlank(s.getSex())).map(BallCheckUploadVo::getSex).distinct()
						.collect(Collectors.toList()));

		//婚姻
		BidiMap theMarriageKeyMap = getTheMarriageKey(
				manual.stream().filter(s -> StrUtil.isNotBlank(s.getMarry())).map(BallCheckUploadVo::getMarry)
						.distinct().collect(Collectors.toList()));

		String nickName = SecurityUtils.getNickName();

		packageDTList = bPackageDTService.list();
		bGroupHdList = bGroupHdService.list();
		bDepartmentGroupTDList = departmentGroupTDService.list();
		bDepartmentGroupHDList = departmentGroupHDService.list();

		//走预约
		if (registrationType == 1) {
			try {
				List<TAppoint> appointList = manual.stream().map(v -> {
					TAppoint appoint = new TAppoint();
					appoint.setName(v.getName());
					appoint.setSex(StrUtil.isNotBlank(v.getSex()) && MapUtil.isNotEmpty(sexPrimaryKeyMap)
							&& sexPrimaryKeyMap.containsKey(v.getSex()) ?
							sexPrimaryKeyMap.get(v.getSex()).toString() :
							"");
					appoint.setAge(Convert.toInt(v.getAge()));
					appoint.setMarital(StrUtil.isNotBlank(v.getMarry()) && MapUtil.isNotEmpty(theMarriageKeyMap)
							&& theMarriageKeyMap.containsKey(v.getMarry()) ?
							theMarriageKeyMap.get(v.getMarry()).toString() :
							"");
					appoint.setMobile(v.getMobile());
					appoint.setIdNo(v.getIdNo());

					String appointSwitch = tenantService.getAppointSwitch(SecurityUtils.getTenantId());
					if (!ApplicationConstant.APPOINT_SWITCH.equals(appointSwitch)) {
						if (StrUtil.isNotBlank(appointDateStart) && StrUtil.isNotBlank(appointDateEnd)) {
							appoint.setAppointDateStart(
									DateUtil.parseDateTime(appointDateStart + " 00:00:00").toTimestamp());
							appoint.setAppointDateEnd(
									DateUtil.parseDateTime(appointDateEnd + " 23:59:59").toTimestamp());
						}
						appoint.setAppointDate(new DateTime().toTimestamp());
					}
					appoint.setAppointType("1");
					appoint.setCompanyId(v.getCompanyId());
					appoint.setCompanyName(v.getCompanyName());

					appoint.setDepartmentId(v.getDepartmentId());
					appoint.setDepartmentName(v.getDepartmentName());
					appoint.setRegisterDoctor(nickName);

					appoint.setDeptGroupId(v.getDeptGroupId());
					appoint.setDeptGroupName(v.getDeptGroupName());

					appoint.setPeType(ObjectUtil.isNotNull(v.getPeTypeId()) ? v.getPeTypeId().toString() : "");
					appoint.setType(ObjectUtil.isNotNull(v.getTypeId()) ? v.getTypeId().toString() : "");

					appoint.setPoisonType(v.getPoisonTypeName());

					appoint.setCode(v.getJobNumber());
					appoint.setJob(ObjectUtil.isNotNull(v.getJobId()) ? v.getJobId().toString() : "");
					appoint.setJobName(v.getJobName());
					appoint.setWorkYears(v.getPickAge());
					appoint.setTotalYears(v.getAllAge());

					appoint.setPackageId(v.getPackageId());
					appoint.setPackageName(v.getPackageName());

					appoint.setAddPackageId(v.getAdditionPackageId());
					appoint.setAddPackage(v.getAdditionPackageName());

					appoint.setAddGroupItemId(v.getAddGroupIds());
					appoint.setAddGroupItem(v.getAddGroupName());
					appoint.setSubtractGroupItem(v.getSubtractGroupItem());
					appoint.setSsNo(v.getSocialSecurityNumber());
					appoint.setMark(v.getRemark());
					return appoint;
				}).collect(Collectors.toList());

				List<String> appointIds = new ArrayList<>();
				appointList.forEach(v -> {
					appointService.save(v);
					appointIds.add(v.getId());
					//提取到了预约组合项服务层了
					tAppointGroupService.saveAppointGroup(v, bDepartmentGroupHDList);
				});
				//                repeatAppointProject(appointIds);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			try {
				List<BItem> itemList = itemService.list();
				List<BGroupDT> groupDTList = bGroupDTService.list();

				List<TPatient> patientList = manual.stream().map(v -> {
					List<TItemDT> itemDtList = new ArrayList<>();
					List<TItemHD> itemHdAllList = new ArrayList<>();
					TPatient patient = new TPatient();
					String paId = workStationService.generatePrimaryKey(1);
					patient.setId(paId);
					patient.setWorkstation(workStationService.getWorkStation());
					patient.setName(v.getName());
					patient.setPeDate(new DateTime().toTimestamp());
					patient.setSex(StrUtil.isNotBlank(v.getSex()) && MapUtil.isNotEmpty(sexPrimaryKeyMap)
							&& sexPrimaryKeyMap.containsKey(v.getSex()) ?
							sexPrimaryKeyMap.get(v.getSex()).toString() :
							null);

					patient.setMarital(StrUtil.isNotBlank(v.getMarry()) && MapUtil.isNotEmpty(theMarriageKeyMap)
							&& theMarriageKeyMap.containsKey(v.getMarry()) ?
							theMarriageKeyMap.get(v.getMarry()).toString() :
							null);
					patient.setPhone(v.getMobile());
					patient.setIdNo(v.getIdNo());
					String birth = IdcardUtil.getBirth(v.getIdNo());
					patient.setBirthday(
							StrUtil.isNotBlank(v.getIdNo()) ? DateTime.of((birth), "yyyyMMdd").toTimestamp() : null);
					//                    patient.setAgeDate(DateUtil.ageOfNow(birth) + "年" + DateTime.of(birth, "yyyyMMdd").toString("M月"));

					Integer peTypeGroupId = bPeTypeHDService.getPeTypeGroupId(v.getPeTypeId());
					AgeCalcUtil.AgeData ageData = AgeCalcUtil.calc(patient.getBirthday());
					patient.setAge(ageData.getYear());
					if (PeTypeGroupEnum.CHILD.getValue().equals(peTypeGroupId)) {
						//儿童体检设置此数据
						patient.setAgeDate(
								(ageData.getYear() != 0 ? ageData.getYear() + "岁" : "") + (ageData.getMonth() != 0 ?
										ageData.getMonth() + "月" :
										""));
					}

					patient.setCompanyId(v.getCompanyId());
					patient.setDepartmentId(v.getDepartmentId());
					patient.setDepartmentGroupId(v.getDeptGroupId());
					patient.setPeTypeHdId(v.getPeTypeId());
					patient.setChargeType(1);
					patient.setPeTypeDtId(v.getTypeId());


					if (peTypeGroupId.equals(PeTypeGroupEnum.PROFESSION.getValue()) || peTypeGroupId.equals(
							PeTypeGroupEnum.ZPHY.getValue())) {
						TPatientZ.TPatientZBuilder patientZBuilder = TPatientZ.builder();
						patientZBuilder.poisonType(v.getPoisonTypeName());
						patientZBuilder.workYears(v.getPickAge()).paId(paId);
						patientZService.save(patientZBuilder.build());
					}

					patient.setRegisterDoctor(nickName);
					patient.setCode(v.getJobNumber());
					patient.setCreateTime(new DateTime().toTimestamp());
					patient.setJob(ObjectUtil.isNull(v.getJobId()) ? "" : v.getJobId().toString());
					patient.setTotalYears(v.getAllAge());

					patient.setPackageId(v.getPackageId());
					patient.setPackageName(v.getPackageName());
					patient.setPackagePrice(
							ObjectUtil.isNull(v.getPackagePrice()) ? null : v.getPackagePrice().doubleValue());
					patient.setPackageAdditionPrice(ObjectUtil.isNull(v.getAdditionPackagePrice()) ?
							null :
							v.getAdditionPackagePrice().doubleValue());
					patient.setTotalPrice(
							ObjectUtil.isNull(v.getPackagePrice()) || ObjectUtil.isNull(v.getAdditionPackagePrice()) ?
									null :
									NumberUtil.add(v.getPackagePrice(), v.getAdditionPackagePrice()).doubleValue());

					List<String> groupIds = new ArrayList<>();

					if (StrUtil.isNotBlank(v.getPackageId())) {
						List<BPackageDT> packageDtList = getByIdPackageDtList(v.getPackageId());
						groupIds = packageDtList.stream().map(BPackageDT::getGroupId).collect(Collectors.toList());
						saveGroupHd(packageDtList, itemList, v.getPackageId(), v.getPackageName(), v.getDepartmentId(),
								paId, v.getPeTypeName(), v.getDeptGroupId(), itemDtList, itemHdAllList,
								patient.getSex());
					}

					if (CollUtil.isEmpty(groupIds) && StrUtil.isNotBlank(v.getDeptGroupId())) {
						BDepartmentGroupHD departmentGroupHD = getByDepartmentGroupHdOne(v.getDeptGroupId());
						List<TItemHD> itemHDList = getByDepartmentGroupHdIdOne(v.getDeptGroupId()).stream().map(td -> {
							BGroupHd groupHd = getByIdGroupHdOne(td.getGroupId());
							BGroupHd ihd = new BGroupHd();
							ihd.setId(td.getGroupId());
							ihd.setDeptId(groupHd.getDeptId());
							ihd.setName(groupHd.getName());
							ihd.setPrice(Convert.toBigDecimal(td.getPrice()));
							ihd.setCost(Convert.toBigDecimal(td.getCost()));
							return buildItemHd(paId, departmentGroupHD, ihd, null, v);
						}).collect(Collectors.toList());

						groupIds = itemHDList.stream().map(TItemHD::getGroupId).collect(Collectors.toList());
						itemHdAllList.addAll(itemHDList);

						itemDtList.addAll(groupDTList.stream()
								.filter(k -> ObjectUtil.isNotNull(getByIdItemOne(itemList, k.getItemId())))
								.map(k -> buildItemDt(paId, getByIdItemOne(itemList, k.getItemId()), k,
										patient.getSex())).collect(Collectors.toList()));
					}
					if (CollUtil.isEmpty(groupIds) && StrUtil.isNotBlank(v.getPoisonTypeName())) {
						List<BPackageDT> packageDtList = poisonService.getPackageList(
								Stream.of(v.getPoisonTypeName().split("、")).collect(Collectors.toList()),
								v.getTypeId().toString());
						groupIds = packageDtList.stream().map(BPackageDT::getGroupId).collect(Collectors.toList());
						saveGroupHd(packageDtList, itemList, packageDtList.get(0).getPackageId(),
								bPackageHDService.getById(packageDtList.get(0).getPackageId()).getName(), null, paId,
								v.getPeTypeName(), v.getDeptGroupId(), itemDtList, itemHdAllList, patient.getSex());
					}

					if (StrUtil.isNotBlank(v.getAdditionPackageId()) && CollUtil.isNotEmpty(groupIds)) {
						saveGroupHd(getByIdPackageDtList(v.getAdditionPackageId()), itemList, v.getAdditionPackageId(),
								v.getAdditionPackageName(), v.getDepartmentId(), paId, v.getPeTypeName(),
								v.getDeptGroupId(), itemDtList, itemHdAllList, patient.getSex());
					}
					final BigDecimal[] addGroupPrice = {new BigDecimal(0)};
					if (StrUtil.isNotBlank(v.getAddGroupIds())) {
						itemHdAllList.addAll(bGroupHdService.lambdaQuery().in(BGroupHd::getId,
										Stream.of(v.getAddGroupIds().split("、")).collect(Collectors.toList())).list().stream()
								.map(hd -> {
									addGroupPrice[0] = ObjectUtil.isNotNull(hd.getPrice()) ?
											addGroupPrice[0].add(hd.getPrice()) :
											addGroupPrice[0];
									return buildItemHd(paId, new BDepartmentGroupHD(), hd, null, v, true);
								}).collect(Collectors.toList()));


						itemDtList.addAll(groupDTList.stream()
								.filter(e -> Stream.of(v.getAddGroupIds().split("、")).collect(Collectors.toList())
										.contains(e.getGroupId()))
								.filter(k -> ObjectUtil.isNotNull(getByIdItemOne(itemList, k.getItemId())))
								.map(k -> buildItemDt(paId, getByIdItemOne(itemList, k.getItemId()), k,
										patient.getSex())).collect(Collectors.toList()));
					}
					if (CollUtil.isNotEmpty(itemHdAllList)) {
						if (StrUtil.isNotBlank(v.getSubtractGroupItem())) {
							List<BGroupHd> groupHdList = bGroupHdService.lambdaQuery().in(BGroupHd::getId,
									Stream.of(v.getSubtractGroupItem().split("、")).collect(Collectors.toList())).list();
							if (CollUtil.isNotEmpty(groupHdList)) {
								itemHdAllList = itemHdAllList.stream()
										.filter(k -> !groupHdList.stream().map(BGroupHd::getId)
												.collect(Collectors.toList()).contains(k.getGroupId()))
										.collect(Collectors.toList());
							}
						}
						itemHdAllList.stream().distinct().forEach(itemHDService::save);
						List<String> groupIdList = itemHdAllList.stream().distinct().map(TItemHD::getGroupId)
								.collect(Collectors.toList());
						if (CollUtil.isNotEmpty(groupIdList)) {
							itemDtList = itemDtList.stream().filter(dt -> groupIdList.contains(dt.getGroupId()))
									.collect(Collectors.toList());
						} else {
							itemDtList.clear();
						}
					} else {
						itemDtList.clear();
					}

					if (CollUtil.isNotEmpty(itemDtList)) {
						if (StrUtil.isNotBlank(v.getSubtractGroupItem())) {
							List<BGroupHd> groupHdList = bGroupHdService.lambdaQuery().in(BGroupHd::getId,
									Stream.of(v.getSubtractGroupItem().split("、")).collect(Collectors.toList())).list();
							if (CollUtil.isNotEmpty(groupHdList)) {
								itemDtList = itemDtList.stream().filter(k -> !groupHdList.stream().map(BGroupHd::getId)
												.collect(Collectors.toList()).contains(k.getGroupId()))
										.collect(Collectors.toList());
							}
						}
						itemDtList.stream().distinct().forEach(itemDTService::save);
					}

					patient.setAdditionPrice(addGroupPrice[0].doubleValue());
					patient.setSsNo(v.getSocialSecurityNumber());
					patient.setMark(v.getRemark());

					//region 档案号操作
					if (ObjectUtil.isNull(patient.getRecordId())) {
						patient.setRecordId(patientBaseinfoService.getPNo(
								TPatientBaseinfoSaveDtoDto.builder().idNo(patient.getIdNo()).name(patient.getName())
										.peTypeHdId(patient.getPeTypeHdId()).build()));
					}
					//endregion 档案号操作
					//
					itemHDService.repeatGroupId(Collections.singletonList(patient.getId()));
					itemDTService.repeatItemId(Collections.singletonList(patient.getId()));

					patientService.generateBarcode(patient.getId());

					return patient;
				}).collect(Collectors.toList());
				//repeatPatientProject(patientList);
				patientService.saveBatch(patientList);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}


	@Override
	public TItemDT convertRef(int sex, BItem item) {
		String refHigh = StrUtil.blankToDefault(1 == sex ? item.getRefHigh() : item.getRefHighF(), null);
		String refLow = StrUtil.blankToDefault(1 == sex ? item.getRefLow() : item.getRefLowF(), null);
		String refValue = StrUtil.hasBlank(refHigh, refLow) ? "" : refLow + "-" + refHigh;
		TItemDT itemDT = new TItemDT();
		itemDT.setRefLow(refLow);
		itemDT.setRefValue(refValue);
		itemDT.setRefHigh(refHigh);
		return itemDT;
	}

	//    @Override
	//    public TAppointGroup calculateEachPrice(BigDecimal price, BigDecimal discount, BigDecimal cost) {
	//        if (ObjectUtil.isNull(price)) {
	//            price = new BigDecimal(0);
	//        }
	//        if (ObjectUtil.isNull(discount)) {
	//            discount = ObjectUtil.isNotNull(price) && !NumberUtil.equals(price, new BigDecimal(0)) && ObjectUtil.isNull(discount) ? NumberUtil.div(cost, price) : new BigDecimal(1);
	//        }
	//        if (ObjectUtil.isNull(cost)) {
	//            cost = NumberUtil.round(NumberUtil.mul(price, discount),2);
	//        }
	//        TAppointGroup group = new TAppointGroup();
	//        group.setPrice(price);
	//        group.setDiscount(discount);
	//        group.setCost(cost);
	//        return group;
	//    }


	//    private TAppointGroup buildTAppointGroup(String id, String groupId, BigDecimal price, Integer payType, BigDecimal discount, BigDecimal cost) {
	//        TAppointGroup group = this.calculateEachPrice(price, discount, cost);
	//        TAppointGroup TAppointGroup = new TAppointGroup();
	//        TAppointGroup.setAppointId(id);
	//        TAppointGroup.setGroupId(groupId);
	//        TAppointGroup.setPrice(group.getPrice());
	//        TAppointGroup.setPayType(payType);
	//        TAppointGroup.setDiscount(group.getDiscount());
	//        TAppointGroup.setCost(group.getCost());
	//        return TAppointGroup;
	//    }

	private void repeatAppointProject(List<String> appointIds) {
		appointIds.forEach(k -> tAppointGroupService.repeatGroupId(Collections.singletonList(k)));
	}

	private void repeatPatientProject(List<TPatient> patientList) {
		patientList.forEach(k -> {
			itemHDService.repeatGroupId(Collections.singletonList(k.getId()));
			itemDTService.repeatItemId(Collections.singletonList(k.getId()));
		});
	}

	private TItemDT buildItemDt(String paId, BItem item, BGroupDT group, String sex) {
		TItemDT itemDT = new TItemDT();
		itemDT.setPaId(paId);
		itemDT.setItemId(item.getId());
		itemDT.setItemName(item.getName());
		itemDT.setGroupId(group.getGroupId());
		itemDT.setInstrCode(item.getInstrCode());
		itemDT.setUpdateTime(new DateTime().toTimestamp());
		itemDT.setUnit(item.getUnit());
		TItemDT ref = this.convertRef(Convert.toInt(sex), item);
		itemDT.setRefHigh(ref.getRefHigh());
		itemDT.setRefLow(ref.getRefLow());
		itemDT.setRefValue(ref.getRefValue());
		return itemDT;
	}


	private TItemHD buildItemHd(String paId, BDepartmentGroupHD departmentGroupHD, BGroupHd groupHd,
			BPackageDT packageDT, BallCheckUploadVo v, Boolean isAddGroupFlag) {
		return buildItem(paId, departmentGroupHD, groupHd, packageDT, v, isAddGroupFlag);
	}

	private TItemHD buildItemHd(String paId, BDepartmentGroupHD departmentGroupHD, BGroupHd groupHd,
			BPackageDT packageDT, BallCheckUploadVo v) {
		return buildItem(paId, departmentGroupHD, groupHd, packageDT, v, false);
	}

	private TItemHD buildItem(String paId, BDepartmentGroupHD departmentGroupHD, BGroupHd groupHd, BPackageDT packageDT,
			BallCheckUploadVo v, boolean isAddGroupFlag) {
		TItemHD tItemHD = new TItemHD();
		tItemHD.setPaId(paId);
		tItemHD.setPackageId(departmentGroupHD.getPackageId());
		tItemHD.setPackageName(departmentGroupHD.getPackageName());

		String groupId, deptId, groupName;
		BigDecimal price, discount, cost;
		if (null != packageDT) {
			groupId = packageDT.getGroupId();
			BGroupHd bGroupHd = getByIdGroupHdOne(groupId);
			deptId = bGroupHd.getDeptId();
			groupName = bGroupHd.getName();

			TAppointGroup group = tAppointGroupService.calculateEachPrice(packageDT.getPrice(), packageDT.getDiscount(),
					packageDT.getCost());
			price = group.getPrice();
			discount = group.getDiscount();
			cost = group.getCost();
		} else {
			groupId = groupHd.getId();
			deptId = groupHd.getDeptId();
			groupName = groupHd.getName();

			TAppointGroup group = tAppointGroupService.calculateEachPrice(groupHd.getPrice(), null, groupHd.getCost());
			price = group.getPrice();
			cost = group.getCost();
			discount = group.getDiscount();
		}

		tItemHD.setGroupId(groupId);
		tItemHD.setUpdateTime(new DateTime().toTimestamp());
		tItemHD.setRegisterDoc(SecurityUtils.getNickName());
		tItemHD.setDeptId(deptId);
		tItemHD.setGroupName(groupName);

		//如果是附加组合项目 或者 非 职业项目
		if (isAddGroupFlag || !StrUtil.containsAny(v.getPeTypeName(), "职业体检", "职业体检复查", "职普合一", "放射体检", "放射体检复查")) {
			tItemHD.setTypeJ("1");
			tItemHD.setTypeZ("0");
		} else {
			if (null != packageDT) {
				tItemHD.setTypeJ(packageDT.getTypeJ());
				tItemHD.setTypeZ(packageDT.getTypeZ());
			} else {
				BDepartmentGroupTD byGroupIdOne = getByGroupIdOne(groupId, departmentGroupHD.getId());
				if (null != byGroupIdOne) {
					tItemHD.setTypeJ(byGroupIdOne.getTypeJ());
					tItemHD.setTypeZ(byGroupIdOne.getTypeZ());
				}
			}
		}
		tItemHD.setPrice(price);
		tItemHD.setDiscount(discount);
		tItemHD.setCost(cost);
		tItemHD.setAddition("0");
		if (StrUtil.isNotBlank(v.getDeptGroupId())) {
			tItemHD.setPayType(getByDepartmentGroupHdOne(v.getDeptGroupId()).getSffs());
		}
		tItemHD.setRegisterState("1");
		tItemHD.setAddTime(DateTime.now());
		tItemHD.setChoose("0");
		return tItemHD;
	}

	private void saveGroupHd(List<BPackageDT> list, List<BItem> itemList, String packageId, String packageName,
			String departmentId, String paId, String peTypeName, String deptGroupId, List<TItemDT> itemDTList,
			List<TItemHD> itemHDAllList, String sex) {
		List<TItemHD> itemHDList = list.stream().map(dt -> {
			BDepartmentGroupHD departmentGroupHD = new BDepartmentGroupHD();
			departmentGroupHD.setPackageId(packageId);
			departmentGroupHD.setPackageName(packageName);
			BallCheckUploadVo checkUploadVo = new BallCheckUploadVo();
			checkUploadVo.setPeTypeName(peTypeName);
			checkUploadVo.setDepartmentId(departmentId);
			checkUploadVo.setDeptGroupId(deptGroupId);
			return buildItemHd(paId, departmentGroupHD, null, dt, checkUploadVo);
		}).collect(Collectors.toList());

		itemHDAllList.addAll(itemHDList);

		itemDTList.addAll(bGroupDTService.lambdaQuery()
				.in(BGroupDT::getGroupId, list.stream().map(BPackageDT::getGroupId).collect(Collectors.toList())).list()
				.stream().filter(k -> ObjectUtil.isNotNull(getByIdItemOne(itemList, k.getItemId())))
				.map(k -> buildItemDt(paId, getByIdItemOne(itemList, k.getItemId()), k, sex))
				.collect(Collectors.toList()));


	}


	/**
	 * 通过名称获取id
	 *
	 * @param sexNameList
	 * @return
	 */
	private BidiMap getSexPrimaryKey(List<String> sexNameList) {
		if (CollUtil.isEmpty(sexNameList)) {
			return null;
		}
		return new DualHashBidiMap(dataBind.ENUM_MAP.get("sex")).inverseBidiMap();
	}

	/**
	 * 婚姻
	 *
	 * @param marriageNameList
	 * @return
	 */
	private BidiMap getTheMarriageKey(List<String> marriageNameList) {
		if (CollUtil.isEmpty(marriageNameList)) {
			return null;
		}
		return new DualHashBidiMap(dataBind.ENUM_MAP.get("marital")).inverseBidiMap();
	}

	private BDepartmentGroupHD getByDepartmentGroupHdOne(String deptGroupId) {
		return bDepartmentGroupHDList.stream().filter(e -> e.getId().equals(deptGroupId)).findFirst().orElse(null);
	}

	private BItem getByIdItemOne(List<BItem> itemList, String itemId) {
		return itemList.stream().filter(e -> e.getId().equals(itemId)).findFirst().orElse(null);
	}

	private BGroupHd getByIdGroupHdOne(String groupId) {
		return bGroupHdList.stream().filter(e -> e.getId().equals(groupId)).findFirst().orElse(null);
	}

	private BDepartmentGroupTD getByGroupIdOne(String groupId, String departmentGroupHdId) {
		return bDepartmentGroupTDList.stream()
				.filter(e -> e.getGroupId().equals(groupId) && e.getDepartmentGroupHdId().equals(departmentGroupHdId))
				.findFirst().orElse(null);
	}

	private List<BDepartmentGroupTD> getByDepartmentGroupHdIdOne(String departmentGroupHdId) {
		return bDepartmentGroupTDList.stream().filter(e -> e.getDepartmentGroupHdId().equals(departmentGroupHdId))
				.collect(Collectors.toList());
	}

	private List<BGroupHd> getByIdGroupHdList(String groupIds) {
		return bGroupHdList.stream()
				.filter(e -> Stream.of(groupIds.split("、")).collect(Collectors.toList()).contains(e.getId()))
				.collect(Collectors.toList());
	}

	private List<BPackageDT> getByIdPackageDtList(String packageId) {
		return packageDTList.stream().filter(e -> e.getPackageId().equals(packageId)).collect(Collectors.toList());
	}
}
