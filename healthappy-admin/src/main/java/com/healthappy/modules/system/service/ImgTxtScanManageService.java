package com.healthappy.modules.system.service;


import com.healthappy.modules.system.domain.TImgTxtScanDTO;
import com.healthappy.modules.system.service.dto.ImgTxtScanDelDTO;
import com.healthappy.modules.system.service.dto.ImgTxtScanManageQueryCriteria;
import com.healthappy.modules.system.service.dto.ImgTxtScanQueryDTO;
import com.healthappy.modules.system.service.dto.ImgTxtScanSaveDTO;

import java.util.Map;

/**
 * 图文扫描录入管理 服务
 * @author Jevany
 * @date 2022/2/15 17:13
 */
public interface ImgTxtScanManageService {

    /**
     * 获取列表-图文扫描录入
     * @author YJ
     * @date 2022/2/15 17:14
     * @param criteria
     * @return java.util.Map〈java.lang.String,java.lang.Object〉
     */
    Map<String, Object> getList(ImgTxtScanManageQueryCriteria criteria);

    /**
     * 获取图文扫描录入-详细
     * @author YJ
     * @date 2022/2/16 14:21
     * @param imgTxtScanQueryDTO
     * @return com.healthappy.modules.system.domain.TImgTxtScan
     */
    TImgTxtScanDTO getImgTxtScan(ImgTxtScanQueryDTO imgTxtScanQueryDTO);


    /**
     * 保存图片扫描录入数据
     * @author YJ
     * @date 2022/2/16 11:58
     * @param imgTxtScanSaveDTO
     * @return java.lang.Integer
     */
    void saveImgTxtScan(ImgTxtScanSaveDTO imgTxtScanSaveDTO);

    /**
     * 删除图片扫描录入数据
     * @author YJ
     * @date 2022/2/16 16:09
     * @param imgTxtScanDelDTO
     */
    void delImgTxtScan(ImgTxtScanDelDTO imgTxtScanDelDTO);
}