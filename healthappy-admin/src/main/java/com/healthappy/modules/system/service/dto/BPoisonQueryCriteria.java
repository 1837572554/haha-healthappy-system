package com.healthappy.modules.system.service.dto;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;

/**
 * @description 有害因素
 * @author sjc
 * @date 2021-08-9
 */
@Data
public class BPoisonQueryCriteria {

    /**
     * id
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("id")
    private String id;

    /**
     * 毒害名称
     */
    @Query(blurry = "poisonName,searchCode")
    @ApiModelProperty("毒害名称")
    private String poisonName;
}
