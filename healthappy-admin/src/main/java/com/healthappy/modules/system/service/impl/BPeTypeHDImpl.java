package com.healthappy.modules.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.BPeTypeHD;
import com.healthappy.modules.system.service.BPeTypeHDService;
import com.healthappy.modules.system.service.mapper.BPeTypeHDMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @description 体检类型实现
 * @author yanjun
 * @date 2021年9月3日 10:44:41
 */
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true, rollbackFor = Exception.class)
public class BPeTypeHDImpl extends BaseServiceImpl<BPeTypeHDMapper,BPeTypeHD> implements BPeTypeHDService {


    /**
     * 通过体检类型（大类）编号获得体检分组的ID
     * @param peTypeHdId 体检类型编号（大类）
     * @return 体检分组的ID
     */
    @Override
    public Integer getPeTypeGroupId(Long peTypeHdId) {
        return this.getOne(new LambdaQueryWrapper<BPeTypeHD>().eq(BPeTypeHD::getId,peTypeHdId)).getGroupID();
    }
}

