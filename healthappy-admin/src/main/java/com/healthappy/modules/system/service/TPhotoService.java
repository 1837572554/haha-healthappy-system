package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TPhoto;

import java.util.List;

/**
 * @description 照片 服务层
 * @author fang
 * @date 2021-06-17
 */
public interface TPhotoService extends BaseService<TPhoto> {

    /**
     * 查询人员照片数据
     * @return Object
     */
    TPhoto query(String paId);
}
