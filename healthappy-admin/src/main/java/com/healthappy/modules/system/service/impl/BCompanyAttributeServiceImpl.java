package com.healthappy.modules.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BCompanyAttribute;
import com.healthappy.modules.system.domain.BTypeCompany;
import com.healthappy.modules.system.service.BCompanyAttributeService;
import com.healthappy.modules.system.service.BTypeCompanyService;
import com.healthappy.modules.system.service.dto.BCompanyAttributeQueryCriteria;
import com.healthappy.modules.system.service.dto.BTypeCompanyQueryCriteria;
import com.healthappy.modules.system.service.mapper.BCompanyAttributeMapper;
import com.healthappy.modules.system.service.mapper.BTypeCompanyMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @description 单位属性
 * @author sjc
 * @date 2021-08-30
 */
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BCompanyAttributeServiceImpl extends BaseServiceImpl<BCompanyAttributeMapper, BCompanyAttribute> implements BCompanyAttributeService {

    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BCompanyAttributeQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BCompanyAttribute> page = new PageInfo<BCompanyAttribute>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("content", generator.convert(page.getList(), BCompanyAttribute.class));
        map.put("totalElements", page.getTotal());

        return map;
    }
    /**
     * 查询单位属性全部数据
     * @return Object
     */
    @Override
    public List<BCompanyAttribute> queryAll(BCompanyAttributeQueryCriteria criteria) {
        return this.list(QueryHelpPlus.getPredicate(BTypeCompany.class, criteria));
    }
}
