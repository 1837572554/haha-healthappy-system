package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class ProjectCheckLiteDto implements Serializable {

    @ApiModelProperty("流水号")
    private String id;

    /**
     * 姓名
     */
    @ApiModelProperty("姓名")
    private String name;

    /**
     * 性别值
     */
    @ApiModelProperty("性别值")
    private String sex;

    /**
     * 性别
     */
    @ApiModelProperty("性别")
    private String sexText;

    /**
     * 年龄
     */
    @ApiModelProperty("年龄")
    private Integer age;

    /**
     * 年龄字符
     */
    @ApiModelProperty("年龄字符")
    private String ageDate;


    @ApiModelProperty("头像")
    private String photo;

    @ApiModelProperty("单位编号")
    private String companyId;

    @ApiModelProperty("部门ID")
    private String departmentId;

    @ApiModelProperty("分组")
    private String departmentGroupId;

}
