package com.healthappy.modules.system.service.mapper;


import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BPackageDT;
import com.healthappy.modules.system.service.dto.BPackageDTQueryCriteria;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 套餐项目表
 * @author sjc
 * @date 2021-07-05
 */
@Mapper
@Repository
public interface BPackageDTMapper extends CoreMapper<BPackageDT> {

    /**
     * 根据套餐编号获取下面的组合项目
     * 通过科室显示序号，组合项目显示序号排序
     * @author YJ
     * @date 2021/12/17 10:14
     * @param packageId 套餐编号
     * @return java.util.List〈com.healthappy.modules.system.domain.BPackageDT〉
     */
    List<BPackageDT> getGroupDispOrder(@Param("packageId") String packageId);

    List<BPackageDT> getGroupDispOrderByCriteria(@Param("criteria") BPackageDTQueryCriteria criteria);
}
