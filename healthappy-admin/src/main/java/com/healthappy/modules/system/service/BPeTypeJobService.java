package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BPeTypeJob;
import com.healthappy.modules.system.service.dto.BPeTypeJobQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 工种 服务层
 * @author fang
 * @date 2021-06-17
 */
public interface BPeTypeJobService extends BaseService<BPeTypeJob> {


    Map<String, Object> queryAll(BPeTypeJobQueryCriteria criteria, Pageable pageable);


    /**
     * 查询数据分页
     * @param criteria 条件
     * @return Map<String, Object>
     */
    List<BPeTypeJob> queryAll(BPeTypeJobQueryCriteria criteria);
}
