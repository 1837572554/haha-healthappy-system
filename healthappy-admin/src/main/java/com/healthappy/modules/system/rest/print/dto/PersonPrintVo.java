package com.healthappy.modules.system.rest.print.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Author: wukefei
 * @Date: Created in 2022/4/28 12:03
 * @Description:
 * @Version: 1.0
 *
 */
@ApiModel("批量个人打印参数")
@Data
public class PersonPrintVo {
	/** 流水号列表 */
	@ApiModelProperty("流水号")
	private List<String> paIds;
	/** 组合项目列表 */
	@ApiModelProperty("报告类型名称:个人健康体检报告、个人精简报告、职业健康检查表、职业健康检查通知单、儿童健康体检报告、从业预防性体检报告、放射健康检查表")
	private String reportName;

	/** 用于接收的Socket的用户编号 */
	@ApiModelProperty("用于接收的Socket的用户编号")
	private String acceptSocketUserId;

}
