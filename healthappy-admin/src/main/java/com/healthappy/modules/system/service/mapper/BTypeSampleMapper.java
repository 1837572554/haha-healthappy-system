package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BTypeSample;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 标本类型
 * @author sjc
 * @date 2021-08-3
 */
@Mapper
@Repository
public interface BTypeSampleMapper extends CoreMapper<BTypeSample> {
}
