package com.healthappy.modules.system.rest.common;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BTypeDevice;
import com.healthappy.modules.system.domain.vo.BTypeDeviceVo;
import com.healthappy.modules.system.service.BTypeDeviceService;
import com.healthappy.modules.system.service.dto.BTypeDeviceQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @description 申请类型 控制器
 * @author sjc
 * @date 2021-08-3
 */
@Slf4j
@Api(tags = "默认基础配置：申请类型")
@RestController
@AllArgsConstructor
@RequestMapping("/BTypeDevice")
public class BTypeDeviceController {

    private final BTypeDeviceService bTypeDeviceService;

    @Log("分页查询结果获取方式")
    @ApiOperation("分页查询结果获取方式，权限码：BTypeDevice:list")
    @GetMapping
    public R listPage(BTypeDeviceQueryCriteria criteria, Pageable pageable) {
        return R.ok(bTypeDeviceService.queryAll(criteria, pageable));
    }

    @Log("新增|修改结果获取方式")
    @ApiOperation("新增|修改结果获取方式，权限码：admin")
    @PostMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BTypeDeviceVo resources) {
        //如果名称重复
        if(checkBTypeDeviceName(resources)){
            return R.error(500,"结果获取名称已经存在");
        }
        BTypeDevice bTypeItem = new BTypeDevice();
        BeanUtil.copyProperties(resources,bTypeItem);
        if(bTypeDeviceService.saveOrUpdate(bTypeItem))
            return R.ok();
        return R.error(500,"方法异常");
    }

    @Log("删除结果获取方式")
    @ApiOperation("删除结果获取方式，权限码：admin")
    @DeleteMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R remove(@RequestParam("id") Long id) {
        if(bTypeDeviceService.deleteById(id))
            return R.ok();
        return R.error(500,"方法异常");
    }

    /**
     * 用于判断名称是否存在
     * @param resources
     * @return
     */
    private Boolean checkBTypeDeviceName(BTypeDeviceVo resources) {
        //名称相同  id不同的情况下能查询到数据，那么就是名称重复
        return bTypeDeviceService.lambdaQuery().eq(BTypeDevice::getName,resources.getName())
                .ne(ObjectUtil.isNotNull(resources.getId()),BTypeDevice::getId,resources.getId()).count() > 0;

    }

}
