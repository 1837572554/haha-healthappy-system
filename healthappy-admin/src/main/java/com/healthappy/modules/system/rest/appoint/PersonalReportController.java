package com.healthappy.modules.system.rest.appoint;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.http.report.HttpServer;
import com.healthappy.modules.system.domain.TPatient;
import com.healthappy.modules.system.domain.vo.AlwaysCheckVo;
import com.healthappy.modules.system.domain.vo.PersonalReportTaskSubmitVo;
import com.healthappy.modules.system.service.PersonalReportService;
import com.healthappy.modules.system.service.TPatientService;
import com.healthappy.modules.system.service.dto.PersonalReportQueryCriteria;
import com.healthappy.modules.system.service.dto.PersonalReportTaskQueryCriteria;
import com.healthappy.utils.R;
import com.healthappy.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author FGQ
 * @description 体检管理 个人报告总检
 * @date 2021-12-06
 */
@Slf4j
@Api(tags = "体检管理：个人报告总检")
@RestController
@AllArgsConstructor
@RequestMapping("/PersonalReport")
public class PersonalReportController {

    private final PersonalReportService personalReportService;

    private final TPatientService patientService;

    @Log("个人报告总检")
    @ApiOperation("个人报告总检，权限码：PersonalReport:list")
    @GetMapping
    @PreAuthorize("@el.check('PersonalReport:list')")
    public R list(PersonalReportQueryCriteria criteria) {
//        if(ObjectUtil.isNotNull(criteria.getDateType())){
//            if(ObjectUtil.isNull(criteria.getStartTime()) || ObjectUtil.isNull(criteria.getEndTime())){
//                return R.error("查询条件有误");
//            }
//        }
        return R.ok(personalReportService.list(criteria));
    }

    @Log("个人报告总检-详情")
    @ApiOperation("个人报告总检-详情，权限码：PersonalReport:list")
    @GetMapping("detail/{id}")
    @PreAuthorize("@el.check('PersonalReport:list')")
    public R detail(@PathVariable String id) {
        return R.ok(personalReportService.detail(id));
    }

    @Log("分配任务")
    @ApiOperation("分配任务，权限码：PersonalReport:task")
    @GetMapping("task")
    @PreAuthorize("@el.check('PersonalReport:task')")
    public R task(PersonalReportTaskQueryCriteria criteria) {
        return R.ok(personalReportService.task(criteria));
    }

    @Log("分配任务-医生总检数")
    @ApiOperation("分配任务-医生数，权限码：PersonalReport:task")
    @GetMapping("task/conclusionDoc")
    @PreAuthorize("@el.check('PersonalReport:task')")
    public R conclusionDoc() {
        return R.ok(personalReportService.conclusionDoc());
    }

    @Log("分配任务-医生审核数")
    @ApiOperation("分配任务-医生审核数，权限码：PersonalReport:task")
    @GetMapping("task/verifyDoc")
    @PreAuthorize("@el.check('PersonalReport:task')")
    public R verifyDoc() {
        return R.ok(personalReportService.verifyDoc());
    }

    @Log("分配任务-确定 or 撤销")
    @ApiOperation("分配任务-确定 or 撤销，权限码：PersonalReport:task")
    @PostMapping("task/submit")
    @PreAuthorize("@el.check('PersonalReport:task')")
    @Klock
    public R submit(@RequestBody List<PersonalReportTaskSubmitVo> taskSubmitVoList) {
        if(CollUtil.isEmpty(taskSubmitVoList)){
            return R.error("参数不能为空");
        }
        personalReportService.submit(taskSubmitVoList);
        return R.ok();
    }

    @Log("结论和综述")
    @ApiOperation("结论和综述，权限码：PersonalReport:list")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "String"),
            @ApiImplicitParam(name = "groupId", value = "体检分类-分组ID", required = true, dataType = "String"),
            @ApiImplicitParam(name = "resultMark", value = "是否异常", dataType = "String"),
            @ApiImplicitParam(name = "type", value = "1:职业,2:普通", dataType = "Integer"),
            @ApiImplicitParam(name = "btn", value = "1:刷新按钮点击而来", dataType = "Integer")
    })
    @GetMapping("summary/{id}/{groupId}")
    @PreAuthorize("@el.check('PersonalReport:list')")
    public R summary(@PathVariable String id, @PathVariable Integer groupId,
                     @RequestParam(required = false) String resultMark,
                     @RequestParam(required = false) Integer type,
                     @RequestParam(required = false) Integer btn) {
        return R.ok(personalReportService.summary(id,resultMark,groupId,type,btn));
    }

    @Log("项目详情结果")
    @ApiOperation("项目详情结果，权限码：PersonalReport:list")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "1:职业,2:普通", dataType = "Integer"),
    })
    @GetMapping("projectDetailsResults/{id}")
    @PreAuthorize("@el.check('PersonalReport:list')")
    public R projectDetailsResults(@PathVariable String id
    ,@RequestParam(required = false) Integer type) {
        return R.ok(personalReportService.projectDetailsResults(id,type));
    }

    @Log("历史对比")
    @ApiOperation("历史对比，权限码：PersonalReport:list")
    @GetMapping("historicalComparison/{id}")
    @PreAuthorize("@el.check('PersonalReport:list')")
    public R historicalComparison(@PathVariable String id) {
        return R.ok(personalReportService.historicalComparison(id));
    }

    @Log("总检 or 撤销")
    @ApiOperation("总检 or 撤销，权限码：PersonalReport:list")
    @PostMapping("alwaysCheck")
    @PreAuthorize("@el.check('PersonalReport:list')")
    @Klock
    public R alwaysCheck(@Validated @RequestBody AlwaysCheckVo alwaysCheckVo) {
        checkAlways(alwaysCheckVo);
        personalReportService.alwaysCheck(alwaysCheckVo);
        return R.ok();
    }

    @Log("总审 or 撤销")
    @ApiOperation("总审 or 撤销，权限码：PersonalReport:check")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "String"),
        @ApiImplicitParam(name = "type", value = "类型 0 / 1", required = true, dataType = "String"),
    })
    @PostMapping("check/{id}/{type}")
    @PreAuthorize("@el.check('PersonalReport:check')")
    @Klock
    public R check(@PathVariable String id, @PathVariable String type) {
        checkPersonalReport(id,type);
        personalReportService.check(id,type);
        return R.ok();
    }

    @Log("退回总检")
    @ApiOperation("退回总检，权限码：PersonalReport:check")
    @PostMapping("returnAlwaysCheck/{id}")
    @PreAuthorize("@el.check('PersonalReport:check')")
    @Klock
    public R returnAlwaysCheck(@PathVariable String id) {
        personalReportService.returnAlwaysCheck(id);
        return R.ok();
    }

    private void checkAlways(AlwaysCheckVo alwaysCheckVo) {
        TPatient patient = patientService.getById(alwaysCheckVo.getId());
        if(alwaysCheckVo.getType() == 1){
            if(patient.getConclusionDate() != null){
                throw new BadRequestException("该数据已经总检，请勿重复总检");
            }
            if(StrUtil.isNotBlank(patient.getConclusionDoc()) && !patient.getConclusionDoc().equals(SecurityUtils.getNickName())){
                throw new BadRequestException("不可以总检其他医生的总检");
            }
        }else {
            if(patient.getConclusionDate() == null){
                throw new BadRequestException("该数据还未总检,请先总检");
            }
            if(StrUtil.isNotBlank(patient.getConclusionDoc()) && !patient.getConclusionDoc().equals(SecurityUtils.getNickName())){
                throw new BadRequestException("不可以撤销其他医生的总检");
            }
            if(ObjectUtil.isNotNull(patient.getVerifyDate())){
                throw new BadRequestException("该数据已经总审完成，请勿撤销总检！！！");
            }
        }
    }

    private void checkPersonalReport(String id, String type) {
        TPatient patient = patientService.getById(id);
        if("1".equals(type)){
            if(null != patient.getVerifyDate()){
                throw new BadRequestException("该数据已经总审，请勿重复总审");
            }
            if(StrUtil.isNotBlank(patient.getVerifyDoc()) && !patient.getVerifyDoc().equals(SecurityUtils.getNickName())){
                throw new BadRequestException("不可以总审其他医生的总审");
            }
        }else {
            if(patient.getVerifyDate() == null){
                throw new BadRequestException("该数据还未总审，请先总审");
            }
            if(StrUtil.isNotBlank(patient.getVerifyDoc()) &&  !patient.getVerifyDoc().equals(SecurityUtils.getNickName())){
                throw new BadRequestException("不可以撤销其他医生的总审");
            }
        }
    }
}
