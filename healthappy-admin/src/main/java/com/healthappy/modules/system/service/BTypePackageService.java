package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BTypePackage;
import com.healthappy.modules.system.service.dto.BTypePackageQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 套餐类型 服务层
 * @author sjc
 * @date 2021-08-24
 */
public interface BTypePackageService extends BaseService<BTypePackage> {


    Map<String, Object> queryAll(BTypePackageQueryCriteria criteria, Pageable pageable);

    List<BTypePackage> queryAll(BTypePackageQueryCriteria criteria);
}
