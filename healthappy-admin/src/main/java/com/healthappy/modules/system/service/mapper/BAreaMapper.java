package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BArea;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * @description 区域表（阿斯利康）
 * @author fang
 * @date 2021-06-17
 */
@Mapper
@Repository
public interface BAreaMapper  extends CoreMapper<BArea> {

    /**
     * 检查是否存在下级
     * @param ids
     * @return
     */
    @Select({
        "<script>",
        "SELECT",
        "COUNT(id)",
        "FROM B_Area",
        "WHERE",
        "    hi_code",
        "IN (",
        "SELECT",
        "DISTINCT `code`",
        "FROM",
        "    B_Area",
        "WHERE",
        "id",
        " IN",
        "<foreach collection='ids' item='id' open='(' separator=',' close=')'>",
        "#{id}",
        "</foreach>)",
        "</script>"
    })
    Integer checkExistHiCode(@Param("ids") Set<Integer> ids);
}
