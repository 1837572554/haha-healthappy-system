package com.healthappy.modules.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Data
public class UNoticeVo implements Serializable {

    /**
     * id
     */
    @ApiModelProperty("id")
    private String id;

    /** 公告标题 */
    @ApiModelProperty("公告标题")
    private String title;

    /**
     * 公告信息
     */
    @ApiModelProperty("公告信息")
    private String notice;

    /**
     * 发布者
     */
    @ApiModelProperty("发布者")
    private String users;

    /**
     * 发布时间
     */
    @ApiModelProperty("发布时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp time;

    /**
     * 有效时间
     */
    @ApiModelProperty("有效时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp validDate;

    /**
     * 截止日期
     */
    @ApiModelProperty("截止日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp expirationDate;

    /**
     * 被公告科室id，多个id
     */
    @ApiModelProperty("被公告科室id，多个id")
    private List<String> deptId;
}
