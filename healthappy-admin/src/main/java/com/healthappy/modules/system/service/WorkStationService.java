package com.healthappy.modules.system.service;

import cn.hutool.core.date.DateTime;
import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.WorkStation;

/**
 * @description 工作站代码 服务层
 * @author FGQ
 * @date 2021-09-08
 */
public interface WorkStationService extends BaseService<WorkStation> {


    /**
     * 根据种子类型，生成对应类型的编号
     * @param type  种子类型 1.体检号 2.条码号 3.项目申请号
     * @return 返回对应类型的编号
     */
    String generatePrimaryKey(Integer type);

    /**
     * 组装编号
     * @param pk
     * @param fillBeforeLen
     * @param tenantId
     * @param dateTime
     * @return
     */
    String createPk(int pk, int fillBeforeLen, DateTime dateTime);

    /**
     * 获得工作站代码
     * @return 返回工作站代码
     */
    String getWorkStation();
}
