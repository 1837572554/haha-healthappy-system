package com.healthappy.modules.system.rest.ballcheck;

import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.service.FileImportRecordService;
import com.healthappy.modules.system.service.dto.FileImportRecoredQueryCriteria;
import com.healthappy.utils.R;
import com.healthappy.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description 导入记录 控制器
 * @author FGQ
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "导入记录：导入记录")
@RestController
@AllArgsConstructor
@RequestMapping("/fileImportRecord")
public class FileImportRecordController {

    private final FileImportRecordService fileImportRecordService;

    @Log("分页查询导入记录")
    @ApiOperation("分页查询导入记录，权限码：BallCheck:list")
    @GetMapping
    @PreAuthorize("@el.check('BallCheck:list')")
    public R listPage(FileImportRecoredQueryCriteria criteria, @PageableDefault(size = Integer.MAX_VALUE) Pageable pageable) {
        criteria.setCreateBy(SecurityUtils.getUserId());
        return R.ok(fileImportRecordService.queryAll(criteria,pageable));
    }
}
