package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class QueryDataCriteria {

    @NotNull(message = "时间类型不能为空")
    @ApiModelProperty("1:登记,2:签到,3:总检时间")
    private Integer timeType;

    @NotBlank(message = "时间不能为空")
    @ApiModelProperty("时间")
    private String startTime;

    @NotBlank(message = "时间不能为空")
    @ApiModelProperty("时间")
    private String endTime;

    @ApiModelProperty("单位")
    private String companyId;

    @ApiModelProperty("体检分类")
    private String peTypeHdId;

    @ApiModelProperty("工作站")
    private String workstation;

    @NotNull(message = "需要查询的字段不能为空")
    @ApiModelProperty("需要查询的字段")
    private List<String> queryField;

    @ApiModelProperty("科室名称集合")
    private List<String> deptList;

    @ApiModelProperty("组合项集合")
    private List<String> groupList;

    @ApiModelProperty("基础项目集合")
    private List<String>  itemList;
}
