package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @description 职业体检人员表
 * @author sjc
 * @date 2021-09-8
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName("T_Patient_Z")
@RenewLog
public class TPatientZ implements Serializable {

    /** ID */
    @TableId(value = "id", type = IdType.AUTO)
    @ApiModelProperty("主键id")
    private Long id;

    @ApiModelProperty("体检号")
    @TableField("pa_id")
    private String paId;

    @ApiModelProperty("接害工龄")
    @TableField("work_years")
    private String workYears;

    @ApiModelProperty("接害种类(B_Poison)")
    @TableField("poison_type")
    private String poisonType;

    @ApiModelProperty("是否职业禁忌证 ")
    @TableField("pj_taboo")
    private String pjTaboo;

    @ApiModelProperty("是否疑似职业病")
    @TableField("pj_suspicion")
    private String pjSuspicion;

    @ApiModelProperty("是否其他复查")
    @TableField("pj_review")
    private String pjReview;

    @ApiModelProperty("未见异常")
    @TableField("pj_remove")
    private String pjRemove;

    @ApiModelProperty("是否其他疾病或异常")
    @TableField("pj_unusual")
    private String pjUnusual;

    @ApiModelProperty("是否复查")
    @TableField("pj_relate")
    private String pjRelate;

    @ApiModelProperty("职业结论")
    @TableField("comment_z")
    private String commentZ;

    @ApiModelProperty("职业建议")
    @TableField("suggest_z")
    private String suggestZ;

    /**
     * 医疗机构ID(U_Hospital_Info的ID)
     */
    @ApiModelProperty("医疗机构ID(U_Hospital_Info的ID)")
    @TableField(value = "tenant_id", fill = FieldFill.INSERT)
    private String tenantId;
}
