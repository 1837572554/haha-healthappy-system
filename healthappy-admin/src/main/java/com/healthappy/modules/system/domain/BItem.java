package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.config.SeedIdGenerator;
import com.healthappy.config.databind.FieldBind;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @description 项目表
 * @author FGQ
 * @date 2021-06-22
 */
@Data
@ApiModel("项目表")
@TableName("B_Item")
@SeedIdGenerator
public class BItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.INPUT)
    /**
     * id
     */
    @ApiModelProperty("id")
    private String id;

    /**
     * 科室编号
     */
    @ApiModelProperty("科室编号")
    private String deptId;

    /**
     * 项目名称
     */
    @ApiModelProperty("项目名称")
    private String name;

    /**
     * 仪器通道号
     */
    @ApiModelProperty("仪器通道号")
    private String instrCode;

    /**
     * 适用性别
     */
    @ApiModelProperty("适用性别")
    @FieldBind(target = "sexText")
    private String sex;

    @TableField(exist = false)
    private String sexText;

    /**
     * 默认结果
     */
    @ApiModelProperty("默认结果")
    private String result;

    /**
     * 结果类型0文本，1数值
     */
    @ApiModelProperty("结果类型0文本，1数值")
    @FieldBind(type = "itemResultType",target = "resultTypeText")
    private String resultType;

    @TableField(exist = false)
    private String resultTypeText;

    /**
     * 结果单位
     */
    @ApiModelProperty("结果单位")
    private String unit;

    /**
     * 参考上限
     */
    @ApiModelProperty("参考上限")
    private String refHigh;

    /**
     * 参考下限
     */
    @ApiModelProperty("参考下限")
    private String refLow;

    /**
     * 参考上限（女）
     */
    @ApiModelProperty("参考上限（女）")
    private String refHighF;

    /**
     * 参考下限（女）
     */
    @ApiModelProperty("参考下限（女）")
    private String refLowF;

    /**
     * 偏高提示
     */
    @ApiModelProperty("偏高提示")
    private String flagHigh;

    /**
     * 偏低提示
     */
    @ApiModelProperty("偏低提示")
    private String flagLow;

    /**
     * 最大值
     */
    @ApiModelProperty("最大值")
    private String max;

    /**
     * 最小值
     */
    @ApiModelProperty("最小值")
    private String min;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remarks;

    /**
     * 是否进入小结
     */
    @ApiModelProperty("是否进入小结")
    private String isSummary;

    /**
     * 冲突项目
     */
    @ApiModelProperty("冲突项目")
    private String clashName;

    /**
     * 冲突级别
     */
    @ApiModelProperty("冲突级别")
    private String clashLevel;

    /**
     * 启用
     */
    @ApiModelProperty("启用")
    private String isEnable;

    /**
     * disp_order
     */
    @ApiModelProperty("disp_order")
    private Integer dispOrder;

    /**
     * 项目编码
     */
    @ApiModelProperty("项目编码")
    private String code;

    /**
     * 简拼
     */
    @ApiModelProperty("简拼")
    private String jp;

    /**
     * 修改医生
     */
    @ApiModelProperty("修改医生")
    @TableField(value = "update_by",fill = FieldFill.INSERT_UPDATE)
    private Long updateBy;

    @ApiModelProperty(value = "租户ID")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time",fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp updateTime;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Timestamp createTime;

    /**
     * 是否删除 0不是  1是
     */
    @ApiModelProperty("是否删除")
    @TableField(value="del_flag",fill=FieldFill.INSERT)
//    @TableLogic(value = "0",delval = "1")
    private String delFlag;
}
