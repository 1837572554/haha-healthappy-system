package com.healthappy.modules.system.domain;

import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 图片信息 DTO
 * @author Jevany
 * @date 2022/2/16 14:49
 */
@Data
@RenewLog
public class TImageDTO {
    /** 主键id-长度20 */
    @ApiModelProperty("主键id-长度20")
    private String id;

    /** 文件名称（上传时的文件名） */
    @ApiModelProperty("文件名称（上传时的文件名）")
    private String fileName;

    /** 图片Url */
    @ApiModelProperty("图片Url")
    private String url;


    /** 添加时间 */
    @ApiModelProperty("添加时间")
    private String createTime;
}