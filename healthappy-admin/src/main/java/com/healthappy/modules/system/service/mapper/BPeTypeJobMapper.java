package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BPeTypeJob;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 工种表（阿斯利康）
 * @author fang
 * @date 2021-06-17
 */
@Mapper
@Repository
public interface BPeTypeJobMapper extends CoreMapper<BPeTypeJob> {

}
