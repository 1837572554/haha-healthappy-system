package com.healthappy.modules.system.rest.basic;

import cn.hutool.core.bean.BeanUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.WorkStation;
import com.healthappy.modules.system.domain.vo.WorkStationVo;
import com.healthappy.modules.system.service.WorkStationService;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Collectors;

/**
 * @description 流水号生成测试
 * @author FGQ
 * @date 2021-09-08
 */
@Slf4j
@Api(tags = "基础管理：工作站")
@RestController
@AllArgsConstructor
@RequestMapping("/workStation")
public class WorkStationController {

    private final WorkStationService workStationService;

//    @Log("新增流水号")
//    @ApiOperation("新增流水号")
//    @PostMapping
//    private R saveOrUpdate() {
//        List<String> primaryKey = Collections.synchronizedList(new LinkedList<>());
//        ThreadUtil.concurrencyTest(5, () -> {
//            for (int i = 0; i < 20; i ++){
//                synchronized (primaryKey){
//                    primaryKey.add(workStationService.generatePrimaryKey( 1));
//                }
//            }
//        });
//        return R.ok(primaryKey);
//    }

    /**
     * 获得当前租户的WorkStation列表
     * @author: YJ
     * @date: 2021/11/26 14:44
     * @return: java.util.List\<com.healthappy.modules.system.domain.WorkStation\>
    */
    @Log("获得当前租户的WorkStation列表")
    @ApiOperation("获得当前租户的WorkStation列表")
    @GetMapping("/getList")
    public R getList(){
        return R.ok(workStationService.lambdaQuery().eq(WorkStation::getIsEnable,true).list().stream().map(ws->{
            return BeanUtil.copyProperties(ws, WorkStationVo.class);
        }).collect(Collectors.toList()));
    }
}
