package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @description 综合设置
 * @author cyt
 * @date 2021-08-17
 */
@Data
public class BSetUpVo {
    /**
     * id
     */
    private String id;

    /**
     * 设置名称
     */
    @NotBlank(message = "名称不能为空")
    @ApiModelProperty(value = "名称",required = true)
    private String xmlName;

    /**
     * 设置值
     */
    @NotBlank(message = "值不能为空")
    @ApiModelProperty(value = "值",required = true)
    private String value;

    /**
     * 编码
     */
    @ApiModelProperty(value = "医疗机构ID")
    private String tenantId;
}
