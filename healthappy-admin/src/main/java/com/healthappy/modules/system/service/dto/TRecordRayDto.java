package com.healthappy.modules.system.service.dto;

import com.baomidou.mybatisplus.annotation.TableName;
import com.healthappy.modules.system.domain.TRecordRay;
import io.swagger.annotations.ApiModel;
import lombok.Data;


/**
 * @description 职业史放射表
 * @author sjc
 * @date 2021-12-15
 */
@Data
@ApiModel("职业史放射表")
@TableName("T_Record_Ray")
public class TRecordRayDto extends TRecordRay {
}
