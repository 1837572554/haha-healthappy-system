package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class BSicknessRuleQueryCriteria {

    /**
     * 病种ID
     */
    @Query(type = Query.Type.IN)
    @ApiModelProperty("病种ID")
    private List<String> sicknessId;

    @ApiModelProperty("病种名称")
    private String sicknessName;

    /**
     * 规则类型
     */
    @Query(type = Query.Type.INNER_LIKE)
    @ApiModelProperty("规则类型")
    private String ruleType;


    @ApiModelProperty("项目ID")
    @Query(type = Query.Type.EQUAL)
    private String itemId;

    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("是否删除 空不限制,0：未删除，1：删除。默认0")
    private String delFlag="0";
}
