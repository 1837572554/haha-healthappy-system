package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TImgTxtScan;
import com.healthappy.modules.system.service.dto.ImgTxtScanManageQueryCriteria;
import com.healthappy.modules.system.service.dto.TPatientLiteDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Jevany
 * @date 2022/2/15 17:16
 */
@Mapper
@Repository
public interface ImgTxtScanMapper extends CoreMapper<TImgTxtScan> {
    /**
     * 获取图文扫描录入管理列表
     * @author YJ
     * @date 2022/2/15 17:21
     * @param criteria
     * @return java.util.List〈com.healthappy.modules.system.service.dto.TPatientLiteDto〉
     */
    List<TPatientLiteDto> getImgTxtScanManageList(@Param("criteria") ImgTxtScanManageQueryCriteria criteria);

    /**
     * 获取图文扫描录入管理条数
     * @author YJ
     * @date 2022/2/15 18:21
     * @param criteria
     * @return java.lang.Integer
     */
    Integer getImgTxtScanCount(@Param("criteria") ImgTxtScanManageQueryCriteria criteria);

    /**
     * 获取图文扫描录入-详细
     * @author YJ
     * @date 2022/2/16 11:53
     * @param paId
     * @param imgTxtTypeId
     * @return com.healthappy.modules.system.domain.TImgTxtScan
     */
    TImgTxtScan getImgTxtScan(@Param("paId") String paId,@Param("imgTxtTypeId") String imgTxtTypeId);

}
