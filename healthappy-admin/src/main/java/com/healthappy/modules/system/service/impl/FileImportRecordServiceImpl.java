package com.healthappy.modules.system.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.FileImportRecord;
import com.healthappy.modules.system.domain.User;
import com.healthappy.modules.system.domain.vo.FileImportRecordVO;
import com.healthappy.modules.system.service.FileImportRecordService;
import com.healthappy.modules.system.service.UserService;
import com.healthappy.modules.system.service.dto.FileImportRecoredQueryCriteria;
import com.healthappy.modules.system.service.mapper.FileImportRecordMapper;
import com.healthappy.utils.CacheConstant;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
@CacheConfig(cacheNames = CacheConstant.FILE_IMPORT_RECORD)
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class FileImportRecordServiceImpl extends BaseServiceImpl<FileImportRecordMapper, FileImportRecord>
		implements FileImportRecordService {

	private final UserService userService;
	private final IGenerator generator;

	@Override
	@CacheEvict(allEntries = true)
	public void submitExecl(String filePath, String fileName) {
		this.lambdaUpdate().set(FileImportRecord::getFileAddress, filePath).eq(FileImportRecord::getFileName, fileName)
				.update();
	}

	@Override
	@Cacheable
	public Map<String, Object> queryAll(FileImportRecoredQueryCriteria criteria, Pageable pageable) {
		getPage(pageable);
		PageInfo<FileImportRecordVO> page = new PageInfo<>(queryAll(criteria));
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("content", page.getList());
		map.put("totalElements", page.getTotal());
		return map;
	}

	@Override
	@Cacheable
	public List<FileImportRecordVO> queryAll(FileImportRecoredQueryCriteria criteria) {
		List<FileImportRecord> list = baseMapper.selectList(
				QueryHelpPlus.getPredicate(criteria, FileImportRecord.class));
		List<FileImportRecordVO> voList = generator.convert(list, FileImportRecordVO.class);

		voList.forEach(e -> {
			User user = userService.getById(e.getCreateBy());
			e.setCreateName(Optional.ofNullable(user).isPresent() ? user.getNickName() : "");
		});
		return voList;
	}

	@Override
	@Cacheable
	public String checkFileName(String fileName) {
		FileImportRecord importRecord = this.lambdaQuery().eq(FileImportRecord::getFileName, fileName).one();
		if (null != importRecord) {
			if (!importRecord.getIsSuccess()) {
				return null;
			} else {
				return fileName + "-" + RandomUtil.randomString(3);
			}
		}
		return fileName;
	}

	@Override
	@CacheEvict(allEntries = true)
	public void saveImport(String fileName, String fileType) {
		FileImportRecord importRecord = new FileImportRecord();
		importRecord.setFileName(fileName);
		importRecord.setFileType(fileType);
		this.save(importRecord);
	}
}
