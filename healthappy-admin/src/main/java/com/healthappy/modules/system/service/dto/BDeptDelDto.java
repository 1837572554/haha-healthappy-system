package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Set;

/**
 * @author Jevany
 * @date 2021/12/20 18:51
 * @desc 科室删除Dto
 */
@Data
public class BDeptDelDto implements Serializable {
    /** 删除的科室编号数组 */
    @ApiModelProperty("删除的科室编号数组")
    @NotBlank( message = "删除的科室编号数组")
    public Set<Integer> ids;

    /** 是否确认删除 */
    @ApiModelProperty("是否确认删除")
    @NotBlank(message = "是否确认删除")
    public Boolean confirm;
}
