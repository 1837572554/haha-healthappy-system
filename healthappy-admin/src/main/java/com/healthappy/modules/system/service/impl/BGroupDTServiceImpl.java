package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BGroupDT;
import com.healthappy.modules.system.domain.vo.BGroupDTVo;
import com.healthappy.modules.system.service.BGroupDTService;
import com.healthappy.modules.system.service.dto.BGroupDTDto;
import com.healthappy.modules.system.service.dto.BGroupDTQueryCriteria;
import com.healthappy.modules.system.service.mapper.BGroupDTMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author sjc
 * @date 2021-08-04
 */
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BGroupDTServiceImpl extends BaseServiceImpl<BGroupDTMapper, BGroupDT> implements BGroupDTService {


    private final IGenerator generator;


    @Override
    public Map<String, Object> queryAll(BGroupDTQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BGroupDT> page = new PageInfo<BGroupDT>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", generator.convert(page.getList(), BGroupDTDto.class));
        map.put("totalElements", page.getTotal());
        return map;
    }

    @Override
    public List<BGroupDT> queryAll(BGroupDTQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BGroupDTDto.class, criteria));
    }

    /**
     * 根据组合项目id删除子项
     * @param groupId
     * @return
     */
    @Override
    public boolean removeBygroupId(String groupId) {
        return   this.lambdaUpdate().eq(BGroupDT::getGroupId,groupId).remove();
    }

    /**
     * 根据项目id删除子项
     * @param ids
     * @return
     */
    @Override
    public boolean removeByIds(Set<Integer> ids) {
        return  this.lambdaUpdate().in(BGroupDT::getGroupId,ids).remove();
    }

    @Override
    public void saveOrUpdate(List<BGroupDTVo> resources) {
        List<BGroupDT> groupDTList = this.lambdaQuery().eq(BGroupDT::getGroupId,resources.get(0).getGroupId()).list();
        //如果没有项目清空当前的组合
        if(ObjectUtil.isNull(resources.get(0).getItemId())){
            this.lambdaUpdate().eq(BGroupDT::getGroupId,resources.get(0).getGroupId()).remove();
            //如果没有项目，新增
        }else if(CollUtil.isEmpty(groupDTList)){
            this.saveBatch(generator.convert(resources, BGroupDT.class));
        }else{
            //新增额外选中的数据
            //将前端传入的数据和数据库中的对比，将所有与数据库中不符合的取出，就是需要新增的数据
            List<BGroupDTVo> groupDTVoList = resources.stream().filter(
                    r -> groupDTList.stream().noneMatch
                    (g-> g.getItemId().equals(r.getItemId()) && g.getGroupId().equals(r.getGroupId()))
            ).collect(Collectors.toList());

            if(CollUtil.isNotEmpty(groupDTVoList) && ObjectUtil.isNotNull(resources.get(0).getItemId())){
                this.saveBatch(generator.convert(groupDTVoList,BGroupDT.class));
            }
            //删除未被选中的数据
            //前端可能会删除部分项目，将其获取出来
            List<BGroupDT> bGroupDTList = groupDTList.stream().filter(g -> resources.stream().noneMatch(r ->
                    r.getItemId().equals(g.getItemId()) && r.getGroupId().
                            equals(g.getGroupId()))).collect(Collectors.toList());

            //取消 GroupID 中数据库存在的，切新增中没有的
            List<BGroupDT> list = bGroupDTList.stream().filter(b -> resources.stream().allMatch(g -> b.getGroupId().equals(g.getGroupId())))
                                                        .collect(Collectors.toList());

            if(CollUtil.isNotEmpty(list)){
                for (BGroupDT dt: list) {
                    this.remove(Wrappers.<BGroupDT>lambdaQuery().eq(BGroupDT::getGroupId,dt.getGroupId()).eq(BGroupDT::getItemId,dt.getItemId()));
                }
            }
        }
    }


}
