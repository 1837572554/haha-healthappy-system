package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BSicknessCause;
import com.healthappy.modules.system.domain.BSicknessScience;

/**
 * @description 医学科普表 服务层
 * @author fang
 * @date 2021-06-17
 */
public interface BSicknessScienceService extends BaseService<BSicknessScience> {



}
