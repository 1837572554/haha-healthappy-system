package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 体检者条码数据
 * @author Jevany
 * @date 2022/5/12 16:32
 */
@Data
@ApiModel("体检者条码数据")
public class PatientBarcodeDTO implements Serializable {

	/** 条码头名称 */
	@ApiModelProperty("条码头名称")
	private String barcodeName;

	/** 条码数量 */
	@ApiModelProperty("条码数量")
	private Integer barcodeNum;

	/** 项目名称 */
	@ApiModelProperty("项目名称")
	private String items;

	/** 申请类型 */
	@ApiModelProperty("申请类型")
	private String applyType;

	/** 条码 */
	@ApiModelProperty("条码")
	private String code;

	private static final long serialVersionUID = 1L;
}