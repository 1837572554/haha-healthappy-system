package com.healthappy.modules.system.service.dto;

import com.healthappy.modules.system.domain.BItem;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author FGQ
 * @date 2020-05-14
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BItemDto extends BItem {

    @ApiModelProperty("科室名称")
    private String deptName;

    @ApiModelProperty(value = "是否删除 空不限制,0：未删除，1：删除。默认0" ,hidden = true)
    private String delFlag="0";
}
