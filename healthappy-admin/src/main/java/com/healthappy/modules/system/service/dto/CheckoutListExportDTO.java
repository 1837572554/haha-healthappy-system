package com.healthappy.modules.system.service.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @Author: YuTang
 * @Date: Created in 2022/3/28 15:02
 * @Description: 结账名单导出模型
 * @Version: 1.0
 */
@Data
public class CheckoutListExportDTO {

  @ExcelProperty("收费单号")
  private String payApplyId;

  @ExcelProperty("流水号")
  private String paId;

  @ExcelProperty("体检分类")
  private String peTypeHdName;

  @ExcelProperty("体检类别")
  private String peTypeDtName;

  @ExcelProperty("姓名")
  private String name;

  @ExcelProperty("性别")
  private String sex;

  @ExcelProperty("年龄")
  private Integer age;

  @ExcelProperty("单位")
  private String companyName;

  @ExcelProperty("部门")
  private String deptName;

  @ExcelProperty("分组")
  private String deptGroupName;

  @ExcelProperty("套餐名称")
  private String packageName;

  @ExcelProperty("结账")
  private String payFlag;

  @ExcelProperty("锁定")
  private String isLock;
}
