package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.modules.system.service.dto.AlonePayCostDetailDto;
import com.healthappy.modules.system.service.dto.AlonePayCostDto;
import com.healthappy.modules.system.service.dto.PayCostAloneQueryCriteria;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 个人
 * @author FGQ
 * @date 2021-06-24
 */
@Mapper
@Repository
public interface AlonePayCostMapper {

    /**
     * 查询列表
     * @param criteria
     * @return
     */
    @SqlParser(filter = true)
    List<AlonePayCostDto> getAll(@Param("criteria") PayCostAloneQueryCriteria criteria,@Param("tenantId") String tenantId);

    /**
     * 根据ID查询数据
     * @param paId
     * @return
     */
    @SqlParser(filter = true)
    AlonePayCostDetailDto.Detail getByPaId(@Param("paId") String paId,@Param("tenantId") String tenantId);


    /**
     * 查询项目详情
     * @param paId
     * @return
     */
    @SqlParser(filter = true)
    List<AlonePayCostDetailDto.ItemHd> getItemHdList(@Param("paId") String paId,@Param("tenantId") String tenantId);

    /**
     * 缴费记录
     * @param paId
     * @return
     */
    @SqlParser(filter = true)
    List<AlonePayCostDetailDto.TCharge> getTChargeList(@Param("paId") String paId,@Param("tenantId") String tenantId);
}
