package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description 职业史表
 * @author sjc
 * @date 2021-12-15
 */
@Data
@ApiModel("职业史表")
@TableName("T_Record")
@RenewLog
public class TRecord implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    @TableId(type = IdType.AUTO)
    private Long id;


    /**
     * 体检号
     */
    @ApiModelProperty("体检号")
    private String paId;

    /**
     * 起止日期
     */
    @ApiModelProperty("起止日期")
    private String startEnd;

    /**
     * 单位
     */
    @ApiModelProperty("单位")
    private String workUnit;

    /**
     * 车间
     */
    @ApiModelProperty("车间")
    private String workshop;

    /**
     * 工种
     */
    @ApiModelProperty("工种")
    private String job;

    /**
     * 危害因素
     */
    @ApiModelProperty("危害因素")
    private String poison;

    /**
     * 防护措施
     */
    @ApiModelProperty("防护措施")
    private String defend;

    /**
     * 医疗机构ID(U_Hospital_Info的ID)
     */
    @ApiModelProperty("医疗机构ID(U_Hospital_Info的ID)")
    private String tenantId;
}
