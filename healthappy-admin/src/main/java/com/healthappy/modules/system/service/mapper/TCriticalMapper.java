package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TCritical;
import com.healthappy.modules.system.service.dto.CriticalDTO;
import com.healthappy.modules.system.service.dto.CriticalObjDTO;
import com.healthappy.modules.system.service.dto.CriticalQueryCriteria;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Jevany
 * @date 2022/2/9 18:18
 */
@Mapper
@Repository
public interface TCriticalMapper extends CoreMapper<TCritical> {

    /**
     * 获得危急值列表
     * @author YJ
     * @date 2022/2/11 11:52
     * @param criteria
     * @return java.util.List〈com.healthappy.modules.system.service.dto.CriticalDTO〉
     */
    List<CriticalDTO> getCriticalList(@Param("criteria") CriticalQueryCriteria criteria);

    /**
     * 获得危急值总数
     * @author YJ
     * @date 2022/2/11 11:52
     * @param criteria
     * @return java.lang.Integer
     */
    Integer getCriticalCount(@Param("criteria") CriticalQueryCriteria criteria);

    /**
     * 获得一个危急值对象
     * @author YJ
     * @date 2022/2/11 11:53
     * @param id
     * @return com.healthappy.modules.system.service.dto.CriticalObjDTO
     */
    CriticalObjDTO getCriticalObj(@Param("id") Long id);

    /**
     * 修改危急值对象
     * @author YJ
     * @date 2022/2/11 11:53
     * @param critical
     * @return java.lang.Integer
     */
    Integer editCritical(@Param("critical") CriticalObjDTO critical);


    /**
     * 获取需要推送（没有处理）的危急值数据
     * @author YJ
     * @date 2022/3/17 14:18
     * @return java.util.List〈com.healthappy.modules.system.service.dto.CriticalObjDTO〉
     */
    @SqlParser(filter = true)
    List<CriticalObjDTO> listNeedNotificationCritical();


}
