package com.healthappy.modules.system.service;

import com.healthappy.modules.system.domain.vo.AlwaysCheckVo;
import com.healthappy.modules.system.domain.vo.PersonalReportTaskSubmitVo;
import com.healthappy.modules.system.service.dto.*;

import java.util.List;
import java.util.Map;

/**
 * @description 体检管理：个人报告总检
 * @author FGQ
 * @date 2021-12-06
 */
public interface PersonalReportService {


    Map<String,Object> list(PersonalReportQueryCriteria criteria);

    Object detail(String id);

    Map<String,Object> task(PersonalReportTaskQueryCriteria criteria);

    List<TaskDockerNumDto> conclusionDoc();

    List<TaskDockerNumDto> verifyDoc();

    void submit(List<PersonalReportTaskSubmitVo> taskSubmitVoList);

    PersonalReportSummaryDto summary(String id, String resultMark,Integer groupId,Integer type,Integer btn);

    List<ProjectDetailsResultsDto> projectDetailsResults(String id,Integer type);

    List<HistoricalComparisonDto> historicalComparison(String id);

    void alwaysCheck(AlwaysCheckVo alwaysCheckVo);

    void check(String id, String type);

    void returnAlwaysCheck(String id);
}
