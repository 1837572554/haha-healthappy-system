package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BBarcode;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 条码设置表
 * @author sjc
 * @date 2021-12-13
 */
@Mapper
@Repository
public interface BBarcodeMapper extends CoreMapper<BBarcode> {


}
