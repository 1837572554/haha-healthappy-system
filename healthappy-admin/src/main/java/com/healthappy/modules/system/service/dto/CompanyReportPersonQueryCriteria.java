package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 单位报告人员列表获取类
 *
 * @author Jevany
 * @date 2022/2/18 19:33
 */
@Data
public class CompanyReportPersonQueryCriteria {
    /**
     * 报告编号，不能为空
     */
    @NotBlank(message = "报告编号不能为空")
    @ApiModelProperty("报告编号，不能为空")
    private String repId;

    /**
     * 页面显示条数
     */
    @ApiModelProperty(value = "页面显示条数,默认为10", position = 101)
    private Integer pageSize = 10;

    /**
     * 页数
     */
    @ApiModelProperty(value = "页数,默认为1", position = 102)
    private Integer pageNum = 1;

    /**
     * 开始条数
     */
    @ApiModelProperty(value = "开始条数", hidden = true)
    private Integer startIndex = -1;

    /**
     * 租户Id
     */
    @ApiModelProperty(value = "租户Id", hidden = true)
    private String tenantId;

    /** 是否导出，默认为否 */
    @ApiModelProperty(value = "是否导出，默认为否", hidden = true)
    private Boolean isExport = false;
}