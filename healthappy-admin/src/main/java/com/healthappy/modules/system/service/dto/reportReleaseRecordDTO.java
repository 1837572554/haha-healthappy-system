package com.healthappy.modules.system.service.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 体检报告发布记录DTO
 * @author Jevany
 * @date 2022/3/7 16:42
 */
@Data
public class reportReleaseRecordDTO implements Serializable {

    /** 发布编号 */
    private String releaseId;

    /** 发布类型名称 */
    private String releaseType;

    /** 报告类型 */
    private String reportType;

    /** 操作医生 */
    private String Doctor;
}