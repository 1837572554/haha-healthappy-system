package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * @author sjc
 * @date 2021-06-29
 */
@Data
public class TItemHDQueryCriteria  {

    /**
     * 体检号
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("体检号")
    private String paId;

    /**
     * 组合编号
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("组合编号")
    private String groupId;

    /**
     * 科室编号
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("科室编号")
    private String deptId;

    /**
     * 组合名称
     */
    @Query(type = Query.Type.INNER_LIKE)
    @ApiModelProperty("组合名称")
    private String groupName;

    /**
     * 条码
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("条码")
    private String barcode;

    /**
     * 复查
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("复查")
    private Integer review;
}
