package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TPatientZ;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 职业体检人员表
 * @author sjc
 * @date 2021-09-8
 */
@Mapper
@Repository
public interface TPatientZMapper extends CoreMapper<TPatientZ> {

}
