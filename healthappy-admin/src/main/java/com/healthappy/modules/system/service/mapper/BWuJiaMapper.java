package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BWuJia;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description
 * @author sjc
 * @date 2021-08-3
 */
@Mapper
@Repository
public interface BWuJiaMapper  extends CoreMapper<BWuJia> {
}
