package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA. Created by yutang 2021/9/8 0008  9:14 Description:
 */
@Data
@TableName("B_Type_Apply")
@ApiModel("预约类型表")
public class BTypeAppoint implements Serializable {

  private Long id;

  @ApiModelProperty("名称")
  private String name;

  @ApiModelProperty("排序")
  private Integer dispOrder;
}
