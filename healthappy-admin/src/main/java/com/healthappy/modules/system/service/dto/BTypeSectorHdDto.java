package com.healthappy.modules.system.service.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author FGQ
 * @date 2020-05-14
 */
@Data
public class BTypeSectorHdDto implements Serializable {

    /**
     * id
     */
    private Long id;

    /**
     * 名称
     */
    private String name;

    /**
     * 排序
     */
    private Integer dispOrder;

    /**
     * 名称
     */
    private String code;
}
