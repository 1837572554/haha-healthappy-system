package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BMaritalStatus;

/**
 * Created with IntelliJ IDEA. Created by yutang 2021/9/2 0002  11:01 Description:
 */
public interface BMaritalStatusService extends BaseService<BMaritalStatus> {

}
