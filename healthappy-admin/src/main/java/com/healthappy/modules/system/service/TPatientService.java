package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TPatient;
import com.healthappy.modules.system.domain.vo.*;
import com.healthappy.modules.system.service.dto.*;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author sjc
 * @description 体检人员表 服务层
 * @date 2021-06-28
 */
public interface TPatientService extends BaseService<TPatient> {

	Map<String, Object> queryAll(TPatientQueryCriteria criteria, Pageable pageable);

	List<TPatient> queryAll(TPatientQueryCriteria criteria);

	@Deprecated
	boolean saveOrUpdate(TPatientVo resources);

	/**
	 * 保存修改体检登记
	 *
	 * @param resources TPatientVo 体检人员数据
	 * @param items     TItemHDVo项目集合
	 */
	@Deprecated
	String saveOrUpdate(TPatientVo resources, List<TItemHDVo> items) throws Exception;

	boolean deleteById(String id);

	/**
	 * 体检登记列表
	 *
	 * @param criteria
	 * @param pageable
	 * @return
	 */
	Map<String, Object> queryRegistrationAll(MedicalRegistrationQueryCriteria criteria, Pageable pageable);

	/**
	 * 体检登记列表 精简 只输出  流水号、姓名、单位
	 *
	 * @param criteria
	 * @param pageable
	 * @return
	 */
	Map<String, Object> queryRegistrationAllLite(MedicalRegistrationQueryCriteria criteria, Pageable pageable);

	/**
	 * 获得体检人员数据
	 *
	 * @param patientId 体检号
	 * @return com.healthappy.modules.system.service.dto.MedicalRegistrationDto
	 * @author YJ
	 * @date 2022/1/12 16:39
	 */
	MedicalRegistrationDto getPatient(String patientId);

	/**
	 * 获取体检表的数据（单表T_Patient表数据）
	 * @author YJ
	 * @date 2022/4/14 10:51
	 * @param patientId
	 * @return com.healthappy.modules.system.domain.TPatient
	 */
	TPatient getPatientInfo(String patientId);

	/**
	 * 体检登记
	 *
	 * @param criteria
	 * @return
	 */
	List<TPatient> queryAll(MedicalRegistrationQueryCriteria criteria);

	String installVo(TPatientAddVo resources);

	/**
	 * 修改体检信息
	 *
	 * @return java.lang.String
	 * @author YJ
	 * @date 2022/2/9 10:52
	 */
	String editPatientInfo(TPatientVo patientVo);

	/**
	 * 修改体检项目
	 *
	 * @param itemVO
	 * @return java.lang.String
	 * @author YJ
	 * @date 2022/2/9 12:02
	 */
	String editPatientItem(TPatientItemVO itemVO);


	/**
	 * 获得体检时间类型
	 *
	 * @param pagePath
	 * @author: Jevany
	 * @date: 2021/11/26 11:59
	 * @return: java.lang.String
	 */
	String getPeDateTypes(String pagePath);

	void generateBarcode(String paId);


	/**
	 * 体检人员第一个项目 体检号，真实签到时间，租户号，必填
	 *
	 * @param tPatient 体检号，真实签到时间，租户号，必填
	 * @return java.lang.Integer
	 * @author YJ
	 * @date 2022/1/11 10:05
	 */
	Integer updSignDate(TPatient tPatient);


	/**
	 * 获取体检报告发布列表
	 * @param criteria
	 * @param pageable
	 * @return java.util.Map〈java.lang.String,java.lang.Object〉
	 * @author YJ
	 * @date 2022/3/3 18:40
	 */
	Map<String, Object> listReportRelease(ReportReleaseQueryCriteria criteria, Pageable pageable);

	/**
	 * 获得批量修改信息Map列表
	 * @author YJ
	 * @date 2022/5/13 9:11
	 * @param criteria
	 * @param pageable
	 * @return java.util.Map〈java.lang.String,java.lang.Object〉
	 */
	Map<String, Object> mapBatchInfoEdit(BatchInfoEditQueryCriteria criteria, Pageable pageable);

	/**
	 * 获得批量修改信息List列表
	 * @author YJ
	 * @date 2022/5/13 9:12
	 * @param criteria
	 * @return java.util.List〈com.healthappy.modules.system.domain.vo.batchInfoEditVO〉
	 */
	List<BatchInfoEditVO> listBatchInfoEdit(BatchInfoEditQueryCriteria criteria);

	/**
	 * 批量修改人员信息
	 * @author YJ
	 * @date 2022/5/13 19:54
	 * @param batchInfoEditVOList
	 */
	void batchInfoEdit(List<BatchInfoEditVO> batchInfoEditVOList);


	/**
	 * 获得批量调整项目Map列表
	 * @author YJ
	 * @date 2022/5/13 15:44
	 * @param criteria
	 * @param pageable
	 * @return java.util.Map〈java.lang.String,java.lang.Object〉
	 */
	Map<String, Object> mapBatchProjectAdjust(BatchProjectAdjustQueryCriteria criteria, Pageable pageable);

	/**
	 * 获得批量调整项目List列表
	 * @author YJ
	 * @date 2022/5/13 15:44
	 * @param criteria
	 * @return java.util.List〈com.healthappy.modules.system.domain.vo.BatchProjectAdjustVO〉
	 */
	List<BatchProjectAdjustVO> listBatchProjectAdjust(BatchProjectAdjustQueryCriteria criteria);

	/**
	 * 批量调整项目
	 * @author YJ
	 * @date 2022/5/14 15:45
	 * @param dto
	 */
	void batchProjectAdjust(BatchProjectAdjustDTO dto);

	/**
	 * 获得批量结果录入Map列表
	 * @author YJ
	 * @date 2022/5/13 15:44
	 * @param criteria
	 * @param pageable
	 * @return java.util.Map〈java.lang.String,java.lang.Object〉
	 */
	Map<String, Object> mapBatchResultEntry(BatchResultEntryQueryCriteria criteria, Pageable pageable);

	/**
	 * 获得批量结果录入List列表
	 * @author YJ
	 * @date 2022/5/13 15:44
	 * @param criteria
	 * @return java.util.List〈com.healthappy.modules.system.domain.vo.BatchProjectAdjustVO〉
	 */
	List<BatchResultEntryVO> listBatchResultEntry(BatchResultEntryQueryCriteria criteria);


	/**
	 * 批量结果录入
	 * @author YJ
	 * @date 2022/5/14 17:18
	 * @param vo
	 */
	void batchResultEntry(BatchResultVO vo);

	/**
	 * 根据编号删除体检人员
	 * @author YJ
	 * @date 2022/5/12 16:12
	 * @param ids
	 * @return java.lang.Boolean
	 */
	Boolean delPatientByIds(String ids);


	/**
	 * 获取人员的条码列表
	 * @author YJ
	 * @date 2022/5/12 16:10
	 * @param paId
	 * @return java.util.List〈BarcodeVO〉
	 */
	List<BarcodeVO> getBarcodeByPaId(String paId);
}
