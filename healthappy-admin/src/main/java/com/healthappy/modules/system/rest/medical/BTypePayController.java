package com.healthappy.modules.system.rest.medical;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BTypePay;
import com.healthappy.modules.system.domain.vo.BTypePayVo;
import com.healthappy.modules.system.service.BTypePayService;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA. Created by yutang 2021/9/2 0002  14:28 Description:
 */
@Api(tags = "体检管理：缴费类型接口")
@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/BTypePay")
public class BTypePayController {

  private final BTypePayService bTypePayService;

  @Log("新增|修改缴费类型")
  @ApiOperation("新增|修改缴费类型，权限码：BTypePay:add")
  @PostMapping
  @PreAuthorize("@el.check('BTypePay:add')")
  @Klock
  public R saveOrUpdate(@Validated @RequestBody BTypePayVo bTypePayVo){
    if(checkName(bTypePayVo)){
      return R.error(500,"存在重复的类型名称！");
    };
    BTypePay bTypePay = new BTypePay();
    BeanUtil.copyProperties(bTypePayVo,bTypePay);
    if(bTypePayService.saveOrUpdate(bTypePay)) return R.ok();
    return R.error(500,"方法异常");
  }

  @Log("所有缴费类型列表")
  @ApiOperation(value = "所有缴费类型列表，权限码：BTypePay:list")
  @GetMapping
  @PreAuthorize("@el.check('BTypePay:list')")
  public R listAll(){
    return R.ok(bTypePayService.list());
  }

  /**
   * 检查重复名称
   * @param bTypePayVo
   * @return
   */
  boolean checkName(BTypePayVo bTypePayVo){
    QueryWrapper<BTypePay> queryWrapper = new QueryWrapper<>();
    queryWrapper.eq("name",bTypePayVo.getName());
    if(bTypePayVo.getId() == null || bTypePayVo.getId() == 0){//修改
      queryWrapper.ne("id",bTypePayVo.getId());
    }
    BTypePay one = bTypePayService.getOne(queryWrapper);
    if(one != null){
      return true;
    }
    return false;
  }

}
