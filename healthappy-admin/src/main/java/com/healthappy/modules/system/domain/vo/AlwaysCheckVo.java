package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class AlwaysCheckVo {

    @NotBlank(message = "流水号不能为空")
    @ApiModelProperty("流水号")
    private String id;

    @ApiModelProperty("综述")
    private String comment;

    @ApiModelProperty("建议")
    private String suggest;

    @ApiModelProperty("职普合一：普通综述")
    private String commentPt;

    @ApiModelProperty("职普合一：普通建议")
    private String suggestPt;

    @ApiModelProperty("职普合一：职业综述")
    private String commentZy;

    @ApiModelProperty("职普合一：职业建议")
    private String suggestZy;


    @ApiModelProperty("职业病结论")
    private String commentZ;

    @ApiModelProperty("职业病建议")
    private String suggestZ;

    @ApiModelProperty("禁忌")
    private Integer pjTaboo;

    @ApiModelProperty("疑似")
    private Integer pjSuspicion;

    @ApiModelProperty("其他复查")
    private Integer pjReview;

    @ApiModelProperty("其他疾病或异常")
    private Integer pjUnusual;

    @ApiModelProperty("复查")
    private Integer pjRelate;

    /** 1: 审核 2:撤销 */
    @ApiModelProperty("1: 审核 2:撤销")
    private Integer type;

    @ApiModelProperty("未见异常")
    private Integer pjRemove;

    @NotNull(message = "分类不能为空")
    @ApiModelProperty("分类-分组Id")
    private Integer groupId;

    @ApiModelProperty("正常/合格")
    private Integer pjQualified;

    @ApiModelProperty("异常/不合格")
    private Integer pjUnqualified;

    /**
     * 一键审核, 0不审核，1总检并审核，默认为0
     * YJ 2022年3月8日 09:41:49
     */
    @ApiModelProperty("一键审核, 0不审核，1总检并审核，默认为0")
    private Integer oneClickVerify = 0;

}
