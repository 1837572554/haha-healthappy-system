package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 性别表
 * @author FGQ
 * @date 2021-06-23
 */
@Data
@TableName("B_Sex")
@ApiModel("性别表")
public class BSex implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
     * id
     */
    @ApiModelProperty("id")
    private Long id;

    /**
     * 代码
     */
    @ApiModelProperty("代码")
    private String type;

    /**
     * 性别
     */
    @ApiModelProperty("性别")
    private String name;
}
