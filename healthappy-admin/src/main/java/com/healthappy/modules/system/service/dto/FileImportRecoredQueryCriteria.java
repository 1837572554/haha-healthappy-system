package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class FileImportRecoredQueryCriteria {

    @Query
    private Long createBy;

    @Query
    @ApiModelProperty("是否成功")
    private Boolean isSuccess = true;


    @Query
    @ApiModelProperty("文件名称")
    private String fileName;
}
