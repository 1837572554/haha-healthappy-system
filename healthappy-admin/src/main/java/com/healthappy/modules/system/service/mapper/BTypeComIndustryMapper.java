package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BTypeComIndustry;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 企业从事行业类型表
 * @author sjc
 * @date 2021-08-30
 */
@Mapper
@Repository
public interface BTypeComIndustryMapper extends CoreMapper<BTypeComIndustry> {

    List<BTypeComIndustry> getTreeBTypeComIndustry();


    List<BTypeComIndustry> getBTypeComIndustryByCode(@Param("code") Long code);

    List<BTypeComIndustry> getBTypeComIndustryByHiCode(@Param("hiCode") Long code);

}
