package com.healthappy.modules.system.rest.common;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BTypeItem;
import com.healthappy.modules.system.domain.vo.BTypeItemVo;
import com.healthappy.modules.system.service.BTypeItemService;
import com.healthappy.modules.system.service.dto.BTypeItemQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @description 项目类型 控制器
 * @author sjc
 * @date 2021-08-3
 */
@Slf4j
@Api(tags = "默认基础设置：项目类型")
@RestController
@AllArgsConstructor
@RequestMapping("/BTypeItem")
public class BTypeItemController {

    private final BTypeItemService bTypeItemService;

    @Log("分页查询项目类型数据")
    @ApiOperation("分页查询项目类型数据，权限码：BTypeItem:list")
    @GetMapping
    public R listPage(BTypeItemQueryCriteria criteria, Pageable pageable) {
        return R.ok(bTypeItemService.queryAll(criteria, pageable));
    }

    @Log("新增|修改项目类型")
    @ApiOperation("新增|修改项目类型，权限码：admin")
    @PostMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BTypeItemVo resources) {
        //如果名称重复
        if(checkBTypeItemName(resources)){
            return R.error(500,"项目类型名称已经存在");
        }

        BTypeItem bTypeItem = new BTypeItem();
        BeanUtil.copyProperties(resources,bTypeItem);
        if(bTypeItemService.saveOrUpdate(bTypeItem))
            return R.ok();
        return R.error(500,"方法异常");
    }

    @Log("删除项目类型")
    @ApiOperation("删除项目类型，权限码：admin")
    @DeleteMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R remove(@RequestBody Long id) {
        if(bTypeItemService.deleteById(id))
            return R.ok();
        return R.error(500,"方法异常");
    }

    /**
     * 用于判断名称是否存在
     * @param resources
     * @return
     */
    private Boolean checkBTypeItemName(BTypeItemVo resources) {
        //名称相同  id不同的情况下能查询到数据，那么就是名称重复
        return bTypeItemService.lambdaQuery().eq(BTypeItem::getName,resources.getName())
                .ne(ObjectUtil.isNotNull(resources.getId()),BTypeItem::getId,resources.getId()).count() > 0;

    }
}
