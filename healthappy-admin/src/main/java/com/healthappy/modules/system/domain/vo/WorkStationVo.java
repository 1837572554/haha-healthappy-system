package com.healthappy.modules.system.domain.vo;/**
 * @author Jevany
 * @date 2021/11/26 16:09
 * @desc
 */

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @desc: 工作站VO
 * @author: YJ
 * @date: 2021-11-26 16:09
 **/
@Data
public class WorkStationVo {
    /**
     * 工作站代码 建议区间设置大点01，别的站设06
     */
    @ApiModelProperty("工作站代码")
    private String code;

    /**
     * 是否激活（）
     */
    @ApiModelProperty("是否激活")
    private Boolean isEnable;

    /**
     * 工作站名称
     * @author YJ
     * @date 2021/11/26 15:58
     */
    @ApiModelProperty("工作站名称")
    private String name;
}
