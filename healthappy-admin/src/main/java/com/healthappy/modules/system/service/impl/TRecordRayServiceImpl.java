package com.healthappy.modules.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.TRecord;
import com.healthappy.modules.system.domain.TRecordRay;
import com.healthappy.modules.system.service.TRecordRayService;
import com.healthappy.modules.system.service.TRecordService;
import com.healthappy.modules.system.service.dto.TRecordDto;
import com.healthappy.modules.system.service.dto.TRecordQueryCriteria;
import com.healthappy.modules.system.service.dto.TRecordRayDto;
import com.healthappy.modules.system.service.dto.TRecordRayQueryCriteria;
import com.healthappy.modules.system.service.mapper.TRecordMapper;
import com.healthappy.modules.system.service.mapper.TRecordRayMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @description 职业史放射表
 * @author sjc
 * @date 2021-12-15
 */
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class TRecordRayServiceImpl extends BaseServiceImpl<TRecordRayMapper, TRecordRay> implements TRecordRayService {

    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(TRecordRayQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<TRecordRay> page = new PageInfo<TRecordRay>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", generator.convert(page.getList(), TRecordRayDto.class));
        map.put("totalElements", page.getTotal());
        return map;
    }

    @Override
    public List<TRecordRay> queryAll(TRecordRayQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(TRecordRayDto.class, criteria));
    }
}
