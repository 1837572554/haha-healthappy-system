package com.healthappy.modules.system.service.dto;

import com.healthappy.modules.system.domain.TItemHD;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 批量项目调整DTO
 * @author Jevany
 * @date 2022/5/14 15:41
 */
@Data
@ApiModel("批量项目调整DTO")
public class BatchProjectAdjustDTO implements Serializable {

	/** 流水号列表  */
	@NotEmpty(message = "流水号列表不能为空")
	@ApiModelProperty("流水号列表")
	List<String> paIdList;

	/** 组合项目列表 */
	@NotEmpty(message = "组合项目列表不能为空")
	@ApiModelProperty("组合项目列表")
	List<TItemHD> groupList;

	/** 操作类型 1：添加 2：删除 */
	@NotNull(message = "操作类型不能为空")
	@ApiModelProperty("操作类型 1：添加 2：删除")
	Integer operationType;

	/** 付费方式：0自费 1统收 */
	@NotBlank(message = "付费方式不能为空")
	@ApiModelProperty("付费方式：0自费 1统收")
	String payType;


	private static final long serialVersionUID = 1L;
}