package com.healthappy.modules.system.service;

import cn.hutool.core.lang.tree.Tree;
import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BCommpanyGroup;
import com.healthappy.modules.system.service.dto.BCommpanyGroupQueryCriteria;
import com.healthappy.modules.system.service.dto.BCommpanyGroupQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @description 集团表
 * @author FGQ
 * @date 2021-06-17
 */
public interface BCommpanyGroupService extends BaseService<BCommpanyGroup> {


    Map<String, Object> queryAll(BCommpanyGroupQueryCriteria criteria, Pageable pageable);

    /**
     * 查询数据分页
     * @param criteria 条件
     * @return Map<String, Object>
     */
    List<BCommpanyGroup> queryAll(BCommpanyGroupQueryCriteria criteria);

}
