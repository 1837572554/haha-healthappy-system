package com.healthappy.modules.system.domain.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Pattern;

@Data
public class ReturnVo {

    private Long id;

    /**省*/
    private String province;

    /**市*/
    private String city;

    /**区*/
    private String district;

    /** 地址 */
    @Length(min = 5,max = 50,message = "详细地址不能少于5个字,不能多余50个字")
    private String address;

    /**联系电话*/
    @Pattern(regexp="^1(3[0-9]|4[01456879]|5[0-35-9]|6[2567]|7[0-8]|8[0-9]|9[0-35-9])\\d{8}$", message="手机号格式有误")
    private String contactNumber;

    /**联系人*/
    @Length(min = 2,max = 5,message = "联系人不能小于2个字,超过五个字")
    private String linkman;
}
