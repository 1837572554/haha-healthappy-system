package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 病种组合-子项
 * @author Jevany
 * @date 2022/3/9 16:29
 */
@ApiModel("病种组合-子项")
@Data
public class SicknessZHVo {
    /**
     * 病种id
     */
    @NotBlank(message = "病种id不能为空")
    @ApiModelProperty("病种id")
    private String sicknessId;

    /**
     * 是否包含
     */
    @NotNull(message = "必须指定是否包含")
    @ApiModelProperty("是否包含")
    private Long containsFlag;


    @ApiModelProperty("病种规则ID")
    private String sicknessRuleId;
}
