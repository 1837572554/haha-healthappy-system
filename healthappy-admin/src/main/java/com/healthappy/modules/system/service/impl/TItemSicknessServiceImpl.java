package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.TItemSickness;
import com.healthappy.modules.system.domain.vo.ProjectCheckVo;
import com.healthappy.modules.system.service.TItemSicknessService;
import com.healthappy.modules.system.service.dto.ItemSicknessMatchDto;
import com.healthappy.modules.system.service.mapper.TItemSicknessMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class TItemSicknessServiceImpl extends BaseServiceImpl<TItemSicknessMapper, TItemSickness> implements TItemSicknessService {

    @Override
    public void installVo(ProjectCheckVo projectCheckVo) {
        //一个组合项目的病种保存
        List<TItemSickness> itemSicknessList = projectCheckVo.getItemSicknessList();
        //组合项下先删除病种信息，再重新增加
        this.lambdaUpdate().eq(TItemSickness::getPaId, projectCheckVo.getCheckBItemHdDto().getPaId()).eq(TItemSickness::getGroupId, projectCheckVo.getCheckBItemHdDto().getGroupId()).remove();
        if (CollUtil.isNotEmpty(itemSicknessList)) {
            //批量保存项目
            this.saveBatch(itemSicknessList);
        }
    }

    @Override
    public List<ItemSicknessMatchDto> listResultSickness(String paId, String itemId) {
        if (StrUtil.isBlank(paId) || StrUtil.isBlank(itemId)) {
            return Collections.emptyList();
        }
        return baseMapper.listResultSickness(paId, itemId);

    }
}
