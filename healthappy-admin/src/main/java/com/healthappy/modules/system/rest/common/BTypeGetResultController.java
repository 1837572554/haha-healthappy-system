package com.healthappy.modules.system.rest.common;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BTypeGetResult;
import com.healthappy.modules.system.domain.vo.BTypeGetResultVo;
import com.healthappy.modules.system.service.BTypeGetResultService;
import com.healthappy.modules.system.service.dto.BTypeGetResultQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description 结果获取方式 控制器
 * @author sjc
 * @date 2021-08-3
 */
@Slf4j
@Api(tags = "默认基础数据：结果获取方式")
@RestController
@AllArgsConstructor
@RequestMapping("/BTypeGetResult")
public class BTypeGetResultController {

    private final BTypeGetResultService bTypeGetResultService;

    @Log("分页查询结果获取方式")
    @ApiOperation("分页查询结果获取方式，权限码：BTypeGetResult:list")
    @GetMapping
    public R listPage(BTypeGetResultQueryCriteria criteria, Pageable pageable) {
        return R.ok(bTypeGetResultService.queryAll(criteria, pageable));
    }

    @Log("新增|修改结果获取方式")
    @ApiOperation("新增|修改结果获取方式，权限码：admin")
    @PostMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BTypeGetResultVo resources) {
        //如果名称重复
        if(checkBTypeGetResultName(resources)) {
            return R.error(500,"结果获取名称已经存在");
        }
        BTypeGetResult bTypeItem = new BTypeGetResult();
        BeanUtil.copyProperties(resources,bTypeItem);
        if(bTypeGetResultService.saveOrUpdate(bTypeItem))
            return R.ok();
        return R.error(500,"方法异常");
    }

    @Log("删除结果获取方式")
    @ApiOperation("删除结果获取方式，权限码：admin")
    @DeleteMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R remove(@RequestParam("id") Long id) {
        if(bTypeGetResultService.deleteById(id))
            return R.ok();
        return R.error(500,"方法异常");
    }

    /**
     * 用于判断名称是否存在
     * @param resources
     * @return
     */
    private Boolean checkBTypeGetResultName(BTypeGetResultVo resources) {
        //名称相同  id不同的情况下能查询到数据，那么就是名称重复
        return bTypeGetResultService.lambdaQuery().eq(BTypeGetResult::getName,resources.getName())
                .ne(ObjectUtil.isNotNull(resources.getId()),BTypeGetResult::getId,resources.getId()).count() > 0;

    }
}
