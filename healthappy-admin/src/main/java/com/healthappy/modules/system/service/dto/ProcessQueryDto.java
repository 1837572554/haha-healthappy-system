package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.*;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Jevany
 * @date 2022/1/26 10:45
 * @desc 体检进度查询 返回数据
 */
@ApiModel("体检进度查询 返回数据")
@Data
public class ProcessQueryDto implements Serializable {
    /** 体检号 */
    @ApiModelProperty("体检号")
    private String paId;

    /** 体检分类 */
    @ApiModelProperty("体检分类")
    private String peTypeHdName;

    /** 体检类别 */
    @ApiModelProperty("体检类别")
    private String peTypeDtName;

    /** 姓名 */
    @ApiModelProperty("姓名")
    private String name;

    /** 性别 */
    @ApiModelProperty("性别")
    private String sex;

    /** 年龄 */
    @ApiModelProperty("年龄")
    private String age;

    /** 单位 */
    @ApiModelProperty("单位")
    private String companyName;

    /** 部门 */
    @ApiModelProperty("部门")
    private String deptName;

    /** 工种名称 */
    @ApiModelProperty("工种名称")
    private String jobName;

    /** 是否完成 */
    @ApiModelProperty(value = "是否完成",hidden = true)
    private Integer finished;

    /** 是否总检 */
    @ApiModelProperty(value = "是否总检",hidden = true)
    private Integer conclusion;

    /** 是否审核 */
    @ApiModelProperty(value = "是否审核",hidden = true)
    private Integer verify;

    /** 是否打印 */
    @ApiModelProperty(value = "是否打印",hidden = true)
    private Integer print;
}