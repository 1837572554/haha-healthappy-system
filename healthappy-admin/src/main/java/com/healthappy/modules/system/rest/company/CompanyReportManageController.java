package com.healthappy.modules.system.rest.company;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.TCompanyRepHd;
import com.healthappy.modules.system.domain.vo.CompanyReportVO;
import com.healthappy.modules.system.service.CompanyReportManageService;
import com.healthappy.modules.system.service.dto.*;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 单位职业报告管理
 *
 * @author Jevany
 * @date 2022/2/17 10:53
 */
@Slf4j
@Api(tags = "单位管理：单位报告管理")
@RestController
@AllArgsConstructor
@RequestMapping("/OccupationReportManage")
public class CompanyReportManageController {

    private final CompanyReportManageService companyReportManageService;

    //region 单位职业报告
    @Log("单位职业报告管理-列表")
    @ApiOperation("单位职业报告管理-列表，权限码：OccupationReportManage:list")
    @PostMapping("/occupation")
    @PreAuthorize("@el.check('OccupationReportManage:list')")
    public R occupationList(@RequestBody CompanyReportQueryCriteria criteria) {
        if (checkTimeList(criteria.getTimeList())) {
            return R.error("时间段不能为空");
        }
        if (ObjectUtil.isEmpty(criteria.getPageNum()) || criteria.getPageNum() <= 0) {
            criteria.setPageNum(1);
        }
        if (ObjectUtil.isEmpty(criteria.getPageSize()) || criteria.getPageSize() <= 0) {
            criteria.setPageSize(10);
        }
        criteria.setRepType(1);
        return R.ok(companyReportManageService.getList(criteria));
    }

    @Log("单位职业报告管理-保存")
    @ApiOperation("单位职业报告管理-新增，权限码：OccupationReportManage:list")
    @PostMapping("/saveOccupation")
    @PreAuthorize("@el.check('OccupationReportManage:list')")
    @Klock
    public R saveOccupationReport(@RequestBody CompanyReportVO companyRepHdVO) {
        companyRepHdVO.setRepType(1);
        return R.ok(companyReportManageService.saveReport(companyRepHdVO));
    }

    @Log("单位职业报告管理-查询人员列表")
    @ApiOperation("单位职业报告管理-查询人员列表，权限码：OccupationReportManage:list")
    @GetMapping("/getOccupationPersonList")
    @PreAuthorize("@el.check('OccupationReportManage:list')")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "单位报告管理 人员列表对象",response = CompanyReportPersonDTO.class)
    })
    public R getOccupationPersonList(@Validated CompanyReportOccupationPersonQueryCriteria criteria) {
        return R.ok(companyReportManageService.getOccupationPersonList(criteria));
    }

    //endregion 单位职业报告

    //region 单位健康报告

    @Log("单位健康报告管理-列表")
    @ApiOperation("单位健康报告管理-列表，权限码：HealthReportManage:list")
    @PostMapping("/health")
    @PreAuthorize("@el.check('HealthReportManage:list')")
    public R healthList(@RequestBody CompanyReportQueryCriteria criteria) {
        if (checkTimeList(criteria.getTimeList())) {
            return R.error("时间段不能为空");
        }
        if (ObjectUtil.isEmpty(criteria.getPageNum()) || criteria.getPageNum() <= 0) {
            criteria.setPageNum(1);
        }
        if (ObjectUtil.isEmpty(criteria.getPageSize()) || criteria.getPageSize() <= 0) {
            criteria.setPageSize(10);
        }
        criteria.setRepType(2);
        return R.ok(companyReportManageService.getList(criteria));
    }

    @Log("单位健康报告管理-新增")
    @ApiOperation("单位健康报告管理-新增，权限码：HealthReportManage:list")
    @PostMapping("/saveHealth")
    @PreAuthorize("@el.check('HealthReportManage:list')")
    @Klock
    public R saveHealthReport(@RequestBody CompanyReportVO companyRepHdVO) {
        companyRepHdVO.setRepType(2);
        return R.ok(companyReportManageService.saveReport(companyRepHdVO));
    }

    @Log("单位健康报告管理-查询人员列表")
    @ApiOperation("单位健康报告管理-查询人员列表，权限码：HealthReportManage:list")
    @GetMapping("/getHealthPersonList")
    @PreAuthorize("@el.check('HealthReportManage:list')")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "单位报告管理 人员列表对象",response = CompanyReportPersonDTO.class)
    })
    public R getHealthPersonList(@Validated CompanyReportHealthPersonQueryCriteria criteria) {
        return R.ok(companyReportManageService.getHealthPersonList(criteria));
    }

    //endregion 单位健康报告

    @Log("单位报告管理-绑定人员")
    @ApiOperation("单位报告管理-新增，权限码：OccupationReportManage:list、HealthReportManage:list")
    @PostMapping("/savePerson")
    @PreAuthorize("@el.check('OccupationReportManage:list','HealthReportManage:list')")
    @Klock
    public R savePerson(@Validated @RequestBody CompanyReportPersonSaveDTO saveDTO) {
        companyReportManageService.savePerson(saveDTO);
        return R.ok();
    }

    @Log("单位报告管理-获取报告内容")
    @ApiOperation("单位报告管理-获取报告内容，权限码：OccupationReportManage:list、HealthReportManage:list")
    @GetMapping("/getCompanyReport")
    @PreAuthorize("@el.check('OccupationReportManage:list','HealthReportManage:list')")
    public R getCompanyReport(@RequestParam String repId) {
        if (StrUtil.isBlank(repId)) {
            return R.error("报告编号不能为空");
        }
        return R.ok(companyReportManageService.getOne(Wrappers.<TCompanyRepHd>lambdaQuery().eq(TCompanyRepHd::getId, repId)));
    }

    @Log("单位报告管理-获取人员")
    @ApiOperation("单位报告管理-获取人员，权限码：OccupationReportManage:list、HealthReportManage:list")
    @PostMapping("/getPerson")
    @PreAuthorize("@el.check('OccupationReportManage:list','HealthReportManage:list')")
    public R getPerson(@Validated @RequestBody CompanyReportPersonQueryCriteria criteria) {
        if (StrUtil.isBlank(criteria.getRepId())) {
            return R.error("报告编号不能为空");
        }
        return R.ok(companyReportManageService.getPerson(criteria));
    }

    @Log("单位报告管理-报告审核")
    @ApiOperation("单位报告管理-报告审核，权限码：OccupationReportManage:list、HealthReportManage:list")
    @PostMapping("/verifyReport")
    @PreAuthorize("@el.check('OccupationReportManage:list','HealthReportManage:list')")
    @Klock
    public R verifyReport(@Validated @RequestBody CompanyReportVerifyDTO verifyDTO) {
        companyReportManageService.verifyReport(verifyDTO);
        return R.ok();
    }

    @Log("单位报告管理-报告删除")
    @ApiOperation("单位报告管理-报告删除，权限码：OccupationReportManage:list、HealthReportManage:list")
    @DeleteMapping("/delReport")
    @PreAuthorize("@el.check('OccupationReportManage:list','HealthReportManage:list')")
    @Klock
    public R delReport(@RequestBody Set<String> repIds) {
        if(companyReportManageService.removeByIds(repIds)) {
            return R.ok();
        }
        return R.error(500,"方法异常");
    }

    //region 疾病分析
    @Log("疾病分析-获取病种名单")
    @ApiOperation("疾病分析-获取病种名单，权限码：HealthReportManage:list")
    @GetMapping("/getSicknessList")
    @PreAuthorize("@el.check('HealthReportManage:list')")
    public R getSicknessList(CompanyReportPersonQueryCriteria criteria) {
        if (StrUtil.isBlank(criteria.getRepId())) {
            return R.error("编制Id不能为空");
        }
        criteria.setIsExport(false);
        return R.ok(companyReportManageService.getSicknessList(criteria));
    }

    @Log("疾病分析-导出病种名单")
    @ApiOperation("疾病分析-导出病种名单，权限码：HealthReportManage:list")
    @GetMapping("/exportSicknessList")
    @PreAuthorize("@el.check('HealthReportManage:list')")
    @Klock
    public void exportSicknessList(HttpServletResponse response, CompanyReportPersonQueryCriteria criteria) throws IOException {
        if (StrUtil.isBlank(criteria.getRepId())) {
            throw new BadRequestException("编制Id不能为空");
        }
        criteria.setIsExport(true);
        Map<String, Object> sicknessList = companyReportManageService.getSicknessList(criteria);
        companyReportManageService.exportSicknessList((List<CompanyRepSicknessListDTO>) sicknessList.get("content"), response);
    }

    @Log("疾病分析-获取病种人员名单")
    @ApiOperation("疾病分析-获取病种人员名单，权限码：HealthReportManage:list")
    @GetMapping("/getSicknessPersonList")
    @PreAuthorize("@el.check('HealthReportManage:list')")
    public R getSicknessPersonList(CompanyReportSicknessPersonListQueryCriteria criteria) {
        if (StrUtil.isBlank(criteria.getRepId())) {
            return R.error("编制Id不能为空");
        }
        if (CollUtil.isEmpty(criteria.getSicknessIdList())) {
            return R.error("病种编号不能为空");
        }
        if (ObjectUtil.isEmpty(criteria.getPageNum()) || criteria.getPageNum() <= 0) {
            criteria.setPageNum(1);
        }
        if (ObjectUtil.isEmpty(criteria.getPageSize()) || criteria.getPageSize() <= 0) {
            criteria.setPageSize(10);
        }
        criteria.setIsExport(false);
        return R.ok(companyReportManageService.getSicknessPersonList(criteria));
    }

    @Log("疾病分析-导出病种人员名单")
    @ApiOperation("疾病分析-导出病种人员名单，权限码：HealthReportManage:list")
    @GetMapping("/exportSicknessPersonList")
    @PreAuthorize("@el.check('HealthReportManage:list')")
    @Klock
    public void exportSicknessPersonList(HttpServletResponse response, CompanyReportSicknessPersonListQueryCriteria criteria) throws IOException {
        if (StrUtil.isBlank(criteria.getRepId())) {
            throw new BadRequestException("编制Id不能为空");
        }
        if (CollUtil.isEmpty(criteria.getSicknessIdList())) {
            throw new BadRequestException("病种编号不能为空");
        }
        criteria.setIsExport(true);
        Map<String, Object> sicknessList = companyReportManageService.getSicknessPersonList(criteria);
        companyReportManageService.exportSicknessPersonList((List<CompanyReportSicknessPersonListDTO>) sicknessList.get("content"), response);
    }
    //endregion 疾病分析

    /**
     * 检测时间段，空或对象不到2个则返回真，正常则为假
     */
    private boolean checkTimeList(List<Timestamp> timeList) {
        if (CollUtil.isEmpty(timeList) || timeList.size() != 2) {
            return true;
        }
        return false;
    }
}
