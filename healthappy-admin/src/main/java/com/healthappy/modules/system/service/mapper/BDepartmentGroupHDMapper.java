package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BDepartmentGroupHD;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @description 分组表
 * @author sjc
 * @date 2021-06-24
 */
@Mapper
@Repository
public interface BDepartmentGroupHDMapper extends CoreMapper<BDepartmentGroupHD> {

	/**
	 * 获得单位下最大的单位分组编号，对象中只有编号字段有值
	 * @param companyId 公司编号
	 * @param tenantId  租户编号
	 * @return 获得单位下最大的单位分组编号，对象中只有编号字段有值
	 */
	BDepartmentGroupHD getMaxDepartmentGroup(@Param("companyId") String companyId, @Param("tenantId") String tenantId);

	/**
	 * 拷贝单位部门分组
	 * @author YJ
	 * @date 2022/4/29 17:22
	 * @param companyId 单位编号
	 * @param departmentId 部门编号
	 * @param departmentGroupId 部门分组编号
	 * @param newDepartmentGroupId 新的部门分组编号
	 */
	void copyDepartmentGroup(@Param("companyId") String companyId, @Param("departmentId") String departmentId,
			@Param("departmentGroupId") String departmentGroupId,
			@Param("newDepartmentGroupId") String newDepartmentGroupId);
}
