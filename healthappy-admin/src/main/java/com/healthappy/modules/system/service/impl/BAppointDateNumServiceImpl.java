package com.healthappy.modules.system.service.impl;

import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.BAppointDateNum;
import com.healthappy.modules.system.service.BAppointDateNumService;
import com.healthappy.modules.system.service.mapper.BAppointDateNumMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Jevany
 * @date 2022/5/7 0:09
 */
@AllArgsConstructor
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BAppointDateNumServiceImpl extends BaseServiceImpl<BAppointDateNumMapper, BAppointDateNum>
		implements BAppointDateNumService {

}