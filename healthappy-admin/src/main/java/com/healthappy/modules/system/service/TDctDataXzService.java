package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TDctDataXz;

/**
 * @author Jevany
 * @date 2021/12/2 15:43
 * @desc 电测听修正数据服务
 */
public interface TDctDataXzService extends BaseService<TDctDataXz> {

}
