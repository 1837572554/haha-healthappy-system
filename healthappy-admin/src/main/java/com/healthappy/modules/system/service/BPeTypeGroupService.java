package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BPeTypeGroup;

/**
 * @author yanjun
 * @description
 * @date 2021/9/10
 */
public interface BPeTypeGroupService extends BaseService<BPeTypeGroup> {
}
