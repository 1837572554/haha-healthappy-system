package com.healthappy.modules.system.rest.dataquery;

import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.service.CKPatientService;
import com.healthappy.modules.system.service.dto.QueryDataCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * @description 数据查询 控制器
 * @author FGQ
 * @date 2021-10-27
 */
@Slf4j
@Api(tags = "数据查询：数据查询")
@RestController
@AllArgsConstructor
@RequestMapping("/dataQuery")
public class DataQueryController {

    private final CKPatientService ckPatientService;

    @Log("自定义查询")
    @ApiOperation("自定义查询")
    @GetMapping("custom-query")
    @PreAuthorize("@el.check('dataQuery:list')")
    public R page(@Validated QueryDataCriteria criteria,Pageable pageable) {
        return R.ok(ckPatientService.getPage(criteria,pageable));
    }

    @Log("自定义查询-导出")
    @ApiOperation("自定义查询-导出")
    @GetMapping("custom-query-report")
    @PreAuthorize("@el.check('dataQuery:upload')")
    @Klock
    public void reportExecl(@Validated QueryDataCriteria criteria,HttpServletResponse response){
         ckPatientService.reportExecl(criteria,response);
    }
}
