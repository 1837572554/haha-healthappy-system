package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.*;
import com.healthappy.modules.system.service.*;
import com.healthappy.modules.system.service.dto.DataQueryCriteria;
import com.healthappy.modules.system.service.dto.DataQueryDto;
import com.healthappy.modules.system.service.dto.TPatientDto;
import com.healthappy.modules.system.service.mapper.TPatientMapper;
import com.healthappy.utils.FileUtil;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author sjc
 * @date 2022-1-20
 */
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class ProfessionalInspectionServerImpl extends BaseServiceImpl<TPatientMapper, TPatient> implements ProfessionalInspectionServer {

    private final IGenerator generator;
    private final TPatientService patientService;
    private final TPatientZService patientzService;
    private final BCompanyService bCompanyService;
    private final BDepartmentService bDepartmentService;
    private final BDepartmentGroupHDService bDepartmentGroupHDService;
    private final BPeTypeHDService bPeTypeHDService;
    private final BPeTypeDTService bPeTypeDTService;
    private final BPeTypeJobService bPeTypeJobService;

    @Override
    public List<DataQueryDto> queryAll(DataQueryCriteria criteria) {
        return buildList(generator.convert(patientService.list(buildQueryCriteria(criteria)), DataQueryDto.class));
    }


    @Override
    public Map<String, Object> queryAll(DataQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<DataQueryDto> page = new PageInfo<DataQueryDto>(patientService.list(buildQueryCriteria(criteria)));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", buildList(generator.convert(page.getList(), DataQueryDto.class)));
        map.put("totalElements", page.getTotal());
        return map;
    }

    /**
     * 导出数据
     */
    @Override
    public void download(DataQueryCriteria criteria, HttpServletResponse response) throws IOException {
        if(ObjectUtil.isNotNull(criteria.getDateType())){
            if(ObjectUtil.isNull(criteria.getStartTime()) || ObjectUtil.isNull(criteria.getEndTime())){
                throw  new BadRequestException("查询时间条件有误");
            }
        }
        List<DataQueryDto> dtoList = queryAll(criteria);

        List<Map<String, Object>> list = new ArrayList<>();
        for (DataQueryDto dataQueryDto : dtoList) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put(" 流水号", dataQueryDto.getId());
            map.put(" 姓名", dataQueryDto.getName());
            map.put(" 性别", dataQueryDto.getSexText());
            map.put(" 年龄", dataQueryDto.getAge());
            map.put(" 体检分类", dataQueryDto.getPeTypeHdName());
            map.put(" 体检类别", dataQueryDto.getPeTypeDtName());
            map.put(" 单位", dataQueryDto.getCompanyName());
            map.put(" 部门", dataQueryDto.getDepName());
            map.put(" 分组", dataQueryDto.getDepGroupName());
            map.put("工种", dataQueryDto.getJobName());
            map.put("危害因素 ",dataQueryDto.getPoisonType());
            map.put("接害工龄 ",dataQueryDto.getWorkYears());
            map.put("体检综述", dataQueryDto.getComment());
            map.put("职业结论", dataQueryDto.getCommentZy());
            map.put("处理意见 ", dataQueryDto.getRemark());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }


    private List<DataQueryDto> buildList(List<DataQueryDto> list) {
        if(CollUtil.isEmpty(list)){
            return Collections.emptyList();
        }

        //单位
        List<String> comList = list.stream().map(DataQueryDto ::getCompanyId).distinct().collect(Collectors.toList());
        List<BCompany> companyList = bCompanyService.lambdaQuery().in(CollUtil.isNotEmpty(comList),BCompany::getId, comList).list();
        //部门
        List<String> depList = list.stream().filter(c-> StrUtil.isNotEmpty(c.getDepartmentId())).map(DataQueryDto::getDepartmentId).distinct().collect(Collectors.toList());
        List<BDepartment> bDepartmentList = bDepartmentService.lambdaQuery().in(CollUtil.isNotEmpty(depList),BDepartment::getId,depList ).list();
        //分组
        List<String> depGroupList = list.stream().filter(c -> StrUtil.isNotEmpty(c.getDepartmentGroupId())).map(DataQueryDto::getDepartmentGroupId).distinct().collect(Collectors.toList());
        List<BDepartmentGroupHD> bDepartmentGroupHDList = bDepartmentGroupHDService.lambdaQuery().in(CollUtil.isNotEmpty(depGroupList),BDepartmentGroupHD::getId, depGroupList).list();

        //体检分类
        List<Long> typeHd = list.stream().filter(c -> c.getPeTypeHdId() != null).map(DataQueryDto::getPeTypeHdId).distinct().collect(Collectors.toList());
        List<BPeTypeHD> bPeTypeHDList = bPeTypeHDService.lambdaQuery().in(CollUtil.isNotEmpty(typeHd),BPeTypeHD::getId,typeHd).list();
        //体检类别
        List<Long> typeDt = list.stream().filter(c -> c.getPeTypeDtId() != null).map(DataQueryDto::getPeTypeDtId).distinct().collect(Collectors.toList());
        List<BPeTypeDT> bPeTypeDTList = bPeTypeDTService.lambdaQuery().in(CollUtil.isNotEmpty(typeDt),BPeTypeDT::getId, typeDt).list();
        //职业体检人员数据
        List<String> pzList = list.stream().map(DataQueryDto::getId).distinct().collect(Collectors.toList());
        List<TPatientZ> patientZList = patientzService.lambdaQuery().in(CollUtil.isNotEmpty(pzList),TPatientZ::getPaId, pzList).list();

        //工种
        List<String> jobList = list.stream().filter(c -> StrUtil.isNotEmpty(c.getJob())).map(DataQueryDto::getJob).distinct().collect(Collectors.toList());
        List<BPeTypeJob> bPeTypeJobList = bPeTypeJobService.lambdaQuery().in(CollUtil.isNotEmpty(jobList),BPeTypeJob::getId, jobList).list();


        list.stream().peek(e -> {
            e.setCompanyName(companyList.stream().filter(c->c.getId().equals(e.getCompanyId())).findFirst().orElse(new BCompany()).getName());
            e.setDepName(bDepartmentList.stream().filter(c->c.getId().equals(e.getDepartmentId())).findFirst().orElse(new BDepartment()).getName());
            e.setDepGroupName(bDepartmentGroupHDList.stream().filter(c->c.getId().equals(e.getDepartmentGroupId())).findFirst().orElse(new BDepartmentGroupHD()).getName());

            e.setPeTypeHdName(bPeTypeHDList.stream().filter(c->c.getId().equals(e.getPeTypeHdId())).findFirst().orElse(new BPeTypeHD()).getName());
            e.setPeTypeDtName(bPeTypeDTList.stream().filter(c->c.getId().equals(e.getPeTypeDtId())).findFirst().orElse(new BPeTypeDT()).getName());

            e.setJobName(bPeTypeJobList.stream().filter(c->c.getId().equals(e.getJob())).findFirst().orElse(new BPeTypeJob()).getName());

            TPatientZ patientZ = patientZList.stream().filter(c -> c.getPaId().equals(e.getId())).findFirst().orElse(new TPatientZ());
            e.setWorkYears(patientZ.getWorkYears());
            e.setPoisonType(patientZ.getPoisonType());
            e.setSuggestZ(patientZ.getSuggestZ());
            e.setCommentZ(patientZ.getCommentZ());
        }).collect(Collectors.toList());
        return list;
    }

    private QueryWrapper buildQueryCriteria(DataQueryCriteria criteria) {
        QueryWrapper<TPatient> wrapper = QueryHelpPlus.getPredicate(TPatient.class,criteria);

        //查询的时间类型
        if(ObjectUtil.isNotNull(criteria.getDateType())){
            String startTime = criteria.getStartTime(),endTime = criteria.getEndTime();
            switch (criteria.getDateType()){
                case 1:
                    //登记
                    wrapper.between("pe_date", startTime,endTime);
                    break;
                case 2:
                    //签到
                    wrapper.between("sign_date",startTime,endTime);
                    break;
                case 3:
                    //总检
                    wrapper.between("conclusion_date",startTime,endTime);
                    break;
                default:
                    throw new BadRequestException("参数有误");
            }
        }

        String whereSql = "";
        if(StrUtil.isNotEmpty(criteria.getPjTaboo()) && criteria.getPjTaboo().equals("1"))
        {
            whereSql+=" or pj_taboo = '1' ";
        }
        if(StrUtil.isNotEmpty(criteria.getPjRelate()) && criteria.getPjRelate().equals("1"))
        {
            whereSql+=" or pj_relate = '1' ";
        }
        if(StrUtil.isNotEmpty(criteria.getPjSuspicion()) && criteria.getPjSuspicion().equals("1"))
        {
            whereSql+=" or pj_suspicion = '1' ";
        }

        if(StrUtil.isNotEmpty(criteria.getPjReview()) && criteria.getPjReview().equals("1"))
        {
            whereSql+=" or pj_review = '1' ";
        }
        if(StrUtil.isNotEmpty(criteria.getPjUnusual()) && criteria.getPjUnusual().equals("1"))
        {
            whereSql+=" or pj_unusual = '1' ";
        }
        if(StrUtil.isNotEmpty(criteria.getPjRemove()) && criteria.getPjRemove().equals("1"))
        {
            whereSql+=" or pj_remove = '1' ";
        }
        if(StrUtil.isNotEmpty(whereSql))
        {
            String lastSql=" and id in (select pa_id from T_Patient_Z where ( 1=0 "+ whereSql+")   and pa_id=T_Patient.id )";
            wrapper.last(lastSql);
        }
        return wrapper;
    }





}
