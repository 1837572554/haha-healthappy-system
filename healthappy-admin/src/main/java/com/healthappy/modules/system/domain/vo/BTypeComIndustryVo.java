package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @description 企业从事行业类型表
 * @author sjc
 * @date 2021-08-30
 */
@Data
public class BTypeComIndustryVo {

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private Long id;

    /**
     * 从事行业名称
     */
    @ApiModelProperty("从事行业名称")
    @NotBlank(message = "名称不能为空")
    private String name;
    /**
     * 标准编码
     */
    @ApiModelProperty("标准编码")
    private String code;

    /**
     * 上级类别编码
     */
    @ApiModelProperty("上级类别编码")
    private String hiCode;
}
