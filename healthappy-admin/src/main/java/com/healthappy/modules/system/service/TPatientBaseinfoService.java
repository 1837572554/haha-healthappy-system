package com.healthappy.modules.system.service;

import com.healthappy.modules.system.domain.TPatientBaseinfo;
import com.healthappy.modules.system.service.dto.TPatientBaseinfoDto;
import com.healthappy.modules.system.service.dto.TPatientBaseinfoQueryCriteria;
import com.healthappy.modules.system.service.dto.TPatientBaseinfoSaveDtoDto;

/**
 * @author Jevany
 * @date 2022/1/20 16:49
 * @desc 人员档案信息表 服务层
 */
public interface TPatientBaseinfoService {

    /**
     * 根据身份证号或档案号查信息
     * @author YJ
     * @date 2022/1/20 16:59
     * @param criteria
     * @return com.healthappy.modules.system.domain.TPatientBaseinfo
     */
    TPatientBaseinfo getBaseinfo(TPatientBaseinfoQueryCriteria criteria);

    /**
     * 保存档案，有档案号则修改，无则保存，已经存在的则提示报错
     * @author YJ
     * @date 2022/1/20 17:00
     * @param dto
     * @return java.lang.Long 返回档案号
     */
    Long saveOrUpdateBaseinfo(TPatientBaseinfoDto dto);

    /**
     * 获得档案号或保存档案后返回号档案
     * @author YJ
     * @date 2022/1/20 19:36
     * @param dto
     * @return java.lang.Long 档案号 如果身份证为空，则返回0
     */
    Long getPNo(TPatientBaseinfoSaveDtoDto dto);
}