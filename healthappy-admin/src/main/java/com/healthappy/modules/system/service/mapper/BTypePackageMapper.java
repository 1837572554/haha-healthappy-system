package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BTypePackage;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 套餐类型
 * @author sjc
 * @date 2021-08-24
 */
@Mapper
@Repository
public interface BTypePackageMapper extends CoreMapper<BTypePackage> {

}
