package com.healthappy.modules.system.service;

import com.healthappy.modules.system.service.dto.IntegratedManageDTO;
import com.healthappy.modules.system.service.dto.IntegratedManageQueryCriteria;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author Jevany
 * @date 2022/2/8 15:50
 * @desc 综合管理 服务层
 */
public interface IntegratedManageService {
    /**
     * 获得综合管理列表
     * @author YJ
     * @date 2022/2/8 15:41
     * @param criteria
     * @return java.util.List〈com.healthappy.modules.system.domain.TPatient〉
     */
    Map<String, Object> getIntegratedManageList(IntegratedManageQueryCriteria criteria);

    /**
     * 导出综合管理数据
     * @author YJ
     * @date 2022/2/8 18:05
     * @param all IntegratedManageDTO对象列表
     * @param response
     */
    void export(List<IntegratedManageDTO> all, HttpServletResponse response) throws IOException;
}