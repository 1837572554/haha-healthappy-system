package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.UsersDepts;

import java.util.Set;

/**
 * @author yanjun
 * @description
 * @date 2021/11/3
 */
public interface UsersDeptsService extends BaseService<UsersDepts> {
    /**
     * 根据用户编号获取对应的数据
     * @param userId 用户编号
     * @return
     */
    Set<UsersDepts> findByUserId(Long userId);
}
