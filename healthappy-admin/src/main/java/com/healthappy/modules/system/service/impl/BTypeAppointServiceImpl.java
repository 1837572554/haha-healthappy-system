package com.healthappy.modules.system.service.impl;

import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.BTypeAppoint;
import com.healthappy.modules.system.service.BTypeAppointService;
import com.healthappy.modules.system.service.mapper.BTypeAppointMapper;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA. Created by yutang 2021/9/8 0008  9:19 Description:
 */
@Service
public class BTypeAppointServiceImpl extends
    BaseServiceImpl<BTypeAppointMapper, BTypeAppoint> implements BTypeAppointService{

}
