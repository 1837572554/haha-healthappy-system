package com.healthappy.modules.system.rest.print.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Author: wukefei
 * @Date: Created in 2022/4/28 12:03
 * @Description:
 * @Version: 1.0
 *
 */
@ApiModel("预约打印")
@Data
public class AppointInformationVo {
	/** 流水号列表 */
	@ApiModelProperty("预约号")
	private List<String> appointIds;

	/** 用于接收的Socket的用户编号 */
	@ApiModelProperty("用于接收的Socket的用户编号")
	private String acceptSocketUserId;

}
