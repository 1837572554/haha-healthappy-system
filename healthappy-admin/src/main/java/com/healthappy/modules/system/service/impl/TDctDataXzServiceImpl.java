package com.healthappy.modules.system.service.impl;

import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.TDctDataXz;
import com.healthappy.modules.system.service.TDctDataXzService;
import com.healthappy.modules.system.service.mapper.TDctDataXzMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @desc: 电测听修正数据服务 实现
 * @author: YJ
 * @date: 2021-12-02 15:49
 **/
@Service
@AllArgsConstructor//有自动注入的功能
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class TDctDataXzServiceImpl extends BaseServiceImpl<TDctDataXzMapper, TDctDataXz> implements TDctDataXzService {

}
