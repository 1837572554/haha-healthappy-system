package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author FGQ
 * @description t_item_sickness
 * @date 2021-11-17
 */
@Data
@TableName("T_Item_Sickness")
@RenewLog
public class TItemSickness implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 受检者id(t_patient)
     */
    @TableField(value = "pa_id")
    @NotBlank(message = "受检者ID不能为空")
    @ApiModelProperty("受检者id(t_patient)")
    private String paId;

    /**
     * 项目表id(b_item)
     */
    @ApiModelProperty("项目表id(b_item)")
    private String itemId;

    /**
     * 项目组合主表(b_group_hd)
     */
    @NotBlank(message = "项目组合ID不能为空")
    @ApiModelProperty("项目组合主表(b_group_hd)")
    private String groupId;

    /**
     * 病种id(b_sickness)
     */
    @ApiModelProperty("病种id(b_sickness)")
    private String sicknessId;

    /**
     * 是否危急值 0不是 1是,默认0
     */
    @ApiModelProperty("是否危急值 0不是 1是,默认0")
    private String isCritical = "0";

    /**
     * create_time
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT)
    private Timestamp createTime;
}
