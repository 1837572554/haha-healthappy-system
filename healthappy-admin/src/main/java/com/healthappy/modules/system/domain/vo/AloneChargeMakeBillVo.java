package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 票据打印
 * @author Jevany
 * @date 2022/4/15 17:16
 */
@Data
public class AloneChargeMakeBillVo {
    /** 流水号 */
    @NotBlank(message = "流水号不能为空")
    @ApiModelProperty("流水号")
    private String paId;

    /**
     * 票据类型
     */
    @NotNull(message = "票据类型不能为空")
    @ApiModelProperty(value = "票据类型")
    private String receiptType;

    /**
     * 收据号
     */
    @ApiModelProperty(value = "票据号")
    private String receipt;

    /**
     * 增长序列
     */
    @ApiModelProperty("增长序列")
    private List<Integer> addSequence;
}
