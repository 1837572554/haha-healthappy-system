package com.healthappy.modules.system.rest.medical;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BCompany;
import com.healthappy.modules.system.domain.BDepartment;
import com.healthappy.modules.system.domain.BDepartmentGroupHD;
import com.healthappy.modules.system.domain.vo.BCompanyVo;
import com.healthappy.modules.system.service.BCompanyService;
import com.healthappy.modules.system.service.BDepartmentGroupHDService;
import com.healthappy.modules.system.service.BDepartmentService;
import com.healthappy.modules.system.service.dto.BCompanyQueryCriteria;
import com.healthappy.utils.R;
import com.healthappy.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @author FGQ
 * @description 单位管理   控制器
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "体检管理：单位管理")
@RestController
@AllArgsConstructor
@RequestMapping("/BCompany")
public class BCompanyController {

    private final BCompanyService bCompanyService;

    private final BDepartmentService bDepartmentService;

    private final BDepartmentGroupHDService bDepartmentGroupHDService;

    @Log("查询单位库全部数据")
    @ApiOperation("查询单位库全部数据，权限码：BCompany:list")
    @GetMapping("/list")
    public R list(BCompanyQueryCriteria criteria) {
        return R.ok(bCompanyService.queryAlls(criteria));
    }

    @Log("分页查询部门表")
    @ApiOperation("分页查询部门表，权限码：BCompany:list")
    @GetMapping
    public R list(BCompanyQueryCriteria criteria, Pageable pageable) {
        return R.ok(bCompanyService.queryAll(criteria, pageable));
    }

    @Log("新增|修改单位库")
    @ApiOperation("新增|修改单位库，权限码：BCompany:add")
    @PostMapping
    @PreAuthorize("@el.check('BCompany:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BCompanyVo resources) {
        BCompany bCompany = new BCompany();
        BeanUtil.copyProperties(resources, bCompany);
        //if(StringUtils.isBlank(bCompany.getId())){
        //    bCompany.setId(bCompanyService.generateCompanyKey(SecurityUtils.getTenantId()));
        //}
        if (StrUtil.isBlank(resources.getId())) {
            if (bCompanyService.count(new LambdaQueryWrapper<BCompany>().eq(BCompany::getName, resources.getName())
                    .eq(BCompany::getTenantId, SecurityUtils.getTenantId())
                    .ne(BCompany::getIsEnable,"0")) > 0) {
                throw new BadRequestException("新增单位【" + resources.getName() + "】名称已存在");
            }
        } else {
            if(bCompanyService.count(new LambdaQueryWrapper<BCompany>().ne(BCompany::getId,resources.getId())
                    .eq(BCompany::getName, resources.getName())
                    .eq(BCompany::getTenantId, SecurityUtils.getTenantId())
                    .ne(BCompany::getIsEnable,"0"))>0){
                throw new BadRequestException("编辑单位【" + resources.getName() + "】名称已存在");
            }
        }

        if (bCompanyService.saveOrUpdate(bCompany)) {
            return R.ok();
        }
        return R.error(500, "方法异常");
    }

    @Log("删除单位库")
    @ApiOperation("删除单位库，权限码：BCompany:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('BCompany:delete')")
    @Klock
    public R remove(@RequestBody Set<String> ids) {
        checkIfItExists(ids);
        if (bCompanyService.removeByIds(ids))
            return R.ok();
        return R.error(500, "方法异常");
    }

    private void checkIfItExists(Set<String> ids) {
        if (bDepartmentService.lambdaQuery().in(BDepartment::getComId, ids).count() > 0) {
            throw new BadRequestException("部门设置中存在删除的单位,请先删除部门中的数据");
        }
        if (bDepartmentGroupHDService.lambdaQuery().in(BDepartmentGroupHD::getCompanyId, ids).count() > 0) {
            throw new BadRequestException("分组设置中存在删除的单位,请先删除分组中的数据");
        }
    }
}
