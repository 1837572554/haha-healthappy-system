package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.UNoticeDept;
import com.healthappy.modules.system.service.dto.UNoticeDeptQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 公告对应科室表
 * @author sjc
 * @date 2021-09-3
 */
public interface UNoticeDeptService extends BaseService<UNoticeDept> {

    Map<String, Object> queryAll(UNoticeDeptQueryCriteria criteria, Pageable pageable);

    List<UNoticeDept> queryAll(UNoticeDeptQueryCriteria criteria);

    boolean deleteByUnoticeId(String id);
}
