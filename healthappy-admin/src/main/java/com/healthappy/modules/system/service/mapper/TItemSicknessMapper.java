package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TItemSickness;
import com.healthappy.modules.system.service.dto.ItemSicknessMatchDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 关联表
 * @author FGQ
 * @date 2021-06-17
 */
@Mapper
@Repository
public interface TItemSicknessMapper extends CoreMapper<TItemSickness> {

    /**
     * 获取结果（已经保存的）对应的病种数据
     * @author YJ
     * @date 2022/3/10 11:38
     * @param paId 流水号
     * @param itemId 明细项编号
     * @return java.util.List〈com.healthappy.modules.system.service.dto.ItemSicknessMatchDto〉
     */
    List<ItemSicknessMatchDto> listResultSickness(@Param("paId") String paId, @Param("itemId") String itemId);

}
