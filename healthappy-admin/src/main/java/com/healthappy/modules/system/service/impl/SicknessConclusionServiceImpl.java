package com.healthappy.modules.system.service.impl;

import com.healthappy.modules.system.service.SicknessConclusionService;
import com.healthappy.modules.system.service.dto.SicknessConclusionListDto;
import com.healthappy.modules.system.service.dto.SicknessConclusionListQueryCriteria;
import com.healthappy.modules.system.service.dto.SicknessConclusionNameDto;
import com.healthappy.modules.system.service.dto.SicknessConclusionNameSexDto;
import com.healthappy.modules.system.service.mapper.SicknessConclusionMapper;
import com.healthappy.utils.FileUtil;
import com.healthappy.utils.PageUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * @author Jevany
 * @date 2022/1/21 14:36
 * @desc 阳性结论名单 服务实现层
 */
@AllArgsConstructor
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class SicknessConclusionServiceImpl implements SicknessConclusionService {

    private final SicknessConclusionMapper sicknessConclusionMapper;

    @Override
    public Map<String,Object> getSicknessNameList(SicknessConclusionListQueryCriteria criteria) {
        //总条数、数据
        List<SicknessConclusionNameDto> sicknessNameList = sicknessConclusionMapper.getSicknessNameList(criteria);
        //总条数
        Integer totalCount=sicknessNameList.size();

        Integer[] pageBase=null;
        if(criteria.getIsExport().equals(false)){
            //计算分页-页数
            pageBase = PageUtil.toStartEnd(criteria.getPageNum(), criteria.getPageSize(), sicknessNameList.size());
            //获得分页数据
            sicknessNameList = sicknessNameList.subList(pageBase[0], pageBase[1]);
        }

        //所有病种对应性别数据
        List<SicknessConclusionNameSexDto> sicknessNameSex = sicknessConclusionMapper.getSicknessNameSex(criteria);
        //统计人数
        sicknessNameList.forEach(n->{
            //总人数
            n.setTotalCnt(sicknessNameSex.stream().filter(ns->ns.getSicknessName().equals(n.getSicknessName())).count());
            n.setManCnt(sicknessNameSex.stream().filter(ns->ns.getSicknessName().equals(n.getSicknessName()) && ns.getSex().equals("1")).count());
            n.setWomanCnt(sicknessNameSex.stream().filter(ns->ns.getSicknessName().equals(n.getSicknessName()) && ns.getSex().equals("2")).count());
        });

        if(criteria.getIsExport().equals(false)){
            Map<String, Object> map = new LinkedHashMap<>(5);
            map.put("content", sicknessNameList);
            map.put("totalElements", totalCount);
            map.put("totalPage", pageBase[3]);
            map.put("currentPage", pageBase[2]);
            map.put("pageSize", criteria.getPageSize());
            return map;
        } else {
            Map<String, Object> map = new LinkedHashMap<>(1);
            map.put("content", sicknessNameList);
            return map;
        }
    }

    @Override
    public void SicknessNameDownload(List<SicknessConclusionNameDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (SicknessConclusionNameDto sicknessConclusionNameDto : all) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("疾病名称",sicknessConclusionNameDto.getSicknessName());
            map.put("人数",sicknessConclusionNameDto.getTotalCnt());
            map.put("男",sicknessConclusionNameDto.getManCnt());
            map.put("女",sicknessConclusionNameDto.getWomanCnt());
            list.add(map);
        }
        FileUtil.downloadExcel(list,response);
    }

    @Override
    public Map<String,Object> getSicknessList(SicknessConclusionListQueryCriteria criteria) {
        //总条数、数据
        List<SicknessConclusionListDto> sicknessList = sicknessConclusionMapper.getSicknessList(criteria);
        //总条数
        Integer totalCount=sicknessList.size();

        Integer[] pageBase=null;
        if(criteria.getIsExport().equals(false)){
            //计算分页-页数
            pageBase = PageUtil.toStartEnd(criteria.getPageNum(), criteria.getPageSize(), sicknessList.size());
            //获得分页数据
            sicknessList = sicknessList.subList(pageBase[0], pageBase[1]);
        }

        if(criteria.getIsExport().equals(false)){
            Map<String, Object> map = new LinkedHashMap<>(5);
            map.put("content", sicknessList);
            map.put("totalElements", totalCount);
            map.put("totalPage", pageBase[3]);
            map.put("currentPage", pageBase[2]);
            map.put("pageSize", criteria.getPageSize());
            return map;
        } else {
            Map<String, Object> map = new LinkedHashMap<>(1);
            map.put("content", sicknessList);
            return map;
        }

//        return sicknessConclusionMapper.getSicknessList(criteria);
    }

    @Override
    public void SicknessListDownload(List<SicknessConclusionListDto> all, HttpServletResponse response) throws IOException {
        List<Map<String,Object>> list=new ArrayList<>();
        for (SicknessConclusionListDto sicknessConclusionListDto : all) {
            Map<String,Object> map=new HashMap<>();
            map.put("疾病名称",sicknessConclusionListDto.getSicknissName());
            map.put("体检号",sicknessConclusionListDto.getPaId());
            map.put("姓名",sicknessConclusionListDto.getName());
            map.put("性别",sicknessConclusionListDto.getSex());
            map.put("年龄",sicknessConclusionListDto.getAge());
            map.put("身份证号",sicknessConclusionListDto.getIdNo());
            map.put("电话",sicknessConclusionListDto.getPhone());
            map.put("单位",sicknessConclusionListDto.getCompanyName());
            map.put("部门",sicknessConclusionListDto.getDepartmentName());
            map.put("分组",sicknessConclusionListDto.getDepartmentGroupName());
            map.put("体检日期",sicknessConclusionListDto.getPeDate());
            map.put("签到日期",sicknessConclusionListDto.getSignDate());
            list.add(map);
        }
        FileUtil.downloadExcel(list,response);
    }


}