package com.healthappy.modules.system.domain.vo;

import com.healthappy.modules.system.domain.BPackageDT;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @description 套餐项目表
 * @author sjc
 * @date 2021-07-05
 */
@Data
public class BPackageDTVo extends BPackageDT  {

    @ApiModelProperty(value = "科室显示序号" ,hidden = true)
    private Integer deptDisp;

    @ApiModelProperty(value = "组合项目显示序号",hidden = true)
    private Integer groupDisp;
}
