package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 有害因素 Lite接口查询
 * @author YJ
 * @date 2021/12/9 14:08
 */
@Data
public class BRegisterPoisonQueryLiteCriteria {

    /** 毒害编号 */
    @ApiModelProperty("毒害编号，带上code可查询单个毒害下的列表")
    @Query(type = Query.Type.EQUAL)
    private String id;

    /**
     * 毒害名称
     */
    @Query(type = Query.Type.BETWEEN_LIKE)
    @ApiModelProperty("毒害名称")
    private List<String> poisonName;

    /**
     * 体检类别 1:上岗前，2：在岗期间，4：离岗时，5：应急
     */
    @ApiModelProperty("体检类别 1:上岗前，2：在岗期间，4：离岗时，5：应急")
    private Integer code;

    /** 性别 0：不限 1：男 2：女 */
    @ApiModelProperty("性别 0：不限 1：男 2：女")
    private Integer sex;

    /** 租户编号 */
    @ApiModelProperty(value = "租户编号" ,hidden = true)
    private String tenantId;
}
