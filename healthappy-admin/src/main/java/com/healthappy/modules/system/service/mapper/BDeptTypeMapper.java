package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BDeptType;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 部门类型
 * @author fang
 * @date 2021-06-17
 */
@Mapper
@Repository
public interface BDeptTypeMapper extends CoreMapper<BDeptType> {

}
