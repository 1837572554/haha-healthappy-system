package com.healthappy.modules.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BDept;
import com.healthappy.modules.system.domain.BGroupHd;
import com.healthappy.modules.system.domain.BItem;
import com.healthappy.modules.system.service.BDeptService;
import com.healthappy.modules.system.service.BDeptTypeService;
import com.healthappy.modules.system.service.dto.BDeptDto;
import com.healthappy.modules.system.service.dto.BDeptQueryCriteria;
import com.healthappy.modules.system.service.mapper.BDeptMapper;
import com.healthappy.modules.system.service.mapper.BGroupHdMapper;
import com.healthappy.modules.system.service.mapper.BItemMapper;
import com.healthappy.utils.CacheConstant;
import com.healthappy.utils.SecurityUtils;
import com.healthappy.utils.StringUtils;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
@CacheConfig(cacheNames = CacheConstant.B_DEPT)
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BDeptServiceImpl extends BaseServiceImpl<BDeptMapper, BDept> implements BDeptService {

    private final IGenerator generator;

    private final BItemMapper bItemMapper;
    private final BGroupHdMapper bGroupHdMapper;

    @Override
    @Cacheable
    public Map<String, Object> queryAll(BDeptQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BDept> page = new PageInfo<BDept>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", generator.convert(page.getList(), BDeptDto.class));
        map.put("totalElements", page.getTotal());
        return map;
    }

    @Override
    @Cacheable
    public List<BDept> queryAll(BDeptQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BDeptDto.class, criteria));
    }



    @Override
    public List<BDept> getDeptEnableAndExistsItem() {
        return baseMapper.getDeptEnableAndExistsItem(SecurityUtils.getTenantId());
    }

    @Override
    public String checkExistsAssociate(Set<Integer> ids) {
        StringBuilder builder=new StringBuilder();
        ids.forEach(id->{
            String tmp="";
            String deptName = Optional.ofNullable(this.lambdaQuery().eq(BDept::getId, id).select(BDept::getName).one())
                    .map(BDept::getName).orElse("");
            if(bItemMapper.selectCount(new LambdaQueryWrapper<BItem>().eq(BItem::getDeptId,id).eq(BItem::getIsEnable,"1").eq(BItem::getDelFlag,"0"))>0){
                tmp=deptName+"有关联项目，";
            }
            if(bGroupHdMapper.selectCount(new LambdaQueryWrapper<BGroupHd>().eq(BGroupHd::getDeptId,id).eq(BGroupHd::getIsEnable,"1").eq(BGroupHd::getDelFlag,"0"))>0){
                if(!"".equals(tmp)){
                    tmp+="有关联组合项目，";
                } else {
                    tmp = deptName +"有关联组合项目，";
                }
            }
            if(!"".equals(tmp)){
                builder.append(StringUtils.trimFirstAndLastChar(tmp, "，")).append("\r\n");
            }
        });

        return StringUtils.trimFirstAndLastChar(builder.toString(),"\r\n");
    }
}
