package com.healthappy.modules.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BTypeDevice;
import com.healthappy.modules.system.service.BTypeDeviceService;
import com.healthappy.modules.system.service.dto.BTypeDeviceDto;
import com.healthappy.modules.system.service.dto.BTypeDeviceQueryCriteria;
import com.healthappy.modules.system.service.mapper.BTypeDeviceMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sjc
 * @date 2021-08-3
 */
@Service
@AllArgsConstructor//有自动注入的功能
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BTypeDeviceServiceImpl extends BaseServiceImpl<BTypeDeviceMapper, BTypeDevice> implements BTypeDeviceService {

    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BTypeDeviceQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BTypeDevice> page = new PageInfo<BTypeDevice>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("content", generator.convert(page.getList(), BTypeDeviceDto.class));
        map.put("totalElements", page.getTotal());

        return map;
    }

    @Override
    public List<BTypeDevice> queryAll(BTypeDeviceQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BTypeDeviceDto.class, criteria));
    }

    @Override
    public boolean deleteById(Long id)
    {
        return baseMapper.deleteById(id)>0;
    }

}
