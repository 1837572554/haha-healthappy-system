package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BNation;

/**
 * @description 民族 服务层
 * @author FGQ
 * @date 2021-06-17
 */
public interface BNationService extends BaseService<BNation> {



}
