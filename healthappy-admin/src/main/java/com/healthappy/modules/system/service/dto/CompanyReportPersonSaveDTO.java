package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.*;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

/**
 * 单位报告人员保存对象
 * @author Jevany
 * @date 2022/2/18 19:05
 */
@Data
public class CompanyReportPersonSaveDTO implements Serializable {
    /** 单位报告编号 */
    @NotBlank(message = "单位报告编号不能为空")
    @ApiModelProperty("单位报告编号")
    private String repId;

    /** 流水号列表 */
    @ApiModelProperty("流水号列表")
    private List<String> paIds;
}