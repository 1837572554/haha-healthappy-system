package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.List;

/**
 * 单位报告管理-（新增）编辑-体检人员查询基础类
 *
 * @author Jevany
 * @date 2022/2/18 9:10
 */
@Data
public class CompanyReportPersonBase {
    /**
     * 体检时间类型
     */
    @ApiModelProperty(value = "体检时间类型 ,页面路径：companyReportPerson", position = 0)
    private Integer dateType;

    /**
     * 时间段（数组）
     */
    @ApiModelProperty(value = "时间段（数组） ",  position = 1)
    private List<Timestamp> timeList;

    /**
     * 体检分类ID
     */
    @ApiModelProperty("体检分类Id")
    private String peTypeHdId;

    /**
     * 单位ID
     */
    @NotBlank(message = "单位Id不能为空")
    @ApiModelProperty("单位Id")
    private String companyId;

    /**
     * 部门Id
     */
    @ApiModelProperty("部门Id")
    private String deptId;

    /**
     * 部门分组Id
     */
    @ApiModelProperty("部门分组Id")
    private String deptGroupId;

    /** 报告编号 */
    @ApiModelProperty("报告编号")
    private String repId;


    /**
     * 页面显示条数
     */
    @ApiModelProperty(value = "页面显示条数,默认为10", position = 101)
    private Integer pageSize = 10;

    /**
     * 页数
     */
    @NotNull(message = "页数不能为空")
    @ApiModelProperty(value = "页数,默认为1", position = 102)
    private Integer pageNum = 1;


    /**
     * 开始条数
     */
    @ApiModelProperty(value = "开始条数", hidden = true)
    private Integer startIndex = -1;

}