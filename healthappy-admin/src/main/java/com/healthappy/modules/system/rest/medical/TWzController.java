package com.healthappy.modules.system.rest.medical;

import cn.hutool.core.bean.BeanUtil;
import com.healthappy.annotation.AnonymousAccess;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.TWz;
import com.healthappy.modules.system.domain.vo.TWzVo;
import com.healthappy.modules.system.service.TWzService;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 问诊表
 * @author sjc
 * @date 2021-12-15
 */
@Validated
@Slf4j
@Api(tags = "体检管理：问诊表")
@RestController
@AllArgsConstructor
@RequestMapping("/TWz")
public class TWzController {

    private final TWzService tWzService;

    @Log("根据体检号获取对应的问诊信息")
    @ApiOperation("根据体检号获取对应的问诊信息，权限码：ProjectCheck:list")
    @GetMapping
    @PreAuthorize("@el.check('ProjectCheck:list')")
    public R query(@RequestParam("paId") String paId) {
        return R.ok(tWzService.query(paId));
    }


    @Log("新增|修改问诊信息")
    @ApiOperation("新增|修改问诊信息，权限码：TWz:add")
    @PostMapping
    @PreAuthorize("@el.check('TWz:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody TWzVo resources) {
        return R.ok(tWzService.saveOrUpdate( BeanUtil.copyProperties(resources, TWz.class)));
    }


    @Log("根据体检号进行删除")
    @ApiOperation("根据体检号进行删除，权限码：TWz:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('TWz:delete')")
    @Klock
    public R remove(@RequestParam("paId") String paId) {
        return R.ok(tWzService.remove(paId));
    }
}
