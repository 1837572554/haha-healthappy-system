package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


/**
 * @description 病种组合子项
 * @author SJC
 * @date 2021-08-3
 */
@Data
public class BSicknessZHVo {


    @ApiModelProperty("ID")
    private String id;

    /**
     * 病种组合id
     */
    @ApiModelProperty("病种组合id")
    private String sicknessZhId;

    /**
     * 病种id
     */
    @NotBlank(message = "病种id不能为空")
    @ApiModelProperty("病种id")
    private String sicknessId;

    /**
     * 是否包含
     */
    @NotNull(message = "必须指定是否包含")
    @ApiModelProperty("是否包含")
    private Long containsFlag;


    @ApiModelProperty("病种规则ID")
    private String sicknessRuleId;
}
