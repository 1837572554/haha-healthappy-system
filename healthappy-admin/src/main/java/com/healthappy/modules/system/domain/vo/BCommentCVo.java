package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class BCommentCVo {

    /**
     * id
     */
    private String id;

    /**
     * 结论
     */
    @NotBlank(message = "结论不能为空")
    @ApiModelProperty("结论")
    private String comment;

    /**
     * 是否合格
     */
    @ApiModelProperty("是否合格")
    private String isQualify;

    /** 编号 */
    @ApiModelProperty("编号")
    private Integer code;

    /**
     * 排序号
     */
    @ApiModelProperty("排序号")
    private Integer dispOrder;
}
