package com.healthappy.modules.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BArea;
import com.healthappy.modules.system.domain.BFeeType;
import com.healthappy.modules.system.service.BFeeTypeService;
import com.healthappy.modules.system.service.dto.BAreaQueryCriteria;
import com.healthappy.modules.system.service.dto.BFeeTypeQueryCriteria;
import com.healthappy.modules.system.service.mapper.BFeeTypeMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BFeeTypeServiceImpl extends BaseServiceImpl<BFeeTypeMapper, BFeeType> implements BFeeTypeService {

    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BFeeTypeQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BFeeType> page = new PageInfo<BFeeType>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", generator.convert(page.getList(), BFeeType.class));
        map.put("totalElements", page.getTotal());
        return map;
    }

    @Override
    public List<BFeeType> queryAll(BFeeTypeQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BFeeType.class, criteria));
    }
}
