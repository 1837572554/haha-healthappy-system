package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BAppointDate;
import com.healthappy.modules.system.domain.vo.AppointDateVO;
import com.healthappy.modules.system.service.dto.AppointDateDTO;
import com.healthappy.modules.system.service.dto.AppointDateOneDTO;

import java.util.List;

/**
 *
 * @author Jevany
 * @date 2022/5/7 0:03
 */
public interface BAppointDateService extends BaseService<BAppointDate> {

	/**
	 * 获取月份对应的预约号源列表
	 * @author YJ
	 * @date 2022/5/7 0:34
	 * @param year 年
	 * @param month 月
	 * @return java.util.List〈com.healthappy.modules.system.domain.vo.AppointDateVO〉预约号源列表
	 */
	List<AppointDateVO> getMonthAppointDateList(Integer year, Integer month);

	/**
	 * 批量添加号源
	 * @author YJ
	 * @date 2022/5/7 10:13
	 * @param appointDateDTO
	 */
	void batchAddition(AppointDateDTO appointDateDTO);

	/**
	 * 批量清除
	 * @author YJ
	 * @date 2022/5/7 18:11
	 * @param dateStart 开始日期
	 * @param dateEnd 结束日期
	 */
	void batchClean(String dateStart, String dateEnd);

	/**
	 * 新增或更新预约号源
	 * @author YJ
	 * @date 2022/5/9 11:53
	 * @param appointDateOneDTO
	 */
	void addOrUpdate(AppointDateOneDTO appointDateOneDTO);

	/**
	 * 公司是否能登记
	 * @author YJ
	 * @date 2022/5/10 19:10
	 * @param companyId 公司编号
	 * @param appointId 预约编号
	 * @return 只有true，否则都抛异常
	 */
	Boolean companyCanRegister(String companyId, String appointId);
}