package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class SicknessTreeDto  implements Serializable {
    @ApiModelProperty("病种ID")
    private Long id;

    @ApiModelProperty("病种名称")
    private String name;

    @ApiModelProperty("病种规则ID")
    private String sicknessRuleId;
}
