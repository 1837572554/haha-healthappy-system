package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.TItemDT;
import com.healthappy.modules.system.domain.TItemHD;
import com.healthappy.modules.system.domain.TItemSickness;
import com.healthappy.modules.system.domain.TPatient;
import com.healthappy.modules.system.service.TItemDTService;
import com.healthappy.modules.system.service.TItemHDService;
import com.healthappy.modules.system.service.TItemSicknessService;
import com.healthappy.modules.system.service.dto.DataQueryCriteria;
import com.healthappy.modules.system.service.dto.DataQueryDto;
import com.healthappy.modules.system.service.dto.TItemHDDto;
import com.healthappy.modules.system.service.dto.TItemHDQueryCriteria;
import com.healthappy.modules.system.service.mapper.TItemDTMapper;
import com.healthappy.modules.system.service.mapper.TItemHDMapper;
import com.healthappy.utils.SecurityConstants;
import com.healthappy.utils.SecurityUtils;
import com.healthappy.utils.StringUtils;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author sjc
 * @date 2021-06-29
 */
@Service
@AllArgsConstructor//有自动注入的功能
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class TItemHDServiceImpl extends BaseServiceImpl<TItemHDMapper, TItemHD> implements TItemHDService {

    private final TItemDTService tItemDTService;
    private final TItemDTMapper tItemDTMapper;
    private final TItemSicknessService itemSicknessService;

    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(TItemHDQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<TItemHD> page = new PageInfo<TItemHD>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("content", generator.convert(page.getList(), TItemHDDto.class));
        map.put("totalElements", page.getTotal());

        return map;
    }

    @Override
    public List<TItemHD> queryAll(TItemHDQueryCriteria criteria) {
        /*List<TItemHD> list = baseMapper.selectList((Wrapper<TItemHD>) QueryHelpPlus.getPredicate(TItemHD.class, criteria).orderByAsc("length(dept_id),dept_id"));
        list.stream().map(g->{
            //追加项目类型
            g.setItemTypeId(Optional.ofNullable(bGroupHdMapper.selectById(g.getGroupId())).map(BGroupHd::getType).orElse(""));
            return g;
        }).collect(Collectors.toList());
        return list;*/
        return baseMapper.all(criteria, SecurityUtils.getTenantId());
    }


    /**
     * 体检项目名单查询
     *
     * @param criteria
     * @return
     */
    @Override
    public List<DataQueryDto> getGroupPanelHd(DataQueryCriteria criteria) {
        return baseMapper.getGroupPanelHd(criteria, SecurityUtils.getTenantId());
    }

    /**
     * 根据体检号获取对应的组合项目条码，用于条码的生成
     *
     * @param paId
     * @return
     */
    @Override
    public List<TItemHD> getBarcodeGroup(String paId) {
        return baseMapper.getBarcodeGroup(paId, SecurityUtils.getTenantId());
    }

    /**
     * 更新指定条码名称对应的项目的条码
     *
     * @param barcode     条码号
     * @param barcodeName 条码名称
     * @param type        组合项目类型
     * @return
     */
    @Override
    public boolean updateBarcode(String paId, String barcode, String barcodeName, String type) {
        return baseMapper.updateBarcode(paId, barcode, barcodeName, type);
    }

    @Override
    public boolean saveOrUpdate(List<TItemHD> resources) {
        List<String> groupIdList = resources.stream().map(TItemHD::getGroupId).collect(Collectors.toList());
        for (TItemHD vo : resources) {
            saveOrUpdate(vo);
        }

        String paId = resources.get(0).getPaId();

        LambdaUpdateWrapper<TItemHD> queryWrapper = new LambdaUpdateWrapper();
        //将数据库中不在传入的项目列表中的数据删除----说明这些项目数据在前台被删除了
        queryWrapper.eq(TItemHD::getPaId, paId);

        //如果数量为0，说明前台的项目被删光了
        if (resources.size() >= 1) {
            queryWrapper.notIn(TItemHD::getGroupId, groupIdList.toArray());
        }
        if (this.remove(queryWrapper)) {
            LambdaUpdateWrapper<TItemDT> queryDTWrapper = new LambdaUpdateWrapper();
            //将数据库中不在传入的项目列表中的数据删除----说明这些项目数据在前台被删除了
            queryDTWrapper.eq(TItemDT::getPaId, paId);
            //如果数量为0，说明前台的项目被删光了
            if (resources.size() >= 1) {
                queryDTWrapper.notIn(TItemDT::getGroupId, groupIdList.toArray());
            }
            return tItemDTService.remove(queryDTWrapper);
        }
        return true;
    }


    /**
     * 根据体检号删除该人员的组合项目
     *
     * @param paId
     * @return
     */
    @Override
    public boolean deleteByPaId(String paId) {
        return baseMapper.deleteByPaId(paId, SecurityUtils.getTenantId());
    }

    /**
     * 根据体检号和组合项目id进行删除
     *
     * @param paId
     * @param groupId
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void deleteByPaIdAndGroupId(String paId, String groupId) {
        baseMapper.deleteByPaIdAndGroupId(paId, groupId, SecurityUtils.getTenantId());
        //删除成功后，继续删除子项
        tItemDTService.delItemDtByPaIdGroupId(paId, groupId);
    }


    /**
     * 根据体检号和组合项目id进行弃检
     *
     * @param paId
     * @param groupId
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void abandon(String paId, String groupId) {
        String nickName = SecurityUtils.getNickName();
        this.lambdaUpdate()
                .set(TItemHD::getResultDate, new Date())
                .set(TItemHD::getResult, SecurityConstants.IS_SUBMIT_RESULT+new StringBuilder().append("\n").toString())
                .set(TItemHD::getResultMark, "1")                    //要求弃检，判定为异常
                .set(TItemHD::getDoctor, nickName)
//                .set(TItemHD::getUpdateResultDoc, nickName)
//                .set(TItemHD::getUpdateResultDate, new Timestamp(System.currentTimeMillis()))
                .eq(TItemHD::getPaId, paId)
                .eq(TItemHD::getGroupId, groupId)
                .update();
        //撤销子项
        tItemDTService.lambdaUpdate()
                .set(TItemDT::getResult, "弃检")
                .set(TItemDT::getResultMark, "")
                .set(TItemDT::getResultDesc, "")
                .eq(TItemDT::getPaId, paId)
                .eq(TItemDT::getGroupId, groupId)
                .update();

        //撤销病种
        itemSicknessService.lambdaUpdate()
                .eq(TItemSickness::getPaId, paId)
                .eq(TItemSickness::getGroupId, groupId)
                .remove();
    }

    /**
     * 根据体检号和组合项目id进行撤销结果
     *
     * @param paId
     * @param groupId
     * @return
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void revocation(String paId, String groupId) {
        this.lambdaUpdate()
                .set(TItemHD::getResultDate, null)
                .set(TItemHD::getResult, null)
                .set(TItemHD::getResultMark, null)
                .set(TItemHD::getResultDesc, null)
                .set(TItemHD::getDoctor, null)
//                .set(TItemHD::getRegisterDoc, null)
//                .set(TItemHD::getUpdateResultDate, null)
//                .set(TItemHD::getUpdateResultDoc, null)
                .eq(TItemHD::getPaId, paId)
                .eq(TItemHD::getGroupId, groupId)
                .update();
        //撤销子项
        tItemDTService.lambdaUpdate()
                .set(TItemDT::getResult, null)
                .set(TItemDT::getResultMark, null)
                .set(TItemDT::getResultDesc, null)
                .eq(TItemDT::getPaId, paId)
                .eq(TItemDT::getGroupId, groupId)
                .update();

        //撤销病种
        itemSicknessService.lambdaUpdate()
                .eq(TItemSickness::getPaId, paId)
                .eq(TItemSickness::getGroupId, groupId)
                .remove();
    }


    /**
     * 组合项保存 实现
     *
     * @param hd TItemHD对象
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveItem(TItemHD hd, String sex) {
        LambdaUpdateWrapper<TItemHD> queryWrapper = new LambdaUpdateWrapper();
        queryWrapper.eq(TItemHD::getGroupId, hd.getGroupId());
        queryWrapper.eq(TItemHD::getPaId, hd.getPaId());
        if (hd.getReview() == null) {
            hd.setReview(0);
        }
        if (CollUtil.isNotEmpty(this.list(queryWrapper))) {
            this.update(hd, queryWrapper);
        } else {
            this.save(hd);
            tItemDTMapper.saveItemDt(hd.getPaId(), hd.getGroupId(), sex, hd.getReview());
        }
    }


    /**
     * 项目项目列表保存 实现
     *
     * @param list TItemHD列表
     * @return
     */
    @Override
    public void saveItems(TPatient tPatient, List<TItemHD> list) throws Exception {

//        //获取体检号对应的数据
//        TPatient tPatient=tPatientService.getById(paId);
        if (ObjectUtil.isEmpty(tPatient)) {
            throw new Exception("当前体检者数据为空");
        }

        //列表不为空或数量>0的，操作数据保存
        if (CollUtil.isNotEmpty(list)) {
            //List中GroupID转换成逗号分隔的字符串
            List<String> groupIds = list.stream().map(TItemHD::getGroupId).collect(Collectors.toList());
            //删除不在当前列表的组合数据
            delItemsNotInGroupIds(tPatient.getId(), groupIds);
            //删除不在当前列表的子项数据
            tItemDTService.delItemDtNotInGroupIds(tPatient.getId(), groupIds);
            //循环保存数据,保存子项
            for (TItemHD hd : list) {
                hd.setPaId(tPatient.getId());
                saveItem(hd, tPatient.getSex());
            }
        } else {   //组合项列表为空，全部清空
            //删除不在当前列表的组合数据
            delItemsNotInGroupIds(tPatient.getId(), null);
            //删除不在当前列表的子项数据
            tItemDTService.delItemDtNotInGroupIds(tPatient.getId(), null);
        }
    }


    /**
     * 删除指定体检号下，指定组合编号外的组合项
     *
     * @param paId          体检号
     * @param notInGroupIds 组合编号列表（如列表为空或Null则删除体检号下所有的组合项）
     */
    @Override
    public void delItemsNotInGroupIds(String paId, List<String> notInGroupIds) {
//        LambdaQueryWrapper<TItemHD> queryWrapper = new LambdaQueryWrapper<TItemHD>().eq(TItemHD::getPaId, paId);
//
//        //如果不为空，则筛选条件删除
//        if(CollUtil.isNotEmpty(notInGroupIds))
//            queryWrapper.notIn(TItemHD::getGroupId,notInGroupIds);
//        this.remove(queryWrapper);


        this.lambdaUpdate().eq(TItemHD::getPaId, paId)
                .notIn(CollUtil.isNotEmpty(notInGroupIds), TItemHD::getGroupId, notInGroupIds)
                .remove();
    }

    /**
     * 修改TypeJ、TypeZ和deptId
     *
     * @param itemHd
     * @return java.lang.Integer 返回受影响的行数
     * @author YJ
     * @date 2021/12/24 16:18
     */
    @Override
    public Integer updateItemTypeAndDeptId(TItemHD itemHd) {
        return baseMapper.updateItemTypeAndDeptId(itemHd);
    }

    /**
     * 删除之前清条码
     *
     * @param paId        体检号
     * @param groupIdList 组合编号列表 不能为空，空就没意义
     * @return java.lang.Integer 返回受影响的行数
     * @author YJ
     * @date 2021/12/24 17:52
     */
    @Override
    public Integer delBeforeClearBarcode(String paId, List<String> groupIdList) {
        if (StringUtils.isBlank(paId) || !Optional.ofNullable(groupIdList).isPresent() || groupIdList.size() == 0) {
            return 0;
        }
        return baseMapper.delBeforeClearBarcode(paId, groupIdList, SecurityUtils.getTenantId());
    }

    /**
     * 添加新项目，去除同条码名称的Barcode
     *
     * @param paId 体检号
     * @return java.lang.Integer
     * @author YJ
     * @date 2021/12/27 10:02
     */
    @Override
    public Integer addItemClearWithNameBarcode(String paId) {
        if (StringUtils.isBlank(paId)) {
            return 0;
        }
        return baseMapper.addItemClearWithNameBarcode(paId, SecurityUtils.getTenantId());
    }

    @Override
    public void repeatGroupId(List<String> paIds) {
        baseMapper.repeatGroupId(paIds, SecurityUtils.getTenantId());
    }
}
