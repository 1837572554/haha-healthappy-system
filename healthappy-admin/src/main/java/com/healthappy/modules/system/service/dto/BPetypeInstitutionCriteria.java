package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BPetypeInstitutionCriteria {

    @ApiModelProperty(hidden = true)
    @Query
    private String isEnable = "1";
}
