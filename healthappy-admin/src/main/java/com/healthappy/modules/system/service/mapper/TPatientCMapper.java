package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TPatientC;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 从业
 * @author fang
 * @date 2021-06-17
 */
@Mapper
@Repository
public interface TPatientCMapper extends CoreMapper<TPatientC> {


}
