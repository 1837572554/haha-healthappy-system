package com.healthappy.modules.system.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.config.databind.FieldBind;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 * 个人缴费-详情
 * @author FGQ
 */
@Data
@ApiOperation("个人缴费")
public class AlonePayCostDetailDto  implements Serializable {

    @ApiModelProperty("详情-内容")
    private Detail detail;

    @ApiModelProperty("项目清单")
    private List<ItemHd> itemHdList;

    @ApiModelProperty("缴费记录")
    private List<TCharge> tChargeList;


    @Data
    public static class TCharge {

        @ApiModelProperty("缴费编号")
        private String id;

        @ApiModelProperty(value = "现金")
        private BigDecimal money;

        @ApiModelProperty(value = "银行卡")
        private BigDecimal card;

        @ApiModelProperty(value = "微信")
        private BigDecimal weiXin;

        @ApiModelProperty(value = "支付宝")
        private BigDecimal zhiFuBao;

        @ApiModelProperty(value = "操作医生")
        private String changeDoctor;

        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
        @ApiModelProperty("操作时间")
        private Timestamp changeTime;

        @ApiModelProperty(value = "是否退费(不是退费就是收费)")
        private String refund;

        @ApiModelProperty(value = "票据号")
        private String receipt;
    }

    @Data
    public static class ItemHd {

        @ApiModelProperty("项目名称")
        private String groupName;

        @ApiModelProperty("科室")
        private String deptName;

        @ApiModelProperty("原价")
        private BigDecimal price;

        @ApiModelProperty("应收")
        private BigDecimal cost;

        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
        @ApiModelProperty("收费时间")
        private Timestamp chargeDate;

        @ApiModelProperty("票据号")
        private String hisNo;
    }

    @Data
    public static class Detail {

        @ApiModelProperty("流水号")
        private String paId;

        @ApiModelProperty("姓名")
        private String name;

        @FieldBind(target = "sex")
        @ApiModelProperty("性别")
        private String sex;

        @ApiModelProperty("性别")
        private String age;

        @ApiModelProperty("缴费状态")
        private String costType;

        @ApiModelProperty("总价格")
        private BigDecimal totalPrice;

        /**
         * 已收价格
         */
        @ApiModelProperty(value = "已收价格")
        private BigDecimal alreadyPrice;

        /**
         * 应收价格
         */
        @ApiModelProperty(value = "应收价格")
        private BigDecimal shouldPrice;

        /**
         * 实收价格
         */
        @ApiModelProperty(value = "实收价格")
        private BigDecimal actualPrice;

        /**
         * 未收金额
         */
        @ApiModelProperty(value = "未收金额")
        private BigDecimal noPrice;

        /**
         * 现金
         */
        @ApiModelProperty(value = "现金")
        private BigDecimal money;

        /**
         * 银行卡
         */
        @ApiModelProperty(value = "银行卡")
        private BigDecimal card;

        /**
         * 微信
         */
        @ApiModelProperty(value = "微信")
        private BigDecimal weiXin;

        /**
         * 支付宝
         */
        @ApiModelProperty(value = "支付宝")
        private BigDecimal zhiFuBao;

        /**
         * 收据号
         */
        @ApiModelProperty(value = "收据号")
        private String receipt;

        /**
         * 发票号
         */
        @ApiModelProperty(value = "票据类型")
        private String receiptType;
    }
}
