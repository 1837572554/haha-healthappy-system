package com.healthappy.modules.system.rest.manage;

import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.vo.BatchResultEntryVO;
import com.healthappy.modules.system.domain.vo.BatchResultVO;
import com.healthappy.modules.system.service.TPatientService;
import com.healthappy.modules.system.service.dto.BatchResultEntryQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author Jevany
 * @date 2022/5/13 17:19
 */
@Slf4j
@Api(tags = "体检管理：批量结果录入")
@RestController
@AllArgsConstructor
@RequestMapping("/batchResultEntry")
public class BatchResultEntryController {
	/** 体检人员服务 */
	private final TPatientService tPatientService;

	@Log("批量结果录入-列表查询")
	@ApiOperation("批量结果录入-列表查询，权限码：batchResultEntry:list")
	@GetMapping("/list")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "批量结果录入", response = BatchResultEntryVO.class, responseContainer = "Map")})
	@PreAuthorize("@el.check('batchResultEntry:list')")
	public R list(BatchResultEntryQueryCriteria criteria, Pageable pageable) {
		return R.ok(tPatientService.mapBatchResultEntry(criteria, pageable));
	}

	@Log("批量结果录入")
	@ApiOperation("批量结果录入，权限码：batchResultEntry:list")
	@PostMapping("/batchResultEntry")
	@PreAuthorize("@el.check('batchResultEntry:list')")
	public R batchResultEntry(@RequestBody BatchResultVO vo) {
		tPatientService.batchResultEntry(vo);
		return R.ok();
	}
}
