package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TReportRecord;
import com.healthappy.modules.system.service.dto.ReportReleaseSubmitDTO;
import com.healthappy.modules.system.service.dto.ReportReleaseUnSubmitDTO;
import com.healthappy.modules.system.service.dto.reportReleaseReceiveDTO;
import org.springframework.data.domain.Pageable;

import java.util.Map;

/**
 * 体检报告记录 服务
 * @author Jevany
 * @date 2022/3/7 9:47
 */
public interface TReportRecordService extends BaseService<TReportRecord> {
    /**
     * 体检报告记录 添加
     * @author YJ
     * @date 2022/3/7 9:49
     * @param submitDTO
     * @return boolean
     */
    void reportRecordAdd(ReportReleaseSubmitDTO submitDTO);

    /**
     * 撤销发布记录
     * @author YJ
     * @date 2022/3/7 13:54
     * @param unSubmitDTO
     */
    void reportReleaseUndoSubmit(ReportReleaseUnSubmitDTO unSubmitDTO);

    /**
     * 领取报告
     * @author YJ
     * @date 2022/3/7 11:40
     * @param receiveDTO
     */
    void reportRecordReceive(reportReleaseReceiveDTO receiveDTO);

    /**
     * 报告领取撤销
     * @author YJ
     * @date 2022/3/7 16:29
     * @param receiveDTO
     */
    void reportRecordUndoReceive(reportReleaseReceiveDTO receiveDTO);

    /**
     * 获取体检报告记录
     * @author YJ
     * @date 2022/3/7 17:14
     * @param paId
     * @return java.util.List〈com.healthappy.modules.system.domain.TReportRecord〉
     */
    Map<String, Object> listReportRecord(String paId, Pageable pageable);
}