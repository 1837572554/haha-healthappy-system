package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TPatientZ;
import com.healthappy.modules.system.service.dto.TPatientZQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 职业体检人员表
 * @author sjc
 * @date 2021-09-8
 */
public interface TPatientZService extends BaseService<TPatientZ> {
    Map<String, Object> queryAll(TPatientZQueryCriteria criteria, Pageable pageable);

    List<TPatientZ> queryAll(TPatientZQueryCriteria criteria);

    TPatientZ getByPaId(String paId);

    boolean deleteByPaId(Long id);
}
