package com.healthappy.modules.system.domain.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class BTypeSectorDtVo {

    /**
     * id
     */
    private Long id;

    /**
     * 行业id
     */
    private Long typeHdId;

    /**
     * 编码
     */
    private String code;

    /**
     * 名称
     */
    @NotBlank(message = "名称不能为空")
    private String name;

    /**
     * 套餐id
     */
    @NotNull(message = "套餐不能为空")
    private String packageId;

    /**
     * 采血类别
     */
    @NotNull(message = "采血类别不能为空")
    private Long takeblood;

    /**
     * 体检有效期
     */
    private Integer examinValidity;

    /**
     * 培训有效期
     */
    private Integer trainingValidity;

    /**
     * disp_order
     */
    private Integer dispOrder;
}
