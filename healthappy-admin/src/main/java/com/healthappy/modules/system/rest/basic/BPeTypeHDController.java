package com.healthappy.modules.system.rest.basic;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BPeTypeHD;
import com.healthappy.modules.system.service.BPeTypeHDService;
import com.healthappy.utils.R;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@Slf4j
@Api(tags = "基础数据：体检类型")
@ApiSort(11)
@RestController
@AllArgsConstructor
@RequestMapping("BPeTypeHD")
public class BPeTypeHDController {
    private final BPeTypeHDService bPeTypeHDService;

    @Log("体检类型")
    @ApiOperation("体检类型列表，权限码：BPeTypeHD:list")
    @GetMapping
    public R list(){
        return R.ok(bPeTypeHDService.list(Wrappers.<BPeTypeHD>lambdaQuery().eq(BPeTypeHD::getIsEnable,true).orderByAsc(BPeTypeHD::getDispOrder)));
    }

    @Log("根据组编号获取体检类型")
    @ApiOperation("根据组编号获取体检类型，权限码：BPeTypeHD:list")
    @PostMapping("/listByGroupId")
    public R listByGroupId(@RequestBody Set<Integer> groupIds) {
        return R.ok(bPeTypeHDService.list(Wrappers.<BPeTypeHD>lambdaQuery().eq(BPeTypeHD::getIsEnable,true).in(BPeTypeHD::getGroupID,groupIds).orderByAsc(BPeTypeHD::getDispOrder)));
    }

    @Log("新增|修改体检类型")
    @ApiOperation("新增|修改体检类型，权限码：admin")
    @PostMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BPeTypeHD resources) {
        return R.ok(bPeTypeHDService.saveOrUpdate(resources));
    }

    @Log("启用体检类型")
    @ApiOperation("启用体检类型，权限码：admin")
    @ApiParam(name = "id",value = "体检类型ID")
    @GetMapping("/openEnable")
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R openEnable(@RequestParam Integer id) {
        return R.ok(bPeTypeHDService.lambdaUpdate().eq(BPeTypeHD::getId,id).set(BPeTypeHD::getIsEnable,true));
    }

    @Log("关闭体检类型")
    @ApiOperation("关闭体检类型，权限码：admin")
    @ApiParam(name = "id",value = "体检类型ID")
    @GetMapping("/colseEnable")
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R closeEnable(@RequestParam Integer id) {
        return R.ok(bPeTypeHDService.lambdaUpdate().in(BPeTypeHD::getId,id).set(BPeTypeHD::getIsEnable,false));
    }
}
