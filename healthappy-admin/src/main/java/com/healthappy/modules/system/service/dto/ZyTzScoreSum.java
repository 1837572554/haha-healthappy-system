package com.healthappy.modules.system.service.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Jevany
 * @date 2021/12/9 19:51
 * @desc 中医体质合计类
 */
@Data
public class ZyTzScoreSum implements Serializable {
    /** 体质类别ID */
    private String id;

    /** 体质类别名称 */
    private String name;

    /** 体质内容 */
    private String content;

    /** 体质类型 */
    private Integer type;

    /** 分数 */
    private Integer score;

    /** 项次数 */
    private Integer cnt;
}
