package com.healthappy.modules.system.domain;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@ApiModel("人员档案信息")
@Data
@TableName("T_Patient_Baseinfo")
@RenewLog
public class TPatientBaseinfo implements Serializable {
    /**
     * 档案号
     */
    @ApiModelProperty(name = "pNo",value = "档案号")
    @TableId(value = "p_no", type = IdType.AUTO)
    @JSONField(name="pNo")
    private Long pNo;

    /** 设置档案号 */
    @ApiModelProperty(name = "pNo",value = "档案号")
    @JsonProperty("pNo")
    public void setPNo(Long pNo){
        this.pNo=pNo;
    }

    /** 获得档案号 */
    @ApiModelProperty(name = "pNo",value = "档案号")
    @JsonProperty("pNo")
    public Long getPNo() {
        return this.pNo;
    }

    /**
     * 身份证
     */
    @ApiModelProperty(value = " 身份证")
    private String idNo;

    /**
     * 姓名
     */
    @ApiModelProperty(value = " 姓名")
    private String name;

    /**
     * 性别
     */
    @ApiModelProperty(value = "性别 1:男 2:女")
    private Integer sex;

    /**
     * 出生日期
     */
    @ApiModelProperty(value = "出生日期")
    private Date birthday;

    private static final long serialVersionUID = 1L;
}
