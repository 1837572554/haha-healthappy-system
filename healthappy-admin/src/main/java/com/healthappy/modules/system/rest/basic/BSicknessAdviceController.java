package com.healthappy.modules.system.rest.basic;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BSicknessAdvice;
import com.healthappy.modules.system.service.BSicknessAdviceService;
import com.healthappy.utils.R;
import com.healthappy.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 病种建议 控制器
 * @author Jevany
 * @date 2022/2/21 17:58
 */
@Slf4j
@Api(tags = "基础管理：病种建议")
@ApiSort(5)
@RestController
@AllArgsConstructor
@RequestMapping("/SicknessAdvice")
public class BSicknessAdviceController {
    private final BSicknessAdviceService sicknessAdviceService;

    @Log("根据病种编号获取建议")
    @ApiOperation("根据病种编号获取建议，权限码：BSicknessRule:list")
    @GetMapping("/getBySicknessId")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "病种建议",response = BSicknessAdvice.class)
    })
    public R getBySicknessId(@RequestParam String sicknessId) {
        if(StringUtils.isBlank(sicknessId)){
            return R.error("病种编号不能为空");
        }
        return R.ok(sicknessAdviceService.getOne(Wrappers.<BSicknessAdvice>lambdaQuery().eq(BSicknessAdvice::getSicknessId,sicknessId)));
    }
}
