package com.healthappy.modules.system.domain;

import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.*;
import lombok.Data;

import java.util.List;

/**
 * 图文扫描录入
 * @author Jevany
 * @date 2022/2/16 10:21
 */
@Data
@RenewLog
public class TImgTxtScanDTO extends TImgTxtScan {
    /** 图片列表 */
    @ApiModelProperty("图片列表")
    List<TImageDTO> images;
}