package com.healthappy.modules.system.service.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.healthappy.modules.system.domain.BSicknessZH;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 病种组合子项
 * @author SJC
 * @date 2021-08-3
 */
@Data
public class BSicknessZHDto implements Serializable {


    /**
     * 病种组合id
     */
    @ApiModelProperty("病种组合id")
    private Long sicknessZhId;

    /**
     * 病种id
     */
    @ApiModelProperty("病种id")
    private String sicknessId;

    /**
     * 是否包含
     */
    @ApiModelProperty("是否包含")
    private Long containsFlag;


}
