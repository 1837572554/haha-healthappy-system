package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class BGroupHdVo {

    /**
     * id
     */
    @ApiModelProperty(value = "id",position = 1)
    private String id;

    /**
     * 科室编号
     */
    @ApiModelProperty(value = "科室编号",position = 2,required = true)
    @NotNull(message = "科室编号不能为空")
    private String deptId;

    /**
     * 科室名称
     */
    @ApiModelProperty(value = "科室名称",position = 3,required = true)
    @NotBlank(message = "科室名称不能为空")
    private String deptName;

    /**
     * 组合名称
     */
    @ApiModelProperty(value = "组合名称",position = 4,required = true)
    @NotBlank(message = "组合名称不能为空")
    private String name;

    /**
     * 价格
     */
    @ApiModelProperty(value = "价格",position = 5)
    private BigDecimal price;

    /**
     * 正常结果
     */
    @ApiModelProperty(value = "正常结果",position = 6)
    private String describe;

    /**
     * 临床意义
     */
    @ApiModelProperty(value = "临床意义",position = 7)
    private String meaning;

    /**
     * 提示信息
     */
    @ApiModelProperty(value = "提示信息",position = 8)
    private String instruction;

    /**
     * 类别
     */
    @ApiModelProperty(value = "类别",position = 9)
    private String type;

    /**
     * 结果获取方式
     */
    @ApiModelProperty(value = "结果获取方式",position = 10,required = true)
    @NotBlank(message = "结果获取方式不能为空")
    private String getResult;

    /**
     * 采样类型
     */
    @ApiModelProperty(value = "采样类型",position = 11,required = true)
    @NotBlank(message = "标本类型不能为空")
    private String sampleType;

    /**
     * 适用性别
     */
    @ApiModelProperty(value = "适用性别",position = 12,required = true)
    @NotBlank(message = "适用性别不能为空")
    private String sex;

    /**
     * 申请单
     */
    @ApiModelProperty(value = "申请单",position = 13)
    private String applyForm;

    /**
     * 条码
     */
    @ApiModelProperty(value = "条码",position = 14)
    private String barcode;

    /**
     * 条码数量
     */
    @ApiModelProperty(value = "条码数量",position = 15)
    private Integer barcodeNum;

    /**
     * 分布位置
     */
    @ApiModelProperty(value = "分布位置",position = 16)
    private String location;

    /**
     * 第三方平台申请类型
     */
    @ApiModelProperty(value = "第三方平台申请类型",position = 17)
    private String applyType;

    /**
     * 执行科室
     */
    @ApiModelProperty(value = "执行科室",position = 18)
    private String execDept;

    /**
     * 设备类型
     */
    @ApiModelProperty(value = "设备类型",position = 19)
    private String modalityType;

    /**
     * 申请大项代码
     */
    @ApiModelProperty(value = "申请大项代码",position = 20)
    private String applyTypeCode;

    /**
     * 申请大项名称
     */
    @ApiModelProperty(value = "申请大项名称",position = 21)
    private String applyTypeName;

    /**
     * 申请代码
     */
    @ApiModelProperty(value = "申请代码",position = 22)
    private String applyCode;

    /**
     * 申请名称
     */
    @ApiModelProperty(value = "申请名称",position = 23)
    private String applyItem;

    /**
     * 部位
     */
    @ApiModelProperty(value = "部位",position = 24)
    private String partway;

    /**
     * 启用
     */
    @ApiModelProperty(value = "启用",position = 25)
    private String isEnable;

    /**
     * 排序号
     */
    @ApiModelProperty(value = "排序号",position = 26)
    private Integer dispOrder;

    /**
     * 是否妇检
     */
    @ApiModelProperty(value = "是否妇检",position = 27)
    private String women;

    /**
     * 餐前/餐后
     */
    @ApiModelProperty(value = "餐前/餐后",position = 28)
    private String eat;

    /**
     * 是否外送
     */
    @ApiModelProperty(value = "是否外送",position = 29)
    private String out;

    /**
     * jp
     */
    @ApiModelProperty(value = "jp",position = 30)
    private String jp;

    /**
     * 是否显示在报告中
     */
    @ApiModelProperty(value = "是否显示在报告中",position = 31)
    private String reportShow;

    /**
     * 费别id
     */
    @ApiModelProperty(value = "费别id",position = 32)
    private String feeTypeId;

    /**
     * 权重 默认为1
     */
    @ApiModelProperty(value = "权重 默认为1",position = 33)
    private Double weight;

    /**
     * 检查权重默认为1
     */
    @ApiModelProperty(value = "检查权重默认为1",position = 34)
    private Double checkWeight;

    /**
     * 采血管颜色
     */
    @ApiModelProperty(value = "采血管颜色",position = 35)
    private String color;

    /**
     * 指引单上是否分行显示
     */
    @ApiModelProperty(value = "指引单上是否分行显示",position = 36)
    private String branchGui;



    /**
     * 是否显示在指引单
     */
    @ApiModelProperty(value = "是否显示在指引单",position = 36)
    private String reportGui;


    /**
     * 条码上显示的简称
     */
    @ApiModelProperty(value = "条码上显示的简称",position = 37)
    private String logogram;

    /**
     * 是否餐前
     */
    @ApiModelProperty(value = "是否餐前",position = 38)
    private Boolean isBreakfast;

    /**
     * 是否优惠
     */
    @ApiModelProperty(value = "是否优惠",position = 39)
    private Boolean isDiscount;

    /**
     * 采血及其耗材
     */
    @ApiModelProperty(value = "采血及其耗材",position = 40)
    private Boolean isBlood;

    /**
     * 健康体检的提示信息
     */
    @ApiModelProperty(value = "健康体检的提示信息",position = 41)
    private String instructionJk;

    /**
     * 绩效金额
     */
    @ApiModelProperty(value = "绩效金额",position = 42)
    private BigDecimal perfMoney;

    /**
     * service_fee
     */
    @ApiModelProperty(value = "service_fee",position = 43)
    private BigDecimal serviceFee;

    /**
     * 实价
     */
    @ApiModelProperty(value = "实价",position = 44)
    private BigDecimal cost;

    /**
     * 项目编码
     */
    @ApiModelProperty(value = "项目编码",position =46)
    private String code;


}
