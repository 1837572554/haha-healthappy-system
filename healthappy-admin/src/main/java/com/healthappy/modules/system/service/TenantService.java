package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.Tenant;
import com.healthappy.modules.system.service.dto.TenantQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author FGQ
 * @date 2021-03-08
 */
public interface TenantService extends BaseService<Tenant> {

	/**
	 * 查询数据分页
	 * @param criteria 条件
	 * @param pageable 分页参数
	 * @return Map<String, Object>
	 */
	Map<String, Object> queryAll(TenantQueryCriteria criteria, Pageable pageable);

	/**
	 * 查询数据分页
	 * @param criteria 条件
	 * @return Map<String, Object>
	 */
	List<Tenant> queryAll(TenantQueryCriteria criteria);

	/**
	 * 生产租户ID
	 * @return
	 */
	String generateTenantId();


	/**
	 * 剔除租户中登录的用户
	 * @param tenant
	 */
	void kickOutTenant(Tenant tenant);

	/**
	 * 删除租户
	 * @param ids 租户编号集合
	 */
	void removeByTenantIds(Set<Long> ids) throws Exception;

	/**
	 * 获得预约号源开关
	 * @author YJ
	 * @date 2022/5/9 17:49
	 * @param tenantId
	 * @return java.lang.String
	 */
	String getAppointSwitch(String tenantId);
}
