package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @desc: 体检图片Vo
 * @author: YJ
 * @date: 2021-12-02 12:00
 **/
@Data
public class TPatientImageVo {
    /** 体检编号 */
    @ApiModelProperty(value = "体检编号")
    private String paId;

    /** 组合项编号 */
    @ApiModelProperty(value = "组合项编号")
    private String groupId;

    /** 小项编号，可为空 */
    @ApiModelProperty(value = "小项编号，可为空")
    private String itemId;

    /** 图片Vo */
    @ApiModelProperty(value = "图片Vo 列表")
    private List<TImageVo> images;
}
