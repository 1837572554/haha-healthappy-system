package com.healthappy.modules.system.service.impl;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.config.ApplicationConstant;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.*;
import com.healthappy.modules.system.domain.vo.TAppointVo;
import com.healthappy.modules.system.service.*;
import com.healthappy.modules.system.service.dto.*;
import com.healthappy.modules.system.service.mapper.*;
import com.healthappy.modules.system.service.wrapper.BallCheckWrapper;
import com.healthappy.utils.FileUtil;
import com.healthappy.utils.SecurityUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author sjc
 * @date 2021-06-28
 */
@Service
@AllArgsConstructor//有自动注入的功能
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class TAppointServiceImpl extends BaseServiceImpl<TAppointMapper, TAppoint> implements TAppointService {

	private final TAppointMapper tAppointMapper;

	private final IGenerator generator;

	private final BPeTypeHDMapper bPeTypeHDMapper;

	private final BPeTypeDTMapper bPeTypeDTMapper;
	private final BPeTypeDTService peTypeDTService;
	private final BPeTypeHDService peTypeHDService;
	//毒害
	private final BPoisonService bPoisonService;

	private final BTypeAppointMapper bTypeAppointMapper;

	private final TAppointGroupService tAppointGroupService;
	//分组项目
	private final BDepartmentGroupTDService bDepartmentGroupTDService;
	private final BDepartmentGroupHDMapper bDepartmentGroupHDMapper;

	//组合项目
	private final BGroupHdService bGroupHdService;
	//套餐
	private final BPackageDTService bPackageDTService;
	private final BPoisonService poisonService;
	private final BPeTypeJobService peTypeJobService;

	private final TItemHDMapper tItemHDMapper;

	private final TenantService tenantService;

	@Override
	public Map<String, Object> queryAll(TAppointQueryCriteria criteria, Pageable pageable) {
		Integer signCount = querySignCount(criteria);
		getPage(pageable);

		PageInfo<TAppoint> page = new PageInfo<TAppoint>(queryAll(criteria));
		Map<String, Object> map = new LinkedHashMap<>();
		List<TAppointDto> convert = buildList(generator.convert(page.getList(), TAppointDto.class));

		//        long count = tAppoints.stream().filter(dto -> dto.getSignDate() != null).count();
		map.put("checkedUserNum", signCount);
		map.put("content", convert);
		map.put("totalElements", page.getTotal());
		return map;
	}

	private List<TAppointDto> buildList(List<TAppointDto> convert) {
		for (TAppointDto dto : convert) {
			//毒害名称
			if (StrUtil.isNotBlank(dto.getPoisonType())) {
				List<BPoison> poisonList = bPoisonService.lambdaQuery()
						.in(BPoison::getId, Stream.of(dto.getPoisonType().split("、")).collect(Collectors.toList()))
						.select(BPoison::getPoisonName).list();
				dto.setPoisonTypeName(CollUtil.isEmpty(poisonList) ?
						"" :
						poisonList.stream().map(BPoison::getPoisonName).collect(Collectors.joining("、")));
			}
			//
			if (StrUtil.isNotBlank(dto.getJob()) && StrUtil.isBlank(dto.getJobName())) {
				dto.setJobName(StrUtil.blankToDefault(
						Optional.ofNullable(peTypeJobService.getById(dto.getJob())).map(BPeTypeJob::getName).orElse(""),
						""));
			}
			dto.setTypeName(StrUtil.blankToDefault(
					Optional.ofNullable(peTypeDTService.getById(dto.getType())).map(BPeTypeDT::getName).orElse(""),
					""));
		}
		return convert;
	}


	@Override
	public boolean saveOrUpdate(TAppointVo resources) {
		//        //保存预约表，获取主键回填
		//        TAppoint tAppoint = new TAppoint();
		//        BeanUtil.copyProperties(resources,tAppoint);
		//        this.saveOrUpdate(tAppoint);
		//        resources.setId(tAppoint.getId());
		//        /* //sqoy20220224  预约管理修改保存用的，预约导名单的方法，封装一下这里也可以用，否则出来数据很有可能不一致*/
		//
		//        return saveAppointGroup(resources);

		//YJ 2022年3月10日 19:27:20 重写
		TAppoint tAppoint = BeanUtil.copyProperties(resources, TAppoint.class);
		this.saveOrUpdate(tAppoint);
		tAppointGroupService.saveAppointGroup(tAppoint, null);
		return true;
	}

	/**
	 * 保存预约项目组合
	 * @param resources
	 * @return
	 */
	private boolean saveAppointGroup(TAppointVo resources) {
		///sqoy 20220222 保存项目之前先把库里的删掉
		LambdaQueryWrapper<TAppointGroup> queryWrapper = new LambdaQueryWrapper();
		queryWrapper.eq(TAppointGroup::getAppointId, resources.getId());
		tAppointGroupService.remove(queryWrapper);
		///

		List<String> groupIds = new ArrayList<>();
		//付费方式
		Integer payType = 0;
		//如果没有套餐，只有分组的情况下
		if (StrUtil.isNotEmpty(resources.getDeptGroupId()) && resources.getPackageId() == null) {
			//获取分组信息
			BDepartmentGroupHD departmentGroupHD = bDepartmentGroupHDMapper.selectById(resources.getDeptGroupId());
			if (StrUtil.isNotEmpty(departmentGroupHD.getSffs()) && StrUtil.isNotBlank(departmentGroupHD.getSffs())) {
				payType = Integer.valueOf(departmentGroupHD.getSffs());
			}
			List<BDepartmentGroupTD> list = bDepartmentGroupTDService.lambdaQuery()
					.eq(BDepartmentGroupTD::getDepartmentGroupHdId, resources.getDeptGroupId()).list();
			groupIds.addAll(list.stream().map(BDepartmentGroupTD::getGroupId).collect(Collectors.toList()));
			//其他情况下只要套餐项目
		} else if (resources.getPackageId() != null) {
			List<BPackageDT> list = bPackageDTService.lambdaQuery()
					.eq(BPackageDT::getPackageId, resources.getPackageId()).list();

			groupIds.addAll(list.stream().map(BPackageDT::getGroupId).collect(Collectors.toList()));
		}
		//毒害  //sqoy 20220223 产品经理说这里注释掉
		else if (StrUtil.isNotEmpty(resources.getPoisonType()) && StrUtil.isNotEmpty(resources.getType())) {
			//List<BPoison> poisons = bPoisonService.lambdaQuery().in(BPoison::getId,Arrays.asList(resources.getPoisonType().split("、"))).list();
			//List<BPoisonDto> poisonDtos = bPoisonService.queryByPeTypeDt(poisons, resources.getType(),null);
			//groupIds.addAll(poisonDtos.stream().map(BPoisonDto::getPackageId).collect(Collectors.toList()));

			List<BPackageDT> packageDtList = poisonService.getPackageList(
					Stream.of(resources.getPoisonType().split("、")).collect(Collectors.toList()),
					resources.getPeType().toString());
			groupIds.addAll(packageDtList.stream().map(BPackageDT::getGroupId).collect(Collectors.toList()));
		}

		//附加套餐
		if (StrUtil.isNotBlank(resources.getAddPackageId())) {
			List<BPackageDT> addPackageDTList = bPackageDTService.lambdaQuery()
					.eq(BPackageDT::getPackageId, resources.getAddPackageId()).list();
			if (CollUtil.isNotEmpty(addPackageDTList)) {
				groupIds.addAll(addPackageDTList.stream().map(BPackageDT::getGroupId).collect(Collectors.toList()));
			}
		}

		//附加项目
		if (StrUtil.isNotBlank(resources.getAddGroupItem())) {
			//groupIds.add(resources.getAddGroupItemId());  //sqoy 20220223 预约管理传过来是空的
			List<String> lsAddGroup = Arrays.asList(resources.getAddGroupItem().split("、"));
			for (String GroupName : lsAddGroup) {
				List<BGroupHd> addGroupItemList = bGroupHdService.lambdaQuery().eq(BGroupHd::getName, GroupName).list();
				groupIds.add(addGroupItemList.get(0).getId());
			}
		}
		//去重放在特殊减项目前
		groupIds = groupIds.stream().distinct().collect(Collectors.toList());
		//特殊减项目
		if (StrUtil.isNotEmpty(resources.getSubtractGroupItem())) {
			//groupIds.removeAll(Arrays.asList(resources.getSubtractGroupItem().split("、")));//sqoy 20220223 特殊减项目存的是项目名称，不会被remove
			List<String> lsSubtractGroup = Arrays.asList(resources.getSubtractGroupItem().split("、"));
			for (String GroupName : lsSubtractGroup) {
				List<BGroupHd> SubtractGroupItemList = bGroupHdService.lambdaQuery().eq(BGroupHd::getName, GroupName)
						.list();
				if (CollUtil.isNotEmpty(SubtractGroupItemList)) {
					groupIds.remove(SubtractGroupItemList.get(0).getId());
				}
			}
		}

		List<BGroupHd> groupHdList;
		if (CollUtil.isNotEmpty(groupIds)) {
			groupHdList = bGroupHdService.lambdaQuery().in(BGroupHd::getId, groupIds).list();
		} else {
			return true;
		}

		ArrayList<TAppointGroup> appointGroupArrayList = new ArrayList<>();
		for (BGroupHd s : groupHdList) {
			TAppointGroup tAppointGroup = new TAppointGroup();
			tAppointGroup.setAppointId(resources.getId());
			tAppointGroup.setGroupId(s.getId());
			tAppointGroup.setPrice(s.getPrice());
			tAppointGroup.setDiscard(0);
			tAppointGroup.setPayType(payType);
			appointGroupArrayList.add(tAppointGroup);
		}
		tAppointGroupService.lambdaUpdate().eq(TAppointGroup::getAppointId, resources.getId()).remove();
		return tAppointGroupService.saveBatch(appointGroupArrayList);
	}

	/**
	 * 导入Excel文件
	 * @param file 上传的文件
	 * @param
	 */
	@Override
	public List<TAppointDto> upload(MultipartFile file) {
		String path = System.getProperty("user.dir");
		path = path + "\\" + file.getOriginalFilename();
		File destFile = new File(path);
		try {
			//将上传的Excel保存下来
			file.transferTo(destFile);
		} catch (IOException e) {
			e.printStackTrace();
		}

		Class<TAppointDto> aClass = TAppointDto.class;
		Field[] fields = aClass.getDeclaredFields();


		ExcelReader reader = ExcelUtil.getReader(path);
		for (Field field : fields) {
			//设置可见性
			field.setAccessible(true);
			String name = field.getName();
			//获取注解
			ApiModelProperty api = field.getAnnotation(ApiModelProperty.class);
			if (api != null) {
				//设置标题和对应字段的映射
				reader.addHeaderAlias(api.value(), name);
			}
		}
		List<TAppointDto> list = reader.readAll(TAppointDto.class);


		FileUtil.del(path);
		return list;
	}

	/**
	 * 预约导入，将Excel文件读取出来的内容保存到预约表中
	 * @param list
	 */
	@Override
	public void importAppoint(List<TAppointVo> list) {
		List<TAppoint> convert = generator.convert(list, TAppoint.class);
		for (TAppoint appoint : convert) {
			tAppointMapper.insert(appoint);
		}

	}

	/**
	 * 导出数据为Excel文件
	 * @param id
	 * @param response
	 * @throws IOException
	 */
	@Override
	public void download(String id, HttpServletResponse response) throws IOException, IllegalAccessException {
		if (StrUtil.isEmpty(id)) {
			throw new BadRequestException("id不可为空");
		}
		List<String> ids = Arrays.asList(id.split(","));
		List<TAppointToExcelDto> excelDtos = tAppointMapper.listForExcel(ids, SecurityUtils.getTenantId());
		completeData(excelDtos);
		FileUtil.listToExcel(excelDtos, TAppointToExcelDto.class, "预约列表", response);
	}

	private void completeData(List<TAppointToExcelDto> data) {
		Set<String> poisonTypes = new HashSet<>();//毒害种类
		Set<String> peTypeIds = new HashSet<>();//体检分类
		Set<String> typeIds = new HashSet<>();//体检类别
		Set<String> appointTypes = new HashSet<>();//预约类型
		for (TAppointToExcelDto dto : data) {
			if (!StrUtil.isEmpty(dto.getPoisonType())) {
				String[] split = dto.getPoisonType().split("、");
				poisonTypes.addAll(Arrays.asList(split));
			}
			peTypeIds.add(dto.getPeType());
			typeIds.add(dto.getType());
			appointTypes.add(dto.getAppointType());
		}

		//毒害种类
		List<BPoison> bPoisons = new ArrayList<>();
		if (poisonTypes.size() > 0) {
			bPoisons = bPoisonService.lambdaQuery().in(BPoison::getId, poisonTypes).list();
		}

		//体检分类
		List<BPeTypeHD> bPeTypeHDS = new ArrayList<>();
		if (peTypeIds.size() > 0) {
			bPeTypeHDS = bPeTypeHDMapper.selectBatchIds(peTypeIds);
		}

		//体检类别
		List<BPeTypeDT> bPeTypeDTS = new ArrayList<>();
		if (typeIds.size() > 0) {
			bPeTypeDTS = bPeTypeDTMapper.selectBatchIds(typeIds);
		}

		//预约类型
		List<BTypeAppoint> bTypeAppoints = new ArrayList<>();
		if (appointTypes.size() > 0) {
			bTypeAppoints = bTypeAppointMapper.selectBatchIds(appointTypes);
		}

		for (TAppointToExcelDto dto : data) {
			if (bPoisons.size() > 0 && !StrUtil.isEmpty(dto.getPoisonType())) {
				String[] split = dto.getPoisonType().split("、");
				StringBuilder poisonType = new StringBuilder();
				bPoisons.forEach(bPoison -> {
					for (String s : split) {
						if (s.equals(bPoison.getId().toString())) {
							poisonType.append(bPoison.getPoisonName()).append("、");
						}
					}
				});
				if (poisonType.length() > 0) {
					dto.setPoisonType(poisonType.substring(0, poisonType.length() - 1));
				}
			}
			if (bPeTypeHDS.size() > 0 && !StrUtil.isEmpty(dto.getPeType())) {
				bPeTypeHDS.forEach(peType -> {
					if (dto.getPeType().equals(peType.getId().toString())) {
						dto.setPeType(peType.getName());
					}
				});
			}
			if (bPeTypeDTS.size() > 0 && !StrUtil.isEmpty(dto.getType())) {
				bPeTypeDTS.forEach(type -> {
					if (dto.getType().equals(type.getId().toString())) {
						dto.setType(type.getName());
					}
				});
			}
			if (bTypeAppoints.size() > 0 && !StrUtil.isEmpty(dto.getAppointType())) {
				bTypeAppoints.forEach(appoint -> {
					if (dto.getAppointType().equals(appoint.getId().toString())) {
						dto.setAppointType(appoint.getName());
					}
				});
			}
		}
	}

	/**
	 * 查询单位全部数据
	 * @return Object
	 */
	@Override
	public List<TAppoint> queryAll(TAppointQueryCriteria criteria) {
		return this.list(QueryHelpPlus.getPredicate(TAppoint.class, criteria));
	}

	/**
	 * 查询预约数据条数
	 * @author YJ
	 * @date 2022/2/8 14:19
	 * @param criteria
	 * @return java.lang.Integer
	 */
	private Integer querySignCount(TAppointQueryCriteria criteria) {
		TAppointQueryCriteria tAppointQueryCriteria = BeanUtil.copyProperties(criteria, TAppointQueryCriteria.class);
		if (ObjectUtil.isNotNull(tAppointQueryCriteria.getSignDoctor()) && tAppointQueryCriteria.getSignDoctor()
				.equals(false)) {
			return 0;
		}
		tAppointQueryCriteria.setSignDoctor(true);
		return this.count(QueryHelpPlus.getPredicate(TAppoint.class, tAppointQueryCriteria));
	}

	/**
	 * 根据身份证获取预约数据
	 * @param  idNo 身份证
	 * @return
	 */
	@Override
	public List<TAppoint> queryByIdNo(String idNo) {
		return tAppointMapper.queryByIdNo(idNo, SecurityUtils.getTenantId());
	}

	/**
	 * 签到
	 * @return Object
	 */
	@Override
	public boolean sign(String id) {
		String doc = SecurityUtils.getUsername();
		return tAppointMapper.sign(id, new Timestamp(System.currentTimeMillis()), doc, SecurityUtils.getTenantId());
	}

	@Override
	public Boolean signPatient(String appointId, String paId) {
		return this.lambdaUpdate().set(TAppoint::getSignDate, new Timestamp(System.currentTimeMillis()))
				.set(TAppoint::getPaId, paId).set(TAppoint::getSignDoctor, SecurityUtils.getUsername())
				.eq(TAppoint::getId, appointId).update();
	}

	@Override
	public TAppointPrintDto detailForPrint(Integer id) {
		return tAppointMapper.detailForPrint(id);
	}

	@Override
	public Map<String, Object> ballCheckQueryAll(DeptWorkloadQueryCriteria criteria, Pageable pageable) {
		getPage(pageable);
		return BallCheckWrapper.build().pageVO(new PageInfo<TAppoint>(queryAll(criteria)));
	}

	@Override
	public List<TAppoint> queryAll(DeptWorkloadQueryCriteria criteria) {
		return this.list(QueryHelpPlus.getPredicate(TAppoint.class, criteria));
	}

	@Override
	public Map<String, Object> queryAppointGroup(String appointId) {
		String tenantId = SecurityUtils.getTenantId();
		//获取数据前，先检测是否开启预约号源管理
		String appointSwitch = tenantService.getAppointSwitch(tenantId);
		if (ApplicationConstant.APPOINT_SWITCH.equals(appointSwitch)
				&& this.baseMapper.appointToDayById(appointId) == 0) {
			throw new BadRequestException("只能签到今天的预约数据");
		}
		Map<String, Object> map = new LinkedHashMap<>(1);
		map.put("content", tItemHDMapper.queryAppointGroup(appointId, tenantId));
		return map;
	}

	@Override
	public List<AppointDateNumDTO> calcAppointDateNum(List<String> timeList) {
		return tAppointMapper.calcAppointDateNum(timeList);
	}

	@Override
	public Integer appointToDayById(String appointId) {
		return tAppointMapper.appointToDayById(appointId);
	}
}
