package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.config.ApplicationConstant;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.enums.PeDateTypeEnum;
import com.healthappy.enums.PeTypeGroupEnum;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.redis.service.RedisUtilService;
import com.healthappy.modules.system.domain.*;
import com.healthappy.modules.system.domain.vo.*;
import com.healthappy.modules.system.service.*;
import com.healthappy.modules.system.service.dto.*;
import com.healthappy.modules.system.service.mapper.MedicalRegistrationMapper;
import com.healthappy.modules.system.service.mapper.TPatientMapper;
import com.healthappy.modules.system.service.wrapper.MedicalRegistrationWrapper;
import com.healthappy.utils.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author sjc
 * @date 2021-06-28
 */
@Slf4j
@Service
@AllArgsConstructor//有自动注入的功能
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class TPatientServiceImpl extends BaseServiceImpl<TPatientMapper, TPatient> implements TPatientService {

	private final TPatientMapper tPatientMapper;
	private final TPatientZService tPatientZService;
	private final IGenerator generator;
	private final WorkStationService workStationService;
	private final BPeTypeHDService bPeTypeHDService;
	private final TItemHDService tItemHDService;
	private final TItemDTService tItemDTService;
	private final BGroupHdService bGroupHdService;
	private final BBarcodeService bBarcodeService;

	private final TPhotoService photoService;

	private final TPatientPoisonService patientPoisonService;

	private final TPatientBaseinfoService baseinfoService;

	private final MedicalRegistrationMapper medicalRegistrationMapper;

	private final TAppointService appointService;

	private final TenantService tenantService;

	private final BAppointDateService appointDateService;

	private final RedisUtilService redisUtilService;

	@Override
	public Map<String, Object> queryAll(TPatientQueryCriteria criteria, Pageable pageable) {
		getPage(pageable);
		PageInfo<TPatient> page = new PageInfo<TPatient>(queryAll(criteria));
		Map<String, Object> map = new LinkedHashMap<>();
		List<TPatientDto> convert = generator.convert(page.getList(), TPatientDto.class);
		map.put("content", convert);
		map.put("totalElements", page.getTotal());
		return map;
	}


	@Override
	public List queryAll(TPatientQueryCriteria criteria) {
		return tPatientMapper.queryAll(criteria, SecurityUtils.getTenantId());
	}

	private TPatient saveOrUpdateVo(TPatientVo resources) {
		TPatient tPatient = new TPatient();
		//用于将对象属性拷贝到另一个对象（类型，名字一样即可）
		//第一个是数据源，第二个是赋值对象
		BeanUtil.copyProperties(resources, tPatient);

		//添加照片
		TPhoto photo = new TPhoto();
		photo.setIdImage(resources.getIdImage());
		photo.setPortrait(resources.getPortrait());

		Integer peTypeGroupId = bPeTypeHDService.getPeTypeGroupId(tPatient.getPeTypeHdId());

		//region 年龄检测
		AgeCalcUtil.AgeData ageData = null;
		if (ObjectUtil.isNull(resources.getAge()) && ObjectUtil.isNotNull(resources.getBirthday())) {
			if (ageData == null) {
				ageData = AgeCalcUtil.calc(resources.getBirthday());
			}
			resources.setAge(ageData.getYear());
		}
		if (StrUtil.isBlank(resources.getAgeDate()) && ObjectUtil.isNotNull(resources.getBirthday())
				&& PeTypeGroupEnum.CHILD.getValue().equals(peTypeGroupId)) {
			if (ageData == null) {
				ageData = AgeCalcUtil.calc(resources.getBirthday());
			}
			resources.setAgeDate((ageData.getYear() != 0 ? ageData.getYear() + "岁" : "") + (ageData.getMonth() != 0 ?
					ageData.getMonth() + "月" :
					""));
		}
		//endregion 年龄检测

		//为空的情况下是新增，需要设置体检时间
		if (StringUtils.isBlank(tPatient.getId())) {
			//新增登记
			//如果为空的，则生成新地体检号
			String primaryKey = workStationService.generatePrimaryKey(1);

			if (BeanUtil.isNotEmpty(primaryKey)) {
				photo.setId(primaryKey);
				photoService.save(photo);
			}

			tPatient.setId(primaryKey);
			//设置体检登记时间
			tPatient.setPeDate(new Timestamp(System.currentTimeMillis()));
			tPatient.setSignDate(new Timestamp(System.currentTimeMillis()));
			//            tPatient.setPeDateReal(new Timestamp(System.currentTimeMillis()));
		} else {
			photoService.lambdaUpdate().set(TPhoto::getIdImage, resources.getIdImage())
					.set(TPhoto::getPortrait, resources.getPortrait()).eq(TPhoto::getId, tPatient.getId()).update();
		}
		this.saveOrUpdate(tPatient);
		//如果是职业体检

		if (PeTypeGroupEnum.PROFESSION.getValue().equals(peTypeGroupId) || PeTypeGroupEnum.ZPHY.getValue()
				.equals(peTypeGroupId)) {
			TPatientZ tPatientZ = tPatientZService.getByPaId(tPatient.getId());
			if (ObjectUtil.isEmpty(tPatientZ)) {
				tPatientZ = TPatientZ.builder().build();
				tPatientZ.setPaId(tPatient.getId());
			}
			tPatientZ.setWorkYears(resources.getWorkYears());
			tPatientZ.setPoisonType(resources.getPoisonType());
			tPatientZService.saveOrUpdate(tPatientZ);

			//region 保存毒害编号列表
			patientPoisonService.savePatientPoison(tPatient.getId(), resources.getPoisonIdList());
			//endregion 保存毒害编号列表
		}
		return tPatient;
	}

	@Override
	public boolean saveOrUpdate(TPatientVo resources) {
		saveOrUpdateVo(resources);
		return true;
	}


	/**
	 * 保存修改体检登记
	 *
	 * @param resources TPatientVo 体检人员数据
	 * @param items     TItemHDVo项目集合
	 */
	@Override
	public String saveOrUpdate(TPatientVo resources, List<TItemHDVo> items) throws Exception {
		TPatient tPatient = saveOrUpdateVo(resources);
		List<TItemHD> list = items.stream().map(h -> BeanUtil.copyProperties(h, TItemHD.class))
				.collect(Collectors.toList());
		tItemHDService.saveItems(tPatient, list);
		return tPatient.getId();
	}


	@Override
	public boolean deleteById(String id) {
		String tenantId = SecurityUtils.getTenantId();
		String appointSwitch = tenantService.getAppointSwitch(tenantId);
		Date nowDate = DateUtils.getNowDate();

		if (ApplicationConstant.APPOINT_SWITCH.equals(appointSwitch)) {
			//清除预约号源缓存
			redisUtilService.evic(CacheConstant.APPOINT_DATE_NUM + StrUtil.COLON + tenantId,
					DateUtil.year(nowDate) + "_" + (DateUtil.month(nowDate) + 1));

		}
		boolean deleteById = baseMapper.deleteById(id, SecurityUtils.getTenantId());

		if (ApplicationConstant.APPOINT_SWITCH.equals(appointSwitch)) {
			//延迟 简单双删
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			redisUtilService.evic(CacheConstant.APPOINT_DATE_NUM + StrUtil.COLON + tenantId,
					DateUtil.year(nowDate) + "_" + DateUtil.month(nowDate));

		}
		return deleteById;
	}


	@Override
	public Map<String, Object> queryRegistrationAll(MedicalRegistrationQueryCriteria criteria, Pageable pageable) {
		getPage(pageable);
		return MedicalRegistrationWrapper.build().pageVO(new PageInfo<>(queryAll(criteria)));
	}

	@Override
	public Map<String, Object> queryRegistrationAllLite(MedicalRegistrationQueryCriteria criteria, Pageable pageable) {
		getPage(pageable);
		PageInfo<TPatientLiteDto> page = new PageInfo<TPatientLiteDto>(baseMapper.getPatientLite(criteria));
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("content", page.getList());
		map.put("totalElements", page.getTotal());
		return map;
	}

	@Override
	public MedicalRegistrationDto getPatient(String patientId) {
		TPatient tPatient = baseMapper.selectOne(new LambdaQueryWrapper<TPatient>().eq(TPatient::getId, patientId));
		if (!Optional.ofNullable(tPatient).isPresent()) {
			throw new BadRequestException("无法查询到数据");
		}
		return MedicalRegistrationWrapper.build().entityVO(tPatient);
	}

	@Override
	public TPatient getPatientInfo(String patientId) {
		TPatient tPatient = baseMapper.selectOne(new LambdaQueryWrapper<TPatient>().eq(TPatient::getId, patientId));
		if (!Optional.ofNullable(tPatient).isPresent()) {
			throw new BadRequestException("无法查询到数据");
		}
		return tPatient;
	}

	/**
	 * 体检登记
	 *
	 * @param criteria
	 * @return
	 */
	@Override
	public List<TPatient> queryAll(MedicalRegistrationQueryCriteria criteria) {
		QueryWrapper predicate = QueryHelpPlus.getPredicate(TPatient.class, criteria);
		return baseMapper.selectList(predicate);
	}

	@Override
	public String installVo(TPatientAddVo resources) {
		TPatientVo patientVo = resources.getResources();
		String tenantId = SecurityUtils.getTenantId();
		//获取数据前，先检测是否开启预约号源管理
		String appointSwitch = tenantService.getAppointSwitch(tenantId);
		if (ApplicationConstant.APPOINT_SWITCH.equals(appointSwitch)) {
			//有预约号的，检测预约号是否当天的,(有预约号则不限制了)
			if (StrUtil.isNotBlank(resources.getAppointId())) {
				if (appointService.appointToDayById(resources.getAppointId()) == 0) {
					//当天没有预约数据的则提示
					throw new BadRequestException("只能签到今天的预约数据");
				}
			} else {
				//没有预约号的，现场登记的
				appointDateService.companyCanRegister(patientVo.getCompanyId(), resources.getAppointId());
			}
		}

		TPatient patient = BeanUtil.copyProperties(patientVo, TPatient.class);
		TPhoto photo = new TPhoto();
		patient.setRegisterDoctor(SecurityUtils.getNickName());
		photo.setIdImage(patientVo.getIdImage());
		//肖像 这里还是储存肖像的数据
		photo.setPortrait(patientVo.getPortrait());
		String patientId = patient.getId();

		Integer peTypeGroupId = bPeTypeHDService.getPeTypeGroupId(patient.getPeTypeHdId());


		if (!PeTypeGroupEnum.HEALTH.getValue().equals(peTypeGroupId) && StrUtil.isBlank(patient.getPhone())) {
			throw new BadRequestException("请输入联系电话");
		}

		//region 年龄检测
		AgeCalcUtil.AgeData ageData = null;
		if (ObjectUtil.isNull(patient.getAge()) && ObjectUtil.isNotNull(patient.getBirthday())) {
			if (ageData == null) {
				ageData = AgeCalcUtil.calc(patient.getBirthday());
			}
			patient.setAge(ageData.getYear());

		}
		if (StrUtil.isBlank(patient.getAgeDate()) && ObjectUtil.isNotNull(patient.getBirthday())
				&& PeTypeGroupEnum.CHILD.getValue().equals(peTypeGroupId)) {
			if (ageData == null) {
				ageData = AgeCalcUtil.calc(patient.getBirthday());
			}
			patient.setAgeDate((ageData.getYear() != 0 ? ageData.getYear() + "岁" : "") + (ageData.getMonth() != 0 ?
					ageData.getMonth() + "月" :
					""));
		}
		//endregion 年龄检测

		if (StrUtil.isBlank(patientId)) {
			String primaryKey = workStationService.generatePrimaryKey(1);

			patient.setWorkstation(workStationService.getWorkStation());
			patient.setId(primaryKey);
			patient.setPeDate(DateTime.now().toTimestamp());
			//            patient.setPeDateReal(DateTime.now().toTimestamp());
			patient.setSignDate(DateTime.now().toTimestamp());

			//region 档案号操作
			if (ObjectUtil.isNull(patient.getRecordId())) {
				patient.setRecordId(baseinfoService.getPNo(
						TPatientBaseinfoSaveDtoDto.builder().idNo(patient.getIdNo()).name(patient.getName())
								.peTypeHdId(patient.getPeTypeHdId()).build()));
			}
			//endregion 档案号操作

			Date nowDate = DateUtils.getNowDate();

			if (ApplicationConstant.APPOINT_SWITCH.equals(appointSwitch)) {
				//清除预约号源缓存
				redisUtilService.evic(CacheConstant.APPOINT_DATE_NUM + StrUtil.COLON + tenantId,
						DateUtil.year(nowDate) + "_" + (DateUtil.month(nowDate) + 1));
			}

			baseMapper.insert(patient);

			if (ApplicationConstant.APPOINT_SWITCH.equals(appointSwitch)) {
				//延迟 简单双删
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				redisUtilService.evic(CacheConstant.APPOINT_DATE_NUM + StrUtil.COLON + tenantId,
						DateUtil.year(nowDate) + "_" + DateUtil.month(nowDate));
			}

			patientId = patient.getId();
			patientVo.setId(patientId);

			//保存用户头像
			photo.setId(patientId);
			photoService.save(photo);

			//region 预约信息 反写
			if (ObjectUtil.isNotNull(resources.getAppointId()) && !resources.getAppointId().isEmpty()) {
				appointService.signPatient(resources.getAppointId(), patientId);
			}
			//endregion 预约信息 反写
		} else {
			photoService.lambdaUpdate().set(TPhoto::getIdImage, patientVo.getIdImage())
					.set(TPhoto::getPortrait, patientVo.getPortrait()).eq(TPhoto::getId, patientId).update();

			//region 档案号操作
			if (ObjectUtil.isNull(patient.getRecordId())) {
				patient.setRecordId(baseinfoService.getPNo(
						TPatientBaseinfoSaveDtoDto.builder().idNo(patient.getIdNo()).name(patient.getName())
								.peTypeHdId(patient.getPeTypeHdId()).build()));
			}
			//endregion 档案号操作

			baseMapper.updateById(patient);
		}
		//region 职业相关操作
		occupationalMedical(patientVo, peTypeGroupId);
		//endregion 职业相关操作

		//region 项目保存相关
		TPatientItemVO itemVO = new TPatientItemVO();
		itemVO.setId(patientId);
		itemVO.setItems(resources.getItems());
		itemVO.setPeTypeHdId(resources.getResources().getPeTypeHdId());
		editItems(itemVO);
		//endregion 项目保存相关

		return patientId;
	}

	@Override
	public String editPatientInfo(TPatientVo patientVo) {
		if (StrUtil.isBlank(patientVo.getId())) {
			throw new BadRequestException("体检号不能为空");
		}
		String patientId = patientVo.getId();
		TPatient patient = BeanUtil.copyProperties(patientVo, TPatient.class);
		TPhoto photo = new TPhoto();
		patient.setRegisterDoctor(SecurityUtils.getNickName());
		patient.setRegisterDoctor(SecurityUtils.getNickName());
		photo.setIdImage(patientVo.getIdImage());
		//肖像 这里还是储存肖像的数据
		photo.setPortrait(patientVo.getPortrait());

		photoService.lambdaUpdate().set(TPhoto::getIdImage, patientVo.getIdImage())
				.set(TPhoto::getPortrait, patientVo.getPortrait()).eq(TPhoto::getId, patientVo.getId()).update();

		//region 档案号操作
		if (ObjectUtil.isNull(patient.getRecordId())) {
			patient.setRecordId(baseinfoService.getPNo(
					TPatientBaseinfoSaveDtoDto.builder().idNo(patient.getIdNo()).name(patient.getName())
							.peTypeHdId(patient.getPeTypeHdId()).build()));
		}
		//endregion 档案号操作
		Integer peTypeGroupId = bPeTypeHDService.getPeTypeGroupId(patient.getPeTypeHdId());

		//region 年龄检测
		AgeCalcUtil.AgeData ageData = null;
		if (ObjectUtil.isNull(patient.getAge()) && ObjectUtil.isNotNull(patient.getBirthday())) {
			if (ageData == null) {
				ageData = AgeCalcUtil.calc(patient.getBirthday());
			}
			patient.setAge(ageData.getYear());
		}
		if (StrUtil.isBlank(patient.getAgeDate()) && ObjectUtil.isNotNull(patient.getBirthday())
				&& PeTypeGroupEnum.CHILD.getValue().equals(peTypeGroupId)) {
			if (ageData == null) {
				ageData = AgeCalcUtil.calc(patient.getBirthday());
			}
			patient.setAgeDate((ageData.getYear() != 0 ? ageData.getYear() + "岁" : "") + (ageData.getMonth() != 0 ?
					ageData.getMonth() + "月" :
					""));
		}
		//endregion 年龄检测

		baseMapper.updateById(patient);

		occupationalMedical(patientVo, peTypeGroupId);

		return patientId;
	}

	@Override
	public String editPatientItem(TPatientItemVO itemVO) {
		String patientId = itemVO.getId();
		if (StrUtil.isBlank(patientId)) {
			throw new BadRequestException("体检号不能为空");
		}
		editItems(itemVO);

		return null;
	}

	/**
	 * 职业体检相关操作
	 *
	 * @param patientVo
	 * @param peTypeGroupId 体检类型（大类）
	 * @author YJ
	 * @date 2022/2/9 12:14
	 */
	private void occupationalMedical(TPatientVo patientVo, Integer peTypeGroupId) {
		String patientId = patientVo.getId();
		//如果是职业体检
		if (ObjectUtil.isNull(peTypeGroupId)) {
			peTypeGroupId = bPeTypeHDService.getPeTypeGroupId(patientVo.getPeTypeHdId());
		}
		if (PeTypeGroupEnum.PROFESSION.getValue().equals(peTypeGroupId) || PeTypeGroupEnum.ZPHY.getValue()
				.equals(peTypeGroupId)) {
			if (StrUtil.isEmpty(patientVo.getPoisonType()) && CollUtil.isEmpty(patientVo.getPoisonIdList())) {
				throw new BadRequestException("毒害种类不能为空");
			}
			TPatientZ tPatientZ = tPatientZService.getByPaId(patientId);
			if (ObjectUtil.isEmpty(tPatientZ)) {
				tPatientZ = new TPatientZ();
				tPatientZ.setPaId(patientId);
			}
			tPatientZ.setWorkYears(patientVo.getWorkYears());
			tPatientZ.setPoisonType(patientVo.getPoisonType());
			tPatientZService.saveOrUpdate(tPatientZ);

			//region 保存毒害编号列表
			patientPoisonService.savePatientPoison(patientId, patientVo.getPoisonIdList());
			//endregion 保存毒害编号列表
		}
	}

	/**
	 * 修改项目
	 *
	 * @param itemVO
	 * @author YJ
	 * @date 2022/2/9 12:10
	 */
	private void editItems(TPatientItemVO itemVO) {
		List<TItemHDVo> itemHDVoList = itemVO.getItems();
		String patientId = itemVO.getId();
		if (CollUtil.isEmpty(itemHDVoList)) {
			//如果项目列表为空，则清除所有
			tItemDTService.lambdaUpdate().eq(TItemDT::getPaId, patientId).remove();
			tItemHDService.lambdaUpdate().eq(TItemHD::getPaId, patientId).remove();
		} else {
			String finalPatientId = patientId;
			//获取当前提交的项目列表的组合编号
			List<String> groupIdList = itemHDVoList.stream().filter(hd -> StrUtil.isNotEmpty(hd.getGroupId()))
					.map(TItemHDVo::getGroupId).collect(Collectors.toList());

			//清空不在列表中的项目条码
			tItemHDService.delBeforeClearBarcode(patientId, groupIdList);

			//删除不在列表中的项目
			tItemDTService.lambdaUpdate().eq(TItemDT::getPaId, patientId).notIn(TItemDT::getGroupId, groupIdList)
					.remove();
			tItemHDService.lambdaUpdate().eq(TItemHD::getPaId, patientId).notIn(TItemHD::getGroupId, groupIdList)
					.remove();

			//根据体检类型设置组合项目的类型
			PeTypeGroupEnum peTypeGroupEnum = PeTypeGroupEnum.toType(
					bPeTypeHDService.getPeTypeGroupId(itemVO.getPeTypeHdId()));
			//获取基础-组合项目数据
			List<BGroupHd> bGroupHds = bGroupHdService.lambdaQuery().in(BGroupHd::getId, groupIdList).list();
			//添加不在数据库表中的项目,循环处理
			itemHDVoList.forEach(hd -> {
				//检测当前体检号，组合编号是否在库中存在
				TItemHD itemHd = tItemHDService.lambdaQuery().eq(TItemHD::getPaId, finalPatientId)
						.eq(TItemHD::getGroupId, hd.getGroupId()).one();
				if (Optional.ofNullable(itemHd).isPresent()) {
					//数据库中已经存在 只改变typeJ、typeZ、deptId
					updateItemTypeDeptId(peTypeGroupEnum, itemHd, bGroupHds, hd);
					if (!itemHd.getPayType().equals(hd.getPayType())) {
						itemHd.setPayType(hd.getPayType());
					}
					itemHd.setPrice(Optional.ofNullable(hd.getPrice()).orElse(BigDecimal.valueOf(0)));
					itemHd.setDiscount(Optional.ofNullable(hd.getDiscount()).orElse(BigDecimal.valueOf(1)));
					if (ObjectUtil.isNull(hd.getCost())) {
						if (ObjectUtil.isNotNull(itemHd.getPrice()) && ObjectUtil.isNotNull(itemHd.getDiscount())) {
							hd.setCost(NumberUtil.round(NumberUtil.mul(itemHd.getPrice(), itemHd.getDiscount()), 2));
						}
					}
					itemHd.setCost(hd.getCost());
					itemHd.setAddition(hd.getAddition());
					tItemHDService.updateItemTypeAndDeptId(itemHd);
				} else {
					//不存在,新增一个
					String nickName = SecurityUtils.getNickName();
					TItemHD tItemHD = BeanUtil.copyProperties(hd, TItemHD.class);
					Integer review = null == tItemHD.getReview() ? 0 : tItemHD.getReview();
					updateItemTypeDeptId(peTypeGroupEnum, tItemHD, bGroupHds, hd);
					tItemHD.setPaId(finalPatientId);
					tItemHD.setAddTime(new Timestamp(System.currentTimeMillis()));
					tItemHD.setReview(review);
					tItemHD.setRegisterDoc(nickName);
					if (ObjectUtil.isNull(tItemHD.getPrice())) {
						tItemHD.setPrice(BigDecimal.valueOf(0));
					}
					if (ObjectUtil.isNull(tItemHD.getDiscount())) {
						tItemHD.setDiscount(BigDecimal.valueOf(1));
					}
					if (ObjectUtil.isNull(tItemHD.getCost())) {
						tItemHD.setCost(NumberUtil.round(NumberUtil.mul(tItemHD.getPrice(), tItemHD.getDiscount()), 2));
					}
					tItemHDService.save(tItemHD);

					String groupId = tItemHD.getGroupId();
					for (BItem dt : tItemDTService.getGroupIdList(tItemHD.getGroupId())) {
						TItemDT itemDT = new TItemDT();
						BeanUtil.copyProperties(dt, itemDT, "result");
						itemDT.setItemName(dt.getName());
						itemDT.setPaId(finalPatientId);
						itemDT.setItemId(dt.getId());
						itemDT.setGroupId(tItemHD.getGroupId());
						if (StrUtil.isNotEmpty(dt.getRefLow()) && StrUtil.isNotEmpty(dt.getRefHigh())) {
							itemDT.setRefValue(dt.getRefLow() + "-" + dt.getRefHigh());
						}
						itemDT.setReview(review);
						tItemDTService.save(itemDT);
					}
				}

			});
			//tItemHDService.addItemClearWithNameBarcode(finalPatientId);
			generateBarcode(finalPatientId);
		}
	}

	/**
	 * 更新目标ItemHd数据的TypeJ、TypeZ、DeptId数据
	 *
	 * @param peTypeGroupEnum 当前体检编号对应的体检枚举对象
	 * @param bGroupHds       前台提交的组合对象，重新从数据库中获取的对应集合
	 * @param hd              前台提交的组合对象中的一个对象（源对象）
	 * @param tItemHD         目标对象（修改这个对象的数据）
	 * @author YJ
	 * @date 2021/12/24 16:33
	 */
	private void updateItemTypeDeptId(PeTypeGroupEnum peTypeGroupEnum, TItemHD tItemHD, List<BGroupHd> bGroupHds,
			TItemHDVo hd) {
		switch (peTypeGroupEnum) {
			case HEALTH:
			case WORK:
			case Driver:
			case CHILD:
			case STUDENT:
				tItemHD.setTypeJ("1");
				tItemHD.setTypeZ("0");
				break;
			case PROFESSION:
				tItemHD.setTypeJ("1");
				tItemHD.setTypeZ("1");
				break;
			case ZPHY:
				if (StringUtils.isNotBlank(hd.getTypeJ()) && hd.getTypeJ().trim().equals("1")) {
					tItemHD.setTypeJ("1");
				} else {
					tItemHD.setTypeJ("0");
				}
				if (StringUtils.isNotBlank(hd.getTypeZ()) && hd.getTypeZ().trim().equals("1")) {
					tItemHD.setTypeZ("1");
				} else {
					tItemHD.setTypeZ("0");
				}
				break;
		}

		//设置科室id
		if (StrUtil.isEmpty(tItemHD.getDeptId()) && CollUtil.isNotEmpty(bGroupHds)) {
			Optional<BGroupHd> first = bGroupHds.stream().filter(hds -> hds.getId().equals(hd.getGroupId()))
					.findFirst();
			if (first.isPresent()) {
				tItemHD.setDeptId(first.get().getDeptId());
			}
		}
	}

	/**
	 * 生成条码
	 *
	 * @param paId
	 */
	@Override
	public void generateBarcode(String paId) {
		List<TItemHD> group = tItemHDService.getBarcodeGroup(paId);
		group.forEach(g -> {
			//如果条码号位空，则判断相同条码名称的项目上是否有条码号，有的话获取这个条码号，没有就创建一个新的条码号
			//条码号不为空就跳过
			if (StrUtil.isEmpty(g.getBarcode())) {
				String barcode = "";
				//获取同条码名称组下有条码的项目
				List<TItemHD> collect = group.stream()
						.filter(gr -> gr.getBarcodeName().equals(g.getBarcodeName()) && gr.getGroupType()
								.equals(g.getGroupType()) && StrUtil.isNotEmpty(gr.getBarcode()))
						.collect(Collectors.toList());
				//生成新的条码
				if (CollUtil.isEmpty(collect)) {
					barcode = bBarcodeService.generateBarcode(g.getGroupType());
					if (barcode == null) {
						barcode = paId;
					}
				} else {//使用之前产生的条码
					barcode = collect.get(0).getBarcode();
				}
				tItemHDService.updateBarcode(paId, barcode, g.getBarcodeName(), g.getGroupType());
			}
		});
	}

	@Override
	public Integer updSignDate(TPatient tPatient) {
		if (ObjectUtil.isNotNull(tPatient) && StrUtil.isNotBlank(tPatient.getId()) && ObjectUtil.isNotNull(
				tPatient.getSignDate()) && StrUtil.isNotBlank(tPatient.getTenantId())) {
			return tPatientMapper.updSignDate(tPatient);
		}
		return 0;
	}

	@Override
	public Map<String, Object> listReportRelease(ReportReleaseQueryCriteria criteria, Pageable pageable) {
		getPage(pageable);
		PageInfo<ReportReleaseDTO> page = new PageInfo<ReportReleaseDTO>(tPatientMapper.listReportRelease(criteria));
		Map<String, Object> map = new LinkedHashMap<>(2);
		map.put("content", page.getList());
		map.put("totalElements", page.getTotal());
		return map;
	}

	@Override
	public Map<String, Object> mapBatchInfoEdit(BatchInfoEditQueryCriteria criteria, Pageable pageable) {
		//		getPage(pageable, "pa_id desc");
		getPage(pageable, "pa_id desc");
		PageInfo<BatchInfoEditVO> pageInfo = new PageInfo<>(listBatchInfoEdit(criteria));
		Map<String, Object> map = new HashMap<>();
		map.put("content", pageInfo.getList());
		map.put("totalElements", pageInfo.getTotal());
		return map;
	}

	@Override
	public List<BatchInfoEditVO> listBatchInfoEdit(BatchInfoEditQueryCriteria criteria) {
		List<BatchInfoEditVO> batchInfoEditVOList = this.baseMapper.listBatchInfoEdit(criteria);
		//		batchInfoEditVOList.forEach(vo -> vo.setPoisonList(
		//				patientPoisonService.lambdaQuery().eq(TPatientPoison::getPatientId, vo.getPaId()).list()));
		return batchInfoEditVOList;
	}

	@Override
	public void batchInfoEdit(List<BatchInfoEditVO> batchInfoEditVOList) {
		this.baseMapper.batchInfoEdit(batchInfoEditVOList);
	}

	@Override
	public Map<String, Object> mapBatchProjectAdjust(BatchProjectAdjustQueryCriteria criteria, Pageable pageable) {
		getPage(pageable, "pa_id desc");
		PageInfo<BatchProjectAdjustVO> pageInfo = new PageInfo<>(listBatchProjectAdjust(criteria));
		Map<String, Object> map = new HashMap<>();
		map.put("content", pageInfo.getList());
		map.put("totalElements", pageInfo.getTotal());
		return map;
	}

	@Override
	public List<BatchProjectAdjustVO> listBatchProjectAdjust(BatchProjectAdjustQueryCriteria criteria) {
		return this.baseMapper.listBatchProjectAdjust(criteria);
	}

	@Override
	public void batchProjectAdjust(BatchProjectAdjustDTO dto) {
		if (dto.getOperationType() == 1) {
			//添加
			for (String paId : dto.getPaIdList()) {
				TPatient patient = this.lambdaQuery().eq(TPatient::getId, paId).one();
				if (!Optional.ofNullable(patient).isPresent()) {
					continue;
				}
				//根据体检类型设置组合项目的类型
				PeTypeGroupEnum peTypeGroupEnum = PeTypeGroupEnum.toType(
						bPeTypeHDService.getPeTypeGroupId(patient.getPeTypeHdId()));

				for (TItemHD itemHD : dto.getGroupList()) {

					//检测当前体检号，组合编号是否在库中存在
					TItemHD itemHd = tItemHDService.lambdaQuery().eq(TItemHD::getPaId, paId)
							.eq(TItemHD::getGroupId, itemHD.getGroupId()).one();
					if (!Optional.ofNullable(itemHd).isPresent()) {
						//不存在,新增一个
						String nickName = SecurityUtils.getNickName();
						TItemHD tItemHD = new TItemHD();

						//region 项目设置
						Integer review = 0;
						Timestamp timestampNow = new Timestamp(System.currentTimeMillis());
						tItemHD.setPaId(paId);
						tItemHD.setGroupId(itemHD.getGroupId());
						tItemHD.setDeptId(itemHD.getDeptId());
						tItemHD.setGroupName(itemHD.getGroupName());
						//						tItemHD.setTypeZ("0");
						//						tItemHD.setTypeJ("0");
						tItemHD.setPrice(itemHD.getPrice());
						tItemHD.setDiscount(itemHD.getDiscount());
						tItemHD.setCost(itemHD.getCost());
						tItemHD.setAddition("0");
						tItemHD.setPayType(dto.getPayType());
						tItemHD.setChoose("0");
						tItemHD.setAddTime(timestampNow);
						tItemHD.setUpdateTime(timestampNow);
						tItemHD.setReview(review);
						tItemHD.setRegisterDoc(nickName);
						tItemHD.setRegisterState("1");
						//endregion 项目设置
						updateItemTypeDeptId(peTypeGroupEnum, tItemHD, null, null);
						tItemHDService.save(tItemHD);

						String groupId = tItemHD.getGroupId();
						for (BItem dt : tItemDTService.getGroupIdList(tItemHD.getGroupId())) {
							TItemDT itemDT = new TItemDT();
							BeanUtil.copyProperties(dt, itemDT, "result");
							itemDT.setItemName(dt.getName());
							itemDT.setPaId(paId);
							itemDT.setItemId(dt.getId());
							itemDT.setGroupId(tItemHD.getGroupId());
							if (StrUtil.isNotEmpty(dt.getRefLow()) && StrUtil.isNotEmpty(dt.getRefHigh())) {
								itemDT.setRefValue(dt.getRefLow() + "-" + dt.getRefHigh());
							}
							itemDT.setReview(review);
							tItemDTService.save(itemDT);
						}
					}
				}
				generateBarcode(paId);
			}
		} else {
			//删除
			for (String paId : dto.getPaIdList()) {
				for (TItemHD itemHD : dto.getGroupList()) {
					tItemHDService.lambdaUpdate().eq(TItemHD::getPaId, paId)
							.eq(TItemHD::getGroupId, itemHD.getGroupId()).remove();
				}
			}
		}

	}

	@Override
	public Map<String, Object> mapBatchResultEntry(BatchResultEntryQueryCriteria criteria, Pageable pageable) {
		getPage(pageable, "pa_id desc");
		PageInfo<BatchResultEntryVO> pageInfo = new PageInfo<>(listBatchResultEntry(criteria));
		Map<String, Object> map = new HashMap<>();
		map.put("content", pageInfo.getList());
		map.put("totalElements", pageInfo.getTotal());
		return map;
	}

	@Override
	public List<BatchResultEntryVO> listBatchResultEntry(BatchResultEntryQueryCriteria criteria) {
		return this.baseMapper.listBatchResultEntry(criteria);
	}

	@Override
	public void batchResultEntry(BatchResultVO vo) {
		for (String paId : vo.getPaIdList()) {
			for (TItemDT itemDT : vo.getItemDTList()) {
				tItemDTService.lambdaUpdate().set(TItemDT::getResult, itemDT.getResult())
						.set(TItemDT::getResultMark, itemDT.getResultMark()).eq(TItemDT::getPaId, paId)
						.eq(TItemDT::getGroupId, vo.getGroupId()).eq(TItemDT::getItemId, itemDT.getItemId()).update();
			}
		}
	}

	/**
	 * 获得体检时间类型
	 * @param pagePath 页面路径码
	 * @author: Jevany
	 * @date: 2021/11/26 12:38
	 * @return: java.lang.String 返回Json
	 */
	@Override
	public String getPeDateTypes(String pagePath) {
		if (StringUtils.isBlank(pagePath)) {
			return PeDateTypeEnum.toJson();
		}
		Map<String, Object> root = new HashMap<>();
		Map<String, Object> map = new HashMap<>();
		List<Map> list = null;
		String path = pagePath.toLowerCase();
		switch (path) {
			case "projectcheck":    //项目检查
			case "deptworkload":    //科室工作量统计
			case "companyexamination":         //单位体检情况查询
			case "sicknessconclusion":          //阳性结论名单
			case "medicalprogress":             //体检进度查询
			case "imgtxtscanmanage":            //图文扫描录入
			case "companyreportperson":         //单位体检报告-人员查询列表
				//region 科室工作量统计
				list = new ArrayList<>();
				list.add(PeDateTypeEnum.REGISTRATION.toMap());
				list.add(PeDateTypeEnum.SIGN.toMap());
				list.add(PeDateTypeEnum.COMPLETION.toMap());
				map.put(pagePath, list);
				//endregion 科室工作量统计
				break;
			case "individualproject":           //单独项目报告
			case "batchmessagemodify":          //批量信息修改
			case "batchprojectadjust":            //批量项目调整
			case "batchresultentry":            //批量结果录入
				list = new ArrayList<>();
				list.add(PeDateTypeEnum.REGISTRATION.toMap());
				list.add(PeDateTypeEnum.SIGN.toMap());
				list.add(PeDateTypeEnum.COMPLETION.toMap());
				map.put(pagePath, list);
				break;
			case "doctorworkload":  //医生工作量统计
				//region 医生工作量统计
				list = new ArrayList<>();
				list.add(PeDateTypeEnum.REGISTRATION.toMap());
				list.add(PeDateTypeEnum.SIGN.toMap());
				list.add(PeDateTypeEnum.COMPLETION.toMap());
				map.put("checkTheWorkload", list);       //检查工作量
				list = new ArrayList<>();
				list.add(PeDateTypeEnum.REGISTRATION.toMap());
				list.add(PeDateTypeEnum.SIGN.toMap());
				list.add(PeDateTypeEnum.CONCLUSION.toMap());
				list.add(PeDateTypeEnum.VERIFY.toMap());
				map.put("businessWorkload", list);       //业务工作量
				list = new ArrayList<>();
				list.add(PeDateTypeEnum.REGISTRATION.toMap());
				list.add(PeDateTypeEnum.SIGN.toMap());
				list.add(PeDateTypeEnum.CONCLUSION.toMap());
				list.add(PeDateTypeEnum.VERIFY.toMap());
				map.put("performanceScore", list);       //绩效分
				//endregion
				break;
			case "classifiedstatistics":        //分类统计
				//region 分类统计
				list = new ArrayList<>();
				list.add(PeDateTypeEnum.REGISTRATION.toMap());
				list.add(PeDateTypeEnum.SIGN.toMap());
				list.add(PeDateTypeEnum.COMPLETION.toMap());
				map.put("unitClassification", list);   //单位及体检分类统计
				list = new ArrayList<>();
				list.add(PeDateTypeEnum.REGISTRATION.toMap());
				list.add(PeDateTypeEnum.SIGN.toMap());
				list.add(PeDateTypeEnum.COMPLETION.toMap());
				map.put("unitCategory", list);   //单位及体检类别统计
				list = new ArrayList<>();
				list.add(PeDateTypeEnum.REGISTRATION.toMap());
				list.add(PeDateTypeEnum.SIGN.toMap());
				list.add(PeDateTypeEnum.COMPLETION.toMap());
				map.put("classificationPeople", list);   //体检分类统计（分类及人数）
				list = new ArrayList<>();
				list.add(PeDateTypeEnum.REGISTRATION.toMap());
				list.add(PeDateTypeEnum.SIGN.toMap());
				list.add(PeDateTypeEnum.COMPLETION.toMap());
				map.put("classificationUnit", list);   //体检分类统计（分类及单位）
				//endregion
				break;
			case "integratedmanage":
				//region 体检管理-综合管理
				list = new ArrayList<>();
				list.add(PeDateTypeEnum.REGISTRATION.toMap());
				list.add(PeDateTypeEnum.SIGN.toMap());
				list.add(PeDateTypeEnum.CONCLUSION.toMap());
				list.add(PeDateTypeEnum.VERIFY.toMap());
				list.add(PeDateTypeEnum.PRINT.toMap());
				list.add(PeDateTypeEnum.UPLOAD.toMap());
				map.put(pagePath, list);
				//endregion 体检管理-综合管理
				break;
			case "criticalstatusmanage":
				//region 体检管理-危急值状况管理
				list = new ArrayList<>();
				list.add(PeDateTypeEnum.OCCUR.toMap());
				list.add(PeDateTypeEnum.HANDLE.toMap());
				map.put(pagePath, list);
				//endregion 体检管理-危急值状况管理
				break;
			case "reportrelease":
				//region 体检报告发布
				list = new ArrayList<>();
				list.add(PeDateTypeEnum.REGISTRATION.toMap());
				list.add(PeDateTypeEnum.CONCLUSION.toMap());
				list.add(PeDateTypeEnum.VERIFY.toMap());
				list.add(PeDateTypeEnum.PRINT.toMap());
				list.add(PeDateTypeEnum.UPLOAD.toMap());
				map.put(pagePath, list);
				//endregion 体检报告发布
				break;
			case "checkoutlist":
				//region 结账名单
				list = new ArrayList<>();
				list.add(PeDateTypeEnum.REGISTRATION.toMap());
				list.add(PeDateTypeEnum.SIGN.toMap());
				list.add(PeDateTypeEnum.CONCLUSION.toMap());
				map.put(pagePath, list);
				//endregion 结账名单
				break;
			case "personalreporttask":
				//region 分配任务
				list = new ArrayList<>();
				list.add(PeDateTypeEnum.REGISTRATION.toMap());
				list.add(PeDateTypeEnum.SIGN.toMap());
				map.put(pagePath, list);
				//endregion
				break;
			case "breakfastmanagement":        //早餐管理
			case "specimenhandover":            //标本交接管理
				list = new ArrayList<>();
				list.add(PeDateTypeEnum.REGISTRATION.toMap());
				list.add(PeDateTypeEnum.SIGN.toMap());
				list.add(PeDateTypeEnum.RECEIPT.toMap());
				break;
			default:
				break;
		}
		root.put(pagePath, map);
		return JSON.toJSON(root).toString();
	}

	@Override
	public Boolean delPatientByIds(String ids) {
		String tenantId = SecurityUtils.getTenantId();
		return this.baseMapper.deleteByIds(ids, tenantId);
	}

	@Override
	public List<BarcodeVO> getBarcodeByPaId(String paId) {
		List<PatientBarcodeDTO> barcodeDTOList = this.baseMapper.getBarcodeByPaId(paId);
		List<BarcodeVO> barcodeVOList = new ArrayList<>();
		if (Optional.ofNullable(barcodeDTOList).isPresent()) {
			TPatient patient = this.getById(paId);
			String infor = patient.getName() + " " + patient.getSexText() + " " + patient.getAge();

			Integer barcodeNum = 0;
			for (PatientBarcodeDTO barcodeDTO : barcodeDTOList) {
				if (StrUtil.isBlank(barcodeDTO.getCode())) {
					barcodeDTO.setCode(paId);
				}
				if (ObjectUtil.isNull(barcodeDTO.getBarcodeNum())) {
					barcodeDTO.setBarcodeNum(0);
				}
				barcodeNum = barcodeDTO.getBarcodeNum();
				while (barcodeNum > 0) {
					BarcodeVO barcodeVO = new BarcodeVO();
					barcodeVO.setPaId(paId);
					barcodeVO.setInfor(infor);
					barcodeVO.setBarcodeName(barcodeDTO.getBarcodeName());
					barcodeVO.setCode(barcodeDTO.getCode());
					barcodeVO.setItems(barcodeDTO.getItems());
					barcodeVO.setApplyType(barcodeDTO.getApplyType());
					barcodeVO.setIsIds(0);
					barcodeVOList.add(barcodeVO);
					barcodeNum--;
				}
			}
		}
		return barcodeVOList;
	}
}
