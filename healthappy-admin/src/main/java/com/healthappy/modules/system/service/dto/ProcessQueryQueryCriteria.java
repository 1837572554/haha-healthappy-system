package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Jevany
 * @date 2022/1/26 9:32
 * @desc 体检进度查询  查询类
 */
@Data
@ApiModel("体检进度查询  查询类")
public class ProcessQueryQueryCriteria {

    /**
     * 体检时间类型
     */
//    @NotNull(message = "体检时间类型不能为空")
    @ApiModelProperty(value = "体检时间类型 ,页面路径：processQuery", position = 0)
    private Integer dateType;

    /**
     * 开始时间
     */
//    @NotBlank(message = "开始时间不能为空")
    @ApiModelProperty(value = "开始时间 如：2021-01-01 00:00:00", example = "2021-01-01 00:00:00", position = 1)
    private String startTime;

    /**
     * 结束时间
     */
//    @NotBlank(message = "结束时间不能为空")
    @ApiModelProperty(value = "结束时间 如：2021-12-01 23:59:59", example = "2022-12-01 23:59:59", position = 2)
    private String endTime;

    /**
     * 体检分类ID
     */
    @ApiModelProperty("体检分类ID")
    private String peTypeHdId;

    /**
     * 体检类别ID
     */
    @ApiModelProperty("体检类别ID")
    private String peTypeDtId;

    /**
     * 体检号
     */
    @ApiModelProperty("体检号")
    private String paId;

    /**
     * 单位ID
     */
    @ApiModelProperty("单位ID")
    private String companyId;

    /**
     * 部门ID
     */
    @ApiModelProperty("部门ID")
    private String deptId;

    /**
     * 体检者姓名
     */
    @ApiModelProperty("体检者姓名")
    private String paName;

    /**
     * 体检者身份证
     */
    @ApiModelProperty("体检者身份证")
    private String idNo;

    /**
     * 普通项目 1：已完成 0：未完成 -1全部，默认-1
     */
    @ApiModelProperty("普通项目 1：已完成 0：未完成 -1全部，默认-1")
    private Integer general = -1;

    /**
     * 检验项目 1：已完成 0：未完成 -1全部，默认-1
     */
    @ApiModelProperty("检验项目 1：已完成 0：未完成 -1全部，默认-1")
    private Integer inspection = -1;

    /**
     * 总检项目 1：已完成 0：未完成 -1全部，默认-1
     */
    @ApiModelProperty("总检项目 1：已完成 0：未完成 -1全部，默认-1")
    private Integer conclusion = -1;

    /**
     * 审核项目 1：已完成 0：未完成 -1全部，默认-1
     */
    @ApiModelProperty("审核项目 1：已完成 0：未完成 -1全部，默认-1")
    private Integer verify = -1;

    /**
     * 打印项目  1：已完成 0：未完成 -1全部，默认-1
     */
    @ApiModelProperty("打印项目  1：已完成 0：未完成 -1全部，默认-1")
    private Integer print = -1;


    /**
     * 页面显示条数
     */
    @ApiModelProperty("页面显示条数,默认为10")
    private Integer pageSize = 10;


    /**
     * 页数
     */
    @ApiModelProperty("页数,默认为1")
    private Integer pageNum = 1;

    /**
     * 是否导出，如果为True，分页功能不启用，默认False
     */
    @ApiModelProperty(value = "是否导出，如果为True，分页功能不启用，默认False",hidden = true)
    private Boolean isExport = false;


    /**
     * 租户ID
     */
    @ApiModelProperty(value = "租户ID", hidden = true)
    private String tenantId;

    /**
     * 开始条数
     */
    @ApiModelProperty(value = "开始条数", hidden = true)
    private Integer startIndex = -1;

}