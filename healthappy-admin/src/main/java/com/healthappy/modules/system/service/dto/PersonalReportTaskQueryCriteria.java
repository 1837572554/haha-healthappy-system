package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PersonalReportTaskQueryCriteria {

    @ApiModelProperty("体检编号")
    @Query(type = Query.Type.EQUAL)
    private String id;

    @ApiModelProperty("姓名")
    @Query(type = Query.Type.INNER_LIKE)
    private String name;

    @ApiModelProperty("单位")
    private String companyId;

    @ApiModelProperty("部门Id")
    @Query
    private String departmentId;

    @ApiModelProperty("分类")
    @Query
    private String peTypeHdId;

    @ApiModelProperty("体检开始日期")
    private String startTime;

    @ApiModelProperty("体检结束日期")
    private String endTime;

    /**
     * 体检时间类型
     */
    @ApiModelProperty(value = "体检时间类型 ,页面路径：personalReportTask")
    private Integer dateType;

    @Query
    @ApiModelProperty("主检医生")
    private String conclusionDoc;

    @Query
    @ApiModelProperty("审核医生")
    private String verifyDoc;

    /**
     * 是否分配
     */
    @ApiModelProperty("是否分配")
    private Boolean isAssign = Boolean.FALSE;

    @ApiModelProperty("是否指定单位")
    private Boolean isAssignCompany = Boolean.TRUE;

    /**
     * 是否有缺项
     */
    @ApiModelProperty("是否有缺项")
    private Boolean isLackItem;

    /**
     * 是否总检
     */
    @ApiModelProperty("是否总检")
    private Boolean isConclusion = Boolean.FALSE;

    /**
     * 是否审核
     */
    @ApiModelProperty("是否审核")
    private Boolean isVerify = Boolean.FALSE;
}
