package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BTypeAppoint;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created with IntelliJ IDEA. Created by yutang 2021/9/8 0008  9:20 Description:
 */
@Mapper
public interface BTypeAppointMapper extends CoreMapper<BTypeAppoint> {

}
