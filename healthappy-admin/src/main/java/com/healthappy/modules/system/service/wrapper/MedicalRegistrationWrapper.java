package com.healthappy.modules.system.service.wrapper;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.healthappy.modules.system.domain.*;
import com.healthappy.modules.system.service.*;
import com.healthappy.modules.system.service.dto.MedicalRegistrationDto;
import com.healthappy.modules.system.service.dto.PoisonTypeDto;
import com.healthappy.utils.SpringUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 包装类,返回视图层所需的字段
 *
 * @author FGQ
 */
public class MedicalRegistrationWrapper extends BaseEntityWrapper<TPatient, MedicalRegistrationDto>{

    private final static MedicalRegistrationService registrationService;
    private final static BPoisonService poisonService;
    private final static BNationService nationService;
    private final static TPhotoService tPhotoService;
    private final static TPatientZService tPatientZService;
    private final static TPatientPoisonService patientPoisonService;

    static {
        registrationService = SpringUtil.getBean(MedicalRegistrationService.class);
        poisonService = SpringUtil.getBean(BPoisonService.class);
        nationService = SpringUtil.getBean(BNationService.class);
        tPhotoService = SpringUtil.getBean(TPhotoService.class);
        tPatientZService=SpringUtil.getBean(TPatientZService.class);
        patientPoisonService=SpringUtil.getBean(TPatientPoisonService.class);
    }

    public static MedicalRegistrationWrapper build(){
        return new MedicalRegistrationWrapper();
    }


    @Override
    public MedicalRegistrationDto entityVO(TPatient entity) {
        MedicalRegistrationDto registrationDto = registrationService.buildBean(entity.getId());
        registrationDto.setNationName(StrUtil.blankToDefault(Optional.ofNullable(nationService.lambdaQuery().eq(BNation::getCode,registrationDto.getNation()).select(BNation::getName).one()).map(BNation::getName).orElse(""),""));
        registrationDto.setAgeDate(entity.getAgeDate());
        //人员头像
        TPhoto query = tPhotoService.query(entity.getId());
        if(query != null)
        {
            registrationDto.setIdImage(query.getIdImage());
            registrationDto.setPortrait(query.getPortrait());
        }
        TPatientZ patientZ = tPatientZService.getByPaId(entity.getId());

        if(Optional.ofNullable(patientZ).isPresent()) {
            registrationDto.setPoisonType(patientZ.getPoisonType());
            registrationDto.setWorkYears(patientZ.getWorkYears());
        }

        //region 获取体检毒害ID列表
        List<TPatientPoison> tPatientPoisons = patientPoisonService.getBaseMapper().selectList(new LambdaQueryWrapper<TPatientPoison>().eq(TPatientPoison::getPatientId, entity.getId()));
        if(CollUtil.isNotEmpty(tPatientPoisons) && tPatientPoisons.size()>0){
            List<String> poIds=new ArrayList<>();
            tPatientPoisons.forEach(patientPoison-> poIds.add(patientPoison.getPoisonId()));
            registrationDto.setPoisonIdList(poIds);
        }
        else{
            registrationDto.setPoisonIdList(new ArrayList<>());
        }
        //endregion 获取体检毒害ID列表

        registrationDto.setPoisonTypeList(
                    StrUtil.isNotBlank(registrationDto.getPoisonType()) ?
                            poisonService.lambdaQuery().in(BPoison::getId,StrUtil.split(registrationDto.getPoisonType(),','))
                                                    .select(BPoison::getPoisonName,BPoison::getId).list()
                                                    .stream().map(b-> BeanUtil.copyProperties(b, PoisonTypeDto.class))
                                                    .collect(Collectors.toList())
                            : Collections.emptyList());
//        registrationDto.setProjectList(ObjectUtil.isNotNull(registrationDto.getDepartmentGroupId()) ? registrationService.buildProject(registrationDto.getDepartmentGroupId()) : Collections.emptyList());
        registrationDto.setProjectList(registrationService.getList(entity.getId()));
        return registrationDto;
    }
}
