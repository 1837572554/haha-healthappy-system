package com.healthappy.modules.system.service.dto;

import com.healthappy.modules.system.domain.BTypeDevice;
import lombok.Data;

/**
 * @description 申请类型
 * @author sjc
 * @date 2021-08-3
 */
@Data
public class BTypeDeviceDto extends BTypeDevice {



}
