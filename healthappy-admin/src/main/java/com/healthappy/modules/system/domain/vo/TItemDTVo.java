package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @description 体检人员子项目表
 * @author sjc
 * @date 2021-07-20
 */
@Data
public class TItemDTVo {

	/**
	 * 主键id
	 */
	@ApiModelProperty("主键id")
	private Long id;


	/**
	 * 体检号
	 */
	@NotNull(message = "体检号不能为空")
	@ApiModelProperty("体检号")
	private String paId;


	/**
	 * 项目编号
	 */
	@NotNull(message = "项目编号不能为空")
	@ApiModelProperty("项目编号")
	private String itemId;

	/**
	 * 项目名称
	 */
	@ApiModelProperty("项目名称")
	private String itemName;


	/**
	 * 组合编号
	 */
	@NotNull(message = "组合编号不能为空")
	@ApiModelProperty("组合编号")
	private String groupId;


	/**
	 * 仪器通道号
	 */
	@ApiModelProperty("仪器通道号")
	private String instrCode;


	/**
	 * 项目结果
	 */
	@ApiModelProperty("项目结果")
	private String result;


	/**
	 * 结果提示
	 */
	@ApiModelProperty("结果提示")
	private String resultMark;


	/**
	 * 结果描述
	 */
	@ApiModelProperty("结果描述")
	private String resultDesc;


	/**
	 * 结果单位
	 */
	@ApiModelProperty("结果单位")
	private String unit;


	/**
	 * 参考上限
	 */
	@ApiModelProperty("参考上限")
	private String refHigh;


	/**
	 * 参考下限
	 */
	@ApiModelProperty("参考下限")
	private String refLow;


	/**
	 * 参考值
	 */
	@ApiModelProperty("参考值")
	private String refValue;


	/**
	 * 1是重大阳性  0正常
	 */
	@ApiModelProperty("1是重大阳性  0正常")
	private String positiveMark;


	/**
	 * 复查字段  0非复查 大于1是复查
	 */
	@ApiModelProperty("复查字段")
	private Integer review;
}
