package com.healthappy.modules.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.TPatientPoison;
import com.healthappy.modules.system.service.TPatientPoisonService;
import com.healthappy.modules.system.service.mapper.TPatientPoisonMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jevany
 * @date 2022/1/12 15:24
 * @desc
 */
@AllArgsConstructor
@Service
public class TPatientPoisonServiceImpl extends BaseServiceImpl<TPatientPoisonMapper, TPatientPoison> implements TPatientPoisonService {
    @Override
    public void savePatientPoison(String patientId, List<String> poisonIds) {
        if(StrUtil.isBlank(patientId)){
            return;
        }
        //体检号不为空则执行一下删除数据操作
        this.lambdaUpdate().eq(TPatientPoison::getPatientId,patientId).remove();
        //毒害列表不为空则插入新的
        if(ObjectUtil.isNotNull(poisonIds) && poisonIds.size()>0){
            List<TPatientPoison> tPatientPoisons=new ArrayList<>();
            for (String poisonId : poisonIds) {
                tPatientPoisons.add(new TPatientPoison(patientId,poisonId));
            }
            this.saveBatch(tPatientPoisons);
        }
    }
}