package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.*;
import com.healthappy.config.WebSocketServer;
import com.healthappy.config.databind.DataBind;
import com.healthappy.config.thread.ThreadPoolExecutorUtil;
import com.healthappy.enums.PeTypeGroupEnum;
import com.healthappy.modules.shop.service.LocalStorageService;
import com.healthappy.modules.system.domain.*;
import com.healthappy.modules.system.domain.vo.BallCheckUploadVo;
import com.healthappy.modules.system.service.*;
import com.healthappy.utils.CacheConstant;
import com.healthappy.utils.SecurityUtils;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Slf4j
@Service
@AllArgsConstructor
public class BallCheckResetServiceImpl implements BallCheckResetService {

	private final WebSocketServer socketServer;
	private final BCompanyService companyService;
	private final BDepartmentService departmentService;
	private final BDepartmentGroupHDService departmentGroupHDService;
	private final BPackageHDService packageHDService;
	private final BPeTypeHDService peTypeHDService;
	private final BPeTypeDTService peTypeDTService;
	private final BPoisonService poisonService;
	private final BPeTypeJobService peTypeJobService;
	private final BGroupHdService bGroupHdService;
	private final BallCheckService ballCheckService;
	private final DataBind dataBind;
	private final FileImportRecordService fileImportRecordService;
	private final FileImportRecordService importRecordService;
	private final LocalStorageService localStorageService;
	private final BTypeSectorHdService bTypeSectorHdService;


	//每20条数据多开一条线程
	private final int THREADS_THRESHOLD = 20;

	@Override
	@Transactional(rollbackFor = Exception.class)
	@CacheEvict(cacheNames = {CacheConstant.FILE_IMPORT_RECORD}, allEntries = true)
	public List<Map<String, Object>> upload(Integer registrationType, String appointDateStart, String appointDateEnd,
			List<BallCheckUploadVo> buildList, String fileName) {
		String username = SecurityUtils.getUsername();
		socketServer.appointSending(username, "正在检查项目{" + fileName + "},进度{0}");
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		List<CompletableFuture<List<Map<String, Object>>>> futures = new ArrayList<>();
		int listSize = buildList.size();
		int threadsNum = NumberUtil.count(listSize, THREADS_THRESHOLD);
		for (int i = 0; i < threadsNum; i++) {
			int finalI = i;
			socketServer.appointSending(username,
					"正在校验[" + finalI + "]组数据{" + fileName + "},进度{" + (int) NumberUtil.div(30, threadsNum - finalI, 0)
							+ "}");
			futures.add(CompletableFuture.supplyAsync(() -> {
				//第一步核查数据完整性
				List<Map<String, Object>> list = new ArrayList<>();
				int startValue = finalI * THREADS_THRESHOLD;
				for (int j = startValue;
					 j < calculateTheNumberOfCycles(listSize, startValue + THREADS_THRESHOLD); j++) {
					checkBasicInformation(list, buildList.get(j));
				}
				return list;
			}, ThreadPoolExecutorUtil.getPoll()));
		}
		CompletableFuture.allOf(futures.toArray(new CompletableFuture[0])).join();
		socketServer.appointSending(username, "校验通过正在上传数据{" + fileName + "},进度{40}");
		//全部通过开始执行导入
		List<Map<String, Object>> mapList = futures.stream().flatMap(f -> {
			try {
				return f.get().stream();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}).collect(Collectors.toList());

		importRecordService.submitExecl(localStorageService.uploadExecl(mapList, fileName), fileName);

		if (mapList.stream().anyMatch(map -> StrUtil.isNotBlank(map.get("备注").toString()))) {
			fileImportRecordService.lambdaUpdate().set(FileImportRecord::getIsSuccess, true)
					.eq(FileImportRecord::getFileName, fileName).set(FileImportRecord::getIsFileCorrect, false)
					.update();
			socketServer.appointSending(username, "导入失败请下载文件检查数据{" + fileName + "},进度{100}");
			return mapList;
		} else {
			Map<String, Integer> synchronizedMap = Collections.synchronizedMap(new HashMap<>());
			for (int i = 0; i < threadsNum; i++) {
				int finalI = i;
				socketServer.appointSending(username, "正在上传第[" + finalI + "]组数据,{" + fileName + "},进度{" + (
						(int) NumberUtil.div(60, threadsNum - finalI, 0) + 30) + "}");
				CompletableFuture.runAsync(() -> {
					SecurityContextHolder.getContext().setAuthentication(authentication);
					//System.out.println("通过线程池方式创建的线程：" + Thread.currentThread().getName() + "----" + SecurityUtils.getTenantId());
					int startValue = finalI * THREADS_THRESHOLD;
					List<BallCheckUploadVo> uploadVoList = new ArrayList<>();
					for (int j = startValue;
						 j < calculateTheNumberOfCycles(listSize, startValue + THREADS_THRESHOLD); j++) {
						uploadVoList.add(buildList.get(j));
					}
					ballCheckService.upload(registrationType, appointDateStart, appointDateEnd, uploadVoList);
					synchronizedMap.put(fileName, synchronizedMap.getOrDefault(fileName, 0) + 1);
				}, ThreadPoolExecutorUtil.getPoll()).thenRun(() -> {
					if (synchronizedMap.get(fileName) == threadsNum) {
						socketServer.appointSending(username, "上传完成{" + fileName + "},进度{100}");
						fileImportRecordService.lambdaUpdate().eq(FileImportRecord::getFileName, fileName)
								.set(FileImportRecord::getIsSuccess, true).update();
					}
				});
			}
		}
		return mapList;
	}


	private Integer calculateTheNumberOfCycles(Integer listSize, Integer forCount) {
		return forCount < listSize ? forCount : listSize;
	}

	/**
	 * 检测数据
	 * @author YJ
	 * @date 2022/4/29 15:24
	 * @param list
	 * @param vo
	 */
	private void checkBasicInformation(List<Map<String, Object>> list, BallCheckUploadVo vo) {
		try {
			Class<?> clz = vo.getClass();
			Field[] fields = ReflectUtil.getFields(clz);
			Map<String, Object> map = new LinkedHashMap<>();
			for (Field field : fields) {
				ApiModelProperty property = field.getAnnotation(ApiModelProperty.class);
				if ("备注".equals(property.value())) {continue;}
				map.put(property.value(), ReflectUtil.getFieldValue(vo, field));
			}
			StringBuilder append = new StringBuilder();
			if (StrUtil.isBlank(vo.getName())) {
				append.append("姓名不能为空,");
			} else if (vo.getName().length() > 12 || vo.getName().length() <= 1) {
				append.append("姓名不能长度不能小于一,不能大于十二,");
			}
			BidiMap sexBidiMap = new DualHashBidiMap(dataBind.ENUM_MAP.get("sex")).inverseBidiMap();
			Object sex = sexBidiMap.get(vo.getSex());
			if (StrUtil.isBlank(vo.getSex())) {
				append.append("性别不能为空,");
			} else if (!sexBidiMap.containsKey(vo.getSex())) {
				append.append("性别只能是").append(sexBidiMap.keySet().stream().collect(Collectors.joining(",")))
						.append(",");
			}
			if (null == vo.getAge()) {
				append.append("年龄不能为空,");
			} else if (Convert.toInt(vo.getAge()) < 1 || Convert.toInt(vo.getAge()) > 120) {
				append.append("年龄要大于一岁,要小于一百二十岁,");
			}
			BidiMap maritalBidiMap = new DualHashBidiMap(dataBind.ENUM_MAP.get("marital")).inverseBidiMap();
			if (StrUtil.isBlank(vo.getMarry())) {
				append.append("婚姻状况不能为空,");
			} else if (!maritalBidiMap.containsKey(vo.getMarry())) {
				append.append("婚姻状况只能是").append(maritalBidiMap.keySet().stream().collect(Collectors.joining(",")))
						.append(",");
			}

			BPeTypeDT typeDT = null;
			if (StrUtil.isBlank(vo.getPeTypeName())) {
				append.append("体检分类不能为空,");
			} else {
				BPeTypeHD typeHD = peTypeHDService.lambdaQuery().eq(BPeTypeHD::getName, vo.getPeTypeName())
						.eq(BPeTypeHD::getIsEnable, true).one();
				if (null == typeHD) {
					append.append("找不到该体检分类,");
				} else {
					//健康体检、儿童体检身份证可以为空
					if (!PeTypeGroupEnum.HEALTH.getValue().equals(typeHD.getGroupID())
							&& !PeTypeGroupEnum.CHILD.getValue().equals(typeHD.getGroupID())) {
						if (StrUtil.isBlank(vo.getIdNo())) {
							append.append("身份证不能为空,");
						} else if (!IdcardUtil.isValidCard(vo.getIdNo())) {
							append.append("身份证格式不正确,");
						}
						if ((PeTypeGroupEnum.PROFESSION.getValue().equals(typeHD.getGroupID())
								|| PeTypeGroupEnum.ZPHY.getValue().equals(typeHD.getGroupID())) && StrUtil.isBlank(
								vo.getTypeName())) {
							append.append("体检分类是【职业体检】,【职普合一】,体检类别必须不能为空,");
						}
					}

					if (StrUtil.isNotBlank(vo.getTypeName())) {
						if ("从业体检".equals(vo.getPeTypeName())) {
							BTypeSectorHd sectorHd = bTypeSectorHdService.lambdaQuery()
									.eq(BTypeSectorHd::getName, vo.getTypeName()).last(" limit 1").one();
							if (null == sectorHd) {
								append.append("找不到该体检类别");
							} else {
								typeDT = new BPeTypeDT();
								typeDT.setId(sectorHd.getId());
							}
						} else {
							typeDT = peTypeDTService.lambdaQuery().eq(BPeTypeDT::getHdID, typeHD.getId())
									.eq(BPeTypeDT::getName, vo.getTypeName()).one();
							if (null == typeDT) {
								append.append("找不到该体检类别");
							}
						}
					} else if (StrUtil.containsAny(vo.getPeTypeName(), "健康体检", "健康体检复查", "驾驶员体检", "儿童体检", "儿童复查体检")) {
						BPeTypeDT one = peTypeDTService.lambdaQuery().eq(BPeTypeDT::getHdID, typeHD.getId())
								.orderByAsc(BPeTypeDT::getDispOrder).last(" limit 1").one();
						if (null == one) {
							append.append("类别为空,请先配置类别");
						} else {
							typeDT = one;
						}
					}
					if (StrUtil.isNotBlank(vo.getPeTypeName()) && StrUtil.isNotBlank(vo.getPoisonTypeName())
							&& !StrUtil.equalsAny(vo.getPeTypeName(), "职业体检", "职业体检复查", "职普合一", "放射体检", "放射体检复查")) {
						append.append("体检分类只有【职业体检】,【职业体检复查】,【职普合一】【放射体检】,【放射体检复查】时,允许填写毒害,");
					}
					vo.setPeTypeId(typeHD.getId());
					vo.setTypeId(Optional.ofNullable(typeDT).map(BPeTypeDT::getId).orElse(null));

					if (!PeTypeGroupEnum.HEALTH.getValue().equals(typeHD.getGroupID()) && StrUtil.isBlank(
							vo.getMobile())) {
						append.append("手机号不能为空,");
					}
				}
			}
			if (StrUtil.isNotBlank(vo.getPoisonTypeName())) {
				List<String> typeNameList = Arrays.stream(vo.getPoisonTypeName().split("、"))
						.collect(Collectors.toList());
				vo.setPoisonTypeIds(typeNameList.stream().map(typeName -> {
					BPoison bPoison = poisonService.lambdaQuery().eq(BPoison::getPoisonName, typeName).one();
					if (null == bPoison) {
						//append.append("找不到【").append(typeName).append("】危害因素,");
					} else {
						return bPoison.getId();
					}
					return null;
				}).collect(Collectors.joining("、")));
			}
			if (StrUtil.isNotBlank(vo.getJobName())) {
				List<BPeTypeJob> jobList = peTypeJobService.lambdaQuery().eq(BPeTypeJob::getName, vo.getJobName())
						.list();
				if (CollUtil.isNotEmpty(jobList)) {
					Map<String, List<BPeTypeJob>> jobMap = jobList.stream()
							.collect(Collectors.groupingBy(BPeTypeJob::getType));
					BPeTypeDT finalTypeDT = typeDT;
					jobMap.forEach((key, value) -> {
						if (value.size() > 1) {
							append.append("数据异常,有多个【").append(value.get(0).getName()).append("】相同工种");
						}
						if (finalTypeDT != null && key.equals(finalTypeDT.getHdID().toString())) {
							vo.setJobId(value.get(0).getId());
						}
					});
				}
			}
			if (StrUtil.isNotBlank(vo.getSubtractGroupItem())) {
				List<String> groupNameList = Arrays.stream(vo.getSubtractGroupItem().split("、"))
						.collect(Collectors.toList());
				vo.setSubtractGroupItem(groupNameList.stream().map(groupName -> {
					BGroupHd groupHd = bGroupHdService.lambdaQuery().eq(BGroupHd::getName, groupName).one();
					if (null == groupHd) {
						append.append("找不到【").append(groupName).append("】找不到特殊减项目,");
					} else if (!"不限".equals(sexBidiMap.getKey(groupHd.getSex())) && ObjectUtil.isNotNull(sex)
							&& !sex.toString().equals(groupHd.getSex())) {
						append.append("特殊减项目【").append(groupName).append("】性别不匹配,");
					} else {
						return groupHd.getId();
					}
					return null;
				}).collect(Collectors.joining("、")));
			}
			if (StrUtil.isNotBlank(vo.getAddGroupName())) {
				List<String> addGroupNameList = Arrays.stream(vo.getAddGroupName().split("、"))
						.collect(Collectors.toList());
				vo.setAddGroupIds(addGroupNameList.stream().map(addGroupName -> {
					BGroupHd groupHd = bGroupHdService.lambdaQuery().eq(BGroupHd::getName, addGroupName).one();
					if (null == groupHd) {
						append.append("找不到【").append(addGroupName).append("】找不到附加项目,");
					} else if (!"不限".equals(sexBidiMap.getKey(groupHd.getSex())) && ObjectUtil.isNotNull(sex)
							&& !sex.toString().equals(groupHd.getSex())) {
						append.append("附加项目【").append(addGroupName).append("】性别不匹配,");
					} else {
						return groupHd.getId();
					}
					return null;
				}).collect(Collectors.joining("、")));
			}
			if (StrUtil.isNotBlank(vo.getAdditionPackageName())) {
				BPackageHD packageHD = packageHDService.lambdaQuery()
						.eq(BPackageHD::getName, vo.getAdditionPackageName()).one();
				if (null == packageHD) {
					append.append("找不到附加套餐,");
				} else if (!"不限".equals(sexBidiMap.getKey(packageHD.getSex())) && ObjectUtil.isNotNull(sex)
						&& !sex.toString().equals(packageHD.getSex())) {
					append.append("附加套餐【").append(packageHD.getName()).append("】性别不匹配,");
				} else {
					vo.setAdditionPackageId(packageHD.getId());
					vo.setAdditionPackagePrice(packageHD.getPrice());
				}
			}
			if (StrUtil.isNotBlank(vo.getPeTypeName()) && StrUtil.equalsAny(vo.getPeTypeName(), "职业体检", "职业体检复查",
					"放射体检", "放射体检复查", "职普合一", "从业体检")) {

			} else if (StrUtil.isAllBlank(vo.getPackageName(), vo.getDeptGroupId(), vo.getDeptGroupName())) {
				append.append("套餐或者分组必须要有一个,");
			}
			if (StrUtil.isNotBlank(vo.getPackageName())) {
				BPackageHD packageHD = packageHDService.lambdaQuery().eq(BPackageHD::getName, vo.getPackageName())
						.orderByDesc(BPackageHD::getUpdateTime).last("limit 1").one();
				if (null == packageHD) {
					append.append("找不到该套餐,");
				} else if (!"不限".equals(sexBidiMap.getKey(packageHD.getSex())) && ObjectUtil.isNotNull(sex)
						&& !sex.toString().equals(packageHD.getSex())) {
					append.append("套餐【").append(packageHD.getName()).append("】性别不匹配,");
				} else {
					vo.setPackageId(packageHD.getId());
					vo.setPackagePrice(packageHD.getPrice());
				}
			}
			BCompany company = null;
			if (StrUtil.isAllBlank(vo.getCompanyId(), vo.getCompanyName())) {
				append.append("单位名称和单位id必须有一个,");
			} else {
				company = companyService.lambdaQuery()
						.eq(StrUtil.isNotBlank(vo.getCompanyId()), BCompany::getId, vo.getCompanyId())
						.eq(StrUtil.isBlank(vo.getCompanyId()), BCompany::getName, vo.getCompanyName()).one();
				if (null == company) {
					append.append("找不到该单位,");
				} else {
					vo.setCompanyId(company.getId());
				}

			}

			BDepartment department = null;
			//公司名或公司编号不为空，且部门名不为空
			if (StrUtil.isNotBlank(vo.getDepartmentName())) {
				department = departmentService.lambdaQuery()
						.eq(StrUtil.isNotBlank(vo.getCompanyId()), BDepartment::getComId, vo.getCompanyId())
						.eq(StrUtil.isBlank(vo.getCompanyId()), BDepartment::getComId,
								Optional.ofNullable(company).map(BCompany::getId).orElse(""))
						.eq(BDepartment::getName, vo.getDepartmentName()).one();
				if (null == department) {
					//					append.append("找不到该部门,");
					department = new BDepartment();
					//如果没有部门,创建一个部门
					department.setId(
							departmentService.generateDepartmentKey(vo.getCompanyId(), SecurityUtils.getTenantId()));
					department.setComId(vo.getCompanyId());
					department.setName(vo.getDepartmentName());
					department.setIsEnable("1");
					department.setDispOrder("999");
					//department.setTenantId(SecurityUtils.getTenantId());
					departmentService.save(department);

					vo.setDepartmentId(department.getId());

					//如果有分组数据，则新建分组
					if (StrUtil.isNotBlank(vo.getDeptGroupId())) {
						vo.setDeptGroupId(
								departmentGroupHDService.copyDepartmentGroup(vo.getCompanyId(), vo.getDepartmentId(),
										vo.getDeptGroupId()));
					} else if (StrUtil.isNotBlank(vo.getDeptGroupName())) {
						BDepartmentGroupHD bDepartmentGroupHD = departmentGroupHDService.lambdaQuery()
								.eq(BDepartmentGroupHD::getCompanyId, vo.getCompanyId())
								.eq(BDepartmentGroupHD::getName, vo.getDeptGroupName())
								.select(BDepartmentGroupHD::getId).last(" limit 1").one();
						if (Optional.ofNullable(bDepartmentGroupHD).isPresent() && StrUtil.isNotBlank(
								bDepartmentGroupHD.getId())) {
							vo.setDeptGroupId(departmentGroupHDService.copyDepartmentGroup(vo.getCompanyId(),
									vo.getDepartmentId(), bDepartmentGroupHD.getId()));
						}
					}
				} else {
					vo.setDepartmentId(department.getId());
				}
			}

			BDepartmentGroupHD groupHD = null;
			if (StrUtil.isAllBlank(vo.getDeptGroupId(), vo.getDeptGroupName())) {
				// append.append("分组ID和分组名称必须有一个,");
			} else {
				groupHD = departmentGroupHDService.lambdaQuery()
						.eq(StrUtil.isNotBlank(vo.getCompanyId()), BDepartmentGroupHD::getCompanyId, vo.getCompanyId())
						.eq(StrUtil.isNotBlank(vo.getDeptGroupId()), BDepartmentGroupHD::getId, vo.getDeptGroupId())
						.eq(ObjectUtil.isNotNull(department), BDepartmentGroupHD::getDepartmentId,
								ObjectUtil.isNull(department) ? null : department.getId())
						.eq(StrUtil.isBlank(vo.getDeptGroupId()), BDepartmentGroupHD::getName, vo.getDeptGroupName())
						.last(" limit 1").one();
				if (null == groupHD) {
					append.append("找不到该分组,");
				} else if (!"不限".equals(sexBidiMap.getKey(groupHD.getSex())) && ObjectUtil.isNotNull(sex)
						&& !sex.toString().equals(groupHD.getSex())) {
					append.append("分组【").append(groupHD.getName()).append("】性别不匹配,");
				} else {
					vo.setDeptGroupId(groupHD.getId());
					if (StrUtil.isBlank(vo.getDepartmentName())) {
						append.append("部门不能为空,");
					}
				}
			}
			if (ObjectUtil.isNotNull(company) && StrUtil.isBlank(vo.getPackageName())) {
				if (null != department && !department.getComId().equals(company.getId())) {
					append.append("部门不是对应的单位下,");
				}
				if (null != groupHD && !groupHD.getCompanyId().equals(company.getId())) {
					append.append("分组不是对应的单位下,");
				}
				if (null != groupHD && null != department && !groupHD.getDepartmentId().equals(department.getId())) {
					append.append("分组不是对应的部门下,");
				}
			}
			map.put("备注", append.toString());
			list.add(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
