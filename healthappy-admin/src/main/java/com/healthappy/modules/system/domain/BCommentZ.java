package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.healthappy.config.SeedIdGenerator;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 职业结论表
 * @author fang
 * @date 2021-06-21
 */
@Data
@SeedIdGenerator
@TableName("B_Comment_Z")
public class BCommentZ implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.INPUT)
    /**
     * id
     */
    private String id;

    /**
     * 检查结果
     */
    @ApiModelProperty("检查结果")
    private String comment;

    /**
     * 建议
     */
    @ApiModelProperty("建议")
    private String suggest;

    /**
     * 排序号
     */
    @ApiModelProperty("排序号")
    private Integer dispOrder;

    /**
     * 医疗机构id(u_hospital_info的id)
     */
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;

    /**
     * code
     */
    @ApiModelProperty("编号")
    private String code;
}