package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 报告发布撤销 操作对象对象
 * @author Jevany
 * @date 2022/3/7 13:48
 */
@Data
public class ReportReleaseUnSubmitDTO extends reportReleaseReceiveDTO {

    /** 发布（操作）类型 */
    @NotBlank(message = "发布（操作）类型不能为空")
    @ApiModelProperty("发布（操作）类型,使用字典：reportReleaseType（体检报告发布类型）-数字型字符串")
    private String releaseType;

}