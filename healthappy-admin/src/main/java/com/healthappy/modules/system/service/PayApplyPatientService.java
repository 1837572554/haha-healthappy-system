package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TChargeComApplyPatient;
import com.healthappy.modules.system.service.dto.CheckoutListExportDTO;
import com.healthappy.modules.system.service.dto.CompanyPayApplyPatientCriteria;
import com.healthappy.modules.system.service.dto.PayApplyPatientServiceCriteria;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface PayApplyPatientService extends BaseService<TChargeComApplyPatient> {

    Map<String, Object> getList(CompanyPayApplyPatientCriteria criteria);



    /**
    * @Author: FGQ
    * @Desc: 结账
    * @Date: 2022/3/24
    */
    void savePayPatient(TChargeComApplyPatient vo);

    /**
     * 重新保存方法，增加保存物价信息
     * @param patients
     */
    void savePatient(Set<TChargeComApplyPatient> patients);

    /**
     * 导出excle
     * @param criteria
     * @return
     */
    List<CheckoutListExportDTO> export(CompanyPayApplyPatientCriteria criteria);
}
