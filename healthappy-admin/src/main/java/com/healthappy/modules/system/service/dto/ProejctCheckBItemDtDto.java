package com.healthappy.modules.system.service.dto;

import cn.hutool.core.util.ObjectUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class ProejctCheckBItemDtDto implements Comparable<ProejctCheckBItemDtDto> {

    /** 主键 */
    @ApiModelProperty("主键")
    private String id;

    /** 项目ID */
    @ApiModelProperty("项目ID")
    private String itemId;

    /** 项目名称 */
    @ApiModelProperty("项目名称")
    private String itemName;

    /** 结果 */
    @ApiModelProperty("结果")
    private String result;

    /** 项目编码 */
    @ApiModelProperty("项目编码")
    private String code;

    /** 结果类型0文本,1数值 */
    @ApiModelProperty("结果类型0文本,1数值")
    private String resultType;
    /**
     * 正常结果
     */
    @ApiModelProperty("正常结果")
    private String normalResult;

    /** 判定 */
    @ApiModelProperty("判定")
    private String resultMark;

    /** 单位 */
    @ApiModelProperty("单位")
    private String unit;

    /** 参考值 */
    @ApiModelProperty("参考值")
    private String refValue;

    /** 是否进入小结 */
    @ApiModelProperty("是否进入小结")
    private Boolean isSummary;

    /** 规则集合 */
    @ApiModelProperty("规则集合")
    private List<SicknessByItemDto> sicknessRuleList;


    /** 显示顺序 */
    @ApiModelProperty("显示顺序")
    private Integer dispOrder;

    /**
     * 之前保存的病种数据
     */
    @ApiModelProperty("之前保存的病种数据")
    private List<ItemSicknessMatchDto> itemSicknessMatchDtoList;


    @Override
    public int compareTo(ProejctCheckBItemDtDto o) {
        if(ObjectUtil.isNull(this.getDispOrder())){
            this.setDispOrder(999);
        }
        if(ObjectUtil.isNull(o.getDispOrder())){
            o.setDispOrder(999);
        }
        return this.getDispOrder().compareTo(o.getDispOrder());
    }
}
