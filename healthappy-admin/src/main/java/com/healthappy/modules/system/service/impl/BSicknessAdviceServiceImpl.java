package com.healthappy.modules.system.service.impl;

import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.BSicknessAdvice;
import com.healthappy.modules.system.service.BSicknessAdviceService;
import com.healthappy.modules.system.service.mapper.BSicknessAdviceMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BSicknessAdviceServiceImpl extends BaseServiceImpl<BSicknessAdviceMapper, BSicknessAdvice> implements BSicknessAdviceService {


}
