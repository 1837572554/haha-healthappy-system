package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 体检报告发布列表对象
 *
 * @author Jevany
 * @date 2022/3/3 14:47
 */
@Data
public class ReportReleaseDTO implements Serializable {
    /**
     * 流水号
     */
    @ApiModelProperty("流水号")
    private String paId;
    /**
     * 姓名
     */
    @ApiModelProperty("姓名")
    private String name;
    /**
     * 性别
     */
    @ApiModelProperty("性别")
    private String sex;
    /**
     * 年龄
     */
    @ApiModelProperty("年龄")
    private Integer age;
    /**
     * 单位
     */
    @ApiModelProperty("单位")
    private String companyName;

    /**
     * 部门
     */
    @ApiModelProperty("部门")
    private String departmentName;

    /**
     * 分组
     */
    @ApiModelProperty("分组")
    private String departmentGroupName;

    /**
     * 打印医生
     */
    @ApiModelProperty("打印医生")
    private String printDoctor;

    /**
     * 上传医生
     */
    @ApiModelProperty("上传医生")
    private String uploadDoctor;

    /**
     * 领取医生
     */
    @ApiModelProperty("领取医生")
    private String receiveDoctor;

}