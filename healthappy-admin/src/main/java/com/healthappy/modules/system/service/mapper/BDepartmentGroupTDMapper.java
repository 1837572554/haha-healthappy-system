package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BDepartmentGroupTD;
import com.healthappy.modules.system.service.dto.BDepartmentGroupTDQueryCriteria;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 分组项目表
 * @author sjc
 * @date 2021-06-25
 */
@Mapper
@Repository
public interface BDepartmentGroupTDMapper extends CoreMapper<BDepartmentGroupTD> {

    /**
     * 根据分组id删除所有的分组项目
     * @param department_group_hd_id 分组id
     * @return
     */
    boolean deleteBDepartmentGroupTD(@Param("id") String department_group_hd_id);

    /**
     * 保存分组项目
     * @param list
     * @return
     */
    boolean insertSelective(List<BDepartmentGroupTD> list);

    /**
     * 获取分组下的数组 并排序
     * @author YJ
     * @date 2021/12/17 14:01
     * @param criteria
     * @return java.util.List〈com.healthappy.modules.system.domain.BDepartmentGroupTD〉
     */
    List<BDepartmentGroupTD> getBDepartmentGroup(@Param("criteria") BDepartmentGroupTDQueryCriteria criteria);


    /**
     * 根据多个分组id来获取分组下的数组 并排序
     * @author YJ
     * @date 2021/12/17 14:01
     * @param criteria
     * @return java.util.List〈com.healthappy.modules.system.domain.BDepartmentGroupTD〉
     */
    List<BDepartmentGroupTD> getBDepartmentGroupIn(@Param("criteria") BDepartmentGroupTDQueryCriteria criteria);
}
