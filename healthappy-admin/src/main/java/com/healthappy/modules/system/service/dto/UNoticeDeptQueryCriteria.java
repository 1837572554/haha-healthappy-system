package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @description 公告对应科室表
 * @author sjc
 * @date 2021-09-3
 */
@Data
public class UNoticeDeptQueryCriteria {

    /**
     * 公告id
     */
    @ApiModelProperty(value = "公告id",position = 1)
    @Query(type = Query.Type.EQUAL)
    private Long noticeId;

    /**
     * 科室id
     */
    @ApiModelProperty(value = "科室id",position = 2)
    @Query(type = Query.Type.EQUAL)
    private String deptId;

}
