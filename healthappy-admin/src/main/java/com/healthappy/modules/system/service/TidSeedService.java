package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TidSeed;

/**
 * @description ID种子
 * @author FGQ
 * @date 2021-06-17
 */
public interface TidSeedService extends BaseService<TidSeed> {

    /**
     * 创建WorkStation的编号
     * @author YJ
     * @date 2022/2/17 16:02
     * @param tableName
     * @return java.lang.String
     */
    String createASeedPrimaryKey(String tableName);

    /**
     * 创建没有WorkStation的编号
     * @author YJ
     * @date 2022/2/17 16:02
     * @param tableName
     * @param keyLength
     * @return java.lang.String
     */
    String createASeendPrimaryKeyNoWorkStation(String tableName,Integer keyLength);
}
