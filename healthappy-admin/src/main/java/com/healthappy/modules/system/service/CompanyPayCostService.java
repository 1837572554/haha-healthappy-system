package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TChargeCOMApplyWujia;
import com.healthappy.modules.system.domain.TChargeComApply;
import com.healthappy.modules.system.service.dto.CompanyPayCostCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface CompanyPayCostService extends BaseService<TChargeComApply>  {

    Map<String,Object> getList(CompanyPayCostCriteria criteria, Pageable pageable);

    /**
     * 查询所有
     * @param criteria
     * @return
     */
    List<TChargeComApply> getAll(CompanyPayCostCriteria criteria);

    /**
     * 保存物价
     * @param priceList
     */
    void savePrice(List<TChargeCOMApplyWujia> priceList);
}
