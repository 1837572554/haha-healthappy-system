package com.healthappy.modules.system.domain.vo;

import com.healthappy.modules.system.service.dto.TDctDataDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @desc: 电测听数据Vo
 * @author: YJ
 * @date: 2021-12-02 15:37
 **/
@Data
public class TDctDataVo {

    /** 体检编号 */
    @ApiModelProperty(value = "体检编号",required = true)
    @NotBlank(message = "体检编号必填")
    private String paId;

    /** 正常数据列表 */
    @ApiModelProperty("正常数据列表")
    List<TDctDataDto> data;

    /** 修正数据列表 */
    @ApiModelProperty("修正数据列表")
    List<TDctDataDto> dataXz;
}
