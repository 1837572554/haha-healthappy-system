package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 单位职业报告管理-人员查询类
 * @author Jevany
 * @date 2022/2/18 9:56
 */
@Data
public class CompanyReportOccupationPersonQueryCriteria extends CompanyReportPersonBase{
    /**
     * 是否上岗前 0：否，1：是 默认0
     */
    @ApiModelProperty("是否上岗前 0：否，1：是 默认0")
    private Integer isBefore = 0;

    /**
     * 是否在岗期间 0：否，1：是 默认0
     */
    @ApiModelProperty("是否在岗期间 0：否，1：是 默认0")
    private Integer isOnJob = 0;

    /**
     * 是否离岗时 0：否，1：是 默认0
     */
    @ApiModelProperty("是否离岗时 0：否，1：是 默认0")
    private Integer isAfter = 0;

    /**
     * 是否应急 0：否，：1是 默认0
     */
    @ApiModelProperty("是否应急 0：否，：1是 默认0")
    private Integer isEmergency = 0;
}