package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA. Created by yutang 2021/9/7 0007  16:12 Description:
 */
@Data
public class TAppointToExcelDto implements Serializable {

    @ApiModelProperty("预约号")
    private String id;

    @ApiModelProperty("人员名称")
    private String name;

    @ApiModelProperty("性别")
    private String sex;

    @ApiModelProperty("年龄")
    private Integer age;

    @ApiModelProperty("身份证号")
    private String idNo;

    @ApiModelProperty("婚姻状况")
    private String marry;

    @ApiModelProperty("手机号")
    private String mobile;

    @ApiModelProperty("部门名称")
    private String departmentName;

    @ApiModelProperty("工种")
    private String job;

    @ApiModelProperty("毒害种类")
    private String poisonType;

    @ApiModelProperty("套餐名称")
    private String packageName;

    @ApiModelProperty("附加套餐")
    private String addPackage;

    @ApiModelProperty("体检分类")
    private String peType;

    @ApiModelProperty("体检类别")
    private String type;

    @ApiModelProperty("社保号")
    private String ssNo;

    @ApiModelProperty("单位名称")
    private String companyName;

    @ApiModelProperty("预约类型")
    private String appointType;

    @ApiModelProperty("备注")
    private String mark;

    @ApiModelProperty("分组名称")
    private String deptGroupName;

    @ApiModelProperty("特殊减项目")
    private String subtractGroupItem;

    @ApiModelProperty("附加组合项目")
    private String addGroupItem;

}
