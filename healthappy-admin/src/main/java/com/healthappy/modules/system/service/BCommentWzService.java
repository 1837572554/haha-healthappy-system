package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BCommentWz;
import com.healthappy.modules.system.service.dto.BCommentWzQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 既往病 服务层
 * @author fang
 * @date 2021-06-17
 */
public interface BCommentWzService extends BaseService<BCommentWz> {


    Map<String, Object> queryAll(BCommentWzQueryCriteria criteria, Pageable pageable);


    /**
     * 查询数据分页
     * @param criteria 条件
     * @return Map<String, Object>
     */
    List<BCommentWz> queryAll(BCommentWzQueryCriteria criteria);
}
