package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.config.WebSocketServer;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BSickness;
import com.healthappy.modules.system.domain.TCritical;
import com.healthappy.modules.system.domain.TPatient;
import com.healthappy.modules.system.domain.User;
import com.healthappy.modules.system.domain.vo.ProjectCheckVo;
import com.healthappy.modules.system.service.*;
import com.healthappy.modules.system.service.dto.*;
import com.healthappy.modules.system.service.mapper.TCriticalMapper;
import com.healthappy.utils.FileUtil;
import com.healthappy.utils.PageUtil;
import com.healthappy.utils.SecurityUtils;
import com.healthappy.utils.StringUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Jevany
 * @date 2022/2/9 18:17
 */
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class TCriticalServiceImpl extends BaseServiceImpl<TCriticalMapper, TCritical> implements TCriticalService {
    /**
     * 体检者服务
     */
    private final TPatientService tPatientService;

    /**
     * 危急值数据操作
     */
    private final TCriticalMapper tCriticalMapper;

    /**
     * 明细项服务
     */
    private final TItemDTService itemDTService;

    /**
     * 危急值通知 标识
     */
    private static final String CRITICAL_NOTIFICATION = "critical";

    /**
     * 危急值 通知权限
     */
    private static final String CRITICAL_PERMIISSION = "Critical:notification";

    /**
     * 危急值权限标识
     */
    private static final String CRITICAL_NOTIFICATION_SIGN = "critical_notification";

    /**
     * WebSocket
     */
    private final WebSocketServer webSocketServer;

    /**
     * 用户服务
     */
    private final UserService userService;

    private final IGenerator generator;

    /**
     * Tag权限
     */
    private final TagService tagService;

    /**
     * 病种服务
     */
    private final BSicknessService sicknessService;

    @Override
    public void checkCritical(ProjectCheckVo projectCheckVo) {
        if (CollUtil.isEmpty(projectCheckVo.getItemSicknessList())) {
            return;
        }

        ProjectCheckBItemHdDto hdDto = projectCheckVo.getCheckBItemHdDto();
        List<ProejctCheckBItemDtDto> checkBItemDtDtoList = projectCheckVo.getCheckBItemDtDtoList();
        TPatient patient = tPatientService.getById(hdDto.getPaId());

        projectCheckVo.getItemSicknessList().forEach(itemSickness -> {
            if ("1".equals(Optional.ofNullable(itemSickness.getIsCritical()).orElse("0") )) {
                //说明是危急值,新增表数据
                TCritical tCritical = new TCritical();
                tCritical.setPaId(hdDto.getPaId());
                tCritical.setIdNo(patient.getIdNo());
                tCritical.setDeptId(hdDto.getDeptId());
                tCritical.setGroupId(hdDto.getGroupId());
                tCritical.setItemId(itemSickness.getItemId());
                tCritical.setSicknessId(itemSickness.getSicknessId());
                tCritical.setCheckDoctor(SecurityUtils.getNickName());
                tCritical.setOccuTime(new Timestamp(System.currentTimeMillis()));
                tCritical.setTenantId(SecurityUtils.getTenantId());
                //获取项目的结果值
                ProejctCheckBItemDtDto itemDT = checkBItemDtDtoList.stream().filter(dt -> dt.getItemId().equals(itemSickness.getItemId())).findAny().orElse(null);

//                TItemDT itemDT = itemDTService.getOne(Wrappers.<TItemDT>lambdaQuery().eq(TItemDT::getPaId, hdDto.getPaId())
//                        .eq(TItemDT::getGroupId, hdDto.getGroupId()).eq(TItemDT::getItemId, itemSickness.getItemId()));
                if (Optional.ofNullable(itemDT).isPresent()) {
                    String detail = itemDT.getItemName() + ":" + itemDT.getResult()
                            + (StringUtils.isNotBlank(itemDT.getUnit()) ? " " + itemDT.getUnit() : "")
                            + (StringUtils.isNotBlank(itemDT.getRefValue()) ? " " + itemDT.getRefValue() : "");

                    BSickness bSickness = sicknessService.getById(itemSickness.getSicknessId());
                    if (Optional.ofNullable(bSickness).isPresent()) {
                        if (!bSickness.getName().equals(itemDT.getResult())) {
                            detail += " " + bSickness.getName();
                        }
                    }

                    tCritical.setDetail(detail);
                }
                this.save(tCritical);
                CriticalNotificationDTO criticalNotificationDTO = BeanUtil.copyProperties(tCritical, CriticalNotificationDTO.class);
                criticalNotificationDTO.setName(patient.getName());
                sendCriticalNotification(criticalNotificationDTO, SecurityUtils.getUserId());
            }
        });
    }


    @Override
    public void testCriteriaNotification(Long criticalId) {
        CriticalObjDTO criticalObj = tCriticalMapper.getCriticalObj(criticalId);
        CriticalNotificationDTO criticalNotificationDTO = BeanUtil.copyProperties(criticalObj, CriticalNotificationDTO.class);
        sendCriticalNotification(criticalNotificationDTO, null);
    }

    /**
     * 发送危急通知
     *
     * @param criticalNotificationDTO
     * @param checkDoctorId
     * @author YJ
     * @date 2022/2/28 17:25
     */
    @Override
    public void sendCriticalNotification(CriticalNotificationDTO criticalNotificationDTO, Long checkDoctorId) {

        //危急值编号空，直接退出
        if (ObjectUtil.isNull(criticalNotificationDTO.getId())) {
            return;
        }

        /** 获取需要发送的人员 */
        List<User> permissionUsers = tagService.getUsersByTagPer(CRITICAL_NOTIFICATION_SIGN,criticalNotificationDTO.getTenantId());

        //获取当前危急值的处理医生
        if (ObjectUtil.isNotNull(checkDoctorId)) {
            UserDto userDTO = userService.findById(checkDoctorId);
            if (StringUtils.isNotBlank(userDTO.getUsername())) {
                User user = generator.convert(userDTO, User.class);
                permissionUsers.add(user);
            }
        }

        if (CollUtil.isEmpty(permissionUsers)) {
            return;
        }

        //过滤重复人员
        permissionUsers = permissionUsers.stream().distinct().collect(Collectors.toList());
        for (User permissionUser : permissionUsers) {
            webSocketServer.sendMessage(permissionUser.getUsername(), JSON.toJSONString(criticalNotificationDTO), CRITICAL_NOTIFICATION);
        }
    }


    @Override
    public Map<String, Object> getCriticalList(CriticalQueryCriteria criteria) {
        criteria.setIsExport(false);

        //总条数
        Integer totalCount = tCriticalMapper.getCriticalCount(criteria);
        Integer[] pageBase = null;

        if (ObjectUtil.isNotNull(criteria.getIsExport()) && criteria.getIsExport().equals(false)) {
            pageBase = PageUtil.toStartEnd(criteria.getPageNum(), criteria.getPageSize(), totalCount);
            criteria.setStartIndex(pageBase[0]);
        }

        List<CriticalDTO> criticalList = tCriticalMapper.getCriticalList(criteria);

        if (criteria.getIsExport().equals(false)) {
            Map<String, Object> map = new LinkedHashMap<>(5);
            map.put("content", criticalList);
            map.put("totalElements", totalCount);
            map.put("totalPage", pageBase[3]);
            map.put("currentPage", pageBase[2]);
            map.put("pageSize", criteria.getPageSize());
            return map;
        } else {
            Map<String, Object> map = new LinkedHashMap<>(1);
            map.put("content", criticalList);
            return map;
        }
    }

    @Override
    public void export(List<CriticalDTO> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        Integer idx = 0;
        for (CriticalDTO dto : all) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("序号", ++idx);
            map.put("流水号", dto.getPaId());
            map.put("姓名", dto.getName());
            map.put("性别", dto.getSex());
            map.put("年龄", dto.getAge());
            map.put("身份证号", dto.getIdNo());
            map.put("电话", dto.getPhone());
            map.put("单位", dto.getCompanyName());
            map.put("部门", dto.getDeptName());
            map.put("发生科室", dto.getOccuDept());
            map.put("项目", dto.getGroupName());
            map.put("异常类型", dto.getAffectLevel());
            map.put("具体情况", dto.getDetail());
            map.put("发生时间", dto.getOccuTime());
            map.put("处理时间", dto.getDealTime());
            map.put("处理医生", dto.getDealDoctor());
            map.put("通知方式", dto.getNoticeWay());
            map.put("转诊科室", dto.getTransferDept());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    @Override
    public CriticalObjDTO getCriteria(Long id) {
        return tCriticalMapper.getCriticalObj(id);
    }

    @Override
    public Integer editCriteria(CriticalObjDTO critical) {
        if (StringUtils.isBlank(critical.getDealDoctor())) {
            critical.setDealDoctor(SecurityUtils.getNickName());
        }
        //字符串转时间类型
        if (StringUtils.isNotBlank(critical.getOccuTime())) {
            critical.setOccuTimestamp(Timestamp.valueOf(critical.getOccuTime()));
        }
        return tCriticalMapper.editCritical(critical);
    }

    @Override
    public Boolean deleteCriteria(Long id) {
        return this.removeById(id);
    }

    @Override
    public void listNeedNotificationCritical() {
        List<CriticalObjDTO> criticalList = baseMapper.listNeedNotificationCritical();
        for (CriticalObjDTO criticalObj : criticalList) {
            sendCriticalNotification(BeanUtil.copyProperties(criticalObj, CriticalNotificationDTO.class), null);
        }
    }
}