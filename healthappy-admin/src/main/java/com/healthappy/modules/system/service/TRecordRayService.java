package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TRecordRay;
import com.healthappy.modules.system.service.dto.TRecordRayQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 职业史放射表
 * @author sjc
 * @date 2021-12-15
 */
public interface TRecordRayService extends BaseService<TRecordRay> {


    Map<String, Object> queryAll(TRecordRayQueryCriteria criteria, Pageable pageable);

    List<TRecordRay> queryAll(TRecordRayQueryCriteria criteria);
}
