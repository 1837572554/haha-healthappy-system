package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.config.databind.FieldBind;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author sjc
 * @description 体检人员表
 * @date 2021-06-28
 */
@Data
@ApiModel("体检人员表")
@TableName("T_Patient")
@RenewLog
public class TPatient implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    @TableId(type = IdType.INPUT)
    private String id;


    /**
     * 姓名
     */
    @ApiModelProperty("姓名")
    private String name;

    /**
     * 体检分类表id
     */
    @ApiModelProperty("体检分类表id")
    private Long peTypeHdId;

    /**
     * 体检类别表id
     */
    @ApiModelProperty("体检类别表id")
    private Long peTypeDtId;
    /**
     * 单位编号
     */
    @ApiModelProperty("单位编号")
    private String companyId;

    /**
     * 体检日期
     */
    @ApiModelProperty("体检日期")
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp peDate;

    /**
     * 生日
     */
    @ApiModelProperty("生日")
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp birthday;

    /**
     * 性别表id 1.男 2.女
     */
    @ApiModelProperty("性别表id")
    @FieldBind(target = "sexText")
    private String sex;

    @TableField(exist = false)
    private String sexText;

    /**
     * 年龄
     */
    @ApiModelProperty("年龄")
    private Integer age;

    /**
     * 身份证
     */
    @ApiModelProperty("身份证")
    private String idNo;

    /**
     * 社保号
     */
    @ApiModelProperty("社保号")
    private String ssNo;

    /**
     * 电话
     */
    @ApiModelProperty("电话")
    private String phone;
    /**
     * 地址
     */
    @ApiModelProperty("地址")
    private String address;

    /**
     * 工种ID
     */
    @ApiModelProperty("工种ID")
    private String job;

    /**
     * 工种名称
     */
    @ApiModelProperty("工种名称")
    private String jobName;

    /**
     * 婚姻情况
     */
    @ApiModelProperty("婚姻情况表id")
    @FieldBind(target = "maritalText")
    private String marital;

    @TableField(exist = false)
    private String maritalText;
    /**
     * 部门/车间
     */
    @ApiModelProperty("部门/车间")
    private String workshop;

    /**
     * 工号
     */
    @ApiModelProperty("工号")
    private String code;

    /**
     * 总工龄
     */
    @ApiModelProperty("总工龄")
    private String totalYears;
    /**
     * 缴费方式
     */
    @ApiModelProperty("缴费方式")
    @FieldBind(type = "sffs", target = "chargeTypeText")
    private Integer chargeType;

    @TableField(exist = false)
    private String chargeTypeText;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String mark;

    /**
     * 医疗机构ID(U_Hospital_Info的ID)
     */
    @ApiModelProperty("医疗机构ID")
    private String hospitalId;
    /**
     * 工作站
     */
    @ApiModelProperty("工作站")
    private String workstation;

    /**
     * 评价
     */
    @ApiModelProperty("评价")
    private String comment;

    /**
     * 建议
     */
    @ApiModelProperty("建议")
    private String suggest;

    /**
     * 职普合一，职业结论
     */
    @ApiModelProperty("职普合一，职业综述")
    private String commentZy;
    /**
     * 职普合一，职业建议
     */
    @ApiModelProperty("职普合一，职业建议")
    private String suggestZy;

    /**
     * 职普合一，职业结论
     */
    @ApiModelProperty("职普合一，普通综述")
    private String commentPt;
    /**
     * 职普合一，职业建议
     */
    @ApiModelProperty("职普合一，普通建议")
    private String suggestPt;


    /**
     * 主检医生
     */
    @ApiModelProperty("主检医生")
    private String conclusionDoc;

    /**
     * 主检日期
     */
    @ApiModelProperty("主检日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp conclusionDate;

    /**
     * 打印医生
     */
    @ApiModelProperty("打印医生")
    private String printDoctor;
    /**
     * 打印日期
     */
    @ApiModelProperty("打印日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private String printDate;

    /**
     * 审核医生
     */
    @ApiModelProperty("审核医生")
    private String verifyDoc;

    /**
     * 审核时间
     */
    @ApiModelProperty("审核时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp verifyDate;
    /**
     * 报告领取医生
     */
    @ApiModelProperty("报告医生")
    private String reportDoctor;

    /**
     * 报告领取日期
     */
    @ApiModelProperty("报告日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp reportDate;

    /**
     * 套餐名称
     */
    @ApiModelProperty("套餐名称")
    private String packageName;
    /**
     * 部门id
     */
    @ApiModelProperty("部门id")
    private String departmentId;

    /**
     * 统收限额
     */
    @ApiModelProperty("统收限额")
    private Double tsxe;

    /**
     * 分组id
     */
    @ApiModelProperty("分组id")
    private String departmentGroupId;
    /**
     * 套餐价格
     */
    @ApiModelProperty("套餐价格")
    private Double packagePrice;

    /**
     * 附加套餐价格
     */
    @ApiModelProperty("附加套餐价格")
    private Double packageAdditionPrice;

    /**
     * 合计价格
     */
    @ApiModelProperty("合计价格")
    private Double totalPrice;

    /**
     * 签到时间,第一次体检时间（登记时设置，批量登记时不设置，做第一次项目时设置）
     */
    @ApiModelProperty("签到时间,第一次体检时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp signDate;

    /**
     * 平台病人唯一索引号
     */
    @ApiModelProperty("平台病人唯一索引号")
    private String hisNo;

    /**
     * 登记医生
     */
    @ApiModelProperty("登记医生")
    private String registerDoctor;
    /**
     * 年龄日期格式，格式:1岁1月17天
     */
    @ApiModelProperty("年龄日期格式，格式:1岁1月17天")
    private String ageDate;

    /**
     * 指引单是否交回 0否1是
     */
    @ApiModelProperty("指引单是否交回 0否1是")
    private String isSubmit;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty("指引单回交时间")
    private Timestamp isSubmitTime;

    @ApiModelProperty("指引单操作人员")
    private String isSubmitBy;

    /**
     *
     */
    @ApiModelProperty("状态 1未总检 2已总检 3已审核 4撤销总检")
    private String isStatus;
    /**
     * 封存时间
     */
    @ApiModelProperty("封存时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp uptowzhTime;

    /**
     * 封存医生
     */
    @ApiModelProperty("封存医生")
    private String uptowzhUser;

    /**
     * 是否封存
     */
    @ApiModelProperty("是否封存")
    private String wzhlock;
    /**
     * 转诊科室
     */
    @ApiModelProperty("转诊科室")
    private Integer transfer;
    /**
     * 统收价格
     */
    @ApiModelProperty("统收价格")
    private Double tsxePric;
    /**
     * 人员来源
     */
    @ApiModelProperty("人员来源")
    private String personnelSource;
    /**
     * 从业体检-是否合格 0否1是
     */
    @ApiModelProperty("从业体检-是否合格 0否1是")
    private String isNormal;
    /**
     * 健康证编号
     */
    @ApiModelProperty("健康证编号")
    private Long certIficateId;
    /**
     * 任务来源
     */
    @ApiModelProperty("任务来源")
    private String charsi;
    /**
     * 结算归口
     */
    @ApiModelProperty("结算归口")
    private Integer settlement;
    /**
     * 电测听暗记  0为正常未操作  1为疑似职业病    2为职业禁忌
     */
    @ApiModelProperty("电测听暗记  0为正常未操作  1为疑似职业病    2为职业禁忌")
    private String dctAb;
    /**
     * 职位
     */
    @ApiModelProperty("职位")
    private String duty;
    /**
     * 打印次数
     */
    @ApiModelProperty("打印次数")
    private Integer printNumer;
    /**
     * 打印份数-一次打印可能会打印多次
     */
    @ApiModelProperty("打印份数")
    private Integer printPart;

//    /**
//     * 上传医事通的时间
//     */
//    @ApiModelProperty("上传医事通的时间")
//    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
//    private Timestamp uploadingDate;


    /**
     * 文化程度
     */
    @ApiModelProperty("文化程度")
    private String culture;
    /**
     * 照射种类
     */
    @ApiModelProperty("照射种类")
    private String radiationType;
    /**
     * 邮编
     */
    @ApiModelProperty("邮编")
    private String postcode;
    /**
     * 出生地
     */
    @ApiModelProperty("出生地")
    private String birthplace;
    /**
     * 档案号，一个身份证对应一个
     */
    @ApiModelProperty("档案号，一个身份证对应一个")
    private Long recordId;
    /**
     * 所属区域
     */
    @ApiModelProperty("所属区域")
    private String area;
    /**
     * 预约对应的微信号
     */
    @ApiModelProperty("预约对应的微信号")
    private String appointWxId;


    /**
     * 所属地区
     */
    @ApiModelProperty("所属地区")
    private String region;
    /**
     * 套餐编号
     */
    @ApiModelProperty("套餐编号")
    private String packageId;
    /**
     * 微信
     */
    @ApiModelProperty("微信")
    private Integer wechat;
    /**
     * 微信时间
     */
    @ApiModelProperty("微信时间")
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp wechatTime;
    /**
     * 微信用户
     */
    @ApiModelProperty("微信用户")
    private String wechatUser;
    /**
     * 体检收费单头表主健ID
     */
    @ApiModelProperty("体检收费单头表主健ID")
    private Long fheadId;
    /**
     * 真实检查时间 都使用SignDate
     */
    @Deprecated
    @ApiModelProperty("真实检查时间 都使用SignDate")
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp peDateReal;

    /**
     * 真实签到时间 都使用SignDate
     */
    @Deprecated
    @ApiModelProperty("真实签到时间 都使用SignDate")
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp signDateReal;
    /**
     * 指引单打印时间
     */
    @ApiModelProperty("指引单打印时间")
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp printDateZyd;
    /**
     * 单位收费表关联ID
     */
    @ApiModelProperty("单位收费表关联ID")
    private Long isCheckOut;
    /**
     * 是否处理锁定
     */
    @ApiModelProperty("是否处理锁定")
    private String isLock;
    /**
     * 主检医生code
     */
    @ApiModelProperty("主检医生code")
    private String conclusionDocCode;
    /**
     * 回访时间
     */
    @ApiModelProperty("回访时间")
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp phonevisitDate;

    /**
     * 病人ID,HIS中病人唯一标识
     */
    @ApiModelProperty("病人ID,HIS中病人唯一标识")
    private Long hisPatientId;
    /**
     * 门诊号,HIS中门诊唯一标识
     */
    @ApiModelProperty("门诊号,HIS中门诊唯一标识")
    private Long hisClinicId;
    /**
     * 健康证编号
     */
    @ApiModelProperty("健康证编号")
    private String healthCert;
//    /**
//     * 平台上传时间
//     */
//    @ApiModelProperty("平台上传时间")
//    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
//    private Timestamp uploadExamTime;
    /**
     * 民族代码
     */
    @ApiModelProperty("民族代码")
    private String nation;
    /**
     * 证件类型
     */
    @ApiModelProperty("证件类型")
    private String idType;
    /**
     * HIS 门诊号码
     */
    @ApiModelProperty("HIS 门诊号码")
    private String hisMzhm;
    /**
     * 健康证打印医生
     */
    @ApiModelProperty("健康证打印医生")
    private String printHealthCert;
    /**
     * 生长发育状况
     */
    @ApiModelProperty("生长发育状况")
    private String growthStatus;
    /**
     * 健康证编号下发失败次数
     */
    @ApiModelProperty("健康证编号下发失败次数")
    private Integer healthCertFailNum;
    /**
     * 健康证编号下发失败原因
     */
    @ApiModelProperty("健康证编号下发失败原因")
    private String healthCertFailReason;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField(fill = FieldFill.UPDATE)
    private Timestamp updateTime;
    /**
     * 业务员ID
     */
    @ApiModelProperty("业务员ID")
    private Long salesmanId;
    /**
     * 业务员姓名
     */
    @ApiModelProperty("业务员姓名")
    private String salesmanName;
    /**
     * IMCIS平台数据
     */
    @ApiModelProperty("IMCIS平台数据")
    private String pacsMasterId;
    /**
     *
     */
    @ApiModelProperty("")
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp updated;
    /**
     * 年龄单位，岁，默认为空
     */
    @ApiModelProperty("年龄单位，岁，默认为空")
    private String ageUnit;
    /**
     * 总价格
     */
    @ApiModelProperty("总价格")
    private Double origTotalPrice;
    /**
     * 总服务费
     */
    @ApiModelProperty("总服务费")
    private Double origTotalServiceFee;


    /**
     * 总实收
     */
    @ApiModelProperty("总实收")
    private Double origTotalCost;

    /**
     *
     */
    @ApiModelProperty("")
    private Double totalOptCost;

    /**
     * 总统收
     */
    @ApiModelProperty("总统收")
    private Double totalTsPrice;

    /**
     * 操作时间
     */
    @ApiModelProperty("操作时间")
    private String created;

    /**
     * 收费状态
     */
    @ApiModelProperty("收费状态")
    private Integer chargeStatus;

    /**
     * 收费统收状态
     */
    @ApiModelProperty("收费统收状态")
    private Integer chargeTsStatus;

    /**
     * 总服务费
     */
    @ApiModelProperty("收费统收ID")
    private Long chargeTsId;

    /**
     * 总服务费
     */
    @ApiModelProperty("总服务费")
    private Double totalServiceFee;

    @ApiModelProperty("初检号")
    private Long historyNo;

    @ApiModelProperty("审核医生code")
    private Long verifyCode;

    /**
     * 报告是否上传
     */
    @ApiModelProperty("报告是否上传")
    private String reportUpload;

    /**
     * 上传时间
     */
    @ApiModelProperty("上传时间")
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp uploadTime;

    /**
     * 上传医生
     */
    @ApiModelProperty("上传医生")
    private String uploadDoctor;

    /**
     * 医疗机构ID(U_Hospital_Info的ID)
     */
    @TableField(value = "tenant_id", fill = FieldFill.INSERT)
    private String tenantId;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT)
    private Timestamp createTime;

    /**
     * 附加项目总价格
     */
    @ApiModelProperty(value = "附加项目总价格")
    private Double additionPrice;

    @ApiModelProperty(value = "是否携带组合项目")
    @TableField(exist = false)
    private Boolean carryItemHd = Boolean.FALSE;

    /**
     * 体检时间类型
     */
    @ApiModelProperty("体检时间类型")
    @TableField(exist = false)
    private Integer peDateType;

    @ApiModelProperty("身份证图片")
    @TableField(exist = false)
    private String idImage;

    @ApiModelProperty("人像")
    @TableField(exist = false)
    private String portrait;

    @ApiModelProperty("医生分组数量")
    @TableField(exist = false)
    private Long docNum;
}
