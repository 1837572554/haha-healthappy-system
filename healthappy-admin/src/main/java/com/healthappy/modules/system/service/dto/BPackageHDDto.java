package com.healthappy.modules.system.service.dto;

import com.healthappy.modules.system.domain.BPackageDT;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @description 套餐表
 * @author sjc
 * @date 2021-07-2
 */
@Data
public class BPackageHDDto implements Serializable {

    /**
     *套餐id
     */
    private String id;

    /**
     * 名称
     */
    private String name;

    /**
     * 体检类型
     */
    private String type;

    /**
     * 适用性别
     */
    private String sex;

    /**
     * 是否启用
     */
    private String isEnable;

    /**
     * 排序号
     */
    private Integer dispOrde;

    /**
     * 描述
     */
    private String desc;

    /**
     * 套餐价格
     */
    private BigDecimal price;

    /**
     * 套餐折扣
     */
    private BigDecimal discount;

    /**
     * 套餐应付价格
     */
    private BigDecimal cost;

    /**
     * 简拼
     */
    private String jp;

    /**
     * 图片
     */
    private String iamge;

    private String typeText;

    private String sexText;

    /**
     * 服务费
     */
    private BigDecimal serviceFee;
    /**
     * 套餐详情
     */
    private List<BPackageDT> bPackageDt;

    @ApiModelProperty("包含项目数量")
    private Integer itemCount;
}
