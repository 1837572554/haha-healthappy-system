package com.healthappy.modules.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BSicknessZH;
import com.healthappy.modules.system.domain.BWuJia;
import com.healthappy.modules.system.service.BWuJiaService;
import com.healthappy.modules.system.service.dto.BSicknessZHDto;
import com.healthappy.modules.system.service.dto.BSicknessZHQueryCriteria;
import com.healthappy.modules.system.service.dto.BWuJiaDto;
import com.healthappy.modules.system.service.dto.BWuJiaQueryCriteria;
import com.healthappy.modules.system.service.mapper.BWuJiaMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @description 物价表 服务层实现
 * @author SJC
 * @date 2021-08-3
 */
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BWuJiaServiceImpl extends BaseServiceImpl<BWuJiaMapper, BWuJia> implements BWuJiaService {

    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BWuJiaQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BWuJia> page = new PageInfo<BWuJia>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content",generator.convert(page.getList(), BWuJiaDto.class));
        map.put("totalElements", page.getTotal());
        return map;
    }

    @Override
    public List<BWuJia> queryAll(BWuJiaQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BWuJiaDto.class, criteria));
    }


    @Override
    public boolean deleteById(Long id) {
        return baseMapper.deleteById(id)>0;
    }
}
