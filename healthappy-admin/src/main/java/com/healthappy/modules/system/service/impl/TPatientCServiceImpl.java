package com.healthappy.modules.system.service.impl;

import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.TPatientC;
import com.healthappy.modules.system.service.TPatientCService;
import com.healthappy.modules.system.service.mapper.TPatientCMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class TPatientCServiceImpl extends BaseServiceImpl<TPatientCMapper, TPatientC> implements TPatientCService {


}
