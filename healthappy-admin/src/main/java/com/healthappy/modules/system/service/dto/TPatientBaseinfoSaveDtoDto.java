package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author Jevany
 * @date 2022/1/20 19:41
 * @desc
 */
@Data
@Builder
public class TPatientBaseinfoSaveDtoDto  implements Serializable {
    /**
     * 身份证
     */
    @NotBlank(message = "身份证不能为空")
    @ApiModelProperty(value = "身份证")
    private String idNo;

    /**
     * 姓名
     */
    @NotBlank(message = "姓名不能为空")
    @ApiModelProperty(value = " 姓名")
    private String name;

    /**
     * 体检分类表id
     */
    @ApiModelProperty("体检分类表id")
    private Long peTypeHdId;
}