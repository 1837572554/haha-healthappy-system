package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BGroupHd;
import com.healthappy.modules.system.service.dto.BGroupHdQueryCriteria;
import com.healthappy.modules.system.service.dto.DeptBusinessDTO;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @description 项目组合 服务层
 * @author fang
 * @date 2021-06-17
 */
public interface BGroupHdService extends BaseService<BGroupHd> {


    Map<String, Object> queryAll(BGroupHdQueryCriteria criteria, Pageable pageable);

    /**
     * 查询数据分页
     * @param criteria 条件
     * @return Map<String, Object>
     */
    List<BGroupHd> queryAll(BGroupHdQueryCriteria criteria);

    boolean removeByIds(Set<String> ids);

    /**
     * 获取指定日期的（项目类型：影像、B超、仪器）预约，登记的科室,项目
     * @author YJ
     * @date 2022/3/15 16:35
     * @param dateStr
     * @return java.util.List〈com.healthappy.modules.system.service.dto.DeptBusinessDTO〉
     */
    List<DeptBusinessDTO> listDeptBusiness(String dateStr);

}
