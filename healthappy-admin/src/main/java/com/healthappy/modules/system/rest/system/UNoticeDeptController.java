package com.healthappy.modules.system.rest.system;

import cn.hutool.core.bean.BeanUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.UNoticeDept;
import com.healthappy.modules.system.domain.vo.UNoticeDeptVo;
import com.healthappy.modules.system.service.UNoticeDeptService;
import com.healthappy.modules.system.service.dto.UNoticeDeptQueryCriteria;
import com.healthappy.utils.CacheConstant;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description 公告对应科室表 控制器
 * @author fang
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "系统设置：公告对应科室表")
@RestController
@AllArgsConstructor
@RequestMapping("/UNoticeDept")
public class UNoticeDeptController {

    private final UNoticeDeptService uNoticeDeptService;

    @Log("查询公告科室")
    @ApiOperation("查询公告科室，权限码：UNotice:list")
    @GetMapping
    @PreAuthorize("@el.check('UNotice:list')")
    public R list(UNoticeDeptQueryCriteria criteria, @PageableDefault(sort = {"notice_id"})  Pageable pageable) {
        return R.ok(uNoticeDeptService.queryAll(criteria, pageable));
    }

    @Log("新增|修改公告科室")
    @ApiOperation("新增|修改公告科室，权限码：UNotice:add")
    @PostMapping
    @PreAuthorize("@el.check('UNotice:add')")
    @Klock
    @CacheEvict(cacheNames = {CacheConstant.U_NOTICE_DEPT},allEntries = true)
    public R saveOrUpdate(@Validated @RequestBody UNoticeDeptVo resources) {
        remove(resources.getNoticeId());
        return R.ok(uNoticeDeptService.save(BeanUtil.copyProperties(resources, UNoticeDept.class)));
    }

    @Log("删除公告")
    @ApiOperation("删除公告，权限码：UNotice:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('UNotice:delete')")
    @Klock
    public R remove(@RequestBody String id) {
        return R.ok(uNoticeDeptService.deleteByUnoticeId(id));
    }
}
