package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotBlank;
import lombok.Data;

/**
 * Created with IntelliJ IDEA. Created by yutang 2021/9/7 0007  10:26 Description:
 */
@ApiModel(value = "缴费类型保存模型")
@Data
public class BTypePayVo {

  @ApiModelProperty("主键id")
  private Long id;

  @ApiModelProperty("名称")
  @NotBlank(message = "名称不可为空")
  private String name;

  @ApiModelProperty("排序")
  private Integer dispOrder;

}
