package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.security.security.vo.OnlineUser;
import com.healthappy.modules.security.service.OnlineUserService;
import com.healthappy.modules.system.domain.Tenant;
import com.healthappy.modules.system.domain.User;
import com.healthappy.modules.system.service.BAreaService;
import com.healthappy.modules.system.service.BPetypeInstitutionService;
import com.healthappy.modules.system.service.TenantService;
import com.healthappy.modules.system.service.UserService;
import com.healthappy.modules.system.service.dto.BAreaQueryCriteria;
import com.healthappy.modules.system.service.dto.TenantDto;
import com.healthappy.modules.system.service.dto.TenantQueryCriteria;
import com.healthappy.modules.system.service.mapper.TenantMapper;
import com.healthappy.utils.CacheConstant;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
@CacheConfig(cacheNames = {CacheConstant.TENANT})
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class TenantServiceImpl extends BaseServiceImpl<TenantMapper, Tenant> implements TenantService {

	private final IGenerator generator;

	private final BPetypeInstitutionService petypeInstitutionService;

	private final BAreaService bAreaService;

	@Override
	@Cacheable
	public Map<String, Object> queryAll(TenantQueryCriteria criteria, Pageable pageable) {
		getPage(pageable);
		PageInfo<Tenant> page = new PageInfo<Tenant>(queryAll(criteria));
		Map<String, Object> map = new LinkedHashMap<>(2);
		map.put("content", buildList(generator.convert(page.getList(), TenantDto.class)));
		map.put("totalElements", page.getTotal());
		return map;
	}

	private List<TenantDto> buildList(List<TenantDto> convert) {
		if (CollUtil.isNotEmpty(convert)) {
			for (TenantDto dto : convert) {
				if (StrUtil.isNotBlank(dto.getInstiType())) {
					dto.setBPetypeInstitution(petypeInstitutionService.getById(dto.getInstiType()));
				}
				if (ObjectUtil.isNotNull(dto.getAreaId())) {
					dto.setAreaTree(
							bAreaService.tree(BAreaQueryCriteria.builder().id(dto.getAreaId().longValue()).build()));
				}
			}
		}
		return convert;
	}

	@Override
	@Cacheable
	public List<Tenant> queryAll(TenantQueryCriteria criteria) {
		return baseMapper.selectList(QueryHelpPlus.getPredicate(Tenant.class, criteria));
	}

	@Override
	@Cacheable
	public String generateTenantId() {
		return getTenantId(baseMapper.getTenantIds());
	}

	private final UserService userService;

	private final OnlineUserService onlineUserService;

	@Override
	public void kickOutTenant(Tenant tenant) {
		List<User> userList = userService.list(
				Wrappers.<User>lambdaQuery().eq(User::getTenantId, tenant.getTenantId()));
		if (CollUtil.isNotEmpty(userList)) {
			List<OnlineUser> onlineUserList = onlineUserService.getAll("", 0);
			if (CollUtil.isNotEmpty(onlineUserList)) {
				List<String> userNames = userList.stream().map(User::getUsername).collect(Collectors.toList());
				List<OnlineUser> onlineUsers = onlineUserList.stream()
						.filter(online -> userNames.contains(online.getUserName())).collect(Collectors.toList());
				onlineUsers.forEach(onlineUser -> {
					try {
						onlineUserService.kickOut(onlineUser.getKey());
					} catch (Exception e) {
						e.printStackTrace();
					}
				});
			}
		}
	}


	/**
	 * 删除租户
	 * @param ids 租户编号集合
	 */
	@Override
	@CacheEvict(allEntries = true)
	public void removeByTenantIds(Set<Long> ids) throws Exception {
		if (userService.count(new LambdaQueryWrapper<User>().in(User::getTenantId, ids.toArray())) > 0) {
			new BadRequestException("所选租户下，有关联的用户，无法删除");
		}
		this.removeByIds(ids);
	}

	@Override
	@Cacheable(value = "appointSwitch", key = "#tenantId")
	public String getAppointSwitch(String tenantId) {
		return Optional.ofNullable(
						this.lambdaQuery().eq(Tenant::getTenantId, tenantId).select(Tenant::getAppointSwitch).one())
				.map(Tenant::getAppointSwitch).orElse("0");
	}

	private String getTenantId(List<String> codes) {
		String code = RandomUtil.randomNumbers(6);
		if (codes.contains(code)) {
			return getTenantId(codes);
		}
		return code;
	}


}
