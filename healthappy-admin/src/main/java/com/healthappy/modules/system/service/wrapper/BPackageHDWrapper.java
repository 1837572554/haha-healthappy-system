package com.healthappy.modules.system.service.wrapper;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.healthappy.modules.system.domain.BGroupHd;
import com.healthappy.modules.system.domain.BPackageDT;
import com.healthappy.modules.system.domain.BPackageHD;
import com.healthappy.modules.system.service.BGroupHdService;
import com.healthappy.modules.system.service.BPackageDTService;
import com.healthappy.modules.system.service.dto.BPackageHDDto;
import com.healthappy.utils.SpringUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BPackageHDWrapper extends BaseEntityWrapper<BPackageHD, BPackageHDDto>{

    private final static BPackageDTService bPackageDTService;
    private final static BGroupHdService bGroupHdService;

    static {
        bGroupHdService = SpringUtil.getBean(BGroupHdService.class);
        bPackageDTService = SpringUtil.getBean(BPackageDTService.class);
    }

    public static BPackageHDWrapper build(){ return new BPackageHDWrapper();}

    @Override
    public BPackageHDDto entityVO(BPackageHD entity) {
        BPackageHDDto dto = BeanUtil.copyProperties(entity,BPackageHDDto.class);
        List<BPackageDT> groupDispOrder = bPackageDTService.getGroupDispOrder(entity.getId());
        if(CollUtil.isNotEmpty(groupDispOrder)){
            dto.setBPackageDt(groupDispOrder);
            dto.setItemCount(groupDispOrder.size());
        } else {
            dto.setBPackageDt(new ArrayList<>());
            dto.setItemCount(0);
        }
        return dto;
    }

    /**
     * 加载列表中的组合项名称
     * @author YJ
     * @date 2021/12/9 13:09
     * @param packageDTList
     * @return java.util.List〈com.healthappy.modules.system.domain.BPackageDT〉
     */
    public List<BPackageDT> loadGroupHdName(List<BPackageDT> packageDTList) {
        return packageDTList.stream().peek(dt -> {
            BGroupHd groupHd = bGroupHdService.getById(dt.getGroupId());
            dt.setGroupName(Optional.ofNullable(groupHd).map(BGroupHd::getName).orElse(""));
        }).collect(Collectors.toList());
    }
}
