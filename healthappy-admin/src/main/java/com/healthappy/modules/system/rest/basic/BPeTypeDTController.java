package com.healthappy.modules.system.rest.basic;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BPeTypeDT;
import com.healthappy.modules.system.service.BPeTypeDTService;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Api(tags = "基础数据：体检类别")
@ApiSort(13)
@RestController
@AllArgsConstructor
@RequestMapping("BPeTypeDT")
public class BPeTypeDTController {
    private  final BPeTypeDTService bPeTypeDTService;

    @Log("体检分类")
    @ApiOperation("全部体检分类数据，权限码：BPeTypeDT:list")
    @GetMapping("/list")
    public R list() {
        return R.ok(bPeTypeDTService.list());
    }

    @Log("体检分类父ID查询")
    @ApiOperation("体检分类数据，权限码：BPeTypeDT:detai")
    @GetMapping("/listByHdID")
    public R listByHdId(@RequestParam(value = "hdId") Long hdId) {
        return R.ok(bPeTypeDTService.list(Wrappers.<BPeTypeDT>lambdaQuery().eq(BPeTypeDT::getHdID,hdId).orderByAsc(BPeTypeDT::getDispOrder)));
    }

}
