package com.healthappy.modules.system.service.dto;

import com.healthappy.modules.system.domain.Tag;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class TagDto extends Tag {

    List<Long> signIds;
}
