package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 区域表（阿斯利康）
 * @author fang
 * @date 2021-06-17
 */
@Data
@TableName("B_Area")
public class BArea implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
     * id
     */
    private Long id;

    /**
     * 编码
     */
    private String code;

    /**
     * 区域名称
     */
    private String name;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 是否启用
     */
    private String isEnable;

    /**
     * 排序
     */
    private Integer dispOrder;


    @ApiModelProperty("上级区域编码")
    private String hiCode;
}
