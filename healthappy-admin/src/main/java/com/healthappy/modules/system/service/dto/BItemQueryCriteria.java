package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BItemQueryCriteria {

    @Query(blurry = "name,jp")
    @ApiModelProperty(value = "项目名称|模糊搜索")
    private String name;

    @Query
    @ApiModelProperty("科室ID")
    private String deptId;

    @Query(type = Query.Type.NOT_EQUAL)
    @ApiModelProperty("不适用性别(1：限制男，2：限制女 不传不限制  例：传1，则不显示只适用于男的项目)")
    private String sex;

    @Query
    @ApiModelProperty("是否启用")
    private String isEnable;

    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("是否删除 空不限制,0：未删除，1：删除。默认0")
    private String delFlag="0";

}
