package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BTypeItem;
import com.healthappy.modules.system.service.dto.BTypeItemQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 项目类型 服务层
 * @author sjc
 * @date 2021-08-3
 */
public interface BTypeItemService extends BaseService<BTypeItem> {

    Map<String, Object> queryAll(BTypeItemQueryCriteria criteria, Pageable pageable);

    List<BTypeItem> queryAll(BTypeItemQueryCriteria criteria);

    boolean deleteById(Long id);
}
