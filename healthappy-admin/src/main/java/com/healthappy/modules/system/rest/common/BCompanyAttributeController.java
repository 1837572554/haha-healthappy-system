package com.healthappy.modules.system.rest.common;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BCompanyAttribute;
import com.healthappy.modules.system.domain.vo.BCompanyAttributeVo;
import com.healthappy.modules.system.service.BCompanyAttributeService;
import com.healthappy.modules.system.service.dto.BCompanyAttributeQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description 单位属性 控制器
 * @author sjc
 * @date 2021-08-30
 */
@Slf4j
@Api(tags = "默认基础数据：单位属性")
@RestController
@AllArgsConstructor
@RequestMapping("/BCompanyAttribute")
public class BCompanyAttributeController {

    private final BCompanyAttributeService bCompanyAttributeService;

    @Log("分页查询单位属性")
    @ApiOperation("分页查询单位属性，权限码：BCompanyAttribute:list")
    @GetMapping
    public R listPage(BCompanyAttributeQueryCriteria criteria, Pageable pageable) {
        return R.ok(bCompanyAttributeService.queryAll(criteria, pageable));
    }

    @Log("新增|修改单位属性")
    @ApiOperation("新增|修改单位属性，权限码：admin")
    @PostMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BCompanyAttributeVo resources) {
        //如果名称重复
        if(checkBCompanyAttributeName(resources)) {
            return R.error(500,"该单位属性名称已经存在");
        }
        BCompanyAttribute bCompanyAttribute = new BCompanyAttribute();
        BeanUtil.copyProperties(resources,bCompanyAttribute);
        if(bCompanyAttributeService.saveOrUpdate(bCompanyAttribute))
            return R.ok();
        return R.error(500,"方法异常");
    }

    @Log("删除单位属性")
    @ApiOperation("删除单位属性，权限码：admin")
    @DeleteMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R remove(@RequestParam("id") Long id) {
        if(bCompanyAttributeService.removeById(id))
            return R.ok();
        return R.error(500,"方法异常");
    }

    /**
     * 用于判断名称是否存在
     * @param resources
     * @return
     */
    private Boolean checkBCompanyAttributeName(BCompanyAttributeVo resources) {
        //名称相同  id不同的情况下能查询到数据，那么就是名称重复
        return bCompanyAttributeService.lambdaQuery().eq(BCompanyAttribute::getName,resources.getName())
                .ne(ObjectUtil.isNotNull(resources.getId()),BCompanyAttribute::getId,resources.getId()).count() > 0;

    }
}
