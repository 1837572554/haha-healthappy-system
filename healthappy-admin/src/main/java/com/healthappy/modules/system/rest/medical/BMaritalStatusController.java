package com.healthappy.modules.system.rest.medical;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BMaritalStatus;
import com.healthappy.modules.system.domain.vo.BMaritalStatusVo;
import com.healthappy.modules.system.service.BMaritalStatusService;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA. Created by yutang 2021/9/2 0002  11:06 Description:
 */
@Slf4j
@Api(tags = "体检管理：婚姻状况接口")
@RestController
@AllArgsConstructor
@RequestMapping("/BMaritalStatus")
public class BMaritalStatusController {

  private final BMaritalStatusService tMaritalStatusService;

  @Log("新增|修改婚姻状况")
  @ApiOperation("新增|修改婚姻状况，权限码：TMaritalStatus:add")
  @PostMapping
  @PreAuthorize("@el.check('TMaritalStatus:add')")
  @Klock
  public R saveOrUpdate(@Validated @RequestBody BMaritalStatusVo bMaritalStatusVo) {
    BMaritalStatus bMaritalStatus = new BMaritalStatus();
    BeanUtil.copyProperties(bMaritalStatusVo, bMaritalStatus);
    if(checkCode(bMaritalStatus)){
      return R.error(500, "存在重复的代码名称！");
    }
    if (tMaritalStatusService.saveOrUpdate(bMaritalStatus)) return R.ok();
    return R.error(500, "方法异常");
  }

  @Log("所有婚姻状况列表")
  @ApiOperation(value = "所有婚姻状况列表，权限码：BMaritalStatus:list")
  @GetMapping
  @PreAuthorize("@el.check('BMaritalStatus:list')")
  public R listAll() {
    return R.ok(tMaritalStatusService.list());
  }

  /**
   * 检查code重复
   * @param bMaritalStatus
   * @return
   */
  boolean checkCode(BMaritalStatus bMaritalStatus){
    QueryWrapper<BMaritalStatus> queryWrapper = new QueryWrapper<>();
    queryWrapper.eq("code", bMaritalStatus.getCode());
    if (bMaritalStatus.getId() != null && bMaritalStatus.getId() != 0) {//修改
      queryWrapper.ne("id", bMaritalStatus.getId());
    }
    if (tMaritalStatusService.getOne(queryWrapper) != null) {
      return true;
    }
    return false;
  }

}
