package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 条码设置表
 * @author sjc
 * @date 2021-12-13
 */
@Data
public class BBarcodeQueryCriteria  {


    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty(value = "项目类型",position = 2)
    private String groupHdType;
}
