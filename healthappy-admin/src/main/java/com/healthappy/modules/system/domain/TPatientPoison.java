package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


@ApiModel("体检毒害编号关联表")
@Data
@TableName("T_Patient_Poison")
@RenewLog
public class TPatientPoison implements Serializable {
    /**
     * 体检编号
     */
    @ApiModelProperty(value = "体检编号")
    private String patientId;

    /**
     * 毒害编号
     */
    @ApiModelProperty(value = "毒害编号")
    private String poisonId;

    /**
     * 医疗机构ID(U_Hospital_Info的ID)
     */
    @ApiModelProperty(value = "医疗机构ID(U_Hospital_Info的ID)")
    @TableField(value = "tenant_id", fill = FieldFill.INSERT)
    private String tenantId;

    public TPatientPoison(){}

    public TPatientPoison(String patientId,String poisonId){
        this.setPatientId(patientId);
        this.setPoisonId(poisonId);
    }

    private static final long serialVersionUID = 1L;
}
