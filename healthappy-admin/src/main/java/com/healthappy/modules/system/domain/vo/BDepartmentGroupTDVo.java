package com.healthappy.modules.system.domain.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 分组项目表
 * @author sjc
 * @date 2021-06-24
 */
@Data
public class BDepartmentGroupTDVo implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 分组id
     */
    //@NotNull(message = "分组id不能为空")
    @ApiModelProperty(value = "分组id",required = true)
    private String departmentGroupHdId;


    /**
     * 组合项目id
     */
    @ApiModelProperty("组合项目id")
    private String groupId;

    /**
     * 项目原价
     */
    @ApiModelProperty("项目原价")
    private Double price;

    /**
     * 折扣
     */
    @ApiModelProperty("折扣")
    @TableField("discount")
    private Double discount;
    /**
     * 应付金额
     */
    @ApiModelProperty("应付金额")
    private Double cost;
    /**
     * 是否是职检项目 0否1是
     */
    @ApiModelProperty("是否是职检项目 0否1是")
    private String typeZ;

    /**
     * 是否是普检项目 0否1是
     */
    @ApiModelProperty("是否是普检项目 0否1是")
    private String typeJ;

    /** 租户ID */
    @ApiModelProperty(hidden = true)
    private String tenantId;
}
