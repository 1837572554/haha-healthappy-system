package com.healthappy.modules.system.service.impl;


import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.BNation;
import com.healthappy.modules.system.service.BNationService;
import com.healthappy.modules.system.service.mapper.BNationMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author FGQ
 * @date 2021-10-27
 */
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BNationServiceImpl extends BaseServiceImpl<BNationMapper, BNation> implements BNationService {

    
}
