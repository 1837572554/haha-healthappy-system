package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.Sign;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 标识
 * @author FGQ
 * @date 2021-06-17
 */
@Mapper
@Repository
public interface SignMapper extends CoreMapper<Sign> {

}
