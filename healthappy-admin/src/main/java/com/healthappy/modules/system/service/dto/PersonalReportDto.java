package com.healthappy.modules.system.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class PersonalReportDto implements Serializable {

    @ApiModelProperty("流水号")
    private String id;

    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("性别id")
    private String sex;

    @ApiModelProperty("性别")
    private String sexText;

    @ApiModelProperty(value = "登记时间-体检数据插入时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp createTime;

    @ApiModelProperty("电话")
    private String phone;

    @ApiModelProperty("民族代码CODE")
    private String nation;

    @ApiModelProperty("民族代码")
    private String nationName;

    @ApiModelProperty("年龄")
    private Integer age;

    @ApiModelProperty("婚姻")
    private String marital;

    @ApiModelProperty("婚姻")
    private String maritalText;

    @ApiModelProperty("单位编号")
    private String companyId;

    @ApiModelProperty("单位名称")
    private String companyName;

    @ApiModelProperty("部门ID")
    private String departmentId;

    @ApiModelProperty("部门名称")
    private String departmentName;

    @ApiModelProperty("分组ID")
    private String departmentGroupId;

    @ApiModelProperty("分组名称")
    private String departmentGroupName;

    @ApiModelProperty("总工龄")
    private String totalYears;

    @ApiModelProperty("体检分类ID")
    private String peTypeHdId;

    @ApiModelProperty("体检分类名称")
    private String peTypeHdName;

    @ApiModelProperty("体检类别ID")
    private String peTypeDtId;

    @ApiModelProperty("体检类别名称")
    private String peTypeDtName;

    @ApiModelProperty("套餐名称")
    private String packageName;

    @ApiModelProperty("套餐金额")
    private Double packagePrice;

    @ApiModelProperty("接害工龄")
    private String workYears;

    @ApiModelProperty("接害种类")
    private String poisonType;
}
