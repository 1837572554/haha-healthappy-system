package com.healthappy.modules.system.rest.basic;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BGroupDT;
import com.healthappy.modules.system.domain.BItem;
import com.healthappy.modules.system.domain.vo.BItemVo;
import com.healthappy.modules.system.service.BGroupDTService;
import com.healthappy.modules.system.service.BItemService;
import com.healthappy.modules.system.service.dto.BItemQueryCriteria;
import com.healthappy.utils.CacheConstant;
import com.healthappy.utils.R;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 体检项目设置控制器
 * @author FGQ
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "基础管理：体检项目设置")
@RestController
@ApiSort(4)
@AllArgsConstructor
@RequestMapping("/BItem")
public class BItemController {

	private final BItemService bItemService;

	private final BGroupDTService bGroupDTService;

	@Log("获取全部体检项目信息")
	@ApiOperation("获取全部体检项目设置，权限码：BItem:list")
	@GetMapping
	public R list(BItemQueryCriteria criteria, @PageableDefault(value = Integer.MAX_VALUE, sort = {
			"update_time"}, direction = Sort.Direction.DESC) Pageable pageable) {
		return R.ok(bItemService.queryAll(criteria, pageable));
	}

	@Log("体检项目信息")
	@ApiOperation("体检项目信息，权限码：BItem:list")
	@GetMapping("/list")
	public R list(BItemQueryCriteria criteria) {
		return R.ok(bItemService.queryAll(criteria));
	}

	@Log("新增|修改体检项目设置")
	@ApiOperation("新增|修改体检项目设置，权限码：BItem:add")
	@PostMapping
	@PreAuthorize("@el.check('BItem:add')")
	@Klock
	@CacheEvict(cacheNames = {CacheConstant.B_ITEM}, allEntries = true)
	public R saveOrUpdate(@Validated @RequestBody BItemVo resources) {
		if (checkNameRepeat(resources)) {
			return R.error("项目名称不能重复");
		}
		BItem bItem = BeanUtil.copyProperties(resources, BItem.class);

		return R.ok(bItemService.saveOrUpdate(bItem));
	}

	@Log("体检项目tree")
	@ApiOperation("体检项目tree，权限码：BItem:list")
	@GetMapping("/tree")
	@PreAuthorize("@el.check('BItem:list')")
	public R list() {
		return R.ok(bItemService.getTree());
	}

	@Log("删除体检项目设置")
	@ApiOperation("删除体检项目设置，权限码：BItem:delete")
	@DeleteMapping
	@PreAuthorize("@el.check('BItem:delete')")
	@Klock
	@CacheEvict(cacheNames = {CacheConstant.B_ITEM}, allEntries = true)
	public R remove(@RequestBody Set<Integer> ids) {
		checkIfItExists(ids);
		return R.ok(bItemService.removeByIds(ids));
	}

	@Log("根据组合项目获取明细列表")
	@ApiOperation("根据组合项目获取明细列表，权限码：BItem:list")
	@GetMapping("/listByGroupId")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "groupId", value = "组合编号", required = true, dataType = "String")})
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "明细项列表", response = BItem.class, responseContainer = "List")})
	@PreAuthorize("@el.check('BItem:list')")
	public R listByGroupId(@RequestParam String groupId) {
		return R.ok(bItemService.listByGroupId(groupId));
	}


	/**
	 * 校验名称是否重复
	 * @param resources 实体
	 */
	private boolean checkNameRepeat(BItemVo resources) {
		return bItemService.lambdaQuery().eq(BItem::getDeptId, resources.getDeptId())
				.eq(BItem::getName, resources.getName())
				.ne(ObjectUtil.isNotNull(resources.getId()), BItem::getId, resources.getId()).count() > 0;
	}

	private void checkIfItExists(Set<Integer> ids) {
		if (bGroupDTService.lambdaQuery().in(BGroupDT::getItemId, ids).count() > 0) {
			throw new BadRequestException("组合项目中 子项 存在删除的体检项目,请先删除组合项目中的子项");
		}
	}
}
