package com.healthappy.modules.system.service.wrapper;

import com.github.pagehelper.PageInfo;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 视图包装基类
 *
 * @author FGQ
 */
public abstract class BaseEntityWrapper<E, V> {

    /**
     * 单个实体类包装
     *
     * @param entity 实体类
     * @return V
     */
    public abstract V entityVO(E entity);

    /**
     * 实体类集合包装
     *
     * @param list 集合
     * @return List V
     */
    public List<V> listVO(List<E> list) {
        return list.stream().map(this::entityVO).collect(Collectors.toList());
    }

    /**
     * 分页实体类集合包装
     *
     * @param pages 分页
     * @return Page V
     */
    public Map<String, Object> pageVO(PageInfo<E> pages) {
        List<V> records = listVO(pages.getList());
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", records);
        map.put("totalElements", pages.getTotal());
        return map;
    }
}