package com.healthappy.modules.system.service.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author hupeng
 * @date 2020-05-14
 */
@Data
public class BAreaDto implements Serializable {

}
