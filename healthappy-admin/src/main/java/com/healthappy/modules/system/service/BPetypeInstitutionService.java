package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BPetypeInstitution;
import com.healthappy.modules.system.domain.BSicknessRule;
import com.healthappy.modules.system.service.dto.BPetypeInstitutionCriteria;
import com.healthappy.modules.system.service.dto.BSicknessRuleQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author FGQ
 * @date 2020-05-14
 */
public interface BPetypeInstitutionService extends BaseService<BPetypeInstitution> {


    Object queryAll(BPetypeInstitutionCriteria criteria, Pageable pageable);

    /**
     * 查询数据分页
     * @param criteria 条件
     * @return Map<String, Object>
     */
    List<BPetypeInstitution> queryAll(BPetypeInstitutionCriteria criteria);
}
