package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.Tenant;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Repository
@Mapper
public interface TenantMapper extends CoreMapper<Tenant> {


    /**
     * 查询所有租户标识
     * @return
     */
    @SqlParser(filter = true)
    @Select("SELECT tenant_id FROM tenant")
    List<String> getTenantIds();
}
