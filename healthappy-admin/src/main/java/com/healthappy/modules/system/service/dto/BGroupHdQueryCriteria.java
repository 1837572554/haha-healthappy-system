package com.healthappy.modules.system.service.dto;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import com.healthappy.annotation.Query;
import com.healthappy.config.databind.DataBind;
import com.healthappy.utils.SpringUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.bidimap.DualHashBidiMap;

import java.util.ArrayList;
import java.util.List;

@Data
public class BGroupHdQueryCriteria {

    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("组合id")
    private String id;

    @Query(blurry = "name,jp", type = Query.Type.INNER_LIKE)
    @ApiModelProperty("组合名称：name，简拼：jp")
    private String name;

    @Query
    @ApiModelProperty("类别")
    private String type;

    @Query(type = Query.Type.IN, propName = "type")
    @ApiModelProperty("类别列表-对应字典数值")
    private List<String> typeList;

    @Query
    @ApiModelProperty(value = "科室编号", position = 2)
    private String deptId;


    @ApiModelProperty(value = "是否妇科检查", hidden = true)
    @Query(type = Query.Type.EQUAL)
    private Boolean women;

    @ApiModelProperty("婚否")
    private String isMarried;

    @Query(type = Query.Type.IN)
    @ApiModelProperty(value = "性别", hidden = true)
    private List<Integer> sex;

    @ApiModelProperty("前端接收 - 性别")
    private Integer receiveSex;

    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("是否删除 空不限制,0：未删除，1：删除。默认0")
    private String delFlag = "0";

    /**
     * 如果是已婚女性 显示所有项目,其他则不显示妇科项目
     *
     * @return
     */
    public Boolean getWomen() {
        DataBind dataBind = SpringUtil.getBean(DataBind.class);
        BidiMap sexBidiMap = new DualHashBidiMap(dataBind.ENUM_MAP.get("sex")).inverseBidiMap();
        BidiMap maritalBidiMap = new DualHashBidiMap(dataBind.ENUM_MAP.get("marital")).inverseBidiMap();
        return Convert.toInt(sexBidiMap.get("女")).equals(receiveSex) && maritalBidiMap.get("已婚").equals(isMarried);
    }

    /**
     * 如果有传值就带上不限
     *
     * @return
     */
    public List<Integer> getSex() {
        if (ObjectUtil.isNotNull(receiveSex)) {
            sex = new ArrayList<>();
            DataBind dataBind = SpringUtil.getBean(DataBind.class);
            BidiMap sexBidiMap = new DualHashBidiMap(dataBind.ENUM_MAP.get("sex")).inverseBidiMap();
            sex.add(Convert.toInt(sexBidiMap.get("不限")));
            sex.add(receiveSex);
        }
        return sex;
    }
}
