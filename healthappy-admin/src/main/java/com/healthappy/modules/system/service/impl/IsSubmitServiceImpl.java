package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.TItemHD;
import com.healthappy.modules.system.domain.TPatient;
import com.healthappy.modules.system.service.IsSubmitService;
import com.healthappy.modules.system.service.TItemHDService;
import com.healthappy.modules.system.service.dto.IsSubmitDto;
import com.healthappy.modules.system.service.dto.IsSubmitQueryCriteria;
import com.healthappy.modules.system.service.mapper.TPatientMapper;
import com.healthappy.utils.SecurityConstants;
import com.healthappy.utils.SecurityUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author: FGQ
 * @Desc: 指引单回交
 * @Date: Created in 15:38 2022/3/7
 */
@Service
@RequiredArgsConstructor
public class IsSubmitServiceImpl extends BaseServiceImpl<TPatientMapper, TPatient> implements IsSubmitService {

    private final IGenerator generator;

    private final TItemHDService tItemHDService;

    @Override
    public Map<String, Object> pageAll(IsSubmitQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<TPatient> page = new PageInfo<TPatient>(getList(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", generator.convert(page.getList(), IsSubmitDto.class));
        map.put("totalElements", page.getTotal());
        return map;
    }

    @Override
    public List<TPatient> getList(IsSubmitQueryCriteria criteria) {
        QueryWrapper<TPatient> wrapper = QueryHelpPlus.getPredicate(TPatient.class, criteria);
            if(ObjectUtil.isNotNull(criteria.getTimeType()) && CollUtil.isNotEmpty(criteria.getTime()) && criteria.getTime().size()==2 ) {
                wrapper.between(criteria.getTimeType().getDesc(), criteria.getTime().get(0), criteria.getTime().get(1));
            }
            if(ObjectUtil.isNotNull(criteria.getIsSubmit())) {
                if (!criteria.getIsSubmit()) {
                    wrapper.and(p -> p.isNull("is_submit").or().eq("is_submit", "0"));
                }
                else {
                    wrapper.eq("is_submit",1);
                }
            }
//        }
        return baseMapper.selectList(wrapper);
    }

    @Override
    public IsSubmitDto detail(String id) {
        TPatient patient = this.getById(id);
        if (patient == null) {
            throw new BadRequestException("查不到数据");
        }
        List<TItemHD> itemHdList = tItemHDService.lambdaQuery().eq(TItemHD::getPaId, patient.getId()).list();
        IsSubmitDto submitDto = BeanUtil.copyProperties(patient, IsSubmitDto.class);
        if (CollUtil.isEmpty(itemHdList)) {
            return submitDto;
        }
        List<IsSubmitDto.Item> itemList = itemHdList.stream()
                .map(p -> BeanUtil.copyProperties(p, IsSubmitDto.Item.class))
                .peek(p -> {
                    // 如果结果是空时,默认是正常
                    if(ObjectUtil.isNull(p.getResultMark())){
                        p.setResultMark(true);
                    }
                    if (StrUtil.isNotBlank(p.getResult()) && p.getResult().equals(SecurityConstants.IS_SUBMIT_RESULT)) {
                        p.setResult("1");
                    } else {
                        p.setResult("0");
                    }
                })
                .collect(Collectors.toList());
        submitDto.setItemList(itemList);
        return submitDto;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void backSubmitRevocation(Set<String> itemIds, Boolean type, String id,Boolean status) {
        if (CollUtil.isNotEmpty(itemIds)) {
            Collection<TItemHD> list = tItemHDService.listByIds(itemIds);
            if (CollUtil.isEmpty(list)) {
                throw new BadRequestException("体检项目不存在");
            }
            TPatient patient = this.getById(id);
            if (null == patient) {
                throw new BadRequestException("体检号不存在");
            }
            if(!type){
                tItemHDService.lambdaUpdate().in(TItemHD::getId, itemIds).set(TItemHD::getResultMark,status ? "1" : "0").set(TItemHD::getResult, status ? SecurityConstants.IS_SUBMIT_RESULT : null).set(TItemHD::getDoctor, status ? SecurityUtils.getNickName() : null).set(TItemHD::getResultDate, status ? DateTime.now().toTimestamp() : null).update();
             }
        }
        if(type){
            this.lambdaUpdate().eq(TPatient::getId, id).set(TPatient::getIsSubmit, status).set(TPatient::getIsSubmitBy, SecurityUtils.getNickName()).set(TPatient::getIsSubmitTime, new DateTime().toTimestamp()).update();
        }

    }
}
