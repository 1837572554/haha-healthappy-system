package com.healthappy.modules.system.rest.common;

import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.healthappy.annotation.AnonymousAccess;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.service.TidSeedService;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description 种子生成
 * @author FGQ
 * @date 2021-10-29
 */
@Slf4j
@Api(tags = "默认基础数据：种子生成")
@ApiSort(2)
@RestController
@AllArgsConstructor
@RequestMapping("/tidSeed")
public class TidSeedController {

    private final TidSeedService tidSeedService;

    @Log("返回种子ID")
    @ApiOperation("返回种子ID")
    @PostMapping
    @AnonymousAccess
    @Klock
    public R returnSeedId() {
        return R.ok(tidSeedService.createASeedPrimaryKey("B_gruop"));
    }
}
