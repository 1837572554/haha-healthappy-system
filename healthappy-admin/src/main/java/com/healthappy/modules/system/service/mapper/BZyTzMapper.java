package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BZyTz;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Jevany
 * @date 2021/12/8 15:15
 * @desc 中医体制特性 Mapper
 */
@Mapper
@Repository
public interface BZyTzMapper extends CoreMapper<BZyTz> {


}
