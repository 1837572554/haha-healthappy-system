package com.healthappy.modules.system.service.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author FGQ
 * @date 2020-05-14
 */
@Data
public class BSicknessDto implements Serializable {

    /**
     * id
     */
    private String id;

    /**
     * 病种名称
     */
    private String name;

    /**
     * 简拼
     */
    private String jp;

    /**
     * 显示顺序
     */
    private Integer dispOrder;

    /**
     * 是否是病种组合子项
     */
    private String isCombine;

    /**
     * 病种组合名称
     */
    private String combineName;

    /**
     * 是否属于重大异常
     */
    private String anomaly;

    /**
     * 病种组合名称
     */
    private String zx;

    /**
     * 统计名称
     */
    private String nameTj;

    /**
     * 病种等级
     */
    private Integer grade;

    private String gradeText;

    /* ------------------------------------------------------------- */

    /**
     * 病种建议
     */
    private String sicknessAdviceId;
    private String sicknessAdviceName;

    /** 病种原因 */
    private String sicknessCauseId;
    private String sicknessCauseName;

    /** 医学解释 */
    private String sicknessScienceId;
    private String sicknessScienceName;
}
