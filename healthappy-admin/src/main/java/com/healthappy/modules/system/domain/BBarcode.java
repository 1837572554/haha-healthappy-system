package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.healthappy.config.SeedIdGenerator;
import com.healthappy.config.databind.FieldBind;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 条码设置表
 * @author sjc
 * @date 2021-12-13
 */
@Data
@TableName("B_Barcode")
@ApiModel("条码设置表")
@SeedIdGenerator
public class BBarcode implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id",position = 1)
    private String id;

    /**
     * 项目类型,来源字典表---项目组合主表类型
     */
    @ApiModelProperty(value = "项目类型",position = 2)
    private String groupHdType;

    /**
     * 条码总长度
     */
    @ApiModelProperty(value = "条码总长度",position = 3)
    private Integer barcodeLength;

    /**
     * 条码头内容
     */
    @ApiModelProperty(value = "条码头内容",position = 4)
    private String barcodeHead;

    /**
     * 种子，对应的条码在种子基础上+1
     */
    @ApiModelProperty(value = "条码种子",position = 6)
    private String seed;

    /**
     * 医疗机构id(u_hospital_info的id)
     */
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;
}
