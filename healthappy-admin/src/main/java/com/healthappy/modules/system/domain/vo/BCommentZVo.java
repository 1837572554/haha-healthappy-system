package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class BCommentZVo {

    /**
     * id
     */
    private String id;

    /**
     * 检查结果
     */
    @NotBlank(message = "检查结果不能为空")
    @ApiModelProperty(value = "检查结果",required = true)
    private String comment;

    /**
     * 建议
     */
    @NotBlank(message = "建议不能为空")
    @ApiModelProperty(value = "建议",required = true)
    private String suggest;

    /**
     * 排序号
     */
    @ApiModelProperty("排序号")
    private Integer dispOrder;

    /**
     * code
     */
    @ApiModelProperty(value = "编号",required = true)
    private Long code;
}
