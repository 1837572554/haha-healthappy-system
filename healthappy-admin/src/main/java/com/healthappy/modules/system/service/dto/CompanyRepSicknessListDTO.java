package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 单位健康报告编制 - 分析病种列表 DTO
 * @author Jevany
 * @date 2022/2/22 18:55
 */
@Data
public class CompanyRepSicknessListDTO implements Serializable {
    /** 病种Id */
    @ApiModelProperty("病种Id")
    private String sicknessId;

    /** 病种名称 */
    @ApiModelProperty("病种名称")
    private String sicknessName;

    /** 人数 */
    @ApiModelProperty("人数")
    private Long totalCount;

    /** 男性人数 */
    @ApiModelProperty("男性人数")
    private Long maleCount;

    /** 女性人数 */
    @ApiModelProperty("女性人数")
    private Long femaleCount;

}