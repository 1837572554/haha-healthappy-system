package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BArea;
import com.healthappy.modules.system.domain.BFeeType;
import com.healthappy.modules.system.service.dto.BFeeTypeQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 费别表（阿斯利康）服务层
 * @author fang
 * @date 2021-06-17
 */
public interface BFeeTypeService extends BaseService<BFeeType> {


    Map<String, Object> queryAll(BFeeTypeQueryCriteria criteria, Pageable pageable);


    /**
     * 查询数据分页
     * @param criteria 条件
     * @return Map<String, Object>
     */
    List<BFeeType> queryAll(BFeeTypeQueryCriteria criteria);
}
