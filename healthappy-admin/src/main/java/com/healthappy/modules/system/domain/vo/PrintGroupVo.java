package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 批量组合项打印参数
 * @author Jevany
 * @date 2022/4/26 9:55
 */
@ApiModel("批量组合项打印参数")
@Data
public class PrintGroupVo {
	/** 流水号列表 */
	@ApiModelProperty("流水号列表")
	private List<String> paIdList;
	/** 组合项目列表 */
	@ApiModelProperty("组合项目列表")
	private List<String> groupIdList;

	/** 用于接收的Socket的用户编号 */
	@ApiModelProperty("用于接收的Socket的用户编号")
	private String acceptSocketUserId;
}
