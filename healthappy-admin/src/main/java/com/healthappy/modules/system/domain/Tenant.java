package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.healthappy.common.entity.TenantEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 租户
 * @author FGQ
 */
@Data
@TableName("tenant")
public class Tenant extends TenantEntity {

	/** 系统用户ID */
	@ApiModelProperty("系统用户ID")
	@TableId
	private Long id;

	private String name;

	/**租户code*/
	@ApiModelProperty("租户code")
	private String code;

	/** 地址 */
	@ApiModelProperty("地址")
	private String address;

	/** 医疗机构类型编码 */
	@ApiModelProperty("医疗机构类型编码")
	private String instiType;

	/** 医疗机构地图坐标 */
	@ApiModelProperty("医疗机构地图坐标")
	private String coordinateX;

	/** 医疗机构地图坐标 */
	@ApiModelProperty("医疗机构地图坐标")
	private String coordinateY;

	/** 联系电话 */
	@ApiModelProperty("联系电话")
	private String telPhone;

	/** 邮编 */
	@ApiModelProperty("邮编")
	private String postcode;

	/** 邮箱 */
	@ApiModelProperty("邮箱")
	private String email;

	/** QQ */
	@ApiModelProperty("QQ")
	private String qq;

	/** 拼音简码 */
	@ApiModelProperty("拼音简码")
	private String pyCode;

	/** 是否启用 */
	@ApiModelProperty("是否启用")
	private Boolean isEnable;

	/** 排序号 */
	@ApiModelProperty("排序号")
	private Integer dispOrder;

	/** 区域ID */
	@ApiModelProperty("区域ID")
	private Integer areaId;

	/** 医院名称简称 */
	@ApiModelProperty("医院名称简称")
	private String companyJx;

	/** 证照类型 */
	@ApiModelProperty("证照类型")
	private String permitType;

	@ApiModelProperty("医院图片")
	private String img;


	/**
	 * 是否已经删除 0不是  1是
	 */
	@ApiModelProperty("是否已经删除")
	@TableField(value = "del_flag", fill = FieldFill.INSERT)
	@TableLogic
	private String delFlag;

	/** 预约号源管理-开关 0关，1开*/
	@ApiModelProperty("预约号源管理-开关")
	private String appointSwitch;

	/**
	 * 医疗机构id(u_hospital_info的id)
	 */
	@ApiModelProperty("医疗机构id(u_hospital_info的id)")
	@TableField(value = "tenant_id", fill = FieldFill.INSERT)
	private String tenantId;
}
