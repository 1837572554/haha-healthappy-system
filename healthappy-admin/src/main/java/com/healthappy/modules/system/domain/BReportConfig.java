package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


@Data
@TableName("B_Report_Config")
public class BReportConfig implements Serializable {
    /**
     * 自动编号
     */
    @ApiModelProperty(value = "自动编号")
    private Long id;

    /**
     * 报告名称
     */
    @ApiModelProperty(value = "报告名称")
    private String reportName;

    /**
     * 模板路径，如：zphy/person/person_j.rtf
     */
    @ApiModelProperty(value = "模板路径，如：zphy/person/person_j.rtf")
    private String templatePath;

    /**
     * 标签，如：[barcode],[group_tables],[conclusion_doc],[verify_doc],[seal],[depts],[card],[wz],[guide],[doctor]
     */
    @ApiModelProperty(value = "标签，如：[barcode],[group_tables],[conclusion_doc],[verify_doc],[seal],[depts],[card],[wz],[guide],[doctor]")
    private String marks;

    /**
     * 是否只显示职业体检项目，默认0
     */
    @ApiModelProperty(value = "是否只显示职业体检项目，默认0")
    private Boolean isShowZy;

    /**
     * 是否只显示健康体检项目，默认0
     */
    @ApiModelProperty(value = "是否只显示健康体检项目，默认0")
    private Boolean isShowJk;

    /**
     * 生成报告文件类型，1pdf , 2word
     */
    @ApiModelProperty(value = "生成报告文件类型，1pdf , 2word")
    private Integer fileType;

    /**
     * 体检分类分组ID
     */
    @ApiModelProperty(value = "体检分类分组ID")
    private Integer peTypeGroupId;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remarks;

    /**
     * 报告类型，1个人报告 2单位报告
     */
    @ApiModelProperty(value = "报告类型，1个人报告 2单位报告")
    private Integer reportType;

    private static final long serialVersionUID = 1L;
}
