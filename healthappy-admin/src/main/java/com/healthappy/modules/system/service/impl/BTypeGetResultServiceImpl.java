package com.healthappy.modules.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BTypeGetResult;
import com.healthappy.modules.system.service.BTypeGetResultService;
import com.healthappy.modules.system.service.dto.BTypeGetResultDto;
import com.healthappy.modules.system.service.dto.BTypeGetResultQueryCriteria;
import com.healthappy.modules.system.service.mapper.BTypeGetResultMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sjc
 * @date 2021-08-3
 */
@Service
@AllArgsConstructor//有自动注入的功能
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class  BTypeGetResultServiceImpl extends BaseServiceImpl<BTypeGetResultMapper, BTypeGetResult> implements BTypeGetResultService {

    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BTypeGetResultQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BTypeGetResult> page = new PageInfo<BTypeGetResult>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("content", generator.convert(page.getList(), BTypeGetResultDto.class));
        map.put("totalElements", page.getTotal());

        return map;
    }

    @Override
    public List<BTypeGetResult> queryAll(BTypeGetResultQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BTypeGetResultDto.class, criteria));
    }

    @Override
    public boolean deleteById(Long id)
    {
        return baseMapper.deleteById(id)>0;
    }

}
