package com.healthappy.modules.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BCommentWz;
import com.healthappy.modules.system.service.BCommentWzService;
import com.healthappy.modules.system.service.dto.BCommentWzDto;
import com.healthappy.modules.system.service.dto.BCommentWzQueryCriteria;
import com.healthappy.modules.system.service.mapper.BCommentWzMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BCommentWzServiceImpl extends BaseServiceImpl<BCommentWzMapper, BCommentWz> implements BCommentWzService {

    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BCommentWzQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BCommentWz> page = new PageInfo<BCommentWz>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", generator.convert(page.getList(), BCommentWzDto.class));
        map.put("totalElements", page.getTotal());
        return map;
    }

    @Override
    public List<BCommentWz> queryAll(BCommentWzQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BCommentWzDto.class, criteria));
    }
}
