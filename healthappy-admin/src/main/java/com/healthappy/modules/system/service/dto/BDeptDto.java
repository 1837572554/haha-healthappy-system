package com.healthappy.modules.system.service.dto;

import com.healthappy.modules.system.domain.BDept;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author FGQ
 * @date 2020-05-14
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BDeptDto extends BDept {

}
