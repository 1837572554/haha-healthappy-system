package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 体检登记项目
 */
@Data
public class RegisterProDto implements Serializable {

    @ApiModelProperty("体检号")
    private String paId;

    @ApiModelProperty("项目ID")
    private String groupId;

    @ApiModelProperty("项目名称")
    private String groupName;

    @ApiModelProperty("科室编号")
    private String deptId;

    @ApiModelProperty("是否是职业项目")
    private String typeZ;

    @ApiModelProperty("是否是健康项目")
    private String typeJ;

    @ApiModelProperty("原价")
    private BigDecimal price;

    @ApiModelProperty("折扣")
    private BigDecimal discount;

    @ApiModelProperty("实际价格")
    private BigDecimal cost;

    @ApiModelProperty("是否附加项目")
    private String addition;

    /**
     * 是否选检项目
     */
    @ApiModelProperty("是否选检项目")
    private String choose;

    /**
     * 付费方式 0自费 1统收
     */
    @ApiModelProperty("付费方式 0自费 1统收")
    private String payType;

    /**
     * 减免标志
     */
    @ApiModelProperty("减免标志")
    private String free;

    /**
     * 项目登记医生
     */
    @ApiModelProperty("项目登记医生")
    private String registerDoc;


}
