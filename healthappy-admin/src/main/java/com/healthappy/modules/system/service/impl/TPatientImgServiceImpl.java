package com.healthappy.modules.system.service.impl;

import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.TPatientImg;
import com.healthappy.modules.system.domain.vo.TImageVo;
import com.healthappy.modules.system.domain.vo.TPatientImageVo;
import com.healthappy.modules.system.service.TPatientImgService;
import com.healthappy.modules.system.service.dto.TPatientImageDto;
import com.healthappy.modules.system.service.dto.TPatientImgDto;
import com.healthappy.modules.system.service.mapper.TImageMapper;
import com.healthappy.modules.system.service.mapper.TPatientImgMapper;
import com.healthappy.utils.StringUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @desc: 体检项目图片绑定服务 实现类
 * @author: YJ
 * @date: 2021-12-01 16:53
 **/
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class TPatientImgServiceImpl extends BaseServiceImpl<TPatientImgMapper,TPatientImg> implements TPatientImgService {

    private final TImageMapper tImageMapper;

    @Override
    public void bindItemImg(List<TPatientImg> tPatientImgs) {
        Function<TPatientImg,List<Object>> compositeKey=tpImg->
                Arrays.asList(tpImg.getPaId(),tpImg.getGroupId(), tpImg.getItemId());
        Map<Object,List<TPatientImg>> map=tPatientImgs.stream().collect(Collectors.groupingBy(compositeKey));

        //            for (Object o : (Arrays.asList(key))) {
        //
        //            }
        //删除数据
        //            baseMapper.delete(new UpdateWrapper<>().eq(key.get(0)))
        for (Map.Entry<Object, List<TPatientImg>> entry : map.entrySet()) {
            List<String> ids=(List<String>)entry.getKey();
            this.lambdaUpdate().eq(StringUtils.isNotBlank(ids.get(0)),TPatientImg::getPaId,ids.get(0))
                    .eq(StringUtils.isNotBlank(ids.get(1)),TPatientImg::getGroupId,ids.get(1))
                    .eq(StringUtils.isNotBlank(ids.get(2)),TPatientImg::getGroupId,ids.get(2)).remove();
            List<TPatientImg> imgs = entry.getValue();
            this.saveBatch(imgs);
        }

    }

    @Override
    public List<TPatientImageVo> getItemImg(TPatientImgDto tPatientImgDto) {
//        StringBuilder stringBuilder=new StringBuilder();
//        stringBuilder.append("select 1 from T_PATIENT_IMG tpi where tpi.file_id = T_IMAGE.id and tpi.pa_id = '"+ tPatientImgDto.getPaId() +"'");
//        if(StringUtil.isNotEmpty(tPatientImgDto.getGroupId())){
//            stringBuilder.append(" AND tpi.group_id= '"+ tPatientImgDto.getGroupId() +"' ");
//        }
//        if(StringUtil.isNotEmpty(tPatientImgDto.getItemId())){
//            stringBuilder.append(" and tpi.item_id = '"+ tPatientImgDto.getItemId() +"' ");
//        }
//        stringBuilder.append(" LIMIT 1 ");
//        List<TImage> tImages = tImageMapper.selectList(new QueryWrapper<TImage>().exists(stringBuilder.toString()));
//        return tImages.stream().map( imgVo -> BeanUtil.copyProperties(imgVo, TImageVo.class)).collect(Collectors.toList());

        List<TPatientImageDto> list = tImageMapper.getPatientImage(tPatientImgDto);

        Function<TPatientImageDto,List<Object>> compositeKey=tpImg->
                Arrays.asList(tpImg.getPaId(),tpImg.getGroupId(), tpImg.getItemId());
        Map<Object,List<TPatientImageDto>> map=list.stream().collect(Collectors.groupingBy(compositeKey));
        List<TPatientImageVo> voList=new ArrayList<>();
        for (Map.Entry<Object, List<TPatientImageDto>> entry : map.entrySet()) {
            List<String> ids=(List<String>)entry.getKey();
            TPatientImageVo vo=new TPatientImageVo();
            vo.setPaId(ids.get(0));
            vo.setGroupId(ids.get(1));
            vo.setItemId(ids.get(2));

            List<TImageVo> collect = entry.getValue().stream()
                    .map(d -> new TImageVo(d.getId(), d.getFileName(), d.getUrl())).collect(Collectors.toList());

            vo.setImages(collect);
            voList.add(vo);
        }
        return voList;
    }
}
