package com.healthappy.modules.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @description 职业史放射表
 * @author sjc
 * @date 2021-12-15
 */
@Data
public class TRecordRayVo  {

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private Long id;


    /**
     * 体检号
     */
    @ApiModelProperty("体检号")
    private String paId;

    /**
     * 起止日期
     */
    @ApiModelProperty("起止日期")
    private String startEnd;

    /**
     * 单位
     */
    @ApiModelProperty("单位")
    private String workUnit;

    /**
     * 部门
     */
    @ApiModelProperty("部门")
    private String department;

    /**
     * 工种
     */
    @ApiModelProperty("工种")
    private String job;

    /**
     * 放射线种类
     */
    @ApiModelProperty("放射线种类")
    private String radiationType;

    /**
     * 工作量
     */
    @ApiModelProperty("工作量")
    private String workLoad;

    /**
     * 过量照射史
     */
    @ApiModelProperty("过量照射史")
    private String overRadiation;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;

    /**
     * 累计受照剂量
     */
    @ApiModelProperty("累计受照剂量")
    private String cumulativeDose;

}
