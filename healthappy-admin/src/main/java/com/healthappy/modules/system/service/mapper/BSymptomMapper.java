package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BSymptom;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 症状表
 * @author sjc
 * @date 2021-08-10
 */
@Mapper
@Repository
public interface BSymptomMapper extends CoreMapper<BSymptom> {
}
