package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.healthappy.config.SeedIdGenerator;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @description 集团表
 * @author FGQ
 * @date 2021-10-15
 */
@Data
@SeedIdGenerator
@TableName("B_Commpany_Group")
public class BCommpanyGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.INPUT)
    /**
     * id
     */
    @ApiModelProperty(value = "主键")
    private String id;

    /**
     * 集团名称
     */
    @NotBlank(message = "集团名称不能为空")
    @ApiModelProperty(value = "集团名称",required = true)
    private String name;

    /**
     * 是否启用
     */
    @ApiModelProperty(value = "是否启用")
    private String isEnable;

    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户ID")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;
}
