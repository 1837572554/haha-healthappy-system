package com.healthappy.modules.system.rest.projectcheck;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.TPatient;
import com.healthappy.modules.system.domain.vo.TPaitentZyQuestionVo;
import com.healthappy.modules.system.service.BZyQuestionService;
import com.healthappy.modules.system.service.TPatientService;
import com.healthappy.utils.R;
import com.healthappy.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * @desc: 中医体质问卷 控制器
 * @author: YJ
 * @date: 2021-12-08 14:30
 **/
@Slf4j
@Api(tags = "体检流程：中医体制")
@RestController
@AllArgsConstructor
@RequestMapping("/zyTz")
public class ZyTzController {

    private final BZyQuestionService bZyQuestionService;
    private final TPatientService tPatientService;

    @Log("获得体检号对应的问卷数据")
    @ApiOperation("获得体检号对应的问卷数据，权限码：ProjectCheck:list")
    @GetMapping("/getQuestions")
    @PreAuthorize("@el.check('ProjectCheck:list')")
    public R getQuestions(@RequestParam(required = true) String paId,@RequestParam(required = true) String groupId) {
        if (StringUtils.isEmpty(paId)) {
            throw new BadRequestException("体检号不能为空");
        }
        if (StringUtils.isEmpty(groupId)) {
            throw new BadRequestException("组合编号不能为空");
        }
        TPatient tPatient = tPatientService.getOne(new LambdaQueryWrapper<TPatient>().eq(TPatient::getId, paId));
        if (!Optional.ofNullable(tPatient).isPresent()) {
            throw new BadRequestException("体检者不存在");
        }
        if (ObjectUtil.isEmpty(tPatient.getAge())) {
            throw new BadRequestException("体检者年龄未设置");
        }
        if (ObjectUtil.isEmpty(tPatient.getSex())) {
            throw new BadRequestException("体检者性别未设置");
        }
        return R.ok(bZyQuestionService.getQuestions(tPatient, groupId));
    }

    @Log("保存体检号对应的问卷数据")
    @ApiOperation("保存体检号对应的问卷数据，权限码：ProjectCheck:update")
    @PostMapping("/saveQuestions")
    @PreAuthorize("@el.check('ProjectCheck:update')")
    @Klock
    public R saveQuestions(@RequestBody @Validated TPaitentZyQuestionVo vo) {
        if (!Optional.ofNullable(vo).isPresent()) {
            throw new BadRequestException("请提交正确的数据");
        }
        if (StringUtils.isBlank(vo.getPaId())) {
            throw new BadRequestException("体检号不能为空");
        }
        if (ObjectUtil.isEmpty(vo.getBankId())) {
            throw new BadRequestException("库编号不能为空");
        }
        if (StringUtils.isBlank(vo.getOperationDoctor())) {
            throw new BadRequestException("操作医生不能为空");
        }
        if (CollUtil.isEmpty(vo.getQuestionDtoList())) {
            throw new BadRequestException("问题答案不能为空");
        }
        if (StringUtils.isBlank(vo.getGroupId())) {
            throw new BadRequestException("组合项编号不能为空");
        }
        return R.ok(bZyQuestionService.saveQuestions(vo));
    }

}
