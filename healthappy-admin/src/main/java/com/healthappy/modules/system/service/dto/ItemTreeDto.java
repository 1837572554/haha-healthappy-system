package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ItemTreeDto implements Serializable {

    @ApiModelProperty("项目ID")
    private String id;

    @ApiModelProperty("项目名称")
    private String name;

    @ApiModelProperty("病种集合")
    private List<SicknessTreeDto> sicknessTreeList;
}
