package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.RolesMenus;

/**
 * @author hupeng
 * @date 2020-05-16
 */
public interface RolesMenusService extends BaseService<RolesMenus> {

}
