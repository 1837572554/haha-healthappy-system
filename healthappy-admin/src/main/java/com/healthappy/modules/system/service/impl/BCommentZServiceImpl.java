package com.healthappy.modules.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BCommentZ;
import com.healthappy.modules.system.service.BCommentZService;
import com.healthappy.modules.system.service.dto.BCommentZDto;
import com.healthappy.modules.system.service.dto.BCommentZQueryCriteria;
import com.healthappy.modules.system.service.mapper.BCommentZMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BCommentZServiceImpl extends BaseServiceImpl<BCommentZMapper, BCommentZ> implements BCommentZService {

    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BCommentZQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BCommentZ> page = new PageInfo<BCommentZ>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", generator.convert(page.getList(), BCommentZDto.class));
        map.put("totalElements", page.getTotal());
        return map;
    }

    @Override
    public List<BCommentZ> queryAll(BCommentZQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BCommentZDto.class, criteria));
    }
}
