package com.healthappy.modules.system.service;


import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BBarcode;
import com.healthappy.modules.system.domain.vo.BBarcodeVo;
import com.healthappy.modules.system.service.dto.BBarcodeQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 条码设置表
 * @author sjc
 * @date 2021-12-13
 */
public interface BBarcodeService extends BaseService<BBarcode> {

    Map<String, Object> queryAll(BBarcodeQueryCriteria criteria, Pageable pageable);

    List<BBarcode> queryAll(BBarcodeQueryCriteria criteria);

    boolean saveOrUpdate(BBarcodeVo resources);

    /**
     * 根据组合项目类型，获取条码号
     * @param groupHdType
     * @return
     */
    String generateBarcode(String groupHdType);
}
