package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BPackageHD;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 套餐表
 * @author sjc
 * @date 2021-07-2
 */
@Mapper
@Repository
public interface BPackageHDMapper extends CoreMapper<BPackageHD> {
}
