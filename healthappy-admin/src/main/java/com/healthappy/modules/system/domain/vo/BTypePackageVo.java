package com.healthappy.modules.system.domain.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 套餐类型
 * @author sjc
 * @date 2021-08-24
 */
@Data
public class BTypePackageVo  {


    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private Long id;


    /**
     * 排序
     */
    @ApiModelProperty("排序")
    private Integer dispOrder;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    private String name;

}
