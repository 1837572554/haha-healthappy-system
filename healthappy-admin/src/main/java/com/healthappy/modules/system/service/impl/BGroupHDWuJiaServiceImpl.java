package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BGroupHDWuJia;
import com.healthappy.modules.system.domain.vo.BGroupHDWuJiaVo;
import com.healthappy.modules.system.service.BGroupHDWuJiaService;
import com.healthappy.modules.system.service.dto.BGroupHDWuJiaDto;
import com.healthappy.modules.system.service.dto.BGroupHDWuJiaQueryCriteria;
import com.healthappy.modules.system.service.mapper.BGroupHDWuJiaMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author SJC
 * @date 2021-08-3
 */
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BGroupHDWuJiaServiceImpl extends BaseServiceImpl<BGroupHDWuJiaMapper, BGroupHDWuJia> implements BGroupHDWuJiaService {

    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BGroupHDWuJiaQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BGroupHDWuJia> page = new PageInfo<BGroupHDWuJia>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content",generator.convert(page.getList(), BGroupHDWuJiaDto.class));
        map.put("totalElements", page.getTotal());
        return map;
    }

    @Override
    public List<BGroupHDWuJia> queryAll(BGroupHDWuJiaQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BGroupHDWuJiaDto.class, criteria));
    }

    @Override
    public void saveOrUpdate(List<BGroupHDWuJiaVo> resources) {
        List<BGroupHDWuJia> bGroupHDWuJiaList = this.lambdaQuery().eq(BGroupHDWuJia::getItemGroupId,resources.get(0).getItemGroupId()).list();
        if(ObjectUtil.isNull(resources.get(0).getItemPriceId())){
            this.lambdaUpdate().eq(BGroupHDWuJia::getItemGroupId,resources.get(0).getItemGroupId()).remove();
        } else if(CollUtil.isEmpty(bGroupHDWuJiaList)){
            this.saveBatch(generator.convert(resources, BGroupHDWuJia.class));
        }else{
            List<BGroupHDWuJiaVo> bGroupHDWuJiaVoList = resources.stream().filter(r -> bGroupHDWuJiaList.stream().noneMatch
                    (g->g.getItemGroupId().equals(r.getItemGroupId()) && ObjectUtil.isNotNull(g.getItemPriceId())  && g.getItemPriceId().equals(r.getItemPriceId()))
            ).collect(Collectors.toList());

            if(CollUtil.isNotEmpty(bGroupHDWuJiaVoList)){
                this.saveBatch(generator.convert(bGroupHDWuJiaVoList,BGroupHDWuJia.class));
            }
            //删除未被选中的数据
            //前端可能会删除部分项目，将其获取出来
            List<BGroupHDWuJia> bGroupDTList = bGroupHDWuJiaList.stream().filter(g ->  resources.stream().noneMatch(r ->
                    r.getItemGroupId().equals(g.getItemGroupId()) && ObjectUtil.isNotNull(r.getItemPriceId()) && r.getItemPriceId().
                            equals(g.getItemPriceId()))).collect(Collectors.toList());

            if(CollUtil.isNotEmpty(bGroupDTList)){
                this.removeByIds(bGroupDTList.stream().map(BGroupHDWuJia::getId).collect(Collectors.toList()));
            }

            //修改前端传回ID的数据
            List<BGroupHDWuJia> wuJiaList = resources.stream().filter(r -> ObjectUtil.isNotNull(r.getId())).map(m -> BeanUtil.copyProperties(m, BGroupHDWuJia.class)).collect(Collectors.toList());
            if(CollUtil.isNotEmpty(wuJiaList)){
                this.updateBatchById(wuJiaList);
            }
        }
    }



    /**
     * 根据组合项目id删除物价数据
     * @param groupId
     * @return
     */
    @Override
    public boolean deleteByGroupId(String groupId) {
        return   this.lambdaUpdate().eq(BGroupHDWuJia::getItemGroupId,groupId).remove();
    }

    /**
     * 根据主键id删除物价数据
     * @param id
     * @return
     */
    @Override
    public boolean deleteById(Long id) {
        return  baseMapper.deleteById(id)>0;
    }
}
