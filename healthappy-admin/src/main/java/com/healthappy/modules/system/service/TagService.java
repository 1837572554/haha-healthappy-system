package com.healthappy.modules.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.Sign;
import com.healthappy.modules.system.domain.Tag;
import com.healthappy.modules.system.domain.User;
import com.healthappy.modules.system.domain.vo.TagSignVo;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Set;


public interface TagService extends BaseService<Tag> {


    Map<String, Object> queryAll(Pageable pageable);

    List<Sign> signList(String type);

    void updPer(TagSignVo tagSignVo);

    void del(Set<Long> ids);

    /**
     * 用户绑定标识
     * @param userId
     * @param tagIds
     */
    void bindUserAndTag(Long userId,Set<Long> tagIds);

    /**
     * 获取当前用户的标识
     * @return
     */
    List<String> getCurrentPermissionList();

    /**
     * 是否包含指定标识
     * @param per 标识
     * @return
     */
    boolean isIncludePer(String per);


    /**
     * 根据标识，获取授权的用户组
     * @param per
     * @return
     */
    List<Long> getPerByUserGroup(String... per);

    /**
     * 根据标识，获取对应的用户集合
     * @author YJ
     * @date 2022/2/28 17:34
     * @param per 标识值 如 critical_notification
     * @return java.util.List〈com.healthappy.modules.system.domain.User〉
     */
    List<User> getUsersByTagPer(String per);

    /**
     * 根据标识，获取对应的用户集合
     * @author YJ
     * @date 2022/2/28 17:34
     * @param per 标识值 如 critical_notification
     * @param tenantId
     * @return java.util.List〈com.healthappy.modules.system.domain.User〉
     */
    List<User> getUsersByTagPer(String per, String tenantId);


    Set<Long> getUserIdByTag(Long id);

    IPage<Tag> customSql(Pageable pageable, String name);
}
