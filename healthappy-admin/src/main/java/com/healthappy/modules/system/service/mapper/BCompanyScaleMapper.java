package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BCompanyScale;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 企业规模表
 * @author sjc
 * @date 2021-09-2
 */
@Mapper
@Repository
public interface BCompanyScaleMapper extends CoreMapper<BCompanyScale> {

}
