package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.TImage;
import com.healthappy.modules.system.domain.vo.Base64ImgVo;
import com.healthappy.modules.system.domain.vo.TImageVo;
import com.healthappy.modules.system.service.TImageService;
import com.healthappy.modules.system.service.mapper.TImageMapper;
import com.healthappy.utils.FileUtil;
import com.healthappy.utils.PictureUtil;
import com.healthappy.utils.StringUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @desc: 图片服务实现
 * @author: YJ
 * @date: 2021-11-30 17:04
 **/
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class TImageServiceImpl extends BaseServiceImpl<TImageMapper, TImage> implements TImageService {

//    /** 图片配置 */
//    @Autowired
//    private ImageConfig imageConfig;

    /**
     * 图片保存路径
     */
    private static final String savePath = "/uploads/images";


    /**
     * 检测文件大小 10M
     */
    private static final Long maxSize = 10L;

    /**
     * 获取当前程序目录 末尾没有/
     *
     * @return java.lang.String
     * @author YJ
     * @date 2021/12/6 19:01
     */
    public String getBasePath() {
        String basePath = System.getProperty("user.dir");
        return StringUtils.trimFirstAndLastChar(basePath, "/");
    }


    /**
     * 获取图片保存目录相对路径 头部有/
     *
     * @return java.lang.String 返回拼接后的保存目录相对路径字符串
     * @author YJ
     * @date 2021/12/6 18:53
     */
    public String getRelativePath() {
        String date = DateUtil.format(DateTime.now(), "yyyyMMdd");
        return "/" + StringUtils.trimFirstAndLastChar(StringUtils.trimFirstAndLastChar(savePath, "/"), "\\") + "/" + date + "/";
    }

    /**
     * 保存文件
     *
     * @param file             文件对象
     * @param path             绝对路径，不包含文件名
     * @param relativePath     相对路径
     * @param originalFilename 原文件名
     * @param fileSize         文件大小
     * @param userName         用户名
     * @return com.healthappy.modules.system.domain.TImage 对象
     * @author YJ
     * @date 2021/12/6 19:34
     */
    public TImage saveFile(File file, String path, String relativePath, String originalFilename, Long fileSize, String userName) {
        TImage tImage = new TImage();
        String md5 = FileUtil.getMd5(file);
        Timestamp time = new Timestamp(System.currentTimeMillis());
        // 验证是否重复上传
        List<TImage> tImages = this.list(new LambdaQueryWrapper<TImage>().eq(TImage::getMd5Code, md5));
        if (CollUtil.isNotEmpty(tImages)) {
            FileUtil.del(file);
            List<TImage> tImagesUser = tImages.stream().filter(i -> userName.equals(i.getUserName())).collect(Collectors.toList());

            if (CollUtil.isNotEmpty(tImagesUser)) {
                List<TImage> tImagesUserFile = tImagesUser.stream().filter(i -> originalFilename.equals(i.getFileName())).collect(Collectors.toList());
                if (CollUtil.isNotEmpty(tImagesUserFile)) {
                    tImage = tImagesUser.get(0);
                    return tImage;
                } else {
                    tImage = tImagesUser.get(0);
                    tImage.setFileName(originalFilename);
                    tImage.setId(null);
                    tImage.setCreateTime(time);
                    this.save(tImage);
                    return tImage;
                }
            } else {
                tImage = tImages.get(0);
                tImage.setFileName(originalFilename);
                tImage.setUserName(userName);
                tImage.setId(null);
                tImage.setCreateTime(time);
                this.save(tImage);
                return tImage;
            }
        }
        tImage = new TImage();
        tImage.setFileName(originalFilename);
        tImage.setFilePath(path + file.getName());
        tImage.setUrl(relativePath + file.getName());
        tImage.setSize(FileUtil.getSize(fileSize));
        tImage.setUserName(userName);
        tImage.setMd5Code(md5);
        tImage.setCreateTime(time);
        this.save(tImage);
        return tImage;
    }

    @Override
    public TImageVo upload(MultipartFile multipartFile, String userName) {
        TImage tImage = new TImage();
        Long fileSize = multipartFile.getSize();
        FileUtil.checkSize(maxSize, fileSize);

        String originalFilename = multipartFile.getOriginalFilename();
        String relativePath = getRelativePath();
        String path = getBasePath() + relativePath;
        try {
            File file = FileUtil.upload(multipartFile, path);
            if (ObjectUtil.isNull(file)) {
                throw new BadRequestException("上传失败");
            }
            tImage = saveFile(file, path, relativePath, originalFilename, fileSize, userName);
        } catch (Exception ex) {
            tImage = new TImage();
            tImage.setFileName(originalFilename);
            tImage.setUrl(ex.getMessage());
        }
        //删除临时文件
        //FileUtil.del(file);
        return new TImageVo(tImage.getId(), tImage.getFileName(), tImage.getUrl());
    }

    @Override
    public List<TImageVo> uploads(MultipartFile[] multipartFile, String userName) {
        List<TImageVo> tImageVos = new ArrayList<>();
        for (MultipartFile file : multipartFile) {
            tImageVos.add(upload(file, userName));
        }
        return tImageVos;
    }


    @Override
    public TImageVo uploadBase64(Base64ImgVo base64ImgVo, String userName) {
        TImage tImage = new TImage();
        //生成原文件名
        String originalFilename = "base64_" + FileUtil.fileRename().toString()
                + "." + StringUtils.trimFirstAndLastChar(base64ImgVo.getImageSuffix(), ".");

        String relativePath = getRelativePath();
        String path = getBasePath() + relativePath;

        try {
            if (PictureUtil.base64StrToImage(path + originalFilename, base64ImgVo.getBase64String())) {
                File file = new File(path+ originalFilename);
                Long fileSize = file.length();
                FileUtil.checkSize(maxSize, fileSize);

                tImage = saveFile(file, path, relativePath, originalFilename, fileSize, userName);
            } else {
                tImage = new TImage();
                tImage.setId("");
                tImage.setFileName("");
                tImage.setUrl("图片转换失败");
            }
        } catch (Exception ex) {
            tImage = new TImage();
            tImage.setId("");
            tImage.setFileName("");
            tImage.setUrl(ex.getMessage());
        }
        return new TImageVo(tImage.getId(), tImage.getFileName(), tImage.getUrl());
    }

    @Override
    public List<TImageVo> uploadBase64List(List<Base64ImgVo> base64ImgVos, String userName) {
        List<TImageVo> tImageVos = new ArrayList<>();
        for (Base64ImgVo base64ImgVo : base64ImgVos) {
            tImageVos.add(uploadBase64(base64ImgVo, userName));
        }
        return tImageVos;
    }


}
