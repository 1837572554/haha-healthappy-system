package com.healthappy.modules.system.rest.basic;

import cn.hutool.core.bean.BeanUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BSetUp;
import com.healthappy.modules.system.domain.vo.BSetUpVo;
import com.healthappy.modules.system.service.BSetUpService;
import com.healthappy.modules.system.service.dto.BSetUpQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description 综合设置   控制器
 * @author cyt
 * @date 2021-08-17
 */
@Slf4j
@Api(tags = "基础管理：综合设置")
@RestController
@AllArgsConstructor
@RequestMapping("/BSetUp")
public class BSetUpController {
    private final BSetUpService bSetUpService;

    @Log("查询综合设置数据")
    @ApiOperation("查询综合设置数据，权限码：BSetUp:list")
    @GetMapping
    public R list(BSetUpQueryCriteria criteria, Pageable pageable) {
        return R.ok(bSetUpService.queryAll(criteria,pageable));
    }

    @Log("新增|修改综合设置")
    @ApiOperation("新增|修改综合设置，权限码：BSetUp:add")
    @PostMapping
    @PreAuthorize("@el.check('BSetUp:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BSetUpVo resources) {
        return R.ok(bSetUpService.saveOrUpdate(BeanUtil.copyProperties(resources, BSetUp.class)));
    }

    @Log("删除综合设置")
    @ApiOperation("删除综合设置，权限码：BSetUp:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('BSetUp:delete')")
    @Klock
    public R remove(@RequestBody List<Long> ids) {
        return R.ok(bSetUpService.removeByIds(ids));
    }
}
