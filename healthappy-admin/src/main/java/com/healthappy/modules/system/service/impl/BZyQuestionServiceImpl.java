package com.healthappy.modules.system.service.impl;

import cn.hutool.core.date.DateTime;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.*;
import com.healthappy.modules.system.domain.vo.TPaitentZyQuestionVo;
import com.healthappy.modules.system.service.BZyQuestionService;
import com.healthappy.modules.system.service.TItemHDService;
import com.healthappy.modules.system.service.TZyAnswerBankService;
import com.healthappy.modules.system.service.TZyAnswerService;
import com.healthappy.modules.system.service.dto.BZyQuestionDto;
import com.healthappy.modules.system.service.mapper.BZyQuestionMapper;
import com.healthappy.utils.StringUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author Jevany
 * @date 2021/12/8 15:13
 * @desc 中医体质问卷问题 服务 实现
 */
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BZyQuestionServiceImpl extends BaseServiceImpl<BZyQuestionMapper, BZyQuestion> implements BZyQuestionService {

    private final TZyAnswerBankService tZyAnswerBankService;
    private final TZyAnswerService tZyAnswerService;
    private final IGenerator generator;
    private final TItemHDService tItemHDService;

    @Override
    public TPaitentZyQuestionVo getQuestions(TPatient tPatient, String groupId) {
        TItemHD itemHd = tItemHDService.getOne(new LambdaQueryWrapper<TItemHD>().eq(TItemHD::getPaId, tPatient.getId()).eq(TItemHD::getGroupId, groupId));
        if(!Optional.ofNullable(itemHd).isPresent()) {
            return new TPaitentZyQuestionVo();
//            throw new BadRequestException("无中医体质项目");
        }
        TPaitentZyQuestionVo paQuestion = new TPaitentZyQuestionVo();
        paQuestion.setPaId(tPatient.getId());
        paQuestion.setGroupId(groupId);
        Integer bankId = 2;
        if (tPatient.getAge() >= 65) {
            //执行第二套题库
            bankId = 1;
        }
        paQuestion.setBankId(bankId);
        TZyAnswerBank tZyAnswerBank = tZyAnswerBankService.getOne(new LambdaQueryWrapper<TZyAnswerBank>().eq(TZyAnswerBank::getBankId, bankId)
                .eq(TZyAnswerBank::getPaId, tPatient.getId()).orderByDesc(TZyAnswerBank::getCreateTime).last("limit 1"));
        if (Optional.ofNullable(tZyAnswerBank).isPresent()) {
            paQuestion.setOperationDoctor(tZyAnswerBank.getOperationDoctor());
            paQuestion.setAnswerBankId(tZyAnswerBank.getId());
        }
        List<BZyQuestionDto> question = baseMapper.getQuestion(tPatient.getId(), bankId, tPatient.getSex(), Optional.ofNullable(tZyAnswerBank).isPresent() ? tZyAnswerBank.getId() : null);
        paQuestion.setQuestionDtoList(question);
        paQuestion.setResult(itemHd.getResult());
        paQuestion.setDesc(itemHd.getResultDesc());
        return paQuestion;
    }

    @Override
    public TPaitentZyQuestionVo saveQuestions(TPaitentZyQuestionVo vo) {
        //防止意外情况，保存前删除一次
        tZyAnswerBankService.remove(Wrappers.<TZyAnswerBank>lambdaUpdate().eq(TZyAnswerBank::getPaId, vo.getPaId()).eq(TZyAnswerBank::getBankId, vo.getBankId()));
        TZyAnswerBank tZyAnswerBank = new TZyAnswerBank();
        tZyAnswerBank.setPaId(vo.getPaId());
        tZyAnswerBank.setBankId(vo.getBankId());
        tZyAnswerBank.setOperationDoctor(vo.getOperationDoctor());
        tZyAnswerBank.setId(vo.getAnswerBankId());
        //保存答案题库
        tZyAnswerBankService.saveOrUpdate(tZyAnswerBank);
        //如果答案题库编号不为空，则清一下，重新保存
        if (StringUtils.isNotBlank(vo.getAnswerBankId())) {
            LambdaUpdateWrapper<TZyAnswer> answerLambdaUpdateWrapper = new LambdaUpdateWrapper<TZyAnswer>().eq(TZyAnswer::getAnswerBankId, vo.getAnswerBankId());
            tZyAnswerService.remove(answerLambdaUpdateWrapper);
        }

        String answerBankId = tZyAnswerBank.getId();
        List<TZyAnswer> answerList = generator.convert(vo.getQuestionDtoList(), TZyAnswer.class);
        answerList.forEach(answer -> {
            answer.setAnswerBankId(answerBankId);
        });
        tZyAnswerService.saveBatch(answerList);

        Map<String, String> map = new HashMap<>();
        if (vo.getBankId() == 1) {
            map = tZyAnswerService.calcScoreBank1(vo.getPaId(), vo.getBankId());
        } else if (vo.getBankId() == 2) {
            map = tZyAnswerService.calcScoreBank2(vo.getPaId(), vo.getBankId());
        }

        TItemHD itemHd = tItemHDService.getOne(new LambdaQueryWrapper<TItemHD>().eq(TItemHD::getPaId, vo.getPaId()).eq(TItemHD::getGroupId, vo.getGroupId()));
        itemHd.setResult(map.get("result"));
        itemHd.setResultDesc(map.get("desc"));
        itemHd.setResultDate(DateTime.now().toTimestamp());
        itemHd.setDoctor(vo.getOperationDoctor());
        tItemHDService.saveOrUpdate(itemHd);

        vo.setResult(map.get("result"));
        vo.setDesc(map.get("desc"));
        return vo;
    }
}
