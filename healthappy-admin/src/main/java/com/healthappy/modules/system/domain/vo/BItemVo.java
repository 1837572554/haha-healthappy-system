package com.healthappy.modules.system.domain.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @description 项目表
 * @author zhengkai.blog.csdn.net
 * @date 2021-06-22
 */
@Data
public class BItemVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty(value = "id",position = 1)
    private String id;

    /**
     * 科室编号o
     */
    @NotNull(message = "科室编号不能为空")
    @ApiModelProperty(value = "科室编号",position = 2)
    private String deptId;

    /**
     * 项目名称
     */
    @NotBlank(message = "项目名称不能为空")
    @ApiModelProperty(value = "项目名称",position = 3,required = true)
    private String name;

    /**
     * 仪器通道号
     */
    @ApiModelProperty(value = "仪器通道号",position = 4)
    private String instrCode;

    /**
     * 适用性别
     */
    @NotBlank(message = "适用性别不能为空")
    @ApiModelProperty(value = "适用性别",position = 5,required = true)
    private String sex;

    /**
     * 默认结果
     */
    @ApiModelProperty(value = "默认结果",position = 6)
    private String result;

    /**
     * 结果类型0文本，1数值
     */
    @ApiModelProperty(value = "结果类型0文本，1数值",position = 7)
    private String resultType;

    /**
     * 结果单位
     */
    @ApiModelProperty(value = "项目单位",position = 8)
    private String unit;

    /**
     * 参考上限
     */
    @ApiModelProperty(value = "参考上限",position = 9)
    private String refHigh;

    /**
     * 参考下限
     */
    @ApiModelProperty(value = "参考下限",position = 10)
    private String refLow;

    /**
     * 参考上限（女）
     */
    @ApiModelProperty(value = "参考上限（女）",position = 11)
    private String refHighF;

    /**
     * 参考下限（女）
     */
    @ApiModelProperty(value = "参考下限（女）",position = 12)
    private String refLowF;

    /**
     * 偏高提示
     */
    @ApiModelProperty(value = "偏高提示",position = 13)
    private String flagHigh;

    /**
     * 偏低提示
     */
    @ApiModelProperty(value = "偏低提示",position = 14)
    private String flagLow;

    /**
     * 最大值
     */
    @ApiModelProperty(value = "最大值",position = 15)
    private String max;

    /**
     * 最小值
     */
    @ApiModelProperty(value = "最小值",position = 16)
    private String min;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注",position = 17)
    private String remarks;

    /**
     * 是否进入小结
     */
    @ApiModelProperty(value ="是否进入小结",position = 18)
    private String isSummary;

    /**
     * 冲突项目
     */
    @ApiModelProperty(value = "冲突项目",position = 19)
    private String clashName;

    /**
     * 冲突级别
     */
    @ApiModelProperty(value = "冲突级别",position = 20)
    private String clashLevel;

    /**
     * 启用
     */
    @ApiModelProperty(value = "是否启用",position = 21)
    private String isEnable;

    /**
     * disp_order
     */
    @ApiModelProperty(value = "排序",position = 22)
    private Integer dispOrder;

    /**
     * 项目编码
     */
    @ApiModelProperty(value = "项目编码",position = 23)
    private String code;

    /**
     * 简拼
     */
    @ApiModelProperty(value = "简拼",position = 24)
    private String jp;
}
