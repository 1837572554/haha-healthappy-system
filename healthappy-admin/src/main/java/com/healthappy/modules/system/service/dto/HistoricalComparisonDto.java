package com.healthappy.modules.system.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

@Data
public class HistoricalComparisonDto {

    @ApiModelProperty("体检编号")
    private String id;

    @ApiModelProperty("体检日期")
    private Timestamp peDate;

    @ApiModelProperty("姓名")
    private String name;

    private String sex;

    @ApiModelProperty("性别")
    private String sexText;

    @ApiModelProperty("年龄")
    private Integer age;

    @ApiModelProperty("身份证")
    private String idNo;

    @ApiModelProperty("单位编号")
    private String companyId;

    @ApiModelProperty("单位编号")
    private String companyName;

    @ApiModelProperty("主检医生")
    private String conclusionDoc;

    @ApiModelProperty("主检日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp conclusionDate;

    @ApiModelProperty("禁忌")
    private Integer pjTaboo;

    @ApiModelProperty("疑似")
    private Integer pjSuspicion;

    @ApiModelProperty("其他复查")
    private Integer pjReview;

    @ApiModelProperty("其他疾病或异常")
    private Integer pjUnusual;

    @ApiModelProperty("复查")
    private Integer pjRelate;

    @ApiModelProperty("职业结论")
    private String commentZ;

    @ApiModelProperty("职业建议")
    private String suggestZ;

    private List<ItemHd> itemHdList;

    @Data
    public static class ItemHd{

        private String groupId;

        @ApiModelProperty("组合名称")
        private String groupName;

        @ApiModelProperty("小结")
        private String result;

        @ApiModelProperty("描述")
        private String resultDesc;

        @ApiModelProperty("操作医生")
        private String doctor;

        @ApiModelProperty("操作日期")
        private Timestamp resultDate;

        private List<ItemDt> itemDtList;

        @Data
        public static class ItemDt{

            @ApiModelProperty("项目ID")
            private String itemId;

            @ApiModelProperty("项目名称")
            private String itemName;

            @ApiModelProperty("结果")
            private String result;

            @ApiModelProperty("判定")
            private String resultMark;

            @ApiModelProperty("参考上限")
            private String refHigh;

            @ApiModelProperty("参考下限")
            private String refLow;
        }
    }
}
