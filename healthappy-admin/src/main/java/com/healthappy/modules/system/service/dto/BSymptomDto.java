package com.healthappy.modules.system.service.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 症状表
 * @author sjc
 * @date 2021-08-10
 */
@Data
public class BSymptomDto implements Serializable {

    /**
     * 症状id
     */
    @ApiModelProperty("症状id")
    private Long id;

    /**
     * 症状名称
     */
    @ApiModelProperty("症状名称")
    private String name;

    /**
     * 正常结果
     */
    @ApiModelProperty("正常结果")
    private String value;

    /**
     * 是否启用
     */
    @ApiModelProperty("是否启用")
    private String isEnable;

    /**
     * 显示顺序
     */
    @ApiModelProperty("显示顺序")
    private Integer dispOrder;

}
