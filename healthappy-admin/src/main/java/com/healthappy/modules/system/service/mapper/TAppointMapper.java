package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TAppoint;
import com.healthappy.modules.system.service.dto.AppointDateNumDTO;
import com.healthappy.modules.system.service.dto.TAppointPrintDto;
import com.healthappy.modules.system.service.dto.TAppointToExcelDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

/**
 * @description 预约表
 * @author sjc
 * @date 2021-06-28
 */
@Mapper
@Repository
public interface TAppointMapper extends CoreMapper<TAppoint> {

	/**
	 * 根据身份证获取预约数据
	 * @param  idNo 身份证
	 * @return
	 */
	List<TAppoint> queryByIdNo(@Param("idNo") String idNo, @Param("tenantId") String tenantId);

	/**
	 * 签到
	 * @param  id 预约id
	 * @param  date 预约时间
	 * @param  doc 预约医生
	 * @return
	 */
	@SqlParser(filter = true)
	boolean sign(@Param("id") String id, @Param("date") Timestamp date, @Param("doc") String doc,
			@Param("tenantId") String tenantId);


	/**
	 * 供导出excel的列表
	 * @param ids
	 * @return
	 */
	List<TAppointToExcelDto> listForExcel(@Param("ids") List<String> ids, @Param("tenantId") String tenantId);

	/**
	 * 供打印的详情
	 * @param id
	 * @return
	 */
	TAppointPrintDto detailForPrint(@Param("id") Integer id);

	/**
	 * 计算时间段内，有预约数据的，日期预约数量
	 * @author YJ
	 * @date 2022/3/16 12:53
	 * @param timeList
	 * @return java.util.List〈com.healthappy.modules.system.service.dto.AppointDateNumDTO〉
	 */
	List<AppointDateNumDTO> calcAppointDateNum(@Param("timeList") List<String> timeList);

	/**
	 * 预约号是否当天的
	 * @author YJ
	 * @date 2022/5/10 18:30
	 * @param appointId 预约号
	 * @return 当天的则大于0, 反之为0
	 */
	Integer appointToDayById(@Param("appointId") String appointId);
}
