package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.modules.system.service.dto.SicknessConclusionListDto;
import com.healthappy.modules.system.service.dto.SicknessConclusionListQueryCriteria;
import com.healthappy.modules.system.service.dto.SicknessConclusionNameDto;
import com.healthappy.modules.system.service.dto.SicknessConclusionNameSexDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Jevany
 * @date 2022/1/21 14:53
 * @desc
 */
@Mapper
@Repository
public interface SicknessConclusionMapper {

    /**
     * 根据条件获取病种名称
     * @author YJ
     * @date 2022/1/21 16:35
     * @param criteria 查询类
     * @return java.util.List〈java.lang.String〉
     */
    @SqlParser(filter = true)
    List<SicknessConclusionNameDto> getSicknessNameList(@Param("criteria") SicknessConclusionListQueryCriteria criteria);

    /**
     * 根据条件获得病种名，性别所有数据
     * @author YJ
     * @date 2022/1/21 17:15
     * @param criteria 查询类
     * @return java.util.List〈com.healthappy.modules.system.service.dto.SicknessConclusionNameSexDto〉
     */
    @SqlParser(filter = true)
    List<SicknessConclusionNameSexDto> getSicknessNameSex(@Param("criteria") SicknessConclusionListQueryCriteria criteria);

    /**
     * 根据条件获取名单数据
     * @author YJ
     * @date 2022/1/21 17:48
     * @param criteria
     * @return java.util.List〈com.healthappy.modules.system.service.dto.SicknessConclusionListDto〉
     */
    @SqlParser(filter = true)
    List<SicknessConclusionListDto> getSicknessList(@Param("criteria") SicknessConclusionListQueryCriteria criteria);

}
