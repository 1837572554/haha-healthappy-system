package com.healthappy.modules.system.domain.vo;

import com.healthappy.modules.system.domain.TItemDT;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 * 批量结果VO
 * @author Jevany
 * @date 2022/5/14 16:53
 */
@Data
@ApiModel("批量结果VO")
public class BatchResultVO implements Serializable {

	/** 流水号集合 */
	@NotEmpty(message = "流水号集合不能为空")
	@ApiModelProperty("流水号集合")
	List<String> paIdList;

	/** 组合编号 */
	@NotBlank(message = "组合编号不能为空")
	@ApiModelProperty("组合编号")
	String groupId;

	/** 明细项列表 */
	@NotEmpty(message = "明细项列表不能为空")
	@ApiModelProperty("明细项列表")
	List<TItemDT> itemDTList;

	private static final long serialVersionUID = 1L;
}