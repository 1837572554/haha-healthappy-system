package com.healthappy.modules.system.service.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.config.databind.FieldBind;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Data
public class ProjectCheckDto  implements Serializable {

    @ApiModelProperty("流水号")
    private String id;

    /**
     * 姓名
     */
    @ApiModelProperty("姓名")
    private String name;

    /**
     * 性别
     */
    @ApiModelProperty("性别")
    private String sexText;

    /**
     * 年龄
     */
    @ApiModelProperty("年龄")
    private Integer age;

    /**
     * 年龄日期格式，格式:1岁1月17天
     */
    @ApiModelProperty("年龄日期格式，格式:1岁1月17天")
    private String ageDate;

    /**
     * 婚否 0否 1是
     */
    @ApiModelProperty("婚否")
    @TableField("marital")
    @FieldBind(target = "maritalText")
    private String marital;

    @TableField(exist = false)
    private String maritalText;

    @ApiModelProperty("民族code")
    private String nation;

    @ApiModelProperty("民族")
    private String nationName;

    @ApiModelProperty("头像")
    private String photo;

    @ApiModelProperty("单位编号")
    private String companyId;

    @ApiModelProperty("单位编号名称")
    private String companyName;

    @ApiModelProperty("部门ID")
    private String departmentId;

    @ApiModelProperty("部门名称")
    private String departmentName;

    @ApiModelProperty("分组")
    private String departmentGroupId;

    @ApiModelProperty("分组名称")
    private String departmentGroupName;

    @ApiModelProperty("套餐")
    private String packageName;

    @ApiModelProperty("套餐金额")
    private String packagePrice;

    @ApiModelProperty("总工龄")
    private String totalYears;

    /**
     * 接害工龄
     */
    @ApiModelProperty("接害工龄")
    private String workYears;

    /**
     * 体检分类表id
     */
    @ApiModelProperty("体检分类表id")
    private Long peTypeHdId;

    @ApiModelProperty("体检分类名称")
    private String peTypeHdName;

    /**
     * 体检类别表id
     */
    @ApiModelProperty("体检类别表id")
    private Long peTypeDtId;

    @ApiModelProperty("体检类别名称")
    private String peTypeDtName;

    @ApiModelProperty("接害种类")
    private String poisonType;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("接害种类名称")
    private List<String> poisonTypeName;

    @ApiModelProperty(value = "登记时间-体检数据插入时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp createTime;

    @ApiModelProperty("组合项目集合")
    private List<ProjectCheckBItemHdDto> itemHdDtoList;

    @ApiModelProperty(value = "是否携带组合项目,默认为False")
    private Boolean carryItemHd = Boolean.FALSE;

    /**
     * 工种ID
     */
    @ApiModelProperty("工种ID")
    private String job;

    /**
     * 工种名称
     */
    @ApiModelProperty("工种名称")
    private String jobName;

    /**
     * 签到时间,第一次体检时间
     */
    @ApiModelProperty("签到时间,第一次体检时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp signDate;

    /**
     * 真实检查时间
     */
    @ApiModelProperty("真实检查时间")
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp peDateReal;
}
