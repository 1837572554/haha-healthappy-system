package com.healthappy.modules.system.domain.vo;

import com.healthappy.modules.system.service.dto.BZyQuestionDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author Jevany
 * @date 2021/12/8 17:22
 * @desc
 */
@Data
@ApiModel("体检者中医体质问卷问题")
public class TPaitentZyQuestionVo {

    /** 体检号 */
    @NotBlank(message = "体检号不能为空")
    @ApiModelProperty("体检号")
    private String paId;

    /** 答案库编号 */
    @ApiModelProperty("答案库编号")
    private String answerBankId;


    /** 库编号  1：33题 2：67题 */
    @ApiModelProperty("库编号  1：33题 2：67题")
    private Integer bankId;

    /** 操作医生*/
    @NotBlank(message = "操作医生不能为空")
    @ApiModelProperty("操作医生")
    private String operationDoctor;

//    /** 适用性别 0：不限 1：男 2：女 */
//    @ApiModelProperty("适用性别 0：不限 1：男 2：女")
//    private String sex;

    /** 问卷问题列表 */
    @ApiModelProperty("问卷问题列表")
    private List<BZyQuestionDto> questionDtoList;

    /** 组合编号，保存时使用*/
    @NotBlank(message = "组合编号不能为空")
    @ApiModelProperty("组合编号")
    private String groupId;

    /** 结果 */
    @ApiModelProperty("结果")
    private String result;

    /** 描述 */
    @ApiModelProperty("描述")
    private String desc;
}
