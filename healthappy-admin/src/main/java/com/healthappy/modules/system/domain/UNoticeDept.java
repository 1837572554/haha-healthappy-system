package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 公告对应科室表
 * @author sjc
 * @date 2021-09-3
 */
@Data
@TableName("U_Notice_Dept")
@ApiModel("公告对应科室表")
public class UNoticeDept implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 公告id
     */
    @ApiModelProperty(value = "公告id",position = 1)
    private String noticeId;

    /**
     * 科室id
     */
    @ApiModelProperty(value = "科室id",position = 2)
    private String deptId;

    /** 租户id */
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;

}
