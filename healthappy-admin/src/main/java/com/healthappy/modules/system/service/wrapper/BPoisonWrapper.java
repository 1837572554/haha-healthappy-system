package com.healthappy.modules.system.service.wrapper;

import cn.hutool.core.bean.BeanUtil;
import com.healthappy.modules.system.domain.BPoison;
import com.healthappy.modules.system.domain.BPoisonSymptom;
import com.healthappy.modules.system.domain.vo.BPoisonSymptomVo;
import com.healthappy.modules.system.service.BPoisonSymptomService;
import com.healthappy.modules.system.service.dto.BPoisonDto;
import com.healthappy.utils.SpringUtil;

import java.util.stream.Collectors;

/**
 * 包装类,返回视图层所需的字段
 *
 * @author FGQ
 */
public class BPoisonWrapper extends BaseEntityWrapper<BPoison, BPoisonDto>{

    private final static BPoisonSymptomService bPoisonSymptomService;

    static {
        bPoisonSymptomService = SpringUtil.getBean(BPoisonSymptomService.class);
    }

    public static BPoisonWrapper build(){
        return new BPoisonWrapper();
    }

    @Override
    public BPoisonDto entityVO(BPoison entity) {
        BPoisonDto poisonDto = BeanUtil.copyProperties(entity, BPoisonDto.class);
        poisonDto.setBPoisonSymptomList(bPoisonSymptomService.lambdaQuery().eq(BPoisonSymptom::getPoisonId, entity.getId()).list().stream().map(p->BeanUtil.copyProperties(p, BPoisonSymptomVo.class)).collect(Collectors.toList()));
        return poisonDto;
    }
}
