package com.healthappy.modules.system.rest.common;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BTypeApplyForm;
import com.healthappy.modules.system.domain.vo.BTypeApplyFormVo;
import com.healthappy.modules.system.service.BTypeApplyFormService;
import com.healthappy.modules.system.service.dto.BTypeApplyFormQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description 申请单类型 控制器
 * @author sjc
 * @date 2021-08-3
 */
@Slf4j
@Api(tags = "默认基础设置：申请单类型")
@RestController
@AllArgsConstructor
@RequestMapping("/BTypeApplyForm")
public class BTypeApplyFormController {

    private final BTypeApplyFormService bTypeApplyFormService;

    @Log("分页查询结果获取方式")
    @ApiOperation("分页查询结果获取方式，权限码：BTypeApplyForm:list")
    @GetMapping
    @PreAuthorize("@el.check('BTypeApplyForm:list','BGroupHd:add')")
    public R listPage(BTypeApplyFormQueryCriteria criteria, Pageable pageable) {
        return R.ok(bTypeApplyFormService.queryAll(criteria, pageable));
    }

    @Log("新增|修改结果获取方式")
    @ApiOperation("新增|修改结果获取方式，权限码：admin")
    @PostMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BTypeApplyFormVo resources) {
        //如果名称重复
        if(checkBTypeApplyFormName(resources)) {
            return R.error(500,"结果获取名称已经存在");
        }
        BTypeApplyForm bTypeItem = new BTypeApplyForm();
        BeanUtil.copyProperties(resources,bTypeItem);
        if(bTypeApplyFormService.saveOrUpdate(bTypeItem))
            return R.ok();
        return R.error(500,"方法异常");
    }

    @Log("删除结果获取方式")
    @ApiOperation("删除结果获取方式，权限码：admin")
    @DeleteMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R remove(@RequestParam("id") Long id) {
        if(bTypeApplyFormService.deleteById(id))
            return R.ok();
        return R.error(500,"方法异常");
    }

    /**
     * 用于判断名称是否存在
     * @param resources
     * @return
     */
    private Boolean checkBTypeApplyFormName(BTypeApplyFormVo resources) {
        //名称相同  id不同的情况下能查询到数据，那么就是名称重复
        return bTypeApplyFormService.lambdaQuery().eq(BTypeApplyForm::getName,resources.getName())
                .ne(ObjectUtil.isNotNull(resources.getId()),BTypeApplyForm::getId,resources.getId()).count() > 0;

    }
}
