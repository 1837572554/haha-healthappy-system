package com.healthappy.modules.system.service.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author Jevany
 * @date 2022/1/20 16:55
 * @desc 人员档案基础信息查询类
 */
public class TPatientBaseinfoQueryCriteria {
    /**
     * 档案号
     */
    @ApiModelProperty(name = "pNo",value = "档案号")
    @Query(type = Query.Type.EQUAL)
    @JSONField(name="pNo")
    private Long pNo;

    /**
     * 身份证
     */
    @ApiModelProperty(value = " 身份证")
    @Query(type = Query.Type.EQUAL)
    private String idNo;



    /** 设置档案号 */
    @ApiModelProperty(name = "pNo",value = "档案号")
    @JsonProperty("pNo")
    public void setPNo(Long pNo){
        this.pNo=pNo;
    }

    /** 获得档案号 */
    @ApiModelProperty(name = "pNo",value = "档案号")
    @JsonProperty("pNo")
    public Long getPNo() {
        return this.pNo;
    }


    /** 获取身份证 */
    public String getIdNo() {
        return idNo;
    }
    /** 设置身份证 */
    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }
}