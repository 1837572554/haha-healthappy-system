package com.healthappy.modules.system.rest.common;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BTypePackage;
import com.healthappy.modules.system.domain.vo.BTypePackageVo;
import com.healthappy.modules.system.service.BTypePackageService;
import com.healthappy.modules.system.service.dto.BTypePackageQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 套餐类型 控制器
 * @author sjc
 * @date 2021-08-24
 */
@Slf4j
@Api(tags = "默认基础设置：套餐类型")
@RestController
@AllArgsConstructor
@RequestMapping("/BTypePackage")
public class BTypePackageController {

    private final BTypePackageService bTypePackageService;

    @Log("分页查询套餐类型")
    @ApiOperation("分页查询套餐类型，权限码：BTypePackage:list")
    @GetMapping
    public R listPage(BTypePackageQueryCriteria criteria, Pageable pageable) {
        return R.ok(bTypePackageService.queryAll(criteria, pageable));
    }

    @Log("新增|修改套餐类型")
    @ApiOperation("新增|修改套餐类型，权限码：BTypePackage:add")
    @PostMapping
    @PreAuthorize("@el.check('BTypePackage:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BTypePackageVo resources) {
        //如果名称重复
        if(checkBTypePackageName(resources)) {
            return R.error(500,"项目类型名称已经存在");
        }
        BTypePackage bTypePackage = new BTypePackage();
        BeanUtil.copyProperties(resources,bTypePackage);
        if(bTypePackageService.saveOrUpdate(bTypePackage))
            return R.ok();
        return R.error(500,"方法异常");
    }

    @Log("删除套餐类型")
    @ApiOperation("删除套餐类型，权限码：BTypePackage:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('BTypePackage:delete')")
    @Klock
    public R remove(@RequestBody Set<Long> ids) {
        if(bTypePackageService.removeByIds(ids))
            return R.ok();
        return R.error(500,"方法异常");
    }

    /**
     * 用于判断名称是否存在
     * @param resources
     * @return
     */
    private Boolean checkBTypePackageName(BTypePackageVo resources) {
        //名称相同  id不同的情况下能查询到数据，那么就是名称重复
        return bTypePackageService.lambdaQuery().eq(BTypePackage::getName,resources.getName())
                .ne(ObjectUtil.isNotNull(resources.getId()),BTypePackage::getId,resources.getId()).count() > 0;

    }

}
