package com.healthappy.modules.system.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

/**
 * 图文扫描录入管理 请求类
 * @author Jevany
 * @date 2022/2/15 14:05
 */
@Data
public class ImgTxtScanManageQueryCriteria {
    /**
     * 体检时间类型
     */
//    @NotNull(message = "体检时间类型不能为空")
    @ApiModelProperty(value = "体检时间类型 ,页面路径：imgTxtScanManage", position = 0)
    private Integer dateType;

    /** 时间段（数组） */
//    @NotEmpty(message = "时间段不能为空")
//    @Size(min = 2,max = 2,message = "必须2个对象")
    @ApiModelProperty(value = "时间段（数组） ", position = 1)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private List<Timestamp> timeList;

    /**
     * 体检分类ID
     */
    @ApiModelProperty("体检分类ID")
    private String peTypeHdId;

    /**
     * 体检类别ID
     */
    @ApiModelProperty("体检类别ID")
    private String peTypeDtId;

    /**
     * 体检号
     */
    @ApiModelProperty("体检号")
    private String paId;

    /**
     * 体检者姓名
     */
    @ApiModelProperty("体检者姓名")
    private String paName;

    /**
     * 单位ID
     */
    @ApiModelProperty("单位ID")
    private String companyId;

    /**类型编号 字典：scanType 1：指引单 2：信息表 3：信用承诺书 4：项目 */
    @ApiModelProperty("类型编号 字典：scanType 1：指引单 2：信息表 3：信用承诺书 4：项目")
    private String typeId;

    /** 组合项目Id（只有类型编号为4时才用） */
    @ApiModelProperty("组合项目Id（只有类型编号为4时才用）")
    private String groupId;

    /** 是否扫描 0：未扫描 1：已扫描 */
    @ApiModelProperty("是否扫描 0：未扫描 1：已扫描")
    private Integer isScan;

    /**
     * 页面显示条数
     */
    @ApiModelProperty(value = "页面显示条数,默认为10", position = 101)
    private Integer pageSize = 10;

    /**
     * 页数
     */
    @ApiModelProperty(value = "页数,默认为1", position = 102)
    private Integer pageNum = 1;

    /**
     * 开始条数
     */
    @ApiModelProperty(value = "开始条数", hidden = true)
    private Integer startIndex = -1;

}