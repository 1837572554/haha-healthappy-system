package com.healthappy.modules.system.rest.medical;

import cn.hutool.core.util.ObjectUtil;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BCommpanyGroup;
import com.healthappy.modules.system.service.BCommpanyGroupService;
import com.healthappy.modules.system.service.dto.BCommpanyGroupQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @description 集团   控制器
 * @author FGQ
 * @date 2021-10-15
 */
@Slf4j
@Api(tags = "体检管理：集团")
@RestController
@AllArgsConstructor
@RequestMapping("/BCommpanyGroup")
public class BCommpanyGroupController {

    private final BCommpanyGroupService BCommpanyGroupService;

    @Log("查询集团数据")
    @ApiOperation("查询集团数据，权限码：BCommpanyGroup:list")
    @GetMapping
    public R list(BCommpanyGroupQueryCriteria criteria, Pageable pageable) {
        return R.ok(BCommpanyGroupService.queryAll(criteria,pageable));
    }

    @Log("新增|修改集团")
    @ApiOperation("新增|修改集团，权限码：BCommpanyGroup:add")
    @PostMapping
    @PreAuthorize("@el.check('BCommpanyGroup:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BCommpanyGroup resources) {
        if(checkNameRepeat(resources)){
            throw new BadRequestException("集团名称重复");
        }
        return R.ok(BCommpanyGroupService.saveOrUpdate(resources));
    }

    @Log("删除集团")
    @ApiOperation("删除集团，权限码：BCommpanyGroup:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('BCommpanyGroup:delete')")
    @Klock
    public R remove(@RequestBody List<Long> ids) {
        return R.ok(BCommpanyGroupService.removeByIds(ids));
    }

    private boolean checkNameRepeat(BCommpanyGroup resources) {
        return BCommpanyGroupService.lambdaQuery().eq(BCommpanyGroup::getName,resources.getName())
                .ne(ObjectUtil.isNotNull(resources.getId()),BCommpanyGroup::getId,resources.getId()).count() > 0;
    }
}
