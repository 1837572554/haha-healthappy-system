package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BPeTypeJob;
import com.healthappy.modules.system.domain.BSicknessAdvice;
import com.healthappy.modules.system.service.dto.BPeTypeJobQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 病种建议表 服务层
 * @author fang
 * @date 2021-06-17
 */
public interface BSicknessAdviceService extends BaseService<BSicknessAdvice> {



}
