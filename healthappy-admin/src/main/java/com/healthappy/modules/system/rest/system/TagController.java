package com.healthappy.modules.system.rest.system;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.Tag;
import com.healthappy.modules.system.domain.vo.TagSignVo;
import com.healthappy.modules.system.service.TagService;
import com.healthappy.utils.CacheConstant;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@Api(tags = "系统：标识")
@RestController
@RequestMapping("/tag")
@RequiredArgsConstructor
public class TagController {

    private final TagService tagService;

    @ApiOperation("标识，权限码：tag:list")
    @GetMapping(value = "/all")
    @PreAuthorize("@el.check('tag:list')")
    public R getAll(Pageable pageable) {
        return R.ok(tagService.queryAll(pageable));
    }

    @ApiOperation("标识，权限码：tag:list")
    @GetMapping(value = "/list")
    @PreAuthorize("@el.check('tag:list')")
    public R list() {
        System.out.println(tagService.getPerByUserGroup("critical_one"));
        return R.ok(tagService.list());
    }

    @ApiOperation("获取权限集合，权限码：tag:list")
    @GetMapping(value = "/signs")
    @PreAuthorize("@el.check('tag:list')")
    public R signList(@RequestParam("type") String type) {
        return R.ok(tagService.signList(type));
    }

    @ApiOperation("标识，权限码：tag:add")
    @PostMapping
    @PreAuthorize("@el.check('tag:add')")
    @Klock
    @CacheEvict(cacheNames = {CacheConstant.TAG},allEntries = true)
    public R create(@Validated @RequestBody Tag tag) {
        if (checkNameRepeat(tag)) {
            throw new BadRequestException("名称不能相同");
        }
        return R.ok(tagService.saveOrUpdate(tag));
    }

    private boolean checkNameRepeat(Tag resources) {
        return tagService.lambdaQuery().eq(Tag::getName, resources.getName())
                .ne(ObjectUtil.isNotNull(resources.getId()), Tag::getId, resources.getId()).count() > 0;
    }

    @ApiOperation("修改权限，权限码：tag:add")
    @PostMapping("updPer")
    @PreAuthorize("@el.check('tag:add')")
    @Klock
    public R updPer(@Validated @RequestBody TagSignVo tagSignVo) {
        tagService.updPer(tagSignVo);
        return R.ok();
    }

    @ApiOperation("删除标识角色，权限码：tag:del")
    @DeleteMapping
    @PreAuthorize("@el.check('tag:del')")
    @Klock
    @CacheEvict(cacheNames = {CacheConstant.TAG},allEntries = true)
    public R del(@RequestBody Set<Long> ids) {
        tagService.del(ids);
        return R.ok();
    }


    /**
     * 自定义SQL 分页查询
     * @param pageable
     * @param name
     * @return
     */
    @GetMapping(value = "/customSql")
    @PreAuthorize("@el.check('tag:list')")
    public R customSql(Pageable pageable,String name) {
        return R.ok(tagService.customSql(pageable,name));
    }
}
