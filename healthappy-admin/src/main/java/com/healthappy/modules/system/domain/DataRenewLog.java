package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * @Author: wukefei
 * @Date: Created in 2022/5/6 17:06
 * @Description:
 * @Version: 1.0
 *
 */
@Data
@Accessors(chain = true)
@TableName("log_business_data_renew")
@ApiModel(value="检查数据更新日志", description="检查数据更新日志")
@Builder
public class DataRenewLog implements Serializable {

	private static final long serialVersionUID=1L;

	@ApiModelProperty(value = "主键")
	@TableId(type = IdType.INPUT)
	private  String id;

	@ApiModelProperty(value = "模型名称")
	private  String modelName;
	@ApiModelProperty(value = "类型（1增加，2修改，3删除）")
	private  String type;
	@ApiModelProperty(value = "数据创建时间")
	private LocalDateTime crateTime;

	@ApiModelProperty(value = "执行的sql")
	private  String sqlString;

	@ApiModelProperty(value = "0表示没有被同步，1表示已经被同步")
	private  boolean isSync;
}
