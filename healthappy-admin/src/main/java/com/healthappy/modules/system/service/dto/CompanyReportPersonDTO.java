package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 单位报告管理 人员列表对象
 * @author Jevany
 * @date 2022/2/18 11:38
 */
@Data
public class CompanyReportPersonDTO implements Serializable {
    /**
     * 体检者姓名
     */
    @ApiModelProperty("收费单号")
    private String payApplyId;
    /** 流水号 */
    @ApiModelProperty("流水号")
    private String paId;
    /** 体检分类 */
    @ApiModelProperty("体检分类")
    private String peTypeHdName;
    /** 体检类别 */
    @ApiModelProperty("体检类别")
    private String peTypeDtName;
    /** 姓名 */
    @ApiModelProperty("姓名")
    private String name;
    /** 性别 */
    @ApiModelProperty("性别")
    private String sex;
    /** 年龄 */
    @ApiModelProperty("年龄")
    private Integer age;
    /** 单位 */
    @ApiModelProperty("单位")
    private String companyName;
    /** 部门 */
    @ApiModelProperty("部门")
    private String deptName;
    /** 分组 */
    @ApiModelProperty("分组")
    private String deptGroupName;

    /** 体检时间 */
    @ApiModelProperty("体检时间")
    private String peDate;

    @ApiModelProperty("套餐名称")
    private String packageName;

    @ApiModelProperty("是否锁定")
    private String isLock;

    @Deprecated
    @ApiModelProperty("结账")
    private String payFlag;
}