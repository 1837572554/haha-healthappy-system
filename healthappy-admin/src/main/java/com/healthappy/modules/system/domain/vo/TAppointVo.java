package com.healthappy.modules.system.domain.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;


@Data
public class TAppointVo  {

    private String id;

    @ApiModelProperty("人员名称")
    private String name;

    @ApiModelProperty("性别")
    private String sex;

    @ApiModelProperty("年龄")
    private Integer age;

    @ApiModelProperty("身份证号")
    private String idNo;

    @ApiModelProperty("婚姻状况")
    private String marital;

    @ApiModelProperty("手机号")
    private String mobile;

    @ApiModelProperty("部门名称")
    private String departmentName;

    @ApiModelProperty("工号")
    private String code;

    @ApiModelProperty("工种")
    private String job;

    @ApiModelProperty("工种名称")
    private String jobName;

    @ApiModelProperty("毒害种类名称，多个使用顿号分隔")
    private String poisonType;

    @ApiModelProperty("套餐名称")
    private String packageName;

    @ApiModelProperty("附加套餐")
    private String addPackage;

    @ApiModelProperty("体检分类")
    private String peType;

    @ApiModelProperty("体检类别")
    private String type;

    @ApiModelProperty("工龄")
    private String workYears;

    @ApiModelProperty("社保号")
    private String ssNo;

    @ApiModelProperty("单位名称")
    private String companyName;

    @ApiModelProperty("单位id")
    private String companyId;

    @ApiModelProperty("预约类型")
    private String appointType;

    @ApiModelProperty("备注")
    private String mark;

    @ApiModelProperty("分组id")
    private String deptGroupId;

    @ApiModelProperty("分组名称")
    private String deptGroupName;

    @ApiModelProperty("人员来源")
    private String personnelSource;

    @ApiModelProperty("任务来源")
    private String charsi;

    @ApiModelProperty("结算归口")
    private String settlement;

    @ApiModelProperty("特殊减项目")
    private String subtractGroupItem;

    @ApiModelProperty("添加项目组合项编号")
    private String addGroupItemId;

    @ApiModelProperty("添加套餐编号")
    private String addPackageId;

    @ApiModelProperty("附加组合项目")
    private String addGroupItem;

    @ApiModelProperty("附加套餐ID")
    private String packageId;

    @ApiModelProperty("部门ID")
    private String departmentId;

    @ApiModelProperty("总工龄")
    private String totalYears;

    @ApiModelProperty("预约日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp appointDate;

    @ApiModelProperty("预约时间开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp appointDateStart;

    @ApiModelProperty("预约时间结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp appointDateEnd;
}
