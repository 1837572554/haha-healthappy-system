package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BItem;
import com.healthappy.modules.system.domain.TItemDT;
import com.healthappy.modules.system.service.TItemDTService;
import com.healthappy.modules.system.service.dto.TItemDTDto;
import com.healthappy.modules.system.service.dto.TItemDTQueryCriteria;
import com.healthappy.modules.system.service.mapper.TItemDTMapper;
import com.healthappy.utils.SecurityUtils;
import lombok.AllArgsConstructor;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sjc
 * @date 2021-07-20
 */
@Service
@AllArgsConstructor//有自动注入的功能
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class TItemDTServiceImpl extends BaseServiceImpl<TItemDTMapper, TItemDT> implements TItemDTService {

    private final IGenerator generator;
    private final TItemDTMapper tItemDTMapper;


    @Override
    public Map<String, Object> queryAll(TItemDTQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<TItemDT> page = new PageInfo<TItemDT>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("content", generator.convert(page.getList(), TItemDTDto.class));
        map.put("totalElements", page.getTotal());

        return map;
    }

    @Override
    public List<TItemDT> queryAll(TItemDTQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(TItemDT.class, criteria));
    }


    /**
     * 保存体检人员组合项目下的子项目 实现
     * @param paId
     * @param groupId
     * @return
     */
    @Override
    public void saveItemDt(String paId, String groupId, String sexId,Integer review)
    {
        tItemDTMapper.saveItemDt(paId,groupId,sexId,review);
    }

    /**
     * 通过体检号、组合项编号，删除体检子项数据(如果组合项编号为空，则删除体检号下的所有子项数据) 实现
     * yanjun
     * @param paId 体检号
     * @param groupId 组合项编号
     * @return 返回真假
     */
    @Override
    public boolean delItemDtByPaIdGroupId(String paId, String groupId) {
        return this.lambdaUpdate().eq(TItemDT::getPaId, paId)
                            .eq(ObjectUtil.isNotEmpty(groupId),TItemDT::getGroupId, groupId)
                            .remove();
    }

    /**
     * 删除指定体检号下，指定组合编号外的数据
     * @param paId
     * @param notInGroupIds
     * @return
     */
    @Override
    public boolean delItemDtNotInGroupIds(String paId,List<String> notInGroupIds)
    {
       return  this.lambdaUpdate().eq(TItemDT::getPaId, paId)
                            .notIn(CollUtil.isNotEmpty(notInGroupIds),TItemDT::getGroupId, notInGroupIds)
                            .remove();
    }

    @Override
    public List<BItem> getGroupIdList(String groupId) {
        return baseMapper.getGroupIdList(groupId);
    }

    @Override
    public void repeatItemId(List<String> paIds) {
        baseMapper.repeatItemId(paIds, SecurityUtils.getTenantId());
    }


}
