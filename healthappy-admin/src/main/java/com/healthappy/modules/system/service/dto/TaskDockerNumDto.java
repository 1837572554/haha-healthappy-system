package com.healthappy.modules.system.service.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class TaskDockerNumDto implements Serializable {

    private String docker;

    private Long num;

    private Long code;
}
