package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotBlank;
import lombok.Data;

/**
 * Created with IntelliJ IDEA. Created by yutang 2021/9/2 0002  11:11 Description:
 */
@Data
@ApiModel(value = "婚姻状况保存模型")
public class BMaritalStatusVo {

  private Long id;

  @ApiModelProperty(value = "编码",required = true)
  @NotBlank(message = "编码不可为空")
  private String code;

  @ApiModelProperty(value = "详情",required = true)
  @NotBlank(message = "详情不可为空")
  private String detail;

}
