package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 综合管理返回数据类
 * @author Jevany
 * @date 2022/2/8 17:24
 */
@Data
public class IntegratedManageDTO implements Serializable {
    /** 体检号 */
    @ApiModelProperty("体检号")
    private String id;

    /** 体检者名称 */
    @ApiModelProperty("体检者名称")
    private String name;

    /** 体检分类 */
    @ApiModelProperty("体检分类")
    private String peTypeHdId;

    /** 体检分类名称 */
    @ApiModelProperty("体检分类名称")
    private String peTypeHdName;

    /** 体检类别名称 */
    @ApiModelProperty("体检类别名称")
    private String peTypeDtName;

    /** 性别编号 */
    @ApiModelProperty("性别编号")
    private String sex;

    /** 性别中文 */
    @ApiModelProperty("性别中文")
    private String sexText;

    /** 年龄 */
    @ApiModelProperty("年龄")
    private Integer age;

    /** 公司名称 */
    @ApiModelProperty("公司名称")
    private String companyName;

    /** 部门 */
    @ApiModelProperty("部门")
    private String deptName;

    /** 分组 */
    @ApiModelProperty("分组")
    private String deptGroupName;

    /** 登记时间 */
    @ApiModelProperty("登记时间")
    private String createDate;

    /** 签到时间 */
    @ApiModelProperty("签到时间")
    private String signDate;

    /** 审核医生 */
    @ApiModelProperty("审核医生")
    private String verifyDoctor;

    /** 审核时间 */
    @ApiModelProperty("审核时间")
    private String verifyDate;

    /** 打印医生 */
    @ApiModelProperty("打印医生")
    private String printDoctor;

    /** 打印时间 */
    @ApiModelProperty("打印时间")
    private String printDate;

    /** 上传医生 */
    @ApiModelProperty("上传医生")
    private String uploadDoctor;

    /** 上传时间 */
    @ApiModelProperty("上传时间")
    private String uploadDate;
}