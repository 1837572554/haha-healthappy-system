package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 部门表
 * @author sjc
 * @date 2021-06-24
 */
@Data
@ApiModel("部门表")
@TableName("B_Department")
public class BDepartment implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    @TableId(type = IdType.INPUT)
    private String id;

    /**
     * 部门名称
     */
    @ApiModelProperty("部门名称")
    private String name;

    /**
     * 单位id
     */
    @ApiModelProperty("单位id")
    private String comId;

    /**
     * 是否启用 0否 1是
     */
    @ApiModelProperty("是否启用")
    private String isEnable;

    /**
     * 排序
     */
    @ApiModelProperty("排序")
    private String dispOrder;

    /**
     * 医疗机构ID(U_Hospital_Info的ID)
     */
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;

}
