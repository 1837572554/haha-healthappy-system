package com.healthappy.modules.system.service.dto;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.modules.system.domain.vo.BPoisonSymptomVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * @description 毒害表
 * @author sjc
 * @date 2021-08-9
 */
@Data
public class BPoisonDto  implements Serializable {



    /**
     * id
     */
    @ApiModelProperty("id")
    private String id;



    /**
     * 毒害名称
     */
    @ApiModelProperty("毒害名称")
    private String poisonName;
    /**
     * 症状名称
     */
    @ApiModelProperty("症状名称")
    private String symptomName;
    /**
     * 上岗套餐ID
     */
    @ApiModelProperty("上岗套餐ID")
    private String packageBeforeId;

    /**
     * 上岗套餐名称
     */
    @ApiModelProperty("上岗套餐名称")
    private String packageBeforeName;

    /**
     *在岗套餐id
     */
    @ApiModelProperty("在岗套餐id")
    private String packageInId;

    /**
     * 在岗套餐名称
     */
    @ApiModelProperty("在岗套餐名称")
    private String packageInName;

    /**
     * 离岗套餐ID
     */
    @ApiModelProperty("离岗套餐ID")
    private String packageAfterId;

    /**
     * 离岗套餐名称
     */
    @ApiModelProperty("离岗套餐名称")
    private String packageAfterName;

    /**
     * 应急套餐ID
     */
    @ApiModelProperty("应急套餐ID")
    private String packageTempId;

    /**
     * 应急套餐名称
     */
    @ApiModelProperty("应急套餐名称")
    private String packageTempName;

    /**
     * 标准
     */
    @ApiModelProperty("标准")
    private String standard;

    /**
     * 检索代码
     */
    @ApiModelProperty("检索代码")
    private String searchCode;

    /**
     * 岗前体检必检项目
     */
    @ApiModelProperty("岗前体检必检项目")
    private String mustBefore;

    /**
     * 岗前体检选检项目
     */
    @ApiModelProperty("岗前体检选检项目")
    private String chooseBefore;

    /**
     * 岗前体检职业病
     */
    @ApiModelProperty("岗前体检职业病")
    private String diseaseBefore;

    /**
     * 岗前体检体格检查
     */
    @ApiModelProperty("岗前体检体格检查")
    private String checkBefore;

    /**
     * 在岗体检必检项目
     */
    @ApiModelProperty("在岗体检必检项目")
    private String mustIn;

    /**
     * 在岗体检选检项目
     */
    @ApiModelProperty("在岗体检选检项目")
    private String chooseIn;

    /**
     * 在岗体检职业病
     */
    @ApiModelProperty("在岗体检职业病")
    private String diseaseIn;

    /**
     * 在岗体检体格检查
     */
    @ApiModelProperty("在岗体检体格检查")
    private String checkIn;

    /**
     * 离岗体检必检项目
     */
    @ApiModelProperty("离岗体检必检项目")
    private String mustAfter;

    /**
     * 离岗体检选检项目
     */
    @ApiModelProperty("离岗体检选检项目")
    private String chooseAfter;

    /**
     * 离岗体检职业病
     */
    @ApiModelProperty("离岗体检职业病")
    private String diseaseAfter;

    /**
     * 离岗体检禁忌症
     */
    @ApiModelProperty("离岗体检禁忌症")
    private String tabooAfter;

    /**
     * 离岗体检体格检查
     */
    @ApiModelProperty("离岗体检体格检查")
    private String checkAfter;

    /**
     * 应急体检必检项目
     */
    @ApiModelProperty("应急体检必检项目")
    private String mustOther;

    /**
     * 应急体检选检项目
     */
    @ApiModelProperty("应急体检选检项目")
    private String chooseOther;

    /**
     * 应急体检职业病
     */
    @ApiModelProperty("应急体检职业病")
    private String diseaseOther;

    /**
     * 应急体检禁忌症
     */
    @ApiModelProperty("应急体检禁忌症")
    private String tabooOther;

    /**
     * 应急体检体格检查
     */
    @ApiModelProperty("应急体检体格检查")
    private String checkOther;
    /**
     * 岗前体检禁忌症
     */
    @ApiModelProperty("岗前体检禁忌症")
    private String tabooBefore;
    /**
     * 在岗体检禁忌症
     */
    @ApiModelProperty("在岗体检禁忌症")
    private String tabooIn;
    /**
     * 排序号
     */
    @ApiModelProperty("排序号")
    private Integer dispOrder;

    /**
     * 毒害种类
     */
    @ApiModelProperty("毒害种类")
    private String posionType;

    /**
     * 修改时间
     */
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty("最后更新时间")
    private Timestamp updateTime;

    /**
     * 修改医生
     */
    @ApiModelProperty("修改医生")
    private String updateDoc;


    @ApiModelProperty("套餐名称")
    private String packageName;

    @ApiModelProperty("套餐id")
    private String packageId;

    /**
     * 毒害对应症状细表
     */
    @ApiModelProperty("毒害对应症状细表")
    private List<BPoisonSymptomVo> bPoisonSymptomList = new ArrayList<>();
}
