package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.*;
import com.healthappy.modules.system.domain.vo.TagSignVo;
import com.healthappy.modules.system.service.TagService;
import com.healthappy.modules.system.service.dto.TagDto;
import com.healthappy.modules.system.service.mapper.SignMapper;
import com.healthappy.modules.system.service.mapper.TagMapper;
import com.healthappy.modules.system.service.mapper.TagSignMapper;
import com.healthappy.modules.system.service.mapper.UserTagMapper;
import com.healthappy.utils.CacheConstant;
import com.healthappy.utils.SecurityUtils;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@CacheConfig(cacheNames = {CacheConstant.TAG})
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class TagServiceImpl extends BaseServiceImpl<TagMapper, Tag> implements TagService {

    private final SignMapper signMapper;
    private final TagSignMapper tagSignMapper;
    private final UserTagMapper userTagMapper;

    @Override
    @Cacheable
    public Map<String, Object> queryAll(Pageable pageable) {
        getPage(pageable);
        PageInfo<Tag> page = new PageInfo<>(baseMapper.selectList(new QueryWrapper<>()));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", page.getList().stream().map(k->{
            TagDto tagDto = BeanUtil.copyProperties(k,TagDto.class);
            tagDto.setSignIds(tagSignMapper.selectList(Wrappers.<TagSign>lambdaQuery().eq(TagSign::getTagId,k.getId())).stream().map(TagSign::getSignId).collect(Collectors.toList()));
            return tagDto;
        }).collect(Collectors.toList()));
        map.put("totalElements", page.getTotal());
        return map;
    }

    @Override
    @Cacheable
    public List<Sign> signList(String type) {
        return signMapper.selectList(Wrappers.<Sign>lambdaQuery().eq(Sign::getType,type));
    }

    @Override
    @CacheEvict(cacheNames = {CacheConstant.TAG},allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void updPer(TagSignVo tagSignVo) {
        tagSignMapper.delete(Wrappers.<TagSign>lambdaUpdate().eq(TagSign::getTagId,tagSignVo.getTagId()));
        tagSignVo.getSignIds().stream().map(k->{
            TagSign tagSign = new TagSign();
            tagSign.setTagId(tagSignVo.getTagId());
            tagSign.setSignId(k);
            return tagSign;
        }).collect(Collectors.toList()).forEach(tagSignMapper::insert);
        //更新Tag表权限
        this.lambdaUpdate().eq(Tag::getId,tagSignVo.getTagId()).set(Tag::getPermission,CollUtil.isNotEmpty(tagSignVo.getSignIds()) ? signMapper.selectBatchIds(tagSignVo.getSignIds()).stream().map(Sign::getValue).collect(Collectors.joining(",")) : "").update();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @CacheEvict(cacheNames = {CacheConstant.TAG},allEntries = true)
    public void del(Set<Long> ids) {
        tagSignMapper.delete(Wrappers.<TagSign>lambdaUpdate().in(TagSign::getTagId,ids));
        baseMapper.delete(Wrappers.<Tag>lambdaUpdate().in(Tag::getId,ids));
    }

    @Override
    @Cacheable
    public void bindUserAndTag(Long userId, Set<Long> tagIds) {
        userTagMapper.delete(Wrappers.<UserTag>lambdaUpdate().eq(UserTag::getUserId,userId));
        tagIds.stream().map(k->{
            UserTag userTag = new UserTag();
            userTag.setUserId(userId);
            userTag.setTagId(k);
            return userTag;
        }).collect(Collectors.toList()).forEach(userTagMapper::insert);
    }

    @Override
    @Cacheable
    public List<String> getCurrentPermissionList() {
        List<UserTag> userTagList = userTagMapper.selectList(Wrappers.<UserTag>lambdaQuery().eq(UserTag::getUserId, SecurityUtils.getUserId()));
        if(CollUtil.isEmpty(userTagList)){
            return Collections.emptyList();
        }
         return this.listByIds(userTagList.stream().map(UserTag::getTagId).collect(Collectors.toList()))
                .stream()
                 .filter(k->StrUtil.isNotBlank(k.getPermission()))
                 .map(k-> Stream.of(k.getPermission().split(",")).distinct().collect(Collectors.toList()))
                 .flatMap(Collection::stream).collect(Collectors.toList());
    }

    @Override
    @Cacheable
    public boolean isIncludePer(String per) {
        return getCurrentPermissionList().contains(per);
    }

    @Override
    @Cacheable
    public List<Long> getPerByUserGroup(String... per) {
        List<Tag> tags = this.list();
        if(CollUtil.isEmpty(tags)){
            return Collections.emptyList();
        }
        List<Long> tagIds = tags.stream().filter(k -> Arrays.asList(k.getPermission().split(",")).containsAll(Arrays.asList(per))).map(Tag::getId).collect(Collectors.toList());
        if(CollUtil.isEmpty(tagIds)){
            return Collections.emptyList();
        }
        return userTagMapper.selectList(Wrappers.<UserTag>lambdaQuery().in(UserTag::getTagId,tagIds)).stream().map(UserTag::getUserId).collect(Collectors.toList());
    }

    @Override
    @Cacheable
    public List<User> getUsersByTagPer(String per) {
        if(StrUtil.isBlank(per)) {
            return Collections.emptyList();
        }
        return userTagMapper.getUsersByTagPer(per);
    }

    @Override
    @Cacheable
    public List<User> getUsersByTagPer(String per, String tenantId) {
        if(StrUtil.isBlank(per)) {
            return Collections.emptyList();
        }
        return userTagMapper.getUsersByTagPerTenantId(per,tenantId);
    }

    @Override
    @Cacheable
    public Set<Long> getUserIdByTag(Long id) {
        List<UserTag> userTagList = userTagMapper.selectList(Wrappers.<UserTag>lambdaQuery().eq(UserTag::getUserId, id));
        if(CollUtil.isEmpty(userTagList)){
           return Collections.emptySet();
        }
        return userTagList.stream().map(UserTag::getTagId).collect(Collectors.toSet());
    }

    @Override
    @Cacheable
    public IPage<Tag> customSql(Pageable pageable, String name) {
        return baseMapper.customSql(new Page<>(pageable.getPageNumber(), pageable.getPageSize()),name);
    }
}
