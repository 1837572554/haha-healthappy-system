package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 套餐类型
 * @author sjc
 * @date 2021-08-24
 */
@Data
@ApiModel("套餐类型")
@TableName("B_Type_Package")
public class BTypePackage implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    @TableId(type = IdType.AUTO)
    private Long id;


    /**
     * 排序
     */
    @ApiModelProperty("排序")
    private Integer dispOrder;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    private String name;

}
