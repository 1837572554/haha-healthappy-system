package com.healthappy.modules.system.rest.medical;

import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.vo.Base64ImgVo;
import com.healthappy.modules.system.domain.vo.TImageVo;
import com.healthappy.modules.system.service.TImageService;
import com.healthappy.utils.R;
import com.healthappy.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @desc: 图片控制类
 * @author: YJ
 * @date: 2021-11-30 18:17
 **/

@Slf4j
@Api(tags = "体检管理：图片相关")
@RestController
@AllArgsConstructor
@RequestMapping("/TImage")
public class TImageController {

    private final TImageService tImageService;

    @Log("上传图片")
    @PostMapping("/upload")
    @ApiOperation("上传图片")
//    @PreAuthorize("@el.check('ProjectCheck:update')")
    @ApiResponses(value = {
            @ApiResponse(code=200,message = "图片Vo",response = TImageVo.class)
    })
    @Klock
    public R upload(@RequestParam MultipartFile file) {
        return R.ok(tImageService.upload(file, SecurityUtils.getNickName()));
    }

    @Log("上传图片集合")
    @PostMapping("/uploads")
    @ApiOperation("上传图片集合")
//    @PreAuthorize("@el.check('ProjectCheck:update')")
    @ApiResponses(value = {
            @ApiResponse(code=200,message = "图片Vo",response = TImageVo.class,responseContainer ="List")
    })
    @Klock
    public R uploads(@RequestParam MultipartFile[] files) {
        return R.ok(tImageService.uploads(files, SecurityUtils.getNickName()));
    }

    @Log("上传Base64图片")
    @PostMapping("/uploadBase64")
    @ApiOperation("上传Base64图片")
//    @PreAuthorize("@el.check('ProjectCheck:update')")
    @ApiResponses(value = {
            @ApiResponse(code=200,message = "图片Vo",response = TImageVo.class)
    })
    @Klock
    public R uploadBase64(@Validated @RequestBody Base64ImgVo base64ImgVo) {
        return R.ok(tImageService.uploadBase64(base64ImgVo, SecurityUtils.getNickName()));
    }

    @Log("上传Base64图片集合")
    @PostMapping("/uploadBase64List")
    @ApiOperation("上传Base64图片集合")
//    @PreAuthorize("@el.check('ProjectCheck:update')")
    @ApiResponses(value = {
            @ApiResponse(code=200,message = "图片Vo",response = TImageVo.class,responseContainer ="List")
    })
    @Klock
    public R uploadBase64List(@Validated @RequestBody List<Base64ImgVo> base64ImgVos) {
        return R.ok(tImageService.uploadBase64List(base64ImgVos, SecurityUtils.getNickName()));
    }
}
