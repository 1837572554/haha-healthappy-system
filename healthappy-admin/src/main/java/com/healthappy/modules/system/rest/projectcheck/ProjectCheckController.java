package com.healthappy.modules.system.rest.projectcheck;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.additional.query.impl.LambdaQueryChainWrapper;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.enums.PeDateTypeEnum;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.TItemHD;
import com.healthappy.modules.system.domain.vo.GroupResultVo;
import com.healthappy.modules.system.domain.vo.ProjectCheckVo;
import com.healthappy.modules.system.domain.vo.TItemHDVo;
import com.healthappy.modules.system.service.ProjectCheckService;
import com.healthappy.modules.system.service.TItemHDService;
import com.healthappy.modules.system.service.dto.DiagnosisRelatedDtQueryCriteria;
import com.healthappy.modules.system.service.dto.DiagnosisRelatedQueryCriteria;
import com.healthappy.modules.system.service.dto.ProjectCheckBItemHdDto;
import com.healthappy.modules.system.service.dto.ProjectCheckQueryCriteria;
import com.healthappy.modules.system.service.mapper.TPatientMapper;
import com.healthappy.utils.R;
import com.healthappy.utils.SecurityUtils;
import com.healthappy.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author FGQ
 * @date 2021-10-25
 */
@Slf4j
@Api(tags = "体检流程：项目检查")
@RestController
@AllArgsConstructor
@RequestMapping("/projectCheck")
public class ProjectCheckController {

	private final IGenerator generator;

	private final TItemHDService tItemHDService;

	private final ProjectCheckService projectCheckService;

	private final TPatientMapper tPatientMapper;

	@Log("查询体检人员")
	@ApiOperation("查询体检人员，权限码：ProjectCheck:list")
	@GetMapping("/query/item")
	@PreAuthorize("@el.check('ProjectCheck:list')")
	public R list(ProjectCheckQueryCriteria criteria) {
		if (ObjectUtil.isNotNull(criteria.getResultStatus()) || ObjectUtil.isNotNull(criteria.getOnlyOneself())
				|| ObjectUtil.isNotNull(criteria.getPeDateType())) {
			getPatientById(criteria);
		}
		return R.ok(projectCheckService.queryAll(criteria));
	}

	@Log("查询体检人员-精简")
	@ApiOperation("查询体检人员-精简，权限码：ProjectCheck:list")
	@GetMapping("/query/itemLite")
	@PreAuthorize("@el.check('ProjectCheck:list')")
	public R listLite(ProjectCheckQueryCriteria criteria) {
		criteria.setTenantId(SecurityUtils.getTenantId());
		criteria.setDoctor(SecurityUtils.getNickName());
		return R.ok(tPatientMapper.getProjectCheckList(criteria));
	}


	@Log("查询一个体检人员")
	@ApiOperation("查询一个体检人员，权限码：ProjectCheck:list")
	@GetMapping("/query/one")
	@PreAuthorize("@el.check('ProjectCheck:list')")
	public R one(@RequestParam(required = true) String paId) {
		if (StringUtils.isEmpty(paId)) {
			throw new BadRequestException("编号不能为空");
		}
		return R.ok(projectCheckService.queryOne(paId));
	}


	@Log("诊断相关查询")
	@ApiOperation("诊断相关查询，权限码：ProjectCheck:list")
	@GetMapping("/diagnosis")
	@PreAuthorize("@el.check('ProjectCheck:list')")
	public R diagnosisRelatedQueries(@Validated DiagnosisRelatedQueryCriteria criteria) {
		return R.ok(generator.convert(tItemHDService.list(QueryHelpPlus.getPredicate(TItemHD.class, criteria)),
				ProjectCheckBItemHdDto.class));
	}

	@Log("体检项目及结果查询")
	@ApiOperation("体检项目及结果查询，权限码：ProjectCheck:list")
	@GetMapping("/result")
	@PreAuthorize("@el.check('ProjectCheck:list')")
	public R checkupItemsAndResultsQuery(@Validated DiagnosisRelatedDtQueryCriteria criteria) {
		return R.ok(projectCheckService.checkupItemsAndResultsQuery(criteria));
	}

	@Log("修改项目及结果")
	@ApiOperation("修改项目及结果，权限码：ProjectCheck:update")
	@PostMapping("/update/result")
	@PreAuthorize("@el.check('ProjectCheck:update')")
	@Klock
	public R saveOrUpdate(@RequestBody @Validated ProjectCheckVo projectCheckVo) {
		projectCheckService.updProjectCheck(projectCheckVo);
		return R.ok();
	}

	@Log("根据体检号和组合项目id撤销项目结果")
	@ApiOperation("根据体检号和组合项目id撤销项目结果，权限码：ProjectCheck:update")
	@PostMapping("/revocation")
	@PreAuthorize("@el.check('ProjectCheck:update')")
	@Klock
	public R revocation(@RequestBody TItemHDVo hd) {
		tItemHDService.revocation(hd.getPaId(), hd.getGroupId());
		return R.ok();
	}

	@Log("根据体检号和组合项目id弃检项目")
	@ApiOperation("根据体检号和组合项目id弃检项目，权限码：ProjectCheck:update")
	@PostMapping("/abandon")
	@PreAuthorize("@el.check('ProjectCheck:update')")
	@Klock
	public R abandon(@RequestBody TItemHDVo hd) {
		tItemHDService.abandon(hd.getPaId(), hd.getGroupId());
		return R.ok();
	}

	@Log("计算项目结果的病种")
	@ApiOperation("计算项目结果的病种，权限码：ProjectCheck:list")
	@PostMapping("/calcItemSickness")
	@PreAuthorize("@el.check('ProjectCheck:list')")
	public R calcItemSickness(@Valid @RequestBody GroupResultVo groupResultVo) {
		if (StrUtil.isBlank(groupResultVo.getPaId())) {
			return R.error("流水号不能为空");
		}
		if (StrUtil.isBlank(groupResultVo.getGroupId())) {
			return R.error("组合编号不能为空");
		}
		if (CollUtil.isEmpty(groupResultVo.getItemResultVos())) {
			return R.error("小项结果集合不能为空");
		}
		return R.ok(projectCheckService.generateResult(groupResultVo));
	}

	@Log("获取体检者历史体检记录")
	@ApiOperation("获取体检者历史体检记录，权限码：ProjectCheck:list")
	@GetMapping("/getHistory")
	@PreAuthorize("@el.check('ProjectCheck:list')")
	public R getHistory(@RequestParam(required = true) String paId) {
		if (StringUtils.isEmpty(paId)) {
			throw new BadRequestException("编号不能为空");
		}
		return R.ok(projectCheckService.getHistory(paId));
	}

	private void getPatientById(ProjectCheckQueryCriteria criteria) {
		LambdaQueryChainWrapper<TItemHD> wrapper = tItemHDService.lambdaQuery()
				//已完成 getResultDate不等于false
				.isNotNull(ObjectUtil.isNotNull(criteria.getResultStatus()) && criteria.getResultStatus().equals(true),
						TItemHD::getResultDate)
				//未完成 getResultDate等于false
				.isNull(ObjectUtil.isNotNull(criteria.getResultStatus()) && criteria.getResultStatus().equals(false),
						TItemHD::getResultDate)
				//是本人的加条件检索，仅自己的条件是基于已完成的情况下
				.eq(ObjectUtil.isNotNull(criteria.getOnlyOneself()) && criteria.getOnlyOneself()
								&& ObjectUtil.isNotNull(criteria.getResultStatus()) && criteria.getResultStatus().equals(true),
						TItemHD::getDoctor, SecurityUtils.getNickName())
				//登记时间
				.between(ObjectUtil.isNotNull(criteria.getPeDateType()) && PeDateTypeEnum.REGISTRATION.getValue()
								.equals(criteria.getPeDateType()) && CollUtil.isNotEmpty(criteria.getPeDate()),
						TItemHD::getAddTime, criteria.getPeDate().get(0), criteria.getPeDate().get(1))
				//完成时间
				.between(ObjectUtil.isNotNull(criteria.getPeDateType()) && PeDateTypeEnum.COMPLETION.getValue()
								.equals(criteria.getPeDateType()) && CollUtil.isNotEmpty(criteria.getPeDate()),
						TItemHD::getResultDate, criteria.getPeDate().get(0), criteria.getPeDate().get(1))
				//签到时间
				.exists(ObjectUtil.isNotNull(criteria.getPeDateType()) && PeDateTypeEnum.SIGN.getValue()
								.equals(criteria.getPeDateType()) && CollUtil.isNotEmpty(criteria.getPeDate()),
						"select 1 from T_Patient tp where (tp.sign_date BETWEEN '" + criteria.getPeDate().get(0)
								+ "' and '" + criteria.getPeDate().get(1) + "') and tp.tenant_id='"
								+ SecurityUtils.getTenantId() + "' and tp.id=T_Item_HD.pa_id LIMIT 1");
		int workStationLength = criteria.getWorkstation().length();
		if (workStationLength > 0) {
			wrapper.apply(workStationLength > 0,
					" SUBSTRING(T_Item_HD.pa_id,7," + workStationLength + ")='" + criteria.getWorkstation() + "'");
		}
		criteria.setIds(
				wrapper.select(TItemHD::getPaId).list().stream().map(TItemHD::getPaId).collect(Collectors.toList()));
		if (!Optional.ofNullable(criteria.getIds()).isPresent() || criteria.getIds().size() == 0) {
			//查询不到数据时，说明已经匹配不到数据了，直接赋值个没有的编号，为null的话，外层会查出所有人的数据
			List<String> tmpList = new ArrayList<>();
			tmpList.add("-1");
			criteria.setIds(tmpList);
		}
	}
}
