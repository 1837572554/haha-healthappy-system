package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BSickness;
import com.healthappy.modules.system.domain.vo.BSicknessVo;
import com.healthappy.modules.system.service.dto.BSicknessDto;
import com.healthappy.modules.system.service.dto.BSicknessQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @description 健康疾病建议库
 * @author fang
 * @date 2021-06-17
 */
public interface BSicknessService extends BaseService<BSickness> {


    Map<String, Object> queryAll(BSicknessQueryCriteria criteria, Pageable pageable);


    /**
     * 查询数据分页
     * @param criteria 条件
     * @return Map<String, Object>
     */
    List<BSickness> queryAll(BSicknessQueryCriteria criteria);


    List<BSicknessDto> buildList(List<BSicknessDto> list);


    void saveOrUpdateSickness(BSicknessVo resources);

    void deleteSickness(Set<String> ids);
}
