package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class BSicknessVo {

    /**
     *  病种编号
     */
    @ApiModelProperty("病种编号")
    private String id;

    /**
     * 病种名称
     */
    @NotBlank(message = "病种名称不能为空")
    @ApiModelProperty("病种名称")
    private String name;

    /**
     * 简拼
     */
    @ApiModelProperty("简拼")
    private String jp;

    /**
     * 显示顺序
     */
    @ApiModelProperty("显示顺序")
    private Integer dispOrder;

    /**
     * 是否是病种组合子项
     */
    @ApiModelProperty("是否是病种组合子项")
    private String isCombine;

    /**
     * 病种组合名称
     */
    @ApiModelProperty("病种组合名称")
    private String combineName;

    /**
     * 是否属于重大异常
     */
    @ApiModelProperty("是否属于重大异常")
    private String anomaly;

    /**
     * 病种组合名称
     */
    @ApiModelProperty("病种组合名称")
    private String zx;

    /**
     * 统计名称
     */
    @ApiModelProperty("统计名称")
    private String nameTj;

    /**
     * 病种等级
     */
    @ApiModelProperty("病种等级")
    private Integer grade;

    /* ------------------------------------------------------------- */

    /**
     * 病种建议
     */
    @ApiModelProperty("病种建议")
    private String sicknessAdviceName;

    /** 病种原因 */
    @ApiModelProperty("病种原因")
    private String sicknessCauseName;

    /** 医学解释 */
    @ApiModelProperty("医学解释")
    private String sicknessScienceName;
}
