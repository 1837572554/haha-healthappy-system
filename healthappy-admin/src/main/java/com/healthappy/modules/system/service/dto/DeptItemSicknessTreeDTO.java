package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 科室、明细项、病种数据DTO
 * @author Jevany
 * @date 2022/3/8 14:15
 */
@Data
@AllArgsConstructor
@ApiModel("科室、明细项、病种数据DTO")
public class DeptItemSicknessTreeDTO implements Serializable {

    /** 科室编号 */
    @ApiModelProperty("科室编号")
    private String key;

    /** 科室名称 */
    @ApiModelProperty("科室名称")
    private String title;

    /** 明细项目病种对象列表 */
    @ApiModelProperty("明细项目病种对象列表")
    List<ItemSicknessDTO> children=new ArrayList<>();
}