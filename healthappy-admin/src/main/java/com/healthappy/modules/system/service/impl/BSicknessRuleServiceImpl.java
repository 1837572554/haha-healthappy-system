package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.*;
import com.healthappy.modules.system.service.*;
import com.healthappy.modules.system.service.dto.*;
import com.healthappy.modules.system.service.mapper.BSicknessRuleMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author sjc
 * @date 2021-06-22
 */
@Service
@AllArgsConstructor//有自动注入的功能
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BSicknessRuleServiceImpl extends BaseServiceImpl<BSicknessRuleMapper, BSicknessRule>
		implements BSicknessRuleService {

	private final IGenerator generator;

	private final BSicknessRuleMapper bSicknessRuleMapper;

	private final BSicknessService bSicknessService;

	private final BItemService bItemService;

	private final BDeptService bDeptService;

	private final BSicknessAdviceService sicknessAdviceService;

	/**
	 * 查询数据
	 * @return Object
	 */
	@Override
	public Object queryAlls(BSicknessRuleQueryCriteria criteria) {
		//List<BSicknessRule> roleList = baseMapper.selectList(QueryHelpPlus.getPredicate(BSicknessRule.class, criteria));
		List<BSicknessRule> roleList = bSicknessRuleMapper.selectList(
				QueryHelpPlus.getPredicate(BSicknessRule.class, criteria));
		List<BSicknessRuleDto> ruleDtoList = generator.convert(roleList, BSicknessRuleDto.class);
		return buildList(ruleDtoList);
	}

	@Override
	public Object queryAll(BSicknessRuleQueryCriteria criteria, Pageable pageable) {
		getPage(pageable);
		PageInfo<BSicknessRule> page = new PageInfo<BSicknessRule>(queryAll(criteria));
		Map<String, Object> map = new LinkedHashMap<>(2);
		List<BSicknessRuleDto> ruleDtoList = generator.convert(page.getList(), BSicknessRuleDto.class);
		map.put("content", buildList(ruleDtoList));
		map.put("totalElements", page.getTotal());
		return map;
	}

	@Override
	public List<BSicknessRule> queryAll(BSicknessRuleQueryCriteria criteria) {
		if (StrUtil.isNotBlank(criteria.getSicknessName())) {
			List<String> sicknessIdListByName = bSicknessService.lambdaQuery()
					.like(BSickness::getName, criteria.getSicknessName()).select(BSickness::getId).list().stream()
					.map(BSickness::getId).collect(Collectors.toList());
			if (CollUtil.isEmpty(sicknessIdListByName)) {
				//如果有病种名称，且查询不到病种，则直接返回空数据
				return new ArrayList<BSicknessRule>();
			}
			//有数据则加到临时数据中
			List<String> sicknessIdList = new ArrayList<>();
			sicknessIdList.addAll(sicknessIdListByName);
			//如果原来已经传了，也加到临时数据中
			if (CollUtil.isNotEmpty(criteria.getSicknessId())) {
				sicknessIdList.addAll(criteria.getSicknessId());
			}
			//重新赋值
			criteria.setSicknessId(sicknessIdList);
		}
		QueryWrapper wrapper = QueryHelpPlus.getPredicate(BSicknessRule.class, criteria);
		//查询的规则必须要有启用的病种
		wrapper.exists(
				"select 1 from B_Sickness bs where bs.tenant_id =B_Sickness_Rule.tenant_id and bs.del_flag='0' and bs.id=B_Sickness_Rule.sickness_id");
		String customSqlSegment = wrapper.getCustomSqlSegment();

		return baseMapper.getAll(wrapper);

		//        LambdaQueryWrapper<BSicknessRule> wrapper = Wrappers.<BSicknessRule>lambdaQuery();
		//        wrapper.in(CollUtil.isNotEmpty(criteria.getSicknessId()),BSicknessRule::getSicknessId,criteria.getSicknessId())
		//                .like(StrUtil.isNotBlank(criteria.getRuleType()),BSicknessRule::getRuleType, criteria.getRuleType())
		//                .eq(StrUtil.isNotBlank(criteria.getItemId()),BSicknessRule::getItemId, criteria.getItemId())
		//                .eq(StrUtil.isNotBlank(criteria.getDelFlag()),BSicknessRule::getDelFlag, criteria.getDelFlag());
		//

		//        return baseMapper.selectList(wrapper);
	}

	@Override
	public List<BSicknessRuleDto> buildList(List<BSicknessRuleDto> ruleDtoList) {
		for (BSicknessRuleDto rule : ruleDtoList) {
			if (ObjectUtil.isNotNull(rule.getSicknessId())) {
				BSickness serviceById = bSicknessService.getById(rule.getSicknessId());
				rule.setBSicknessName(Optional.ofNullable(serviceById).map(BSickness::getName).orElse(""));
			}
			BSicknessAdvice sicknessAdvice = sicknessAdviceService.getOne(
					Wrappers.<BSicknessAdvice>lambdaQuery().eq(BSicknessAdvice::getSicknessId, rule.getSicknessId())
							.last("limit 1"));
			rule.setSicknessAdviceName(Optional.ofNullable(sicknessAdvice).map(BSicknessAdvice::getJy).orElse(""));
		}
		return ruleDtoList;
	}

	@Override
	public Object tree(BItemTreeQueryCriteria criteria) {
		List<BItem> itemList = bItemService.list(QueryHelpPlus.getPredicate(BItem.class, criteria));
		if (CollUtil.isEmpty(itemList)) {
			return new ArrayList<>();
		}
		return bDeptService.listByIds(itemList.stream().map(BItem::getDeptId).collect(Collectors.toList())).stream()
				.map(dept -> {
					BDeptTreeDto treeDto = BeanUtil.copyProperties(dept, BDeptTreeDto.class);
					treeDto.setBItemList(itemList.stream().filter(item -> item.getDeptId().equals(treeDto.getId()))
							.collect(Collectors.toList()));
					return treeDto;
				}).collect(Collectors.toList());
	}

	@Override
	public Object search(SearchSicknessRuleCriteria criteria) {
		List<BSickness> list = bSicknessService.list(QueryHelpPlus.getPredicate(BSickness.class, criteria));
		if (CollUtil.isEmpty(list)) {
			return Collections.EMPTY_LIST;
		}
		List<String> ids = list.stream().map(BSickness::getId).collect(Collectors.toList());
		List<BSicknessRule> ruleList = this.lambdaQuery().in(BSicknessRule::getSicknessId, ids).list();
		if (CollUtil.isEmpty(ruleList)) {
			return Collections.EMPTY_LIST;
		}

		List<BItem> itemList = bItemService.lambdaQuery()
				.in(BItem::getId, ruleList.stream().map(BSicknessRule::getItemId).collect(Collectors.toList())).list();

		List<BDept> deptList = bDeptService.lambdaQuery()
				.in(BDept::getId, itemList.stream().map(BItem::getDeptId).collect(Collectors.toList())).list();

		return deptList.stream().map(d -> {
			SearchSicknessTreeDto bItemTreeDto = new SearchSicknessTreeDto();
			bItemTreeDto.setId(d.getId());
			bItemTreeDto.setDeptName(d.getName());

			List<ItemTreeDto> itemTreeDtoList = generator.convert(
					itemList.stream().filter(i -> i.getDeptId().equals(d.getId())).collect(Collectors.toList()),
					ItemTreeDto.class).stream().peek(x -> {
				List<BSicknessRule> sicknessRuleList = ruleList.stream().filter(r -> r.getItemId().equals(x.getId()))
						.collect(Collectors.toList());

				x.setSicknessTreeList(list.stream()
						.filter(l -> sicknessRuleList.stream().anyMatch(s -> s.getSicknessId().equals(l.getId())))
						.map(b -> BeanUtil.copyProperties(b, SicknessTreeDto.class)).peek(t -> t.setSicknessRuleId(
								Optional.of(sicknessRuleList.stream()
												.filter(y -> ObjectUtil.isNotNull(t.getId()) && y.getSicknessId()
														.equals(t.getId().toString())).findAny().get())
										.map(BSicknessRule::getId).orElse(""))).collect(Collectors.toList()));
			}).collect(Collectors.toList());
			bItemTreeDto.setItemTreeList(itemTreeDtoList);
			return bItemTreeDto;
		}).collect(Collectors.toList());
	}

	@Override
	public List<ItemSicknessMatchDto> txtItemSicknessMath(String itemId, String result, String mark, String sex) {
		if (StrUtil.isBlank(itemId)) {
			return new ArrayList<>();
		}
		return bSicknessRuleMapper.txtItemSicknessMath(itemId, result, mark, sex);
	}

	@Override
	public List<ItemSicknessMatchDto> numItemSicknessMath(String itemId, String result, String mark, String sex) {
		if (StrUtil.isBlank(itemId)) {
			return new ArrayList<>();
		}
		return bSicknessRuleMapper.numItemSicknessMath(itemId, result, mark, sex);
	}

	@Override
	public List<SicknessByItemDto> getByItemId(String itemId) {
		return baseMapper.getByItemId(itemId);
	}


	@Override
	public boolean removeByIds(Set<String> ids) {
		return this.lambdaUpdate().in(BSicknessRule::getId, ids).remove();
	}
}
