package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BSetUp;
import com.healthappy.modules.system.service.dto.BSetUpQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 综合设置 服务层
 * @author cyt
 * @date 2021-08-17
 */
public interface BSetUpService  extends BaseService<BSetUp> {
    Map<String, Object> queryAll(BSetUpQueryCriteria criteria, Pageable pageable);
    List<BSetUp> queryAll(BSetUpQueryCriteria criteria);
}
