package com.healthappy.modules.system.service.dto;

import com.healthappy.modules.system.domain.BCompanyScale;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 企业规模表
 * @author sjc
 * @date 2021-09-2
 */
@Data
public class BCompanyScaleDto extends BCompanyScale {
}
