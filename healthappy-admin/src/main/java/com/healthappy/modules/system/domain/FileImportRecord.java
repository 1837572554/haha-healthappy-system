package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 上传记录
 * @author FGQ
 */
@Data
public class FileImportRecord implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;

    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;

    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_by",fill = FieldFill.INSERT)
    private Long createBy;

    @ApiModelProperty("是否成功")
    private Boolean isSuccess;

    @ApiModelProperty("文件地址")
    private String fileAddress;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Timestamp createTime;

    @ApiModelProperty("文件名称")
    private String fileName;

    @ApiModelProperty("文件是否正确")
    private Boolean isFileCorrect;

    @ApiModelProperty("文件类型")
    private String fileType;
}
