package com.healthappy.modules.system.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.BReportConfig;
import com.healthappy.modules.system.service.BReportConfigService;
import com.healthappy.modules.system.service.mapper.BReportConfigMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * 报告配置 服务实现类
 *
 * @author Jevany
 * @date 2022/4/11 15:21
 */
@AllArgsConstructor
@Service
public class BReportConfigServiceImpl extends BaseServiceImpl<BReportConfigMapper, BReportConfig> implements BReportConfigService {
    @Override
    public BReportConfig getBReportConfig(String reportName, Integer peTypeGroupId, Integer reportType) {
        BReportConfig bReportConfig = baseMapper.selectOne(Wrappers.<BReportConfig>lambdaQuery()
                .eq(BReportConfig::getReportName, reportName)
                .eq(BReportConfig::getPeTypeGroupId, peTypeGroupId)
                .eq(BReportConfig::getReportType, reportType)
                .last("limit 1"));
        if (Optional.ofNullable(bReportConfig).isPresent()) {
            return bReportConfig;
        }
        return new BReportConfig();
    }
}