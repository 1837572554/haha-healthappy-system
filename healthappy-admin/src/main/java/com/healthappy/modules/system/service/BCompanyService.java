package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BCompany;
import com.healthappy.modules.system.service.dto.BCompanyQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 单位表 服务层
 * @author sjc
 * @date 2021-06-24
 */
public interface BCompanyService extends BaseService<BCompany> {

    Map<String, Object> queryAll(BCompanyQueryCriteria criteria, Pageable pageable);

    /**
     * 查询全部数据
     * @return Object
     */
    List<BCompany> queryAlls(BCompanyQueryCriteria criteria);

    /**
     * 判断单位是否存在
     * @param companyId 单位id
     * @return
     */
    boolean isExistCompany(String companyId);

//    /**
//     * 获得最大的单位ID
//     * @param workStationCode
//     * @param tenantId
//     * @return
//     */
//    BCompany getMaxIDCompany(String workStationCode, String tenantId);

//    /**
//     * 生成单位编号
//     * @param tenantId 医疗机构ID(U_Hospital_Info的ID)
//     * @return 返回单位编号
//     */
//    String generateCompanyKey(String tenantId);
}
