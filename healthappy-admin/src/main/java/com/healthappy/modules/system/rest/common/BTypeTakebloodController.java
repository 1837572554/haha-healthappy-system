package com.healthappy.modules.system.rest.common;

import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BTypeTakeblood;
import com.healthappy.modules.system.service.BTypeTakebloodService;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 从业采血
 * @author FGQ
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "默认基础设置：从业采血")
@RestController
@AllArgsConstructor
@RequestMapping("/bTypeTakeblood")
public class BTypeTakebloodController {

    private final BTypeTakebloodService bTypeTakebloodService;

    @Log("查询从业采血")
    @ApiOperation("查询从业采血，权限码：bTypeTakeblood:list")
    @GetMapping("/list")
    public R list() {
        return R.ok(bTypeTakebloodService.list());
    }

    @Log("新增|修改从业采血")
    @ApiOperation("新增|修改从业采血，权限码：admin")
    @PostMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BTypeTakeblood resources) {
        return R.ok(bTypeTakebloodService.saveOrUpdate(resources));
    }

    @Log("删除从业采血")
    @ApiOperation("删除从业采血，权限码：admin")
    @DeleteMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R remove(@RequestBody Set<Integer> ids) {
        return R.ok(bTypeTakebloodService.removeByIds(ids));
    }
}
