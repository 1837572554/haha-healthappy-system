package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import java.util.List;

/**
 * 体检项目修改VO
 * @author Jevany
 * @date 2022/2/9 11:52
 */
@Data
public class TPatientItemVO {
    /**
     *体检号
     */
    @ApiModelProperty("体检号")
    private String id;

    /**
     * 体检分类表id
     */
    @ApiModelProperty("体检分类表id")
    private Long peTypeHdId;


    /** 组合项目List */
    @Valid
    @ApiModelProperty("组合项目List")
    private List<TItemHDVo> items;
}
