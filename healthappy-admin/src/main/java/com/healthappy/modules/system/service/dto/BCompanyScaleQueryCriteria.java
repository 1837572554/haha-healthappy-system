package com.healthappy.modules.system.service.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 企业规模表
 * @author sjc
 * @date 2021-09-2
 */
@Data
public class BCompanyScaleQueryCriteria  {



    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    @Query(type = Query.Type.EQUAL)
    private Long id;

    /**
     * 规模类型
     */
    @ApiModelProperty("规模类型")
    @Query(type = Query.Type.INNER_LIKE)
    private String type;

    /**
     * 标准代码
     */
    @ApiModelProperty("标准代码")
    @Query(type = Query.Type.EQUAL)
    private String code;
}
