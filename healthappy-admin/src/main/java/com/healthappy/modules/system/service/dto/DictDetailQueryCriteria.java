package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import lombok.Data;

/**
 * @author hupeng
 * @date 2020-05-14
 */
@Data
public class DictDetailQueryCriteria {

    @Query(type = Query.Type.INNER_LIKE)
    private String label;

    private String dictName;
}
