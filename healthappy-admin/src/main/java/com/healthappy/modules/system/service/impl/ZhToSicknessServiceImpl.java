package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.BSickness;
import com.healthappy.modules.system.domain.BSicknessAdvice;
import com.healthappy.modules.system.domain.BSicknessZH;
import com.healthappy.modules.system.domain.ZhToSickness;
import com.healthappy.modules.system.domain.vo.SicknessZHVo;
import com.healthappy.modules.system.domain.vo.ZhToSicknessVo;
import com.healthappy.modules.system.service.BSicknessAdviceService;
import com.healthappy.modules.system.service.BSicknessService;
import com.healthappy.modules.system.service.BSicknessZHService;
import com.healthappy.modules.system.service.ZhToSicknessService;
import com.healthappy.modules.system.service.dto.BSicknessZHQueryCriteria;
import com.healthappy.modules.system.service.dto.ZhToSicknessDto;
import com.healthappy.modules.system.service.dto.ZhToSicknessQueryCriteria;
import com.healthappy.modules.system.service.mapper.ZhToSicknessMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;


/**
 * @author sjc
 * @date 2021-08-2
 */
@Service
@AllArgsConstructor//有自动注入的功能
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class ZhToSicknessServiceImpl extends BaseServiceImpl<ZhToSicknessMapper, ZhToSickness> implements ZhToSicknessService {

    private final IGenerator generator;
    private final BSicknessZHService bSicknessZHService;
    private final BSicknessService sicknessService;
    private final BSicknessAdviceService sicknessAdviceService;


    @Override
    public Map<String, Object> queryAll(ZhToSicknessQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<ZhToSickness> page = new PageInfo<ZhToSickness>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>();
        List<ZhToSicknessDto> convert = generator.convert(page.getList(), ZhToSicknessDto.class);
        buildList(convert);
        map.put("content",convert);
        map.put("totalElements", page.getTotal());
        return map;
    }

    private void buildList(List<ZhToSicknessDto> convert) {
        for (ZhToSicknessDto dto:convert) {
            if(ObjectUtil.isNotNull(dto.getSicknessId())){
                BSickness bSickness = sicknessService.getById(dto.getSicknessId());
                if(Optional.ofNullable(bSickness).isPresent()){
                    dto.setSicknessName(StrUtil.isNotBlank(bSickness.getName()) ? bSickness.getName() : "");
                    BSicknessAdvice sicknessAdvice = sicknessAdviceService.lambdaQuery().eq(BSicknessAdvice::getSicknessId, bSickness.getId()).one();
                    if(Optional.ofNullable(sicknessAdvice).isPresent()){
                        dto.setSicknessAdviceJy(StrUtil.isNotBlank(sicknessAdvice.getJy()) ? sicknessAdvice.getJy() : "");
                        dto.setSicknessAdviceId(ObjectUtil.isNotEmpty(sicknessAdvice.getId()) ? sicknessAdvice.getId() : null);
                    }
                }
            }
        }
    }

    @Override
    public List<ZhToSickness> queryAll(ZhToSicknessQueryCriteria criteria) {
        List list = baseMapper.selectList(QueryHelpPlus.getPredicate(ZhToSickness.class, criteria));
        for (Object obj : list) {
            ZhToSickness zh = (ZhToSickness)obj;
            BSicknessZHQueryCriteria criteria1 = new BSicknessZHQueryCriteria();
            criteria1.setSicknessZhId(zh.getId());
            List<BSicknessZH> sicknessZHS = bSicknessZHService.queryAll(criteria1);
            if(CollUtil.isNotEmpty(sicknessZHS)){
                for (BSicknessZH vo: sicknessZHS) {
                    BSickness sickness = sicknessService.getById(vo.getSicknessId());
                    if(Optional.ofNullable(sickness).isPresent()){
                        vo.setSicknessName(StrUtil.isNotBlank(sickness.getName()) ? sickness.getName() : "");
                    }
                }
                Map<Long, List<BSicknessZH>> listMap = sicknessZHS.stream()
                        .collect(Collectors.groupingBy(z->Optional.ofNullable(z.getContainsFlag()).orElse(0L)));
                List<BSicknessZH> bSicknessZHS = listMap.get(0L);
                List<BSicknessZH> sicknessZHList = listMap.get(1L);
                zh.setContainSicknessZHList(CollUtil.isNotEmpty(bSicknessZHS) ? bSicknessZHS : Collections.emptyList());
                zh.setDoesContainsicknessZHList(CollUtil.isNotEmpty(sicknessZHList) ? sicknessZHList : Collections.emptyList());
            }

        }
        return list;
    }

    @Override
    public boolean deleteById(Long id)
    {
        return baseMapper.deleteById(id)>0;
    }

    @Override
    public void saveOrUpdateZhToSickness(ZhToSicknessVo resources) {
        if(CollUtil.isEmpty(resources.getBSicknessZHVoList())){
            throw new BadRequestException("病种组合-子项集合数据必须设置");
        }
        for (SicknessZHVo sicknessZHVo : resources.getBSicknessZHVoList()) {
            if(ObjectUtil.isNull(sicknessZHVo.getContainsFlag())){
                throw new BadRequestException("子项是否包含参数不能为Null");
            }
        }

        ZhToSickness zhToSickness = BeanUtil.copyProperties(resources, ZhToSickness.class);
        this.saveOrUpdate(zhToSickness);
        List<SicknessZHVo> bSicknessZHVoList = resources.getBSicknessZHVoList();

//        if (ObjectUtil.isNull(resources.getId())) {
//            saveBatchList(bSicknessZHVoList,zhToSickness.getId());
//        } else {
//            Map<Boolean, List<BSicknessZHVo>> booleanListMap = bSicknessZHVoList.stream().collect(Collectors.partitioningBy(b -> ObjectUtil.isNotNull(b.getId())));
//            //删除数据库中存在但前端没传的数据
//            bSicknessZHService.lambdaUpdate().notIn(CollUtil.isNotEmpty(booleanListMap.get(true)), BSicknessZH::getId,
//                    booleanListMap.get(true).stream().map(BSicknessZHVo::getId).collect(Collectors.toList())
//            ).eq(BSicknessZH::getSicknessZhId, resources.getId()).remove();
//            //新增数据库中没有的数据
//            saveBatchList(booleanListMap.get(false),zhToSickness.getId());
            bSicknessZHService.lambdaUpdate().eq(BSicknessZH::getSicknessZhId,zhToSickness.getId()).remove();
            if(CollUtil.isNotEmpty(bSicknessZHVoList)){
                saveBatchList(bSicknessZHVoList,zhToSickness.getId());
            }

//        }
    }

    private void saveBatchList(List<SicknessZHVo> bSicknessZHVoList,String id){
        bSicknessZHService.saveBatch(generator.convert(bSicknessZHVoList,BSicknessZH.class).stream().peek(t->t.setSicknessZhId(id)).collect(Collectors.toList()));
//        bSicknessZHService.saveBatch(generator.convert(bSicknessZHVoList.stream().peek(t -> t.setSicknessZhId(id)).collect(Collectors.toList()),BSicknessZH.class));
    }

}
