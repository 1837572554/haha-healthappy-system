package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @description 分组项目表
 * @author sjc
 * @date 2021-06-24
 */
@Data
@ApiModel("分组项目表")
@TableName("B_Department_Group_TD")
public class BDepartmentGroupTD implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     * 分组id
     */
    @ApiModelProperty("分组id")
    private String departmentGroupHdId;


    /**
     * 组合项目id
     */
    @ApiModelProperty("组合项目id")
    private String groupId;

    /**
     * 组合项目名称
     */
    @ApiModelProperty("组合项目名称")
    @TableField(exist=false)
    private String groupName;

    /**
     * 项目原价
     */
    @ApiModelProperty("项目原价")
    private Double price;

    /**
     * 折扣
     */
    @ApiModelProperty("折扣")
    private Double discount;
    /**
     * 应付金额
     */
    @ApiModelProperty("应付金额")
    private Double cost;
    /**
     * 是否是职检项目 0否1是
     */
    @ApiModelProperty("是否是职检项目")
    private String typeZ;

    /**
     * 是否是普检项目 0否1是
     */
    @ApiModelProperty("是否是普检项目")
    private String typeJ;



    /**
     * 更新时间
     */
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty("更新时间")
    @TableField(fill = FieldFill.UPDATE)
    private Timestamp updteTime;

    @TableField(exist=false)
    @ApiModelProperty("性别")
    private String sex;

    @TableField(exist=false)
    @ApiModelProperty("组合项目的科室编号")
    private String deptId;

    /** 租户编号 */
    @ApiModelProperty("租户编号")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;
}
