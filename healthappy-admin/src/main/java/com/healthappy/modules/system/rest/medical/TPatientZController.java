package com.healthappy.modules.system.rest.medical;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.TPatientZ;
import com.healthappy.modules.system.domain.vo.TPatientZVo;
import com.healthappy.modules.system.service.TPatientZService;
import com.healthappy.modules.system.service.dto.TPatientZQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @description 职业体检人员 控制器
 * @author sjc
 * @date 2021-09-8
 */
@Slf4j
@Api(tags = "体检管理：职业体检人员")
@RestController
@AllArgsConstructor
@RequestMapping("/TPatientZ")
public class TPatientZController {

    private final TPatientZService tPatientzService;

    @Log("分页查询职业体检人员数据")
    @ApiOperation("分页查询职业体检人员数据，权限码：TPatient:list")
    @GetMapping
    @PreAuthorize("@el.check('TPatient:list')")
    public R listPage(TPatientZQueryCriteria criteria, Pageable pageable) {
        return R.ok(tPatientzService.queryAll(criteria, pageable));
    }

    @Log("新增|修改 职业体检人员数据")
    @ApiOperation("新增|修改  职业体检人员数据，权限码：TPatient:add")
    @PostMapping
    @PreAuthorize("@el.check('TPatient:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody TPatientZVo resources) {
        return R.ok(tPatientzService.saveOrUpdate(BeanUtil.copyProperties(resources,TPatientZ.class),new LambdaQueryWrapper<TPatientZ>().eq(TPatientZ::getPaId,resources.getPaId())));
    }

    @Log("删除职业体检人员数据")
    @ApiOperation("删除职业体检人员数据，权限码：TPatient:delete")
    @DeleteMapping()
    @PreAuthorize("@el.check('TPatient:delete')")
    @Klock
    public R remove(@RequestParam("paId") Long paId) {
        if(tPatientzService.lambdaUpdate().eq(TPatientZ::getPaId,paId).remove())
            return R.ok();
        return R.error(500,"方法异常");
    }
}
