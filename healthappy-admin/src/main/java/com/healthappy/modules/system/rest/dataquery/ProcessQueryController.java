package com.healthappy.modules.system.rest.dataquery;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.service.ProcessQueryService;
import com.healthappy.modules.system.service.dto.ProcessQueryDto;
import com.healthappy.modules.system.service.dto.ProcessQueryQueryCriteria;
import com.healthappy.utils.R;
import com.healthappy.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Jevany
 * @date 2022/1/26 9:26
 * @desc 体检进度查询
 */
@Slf4j
@Api(tags = "数据查询：体检进度查询")
@RestController
@AllArgsConstructor
@RequestMapping("/processQuery")
public class ProcessQueryController {

    private final ProcessQueryService processQueryService;

    @Log("体检进度")
    @ApiOperation("体检进度，权限码：processQuery:list")
    @GetMapping
    @PreAuthorize("@el.check('processQuery:list')")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "体检进度查询", response = ProcessQueryDto.class)
    })
    public R list(ProcessQueryQueryCriteria criteria) {
//        if (ObjectUtil.isNull(criteria.getDateType())) {
//            return R.error("时间类型必填");
//        }
//        if (StrUtil.isBlank(criteria.getStartTime()) || StrUtil.isBlank(criteria.getEndTime())) {
//            return R.error("查询时间条件有误");
//        }
        if (StrUtil.isBlank(criteria.getTenantId())) {
            criteria.setTenantId(SecurityUtils.getTenantId());
        }
        if (criteria.getIsExport().equals(false)) {
            if (ObjectUtil.isEmpty(criteria.getPageNum()) || criteria.getPageNum() <= 0) {
                criteria.setPageNum(1);
            }
            if (ObjectUtil.isEmpty(criteria.getPageSize()) || criteria.getPageSize() <= 0) {
                criteria.setPageSize(10);
            }
        }
        return R.ok(processQueryService.getProcessQueryMap(criteria));
    }

    @Log("体检进度导出")
    @ApiOperation("体检进度导出，权限码：processQuery:upload")
    @GetMapping("/download")
    @PreAuthorize("@el.check('processQuery:upload')")
    @Klock
    public void download(HttpServletResponse response, ProcessQueryQueryCriteria criteria) throws IOException {
        if (StrUtil.isBlank(criteria.getTenantId())) {
            criteria.setTenantId(SecurityUtils.getTenantId());
        }
        criteria.setIsExport(true);
        processQueryService.download(processQueryService.getProcessQueryList(criteria), response);
    }


}
