package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 *
 * @author Jevany
 * @date 2022/5/7 2:56
 */
@ApiModel("预约号源详细数据DTO")
@Data
public class BAppointDateNumDTO implements Serializable {

	/**
	 * 单位编号
	 */
	@ApiModelProperty(value = "单位编号")
	@NotBlank(message = "单位编号不能为空")
	private String companyId;

	/**
	 * 人数
	 */
	@ApiModelProperty(value = "人数")
	@NotNull(message = "人数不能为空")
	private Integer num;

	private static final long serialVersionUID = 1L;
}