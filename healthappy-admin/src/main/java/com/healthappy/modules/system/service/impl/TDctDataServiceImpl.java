package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.TDctData;
import com.healthappy.modules.system.domain.TDctDataXz;
import com.healthappy.modules.system.domain.vo.TDctDataVo;
import com.healthappy.modules.system.service.TDctDataService;
import com.healthappy.modules.system.service.dto.TDctDataDto;
import com.healthappy.modules.system.service.mapper.TDctDataMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @desc: 电测听数据服务 实现
 * @author: YJ
 * @date: 2021-12-02 15:49
 **/
@Service
@AllArgsConstructor//有自动注入的功能
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class TDctDataServiceImpl extends BaseServiceImpl<TDctDataMapper, TDctData> implements TDctDataService {

    private final TDctDataXzServiceImpl xmServiceImpl;
    private final IGenerator generator;

    @Override
    public TDctDataVo getDctData(String paId) {
        TDctDataVo dctVo=new TDctDataVo();
        dctVo.setPaId(paId);
        List<TDctData> list = this.list(new LambdaQueryWrapper<TDctData>().eq(TDctData::getPaId, paId));

        dctVo.setData(Optional.ofNullable(list).isPresent()?list.stream().map(d-> BeanUtil.copyProperties(d,TDctDataDto.class)).collect(Collectors.toList()): new ArrayList());
        List<TDctDataXz> list1 = xmServiceImpl.list(new LambdaQueryWrapper<TDctDataXz>().eq(TDctDataXz::getPaId, paId));
        dctVo.setDataXz(Optional.ofNullable(list).isPresent()?list1.stream().map(xz->BeanUtil.copyProperties(xz,TDctDataDto.class)).collect(Collectors.toList()):new ArrayList<>());
        return dctVo;
    }

    @Override
    public void saveDctData(TDctDataVo vo) {
        this.lambdaUpdate().eq(TDctData::getPaId,vo.getPaId()).remove();
        List<TDctData> datas = generator.convert(vo.getData(), TDctData.class);
        datas.forEach(d->d.setPaId(vo.getPaId()));

        xmServiceImpl.lambdaUpdate().eq(TDctDataXz::getPaId,vo.getPaId()).remove();
        List<TDctDataXz> dataXzs = generator.convert(vo.getDataXz(), TDctDataXz.class);
        dataXzs.forEach(d->d.setPaId(vo.getPaId()));

        this.saveBatch(datas);
        xmServiceImpl.saveBatch(dataXzs);
    }
}
