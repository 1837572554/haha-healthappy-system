package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * Created with IntelliJ IDEA. Created by yutang 2021/9/1 0001  16:29 Description:
 */
@Data
@ApiModel(value = "分组项目保存模型")
public class BDepartmentGroupTDSaveVo implements Serializable {

  private static final long serialVersionUID = -4554775887206332620L;

  @ApiModelProperty(value = "分组id")
  @NotNull(message = "分组id不可为空")
  private String departmentGroupHdId;

  @ApiModelProperty(value = "分组项目列表")
  private List<BDepartmentGroupTDVo> resources;

}
