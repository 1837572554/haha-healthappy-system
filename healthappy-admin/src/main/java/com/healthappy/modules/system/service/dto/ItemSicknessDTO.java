package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 明细项基础信息DTO
 * @author Jevany
 * @date 2022/3/8 14:18
 */
@Data
@AllArgsConstructor
@ApiModel("明细项基础信息DTO")
public class ItemSicknessDTO implements Serializable {

    /** 明细项id */
    @ApiModelProperty("明细项id")
    private String key;

    /** 明细项名称 */
    @ApiModelProperty("明细项名称")
    private String title;

    /** 病种基础信息列表 */
    @ApiModelProperty("病种基础信息列表")
    private List<SicknessBaseDTO> children=new ArrayList<>();
}