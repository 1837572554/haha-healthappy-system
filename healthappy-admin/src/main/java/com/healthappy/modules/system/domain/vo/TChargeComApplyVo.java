package com.healthappy.modules.system.domain.vo;

import com.healthappy.modules.system.domain.TChargeComApply;
import io.swagger.annotations.*;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * 单位收费申请表扩展
 * @author Jevany
 * @date 2022/4/1 11:06
 */
@ApiModel("单位收费申请表扩展")
@Data
public class TChargeComApplyVo extends TChargeComApply {

    /** 是否人员绑定过 */
    @ApiModelProperty("是否人员绑定过")
    private Boolean patientBind;
}
