package com.healthappy.modules.system.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.List;

/**
 * 单位职业报告管理 查询类
 * @author Jevany
 * @date 2022/2/17 10:56
 */
@Data
public class CompanyReportQueryCriteria {
    /** 时间段（数组） */
    @NotEmpty(message = "时间段不能为空")
    @Size(min = 2,max = 2,message = "必须2个对象")
    @ApiModelProperty(value = "时间段（数组） ",  position = 1)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private List<Timestamp> timeList;

    /** 审核类型 0：全部，1：是，2：否 */
    @ApiModelProperty("审核类型 0：全部，1：是，2：否")
    private Integer verifyType=0;

    /**
     * 报告类型 1：职业 2：健康
     */
    @ApiModelProperty(value = "报告类型 1：职业 2：健康" ,hidden = true)
    private Integer repType;

    /**
     * 页面显示条数
     */
    @ApiModelProperty(value = "页面显示条数,默认为10", position = 101)
    private Integer pageSize = 10;

    /**
     * 页数
     */
    @ApiModelProperty(value = "页数,默认为1", position = 102)
    private Integer pageNum = 1;

    /**
     * 开始条数
     */
    @ApiModelProperty(value = "开始条数", hidden = true)
    private Integer startIndex = -1;
}