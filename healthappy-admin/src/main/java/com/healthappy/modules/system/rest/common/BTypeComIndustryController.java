package com.healthappy.modules.system.rest.common;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BTypeComIndustry;
import com.healthappy.modules.system.domain.vo.BTypeComIndustryVo;
import com.healthappy.modules.system.service.BTypeComIndustryService;
import com.healthappy.modules.system.service.dto.BTypeComIndustryQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @description 企业从事行业类型表 控制器
 * @author sjc
 * @date 2021-08-30
 */
@Slf4j
@Api(tags = "默认基础数据：企业从事行业类型")
@RestController
@AllArgsConstructor
@RequestMapping("/BTypeComIndustry")
public class BTypeComIndustryController {

    private final BTypeComIndustryService bTypeComIndustryService;


    @Log("查询全部企业从事行业类型")
    @ApiOperation("查询全部企业从事行业类型，权限码：BTypeComIndustry:list")
    @GetMapping("/list")
    public R listPage(BTypeComIndustryQueryCriteria criteria) {
        return R.ok(bTypeComIndustryService.queryAll(criteria));
    }

    @Log("新增|修改企业从事行业类型")
    @ApiOperation("新增|修改企业从事行业类型，权限码：admin")
    @PostMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BTypeComIndustryVo resources) {
        if(checkBTypeComIndustryName(resources)){
            return R.error(500,"该类型名称已经存在");
        }
        BTypeComIndustry bTypeComIndustry1 = new BTypeComIndustry();
        BeanUtil.copyProperties(resources,bTypeComIndustry1);
        if(bTypeComIndustryService.saveOrUpdate(bTypeComIndustry1))
            return R.ok();
        return R.error(500,"方法异常");
    }

    @Log("删除企业从事行业类型")
    @ApiOperation("删除企业从事行业类型，权限码：admin")
    @DeleteMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R remove(@RequestParam("id") Long id) {
        if(bTypeComIndustryService.removeById(id))
            return R.ok();
        return R.error(500,"方法异常");
    }

    /**
     * 用于判断名称是否存在
     * @param resources
     * @return
     */
    private Boolean checkBTypeComIndustryName(BTypeComIndustryVo resources) {
        //名称相同  id不同的情况下能查询到数据，那么就是名称重复
        return bTypeComIndustryService.lambdaQuery().eq(BTypeComIndustry::getName,resources.getName())
                .ne(ObjectUtil.isNotNull(resources.getId()),BTypeComIndustry::getId,resources.getId()).count() > 0;

    }
}
