package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 组合项目物价表
 * @author SJC
 * @date 2021-08-3
 */
@Data
@ApiModel("组合项目物价表")
@TableName("B_Group_HD_WuJia")
public class BGroupHDWuJia implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     *主键id
     */
    @ApiModelProperty("主键id")
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 组合项目ID
     */
    @ApiModelProperty("组合项目ID")
    private String itemGroupId;

    /**
     * 物价项目ID
     */
    @ApiModelProperty("物价项目ID")
    private String itemPriceId;


    /**
     * 物价项目折扣
     */
    @ApiModelProperty("物价项目折扣")
    private Long itemDiscountType;

    /**
     * 物价项目数量
     */
    @ApiModelProperty("物价项目数量")
    private Long itemQuantity;



    /**
     * 物价项目折扣比率
     */
    @ApiModelProperty("物价项目折扣比率")
    private Double itemDiscountValue;

    /**
     * 项目编码
     */
    @ApiModelProperty("项目编码")
    private String itemCode;


    /**
     * 价格
     */
    @ApiModelProperty("价格")
    private Double price;


    /**
     * 规格
     */
    @ApiModelProperty("规格")
    private String pkgName;


    /**
     * 单位
     */
    @ApiModelProperty("单位")
    private String pkgUnit;


    /**
     * 费别编号
     */
    @ApiModelProperty("费别编号")
    private Long payOffId;


    /**
     * 费别名称
     */
    @ApiModelProperty("费别名称")
    private String payOff;


    /**
     * 物价名称
     */
    @ApiModelProperty("物价名称")
    private String itemCnname;


    /**
     * His物价项目Localid
     */
    @ApiModelProperty("His物价项目Localid")
    private Long localId;


    /**
     * 医疗机构id(u_hospital_info的id)
     */
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;
}
