package com.healthappy.modules.system.rest.system;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateTime;
import com.healthappy.annotation.AnonymousAccess;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.UNotice;
import com.healthappy.modules.system.domain.UNoticeDept;
import com.healthappy.modules.system.domain.vo.UNoticeVo;
import com.healthappy.modules.system.service.UNoticeDeptService;
import com.healthappy.modules.system.service.UNoticeService;
import com.healthappy.modules.system.service.dto.UNoticeQueryCriteria;
import com.healthappy.utils.CacheConstant;
import com.healthappy.utils.R;
import com.healthappy.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @description 公告表（阿斯利康）控制器
 * @author fang
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "系统设置：公告设置")
@RestController
@AllArgsConstructor
@RequestMapping("/UNotice")
public class UNoticeController {

    private final UNoticeService UNoticeService;
    private final UNoticeDeptService uNoticeDeptService;

    @Log("查询公告")
    @ApiOperation("查询公告，权限码：UNotice:list")
    @GetMapping
    @PreAuthorize("@el.check('UNotice:list')")
    public R list(UNoticeQueryCriteria criteria, Pageable pageable) {
        return R.ok(UNoticeService.queryAll(criteria, pageable));
    }

    @Log("新增|修改公告")
    @ApiOperation("新增|修改公告，权限码：UNotice:add")
    @PostMapping
    @PreAuthorize("@el.check('UNotice:add')")
    @Klock
    @CacheEvict(cacheNames = {CacheConstant.UNOTICE,CacheConstant.U_NOTICE_DEPT},allEntries = true)
    public R saveOrUpdate(@Validated @RequestBody UNoticeVo resources) {
        //resources.setUsers(SecurityUtils.getUserId().toString());
        UNotice uNotice = BeanUtil.copyProperties(resources, UNotice.class);
        uNotice.setTime(new DateTime().toTimestamp());
        uNotice.setUsers(SecurityUtils.getNickName());
        if(UNoticeService.saveOrUpdate(uNotice) && resources.getDeptId()!=null)
        {
            uNoticeDeptService.deleteByUnoticeId(uNotice.getId());
            String tenantId = SecurityUtils.getTenantId();
            List<UNoticeDept> depts = resources.getDeptId().stream().map(s -> {
                UNoticeDept uNoticeDept = new UNoticeDept();
                uNoticeDept.setNoticeId(uNotice.getId());
                uNoticeDept.setDeptId(s);
                uNoticeDept.setTenantId(tenantId);
                return uNoticeDept;
            }).collect(Collectors.toList());

            uNoticeDeptService.saveBatch(depts);
        }
        return R.ok();
    }

    @Log("删除公告")
    @ApiOperation("删除公告，权限码：UNotice:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('UNotice:delete')")
    @Klock
    @CacheEvict(cacheNames = {CacheConstant.UNOTICE,CacheConstant.U_NOTICE_DEPT},allEntries = true)
    public R remove(@RequestBody Set<String> ids) {
        if(UNoticeService.removeByIds(ids))
        {
            uNoticeDeptService.lambdaUpdate().in(UNoticeDept::getNoticeId,ids).remove();
        }
        return R.ok();
    }

    @Log("我的公告")
    @ApiOperation("我的公告")
    @GetMapping("/myNotice")
    @AnonymousAccess
    @CacheEvict(cacheNames = {CacheConstant.UNOTICE,CacheConstant.U_NOTICE_DEPT},allEntries = true)
    public R myNotice(){
        return R.ok(UNoticeService.myNotice());
    }
}
