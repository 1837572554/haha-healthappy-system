package com.healthappy.modules.system.rest.system;

import com.healthappy.annotation.AnonymousAccess;
import com.healthappy.exception.BadRequestException;
import com.healthappy.utils.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.service.ApiListing;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * @Author: wukefei
 * @Date: Created in 2022/4/2 16:53
 * @Description:
 * @Version: 1.0
 *
 */
@Api(tags = "系统：条形码")
@RestController
@RequestMapping("/api/BarCode")
public class BarCodeController {

	@Value("${barCode.path}")
	private String barCodePath;

	/**
	 * 获取 条形码
	 * @param code
	 * @return
	 */
	@ApiOperation("生成条形码")
	@GetMapping(value = "/getBarCode")
	@AnonymousAccess
	public R getBarCode(@RequestParam(required = false) String code){
		if(StringUtils.isEmpty(code)){
			throw new BadRequestException("code不能为空");
		}
		BufferedImage image = BarCodeUtils.getBarCodeAndMsg(code);
//		String codeName=UUID.randomUUID().toString().replace("-","");
		String path = ClassUtils.getDefaultClassLoader().getResource("").getPath();

		String filePath=path+barCodePath+code+".png";
		String port = SpringBeanFactoryUtil.resolve("${server.port}") ;
		// 项目端口
		String url= "http://"+IpUtil.getLocalIP() + ":" + port +"/"+code+".png";
		boolean flag=false;
		try {
			File file = new File(filePath);
			file.getParentFile().mkdirs();
			file.createNewFile();
			flag= ImageIO.write(image, "png", file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(flag){
			System.out.println(filePath);
			return R.ok(url);
		}
		return R.error("条形码生成失败");
	}
}
