package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;

import java.io.Serializable;

@Data
public class PersonalReportSummaryDto implements Serializable {

    @ApiModelProperty("组合项目id")
    private String groupId;

    @ApiModelProperty("组合项目名称")
    private String groupName;

    /**
     * 是否是职检项目 0否1是
     */
    @ApiModelProperty("是否是职检项目")
    private String typeZ;

    /**
     * 是否是普检项目 0否1是
     */
    @ApiModelProperty("是否是普检项目")
    private String typeJ;

    @ApiModelProperty("综述")
    private String comment;

    @ApiModelProperty("建议")
    private String suggest;

    /**
     * 职普合一，职业结论
     */
    @ApiModelProperty("职普合一，职业综述")
    private String commentZy;
    /**
     * 职普合一，职业建议
     */
    @ApiModelProperty("职普合一，职业建议")
    private String suggestZy;

    /**
     * 职普合一，职业结论
     */
    @ApiModelProperty("职普合一，普通综述")
    private String commentPt;
    /**
     * 职普合一，职业建议
     */
    @ApiModelProperty("职普合一，普通建议")
    private String suggestPt;


    @ApiModelProperty("职业结论")
    private String commentZ;

    @ApiModelProperty("职业建议")
    private String suggestZ;

    @ApiModelProperty("禁忌")
    private Integer pjTaboo;

    @ApiModelProperty("疑似")
    private Integer pjSuspicion;

    @ApiModelProperty("其他复查")
    private Integer pjReview;

    @ApiModelProperty("其他疾病或异常")
    private Integer pjUnusual;

    @ApiModelProperty("复查")
    private Integer pjRelate;

    @ApiModelProperty("未见异常")
    private Integer pjRemove;

    @ApiModelProperty("正常/合格")
    private Integer pjQualified;

    @ApiModelProperty("异常/不合格")
    private Integer pjUnqualified;
}
