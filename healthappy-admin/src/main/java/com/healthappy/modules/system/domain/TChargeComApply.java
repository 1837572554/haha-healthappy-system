package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.config.SeedIdGenerator;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @description 单位收费申请表
 * @author sqoy
 * @date 2021-06-24
 */
@Data
@ApiModel("单位收费申请表")
@SeedIdGenerator
@TableName("T_Charge_COM_Apply")
@RenewLog
public class TChargeComApply {

    @TableId(type = IdType.INPUT)
    @ApiModelProperty("收费单号")
    private String id;

    @ApiModelProperty("申请时间")
    @TableField(fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private String applyTime;

    @ApiModelProperty("申请人")
    private String applyDoctor;

    @ApiModelProperty("发票抬头")
    private String invoiceTitle;

    @ApiModelProperty("税号")
    private String taxNo;

    @ApiModelProperty("发票号")
    private String invoiceId;

    @ApiModelProperty("付款人")
    private String payRole;

    @ApiModelProperty("付款人联系电话")
    private String payPhone;

    @ApiModelProperty("备注")
    private  String mark;

    @ApiModelProperty("单位ID")
    private  String companyId;

    @ApiModelProperty("单位名称")
    private String companyName;

    @ApiModelProperty("实收金额")
    private String cost;

    @ApiModelProperty("结账标志")
    private Integer payFlag;

    @ApiModelProperty("结账时间")
    @TableField(fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private String payTime;

    @ApiModelProperty("结账人")
    private String payDoctor;

    @ApiModelProperty("租户ID")
    private String tenantId;

}
