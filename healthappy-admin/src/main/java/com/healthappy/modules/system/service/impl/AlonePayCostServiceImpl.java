package com.healthappy.modules.system.service.impl;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.TCharge;
import com.healthappy.modules.system.domain.TItemHD;
import com.healthappy.modules.system.domain.vo.AloneChargeMakeBillVo;
import com.healthappy.modules.system.domain.vo.AloneChargeVo;
import com.healthappy.modules.system.domain.vo.AloneTicketVo;
import com.healthappy.modules.system.service.AlonePayCostService;
import com.healthappy.modules.system.service.TItemHDService;
import com.healthappy.modules.system.service.dto.AlonePayCostDetailDto;
import com.healthappy.modules.system.service.dto.AlonePayCostDto;
import com.healthappy.modules.system.service.dto.PayCostAloneQueryCriteria;
import com.healthappy.modules.system.service.mapper.AlonePayCostMapper;
import com.healthappy.modules.system.service.mapper.TChargeMapper;
import com.healthappy.utils.RedisUtil;
import com.healthappy.utils.SecurityUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author FGQ
 * @date 2022-02-18
 */
@Service
@RequiredArgsConstructor
public class AlonePayCostServiceImpl implements AlonePayCostService {

	/**
	 * 票据号缓存
	 */
	private final static String TICKET_TENANT_CACHE = "alone:ticket:";
	private final AlonePayCostMapper mapper;
	private final TChargeMapper tChargeMapper;
	private final TItemHDService itemHDService;

	/**
	 * 计算票据号
	 *
	 * @param ticket
	 * @param addSequence
	 * @return
	 */
	public static String calculateTicket(String ticket, List<Integer> addSequence) {
		Integer startSeq = addSequence.get(0);
		Integer endSeq = addSequence.get(1);
		Long countTicket;
		try {
			countTicket = Convert.toLong(ticket);
		} catch (Exception e) {
			throw new BadRequestException("转换失败,请检查票号和种子数");
		}
		if (startSeq > endSeq) {
			throw new BadRequestException("种子数已使用完");
		}
		return StrUtil.fillBefore(Convert.toStr(countTicket + startSeq), '0', ticket.length());
	}

	@Override
	public Map<String, Object> getAll(PayCostAloneQueryCriteria criteria) {
		String tenantId = SecurityUtils.getTenantId();
		List<AlonePayCostDto> list = mapper.getAll(criteria, tenantId);
		Map<String, Object> map = new LinkedHashMap<>();
		map.put("content", list);
		map.put("size", list.size());
		return map;
	}

	@Override
	public AlonePayCostDetailDto detail(String paId) {
		String tenantId = SecurityUtils.getTenantId();
		AlonePayCostDetailDto detailDto = new AlonePayCostDetailDto();
		detailDto.setDetail(mapper.getByPaId(paId, tenantId));
		detailDto.setItemHdList(mapper.getItemHdList(paId, tenantId));
		detailDto.setTChargeList(mapper.getTChargeList(paId, tenantId));
		return detailDto;
	}

	public String addTicket(AloneTicketVo vo, Boolean isRefund) {
		List<Integer> addSequence = vo.getAddSequence();
		int startSeq = addSequence.get(0) + 1;
		if (startSeq > addSequence.get(1)) {
			throw new BadRequestException("增长序列超出");
		}
		AloneTicketVo aloneTicketVo = new AloneTicketVo();
		aloneTicketVo.setType(vo.getType());
		aloneTicketVo.setTicket(vo.getTicket());
		aloneTicketVo.setAddSequence(CollUtil.newArrayList(startSeq, addSequence.get(1)));
		String ticket1 = calculateTicket(vo.getTicket(), vo.getAddSequence());
		if (tChargeMapper.selectCount(
				Wrappers.<TCharge>lambdaQuery().eq(TCharge::getReceipt, ticket1).eq(TCharge::getRefund, isRefund)
						.eq(TCharge::getReceiptType, vo.getType())) > 0) {
			throw new BadRequestException(StrUtil.format("【{}】已经被使用", ticket1));
		}
		RedisUtil.set(buildAloneCacheKey(vo.getType()), aloneTicketVo);
		return ticket1;
	}

	private String buildAloneCacheKey(Integer type) {
		String tenantId = SecurityUtils.getTenantId();
		Long userId = SecurityUtils.getUserId();
		return StrUtil.format(TICKET_TENANT_CACHE + ":{}:{}:{}", type, tenantId, userId);
	}

	@Override
	public AloneTicketVo checkTicket(AloneTicketVo vo) {
		String cacheKey = buildAloneCacheKey(vo.getType());
		if (RedisUtil.hasKey(cacheKey)) {
			AloneTicketVo cacheTicketVo = RedisUtil.get(cacheKey);
			if (StrUtil.isBlank(vo.getTicket()) || StrUtil.isBlank(cacheTicketVo.getTicket()) || Convert.toStr(
					cacheTicketVo.getTicket()).equals(Convert.toStr(vo.getTicket()))) {
				vo = cacheTicketVo;
			}
		}
		return vo;
	}

	private void checkDataIsLegal(AloneChargeVo vo) {
		TCharge tCharge = tChargeMapper.selectOne(
				Wrappers.<TCharge>lambdaQuery().eq(TCharge::getPaId, vo.getPaId()).orderByDesc(TCharge::getId)
						.last("limit 1"));
		if (null == tCharge) {
			return;
		}
		Boolean isRefund = vo.getIsRefund();
		switch (tCharge.getRefund()) {
			case "0":
				if (!isRefund) {
					throw new BadRequestException("该订单已经缴费,请勿重复缴费");
				}
				break;
			case "1":
				if (isRefund) {
					throw new BadRequestException("该订单已经退费,请勿重复退费");
				}
				break;
			default:
				throw new BadRequestException("参数不对");
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public synchronized void charge(AloneChargeVo vo) {
		AlonePayCostDetailDto.Detail detail = mapper.getByPaId(vo.getPaId(), SecurityUtils.getTenantId());
		checkDataIsLegal(vo);
		BigDecimal totalPrice = detail.getTotalPrice();
		//实收
		BigDecimal price = NumberUtil.add(vo.getCard(), vo.getMoney(), vo.getWeiXin(), vo.getZhiFuBao());
		//开始计算各个价格
		TCharge tCharge = new TCharge();
		//已收
		BigDecimal alreadyPrice = new BigDecimal(0);
		//应收
		BigDecimal shouldPrice;
		//未收
		BigDecimal noPrice;
		if (vo.getIsRefund()) {
			tCharge.setCard(detail.getCard());
			tCharge.setMoney(detail.getMoney());
			tCharge.setWeiXin(detail.getWeiXin());
			tCharge.setZhiFuBao(detail.getZhiFuBao());
			shouldPrice = price;
			noPrice = price;
			price = new BigDecimal(0);

		} else {
			if (price.compareTo(totalPrice) > 0) {
				throw new BadRequestException("所有金额超出了总价格");
			} else if (price.compareTo(totalPrice) < 0) {
				throw new BadRequestException("填写的金额未等于总价格");
			}
			alreadyPrice = price;
			tCharge.setCard(vo.getCard());
			tCharge.setMoney(vo.getMoney());
			tCharge.setWeiXin(vo.getWeiXin());
			tCharge.setZhiFuBao(vo.getZhiFuBao());
			shouldPrice = NumberUtil.sub(detail.getTotalPrice(), alreadyPrice);
			noPrice = NumberUtil.sub(shouldPrice, price);
		}
		//开始组装
		tCharge.setActualPrice(price);
		tCharge.setAlreadyPrice(alreadyPrice);
		tCharge.setPaId(vo.getPaId());
		tCharge.setNoPrice(noPrice);
		tCharge.setRefund(vo.getIsRefund() ? "1" : "0");
		tCharge.setShouldPrice(shouldPrice);
		tCharge.setReceiptType(vo.getReceiptType());
		if (vo.getIsRefund()) {
			tCharge.setReceipt(detail.getReceipt());
			tCharge.setRefundTime(DateTime.now().toTimestamp());
			tCharge.setRefundDoctor(SecurityUtils.getNickName());
		} else {
			//新增票据号
			AloneTicketVo ticketVo = new AloneTicketVo();
			ticketVo.setAddSequence(vo.getAddSequence());
			ticketVo.setTicket(vo.getReceipt());
			ticketVo.setType(vo.getReceiptType() ? 1 : 0);
			AloneTicketVo checkTicket = checkTicket(ticketVo);
			tCharge.setReceipt(this.addTicket(checkTicket, vo.getIsRefund()));
			tCharge.setChargeDoctor(SecurityUtils.getNickName());
			tCharge.setChargeTime(DateTime.now().toTimestamp());
		}
		int res = tChargeMapper.insert(tCharge);
		if (res > 0) {
			//开始修改 【项目】 中缴费
			updItemHdTicketByPaId(vo.getPaId(), tCharge.getReceipt(), vo.getIsRefund());
		}
	}

	@Override
	public AloneTicketVo addCurrentTicket(AloneTicketVo vo) {
		RedisUtil.set(buildAloneCacheKey(vo.getType()), vo);
		return vo;
	}

	@Override
	public void makeBill(AloneChargeMakeBillVo vo) {
		//判断是否有有效的收据号
		TCharge tCharge = tChargeMapper.getLatestValidData(vo.getPaId(), vo.getReceiptType(),
				SecurityUtils.getTenantId());
		if (!Optional.ofNullable(tCharge).isPresent()) {
			throw new BadRequestException("当前人员无有效记录");
		}
		if (tCharge.getReceipt().equals(vo.getReceipt())) {
			throw new BadRequestException("票据号与有效数据冲突");
		}

		//创建一条退费记录
		TCharge refund = BeanUtil.copyProperties(tCharge, TCharge.class);
		//清空已收
		refund.setAlreadyPrice(BigDecimal.valueOf(0));
		//设置应收
		refund.setShouldPrice(tCharge.getShouldPrice());
		//清空实收
		refund.setActualPrice(BigDecimal.valueOf(0));
		//设置未收
		refund.setNoPrice(NumberUtil.mul(-1, tCharge.getNoPrice()));
		//清除付费医生，时间,设置退费时间
		refund.setChargeDoctor(null);
		refund.setChargeTime(null);
		refund.setRefund("1");
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		refund.setRefundTime(timestamp);
		refund.setRefundDoctor(tCharge.getChargeDoctor());
		refund.setCreateTime(timestamp);
		tChargeMapper.insert(refund);

		//修改票据数据
		AloneTicketVo ticketVo = new AloneTicketVo();
		ticketVo.setAddSequence(vo.getAddSequence());
		ticketVo.setTicket(vo.getReceipt());
		ticketVo.setType("1".equals(vo.getReceiptType()) ? 1 : 0);
		AloneTicketVo checkTicket = checkTicket(ticketVo);
		tCharge.setReceipt(this.addTicket(checkTicket, true));

		//修改新记录
		tCharge.setCreateTime(timestamp);
		tCharge.setChargeDoctor(SecurityUtils.getNickName());
		tCharge.setChargeTime(timestamp);
		//        tCharge.setReceipt(vo.getReceipt());
		tChargeMapper.insert(tCharge);
	}

	private void updItemHdTicketByPaId(String paId, String ticket, Boolean chargeReturn) {
		itemHDService.lambdaUpdate().eq(TItemHD::getPaId, paId)
				.set(TItemHD::getChargeDate, chargeReturn ? null : DateTime.now().toTimestamp())
				.set(TItemHD::getHisNo, chargeReturn ? null : ticket).set(TItemHD::getChargeReturn, chargeReturn)
				.update();
	}
}
