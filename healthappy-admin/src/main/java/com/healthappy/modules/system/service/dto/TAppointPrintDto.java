package com.healthappy.modules.system.service.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA. Created by yutang 2021/9/10 0010  13:42 Description:预约管理打印对象
 */
@Data
public class TAppointPrintDto  implements Serializable {

  /**
   * 医院名称
   */
  private String hospitalName;

  /**
   * 姓名
   */
  private String name;

  /**
   * 性别
   */
  private String sex;

  /**
   * 婚否
   */
  private String marry;

  /**
   * 联系电话
   */
  private String mobile;

  /**
   * 工作单位
   */
  private String companyName;

  /**
   * 身份证号
   */
  private String idNo;

  /**
   * 体检类别
   */
  private String type;

  /**
   * 工龄
   */
  private String workYears;

  /**
   * 总工龄
   */
  private String totalYears;

  /**
   * 毒害种类
   */
  private String poisonType;

}
