package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class BallTransitionDto  implements Serializable {

    @ApiModelProperty("登记ID")
    private String paId;
}
