package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BSicknessAdvice;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 病种建议表
 * @author fang
 * @date 2021-06-17
 */
@Mapper
@Repository
public interface BSicknessAdviceMapper extends CoreMapper<BSicknessAdvice> {

}
