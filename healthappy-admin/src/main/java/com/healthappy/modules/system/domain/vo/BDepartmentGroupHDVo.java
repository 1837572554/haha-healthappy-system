package com.healthappy.modules.system.domain.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class BDepartmentGroupHDVo {

    /**
     * 分组id
     */
    private String id;

    /**
     * 分组名称
     */
    @NotNull(message = "分组名称不能为空")
    private String name;

    /**
     * 单位id
     */
    @NotNull(message = "单位id不能为空")
    private String companyId;

    /**
     * 部门Id
     */
    private String departmentId;

    /**
     * 统收限额
     */
    private String tsxe;

    /**
     * 收费方式
     */
    private String sffs;

    /**
     * 绑定的套餐名
     */
    private String packageName;

    /**
     * 允许性别
     */
    private String sex;

    /**
     * 年龄低值
     */
    private Integer ageLow;

    /**
     * 年龄高值
     */
    private Integer ageHigh;

    /**
     * 婚否 0否 1是
     */
    private String marital;
    /**
     * 套餐id
     */
    private Long packageId;
    /**
     * 是否是项目追加 0否 1是
     */
    private String isUsed;
    /**
     * 是否启用 0否 1是
     */
    private String isEnable;
    /**
     * 毒害
     */
    private String poison;
    /**
     * 折扣后总价格
     */
    private Double cost;
    /**
     * 实际价格
     */
    private Double priceReal;

    /**
     * 原价
     */
    private Double price;
    /**
     * 服务费
     */
    private Double serviceFee;
    /**
     * 折扣
     */
    private Double discount;



}
