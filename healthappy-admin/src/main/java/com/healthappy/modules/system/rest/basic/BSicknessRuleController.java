package com.healthappy.modules.system.rest.basic;


import cn.hutool.core.bean.BeanUtil;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BSicknessRule;
import com.healthappy.modules.system.domain.vo.BSicknessRuleVo;
import com.healthappy.modules.system.service.BSicknessRuleService;
import com.healthappy.modules.system.service.dto.BItemTreeQueryCriteria;
import com.healthappy.modules.system.service.dto.BSicknessRuleQueryCriteria;
import com.healthappy.modules.system.service.dto.SearchSicknessRuleCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 病种规则设置 控制器
 * @author FGQ
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "基础管理：病种规则设置")
@ApiSort(5)
@RestController
@AllArgsConstructor
@RequestMapping("/BSicknessRule")
public class BSicknessRuleController {

    private final BSicknessRuleService bSicknessRuleService;

    @Log("查询病种规则 tree")
    @ApiOperation("查询病种规则 tree，权限码：BSicknessRule:list")
    @GetMapping("/tree")
    public R tree(BItemTreeQueryCriteria criteria) {
        return R.ok(bSicknessRuleService.tree(criteria));
    }

    @Log("查询病种规则 search")
    @ApiOperation("查询病种规则 tree，权限码：BSicknessRule:list")
    @GetMapping("/search")
    public R search(SearchSicknessRuleCriteria criteria) {
        return R.ok(bSicknessRuleService.search(criteria));
    }

    @Log("查询病种规则")
    @ApiOperation("查询病种规则，权限码：BSicknessRule:list")
    @GetMapping("/page")
    public R page(BSicknessRuleQueryCriteria criteria,Pageable pageable) {
        return R.ok(bSicknessRuleService.queryAll(criteria,pageable));
    }

    @Log("查询所有病种规则")
    @ApiOperation("查询所有病种规则，权限码：BSicknessRule:list")
    @GetMapping
    public R list(BSicknessRuleQueryCriteria criteria) {
        return R.ok(bSicknessRuleService.queryAlls(criteria));
    }

    @Log("新增|修改病种规则")
    @ApiOperation("新增|修改病种规则，权限码：BSicknessRule:add")
    @PostMapping
    @PreAuthorize("@el.check('BSicknessRule:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BSicknessRuleVo resources) {
        BSicknessRule bSicknessRule = new BSicknessRule();
        //用于将对象属性拷贝到另一个对象（类型，名字一样即可）
        //第一个是数据源，第二个是赋值对象
        BeanUtil.copyProperties(resources,bSicknessRule);
        bSicknessRuleService.saveOrUpdate(bSicknessRule);
        return R.ok();
    }

    @Log("删除病种规则")
    @ApiOperation("删除病种规则，权限码：BSicknessRule:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('BSicknessRule:delete')")
    @Klock
    public R remove(@RequestBody Set<String> ids) {
        bSicknessRuleService.removeByIds(ids);
        return R.ok();
    }
}
