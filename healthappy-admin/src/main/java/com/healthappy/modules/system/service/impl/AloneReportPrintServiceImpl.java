package com.healthappy.modules.system.service.impl;

import com.healthappy.modules.system.service.AloneReportPrintService;
import com.healthappy.modules.system.service.dto.AloneReportPrintCriteria;
import com.healthappy.modules.system.service.mapper.TItemHDMapper;
import com.healthappy.modules.system.service.mapper.TPatientMapper;
import com.healthappy.utils.SecurityUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.Map;

@AllArgsConstructor
@Service
public class AloneReportPrintServiceImpl implements AloneReportPrintService {
    private final TPatientMapper tPatientMapper;
    private final TItemHDMapper tItemHDMapper;

    @Override
    public Map<String, Object> getList(AloneReportPrintCriteria criteria) {
        //总条数
//        Integer totalCount = tPatientMapper.getAloneReportPrintCount(criteria);
//        Integer[] pageBase = PageUtil.toStartEnd(criteria.getPageNum(), criteria.getPageSize(), totalCount);
//        criteria.setStartIndex(pageBase[0]);
        Map<String, Object> map = new LinkedHashMap<>(5);
        map.put("content", tPatientMapper.getAloneReportPrintList(criteria));
//        map.put("totalElements", totalCount);
//        map.put("totalPage", pageBase[3]);
//        map.put("currentPage", pageBase[2]);
//        map.put("pageSize", criteria.getPageSize());
        return map;
    }

    @Override
    public Map<String, Object> getGroupList(String PaId, String deptId, String groupId) {
        Map<String, Object> map = new LinkedHashMap<>(5);
        map.put("content", tItemHDMapper.getAloneReportPrintGroup(PaId, deptId, groupId, SecurityUtils.getTenantId()));
        return map;
    }

    @Override
    public Map<String, Object> getItemList(String PaId, String GroupId) {
        Map<String, Object> map = new LinkedHashMap<>(5);
        map.put("content", tItemHDMapper.getAloneReportPrintItem(PaId, GroupId, SecurityUtils.getTenantId()));
        return map;
    }

}
