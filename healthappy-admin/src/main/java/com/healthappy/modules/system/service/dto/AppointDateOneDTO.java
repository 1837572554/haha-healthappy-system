package com.healthappy.modules.system.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 单独预约日期对象
 * @author Jevany
 * @date 2022/5/9 11:39
 */
@ApiModel("单独预约日期对象")
@Data
public class AppointDateOneDTO implements Serializable {

	/**
	 * 预约日期编号
	 */
	@ApiModelProperty(value = "预约日期编号,新增为空")
	private String id;

	/**
	 * 预约日期
	 */
	@ApiModelProperty(value = "预约日期")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@NotNull(message = "预约日期不能为空")
	private String date;


	/** 预约详细数据 */
	@Valid
	@ApiModelProperty("预约详细数据")
	@NotEmpty(message = "预约详细数据不能为空")
	private List<BAppointDateNumDTO> appointDateNumDTOList;

	private static final long serialVersionUID = 1L;
}