package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TWz;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 问诊表
 * @author sjc
 * @date 2021-12-15
 */
@Mapper
@Repository
public interface TWzMapper extends CoreMapper<TWz> {

}
