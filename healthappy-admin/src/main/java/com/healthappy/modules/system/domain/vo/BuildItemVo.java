package com.healthappy.modules.system.domain.vo;

import com.healthappy.modules.system.domain.BItem;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class BuildItemVo {

    private String paId;

    private String packageId;

    private String packageName;

    private String groupId;

    private String deptId;

    private String groupName;

    private BigDecimal price;

    private BigDecimal discount;

    private BigDecimal cost;

    private String deptGroupId;

}
