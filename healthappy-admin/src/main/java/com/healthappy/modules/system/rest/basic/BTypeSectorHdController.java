package com.healthappy.modules.system.rest.basic;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BTypeSectorHd;
import com.healthappy.modules.system.domain.vo.BTypeSectorHdVo;
import com.healthappy.modules.system.service.BTypeSectorHdService;
import com.healthappy.modules.system.service.dto.BTypeSectorHdQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 行业类别设置设置 控制器
 * @author fang
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "基础管理：行业类别设置")
@RestController
@AllArgsConstructor
@RequestMapping("/BTypeSectorHd")
public class BTypeSectorHdController {

    private final BTypeSectorHdService bTypeSectorHdService;

    @Log("查询行业类别设置")
    @ApiOperation("查询行业类别设置，权限码：BTypeSectorHd:list")
    @GetMapping
    public R list(BTypeSectorHdQueryCriteria criteria, Pageable pageable) {
        return R.ok(bTypeSectorHdService.queryAll(criteria, pageable));
    }

    @Log("新增|修改行业类别设置")
    @ApiOperation("新增|修改行业类别设置，权限码：BTypeSectorHd:add")
    @PostMapping
    @PreAuthorize("@el.check('BTypeSectorHd:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BTypeSectorHdVo resources) {
        if(checkNameRepeat(resources)){
            return R.error("行业类别名称不能重复");
        }
        return R.ok(bTypeSectorHdService.saveOrUpdate(BeanUtil.copyProperties(resources, BTypeSectorHd.class)));
    }

    @Log("删除行业类别设置")
    @ApiOperation("删除行业类别设置，权限码：BTypeSectorHd:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('BTypeSectorHd:delete')")
    @Klock
    public R remove(@RequestBody Set<Integer> ids) {
        return R.ok(bTypeSectorHdService.removeByIds(ids));
    }

    private boolean checkNameRepeat(BTypeSectorHdVo resources) {
        return  bTypeSectorHdService.lambdaQuery().eq(BTypeSectorHd::getName, resources.getName())
                .ne(ObjectUtil.isNotNull(resources.getId()),BTypeSectorHd::getId,resources.getId()).count() > 0;
    }
}
