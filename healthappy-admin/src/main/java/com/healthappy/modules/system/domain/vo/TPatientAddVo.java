package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import java.util.List;

/**
 * @author yanjun
 * @description
 * @date 2021/9/15
 */
@Data
public class TPatientAddVo {

    /** 体检信息 */
    @ApiModelProperty("体检信息")
    private  TPatientVo resources;
    
    /** 预约编号 */
    @ApiModelProperty("预约编号")
    private String appointId;

    @Valid
    @ApiModelProperty("组合项目List")
    private List<TItemHDVo> items;
}
