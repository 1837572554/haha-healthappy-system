package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BTypeSectorDtQueryCriteria {

    @Query(type = Query.Type.INNER_LIKE)
    private String name;


    /**
     * 行业id
     */
    @Query
    @ApiModelProperty("行业类别ID")
    private Long typeHdId;
}
