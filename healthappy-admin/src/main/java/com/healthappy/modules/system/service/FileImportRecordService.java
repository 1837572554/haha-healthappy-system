package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.FileImportRecord;
import com.healthappy.modules.system.domain.vo.FileImportRecordVO;
import com.healthappy.modules.system.service.dto.FileImportRecoredQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 上传文件记录
 * @author FGQ
 * @date 2021-06-17
 */
public interface FileImportRecordService extends BaseService<FileImportRecord> {


	void submitExecl(String filePath, String fileName);

	Map<String, Object> queryAll(FileImportRecoredQueryCriteria criteria, Pageable pageable);

	List<FileImportRecordVO> queryAll(FileImportRecoredQueryCriteria criteria);

	String checkFileName(String fileName);

	/**
	 * 保存导入记录
	 * @author YJ
	 * @date 2022/5/11 19:17
	 * @param fileName 文件名
	 * @param fileType 1预约  0登记
	 */
	void saveImport(String fileName, String fileType);
}
