package com.healthappy.modules.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 预约号源对象
 * @author Jevany
 * @date 2022/5/7 0:22
 */
@ApiModel("预约号源对象")
@Data
public class AppointDateVO implements Serializable {

	/**
	 * 预约日期编号
	 */
	@ApiModelProperty(value = "预约日期编号")
	private String id;

	/**
	 * 预约日期
	 */
	@ApiModelProperty(value = "预约日期")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	private Date date;

	/** 总数 */
	@ApiModelProperty("总数")
	private Integer totalNum;

	/** 已预约数 */
	@ApiModelProperty("已预约数")
	private Integer appointNum;

	/** 预约详细数据 */
	@ApiModelProperty("预约详细数据")
	private List<AppointDateNumVO> appointDateNumVOList;

	private static final long serialVersionUID = 1L;
}
