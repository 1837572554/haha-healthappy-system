package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.UsersDepts;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * @author yanjun
 * @description
 * @date 2021/11/3
 */
@Repository
@Mapper
public interface UsersDeptsMapper extends CoreMapper<UsersDepts> {

    @SqlParser(filter = true)
    @Select("SELECT user_id,dept_id FROM `users_depts` where user_id= #{id}")
    Set<UsersDepts> findByUserId(@Param("id") Long id);
}
