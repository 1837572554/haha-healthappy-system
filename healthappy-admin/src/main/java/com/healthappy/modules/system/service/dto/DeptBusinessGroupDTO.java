package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

/**
 * 科室业务量统计-项目
 * @author Jevany
 * @date 2022/3/15 16:26
 */
@Data
@ApiModel("科室业务量统计-项目")
public class DeptBusinessGroupDTO implements Serializable {
    /** 项目名称 */
    private String groupName;
    /** 预约人数 */
    private Integer appointNum;
    /** 签到人数 */
    private Integer signNum;
}