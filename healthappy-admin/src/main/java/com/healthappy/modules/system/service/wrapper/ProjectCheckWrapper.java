package com.healthappy.modules.system.service.wrapper;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.healthappy.modules.system.domain.*;
import com.healthappy.modules.system.service.*;
import com.healthappy.modules.system.service.dto.ProjectCheckBItemHdDto;
import com.healthappy.modules.system.service.dto.ProjectCheckDto;
import com.healthappy.utils.SpringUtil;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 包装类,返回视图层所需的字段
 *
 * @author FGQ
 */
public class ProjectCheckWrapper extends BaseEntityWrapper<TPatient, ProjectCheckDto>{

    private final static BPeTypeHDService bPeTypeHDService;
    private final static BPeTypeDTService bPeTypeDtService;
    private final static TPatientZService tPatientZService;
    private final static BPoisonService bPoisonService;
    private final static TItemHDService itemHDService;
    private final static BNationService nationService;
    private final static TPhotoService photoService;
    private final static BCompanyService companyService;
    private final static BDepartmentService departmentService;
    private final static BDepartmentGroupHDService departmentGroupHDService;

    static {
        bPeTypeHDService = SpringUtil.getBean(BPeTypeHDService.class);
        bPeTypeDtService = SpringUtil.getBean(BPeTypeDTService.class);
        tPatientZService = SpringUtil.getBean(TPatientZService.class);
        bPoisonService = SpringUtil.getBean(BPoisonService.class);
        itemHDService = SpringUtil.getBean(TItemHDService.class);
        nationService = SpringUtil.getBean(BNationService.class);
        photoService = SpringUtil.getBean(TPhotoService.class);
        companyService = SpringUtil.getBean(BCompanyService.class);
        departmentService = SpringUtil.getBean(BDepartmentService.class);
        departmentGroupHDService = SpringUtil.getBean(BDepartmentGroupHDService.class);
    }

    public static ProjectCheckWrapper build(){
        return new ProjectCheckWrapper();
    }

    @Override
    public ProjectCheckDto entityVO(TPatient entity) {
        ProjectCheckDto projectCheckDto = BeanUtil.copyProperties(entity, ProjectCheckDto.class);
        if(Optional.ofNullable(projectCheckDto).isPresent()){
            if(ObjectUtil.isNotNull(projectCheckDto.getPeTypeHdId())){
                projectCheckDto.setPeTypeHdName(Optional.ofNullable(bPeTypeHDService.getById(projectCheckDto.getPeTypeHdId())).map(BPeTypeHD::getName).orElse(""));
            }
            if(ObjectUtil.isNotNull(projectCheckDto.getPeTypeDtId())){
                 projectCheckDto.setPeTypeDtName(Optional.ofNullable(bPeTypeDtService.getById(projectCheckDto.getPeTypeDtId())).map(BPeTypeDT::getName).orElse(""));
            }
            TPatientZ tPatientZ = tPatientZService.getByPaId(projectCheckDto.getId());
            if(Optional.ofNullable(tPatientZ).isPresent()){
                projectCheckDto.setWorkYears(Optional.of(tPatientZ).map(TPatientZ::getWorkYears).orElse(""));
                if(StrUtil.isNotBlank(tPatientZ.getPoisonType())){
                    projectCheckDto.setPoisonTypeName(bPoisonService.lambdaQuery().in(BPoison::getId, Stream.of(tPatientZ.getPoisonType().split("、")).collect(Collectors.toList())).select(BPoison::getPoisonName).list().stream().map(BPoison::getPoisonName).collect(Collectors.toList()));
                }
            }
            //人员查询时携带组合项目
            if(projectCheckDto.getCarryItemHd().equals(true)) {
                List<TItemHD> itemHDList = itemHDService.lambdaQuery().eq(TItemHD::getPaId, projectCheckDto.getId()).list();
                projectCheckDto.setItemHdDtoList(
                        CollUtil.isEmpty(itemHDList) ? Collections.emptyList() :
                                itemHDList.stream().map(hd -> BeanUtil.copyProperties(hd, ProjectCheckBItemHdDto.class)).collect(Collectors.toList())
                );
            }
            //设置民族
            if(StrUtil.isNotBlank(projectCheckDto.getNation())){
                projectCheckDto.setNationName(Optional.ofNullable(nationService.lambdaQuery().eq(BNation::getCode,projectCheckDto.getNation()).select(BNation::getName).one()).map(BNation::getName).orElse(""));
            }

            if(ObjectUtil.isNotNull(projectCheckDto.getCompanyId())){
                projectCheckDto.setCompanyName(Optional.ofNullable(companyService.lambdaQuery().eq(BCompany::getId,projectCheckDto.getCompanyId()).select(BCompany::getName).one()).map(BCompany::getName).orElse(""));
            }
            if(ObjectUtil.isNotNull(projectCheckDto.getDepartmentId())){
                projectCheckDto.setDepartmentName(Optional.ofNullable(departmentService.lambdaQuery().eq(BDepartment::getId,projectCheckDto.getDepartmentId()).select(BDepartment::getName).one()).map(BDepartment::getName).orElse(""));
            }
            if(ObjectUtil.isNotNull(projectCheckDto.getDepartmentGroupId())){
                projectCheckDto.setDepartmentGroupName(Optional.ofNullable(departmentGroupHDService.lambdaQuery().eq(BDepartmentGroupHD::getId,projectCheckDto.getDepartmentGroupId()).select(BDepartmentGroupHD::getName).one()).map(BDepartmentGroupHD::getName).orElse(""));
            }
        }
        return projectCheckDto;
    }
}
