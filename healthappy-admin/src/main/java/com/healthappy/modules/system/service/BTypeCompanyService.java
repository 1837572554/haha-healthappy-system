package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BTypeCompany;
import com.healthappy.modules.system.domain.BTypeSample;
import com.healthappy.modules.system.service.dto.BTypeCompanyQueryCriteria;
import com.healthappy.modules.system.service.dto.BTypeSampleQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 企业经济类型
 * @author sjc
 * @date 2021-08-30
 */
public interface BTypeCompanyService extends BaseService<BTypeCompany> {

    Map<String, Object> queryAll(BTypeCompanyQueryCriteria criteria, Pageable pageable);

    List<BTypeCompany> queryAll(BTypeCompanyQueryCriteria criteria);
}
