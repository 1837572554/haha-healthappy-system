package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.Tag;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @description 标识
 * @author FGQ
 * @date 2021-06-17
 */
@Mapper
@Repository
public interface TagMapper extends CoreMapper<Tag> {


    IPage<Tag> customSql(IPage<Tag> page,@Param("name") String name);
}
