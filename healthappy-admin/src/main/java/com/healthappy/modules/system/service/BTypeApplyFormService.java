package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BTypeApplyForm;
import com.healthappy.modules.system.service.dto.BTypeApplyFormQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 申请单类型 服务层
 * @author sjc
 * @date 2021-08-3
 */
public interface BTypeApplyFormService extends BaseService<BTypeApplyForm> {


    Map<String, Object> queryAll(BTypeApplyFormQueryCriteria criteria, Pageable pageable);

    List<BTypeApplyForm> queryAll(BTypeApplyFormQueryCriteria criteria);

    boolean deleteById(Long id);
}
