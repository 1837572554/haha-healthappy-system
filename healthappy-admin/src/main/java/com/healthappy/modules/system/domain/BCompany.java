package com.healthappy.modules.system.domain;


import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.config.SeedIdGenerator;
import com.healthappy.config.databind.FieldBind;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @description 单位表
 * @author sjc
 * @date 2021-06-24
 */
@Data
@ApiModel("单位表")
@SeedIdGenerator
@TableName("B_Company")
public class BCompany implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id 7位
     */
    @ApiModelProperty("主键id")
    @TableId(type = IdType.INPUT)
    private String id;


    /**
     * 单位名称
     */
    @ApiModelProperty("单位名称")
    private String name;

    /**
     * 单位代码
     */
    @ApiModelProperty("单位代码")
    private String code;

    /**
     * 单位电话
     */
    @ApiModelProperty("单位电话")
    private String companyTel;

    /**
     * 邮编
     */
    @ApiModelProperty("邮编")
    private String postcode;


    /**
     * 经济类型(对应表b_type_company)
     */
    @ApiModelProperty("经济类型")
    @FieldBind(type = "typeCompany" , target = "equityTypeText")
    private String equityType;

    @TableField(exist = false)
    private String equityTypeText;

    /**
     * 企业规模
     */
    @ApiModelProperty("企业规模")
    @FieldBind(type = "companyScale" , target = "scaleText")
    private String scale;

    @TableField(exist = false)
    private String scaleText;

    /**
     * 从事行业
     */
    @ApiModelProperty("从事行业")
    private String trade;
    /**
     * 总工人数
     */
    @ApiModelProperty("总工人数")
    private String totalCount;
    /**
     * 总生产工人数
     */
    @ApiModelProperty("总生产工人数")
    private String workerCount;
    /**
     * 接害工人数
     */
    @ApiModelProperty("接害工人数")
    private String poisonCount;
    /**
     * 总人数（女）
     */
    @ApiModelProperty("总人数（女）")
    private String totalCountF;
    /**
     * 生产工人总数（女）
     */
    @ApiModelProperty("生产工人总数（女）")
    private String workerCountF;
    /**
     * 接触有害因数工人总数（女）
     */
    @ApiModelProperty("接触有害因数工人总数（女）")
    private String poisonCountF;
    /**
     * 联系人
     */
    @ApiModelProperty("联系人")
    private String linkman;
    /**
     * 电话
     */
    @ApiModelProperty("电话")
    private String phone;
    /**
     * 单位邮箱
     */
    @ApiModelProperty("单位邮箱")
    private String email;
    /**
     * 单位qq号
     */
    @ApiModelProperty("单位qq号")
    private String qq;

    /**
     * 地址
     */
    @ApiModelProperty("地址")
    private String address;

    /**
     * 拼音简码
     */
    @ApiModelProperty("拼音简码")
    private String pyCode;
    /**
     * 检索编号
     */
    @ApiModelProperty("检索编号")
    private String searchCode;
    /**
     * 是否启用  0不是  1是
     */
    @ApiModelProperty("是否启用  0不是  1是")
    private String isEnable;
    /**
     * 排序
     */
    @ApiModelProperty("排序")
    private String dispOrder;
    /**
     * 集团公司id
     */
    @ApiModelProperty("集团公司id")
    private Long parentId;

    /**
     * 区域
     */
    @ApiModelProperty("区域")
    private String area;

    /**
     * 集团名称
     */
	@TableField(updateStrategy = FieldStrategy.IGNORED)
    @ApiModelProperty("集团名称")
    private String blocName;

    /**
     * 单位简称
     */
    @ApiModelProperty("单位简称")
    private String logogram;
    /**
     * 主要职业病危害因素
     */
    @ApiModelProperty("主要职业病危害因素")
    private String careerHazard;
    /**
     * 集团id
     */
    @ApiModelProperty("集团id")
    private String groupCommpanyId;
    /**
     * 公司简称
     */
    @ApiModelProperty("公司简称")
    private String companyJx;
    /**
     * 最后更新时间
     */
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @TableField(fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty("最后更新时间")
    private Timestamp updateTime;
    /**
     * 医疗机构ID(U_Hospital_Info的ID)
     */
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;


    @ApiModelProperty("男性职工人数")
    private Integer workerMenNum;

    @ApiModelProperty("生产职工人数")
    private Integer prodMenNum;

    @ApiModelProperty("有害因素人数")
    private Integer harmfulMenNum;
}
