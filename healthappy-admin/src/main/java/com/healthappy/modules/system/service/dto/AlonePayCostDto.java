package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author FGQ
 * @date 2022-02-18
 */
@Data
public class AlonePayCostDto implements Serializable {

    @ApiModelProperty("流水号")
    private String id;

    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("公司")
    private String company;
}
