package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.healthappy.config.SeedIdGenerator;
import com.healthappy.config.databind.FieldBind;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 问诊模板表
 * @author fang
 * @date 2021-06-21
 */
@Data
@SeedIdGenerator
@TableName("B_Comment_WZ")
public class BCommentWz implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.INPUT)
    /**
     * id
     */
    private String id;

    /**
     * 结论
     */
    @ApiModelProperty("结论")
    private String comment;

    /**
     * 排序号
     */
    @ApiModelProperty("排序号")
    private String dispOrder;

    /**
     * 问诊模板类型  0既往病史 1现病史 2家族史 3手术史
     */
    @ApiModelProperty("问诊模板类型  0既往病史 1现病史 2家族史 3手术史")
    @FieldBind(type = "commentWzType" , target = "typeText")
    private String type;

    @TableField(exist = false)
    private String typeText;

    /**
     * 医疗机构id(u_hospital_info的id)
     */
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;

    /** 编号 */
    @ApiModelProperty("编号")
    private String code;
}
