package com.healthappy.modules.system.service.dto;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Author: YuTang
 * @Date: Created in 2022/3/30 15:32
 * @Description:
 * @Version: 1.0
 */
@Data
public class ApplyWujiaExportDTO {

    @ExcelProperty("物价id")
    private String wujiaId;

    @ExcelProperty("物价名称")
    private String wujiaName;

    @ExcelProperty("规格")
    private String pkgName;

    @ExcelProperty("单价")
    private BigDecimal price;

    @ExcelProperty("数量")
    private BigDecimal count;

    @ExcelProperty("额度")
    private BigDecimal discountValue;

    @ExcelProperty("合计")
    private BigDecimal totalPrice;
}
