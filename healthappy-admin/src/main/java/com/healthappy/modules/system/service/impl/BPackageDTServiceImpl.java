package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BGroupHd;
import com.healthappy.modules.system.domain.BPackageDT;
import com.healthappy.modules.system.service.BGroupHdService;
import com.healthappy.modules.system.service.BPackageDTService;
import com.healthappy.modules.system.service.dto.BPackageDTDto;
import com.healthappy.modules.system.service.dto.BPackageDTQueryCriteria;
import com.healthappy.modules.system.service.mapper.BPackageDTMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 套餐项目表服务层实现层
 *
 * @author sjc
 * @date 2021-07-2
 */
@Service
@AllArgsConstructor//有自动注入的功能
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BPackageDTServiceImpl extends BaseServiceImpl<BPackageDTMapper, BPackageDT> implements BPackageDTService {

    private final BGroupHdService bGroupHdService;
    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BPackageDTQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageHelper.orderBy("CAST(IFNULL(bd.disp_order,999) as signed),CAST(IFNULL(bgh.disp_order,999) as signed)");
        PageInfo<BPackageDT> page = new PageInfo<BPackageDT>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("content", loadingGroupName(generator.convert(page.getList(), BPackageDTDto.class)));
        map.put("totalElements", page.getTotal());

        return map;
    }

    @Override
    public List<BPackageDT> queryAll(BPackageDTQueryCriteria criteria) {
        return baseMapper.getGroupDispOrderByCriteria(criteria);
    }

    private List<BPackageDTDto> loadingGroupName(List<BPackageDTDto> criteria) {
        if (CollUtil.isEmpty(criteria)) {
            return Collections.emptyList();
        }
        List<String> permissions = criteria.stream()
                .map(BPackageDTDto::getGroupId)//指定字段
                .collect(Collectors.toList());//将集合内的指定字段内容形成一个集合返回

        LambdaUpdateWrapper<BGroupHd> updateLam = new LambdaUpdateWrapper();
        updateLam.in(BGroupHd::getId, permissions);
        List<BGroupHd> hdList = bGroupHdService.list(updateLam);

        for (BGroupHd bGroupHd : hdList) {
            for (BPackageDTDto td : criteria) {
                if (td.getGroupId().equals(bGroupHd.getId())) {
                    td.setGroupName(bGroupHd.getName());
                    td.setSex(bGroupHd.getSex());
                }
            }
        }
        return criteria;
    }

    /**
     * 根据套餐编号获取下面的组合项目
     * 通过科室显示序号，组合项目显示序号排序
     *
     * @param packageId 套餐编号
     * @return java.util.List〈com.healthappy.modules.system.domain.BPackageDT〉
     * @author YJ
     * @date 2021/12/17 10:14
     */
    @Override
    public List<BPackageDT> getGroupDispOrder(String packageId) {
        return baseMapper.getGroupDispOrder(packageId);
    }
}
