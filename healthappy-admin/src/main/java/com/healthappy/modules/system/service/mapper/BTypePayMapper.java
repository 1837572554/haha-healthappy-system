package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BTypePay;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * Created with IntelliJ IDEA. Created by yutang 2021/9/2 0002  14:42 Description:
 */
@Mapper
@Repository
public interface BTypePayMapper extends CoreMapper<BTypePay> {

}
