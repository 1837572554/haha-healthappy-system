package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class BPeTypeJobVo {

    /**
     * id
     */
    private Long id;

    /**
     * 名称
     */
    @NotBlank(message = "名称不能为空")
    @ApiModelProperty(value = "名称",required = true)
    private String name;

    /**
     * 排序
     */
    @ApiModelProperty("排序")
    private Integer dispOrder;

    /**
     * 编码
     */
    @ApiModelProperty(value = "编码",required = true)
    private String code;

    /**
     * 类型(1代表职业；2代表从业)
     */
    @NotBlank(message = "类型不能为空")
    @ApiModelProperty(value = "类型(1代表职业；2代表从业)",required = true)
    private String type;
}
