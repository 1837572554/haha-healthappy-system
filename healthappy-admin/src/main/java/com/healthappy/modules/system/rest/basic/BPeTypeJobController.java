package com.healthappy.modules.system.rest.basic;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BPeTypeJob;
import com.healthappy.modules.system.domain.vo.BPeTypeJobVo;
import com.healthappy.modules.system.service.BPeTypeJobService;
import com.healthappy.modules.system.service.dto.BPeTypeJobQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 工种设置 控制器
 * @author FGQ
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "基础管理：工种设置")
@RestController
@AllArgsConstructor
@RequestMapping("/BPeTypeJob")
public class BPeTypeJobController {

    private final BPeTypeJobService bPeTypeJobService;

    @Log("分页查询工种")
    @ApiOperation("分页查询工种，权限码：BPeTypeJob:list")
    @GetMapping
    public R list(BPeTypeJobQueryCriteria criteria, Pageable pageable) {
        return R.ok(bPeTypeJobService.queryAll(criteria, pageable));
    }
    @Log("查询工种全部信息")
    @ApiOperation("查询工种全部信息，权限码：BPeTypeJob:list")
    @GetMapping("/list")
    public R list(BPeTypeJobQueryCriteria criteria) {
        return R.ok(bPeTypeJobService.queryAll(criteria));
    }

    @Log("新增|修改工种")
    @ApiOperation("新增|修改工种，权限码：BPeTypeJob:add")
    @PostMapping
    @PreAuthorize("@el.check('BPeTypeJob:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BPeTypeJobVo resources) {
        if(checkNameRepeat(resources)){
            return R.error("工种名称不能重复");
        }
        return R.ok(bPeTypeJobService.saveOrUpdate(BeanUtil.copyProperties(resources, BPeTypeJob.class)));
    }

    @Log("删除工种")
    @ApiOperation("删除工种，权限码：BPeTypeJob:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('BPeTypeJob:delete')")
    @Klock
    public R remove(@RequestBody Set<Integer> ids) {
        return R.ok(bPeTypeJobService.removeByIds(ids));
    }

    private boolean checkNameRepeat(BPeTypeJobVo resources) {
        return bPeTypeJobService.lambdaQuery().eq(BPeTypeJob::getType,resources.getType()).eq(BPeTypeJob::getName, resources.getName())
                .ne(ObjectUtil.isNotNull(resources.getId()),BPeTypeJob::getId,resources.getId()).count() > 0;
    }
}
