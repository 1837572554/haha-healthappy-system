package com.healthappy.modules.system.service.dto;


import lombok.Data;

import java.io.Serializable;

/**
 * @author cyt
 * @date 2020-08-17
 */
@Data
public class BSetUpDto  implements Serializable {
    /**
     * id
     */
    private String id;

    /**
     * 设置名称
     */
    private String xmlName;

    /**
     * 设置值
     */
    private String value;

    /**
     * 医疗机构ID
     */
    private String tenantId;
}
