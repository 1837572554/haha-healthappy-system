package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * @description 套餐表
 * @author sjc
 * @date 2021-07-2
 */
@Data
public class BPackageHDVo  {

    /**
     *套餐id
     */
    private String id;

    /**
     * 套餐名称
     */
    @NotNull(message = "套餐名称不能为空")
    @ApiModelProperty("套餐名称")
    private String name;

    /**
     * 体检类型
     */
    @ApiModelProperty("体检类型")
    private String type;

    /**
     * 适用性别
     */
    @ApiModelProperty("适用性别")
    private String sex;

    /**
     * 是否启用
     */
    @ApiModelProperty("是否启用")
    private String isEnable;

    /**
     * 排序号
     */
    @ApiModelProperty("排序号")
    private Integer dispOrde;

    /**
     * 描述
     */
    @ApiModelProperty("描述")
    private String desc;

    /**
     * 套餐价格
     */
    @ApiModelProperty("套餐价格")
    private BigDecimal price;

    /**
     * 套餐折扣
     */
    @ApiModelProperty("套餐折扣")
    private BigDecimal discount;

    /**
     * 套餐应付价格
     */
    @ApiModelProperty("套餐应付价格")
    private BigDecimal cost;

    /**
     * 简拼
     */
    @ApiModelProperty("简拼")
    private String jp;

    /**
     * 图片
     */
    @ApiModelProperty("图片")
    private String iamge;


    /**
     * 服务费
     */
    @ApiModelProperty("服务费")
    private BigDecimal serviceFee;

    /**
     * 套餐详情
     */
    @ApiModelProperty("套餐详情")
    @NotNull(message = "套餐组合不能为空")
    private List<BPackageDTVo> bPackageDt;
}
