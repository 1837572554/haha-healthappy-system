package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 批量结果录入-查询类
 * @author Jevany
 * @date 2022/5/13 17:23
 */
@Data
@ApiModel("批量结果录入-查询类")
public class BatchResultEntryQueryCriteria implements Serializable {
	/**
	 * 体检时间类型
	 */
	@ApiModelProperty(value = "体检时间类型 ,页面路径：batchResultEntry", position = 0)
	private Integer dateType;

	/**
	 * 时间段
	 */
	@ApiModelProperty(value = "时间段")
	private List<String> timeList;

	/**
	 * 体检分类ID
	 */
	@ApiModelProperty("体检分类ID")
	private String peTypeHdId;

	/**
	 * 体检类别ID
	 */
	@ApiModelProperty("体检类别ID")
	private String peTypeDtId;

	/**
	 * 体检号
	 */
	@ApiModelProperty("体检号")
	private String paId;

	/**
	 * 体检者姓名
	 */
	@ApiModelProperty("体检者姓名")
	private String paName;

	/**
	 * 单位ID
	 */
	@ApiModelProperty("单位ID")
	private String companyId;

	/**
	 * 科室编号
	 */
	@ApiModelProperty("科室编号")
	private String deptId;

	/**
	 * 组合编号
	 */
	@ApiModelProperty("组合编号")
	private String groupId;


	private static final long serialVersionUID = 1L;
}