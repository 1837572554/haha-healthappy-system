package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.*;
import io.swagger.annotations.*;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

/**
 * 组合项下的明细项结果集
 * @author Jevany
 * @date 2022/3/24 19:49
 */
@Data
@ApiModel("组合项下的明细项结果集")
public class GroupResultVo {
	/** 流水号 */
	@ApiModelProperty("流水号")
	private String paId;

    /** 组合项目编号 */
    @ApiModelProperty("组合项目编号")
    private String groupId;

    /** 小项结果集合 */
    @ApiModelProperty("小项结果集合")
    List<ItemResultVo> itemResultVos;
}
