package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.healthappy.config.SeedIdGenerator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 医学科普表
 * @author fang
 * @date 2021-06-21
 */
@Data
@TableName("B_Sickness_Science")
@ApiModel("医学科普表")
@SeedIdGenerator
public class BSicknessScience implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * id
     */
    @ApiModelProperty("id")
    @TableId(type = IdType.INPUT)
    private String id;

    /**
     * 病种名称id
     */
    @ApiModelProperty("病种名称id")
    private String sicknessId;

    /**
     * 医学科普
     */
    @ApiModelProperty("医学科普")
    private String science;

    /**
     * 是否删除
     */
    @ApiModelProperty("是否删除")
    @TableField(value = "del_flag",fill = FieldFill.INSERT)
    @TableLogic
    private String delFlag;

    /**
     * 医疗机构id(u_hospital_info的id)
     */
    @ApiModelProperty("医疗机构id(u_hospital_info的id)")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;
}
