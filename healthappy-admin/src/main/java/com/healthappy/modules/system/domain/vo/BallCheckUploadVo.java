package com.healthappy.modules.system.domain.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class BallCheckUploadVo {

    /**
     * 人员名称
     */
    @ExcelProperty(value = "姓名")
    @ApiModelProperty("姓名")
    private String name;

    @ExcelProperty(value = "性别")
    @ApiModelProperty("性别")
    private String sex;

    @ApiModelProperty("年龄")
    @ExcelProperty(index = 2)
    private String age;

    @ExcelProperty(value = "婚姻状况")
    @ApiModelProperty("婚姻状况")
    private String marry;

    @ExcelProperty(value = "手机")
    @ApiModelProperty("手机")
    private String mobile;

    @ExcelProperty(value = "身份证号")
    @ApiModelProperty("身份证号")
    private String idNo;

    @ExcelProperty(value = "单位ID")
    @ApiModelProperty("单位ID")
    private String companyId;

    @ExcelProperty(value = "单位名称")
    @ApiModelProperty("单位名称")
    private String companyName;

    @ExcelProperty(value = "部门")
    @ApiModelProperty("部门")
    private String departmentName;

    @ApiModelProperty("部门ID")
    private String departmentId;

    @ExcelProperty(value = "分组ID")
    @ApiModelProperty("分组ID")
    private String deptGroupId;

    @ExcelProperty(value = "分组名称")
    @ApiModelProperty("分组名称")
    private String deptGroupName;

    @ExcelProperty(value = "体检分类")
    @ApiModelProperty("体检分类")
    private String peTypeName;

    @ApiModelProperty("体检分类ID")
    private Long peTypeId;

    @ApiModelProperty("体检类别ID")
    private Long typeId;

    @ExcelProperty(value = "体检类别")
    @ApiModelProperty("体检类别")
    private String typeName;

    @ApiModelProperty("危害因素Ids")
    private String poisonTypeIds;

    @ExcelProperty(value = "危害因素")
    @ApiModelProperty("危害因素")
    private String poisonTypeName;

    @ApiModelProperty("工号")
    @ExcelProperty(value = "工号")
    private String jobNumber;

    @ExcelProperty(value = "工种")
    @ApiModelProperty("工种")
    private String jobName;

    @ApiModelProperty("工种ID")
    private Long jobId;

    @ExcelProperty(value = "接害工龄")
    @ApiModelProperty("接害工龄")
    private String pickAge;

    @ExcelProperty(value = "总工龄")
    @ApiModelProperty("总工龄")
    private String allAge;

    @ExcelProperty(value = "体检套餐")
    @ApiModelProperty("体检套餐")
    private String packageName;

    @ApiModelProperty("体检套餐ID")
    private String packageId;

    @ApiModelProperty("体检套餐价格")
    private BigDecimal packagePrice;

    @ExcelProperty(value = "附加套餐")
    @ApiModelProperty("附加套餐")
    private String additionPackageName;

    @ApiModelProperty("附加套餐ID")
    private String additionPackageId;

    @ApiModelProperty("附加套餐价格")
    private BigDecimal additionPackagePrice;

    @ApiModelProperty("附加项目Ids")
    private String addGroupIds;

    @ExcelProperty(value = "附加项目")
    @ApiModelProperty("附加项目")
    private String addGroupName;

    @ExcelProperty(value = "社保编号")
    @ApiModelProperty("社保编号")
    private String socialSecurityNumber;

    @ExcelProperty(value = "特殊减项目")
    @ApiModelProperty("特殊减项目")
    private String subtractGroupItem;

    @ExcelProperty(value = "备注")
    @ApiModelProperty("备注")
    private String remark;
}
