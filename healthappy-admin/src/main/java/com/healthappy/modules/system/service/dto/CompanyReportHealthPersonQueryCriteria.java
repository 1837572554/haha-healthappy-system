package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 单位健康报告管理-人员查询类
 * @author Jevany
 * @date 2022/2/18 9:58
 */
@Data
public class CompanyReportHealthPersonQueryCriteria extends CompanyReportPersonBase{

    /**
     * 体检类别Id
     */
    @ApiModelProperty("体检类别Id")
    private String peTypeDtId;
}
