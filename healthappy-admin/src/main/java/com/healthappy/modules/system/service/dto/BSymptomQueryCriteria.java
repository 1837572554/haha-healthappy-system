package com.healthappy.modules.system.service.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 症状表
 * @author sjc
 * @date 2021-08-10
 */
@Data
public class BSymptomQueryCriteria  {

    /**
     * 症状id
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("症状id")
    private Long id;

    /**
     * 症状名称
     */
    @Query(type = Query.Type.INNER_LIKE)
    @ApiModelProperty("症状名称")
    private String name;

    @Query
    @ApiModelProperty("是否启用")
    private String isEnable = "1";
}
