package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * @description 体检人员子项目表
 * @author sjc
 * @date 2021-07-20
 */
@Data
public class TItemDTQueryCriteria  {


    /**
     * 体检号
     */
    @ApiModelProperty("体检号")
    @Query(type = Query.Type.EQUAL)
    private String paId;


    /**
     * 项目编号
     */
    @ApiModelProperty("项目编号")
    @Query(type = Query.Type.EQUAL)
    private String itemId;

    /**
     * 项目名称
     */
    @ApiModelProperty("项目名称")
    @Query(type = Query.Type.INNER_LIKE)
    private String itemName;


    /**
     * 组合编号
     */
    @ApiModelProperty("组合编号")
    @Query(type = Query.Type.EQUAL)
    private String groupId;


    /**
     * 仪器通道号
     */
    @ApiModelProperty("仪器通道号")
    @Query(type = Query.Type.EQUAL)
    private String instrCode;



    /**
     * 复查字段  0非复查 大于1是复查
     */
    @ApiModelProperty("复查字段")
    @Query(type = Query.Type.EQUAL)
    private Integer review;
}
