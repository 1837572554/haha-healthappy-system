package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author Jevany
 * @date 2022/1/21 10:25
 * @desc 阳性结论名称 查询类
 */
@Data
public class SicknessConclusionListQueryCriteria {

	/**
	 * 体检时间类型Enum
	 */
	@NotNull(message = "体检时间类型不能为空")
	@ApiModelProperty(value = "体检时间类型, 页面路径：sicknessConclusion", required = true, position = 0)
	private Integer peDateType;

	@NotBlank(message = "开始时间不能为空")
	@ApiModelProperty(value = "开始时间 如：2021-01-01 00:00:00", required = true, example = "2021-01-01 00:00:00", position = 1)
	private String startTime;

	@NotBlank(message = "结束时间不能为空")
	@ApiModelProperty(value = "结束时间 如：2021-12-01 23:59:59", required = true, example = "2022-12-01 23:59:59", position = 2)
	private String endTime;

	/**
	 * 体检分类ID
	 */
	@ApiModelProperty("体检分类ID")
	private String peTypeHdId;

	/**
	 * 体检类别ID
	 */
	@ApiModelProperty("体检类别ID")
	private String peTypeDtId;

	/**
	 * 开始年龄
	 */
	@ApiModelProperty("开始年龄")
	private Integer startAge;

	/**
	 * 结束年龄
	 */
	@ApiModelProperty("结束年龄")
	private Integer endAge;

	/**
	 * 单位ID
	 */
	@ApiModelProperty("单位ID")
	private String companyId;

	/**
	 * 部门ID
	 */
	@ApiModelProperty("部门ID")
	private String deptId;

	/**
	 * 部门分组ID
	 */
	@ApiModelProperty("部门分组ID")
	private String deptGroupId;

	/**
	 * 租户ID
	 */
	@ApiModelProperty(value = "租户ID", hidden = true)
	private String tenantId;

	/** 病种id列表 */
	@ApiModelProperty("病种id列表")
	private List<String> sicknessIdList;

	/**
	 * 页面显示条数
	 */
	@ApiModelProperty("页面显示条数,默认为10")
	private Integer pageSize = 10;

	/**
	 * 是否导出，如果为True，分页功能不启用，默认False
	 */
	@ApiModelProperty(value = "是否导出，如果为True，分页功能不启用，默认False", hidden = true)
	private Boolean isExport = false;

	/**
	 * 页数
	 */
	@ApiModelProperty("页数,默认为1")
	private Integer pageNum = 1;

	/**
	 * 开始条数
	 */
	@ApiModelProperty(value = "开始条数", hidden = true)
	private Integer startIndex = -1;

	/**
	 * 体检类别ID
	 */
	@ApiModelProperty("体检类别ID")
	public String getPeTypeDtId() {
		if ("39".equals(this.peTypeDtId)) {
			return null;
		} else {
			return this.peTypeDtId;
		}
	}
}