package com.healthappy.modules.system.constants;

import com.healthappy.modules.http.report.RequestConstant;
import lombok.Data;
import org.apache.poi.ss.formula.functions.T;

/**
 * @Author: wukefei
 * @Date: Created in 2022/4/27 16:21
 * @Description:
 * @Version: 1.0
 *
 */

public enum CommRequstPathEnum {
	/**条码 **/
	BAR_CODE("条码", "PrintBarcode", "/Report/GetBarcodeReport"),
	/**指引单 **/
	GUIDE("指引单", "PrintGuide","/Report/GetGuideReport"),
	/**信息表 **/
	INFO_REPORT("打印信息表", "PrintInfo","/Report/GetPersonInfoReport"),
	/**预约信息表 **/
	APPOINT_INFO_REPORT("预约信息表", "PrintInfo","/Report/GetAppointInfoReport"),
	/**个人体检报告 **/
	PERSON_REPORT("个人体检报告", "PrintInfo", "/report/GetPersonReport"),
	/**单独体检项目报告生成 **/
	GET_ITEM_REPORT("体检项目报告生成", "PrintInfo","/report/GetItemReport"),
	/**单位报告 **/
	COMPANY_REPORT("单位报告", "PrintInfo","/report/GetCompanyProfessionReport"),
	/**单位健康分析报告 **/
	COMPANY_ANALYSIS("单位健康分析报告", "PrintInfo","/report/GetCompanyAnalysisReport"),
	/**健康证 **/
	HEALTH_CERTIFICATE("健康证", "PrintInfo","/report/GetHealthCertificateReport"),
	/**收费票据 **/
	PAY_REPORRT("收费票据","PrintInfo","/report/GetPayReport");


	private  String title;
	//指令
	private  String comm;
	//请求路径
	private  String requstPath;


	CommRequstPathEnum(String title,String comm,String requstPath){
		this.title=title;
		this.comm=comm;
		this.requstPath=requstPath;
	}

	public String getComm() {
		return comm;
	}

	public String getTitle() {
		return title;
	}

	public String getRequstPath() {
		return requstPath;
	}


}
