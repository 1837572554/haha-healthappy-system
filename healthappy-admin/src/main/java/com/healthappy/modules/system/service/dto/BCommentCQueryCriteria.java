package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BCommentCQueryCriteria {

    @Query(type = Query.Type.INNER_LIKE)
    @ApiModelProperty("结论")
    private String comment;
}
