package com.healthappy.modules.system.rest.print;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.healthappy.annotation.AnonymousAccess;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.http.report.HttpFileServer;
import com.healthappy.modules.http.report.HttpServer;
import com.healthappy.modules.http.report.RequestConstant;
import com.healthappy.modules.my.queue.JQueue;
import com.healthappy.modules.my.queue.JQueueHandle;
import com.healthappy.modules.my.queue.PrintRequestObject;
import com.healthappy.modules.system.domain.BReportConfig;
import com.healthappy.modules.system.domain.TPatient;
import com.healthappy.modules.system.domain.vo.AloneChargeMakeBillVo;
import com.healthappy.modules.system.domain.vo.AloneChargePrintVo;
import com.healthappy.modules.system.domain.vo.PrintGroupVo;
import com.healthappy.modules.system.rest.print.dto.BarcodePrintVO;
import com.healthappy.modules.system.service.AlonePayCostService;
import com.healthappy.modules.system.service.BPeTypeHDService;
import com.healthappy.modules.system.service.BReportConfigService;
import com.healthappy.modules.system.service.TPatientService;
import com.healthappy.modules.system.service.mapper.TChargeMapper;
import com.healthappy.utils.ConvertUpMoney;
import com.healthappy.utils.R;
import com.healthappy.utils.SecurityUtils;
import com.healthappy.utils.StringUtils;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author FGQ
 */
@Slf4j
@Api(tags = "打印")
@RestController
@AllArgsConstructor
@RequestMapping("/print")
public class PrintController {

	private final BReportConfigService reportConfigService;
	private final TPatientService patientService;
	private final BPeTypeHDService peTypeHDService;
	private final AlonePayCostService alonePayCostService;

	private final TChargeMapper chargeMapper;

	private final JQueueHandle jQueueHandle;


	@Log("打印健康证")
	@ApiOperation("打印健康证")
	@PostMapping("/healthCertificate")
	@ApiImplicitParams(@ApiImplicitParam(name = "paId", value = "流水号多个逗号分隔", dataType = "String", required = true))
	@ApiResponses(value = {@ApiResponse(code = 200, message = "返回健康证地址")})
	@AnonymousAccess
	public R healthCertificate(@RequestBody String paId) {
		if (StrUtil.isBlank(paId)) {
			return R.error("流水号不能为空");
		}
		return R.ok(healthCertificateReport(paId));
	}

	/**
	 * 打印指引单 项目显示要求组合项设置，1.申请类型不能为空 2.条码名称不能为空，才会显示
	 */
	@Log("打印指引单")
	@ApiOperation("打印指引单")
	@GetMapping("/guide")
	@ApiImplicitParams(@ApiImplicitParam(name = "paId", value = "流水号", dataType = "String", required = true))
	@ApiResponses(value = {@ApiResponse(code = 200, message = "返回指引单地址")})
	@AnonymousAccess
	public R guide(@RequestParam String paId) {
		if (StrUtil.isBlank(paId)) {
			return R.error("流水号不能为空");
		}
		HttpServer.BeanOne beanOne = new HttpServer.BeanOne();
		beanOne.init();
		beanOne.setPatientId(paId);
		return R.ok(HttpFileServer.fileByUrl(HttpServer.getGuideReport(beanOne)));
	}

	/**
	 * 打印条码：要求组合项设置，1.条码名称不为空2.条码数量不等于0，才会显示
	 */
	@Log("打印条码")
	@ApiOperation("打印条码")
	@GetMapping("/barcode")
	@ApiImplicitParams(@ApiImplicitParam(name = "paId", value = "流水号", dataType = "String", required = true))
	@ApiResponses(value = {@ApiResponse(code = 200, message = "返回条码地址")})
	@AnonymousAccess
	public R barcode(@RequestParam String paId) {
		if (StrUtil.isBlank(paId)) {
			return R.error("流水号不能为空");
		}
		HttpServer.BeanOne beanOne = new HttpServer.BeanOne();
		beanOne.init();
		beanOne.setPatientId(paId);
		return R.ok(HttpFileServer.fileByUrl(HttpServer.getBarcodeReport(beanOne)));
	}

	/**
	 * 打印条码：要求组合项设置，1.条码名称不为空2.条码数量不等于0，才会显示
	 */
	@Log("打印条码列表")
	@ApiOperation("打印条码列表")
	@PostMapping("/barcodeByList")
	@ApiResponses(value = {@ApiResponse(code = 200, message = "返回条码地址")})
	@AnonymousAccess
	public R barcodeByList(@Validated @RequestBody BarcodePrintVO barcodePrintVO) {
		HttpServer.BarcodeInfo barcodeInfo = new HttpServer.BarcodeInfo();
		barcodeInfo.init();
		barcodeInfo.setPatientId(barcodePrintVO.getPaId());
		barcodeInfo.setBarcodes(barcodePrintVO.getBarcodes());
		return R.ok(HttpFileServer.fileByUrl(HttpServer.getBarcodeReportList(barcodeInfo)));
	}


	@Log("打印信息表")
	@ApiOperation("打印信息表")
	@GetMapping("/information")
	@ApiImplicitParams(@ApiImplicitParam(name = "paId", value = "流水号", dataType = "String", required = true))
	@ApiResponses(value = {@ApiResponse(code = 200, message = "返回信息表地址")})
	@AnonymousAccess
	public R information(@RequestParam String paId) {
		if (StrUtil.isBlank(paId)) {
			return R.error("流水号不能为空");
		}
		HttpServer.PersonReport personReport = new HttpServer.PersonReport();
		personReport.initInfo();
		personReport.setPatientId(paId);
		return R.ok(HttpFileServer.fileByUrl(HttpServer.getInfoReport(personReport)));
	}

	@Log("打印预约信息表")
	@ApiOperation("打印预约信息表")
	@GetMapping("/appointInformation")
	@ApiImplicitParams(@ApiImplicitParam(name = "appointId", value = "预约号", dataType = "String", required = true))
	@ApiResponses(value = {@ApiResponse(code = 200, message = "返回信息表地址")})
	@AnonymousAccess
	public R appointInformation(@RequestParam String appointId) {
		if (StrUtil.isBlank(appointId)) {
			return R.error("预约号不能为空");
		}
		HttpServer.AppointInfo appointInfo = new HttpServer.AppointInfo();
		appointInfo.init();
		appointInfo.setAppointId(appointId);
		return R.ok(HttpFileServer.fileByUrl(HttpServer.getAppointInfoReport(appointInfo)));
	}

	@Log("打印项目报告")
	@ApiOperation("打印项目报告")
	@GetMapping("/groupPrint")
	@ApiImplicitParams({@ApiImplicitParam(name = "paId", value = "流水号", dataType = "String", required = true),
			@ApiImplicitParam(name = "groupId", value = "组合编号", dataType = "String", required = true)})
	@ApiResponses(value = {@ApiResponse(code = 200, message = "返回项目报告地址")})
	@AnonymousAccess
	public R groupPrint(@RequestParam String paId, @RequestParam String groupId) {
		if (StrUtil.isBlank(paId)) {
			return R.error("流水号不能为空");
		}
		if (StrUtil.isBlank(groupId)) {
			return R.error("组合编号不能为空");
		}
		HttpServer.GetItemReport itemReport = new HttpServer.GetItemReport();
		itemReport.init();
		itemReport.setPatientId(paId);
		itemReport.setGroupId(groupId);
		HttpServer.getItemReport(itemReport);
		return R.ok(HttpFileServer.fileByUrl(HttpServer.getItemReport(itemReport)));
	}

	@Log("批量打印项目报告")
	@ApiOperation("批量打印项目报告")
	@PostMapping("/batchGroupPrint")
	@ApiResponses(value = {@ApiResponse(code = 200, message = "通过WebSocket返回对应的地址")})
	@AnonymousAccess
	public R batchGroupPrint(@RequestBody PrintGroupVo groupVos) {
		if (!Optional.ofNullable(groupVos).isPresent()) {
			return R.error("参数不能为空");
		}
		if (CollUtil.isEmpty(groupVos.getPaIdList())) {
			return R.error("流水号不能为空");
		}
		if (CollUtil.isEmpty(groupVos.getGroupIdList())) {
			return R.error("组合编号不能为空");
		}
		if (StringUtils.isBlank(groupVos.getAcceptSocketUserId())) {
			return R.error("用于接收的Socket的用户编号不能为空");
		}
		HttpServer.GetItemReport itemReport = new HttpServer.GetItemReport();
		itemReport.init();

		JQueue<PrintRequestObject> jQueue = new JQueue<>();
		//		String userName = SecurityUtils.getUsername();
		//		String userId = String.valueOf(SecurityUtils.getUserId());

		PrintRequestObject object = null;
		for (String pId : groupVos.getPaIdList()) {
			for (String gId : groupVos.getGroupIdList()) {
				itemReport.setPatientId(pId);
				itemReport.setGroupId(gId);

				object = new PrintRequestObject();
				object.setHttpMethod(HttpMethod.POST);
				object.setRequestMapping(RequestConstant.GET_ITEM_REPORT);
				object.setParamMap(BeanUtil.beanToMap(itemReport));
				object.setCommType("PrintReport");
				object.setUserName(groupVos.getAcceptSocketUserId());
				jQueue.add(object);
			}
		}

		new Thread(() -> {
			try {
				jQueueHandle.generatePDF(jQueue);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
		return R.ok();
		//		return R.ok(HttpFileServer.fileByUrl(HttpServer.getItemReport(itemReport)));
	}

	@Log("导出预约信息表")
	@ApiOperation("导出预约信息表")
	@PostMapping("/exportAppointInfo")
	@ApiImplicitParams({@ApiImplicitParam(name = "appointIds", value = "预约号列表", dataType = "List", required = true)})
	@ApiResponses(value = {@ApiResponse(code = 200, message = "返回预约列表的压缩包")})
	@AnonymousAccess
	public R exportAppointInfo(@RequestBody List<String> appointIds) {
		if (CollUtil.isEmpty(appointIds)) {
			return R.error("预约号列表不能为空");
		}
		List<String> reportInfoNames = new ArrayList<>();
		for (String appointId : appointIds) {
			try {
				HttpServer.AppointInfo appointInfo = new HttpServer.AppointInfo();
				appointInfo.init();
				appointInfo.setAppointId(appointId);
				appointInfo.setFileType(1);
				reportInfoNames.add(HttpServer.getAppointInfoReport(appointInfo));
			} catch (Exception e) {
				log.error("批量预约导出出错：{0}", e.getMessage());
			}
		}
		if (CollUtil.isNotEmpty(reportInfoNames)) {
			return R.ok(HttpFileServer.fileZipByUrl(reportInfoNames.toArray(new String[0])));
		}
		return R.ok();
	}


	@Log("打印个人报告")
	@ApiOperation("打印个人报告")
	@GetMapping("/personPrint")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "paId", value = "流水号（健康证多个逗号分隔）", dataType = "String", required = true),
			@ApiImplicitParam(name = "reportName", value = "报告类型名称:个人健康体检报告、个人精简报告、职业健康检查表、职业健康检查通知单、儿童健康体检报告、从业预防性体检报告、放射健康检查表", dataType = "String", required = true)})
	@ApiResponses(value = {@ApiResponse(code = 200, message = "返回个人报告地址")})
	@AnonymousAccess
	public R personPrint(@RequestParam String paId, @RequestParam String reportName) {
		if (StrUtil.isBlank(paId)) {
			return R.error("流水号不能为空");
		}
		if (StrUtil.isBlank(reportName)) {
			return R.error("报告类型名称不能为空");
		}
		if ("健康证".equals(reportName)) {
			return R.ok(healthCertificateReport(paId));
		}

		return R.ok(personReport(paId, reportName));
	}


	@Log("打印单位职业报告")
	@ApiOperation("打印单位职业报告")
	@GetMapping("/companyPrint")
	@ApiImplicitParams({@ApiImplicitParam(name = "reportId", value = "单位报告编号", dataType = "String", required = true),
			@ApiImplicitParam(name = "reportName", value = "报告类型名称:单位职业报告", dataType = "String", required = true)})
	@ApiResponses(value = {@ApiResponse(code = 200, message = "返回单位报告地址")})
	@AnonymousAccess
	public R companyPrint(@RequestParam String reportId, @RequestParam String reportName) {
		if (StrUtil.isBlank(reportId)) {
			return R.error("单位报告编号不能为空");
		}
		if (StrUtil.isBlank(reportName)) {
			return R.error("报告类型名称不能为空");
		}
		HttpServer.CompanyReport companyReport = new HttpServer.CompanyReport();
		companyReport.setReportId(reportId);

		BReportConfig reportConfig = reportConfigService.getBReportConfig("单位职业报告", 1, 2);
		if (StrUtil.isNotBlank(reportConfig.getReportName())) {
			companyReport.setConfig(reportConfig);
		} else {
			companyReport.initOccupation();
		}
		return R.ok(HttpFileServer.fileZipByUrl(HttpServer.getCompanyReport(companyReport)));

		//        switch (reportName) {
		//            case "单位职业报告":
		//
		//                companyReport.initOccupation();
		//                break;
		//            default:
		//                companyReport.initOccupation();
		//                break;
		//        }
		//        return R.ok(HttpFileServer.fileZipByUrl(HttpServer.getCompanyReport(companyReport)));
	}

	@Log("打印单位健康分析报告")
	@ApiOperation("打印单位健康分析报告")
	@GetMapping("/companyAnalysisPrint")
	@ApiImplicitParams({@ApiImplicitParam(name = "reportId", value = "单位报告编号", dataType = "String", required = true),
			@ApiImplicitParam(name = "reportName", value = "报告类型名称:单位职业报告", dataType = "String", required = true)})
	@ApiResponses(value = {@ApiResponse(code = 200, message = "返回单位报告地址")})
	@AnonymousAccess
	public R companyAnalysisPrint(@RequestParam String reportId, @RequestParam String sicknessIds) {
		if (StrUtil.isBlank(reportId)) {
			return R.error("单位报告编号不能为空");
		}
		if (StrUtil.isBlank(sicknessIds)) {
			return R.error("疾病编号不能为空");
		}
		HttpServer.CompanyAnalysisReport companyAnalysisReport = new HttpServer.CompanyAnalysisReport();
		companyAnalysisReport.setReportId(reportId);
		companyAnalysisReport.setSicknessIds(sicknessIds);

		BReportConfig reportConfig = reportConfigService.getBReportConfig("单位健康报告", 1, 2);
		if (StrUtil.isNotBlank(reportConfig.getReportName())) {
			companyAnalysisReport.setConfig(reportConfig);
		} else {
			companyAnalysisReport.init();
		}
		return R.ok(HttpFileServer.fileZipByUrl(HttpServer.getCompanyAnalysis(companyAnalysisReport)));
	}

	@Log("生成收费票据")
	@ApiOperation("生成收费票据")
	@PostMapping("/payReportPrint")
	@ApiResponses(value = {@ApiResponse(code = 200, message = "返回收费票据地址")})
	@AnonymousAccess
	public R payReportPrint(@RequestBody AloneChargeMakeBillVo vo) {
		String tenantId = SecurityUtils.getTenantId();
		if (StrUtil.isBlank(tenantId)) {
			return R.error("租户编号出错");
		}
		if (StrUtil.isBlank(vo.getPaId())) {
			return R.error("流水号不能为空");
		}
		if (StrUtil.isBlank(vo.getReceiptType())) {
			return R.error("支付类型不能为空");
		}

		if (StrUtil.isNotBlank(vo.getReceipt())) {
			alonePayCostService.makeBill(BeanUtil.copyProperties(vo, AloneChargeMakeBillVo.class));
		}
		AloneChargePrintVo aloneChargePrintVo = chargeMapper.getLatestValidDataPrint(vo.getPaId(), vo.getReceiptType(),
				tenantId);
		if (!Optional.ofNullable(aloneChargePrintVo).isPresent()) {
			throw new BadRequestException("当前人员无有效记录");
		}
		aloneChargePrintVo.setPayType("自费");
		aloneChargePrintVo.setFeeName("体检费");
		if (StrUtil.isBlank(aloneChargePrintVo.getFee())) {
			aloneChargePrintVo.setFee("0");
		}
		aloneChargePrintVo.setCost(aloneChargePrintVo.getFee());
		aloneChargePrintVo.setPay(ConvertUpMoney.toChinese(aloneChargePrintVo.getFee()));
		aloneChargePrintVo.setFileType(1);

		return R.ok(HttpFileServer.fileByUrl(HttpServer.getPayReport(aloneChargePrintVo)));
	}

	/**
	 * 生成健康证
	 * @author YJ
	 * @date 2022/4/11 17:05
	 * @param paId
	 * @return java.lang.String
	 */
	private String healthCertificateReport(String paId) {
		HttpServer.BeanOne beanOne = new HttpServer.BeanOne();
		beanOne.init();
		beanOne.setPatientId(paId);
		return HttpFileServer.fileByUrl(HttpServer.getHealthCertificate(beanOne));
	}


	/**
	 * 个人报告打印
	 *
	 * @param paId       流水号
	 * @param reportName 报告类型名称
	 * @return java.lang.String
	 * @author YJ
	 * @date 2022/3/22 15:54
	 */
	private String personReport(String paId, String reportName) {
		HttpServer.PersonReport personReport = new HttpServer.PersonReport();
		personReport.setPatientId(paId);
		TPatient patient = patientService.getPatientInfo(paId);
		Integer peTypeGroupId = peTypeHDService.getPeTypeGroupId(patient.getPeTypeHdId());
		Integer reportType = 1;
		BReportConfig reportConfig;

		switch (reportName) {
			case "个人健康体检报告":
				reportConfig = reportConfigService.getBReportConfig("健康体检报告", peTypeGroupId, reportType);
				if (StrUtil.isNotBlank(reportConfig.getReportName())) {
					personReport.setConfig(reportConfig);
				} else {
					personReport.initHealth();
				}
				break;
			case "个人精简报告":
				reportConfig = reportConfigService.getBReportConfig("个人精简报告", peTypeGroupId, reportType);
				if (StrUtil.isNotBlank(reportConfig.getReportName())) {
					personReport.setConfig(reportConfig);
				} else {
					personReport.initHealthLite();
				}
				break;
			case "职业健康检查表":
				reportConfig = reportConfigService.getBReportConfig("职业体检报告", peTypeGroupId, reportType);
				if (StrUtil.isNotBlank(reportConfig.getReportName())) {
					personReport.setConfig(reportConfig);
				} else {
					personReport.initOccupation();
				}
				break;
			case "职业健康检查通知单":
				reportConfig = reportConfigService.getBReportConfig("健康检查通知单", peTypeGroupId, reportType);
				if (StrUtil.isNotBlank(reportConfig.getReportName())) {
					personReport.setConfig(reportConfig);
				} else {
					personReport.initOccupationNotice();
				}
				break;
			case "儿童健康体检报告":
				reportConfig = reportConfigService.getBReportConfig("儿童健康体检报告", peTypeGroupId, reportType);
				if (StrUtil.isNotBlank(reportConfig.getReportName())) {
					personReport.setConfig(reportConfig);
				} else {
					personReport.childHealth();
				}
				break;
			case "从业预防性体检报告":
				reportConfig = reportConfigService.getBReportConfig("从业体检报告", peTypeGroupId, reportType);
				if (StrUtil.isNotBlank(reportConfig.getReportName())) {
					personReport.setConfig(reportConfig);
				} else {
					personReport.practitionerPrevention();
				}
				break;
			case "健康证":
				return healthCertificateReport(paId);
			//            case "放射健康检查表":
			//                personReport.initHealth();
			//                break;
			default:
				personReport.initHealth();
				break;
		}
		//		return HttpServer.getPersonReport(personReport);
		return HttpFileServer.fileByUrl(HttpServer.getPersonReport(personReport));
	}
}
