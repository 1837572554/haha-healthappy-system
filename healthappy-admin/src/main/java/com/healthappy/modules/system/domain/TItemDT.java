package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @description 体检人员子项目表
 * @author sjc
 * @date 2021-07-20
 */
@Data
@ApiModel("体检人员子项目表")
@TableName("T_Item_DT")
@RenewLog
public class TItemDT implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    @TableId(type = IdType.AUTO)
    private Long id;


    /**
     * 体检号
     */
    @ApiModelProperty("体检号")
    private String paId;


    /**
     * 项目编号
     */
    @ApiModelProperty("项目编号")
    private String itemId;

    /**
     * 项目名称
     */
    @ApiModelProperty("项目名称")
    private String itemName;


    /**
     * 组合编号
     */
    @ApiModelProperty("组合编号")
    private String groupId;


    /**
     * 仪器通道号
     */
    @ApiModelProperty("仪器通道号")
    private String instrCode;


    /**
     * 项目结果
     */
    @ApiModelProperty("项目结果")
    private String result;


    /**
     * 结果提示
     */
    @ApiModelProperty("结果提示")
    private String resultMark;


    /**
     * 结果描述
     */
    @ApiModelProperty("结果描述")
    private String resultDesc;


    /**
     * 结果单位
     */
    @ApiModelProperty("结果单位")
    private String unit;


    /**
     * 参考上限
     */
    @ApiModelProperty("参考上限")
    private String refHigh;


    /**
     * 参考下限
     */
    @ApiModelProperty("参考下限")
    private String refLow;


    /**
     * 参考值
     */
    @ApiModelProperty("参考值")
    private String refValue;


    /**
     * 1是重大阳性  0正常
     */
    @ApiModelProperty("1是重大阳性  0正常")
    private String positiveMark;


    /**
     * 复查字段  0非复查 大于1是复查
     */
    @ApiModelProperty("复查字段")
    private Integer review;


    /**
     * 体检收费单明细表主健ID
     */
    @ApiModelProperty("体检收费单明细表主健ID")
    private String fDetailId;


    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp updateTime;


    /**
     * 医疗机构ID(U_Hospital_Info的ID)
     */
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;
}
