package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BCompany;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @description 单位表
 * @author sjc
 * @date 2021-06-24
 */
@Mapper
@Repository
public interface BCompanyMapper extends CoreMapper<BCompany> {

    BCompany getMaxCompany(@Param("wsCode") String workStationCode,@Param("tenantId") String tenantId);
}
