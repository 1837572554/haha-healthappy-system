package com.healthappy.modules.system.domain;


import com.baomidou.mybatisplus.annotation.*;
import com.healthappy.config.SeedIdGenerator;
import com.healthappy.config.databind.FieldBind;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 病种规则
 * @author sjc
 * @date 2021-6-22
 */
@Data
@ApiModel("病种规则")
@SeedIdGenerator
@TableName("B_Sickness_Rule")
public class BSicknessRule implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    @TableId(type = IdType.INPUT)
    private String id;


    /**
     * 子项目id
     */
    @ApiModelProperty("子项目id")
    private String itemId;

    /**
     * 病种名称id
     */
    @ApiModelProperty("病种名称id")
    private String sicknessId;

//    /**
//     * 病种建议ID
//     */
//    @ApiModelProperty("病种建议ID")
//    private String sicknessAdviceId;

    /**
     * 规则类型
     */
    @ApiModelProperty("规则类型")
    @FieldBind(target = "ruleTypeText")
    private String ruleType;

    @TableField(exist = false)
    private String ruleTypeText;
    /**
     * 规则字符串
     */
    @ApiModelProperty("规则字符串")
    private String ruleStr;

    /**
     * 性别
     */
    @ApiModelProperty("性别")
    @FieldBind(target = "sexText")
    private String sex;

    @TableField(exist = false)
    private String sexText;

    /**
     * 年龄低值
     */
    @ApiModelProperty("年龄低值")
    private Integer nlLow;

    /**
     * 年龄高值
     */
    @ApiModelProperty("年龄高值")
    private Integer nlHigh;

    /**
     * 规则低值
     */
    @ApiModelProperty("规则低值")
    private Double ruleLow;

    /**
     * 规则高值
     */
    @ApiModelProperty("规则高值")
    private Double ruleHigh;

    /**
     * 异常表示
     */
    @ApiModelProperty("异常表示")
    private String ycbs;


    /**
     * 是否阳性结果 0不是  1是
     */
    @ApiModelProperty("是否阳性结果")
    private String isPositive;

    /**
     * 正常范围参考值
     */
    @ApiModelProperty("正常范围参考值")
    private String reference;

    /**
     * 显示序号
     */
    @ApiModelProperty("显示序号")
    private Integer dispOrder;

//    /**
//     * 病种建议ID
//     */
//    @ApiModelProperty("病种建议ID")
//    private String adviceId;

    /**
     * 是否是危急值 0不是  1是
     */
    @ApiModelProperty("是否是危急值")
    private String isCritical;

    /**
     * 是否已经删除 0不是  1是
     */
    @ApiModelProperty("是否已经删除")
    @TableField(value = "del_flag",fill = FieldFill.INSERT)
    @TableLogic
    private String delFlag;
}
