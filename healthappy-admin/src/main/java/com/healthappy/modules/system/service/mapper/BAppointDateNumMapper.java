package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BAppointDateNum;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Jevany
 * @date 2022/5/7 0:08
 */
@Mapper
@Repository
public interface BAppointDateNumMapper extends CoreMapper<BAppointDateNum> {

	
}
