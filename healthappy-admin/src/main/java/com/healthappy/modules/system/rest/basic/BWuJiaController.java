package com.healthappy.modules.system.rest.basic;

import cn.hutool.core.bean.BeanUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BWuJia;
import com.healthappy.modules.system.domain.vo.BWuJiaVo;
import com.healthappy.modules.system.service.BWuJiaService;
import com.healthappy.modules.system.service.dto.BWuJiaQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description 物价 控制器
 * @author sjc
 * @date 2021-06-28
 */
@Slf4j
@Api(tags = "基础管理：物价")
@RestController
@AllArgsConstructor
@RequestMapping("/BWuJia")
public class BWuJiaController {

    private final BWuJiaService bWuJiaService;

    @Log("分页查询物价表数据")
    @ApiOperation("分页查询物价表数据，权限码：BWuJia:list")
    @GetMapping
    @PreAuthorize("@el.check('BWuJia:list','BGroupHd:add')")
    public R listPage(BWuJiaQueryCriteria criteria, Pageable pageable) {
        return R.ok(bWuJiaService.queryAll(criteria, pageable));
    }

    @Log("新增|修改物价表")
    @ApiOperation("新增|修改物价表，权限码：BWuJia:add")
    @PostMapping
    @PreAuthorize("@el.check('BWuJia:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BWuJiaVo resources) {
        BWuJia bWuJia = new BWuJia();
        BeanUtil.copyProperties(resources,bWuJia);
        if(bWuJiaService.saveOrUpdate(bWuJia)){
            return R.ok();
        }
        return R.error(500,"方法异常");
    }

    @Log("删除物价项")
    @ApiOperation("删除物价项，权限码：BWuJia:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('BWuJia:delete')")
    @Klock
    public R removeSicknessId(@RequestParam("id") Long id) {
        if(bWuJiaService.deleteById(id))
            return R.ok();
        return R.error(500,"方法异常");
    }
}
