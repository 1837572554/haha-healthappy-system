package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class DictEnumVo implements Serializable {

    @ApiModelProperty("名称")
    private String name;

    private String label;

    private String value;

    private Integer sort;
}
