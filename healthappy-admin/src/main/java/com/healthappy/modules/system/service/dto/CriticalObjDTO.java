package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 危急值 对象 DTO
 * @author Jevany
 * @date 2022/2/11 11:39
 */
@Data
public class CriticalObjDTO implements Serializable {

    /** 危急值自动编号 */
    @ApiModelProperty("危急值自动编号")
    private Long id;

    /** 流水号 */
    @NotBlank(message = "流水号不能为空")
    @ApiModelProperty("流水号")
    private String paId;

    /** 姓名 */
    @ApiModelProperty("姓名")
    private String name;

    /** 性别ID */
    @ApiModelProperty("性别ID")
    private String sexId;

    /** 性别 */
    @ApiModelProperty("性别")
    private String sex;

    /** 年龄 */
    @ApiModelProperty("年龄")
    private Integer age;

    /** 身份证 */
    @ApiModelProperty("身份证")
    private String idNo;

    /** 电话 */
    @ApiModelProperty("电话")
    private String phone;

    /** 发生科室ID */
    @NotBlank(message = "发生科室不能为空")
    @ApiModelProperty("发生科室Id")
    private String occuDeptId;

    /** 发生科室 */
    @ApiModelProperty("发生科室")
    private String occuDept;

    /** 组合项目编号 */
    @NotBlank(message = "组合项目不能为空")
    @ApiModelProperty("组合项目编号")
    private String groupId;

    /** 组合项目名称 */
    @ApiModelProperty("组合项目名称")
    private String groupName;

    /** 小项编号 */
    @ApiModelProperty("小项编号")
    private String itemId;

    /** 小项名称 */
    @ApiModelProperty("小项名称")
    private String itemName;

    /** 通知方式 */
    @ApiModelProperty("通知方式")
    private String noticeWay;

    /** 发生时间 */
    @ApiModelProperty("发生时间")
    private String occuTime;

    /** 发生时间-时间类型 */
    @ApiModelProperty(value = "发生时间-时间类型",hidden = true)
    private Timestamp occuTimestamp;

    /** 具体情况 */
    @ApiModelProperty("具体情况")
    private String detail;

    /** 处理时间 */
    @NotBlank(message = "处理时间不能为空")
    @ApiModelProperty("处理时间")
    private String dealTime;

    /** 处理医生 */
    @ApiModelProperty("处理医生")
    private String dealDoctor;

    /** 处理情况 */
    @ApiModelProperty("处理情况")
    private String dealDetail;

    /** 异常类型 */
    @NotBlank(message = "异常类型不能为空")
    @ApiModelProperty("异常类型")
    private String affectLevel;

    /** 转诊科室 */
    @ApiModelProperty("转诊科室")
    private String transferDept;

    /** 租户id */
    @ApiModelProperty("租户id")
    private String tenantId;
}