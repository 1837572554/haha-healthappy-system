package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CustomQueryCriteria {

    @ApiModelProperty("1:登记时间,2:签到时间,3:总检时间")
    private Integer typeTime;

    @ApiModelProperty("时间")
    private String time;

    @ApiModelProperty("单位")
    private String companyId;

    @ApiModelProperty("体检分类")
    private String bPeTypeHdId;

    @ApiModelProperty("工作站")
    private String uWorkStationId;

    @ApiModelProperty("参数名")
    private String fileName;

    @ApiModelProperty("查询参数")
    private String queryName;

    @ApiModelProperty("组合项-判定")
    private Boolean zhDecide = false;

    @ApiModelProperty("描述")
    private Boolean desc = false;

    @ApiModelProperty("基础项-判定")
    private Boolean jcDecide = false;
}