package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 单位健康报告编制 - 分析病种列表明细 DTO
 * @author Jevany
 * @date 2022/2/22 19:03
 */
@Data
public class CompanyRepSicknessDetailDTO {
    /** 性别 */
    @ApiModelProperty("性别")
    private String sex;

    /** 病种Id */
    @ApiModelProperty("病种Id")
    private String sicknessId;

    /** 病种名称 */
    @ApiModelProperty("病种名称")
    private String sicknessName;
}