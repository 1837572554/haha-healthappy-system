package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TCritical;
import com.healthappy.modules.system.domain.vo.ProjectCheckVo;
import com.healthappy.modules.system.service.dto.CriticalDTO;
import com.healthappy.modules.system.service.dto.CriticalObjDTO;
import com.healthappy.modules.system.service.dto.CriticalQueryCriteria;
import com.healthappy.modules.system.service.impl.CriticalNotificationDTO;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 危急值 服务接口层
 * @author Jevany
 * @date 2022/2/9 18:14
 */
public interface TCriticalService extends BaseService<TCritical> {

    /**
     * 检查病种中是否有危急值，如果存在则新增危急数据(每一次保存都会新增危急值)
     * @author YJ
     * @date 2022/2/9 18:15
     * @param projectCheckVo
     */
    void checkCritical(ProjectCheckVo projectCheckVo);

    /**
     * 获得危急值列表
     * @author YJ
     * @date 2022/2/10 18:37
     * @param criteria 危急值状况查询类
     * @return java.util.Map〈java.lang.String,java.lang.Object〉
     */
    Map<String,Object> getCriticalList(CriticalQueryCriteria criteria);

    /**
     * 导出危急值列表
     * @author YJ
     * @date 2022/2/10 18:37
     * @param all
     * @param response
     */
    void export(List<CriticalDTO> all, HttpServletResponse response) throws IOException;


    /**
     * 获得危急值对象
     * @author YJ
     * @date 2022/2/11 9:58
     * @param id 危急值自动编号
     * @return com.healthappy.modules.system.domain.TCritical
     */
    CriticalObjDTO getCriteria(Long id);

    /**
     * 编辑危急值状态
     * @author YJ
     * @date 2022/2/11 9:54
     * @param critical
     */
    Integer editCriteria(CriticalObjDTO critical);

    /**
     * 删除 危急值
     * @author YJ
     * @date 2022/2/16 15:13
     * @param id
     * @return java.lang.Boolean
     */
    Boolean deleteCriteria(Long id);

    /**
     * 测试危急值通知
     * @author YJ
     * @date 2022/2/28 9:33
     */
    void testCriteriaNotification(Long criticalId);

    /**
     * 发送危急值通知
     * @author YJ
     * @date 2022/3/17 10:44
     * @param criticalNotificationDTO 危急值通知类
     * @param checkDoctorId 检测医生（项目检查保存的医生），没有则填Null
     */
    void sendCriticalNotification(CriticalNotificationDTO criticalNotificationDTO, Long checkDoctorId);

    /**
     * 获得需要推送的危急值数据
     * @author YJ
     * @date 2022/3/17 10:45
     */
    void listNeedNotificationCritical();
}