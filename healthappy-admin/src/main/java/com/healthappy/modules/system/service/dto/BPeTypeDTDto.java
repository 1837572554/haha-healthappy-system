package com.healthappy.modules.system.service.dto;

import com.healthappy.modules.system.domain.BPeTypeDT;
import lombok.Data;



/**
 * @description 体检类别
 * @author yanjun
 * @date 2021年9月3日 11:57:02
 */
@Data
public class BPeTypeDTDto extends BPeTypeDT {


}
