package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 预约号源详细
 * @author Jevany
 * @date 2022/5/7 0:29
 */
@ApiModel("预约号源详细")
@Data
public class AppointDateNumVO implements Serializable {
	/** 公司编号 */
	@ApiModelProperty("公司编号")
	private String companyId;
	/** 公司名称 */
	@ApiModelProperty("公司名称")
	private String company;
	/** 已预约数 */
	@ApiModelProperty("已预约数")
	private Integer num1;
	/** 设置数量 */
	@ApiModelProperty("设置数量")
	private Integer num2;

	private static final long serialVersionUID = 1L;
}