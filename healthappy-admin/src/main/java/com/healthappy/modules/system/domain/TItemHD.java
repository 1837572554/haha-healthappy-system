package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @description 体检人员项目表
 * @author sjc
 * @date 2021-06-29
 */
@Data
@ApiModel("体检人员项目表")
@TableName("T_Item_HD")
@RenewLog
public class TItemHD implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    @TableId(type = IdType.AUTO)
    private Long id;


    /**
     * 体检号
     */
    @ApiModelProperty("体检号")
    private String paId;

    /**
     * 组合编号
     */
    @ApiModelProperty("组合编号")
    private String groupId;

    /**
     * 科室编号
     */
    @ApiModelProperty("科室编号")
    private String deptId;

    /**
     * 组合名称
     */
    @ApiModelProperty("组合名称")
    private String groupName;
    /**
     * 结果
     */
    @ApiModelProperty("结果")
    private String result;

    /**
     * 描述
     */
    @ApiModelProperty("描述")
    private String resultDesc;
    /**
     * 结果判定 0正常 1异常
     */
    @ApiModelProperty("结果判定 0正常 1异常")
    private String resultMark;

    /**
     * 检查日期
     */
    @ApiModelProperty("检查日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date resultDate;

    /**
     * 医生
     */
    @ApiModelProperty("医生")
    private String doctor;

    /**
     * 审核医生
     */
    @ApiModelProperty("审核医生")
    private String reportDoctor;

    /**
     * 是否是职业项目
     */
    @ApiModelProperty("是否是职业项目")
    private String typeZ;

    /**
     * 是否是健康项目
     */
    @ApiModelProperty("是否是健康项目")
    private String typeJ;


    @ApiModelProperty("类型")
    private String type;


    @TableField(exist=false)
    @ApiModelProperty("组合项目类型")
    private String groupType;

    /**
     * 条码
     */
    @ApiModelProperty("条码")
    private String barcode;

    /**
     * 收费
     */
    @ApiModelProperty("收费")
    private BigDecimal price;

    /**
     * 折扣
     */
    @ApiModelProperty("折扣")
    private BigDecimal discount;

    /**
     * 优惠后价格
     */
    @ApiModelProperty("优惠后价格")
    private BigDecimal cost;

    /**
     * 是否附加项目
     */
    @ApiModelProperty("是否附加项目")
    private String addition;

    /**
     * 付费方式 0自费 1统收
     */
    @ApiModelProperty("付费方式 0自费 1统收")
    private String payType;

    /**
     * 发票id
     */
    @ApiModelProperty("发票id")
    private Long invoiceId;

    /**
     * 收费时间
     */
    @ApiModelProperty("收费时间")
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date chargeDate;

    /**
     * 是否选检项目
     */
    @ApiModelProperty("是否选检项目")
    private String choose;

    /**
     * 添加项目的时间
     */
    @ApiModelProperty("添加项目的时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date addTime;

    /**
     * 复查
     */
    @ApiModelProperty("复查")
    private Integer review;

    /**
     * 项目登记医生
     */
    @ApiModelProperty("项目登记医生")
    private String registerDoc;

    /**
     * 修改小结医生-废弃
     */
    @Deprecated
    @ApiModelProperty("修改小结医生-废弃")
    private String updateResultDoc;

    /**
     * 修改小结时间-废弃
     */
    @Deprecated
    @ApiModelProperty("修改小结时间-废弃")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateResultDate;

    /**
     * 条码名称
     */
    @ApiModelProperty("条码名称")
    private String barcodeName;

    /**
     * His收费主表的Guid
     */
    @ApiModelProperty("His收费主表的Guid")
    private Long hisFheadId;

    /**
     * 减免标志
     */
    @ApiModelProperty("减免标志")
    private String free;

    /**
     * 上机状态
     */
    @ApiModelProperty("上机状态")
    private String practice;

    /**
     * 登记状态
     */
    @ApiModelProperty("登记状态")
    private String registerState;

    /**
     * 是否退费
     */
    @ApiModelProperty("是否退费")
    private String chargeReturn;

    /**
     * 平台ID
     */
    @ApiModelProperty("平台ID")
    private Long platformId;

    /**
     * 是否下载平台文件
     */
    @ApiModelProperty("是否下载平台文件")
    private String isDownload;

    /**
     * 体检收费单头表主健ID
     */
    @ApiModelProperty("体检收费单头表主健ID")
    private Long fheadId;

    /**
     * 医嘱ID,HIS申请的唯一标识
     */
    @ApiModelProperty("医嘱ID,HIS申请的唯一标识")
    private Long hisDoctorId;

    /**
     * 单据号,HIS缴费所需的唯一标识
     */
    @ApiModelProperty("单据号,HIS缴费所需的唯一标识识")
    private Integer hisNo;

    /**
     * 最后更新时间
     */
    @ApiModelProperty("最后更新时间")
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(fill = FieldFill.UPDATE)
    private Timestamp updateTime;

    /**
     * 套餐ID
     */
    @ApiModelProperty("套餐ID")
    private String packageId;

    /**
     * 套餐名
     */
    @ApiModelProperty("套餐名")
    private String packageName;

    /**
     * 分组id
     */
    @ApiModelProperty("分组id")
    private Long deptGroupHdId;

    /**
     * 医疗机构ID(U_Hospital_Info的ID)
     */
    @ApiModelProperty("医疗机构ID(U_Hospital_Info的ID)")
    private String tenantId;

    /**
     * 项目类型id
     */
    @ApiModelProperty(value = "项目类型id")
    @TableField(exist = false)
    private String itemTypeId;
}
