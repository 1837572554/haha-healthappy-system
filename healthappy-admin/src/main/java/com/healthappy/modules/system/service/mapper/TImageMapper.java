package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TImage;
import com.healthappy.modules.system.service.dto.TPatientImageDto;
import com.healthappy.modules.system.service.dto.TPatientImgDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Jevany
 * @date 2021/11/30 17:07
 * @desc 图片Mapper
 */
@Mapper
@Repository
public interface TImageMapper extends CoreMapper<TImage> {

    // @SqlParser(filter = true)
    List<TPatientImageDto> getPatientImage(@Param("imgDto") TPatientImgDto tPatientImgDto);
}
