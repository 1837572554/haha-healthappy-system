package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BDeptQueryCriteria {

    @ApiModelProperty(name = "name",value = "科室名称(简拼)|模糊")
    @Query(blurry = "name,jp")
    private String name;

    @Query
    @ApiModelProperty("是否启用")
    private Boolean isEnable = true;

    @Query
    @ApiModelProperty("科室ID")
    private String id;

    @Query
    @ApiModelProperty("是否删除 空不限制,0：未删除，1：删除。默认0")
    private String delFlag="0";
}
