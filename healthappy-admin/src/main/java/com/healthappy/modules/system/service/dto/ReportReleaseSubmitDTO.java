package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Set;

/**
 * 报告发布提交（新增）操作对象
 * @author Jevany
 * @date 2022/3/3 19:01
 */
@Data
@ApiModel("报告发布提交（新增）操作对象")
public class ReportReleaseSubmitDTO  implements Serializable {
    /** 发布（操作）类型 */
    @NotBlank(message = "发布（操作）类型不能为空")
    @ApiModelProperty("发布（操作）类型,使用字典：reportReleaseType（体检报告发布类型）-数字型字符串")
    private String releaseType;

    /** 流水号 */
    @ApiModelProperty("流水号")
    private Set<String> paIdList;

    /** 报告类型 */
    @ApiModelProperty("报告类型，文本内容")
    private String reportType;
}