package com.healthappy.modules.system.rest.manage;

import com.healthappy.annotation.AnonymousAccess;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.vo.BatchInfoEditVO;
import com.healthappy.modules.system.service.TPatientService;
import com.healthappy.modules.system.service.dto.BatchInfoEditQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 批量信息修改-控制器
 * @author Jevany
 * @date 2022/5/12 22:09
 */
@Slf4j
@Api(tags = "体检管理：批量信息修改")
@RestController
@AllArgsConstructor
@RequestMapping("/batchMessageModify")
public class BatchInfoEditController {
	/** 体检人员服务 */
	private final TPatientService tPatientService;

	@Log("批量信息修改-列表查询")
	@ApiOperation("批量信息修改-列表查询，权限码：batchMessageModify:list")
	@GetMapping("/list")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "批量修改信息数据", response = BatchInfoEditVO.class, responseContainer = "Map")})
	@PreAuthorize("@el.check('batchMessageModify:list')")
	public R list(BatchInfoEditQueryCriteria criteria, Pageable pageable) {
		return R.ok(tPatientService.mapBatchInfoEdit(criteria, pageable));
	}

	@Log("批量信息修改-信息修改")
	@ApiOperation("批量信息修改-信息修改，权限码：batchMessageModify:list")
	@PostMapping("/edit")
	@PreAuthorize("@el.check('batchMessageModify:list')")
	public R edit(@RequestBody List<BatchInfoEditVO> batchInfoEditVOList) {
		tPatientService.batchInfoEdit(batchInfoEditVOList);
		return R.ok();
	}

	@Log("Test")
	@ApiOperation("Test")
	@AnonymousAccess
	@GetMapping("/test")
	public R test() {
		return R.ok();
	}

}
