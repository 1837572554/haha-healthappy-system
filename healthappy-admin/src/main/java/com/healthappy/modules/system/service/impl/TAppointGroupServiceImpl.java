package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.*;
import com.healthappy.modules.system.service.*;
import com.healthappy.modules.system.service.mapper.TAppointGroupMapper;
import com.healthappy.utils.SecurityUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author sjc
 * @description 预约组合项目记录表
 * @date 2021-09-14
 */
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
class TAppointGroupServiceImpl extends BaseServiceImpl<TAppointGroupMapper, TAppointGroup> implements TAppointGroupService {

    private final BDepartmentGroupHDService departmentGroupHDService;
    private final BDepartmentGroupTDService departmentGroupTDService;
    private final BPackageDTService bPackageDTService;
    private final BPoisonService poisonService;
    private final BGroupHdService bGroupHdService;

    @Override
    public void repeatGroupId(List<String> appointIds) {
        baseMapper.repeatGroupId(appointIds, SecurityUtils.getTenantId());
    }

    @Override
    public TAppointGroup buildTAppointGroup(String id, String groupId, BigDecimal price, Integer payType, BigDecimal discount, BigDecimal cost) {
        TAppointGroup group = this.calculateEachPrice(price, discount, cost);
        TAppointGroup TAppointGroup = new TAppointGroup();
        TAppointGroup.setAppointId(id);
        TAppointGroup.setGroupId(groupId);
        TAppointGroup.setPrice(group.getPrice());
        TAppointGroup.setPayType(payType);
        TAppointGroup.setDiscount(group.getDiscount());
        TAppointGroup.setCost(group.getCost());
        return TAppointGroup;
    }

    @Override
    public TAppointGroup calculateEachPrice(BigDecimal price, BigDecimal discount, BigDecimal cost) {
        if (ObjectUtil.isNull(price)) {
            price = new BigDecimal(0);
        }
        if (ObjectUtil.isNull(discount)) {
            discount = ObjectUtil.isNotNull(price) && !NumberUtil.equals(price, new BigDecimal(0)) && ObjectUtil.isNull(discount) ? NumberUtil.div(cost, price) : new BigDecimal(1);
        }
        if (ObjectUtil.isNull(cost)) {
            cost = NumberUtil.round(NumberUtil.mul(price, discount), 2);
        }
        TAppointGroup group = new TAppointGroup();
        group.setPrice(price);
        group.setDiscount(discount);
        group.setCost(cost);
        return group;
    }

    @Override
    public void saveAppointGroup(TAppoint v, List<BDepartmentGroupHD> bDepartmentGroupHDList) {
        //缴费方式 0自费 1统收 默认自费
        Integer[] offs = new Integer[1];
        offs[0] = 0;
        if (ObjectUtil.isNotEmpty(v.getDeptGroupId())) {
            BDepartmentGroupHD departmentGroupHD = null;
            if (CollUtil.isNotEmpty(bDepartmentGroupHDList)) {
                departmentGroupHD = bDepartmentGroupHDList.stream().filter(e -> e.getId().equals(v.getDeptGroupId())).findFirst().orElse(null);
            }
            if (null != departmentGroupHD) {
                departmentGroupHD = departmentGroupHDService.getById(v.getDeptGroupId());
            }
            if (null != departmentGroupHD) {
                offs[0] = Convert.toInt(departmentGroupHD.getSffs());
            }
        }
        List<TAppointGroup> groupList = new ArrayList<>();
        if (StrUtil.isNotBlank(v.getPackageId())) {
            groupList = bPackageDTService.lambdaQuery().eq(BPackageDT::getPackageId, v.getPackageId()).list().stream().map(p -> this.buildTAppointGroup(v.getId(), p.getGroupId(), p.getPrice(), offs[0], p.getDiscount(), p.getCost())).collect(Collectors.toList());
        }
        if (CollUtil.isEmpty(groupList) && StrUtil.isNotBlank(v.getDeptGroupId())) {
            groupList = departmentGroupTDService.lambdaQuery().eq(BDepartmentGroupTD::getDepartmentGroupHdId, v.getDeptGroupId()).list()
                    .stream().map(p ->
                            this.buildTAppointGroup(v.getId(), p.getGroupId(), Convert.toBigDecimal(p.getPrice()), offs[0], Convert.toBigDecimal(p.getDiscount()), Convert.toBigDecimal(p.getCost())))
                    .collect(Collectors.toList());
        }
        if (CollUtil.isEmpty(groupList) && StrUtil.isNotBlank(v.getPoisonType())) {
            groupList = poisonService.getPackageList(Stream.of(v.getPoisonType().split("、")).collect(Collectors.toList()), v.getType())
                    .stream().map(p -> this.buildTAppointGroup(v.getId(), p.getGroupId(), p.getPrice(), offs[0], p.getDiscount(), p.getCost())).collect(Collectors.toList());
        }

        if (StrUtil.isNotBlank(v.getAddGroupItemId())) {
            groupList.addAll(bGroupHdService.lambdaQuery().in(BGroupHd::getId, Stream.of(v.getAddGroupItemId().split("、")).collect(Collectors.toList()))
                    .list().stream()
                    .map(p -> this.buildTAppointGroup(v.getId(), p.getId(), p.getPrice(), offs[0], new BigDecimal(1), p.getCost())).collect(Collectors.toList()));
        }
        if (ObjectUtil.isNotNull(v.getAddPackageId())) {
            groupList.addAll(bPackageDTService.lambdaQuery().eq(BPackageDT::getPackageId, v.getAddPackageId())
                    .list().stream()
                    .map(p -> this.buildTAppointGroup(v.getId(), p.getGroupId(), p.getPrice(), offs[0], p.getDiscount(), p.getCost())).collect(Collectors.toList()));
        }
        if (ObjectUtil.isNotNull(v.getSubtractGroupItem())) {
            groupList = groupList.stream().filter(g -> bGroupHdService.lambdaQuery().in(BGroupHd::getId, Stream.of(v.getSubtractGroupItem().split("、")).collect(Collectors.toList()))
                    .list().stream().noneMatch(i -> g.getGroupId().equals(i.getId()))).collect(Collectors.toList());
        }
        this.remove(Wrappers.<TAppointGroup>lambdaUpdate().eq(TAppointGroup::getAppointId,v.getId()));
        if (CollUtil.isNotEmpty(groupList)) {
            groupList.stream().distinct().forEach(this::save);
        }
        this.repeatGroupId(Collections.singletonList(v.getId()));
    }
}
