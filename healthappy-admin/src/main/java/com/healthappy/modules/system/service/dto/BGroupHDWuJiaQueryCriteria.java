package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @description 组合项目物价表
 * @author SJC
 * @date 2021-08-3
 */
@Data
public class BGroupHDWuJiaQueryCriteria  {


    /**
     *主键id
     */
    @ApiModelProperty("主键id")
    @Query(type = Query.Type.EQUAL)
    private Long id;


    /**
     * 组合项目ID
     */
    @ApiModelProperty("组合项目ID")
    @Query(type = Query.Type.EQUAL)
    private String itemGroupId;

    /**
     * 物价名称
     */
    @ApiModelProperty("物价名称")
    @Query(type = Query.Type.INNER_LIKE)
    private String itemCnname;


    /**
     * 价格
     */
    @ApiModelProperty("价格")
    @Query(type = Query.Type.BETWEEN)
    private List<Double> price;

}
