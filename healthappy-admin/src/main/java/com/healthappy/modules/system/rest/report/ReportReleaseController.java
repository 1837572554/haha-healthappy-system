package com.healthappy.modules.system.rest.report;

import cn.hutool.core.util.StrUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.TReportRecord;
import com.healthappy.modules.system.service.TPatientService;
import com.healthappy.modules.system.service.TReportRecordService;
import com.healthappy.modules.system.service.dto.ReportReleaseQueryCriteria;
import com.healthappy.modules.system.service.dto.ReportReleaseSubmitDTO;
import com.healthappy.modules.system.service.dto.ReportReleaseUnSubmitDTO;
import com.healthappy.modules.system.service.dto.reportReleaseReceiveDTO;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 体检报告发布 控制器
 *
 * @author Jevany
 * @date 2022/3/3 11:57
 */
@Slf4j
@Api(tags = "体检流程：体检报告发布")
@RestController
@AllArgsConstructor
@RequestMapping("/reportRelease")
public class ReportReleaseController {
    /**
     * 体检者信息服务层
     */
    private final TPatientService tPatientService;

    private final TReportRecordService reportRecordService;

    @Log("列表")
    @ApiOperation("列表，权限码：ReportRelease:list")
    @GetMapping
    @PreAuthorize("@el.check('ReportRelease:list')")
    public R list(@Validated ReportReleaseQueryCriteria criteria, Pageable pageable) {
        return R.ok(tPatientService.listReportRelease(criteria, pageable));
    }

    @Log("提交-预览、导出、打印、上传")
    @ApiOperation("提交-预览、导出、打印、上传，权限码：ReportRelease:list")
    @PostMapping("/reportReleaseSubmit")
    @PreAuthorize("@el.check('ReportRelease:list')")
    @Klock
    public R reportReleaseSubmit(ReportReleaseSubmitDTO submitDTO) {
        reportRecordService.reportRecordAdd(submitDTO);
        return R.ok();
    }

    @Log("撤销-打印、上传")
    @ApiOperation("撤销-打印、上传，权限码：ReportRelease:list")
    @PostMapping("/reportReleaseUnSubmit")
    @PreAuthorize("@el.check('ReportRelease:list')")
    @Klock
    public R reportReleaseUnSubmit(ReportReleaseUnSubmitDTO unSubmitDTO) {
        reportRecordService.reportReleaseUndoSubmit(unSubmitDTO);
        return R.ok();
    }


    @Log("领取报告")
    @ApiOperation("领取报告，权限码：ReportRelease:list")
    @PostMapping("/reportReceive")
    @PreAuthorize("@el.check('ReportRelease:list')")
    @Klock
    public R reportReceive(reportReleaseReceiveDTO receiveDTO){
        reportRecordService.reportRecordReceive(receiveDTO);
        return R.ok();
    }

    @Log("撤销-报告领取")
    @ApiOperation("撤销-报告领取，权限码：ReportRelease:list")
    @PostMapping("/reportUndoReceive")
    @PreAuthorize("@el.check('ReportRelease:list')")
    @Klock
    public R reportRecordUndoReceive(reportReleaseReceiveDTO receiveDTO){
        reportRecordService.reportRecordUndoReceive(receiveDTO);
        return R.ok();
    }

    @Log("获得体检报告发布记录")
    @ApiOperation("获得体检报告发布记录，权限码：ReportRelease:list")
    @PostMapping("/listReportRelease")
    @PreAuthorize("@el.check('ReportRelease:list')")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "报告发布记录",response = TReportRecord.class)
    })
    public R listReportRelease(@RequestParam String paId, Pageable pageable){
        if(StrUtil.isBlank(paId)){
            return R.error("流水号不能为空");
        }
        return R.ok(reportRecordService.listReportRecord(paId,pageable));
    }

}
