package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class BAreaVo {
    /**
     * id
     */
    private Long id;

    /**
     * 编码
     */
    @NotBlank(message = "区域编码不能为空")
    private String code;

    /**
     * 区域名称
     */
    @NotBlank(message = "区域名称不能为空")
    private String name;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 是否启用
     */
    private String isEnable;

    /**
     * 排序
     */
    private Integer dispOrder;

    @ApiModelProperty("上级区域编码")
    private String hiCode;
}
