package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 项目小结
 * @author yanjun
 * @description 项目小结
 * @date 2021/11/10
 */
@Data
public class ItemSicknessDescDto implements Serializable {
    /**
     * 小结
     */
    @ApiModelProperty("小结")
    private String desc;
    /**
     * 判定
     */
    @ApiModelProperty("判定")
    private String Mark;

    /**
     * 病种规则匹配后得到的病种结果集
     */
    @ApiModelProperty("病种规则匹配后得到的病种结果集")
    private List<ItemSicknessMatchDto> itemSicknessMatchDtoList;
}
