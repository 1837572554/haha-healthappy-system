package com.healthappy.modules.system.rest.system;

import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.vo.BBarcodeVo;
import com.healthappy.modules.system.service.BBarcodeService;
import com.healthappy.modules.system.service.dto.BBarcodeQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 条码设置表
 * @author sjc
 * @date 2021-12-13
 */
@Api(tags = "系统：条码管理")
@RestController
@AllArgsConstructor
@RequestMapping("/BBarcode")
public class BBarcodeController {

    private final BBarcodeService bBarcodeService;


    @Log("查询条码设置")
    @ApiOperation("查询条码设置")
    @GetMapping
    public R list(BBarcodeQueryCriteria criteria) {
        return R.ok(bBarcodeService.queryAll(criteria));
    }

    @Log("新增|修改条码设置")
    @ApiOperation("新增|修改条码设置，权限码：admin")
    @PostMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R saveOrUpdate(@RequestBody BBarcodeVo bBarcodeVo) {
        return R.ok(bBarcodeService.saveOrUpdate(bBarcodeVo));
    }


    @Log("删除条码设置")
    @ApiOperation("删除条码设置，权限码：admin")
    @DeleteMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R remove(@RequestBody Set<Integer> ids) {
        return R.ok(bBarcodeService.removeByIds(ids));
    }
}
