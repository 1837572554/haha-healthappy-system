package com.healthappy.modules.system.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * @Author: FGQ
 * @Desc:
 * @Date: Created in 16:30 2022/3/7
 */
@Data
public class IsSubmitDto implements Serializable {

    /**
     * 主键id
     */
    private String id;

    /**
     * 姓名
     */
    @ApiModelProperty("姓名")
    private String name;

    /**
     * 性别
     */
    @ApiModelProperty("性别")
    private String sexText;

    /**
     * 年龄
     */
    @ApiModelProperty("年龄")
    private Integer age;

    /**
     * 电话
     */
    @ApiModelProperty("电话")
    private String phone;

    /**
     * 指引单是否交回 0否1是
     */
    @ApiModelProperty("指引单是否交回")
    private String isSubmit;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty("指引单回交时间")
    private Timestamp isSubmitTime;

    @ApiModelProperty("指引单操作人员")
    private String isSubmitBy;

    @ApiModelProperty("回单项目")
    private List<Item> itemList;

    @Data
    public static class Item {

        @ApiModelProperty("主键")
        private String id;

        @ApiModelProperty("组合项目")
        private String groupName;

        @ApiModelProperty("正常")
        private Boolean resultMark;

        @ApiModelProperty("操作医生")
        private String doctor;

        @ApiModelProperty("检查日期")
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
        private Date resultDate;

        @ApiModelProperty("是否弃检")
        private String result;
    }
}
