package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @description 组合项目子项
 * @author sjc
 * @date 2021-08-4
 */
@Data
public class BGroupDTVo  {


    /**
     * 组合项目id
     */
    @NotNull(message = "组合项目id不能为空")
    @ApiModelProperty("组合项目id")
    private String groupId;

    /**
     * 项目编号
     */
    @ApiModelProperty("项目编号")
    private String itemId;


}
