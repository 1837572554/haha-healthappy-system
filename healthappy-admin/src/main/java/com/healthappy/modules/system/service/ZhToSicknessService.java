package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.ZhToSickness;
import com.healthappy.modules.system.domain.vo.ZhToSicknessVo;
import com.healthappy.modules.system.service.dto.ZhToSicknessQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 组合判断设置 服务层
 * @author sjc
 * @date 2021-08-2
 */
public interface ZhToSicknessService extends BaseService<ZhToSickness> {
    Map<String, Object> queryAll(ZhToSicknessQueryCriteria criteria, Pageable pageable);

    List<ZhToSickness> queryAll(ZhToSicknessQueryCriteria criteria);

    boolean deleteById(Long id);

    void saveOrUpdateZhToSickness(ZhToSicknessVo resources);
}
