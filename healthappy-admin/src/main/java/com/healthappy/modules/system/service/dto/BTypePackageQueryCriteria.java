package com.healthappy.modules.system.service.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 套餐类型
 * @author sjc
 * @date 2021-08-24
 */
@Data
public class BTypePackageQueryCriteria  {


    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    @Query(type = Query.Type.EQUAL)
    private Long id;



    /**
     * 名称
     */
    @ApiModelProperty("名称")
    @Query(type = Query.Type.INNER_LIKE)
    private String name;

}
