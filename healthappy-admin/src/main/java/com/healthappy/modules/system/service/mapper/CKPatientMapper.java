package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.modules.system.service.dto.QueryCustomDTO;
import com.healthappy.modules.system.service.dto.QueryDataCriteria;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CKPatientMapper {

    @SqlParser(filter = true)
    List<QueryCustomDTO> getQueryAll(@Param("criteria") QueryDataCriteria criteria,@Param("tenantId") String tenantId);
}
