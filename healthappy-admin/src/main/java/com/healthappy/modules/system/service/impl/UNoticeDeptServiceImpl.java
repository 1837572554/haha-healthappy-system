package com.healthappy.modules.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.UNoticeDept;
import com.healthappy.modules.system.service.UNoticeDeptService;
import com.healthappy.modules.system.service.dto.UNoticeDeptQueryCriteria;
import com.healthappy.modules.system.service.mapper.UNoticeDeptMapper;
import com.healthappy.utils.CacheConstant;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * @description 公告对应科室表
 * @author sjc
 * @date 2021-09-3
 */
@Service
@CacheConfig(cacheNames = {CacheConstant.U_NOTICE_DEPT})
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class UNoticeDeptServiceImpl extends BaseServiceImpl<UNoticeDeptMapper, UNoticeDept> implements UNoticeDeptService {

    private final IGenerator generator;

    @Override
    @Cacheable
    public Map<String, Object> queryAll(UNoticeDeptQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<UNoticeDept> page = new PageInfo<UNoticeDept>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", generator.convert(page.getList(), UNoticeDept.class));
        map.put("totalElements", page.getTotal());
        return map;
    }

    @Override
    @Cacheable
    public List<UNoticeDept> queryAll(UNoticeDeptQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(UNoticeDept.class, criteria));
    }



    @Override
    @CacheEvict(allEntries = true)
    public boolean deleteByUnoticeId(String id) {
        LambdaQueryWrapper<UNoticeDept> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(UNoticeDept::getNoticeId,id);

        return baseMapper.delete(lambdaQueryWrapper)>0;
    }
}
