package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 病种名单列表
 * @author YJ
 * @date 2022/1/24 17:14
 */

@ApiModel("病种名单列表")
@Data
public class SicknessConclusionListDto implements Serializable {
    /**
     * 病种名
     */
    @ApiModelProperty(value = "病种名")
    private String sicknissName;

    /**
     * 体检号
     */
    @ApiModelProperty(value = "体检号")
    private String paId;

    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名")
    private String name;

    /**
     * 性别
     */
    @ApiModelProperty(value = "性别")
    private String sex;

    /**
     * 年龄
     */
    @ApiModelProperty(value = "年龄")
    private Integer age;

    /**
     * 身份证
     */
    @ApiModelProperty(value = "身份证")
    private String idNo;

    /**
     * 电话
     */
    @ApiModelProperty(value = "电话")
    private String phone;

    /**
     * 单位
     */
    @ApiModelProperty(value = "单位")
    private String companyName;

    /**
     * 部门
     */
    @ApiModelProperty(value = "部门")
    private String departmentName;

    /**
     * 分组
     */
    @ApiModelProperty(value = "分组")
    private String departmentGroupName;

    /**
     * 体检日期
     */
    @ApiModelProperty(value = "体检日期")
    private Date peDate;

    /**
     * 签到日期
     */
    @ApiModelProperty(value = "签到日期")
    private Date signDate;

    private static final long serialVersionUID = 1L;
}
