package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BReportConfig;

/**
 * 报告配置 服务类
 * @author Jevany
 * @date 2022/4/11 15:20
 */
public interface BReportConfigService extends BaseService<BReportConfig> {
    /**
     * 获得报告配置
     * @author YJ
     * @date 2022/4/11 15:32
     * @param reportName 报告模板名称
     * @param peTypeGroupId 体检大类编号
     * @param reportType 报告类型，1个人报告 2单位报告
     * @return com.healthappy.modules.system.domain.BReportConfig
     */
    BReportConfig getBReportConfig(String reportName,Integer peTypeGroupId,Integer reportType);
}