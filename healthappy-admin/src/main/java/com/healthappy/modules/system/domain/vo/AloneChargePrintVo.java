package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.*;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * 票据打印对象
 * @author Jevany
 * @date 2022/4/15 18:53
 */
@ApiModel("票据打印对象")
@Data
public class AloneChargePrintVo {
    /** 流水号 */
    @ApiModelProperty("流水号")
    private String paId;

    /** 姓名 */
    @ApiModelProperty("姓名")
    private String name;

    /** 票据号 */
    @ApiModelProperty("票据号")
    private String invoice;

    /** 性别 */
    @ApiModelProperty("性别")
    private String sex;

    /** 费别 */
    @ApiModelProperty("费别")
    private String payType;

    /** 民族 */
    @ApiModelProperty("民族")
    private String ssNo;

    /** 费用名称 */
    @ApiModelProperty("费用名称")
    private String feeName;

    /** 费用 */
    @ApiModelProperty("费用")
    private String fee;

    /** 大写 */
    @ApiModelProperty("pay")
    private String pay;

    /** 实收 */
    @ApiModelProperty("实收")
    private String cost;

    /** 医生 */
    @ApiModelProperty("医生")
    private String doctor;

    /** 费用日期 */
    @ApiModelProperty("费用日期")
    private String payDate;

    /** 导出文件类型 1=pdf 2=word */
    @ApiModelProperty("导出文件类型 1=pdf 2=word")
    private Integer fileType;
}
