package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TAppointGroup;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TAppointGroupMapper extends CoreMapper<TAppointGroup> {

    /**
     * 清除重复项目
     * @param appointIds
     */
    @SqlParser(filter = true)
    void repeatGroupId(@Param("appointIds") List<String> appointIds,@Param("tenantId") String tenantId);
}

