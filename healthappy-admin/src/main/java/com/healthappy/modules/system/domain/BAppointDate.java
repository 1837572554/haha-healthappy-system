package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.config.SeedIdGenerator;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/** 预约号源表  对象*/
@TableName("B_Appoint_Date")
@Data
@SeedIdGenerator
public class BAppointDate implements Serializable {
	/**
	 * 预约日期编号
	 */
	@TableId(type = IdType.INPUT)
	@ApiModelProperty(value = "预约日期编号")
	private String id;

	/**
	 * 预约日期
	 */
	@ApiModelProperty(value = "预约日期")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	private Date date;

	/** 总数 */
	@ApiModelProperty("总数")
	private Integer totalNum;

	/**
	 * 租户编号
	 */
	@ApiModelProperty(value = "租户编号")
	@TableField(value = "tenant_id", fill = FieldFill.INSERT)
	private String tenantId;

	private static final long serialVersionUID = 1L;
}
