package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * @description 职业体检人员表
 * @author sjc
 * @date 2021-09-8
 */
@Data
public class TPatientZQueryCriteria  {

    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("体检号")
    private String paId;
}
