package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 单位报告编制子表（绑定人员）
 * @author YJ
 * @date 2022/2/17 15:28
 */
@Data
@TableName("T_COMPANY_REP_DT")
@RenewLog
public class TCompanyRepDt implements Serializable {
    /**
     * 报告编号
     */
    @ApiModelProperty(value = "报告编号")
    private String repId;

    /**
     * 体检号
     */
    @ApiModelProperty(value = "体检号")
    private String paId;

    /**
     * 租户编号
     */
    @ApiModelProperty(value = "租户编号")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;

    private static final long serialVersionUID = 1L;
}
