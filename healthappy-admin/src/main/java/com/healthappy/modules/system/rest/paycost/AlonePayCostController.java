package com.healthappy.modules.system.rest.paycost;

import com.healthappy.modules.system.domain.vo.AloneChargeMakeBillVo;
import com.healthappy.modules.system.domain.vo.AloneChargeVo;
import com.healthappy.modules.system.domain.vo.AloneTicketVo;
import com.healthappy.modules.system.service.AlonePayCostService;
import com.healthappy.modules.system.service.dto.PayCostAloneQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 个人缴费 管理
 * @author FGQ
 */
@Api(tags = "缴费管理：个人缴费")
@RestController
@RequiredArgsConstructor
@RequestMapping("/paycost/alone")
public class AlonePayCostController {

    private  final AlonePayCostService alonePayCostService;


    @ApiOperation("列表，权限码：paycost:alone:list")
    @GetMapping
    @PreAuthorize("@el.check('paycost:alone:list')")
    public R getAll(@Validated PayCostAloneQueryCriteria criteria) {
        return R.ok(alonePayCostService.getAll(criteria));
    }

    @ApiOperation("详情，权限码：paycost:alone:list")
    @GetMapping("/detail/{paId}")
    @PreAuthorize("@el.check('paycost:alone:list')")
    public R detail(@PathVariable String paId) {
        return R.ok(alonePayCostService.detail(paId));
    }

    @ApiOperation("检查/获取 票据号：paycost:alone:list")
    @GetMapping("/detail/ticket")
    @PreAuthorize("@el.check('paycost:alone:list')")
    public R checkTicket(@Validated AloneTicketVo vo) {
        return R.ok(alonePayCostService.checkTicket(vo));
    }


    @ApiOperation("收费 / 退费 ：paycost:alone:upd")
    @GetMapping("/charge")
    @PreAuthorize("@el.check('paycost:alone:upd')")
    @Klock
    public R charge(@Validated AloneChargeVo vo) {
        alonePayCostService.charge(vo);
        return R.ok();
    }

    @ApiOperation("补打票据：paycost:alone:upd")
    @GetMapping("/makeBill")
    @PreAuthorize("@el.check('paycost:alone:upd')")
    @Klock
    public R makeBill(AloneChargeMakeBillVo vo) {
        alonePayCostService.makeBill(vo);
        return R.ok();
    }

    @ApiOperation("保存 票据号：paycost:alone:upd")
    @GetMapping("/add/ticket")
    @PreAuthorize("@el.check('paycost:alone:upd')")
    @Klock
    public R addCurrentTicket(@Validated AloneTicketVo vo) {
        return R.ok(alonePayCostService.addCurrentTicket(vo));
    }
}
