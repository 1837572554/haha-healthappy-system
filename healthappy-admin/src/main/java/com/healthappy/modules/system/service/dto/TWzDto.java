package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @description 问诊表
 * @author sjc
 * @date 2021-12-15
 */
@Data
public class TWzDto implements Serializable {

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private Long id;


    /**
     * 体检号
     */
    @NotNull(message = "体检号不能为空")
    @ApiModelProperty("体检号")
    private String paId;

    /**
     * 既往病史
     */
    @ApiModelProperty("既往病史")
    private String bsJwbs;

    /**
     * 病名
     */
    @ApiModelProperty("病名")
    private String bsBm;

    /**
     * 诊断时间
     */
    @ApiModelProperty("诊断时间")
    private String bsZdsj;

    /**
     * 诊断单位
     */
    @ApiModelProperty("诊断单位")
    private String bsZddw;

    /**
     * 是否痊愈
     */
    @ApiModelProperty("是否痊愈")
    private String bsSfqy;

    /**
     * 初潮
     */
    @ApiModelProperty("初潮")
    private String bsCc;

    /**
     * 经期
     */
    @ApiModelProperty("经期")
    private String bsJq;

    /**
     * 周期
     */
    @ApiModelProperty("周期")
    private String bsZq;

    /**
     * 末次月经
     */
    @ApiModelProperty("末次月经")
    private String bsMcyj;

    /**
     * 现有子女
     */
    @ApiModelProperty("现有子女")
    private String bsXyzn;

    /**
     * 流产
     */
    @ApiModelProperty("流产")
    private String bsLc;

    /**
     * 早产
     */
    @ApiModelProperty("早产")
    private String bsZc;

    /**
     * 死产
     */
    @ApiModelProperty("死产")
    private String bsSc;

    /**
     * 异常胎
     */
    @ApiModelProperty("异常胎")
    private String bsYct;


    /**
     * 烟史
     */
    @ApiModelProperty("烟史")
    private String bsYs;

    /**
     * 烟史-烟龄
     */
    @ApiModelProperty("烟史-烟龄，每年吸的数量")
    private String ysYl;

    /**
     * 烟史-每天吸的数量
     */
    @ApiModelProperty("烟史-每天吸的数量")
    private String ysNs;


    /**
     * 酒史
     */
    @ApiModelProperty("酒史")
    private String bsJs;



    /**
     * 酒史-酒龄,每年喝的数量
     */
    @ApiModelProperty("酒史-酒龄,每年喝的数量")
    private String jsJl;

    /**
     * 酒史-每天喝的数量
     */
    @ApiModelProperty("酒史-每天喝的数量")
    private String jsNs;

    /**
     * 其他
     */
    @ApiModelProperty("其他")
    private String bsQt;

    /**
     * 症状
     */
    @ApiModelProperty("症状")
    private String zz;

    /**
     * 操作医生
     */
    @ApiModelProperty("操作医生")
    private String doctor;

}
