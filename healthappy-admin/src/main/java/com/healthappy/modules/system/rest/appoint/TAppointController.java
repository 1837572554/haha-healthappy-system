package com.healthappy.modules.system.rest.appoint;

import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.TAppointGroup;
import com.healthappy.modules.system.domain.vo.TAppointVo;
import com.healthappy.modules.system.service.TAppointGroupService;
import com.healthappy.modules.system.service.TAppointService;
import com.healthappy.modules.system.service.dto.TAppointDto;
import com.healthappy.modules.system.service.dto.TAppointPrintDto;
import com.healthappy.modules.system.service.dto.TAppointQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * @author sjc
 * @description 预约表 控制器
 * @date 2021-06-28
 */
@Slf4j
@Api(tags = "预约管理：预约管理")
@RestController
@AllArgsConstructor
@RequestMapping("/TAppoint")
public class TAppointController {

  private final TAppointService tAppointService;
  private final TAppointGroupService tAppointGroupService;

  @Log("查询预约库全部数据")
  @ApiOperation("查询预约库全部数据，权限码：TAppoint:list")
  @GetMapping
  @PreAuthorize("@el.check('TAppoint:list')")
  @ApiResponses(value = {
      @ApiResponse(code = 200,message = "预约数据",response = TAppointDto.class)
  })
  public R list(TAppointQueryCriteria criteria, Pageable pageable) {
    return R.ok(tAppointService.queryAll(criteria, pageable));
  }

  @Log("预约签到")
  @ApiOperation("预约签到，权限码：TAppoint:sign")
  @GetMapping("/sign")
  @PreAuthorize("@el.check('TAppoint:sign')")
  public R sign(@RequestParam String id) {
    return R.ok(tAppointService.sign(id));
  }

  @Log("导出数据为Excel文件")
  @ApiOperation("导出数据，权限码：TAppoint:list")
  @GetMapping(value = "/download/{id}")
  @PreAuthorize("@el.check('TAppoint:list')")
  @Klock
  public void download(HttpServletResponse response,
      @ApiParam(value = "需要导出的数据的id（多个使用英文逗号连接）",required = true)
      @PathVariable String id) throws IOException, IllegalAccessException {
    tAppointService.download(id, response);
  }

  @Log("新增|修改预约库")
  @ApiOperation("新增|修改预约库，权限码：TAppoint:add")
  @PostMapping
  @PreAuthorize("@el.check('TAppoint:add')")
  @Klock
  public R saveOrUpdate(@Validated @RequestBody TAppointVo resources) {
    if (tAppointService.saveOrUpdate(resources)) {
      return R.ok();
    }
    return R.error(500, "方法异常");
  }

  @Log("上传Excel文件,读取数据并返回 目前只有预约")
  @PostMapping("/upload")
  @ApiOperation("上传Excel文件,读取数据并返回，权限码：TAppoint:upload")
  @PreAuthorize("@el.check('pictures:add:upload')")
  @Klock
  public R upload(@RequestParam MultipartFile file, @RequestParam("type") String type) {
    return R.ok(tAppointService.upload(file));
  }

  @Log("导入，将Excel文件读取出来的内容保存到预约表中")
  @ApiOperation("导入，将Excel文件读取出来的内容保存到预约表中，权限码：TAppoint:import")
  @GetMapping(value = "/importAppoint")
  @PreAuthorize("@el.check('TAppoint:import')")
  @Klock
  public void importAppoint(List<TAppointVo> list) {
    tAppointService.importAppoint(list);
  }

  @Log("删除预约库")
  @ApiOperation("删除预约库，权限码：TAppoint:delete")
  @DeleteMapping
  @PreAuthorize("@el.check('TAppoint:delete')")
  @Klock
  public R remove(@RequestBody Set<Integer> ids) {
    //删除 预约组合项目记录
    tAppointGroupService.lambdaUpdate().in(TAppointGroup::getAppointId,ids).remove();
    tAppointService.removeByIds(ids);
    return R.ok();
  }

  @Log("预约管理打印")
  @ApiOperation("预约管理打印，权限码：TAppoint:detail")
  @GetMapping(value = "/print")
  @PreAuthorize("@el.check('TAppoint:detail')")
  @Klock
  public ModelAndView print(@RequestParam("id")
  @ApiParam(value = "预约表id",required = true) Integer id){
    TAppointPrintDto tAppointPrintDto = tAppointService.detailForPrint(id);
    if(tAppointPrintDto == null){
      throw new BadRequestException("预约信息不存在！");
    }
    ModelAndView modelAndView = new ModelAndView("AppointPage");
    modelAndView.addObject("detail",tAppointPrintDto);
    return modelAndView;
  }

  @Log("查询预约项目")
  @ApiOperation("查询预约项目，权限码：TAppointGroup:list")
  @GetMapping("/getAppointGroup")
  //@PreAuthorize("@el.check('BPackageDT:list')")
  public R list(@RequestParam() String appointId) {
    return R.ok(tAppointService.queryAppointGroup(appointId));
  }

}
