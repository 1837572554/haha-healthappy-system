package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.healthappy.config.SeedIdGenerator;
import com.healthappy.config.databind.FieldBind;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 科室表
 * @author fang
 * @date 2021-06-22
 */
@Data
@TableName("B_Dept")
@ApiModel("科室表")
@SeedIdGenerator
public class BDept implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.INPUT)
    /**
     * id
     */
    @ApiModelProperty(value = "id",position = 1)
    private String id;

    /**
     * 科室名
     */
    @ApiModelProperty(value = "科室名",position = 2)
    private String name;

    /**
     * 类型
     */
    @ApiModelProperty(value = "类型",position = 3)
    @FieldBind(target = "deptTypeText")
    private Integer deptType;


    @TableField(exist = false)
    private String deptTypeText;

    /**
     * 排序号
     */
    @ApiModelProperty(value = "排序号",position = 4)
    private Integer dispOrder;

    /**
     * 是否启用
     */
    @ApiModelProperty(value = "是否启用",position = 5)
    private Boolean isEnable;

    /**
     * 描述
     */
    @ApiModelProperty(value = "描述",position = 6)
    private String description;

    /**
     * 简拼
     */
    @ApiModelProperty(value = "简拼",position = 7)
    private String jp;

    /**
     * 类别名称
     */
    @ApiModelProperty(value = "类别名称",position = 8)
    private String deptCode;

    /**
     * 科室类别名
     */
    @ApiModelProperty(value = "科室类别名",position = 9)
    private String categoryName;

    /**
     * 医疗机构id(u_hospital_info的id)
     */
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;

    /**
     * 是否删除 0不是  1是
     */
    @ApiModelProperty("是否删除")
    @TableField(value="del_flag",fill=FieldFill.INSERT)
//    @TableLogic(value = "0",delval = "1")
    private String delFlag;
}
