package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BPackageDT;
import com.healthappy.modules.system.domain.BPackageHD;
import com.healthappy.modules.system.domain.BPoison;
import com.healthappy.modules.system.domain.vo.BPoisonLiteVo;
import com.healthappy.modules.system.service.dto.*;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 有害因素 服务层
 * @author sjc
 * @date 2021-08-9
 */
public interface BPoisonService extends BaseService<BPoison> {

    Map<String, Object> queryAll(BPoisonQueryCriteria criteria, Pageable pageable);

    List<BPoison> queryAll(BPoisonQueryCriteria criteria);

    /**
     * 根据体检类别获取对应的套餐信息
     * @param id
     * @return
     */
    List<BPoisonDto> queryAllByPeTypeDt(String id,Integer sex);
    /**
     * 根据毒害列表和体检类别获取对应的套餐信息
     * @param list 要查询的毒害列表
     * @param id 体检分类id
     * @return
     */
    List<BPoisonDto> queryByPeTypeDt(List<BPoison> list, String id,Integer sex);

    /**
     * 体检登记 需要的接口
     * @param criteria
     * @param pageable
     * @return
     */
    Object registerPoisonQueryAll(BRegisterPoisonQueryCriteria criteria, Pageable pageable);

    /**
     * 体检登记 需要的接口
     * @param criteria
     * @return
     */
    List<BPoisonLiteVo> registerPoisonQueryAllLite(BRegisterPoisonQueryLiteCriteria criteria);

    /**
     * 获取毒害下的套餐项目
     * @author YJ
     * @date 2021/12/7 19:14
     * @param criteria
     * @return java.util.List〈com.healthappy.modules.system.domain.BPackageHD〉
     */
    List<BPackageHD> registerPoisonPackage(BRegisterPoisonQueryCriteria criteria);

    /**
     * 获取毒害下的组合项目
     * @author YJ
     * @date 2021/12/9 17:55
     * @param criteria
     * @return java.util.List〈com.healthappy.modules.system.domain.BPackageDT〉
     */
    List<BPackageDT> registerPoisonPackageDt(BRegisterPoisonQueryLiteCriteria criteria);

    /**
     * 查询有害因素集合下的组合项目
     * @author YJ
     * @date 2022/1/13 11:17
     * @param criteria 登记毒害查询类
     * @return java.util.List〈com.healthappy.modules.system.domain.BPackageDT〉
     */
    List<BPackageDT> getPoisonGroups(RegisterPoisonQueryCriteria criteria);

    /**
     *
     * @param poisonTypeList 毒害集合
     * @param peType  体检类别ID
     * @return
     */
    List<BPackageDT> getPackageList(List<String> poisonTypeList, String peType);
}
