package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BPeTypeHD;


/**
 * @description 体检类型 服务层
 * @author yanjun
 * @date 2021年9月3日 10:41:23
 */
public interface BPeTypeHDService extends BaseService<BPeTypeHD> {
    /**
     * 通过体检类型（大类）编号获得体检分组的ID
     * @param peTypeHdId 体检类型编号（大类）
     * @return
     */
    Integer getPeTypeGroupId(Long peTypeHdId);
}
