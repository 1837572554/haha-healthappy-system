package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.*;
import com.healthappy.modules.system.service.*;
import com.healthappy.modules.system.service.dto.DataQueryCriteria;
import com.healthappy.modules.system.service.dto.DataQueryDto;
import com.healthappy.modules.system.service.mapper.TPatientMapper;
import com.healthappy.utils.FileUtil;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author sjc
 * @date 2022-1-21
 */
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class GroupPanelServerImpl extends BaseServiceImpl<TPatientMapper, TPatient> implements GroupPanelServer {


    private final IGenerator generator;
    private final TPatientService patientService;
    private final BCompanyService bCompanyService;
    private final BDepartmentService bDepartmentService;
    private final BDepartmentGroupHDService bDepartmentGroupHDService;
    private final BPeTypeHDService bPeTypeHDService;
    private final BPeTypeDTService bPeTypeDTService;
    private final TItemHDService tItemHDService;

    @Override
    public List<DataQueryDto> queryAll(DataQueryCriteria criteria) {
        return buildList(tItemHDService.getGroupPanelHd(criteria));
    }


    @Override
    public Map<String, Object> queryAll(DataQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<DataQueryDto> page = new PageInfo<DataQueryDto>(tItemHDService.getGroupPanelHd(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", buildList(page.getList()));
        map.put("totalElements", page.getTotal());
        return map;
    }


    /**
     * 导出数据
     */
    @Override
    public void download(DataQueryCriteria criteria, HttpServletResponse response) throws IOException {
        if(ObjectUtil.isNotNull(criteria.getDateType())){
            if(ObjectUtil.isNull(criteria.getStartTime()) || ObjectUtil.isNull(criteria.getEndTime())){
                throw  new BadRequestException("查询时间条件有误");
            }
        }
        List<DataQueryDto> dtoList = queryAll(criteria);

        List<Map<String, Object>> list = new ArrayList<>();
        for (DataQueryDto dataQueryDto : dtoList) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put(" 流水号", dataQueryDto.getId());
            map.put(" 姓名", dataQueryDto.getName());
            map.put(" 性别", dataQueryDto.getSexText());
            map.put(" 年龄", dataQueryDto.getAge());
            map.put(" 体检分类", dataQueryDto.getPeTypeHdName());
            map.put(" 体检类别", dataQueryDto.getPeTypeDtName());
            map.put(" 单位", dataQueryDto.getCompanyName());
            map.put(" 部门", dataQueryDto.getDepName());
            map.put(" 分组", dataQueryDto.getDepGroupName());
            map.put("体检套餐", dataQueryDto.getPackageName());
            map.put("项目名称 ",dataQueryDto.getGroupName());
            map.put("小结", dataQueryDto.getResult());
            map.put("检查时间 ", dataQueryDto.getResultDate());
            map.put("检查医生", dataQueryDto.getDoctor());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }



    private List<DataQueryDto> buildList(List<DataQueryDto> list) {
        if(CollUtil.isEmpty(list)){
            return Collections.emptyList();
        }

        //单位
        List<String> comList = list.stream().map(DataQueryDto::getCompanyId).distinct().collect(Collectors.toList());
        List<BCompany> companyList = bCompanyService.lambdaQuery().in(CollUtil.isNotEmpty(comList),BCompany::getId, comList).list();
        //部门
        List<String> depList = list.stream().filter(c-> StrUtil.isNotEmpty(c.getDepartmentId())).map(DataQueryDto::getDepartmentId).distinct().collect(Collectors.toList());
        List<BDepartment> bDepartmentList = bDepartmentService.lambdaQuery().in(CollUtil.isNotEmpty(depList),BDepartment::getId,depList ).list();
        //分组
        List<String> depGroupList = list.stream().filter(c -> StrUtil.isNotEmpty(c.getDepartmentGroupId())).map(DataQueryDto::getDepartmentGroupId).distinct().collect(Collectors.toList());
        List<BDepartmentGroupHD> bDepartmentGroupHDList = bDepartmentGroupHDService.lambdaQuery().in(CollUtil.isNotEmpty(depGroupList),BDepartmentGroupHD::getId, depGroupList).list();

        //体检分类
        List<Long> typeHd = list.stream().filter(c -> c.getPeTypeHdId() != null).map(DataQueryDto::getPeTypeHdId).distinct().collect(Collectors.toList());
        List<BPeTypeHD> bPeTypeHDList = bPeTypeHDService.lambdaQuery().in(CollUtil.isNotEmpty(typeHd),BPeTypeHD::getId,typeHd).list();
        //体检类别
        List<Long> typeDt = list.stream().filter(c -> c.getPeTypeDtId() != null).map(DataQueryDto::getPeTypeDtId).distinct().collect(Collectors.toList());
        List<BPeTypeDT> bPeTypeDTList = bPeTypeDTService.lambdaQuery().in(CollUtil.isNotEmpty(typeDt),BPeTypeDT::getId, typeDt).list();

        list.stream().peek(e -> {
            e.setCompanyName(companyList.stream().filter(c->c.getId().equals(e.getCompanyId())).findFirst().orElse(new BCompany()).getName());
            e.setDepName(bDepartmentList.stream().filter(c->c.getId().equals(e.getDepartmentId())).findFirst().orElse(new BDepartment()).getName());
            e.setDepGroupName(bDepartmentGroupHDList.stream().filter(c->c.getId().equals(e.getDepartmentGroupId())).findFirst().orElse(new BDepartmentGroupHD()).getName());

            e.setPeTypeHdName(bPeTypeHDList.stream().filter(c->c.getId().equals(e.getPeTypeHdId())).findFirst().orElse(new BPeTypeHD()).getName());
            e.setPeTypeDtName(bPeTypeDTList.stream().filter(c->c.getId().equals(e.getPeTypeDtId())).findFirst().orElse(new BPeTypeDT()).getName());
        }).collect(Collectors.toList());


        return list;
    }

    private QueryWrapper buildQueryCriteria(DataQueryCriteria criteria) {
        QueryWrapper<TPatient> wrapper = QueryHelpPlus.getPredicate(TPatient.class,criteria);

        //查询的时间类型
        if(ObjectUtil.isNotNull(criteria.getDateType())){
            String startTime = criteria.getStartTime(),endTime = criteria.getEndTime();
            switch (criteria.getDateType()){
                case 1:
                    //登记
                    wrapper.between("pe_date", startTime,endTime);
                    break;
                case 2:
                    //签到
                    wrapper.between("sign_date",startTime,endTime);
                    break;
                case 3:
                    //总检
                    wrapper.between("conclusion_date",startTime,endTime);
                    break;
                default:
                    throw new BadRequestException("参数有误");
            }
        }

        String whereSql = "";
        if(StrUtil.isNotEmpty(criteria.getGroupId()))
        {
            whereSql+=" and  group_id = '"+criteria.getGroupId()+"' ";
        }
        if(StrUtil.isNotEmpty(criteria.getDeptId()) )
        {
            whereSql+=" and dept_id = '"+criteria.getDeptId()+"' ";
        }
        if(StrUtil.isNotEmpty(whereSql))
        {
            String lastSql=" and id in (select pa_id from T_Item_HD where ( 1=1 "+ whereSql+")   and pa_id=T_Patient.id )";
            wrapper.last(lastSql);
        }

        return wrapper;
    }

}
