package com.healthappy.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.healthappy.modules.system.domain.TChargeCOMApplyWujia;

/**
 * @Author: YuTang
 * @Date: Created in 2022/3/29 9:39
 * @Description:
 * @Version: 1.0
 */
public interface TChargeCOMApplyWujiaService extends IService <TChargeCOMApplyWujia> {

    /**
     * 单位缴费选择结账名单时保存该名单下所有人员所选项目的所有物价
     * @param payApplyId
     * @return
     */
    void saveByApply(String payApplyId);
}
