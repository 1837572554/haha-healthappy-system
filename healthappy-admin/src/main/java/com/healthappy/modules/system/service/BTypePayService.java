package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BTypePay;

/**
 * Created with IntelliJ IDEA. Created by yutang 2021/9/2 0002  14:40 Description:
 */
public interface BTypePayService extends BaseService<BTypePay> {

}
