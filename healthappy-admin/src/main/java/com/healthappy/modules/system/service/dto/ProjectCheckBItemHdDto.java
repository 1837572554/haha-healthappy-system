package com.healthappy.modules.system.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class ProjectCheckBItemHdDto implements Serializable {

    @ApiModelProperty("主键")
    private String id;

    @ApiModelProperty("体检号")
    private String paId;

    @ApiModelProperty("组合ID")
    private String groupId;

    @ApiModelProperty("科室ID")
    private String deptId;

    @ApiModelProperty("组合名称")
    private String groupName;

    @ApiModelProperty("判定")
    private String resultMark;

    @ApiModelProperty("描述")
    private String resultDesc;

    @ApiModelProperty("小结")
    private String result;

    @ApiModelProperty("报告医生")
    private String reportDoctor;

    @ApiModelProperty("操作医生")
    private String doctor;

    @ApiModelProperty("修改小结医生")
    private String updateResultDoc;

    @ApiModelProperty("操作时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateResultDate;

    @ApiModelProperty("检查日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date resultDate;

    @ApiModelProperty("开单医生")
    private String registerDoc;
}
