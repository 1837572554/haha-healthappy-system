package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.*;
import lombok.Data;

/**
 * 今天预约登记数据统计
 * @author Jevany
 * @date 2022/3/11 17:10
 */
@Data
@ApiModel("今天预约登记数据统计")
public class AppointRegisterTodayDTO {
    /** 1.总登记人数 */
    @ApiModelProperty("1.总登记人数")
    private Integer registerNum;

    /** 2.预约人数 */
    @ApiModelProperty("2.预约人数")
    private Integer appointNum;

    /** 3.预约签到人数 */
    @ApiModelProperty("3.预约签到人数")
    private Integer appointSignNum;

    /** 4. 1-3（总登记人数-预约签到人数）*/
    @ApiModelProperty("4. 1-3（总登记人数-预约签到人数）")
    private Integer otherNum;
    
}