package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.healthappy.modules.dataSync.config.RenewLog;
import lombok.Data;

/**
 * @desc: 电测听修正数据
 * @author: YJ
 * @date: 2021-12-02 16:01
 **/
@Data
@TableName("T_DCT_DATA_XZ")
@RenewLog
public class TDctDataXz extends TDctData {

}
