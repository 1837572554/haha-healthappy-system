package com.healthappy.modules.system.service.dto;

import com.healthappy.modules.system.domain.UNoticeDept;
import lombok.Data;


/**
 * @description 公告对应科室表
 * @author sjc
 * @date 2021-09-3
 */
public class UNoticeDeptDto extends UNoticeDept {


}
