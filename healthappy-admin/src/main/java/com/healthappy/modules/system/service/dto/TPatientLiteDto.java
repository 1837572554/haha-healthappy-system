package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Jevany
 * @date 2021/12/16 17:12
 * @desc
 */
@Data
public class TPatientLiteDto implements Serializable {

    /** 体检号 */
    @ApiModelProperty("体检号")
    private String id;

    /** 体检者姓名 */
    @ApiModelProperty("体检者姓名")
    private String name;

    /** 体检编号 */
    @ApiModelProperty("单位编号")
    private String companyId;

    /** 公司名称 */
    @ApiModelProperty("公司名称")
    private String companyName;
}
