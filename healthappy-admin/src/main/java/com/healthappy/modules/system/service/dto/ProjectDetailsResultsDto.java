package com.healthappy.modules.system.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.modules.system.domain.TImage;
import com.healthappy.modules.system.domain.vo.TImageVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author FGQ
 */
@Data
public class ProjectDetailsResultsDto implements Serializable {

    @ApiModelProperty("项目ID")
    private String groupId;

    @ApiModelProperty("项目名称")
    private String groupName;

    @ApiModelProperty("描述")
    private String resultDesc;

    @ApiModelProperty("小结")
    private String result;

    @ApiModelProperty("检查日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp resultDate;

    @ApiModelProperty("医生")
    private String doctor;

    @ApiModelProperty("项目明细")
    private List<ItemDt> itemDetailList;

    @ApiModelProperty("报告图片预览")
    private List<TImageVo> tImageVoList;

    @Data
    public static class ItemDt{

        @ApiModelProperty("项目名称")
        private String itemName;

        @ApiModelProperty("判定")
        private String resultMark;

        @ApiModelProperty("结果")
        private String result;

        @ApiModelProperty("参考值")
        private String refValue;
    }
}
