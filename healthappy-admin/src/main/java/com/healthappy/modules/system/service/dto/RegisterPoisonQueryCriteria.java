package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author Jevany
 * @date 2022/1/13 10:44
 * @desc 登记毒害查询类
 */
@Data
@ApiModel("登记毒害查询类")
public class RegisterPoisonQueryCriteria {
    /** 毒害编号集合 */
    @ApiModelProperty(value = "毒害编号集合",required = true)
    private List<String> poisonIdList;

    /** 体检类别 */
    @ApiModelProperty(value = "体检类别 1:上岗前，2：在岗期间，4：离岗后，5：应急",required = true)
    private Integer code;

    /** 性别 0：不限 1：男 2：女  （默认：0） */
    @ApiModelProperty("性别 0：不限 1：男 2：女  （默认：0）")
    private Integer sex=0;

    //region 后端的使用参数
    /** 毒害编号 */
    @ApiModelProperty(value = "毒害编号" ,hidden = true)
    private String poisonId;

    /** 排除的组合编号 */
    @ApiModelProperty(value = "排除的组合编号" ,hidden = true)
    private List<String> notGroupIds;

    /** 租户编号 */
    @ApiModelProperty(value = "租户编号" ,hidden = true)
    private String tenantId;
    //endregionn 后端的使用参数
}
