package com.healthappy.modules.system.rest.system;

import cn.hutool.core.lang.Dict;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.Role;
import com.healthappy.modules.system.domain.User;
import com.healthappy.modules.system.domain.UsersRoles;
import com.healthappy.modules.system.service.RoleService;
import com.healthappy.modules.system.service.UserService;
import com.healthappy.modules.system.service.UsersRolesService;
import com.healthappy.modules.system.service.dto.RoleDto;
import com.healthappy.modules.system.service.dto.RoleQueryCriteria;
import com.healthappy.modules.system.service.dto.UserDto;
import com.healthappy.utils.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author hupeng
 * @date 2018-12-03
 */
@Api(tags = "系统：角色管理")
@RestController
@RequestMapping("/api/roles")
public class RoleController {

    private final RoleService roleService;
    private final UserService userService;
    private final IGenerator generator;
	private final RedisUtils redisUtils;

    private static final String ENTITY_NAME = "role";

    public RoleController(RoleService roleService, UserService userService, IGenerator generator, RedisUtils redisUtils) {
        this.roleService = roleService;
        this.userService = userService;
        this.generator = generator;
		this.redisUtils=redisUtils;
    }

    @ApiOperation("获取单个role，权限码：roles:list")
    @GetMapping(value = "/{id}")
    @PreAuthorize("@el.check('roles:list')")
    public R getRoles(@PathVariable Long id) {
        return R.ok(roleService.findById(id));
    }

    @Log("导出角色数据")
    @ApiOperation("导出角色数据，权限码：roles:list")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('role:list')")
    @Klock
    public void download(HttpServletResponse response, RoleQueryCriteria criteria) throws IOException {
        roleService.download(generator.convert(roleService.queryAll(criteria), RoleDto.class), response);
    }

    @ApiOperation("返回全部的角色，权限码：roles:list,user:add,user:edit")
    @GetMapping(value = "/all")
    @PreAuthorize("@el.check('roles:list','user:add','user:edit')")
    public R getAll(RoleQueryCriteria criteria, @PageableDefault(value = Integer.MAX_VALUE, sort = {"level"}, direction = Sort.Direction.ASC) Pageable pageable) {
        return R.ok(roleService.queryAlls(criteria, pageable));
    }

    @Log("查询角色")
    @ApiOperation("查询角色，权限码：roles:list")
    @GetMapping
    @PreAuthorize("@el.check('roles:list')")
    public R getRoles(RoleQueryCriteria criteria, Pageable pageable) {
        return R.ok(roleService.queryAll(criteria, pageable));
    }

    @Log("查询角色菜单数据")
    @ApiOperation("查询角色菜单数据，权限码：roles:list")
    @GetMapping("/getRolesMenuByRole")
    @PreAuthorize("@el.check('roles:list')")
    public R getRolesMenuByRole(@RequestParam Long roleId) {
        if(ObjectUtil.isNull(roleId)){
            return R.error("角色编号不能为空");
        }
        return R.ok(roleService.getRolesMenuByRole(roleId));
    }


    @ApiOperation("获取用户级别")
    @GetMapping(value = "/level")
    public R getLevel() {
        return R.ok(Dict.create().set("level", getLevels(null)));
    }


    @Log("新增角色")
    @ApiOperation("新增角色，权限码：roles:add")
    @PostMapping
    @PreAuthorize("@el.check('roles:add')")
	@CacheEvict(cacheNames = {CacheConstant.MENU, CacheConstant.ROLE},allEntries = true)
    @Klock
    public R create(@Validated @RequestBody Role resources) {

        if (resources.getId() != null) {
            throw new BadRequestException("A new " + ENTITY_NAME + " cannot already have an ID");
        }
        getLevels(resources.getLevel());
        return R.ok(roleService.create(resources));
    }

    @Log("修改角色")
    @ApiOperation("修改角色，权限码：roles:edit")
    @PutMapping
    @PreAuthorize("@el.check('roles:edit')")
	@CacheEvict(cacheNames = {CacheConstant.MENU, CacheConstant.USER, CacheConstant.ROLE},allEntries = true)
    @Klock
    public R update(@Validated @RequestBody Role resources) {
        getLevels(resources.getLevel());
        roleService.update(resources);
        return R.ok();
    }

    @Log("修改角色菜单")
    @ApiOperation("修改角色菜单，权限码：roles:edit")
    @PutMapping(value = "/menu")
    @PreAuthorize("@el.check('roles:edit')")
	@CacheEvict(cacheNames = {CacheConstant.MENU, CacheConstant.USER, CacheConstant.ROLE,CacheConstant.USER_MENU},allEntries = true)
    @Klock
    public R updateMenu(@RequestBody Role resources) {
        RoleDto role = roleService.findById(resources.getId());
        getLevels(role.getLevel());
        roleService.updateMenu(resources, role);
		//清理权限 缓存
		Long roleId = resources.getId();
		//删除用户菜单缓存
		redisUtils.hdel(CacheConstant.USER_MENU);
		//组装查询条件 查询role 关联的user
		List<String> nameList = roleService.getUserNameByRoleId(roleId);
		for (String  userName:nameList) {
			redisUtils.del(SecurityConstants.AUTH_USER_TENANT_ID+userName);
		}

        return R.ok();
    }

    @Log("删除角色")
    @ApiOperation("删除角色，权限码：roles:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('roles:delete')")
	@CacheEvict(cacheNames = {CacheConstant.MENU, CacheConstant.USER, CacheConstant.ROLE},allEntries = true)
    @Klock
    public R delete(@RequestBody Set<Long> ids) {
        for (Long id : ids) {
            RoleDto role = roleService.findById(id);
            getLevels(role.getLevel());
        }
        try {
            roleService.delete(ids);
        } catch (Throwable e) {
            throw new BadRequestException("所选角色存在用户关联，请取消关联后再试");
        }
        return R.ok();
    }

    /**
     * 获取用户的角色级别
     * @return /
     */
    private int getLevels(Integer level) {
        UserDto user = userService.findByName(SecurityUtils.getUsername());
        List<Integer> levels = roleService.findByUsersId(user.getId()).stream().map( it->{return ObjectUtil.isNull(it.getLevel())?99:it.getLevel();}).collect(Collectors.toList());
        int min = Collections.min(levels);
        if (level != null) {
            if (level < min) {
                throw new BadRequestException("权限不足，你的角色级别：" + min + "，低于操作的角色级别：" + level);
            }
        }
        return min;
    }
}
