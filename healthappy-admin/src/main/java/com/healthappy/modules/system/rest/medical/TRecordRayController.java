package com.healthappy.modules.system.rest.medical;

import cn.hutool.core.bean.BeanUtil;
import com.healthappy.annotation.AnonymousAccess;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.TRecordRay;
import com.healthappy.modules.system.domain.vo.TRecordRayVo;
import com.healthappy.modules.system.service.TRecordRayService;
import com.healthappy.modules.system.service.dto.TRecordRayQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 职业史放射表
 * @author sjc
 * @date 2021-12-15
 */
@Validated
@Slf4j
@Api(tags = "体检管理：职业史放射表")
@RestController
@AllArgsConstructor
@RequestMapping("/TRecordRay")
public class TRecordRayController {

    private final TRecordRayService tRecordRayService;

    @Log("查询职业史放射数据")
    @ApiOperation("查询职业史放射数据，权限码：ProjectCheck:list")
    @GetMapping
    @PreAuthorize("@el.check('ProjectCheck:list')")
    public R list(TRecordRayQueryCriteria criteria) {
        return R.ok(tRecordRayService.queryAll(criteria));
    }

    @Log("分页查询职业史放射数据")
    @ApiOperation("分页查询职业史放射数据，权限码：ProjectCheck:list")
    @GetMapping("/page")
    @PreAuthorize("@el.check('ProjectCheck:list')")
    public R listPage(TRecordRayQueryCriteria criteria, Pageable pageable) {
        return R.ok(tRecordRayService.queryAll(criteria, pageable));
    }

    @Log("新增|修改职业史放射")
    @ApiOperation("新增|修改职业史放射，权限码：TRecord:add")
    @PostMapping("/add")
    @PreAuthorize("@el.check('TRecordRay:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody TRecordRayVo resources) {
        return R.ok(tRecordRayService.saveOrUpdate( BeanUtil.copyProperties(resources, TRecordRay.class)));
    }


    @Log("根据id进行删除")
    @ApiOperation("根据id进行删除，权限码：TRecordRay:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('TRecord:delete')")
    @Klock
    public R remove(@RequestBody Set<Integer> ids) {
        return R.ok(tRecordRayService.removeByIds(ids));
    }
}
