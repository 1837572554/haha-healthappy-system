package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.*;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @description 部门表
 * @author sjc
 * @date 2021-06-24
 */
@Data
public class BDepartmentVo  {


    /**
     * 部门编号
     */
    @ApiModelProperty("部门编号")
    private String id;

    /**
     * 部门名称
     */
    @NotBlank(message = "部门名称不能为空")
    @ApiModelProperty("部门名称")
    private String name;

    /**
     * 单位id
     */
    @NotBlank(message = "单位id不能为空")
    @ApiModelProperty("单位id")
    private String comId;

    /**
     * 是否启用 0否 1是
     */
    @ApiModelProperty("是否启用 0否 1是")
    private String isEnable;

    /**
     * 排序
     */
    @ApiModelProperty("排序")
    private String dispOrder;

}
