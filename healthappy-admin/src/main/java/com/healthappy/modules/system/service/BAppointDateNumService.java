package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BAppointDateNum;

/**
 *
 * @author Jevany
 * @date 2022/5/7 0:09
 */
public interface BAppointDateNumService extends BaseService<BAppointDateNum> {

}