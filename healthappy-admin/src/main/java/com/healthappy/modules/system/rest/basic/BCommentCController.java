package com.healthappy.modules.system.rest.basic;

import cn.hutool.core.bean.BeanUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BCommentC;
import com.healthappy.modules.system.domain.vo.BCommentCVo;
import com.healthappy.modules.system.service.BCommentCService;
import com.healthappy.modules.system.service.dto.BCommentCQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 从业主检意见设置 控制器
 * @author FGQ
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "基础管理：从业主检意见")
@RestController
@AllArgsConstructor
@RequestMapping("/BCommentC")
public class BCommentCController {

    private final BCommentCService bCommentCService;

    @Log("查询从业主检意见")
    @ApiOperation("查询从业主检意见，权限码：BCommentC:list")
    @GetMapping
    public R list(BCommentCQueryCriteria criteria, Pageable pageable) {
        return R.ok(bCommentCService.queryAll(criteria, pageable));
    }

    @Log("新增|修改从业主检意见")
    @ApiOperation("新增|修改从业主检意见，权限码：BCommentC:add")
    @PostMapping
    @PreAuthorize("@el.check('BCommentC:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BCommentCVo resources) {
        return R.ok(bCommentCService.saveOrUpdate(BeanUtil.copyProperties(resources, BCommentC.class)));
    }

    @Log("删除从业主检意见")
    @ApiOperation("删除从业主检意见，权限码：BCommentC:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('BCommentC:delete')")
    @Klock
    public R remove(@RequestBody Set<String> ids) {
        return R.ok(bCommentCService.removeByIds(ids));
    }
}
