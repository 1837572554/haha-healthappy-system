package com.healthappy.modules.system.service;

import cn.hutool.core.date.DateTime;
import com.healthappy.modules.system.service.dto.QueryCustomDTO;
import com.healthappy.modules.system.service.dto.QueryDataCriteria;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public interface CKPatientService { ;

    /**
     * 自定义查询-导出
     */
    void reportExecl(QueryDataCriteria criteria,HttpServletResponse response);

    Map<String, Object> getPage(QueryDataCriteria criteria, Pageable pageable);

    List<QueryCustomDTO> queryAll(QueryDataCriteria criteria);
}
