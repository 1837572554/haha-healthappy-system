package com.healthappy.modules.system.service;

import com.healthappy.modules.system.domain.BItem;
import com.healthappy.modules.system.domain.TItemDT;
import com.healthappy.modules.system.domain.vo.BallCheckUploadVo;

import java.util.List;

/**
 * @description 团检备单 服务层
 * @author fang
 * @date 2021-06-17
 */
public interface BallCheckService {

    /**
     * 导入
     * @param registrationType
     * @param manual
     * @return
     */
    void upload(Integer registrationType, String appointDateStart, String appointDateEnd, List<BallCheckUploadVo> manual);


    /**
     *  转换  - 上限下限
     * @param sex 1 男,2 女
     * @param item
     * @return refHigh,refLow,refValue
     */
    TItemDT convertRef(int sex, BItem item);
}
