package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BCompanyAttribute;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 单位属性
 * @author sjc
 * @date 2021-08-30
 */
@Mapper
@Repository
public interface BCompanyAttributeMapper extends CoreMapper<BCompanyAttribute> {

}
