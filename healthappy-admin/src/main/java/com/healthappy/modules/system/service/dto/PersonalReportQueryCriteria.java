package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PersonalReportQueryCriteria {

    /** 体检编号 */
    @ApiModelProperty("体检编号")
    @Query(type = Query.Type.EQUAL)
    private String id;

    /** 姓名 */
    @ApiModelProperty("姓名")
    @Query(type = Query.Type.INNER_LIKE)
    private String name;

    /** 单位 */
    @ApiModelProperty("单位")
    @Query
    private String companyId;

    /** 分类 */
    @ApiModelProperty("分类")
    @Query
    private String peTypeHdId;

    /** 类别 */
    @ApiModelProperty("类别")
    @Query
    private String peTypeDtId;

    /** 日期类型：1：登记，2：签到，3：总检,4:总审 */
    @ApiModelProperty("日期类型：1：登记，2：签到，3：总检,4:总审")
    private Integer dateType;

    /** 开始日期 */
    @ApiModelProperty("开始日期")
    private String startTime;

    /** 结束日期 */
    @ApiModelProperty("结束日期")
    private String endTime;

    /** 状态 1未总检 2已总检 3已审核 4撤销总检,-1查全部 */
    @ApiModelProperty("状态 1未总检 2已总检 3已审核 4撤销总检,-1查全部")
    private String isStatus = "-1";

    /** 是否有缺项 */
    @ApiModelProperty("是否有缺项")
    private Integer isLackItem;

    /** 工作站 */
    @Query
    @ApiModelProperty("工作站")
    private String workstation;

    /** 总检医生ID */
    @ApiModelProperty("总检医生ID")
    private String conclusionDoc;

    /** 1:总检,2:总审 */
    @ApiModelProperty("1:总检,2:总审")
    private Integer type = 1;

    /** 1:未审核，2:已审核，3:已退回,-1:查询全部 */
    @ApiModelProperty("1:未审核，2:已审核，3:已退回,-1:查询全部")
    private Integer acType = -1;

    /** 总审医生 */
    @ApiModelProperty("总审医生")
    private String verifyDoc;
}
