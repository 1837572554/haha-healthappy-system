package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BTypeSectorDt;
import com.healthappy.modules.system.service.dto.BTypeSectorDtQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 从业体检行业 服务层
 * @author fang
 * @date 2021-06-17
 */
public interface BTypeSectorDtService extends BaseService<BTypeSectorDt> {


    Map<String, Object> queryAll(BTypeSectorDtQueryCriteria criteria, Pageable pageable);


    /**
     * 查询数据分页
     * @param criteria 条件
     * @return Map<String, Object>
     */
    List<BTypeSectorDt> queryAll(BTypeSectorDtQueryCriteria criteria);
}
