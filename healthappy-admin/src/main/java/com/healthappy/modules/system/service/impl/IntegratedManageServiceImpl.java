package com.healthappy.modules.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.TPatient;
import com.healthappy.modules.system.service.IntegratedManageService;
import com.healthappy.modules.system.service.dto.IntegratedManageDTO;
import com.healthappy.modules.system.service.dto.IntegratedManageQueryCriteria;
import com.healthappy.modules.system.service.mapper.TPatientMapper;
import com.healthappy.utils.FileUtil;
import com.healthappy.utils.PageUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Jevany
 * @date 2022/2/8 15:54
 * @desc
 */
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class IntegratedManageServiceImpl extends BaseServiceImpl<TPatientMapper, TPatient> implements IntegratedManageService {

    private final TPatientMapper tPatientMapper;

    @Override
    public Map<String, Object> getIntegratedManageList(IntegratedManageQueryCriteria criteria) {
        //总条数
        Integer totalCount = tPatientMapper.getIntegratedManageCount(criteria);
        Integer[] pageBase = null;

        if (ObjectUtil.isNotNull(criteria.getIsExport()) && criteria.getIsExport().equals(false)) {
            pageBase = PageUtil.toStartEnd(criteria.getPageNum(), criteria.getPageSize(), totalCount);
            criteria.setStartIndex(pageBase[0]);
        }

        List<IntegratedManageDTO> integratedManageList = tPatientMapper.getIntegratedManageList(criteria);

        if (criteria.getIsExport().equals(false)) {
            Map<String, Object> map = new LinkedHashMap<>(5);
            map.put("content", integratedManageList);
            map.put("totalElements", totalCount);
            map.put("totalPage", pageBase[3]);
            map.put("currentPage", pageBase[2]);
            map.put("pageSize", criteria.getPageSize());
            return map;
        } else {
            Map<String, Object> map = new LinkedHashMap<>(1);
            map.put("content", integratedManageList);
            return map;
        }
    }

    @Override
    public void export(List<IntegratedManageDTO> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        Integer idx = 0;
        for (IntegratedManageDTO dto : all) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("序号", ++idx);
            map.put("流水号", dto.getId());
            map.put("体检分类", dto.getPeTypeHdName());
            map.put("体检类别", dto.getPeTypeDtName());
            map.put("姓名", dto.getName());
            map.put("性别", dto.getSexText());
            map.put("年龄", dto.getAge());
            map.put("单位", dto.getCompanyName());
            map.put("部门", dto.getDeptName());
            map.put("分组", dto.getDeptGroupName());
            map.put("登记时间", dto.getCreateDate());
            map.put("签到时间", dto.getSignDate());
            map.put("审核医生", dto.getVerifyDoctor());
            map.put("审核时间", dto.getVerifyDate());
            map.put("打印医生", dto.getPrintDoctor());
            map.put("打印时间", dto.getPrintDate());
            map.put("上传医生", dto.getUploadDoctor());
            map.put("上传时间", dto.getUploadDate());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }


}