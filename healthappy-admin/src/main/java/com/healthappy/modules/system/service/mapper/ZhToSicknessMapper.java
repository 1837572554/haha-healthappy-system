package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.ZhToSickness;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 组合判断设置相关mapper
 * @author sjc
 * @date 2021-08-2
 */
@Mapper
@Repository
public interface ZhToSicknessMapper extends CoreMapper<ZhToSickness> {
}
