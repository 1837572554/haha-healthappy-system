package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TPatientPoison;

import java.util.List;

/**
 * @author Jevany
 * @date 2022/1/12 15:22
 * @desc 体检毒害关联 服务层
 */
public interface TPatientPoisonService extends BaseService<TPatientPoison> {

    /**
     * 保存体检号的毒害编号
     * @author YJ
     * @date 2022/1/12 15:23
     * @param patientId
     * @param poisonIds
     */
    void savePatientPoison(String patientId, List<String> poisonIds);
}