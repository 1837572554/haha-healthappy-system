package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BTypeSectorHd;
import com.healthappy.modules.system.service.dto.BTypeSectorHdQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 从业体检行业 服务层
 * @author fang
 * @date 2021-06-17
 */
public interface BTypeSectorHdService extends BaseService<BTypeSectorHd> {


    Map<String, Object> queryAll(BTypeSectorHdQueryCriteria criteria, Pageable pageable);


    /**
     * 查询数据分页
     * @param criteria 条件
     * @return Map<String, Object>
     */
    List<BTypeSectorHd> queryAll(BTypeSectorHdQueryCriteria criteria);
}
