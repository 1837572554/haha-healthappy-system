package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BSetUp;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 综合设置
 * @author cyt
 * @date 2021-08-17
 */
@Mapper
@Repository
public interface BSetUpMapper extends CoreMapper<BSetUp> {
}
