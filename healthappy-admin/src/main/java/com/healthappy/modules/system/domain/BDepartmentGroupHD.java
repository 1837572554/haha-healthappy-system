package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.config.databind.FieldBind;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @description 分组表
 * @author sjc
 * @date 2021-06-24
 */
@Data
@ApiModel("分组表")
@TableName("B_Department_Group_HD")
public class BDepartmentGroupHD implements Serializable  {
    private static final long serialVersionUID = 1L;


    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    @TableId(type = IdType.INPUT)
    @TableField("id")
    private String id;

    /**
     * 分组名称
     */
    @ApiModelProperty("分组名称")
    @TableField("name")
    private String name;

    /**
     * 单位id
     */
    @ApiModelProperty("单位id")
    @TableField("company_id")
    private String companyId;

    /**
     * 部门Id
     */
    @ApiModelProperty("部门Id")
    @TableField("department_id")
    private String departmentId;

    /**
     * 统收限额
     */
    @ApiModelProperty("统收限额")
    @TableField("tsxe")
    private String tsxe;

    /**
     * 收费方式 0自费 1统收
     */
    @ApiModelProperty("收费方式")
    @TableField("sffs")
    @FieldBind(target = "sffsText")
    private String sffs;

    @TableField(exist = false)
    private String sffsText;

    /**
     * 绑定的套餐名
     */
    @ApiModelProperty("绑定的套餐名")
    @TableField("package_name")
    private String packageName;

    /**
     * 允许性别
     */
    @ApiModelProperty("允许性别")
    @TableField("sex")
    @FieldBind(target = "sexText")
    private String sex;

    @TableField(exist = false)
    private String sexText;

    /**
     * 年龄低值
     */
    @ApiModelProperty("年龄低值")
    @TableField("age_low")
    private Integer ageLow;

    /**
     * 年龄高值
     */
    @ApiModelProperty("年龄高值")
    @TableField("age_high")
    private Integer ageHigh;

    /**
     * 婚否 0否 1是
     */
    @ApiModelProperty("婚否")
    @TableField("marital")
    @FieldBind(target = "maritalText")
    private String marital;

    @TableField(exist = false)
    private String maritalText;
    /**
     * 套餐id
     */
    @ApiModelProperty("套餐id")
    @TableField("package_id")
    private String packageId;
    /**
     * 是否是项目追加 0否 1是
     */
    @ApiModelProperty("是否是项目追加")
    @TableField("is_used")
    private String isUsed;
    /**
     * 更新时间
     */
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty("更新时间")
    @TableField(value = "update_time",fill = FieldFill.UPDATE)
    private Timestamp updateTime;

    /**
     * 更新医生
     */
    @ApiModelProperty("更新医生")
    @TableField("update_doc")
    private String updateDoc;
    /**
     * 更新电脑ip
     */
    @ApiModelProperty("更新电脑ip")
    @TableField("update_ip")
    private String updateIp;
    /**
     * 是否启用 0否 1是
     */
    @ApiModelProperty("是否启用")
    @TableField("is_enable")
    private String isEnable;
    /**
     * 毒害
     */
    @ApiModelProperty("毒害")
    @TableField("poison")
    private String poison;
    /**
     * 折扣后总价格
     */
    @ApiModelProperty("折扣后总价格")
    @TableField("cost")
    private Double cost;
    /**
     * 实际价格
     */
    @ApiModelProperty("实际价格")
    @TableField("price_real")
    private Double priceReal;

    /**
     * 原价
     */
    @ApiModelProperty("原价")
    @TableField("price")
    private Double price;
    /**
     * 服务费
     */
    @ApiModelProperty("服务费")
    @TableField("service_fee")
    private Double serviceFee;
    /**
     * 折扣
     */
    @ApiModelProperty("折扣")
    @TableField("discount")
    private Double discount;

    /** 租户编号 */
    @ApiModelProperty("租户编号")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;
}
