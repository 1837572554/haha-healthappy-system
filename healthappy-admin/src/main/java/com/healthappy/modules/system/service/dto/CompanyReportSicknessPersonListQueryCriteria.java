package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.*;
import lombok.Data;

import java.util.List;

/**
 * 单位报告病种人员列表查询类
 * @author Jevany
 * @date 2022/2/23 16:09
 */
@Data
public class CompanyReportSicknessPersonListQueryCriteria extends CompanyReportPersonQueryCriteria {
    /** 病种编号列表 */
    @ApiModelProperty("病种编号列表")
    List<String> sicknessIdList;
}
