package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 单位报告审核
 * @author Jevany
 * @date 2022/2/25 16:40
 */
@Data
public class CompanyReportVerifyDTO implements Serializable {

    /** 单位报告编号 */
    @NotBlank(message = "单位报告编号不能为空")
    @ApiModelProperty("单位报告编号")
    private String repId;

    /** 是否审核 true审核，fasle取消审核 */
    @NotNull(message = "是否审核不能为空")
    @ApiModelProperty("是否审核 true审核，fasle取消审核")
    private Boolean verify;
}