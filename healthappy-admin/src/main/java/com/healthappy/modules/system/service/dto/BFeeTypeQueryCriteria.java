package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BFeeTypeQueryCriteria {

    @Query(blurry = "name")
    @ApiModelProperty("费别名称")
    private String blurry;
}
