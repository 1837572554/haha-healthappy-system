package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @Author: YuTang
 * @Date: Created in 2022/3/29 8:39
 * @Description: 单位收费物价表
 * @Version: 1.0
 */
@Data
@TableName(value = "T_Charge_COM_Apply_Wujia")
@ApiModel(value = "单位收费物价")
@RenewLog
public class TChargeCOMApplyWujia {

    /**
     * 缴费申请单号
     */
    @ApiModelProperty(value ="缴费申请单号")
    @NotBlank(message = "缴费申请单号不可为空")
    private String payApplyId;

    /**
     * 租户id
     */
    @ApiModelProperty(value ="租户id")
    private String tenantId;

    /**
     * 物价id
     */
    @ApiModelProperty(value ="物价id")
    @NotBlank(message = "物价id不可为空")
    private String wujiaId;

    /**
     * 物价名称
     */
    @ApiModelProperty(value ="物价名称")
    @NotBlank(message = "物价名称不可为空")
    private String wujiaName;

    /**
     * 规格
     */
    @ApiModelProperty(value ="规格")
    @NotBlank(message = "规格不可为空")
    private String pkgName;

    /**
     * 单价
     */
    @ApiModelProperty(value ="单价")
    @NotNull(message = "单价不可为空")
    private BigDecimal price;

    /**
     * 数量
     */
    @ApiModelProperty(value ="数量")
    @NotNull(message = "数量不可为空")
    private BigDecimal count;

    /**
     * 折扣比例
     */

    @ApiModelProperty(value ="折扣比例（额度）")
    @NotNull(message = "折扣比例不可为空")
    private BigDecimal discountValue;

    /**
     * 总金额
     */
    @ApiModelProperty(value ="总金额")
    @NotNull(message = "总金额不可为空")
    private BigDecimal totalPrice;
}
