package com.healthappy.modules.system.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author FGQ
 */
@Data
@ApiModel(value = "标识中间表")
public class TagSign implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("标识ID")
    private Long tagId;

    @ApiModelProperty("权限ID")
    private Long signId;

}
