package com.healthappy.modules.system.constants;

/**
 * 小工具打印 控制指令
 * @Author: wukefei
 * @Date: Created in 2022/4/27 15:23
 * @Description:
 * @Version: 1.0
 *
 */
public class CommConstants {

	/**  条码打印 标识 */
	public static final String  BAR_CODE = "PrintBarcode";

	/** 指引单打印 标识 */
	public static final String  GUIDE= "PrintGuide";

	/** 信息表打印 标识 */
	public static final String  INFO_REPORT = "PrintInfo";



	/**  读取身份证 标识 */
	public static final String  READ_CARD = "ReadCard";

	/** 电测听生成图片、结果、描述 标识 */
	public static final String  DCT_GENERATE = "DctGenerate";



	/** 报告打印 标识 */
	public static final String  PRINT_REPORT = "PrintReport";

	/** 健康证打印 标识 */
	public static final String  HEALTH = "PrintHealth";

	/** 票据打印 标识 */
	public static final String  BILL = "PrintBill";


}
