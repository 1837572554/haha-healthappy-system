package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.healthappy.config.databind.FieldBind;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 从业体检类别
 * @author fang
 * @date 2021-06-21
 */
@Data
@TableName("B_Type_Sector_DT")
public class BTypeSectorDt implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
     * id
     */
    private Long id;

    /**
     * 行业id
     */
    private Long typeHdId;

    /**
     * 编码
     */
    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 套餐id
     */
    private String packageId;

    /**
     * 采血类别
     */
    @FieldBind(type = "takebloodType" , target = "takebloodText")
    private Long takeblood;

    @TableField(exist = false)
    private String takebloodText;

    /**
     * 体检有效期
     */
    private Integer examinValidity;

    /**
     * 培训有效期
     */
    private Integer trainingValidity;

    /**
     * disp_order
     */
    private Integer dispOrder;

    /**
     * 医疗机构id(u_hospital_info的id)
     */
    @ApiModelProperty("医疗机构id(u_hospital_info的id)")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;
}
