package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Builder
@Data
@AllArgsConstructor
public class BAreaQueryCriteria {

    @Query(blurry = "name")
    private String blurry;

    @Query
    private String isEnable;

    @Query
    private String hiCode;

    @Query
    private String code;

    private Long id;
}
