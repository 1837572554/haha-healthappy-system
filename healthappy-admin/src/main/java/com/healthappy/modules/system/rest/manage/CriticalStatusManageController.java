package com.healthappy.modules.system.rest.manage;

import cn.hutool.core.util.ObjectUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.service.TCriticalService;
import com.healthappy.modules.system.service.dto.CriticalDTO;
import com.healthappy.modules.system.service.dto.CriticalObjDTO;
import com.healthappy.modules.system.service.dto.CriticalQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 危急值状况管理 控制器
 *
 * @author Jevany
 * @date 2022/2/10 11:21
 */

@Slf4j
@Api(tags = "体检管理：危急值状况管理")
@RestController
@AllArgsConstructor
@RequestMapping("/CriticalStatusManage")
public class CriticalStatusManageController {

    private final TCriticalService tCriticalService;

    @Log("危急值状况管理-查询")
    @ApiOperation("危急值状况管理-查询，权限码：CriticalStatusManage:list")
    @GetMapping
    @PreAuthorize("@el.check('CriticalStatusManage:list')")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "危急值状况管理 返回类", response = CriticalDTO.class)
    })
    public R list(CriticalQueryCriteria criteria) {
        if (ObjectUtil.isEmpty(criteria.getPageNum()) || criteria.getPageNum() <= 0) {
            criteria.setPageNum(1);
        }
        if (ObjectUtil.isEmpty(criteria.getPageSize()) || criteria.getPageSize() <= 0) {
            criteria.setPageSize(10);
        }
        criteria.setIsExport(false);
        return R.ok(tCriticalService.getCriticalList(criteria));
    }

    @Log("危急值状况管理-导出")
    @ApiOperation("危急值状况管理-导出，权限码：CriticalStatusManage:list")
    @GetMapping("/export")
    @PreAuthorize("@el.check('CriticalStatusManage:list')")
    @Klock
    public void export(HttpServletResponse response, CriticalQueryCriteria criteria) throws IOException {
        criteria.setIsExport(true);
        Map<String, Object> map = tCriticalService.getCriticalList(criteria);
        tCriticalService.export((List<CriticalDTO>) map.get("content"), response);
    }

    @Log("危急值状况管理-获得危急值对象")
    @ApiOperation("危急值状况管理-获得危急值对象，权限码：CriticalStatusManage:list")
    @GetMapping("/getCriteria")
    @PreAuthorize("@el.check('CriticalStatusManage:list')")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "获得危急值对象", response = CriticalObjDTO.class)
    })
    public R getCriteriaObj(@RequestParam Long id) {
        return R.ok(tCriticalService.getCriteria(id));
    }

    @Log("危急值状况管理-编辑")
    @ApiOperation("危急值状况管理-编辑，权限码：CriticalStatusManage:list")
    @PostMapping("/edit")
    @PreAuthorize("@el.check('CriticalStatusManage:list')")
    @Klock
    public R editCriteria(@Validated @RequestBody CriticalObjDTO critical) {
        return R.ok(tCriticalService.editCriteria(critical));
    }

    @Log("危急值状况管理-删除")
    @ApiOperation("危急值状况管理-删除，权限码：CriticalStatusManage:list")
    @DeleteMapping
    @PreAuthorize("@el.check('CriticalStatusManage:list')")
    @Klock
    public R delCriteria(@RequestParam Long id) {
        return R.ok(tCriticalService.deleteCriteria(id));
    }

    @Log("危急值状况管理-测试通知")
    @ApiOperation("危急值状况管理-测试通知，权限码：CriticalStatusManage:list")
    @GetMapping("/testNotification")
    @PreAuthorize("@el.check('CriticalStatusManage:list')")
    public R testCriteriaNotification(@RequestParam Long criticalId){
        tCriticalService.testCriteriaNotification(criticalId);
        return R.ok();
    }
}
