package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BSicknessCause;

/**
 * @description 病种原因 服务层
 * @author fang
 * @date 2021-06-17
 */
public interface BSicknessCauseService extends BaseService<BSicknessCause> {



}
