package com.healthappy.modules.system.domain.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.config.databind.FieldBind;
import com.healthappy.modules.system.domain.TPatientPoison;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 批量信息修改VO类
 * @author Jevany
 * @date 2022/5/12 19:37
 */
@Data
@ApiModel("批量信息修改VO类")
public class BatchInfoEditVO implements Serializable {
	/** 流水号 */
	@ApiModelProperty("流水号")
	private String paId;

	/** 姓名 */
	@ApiModelProperty("姓名")
	private String name;

	/** 性别id */
	@ApiModelProperty("性别id")
	@FieldBind(target = "sexText")
	private String sex;

	/** 性别文本 */
	@ApiModelProperty("性别文本")
	@TableField(exist = false)
	private String sexText;

	/** 年龄 */
	@ApiModelProperty("年龄")
	private Integer age;

	/** 单位编号 */
	@ApiModelProperty("单位编号")
	private String companyId;

	/** 单位名称 */
	@ApiModelProperty("单位名称")
	private String companyName;

	/** 部门编号 */
	@ApiModelProperty("部门编号")
	private String departmentId;

	/** 部门名称 */
	@ApiModelProperty("部门名称")
	private String departmentName;

	/** 分组编号 */
	@ApiModelProperty("分组编号")
	private String departmentGroupId;

	/** 分组名称 */
	@ApiModelProperty("分组名称")
	private String departmentGroupName;

	/** 工种编号 */
	@ApiModelProperty("工种编号")
	private String job;

	/** 工种名称 */
	@ApiModelProperty("工种名称")
	private String jobName;

	/** 接害因素 */
	@ApiModelProperty("接害因素")
	private String poisonType;

	/** 接害因素列表 */
	@ApiModelProperty("接害因素列表")
	private List<TPatientPoison> poisonList;

	/** 套餐编号 */
	@ApiModelProperty("套餐编号")
	private String packageId;

	/** 套餐名称 */
	@ApiModelProperty("套餐名称")
	private String packageName;

	/** 总检医生编号 */
	@ApiModelProperty("总检医生编号")
	private String conclusionDocCode;

	/** 总检医生 */
	@ApiModelProperty("总检医生")
	private String conclusionDoc;

	/** 总检时间 */
	@ApiModelProperty("总检时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private String conclusionDate;

	private static final long serialVersionUID = 1L;
}