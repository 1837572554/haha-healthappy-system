package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SearchSicknessRuleCriteria {

    @Query(blurry = "name,jp")
    @ApiModelProperty(name = "blurry",value = "项目名称 = name|简拼 = jp")
    private String blurry;
}
