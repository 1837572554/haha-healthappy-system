package com.healthappy.modules.system.service;

import com.healthappy.modules.system.service.dto.AloneReportPrintCriteria;

import java.util.Map;

public interface AloneReportPrintService {
    /**
     * 获取人员列表-单独报告
     *
     * @param criteria
     * @return java.util.Map〈java.lang.String,java.lang.Object〉
     * @author sqoy
     * @date 2022/2/17 16:06
     */
    Map<String, Object> getList(AloneReportPrintCriteria criteria);

    /**
     * 获取项目列表-单独报告
     *
     * @param PaId
     * @return java.util.Map〈java.lang.String,java.lang.Object〉
     * @author sqoy
     * @date 2022/2/18 16:06
     */
    Map<String, Object> getGroupList(String PaId, String deptId, String groupId);

    /**
     * 获取项目列表-单独报告
     *
     * @param PaId
     * @return java.util.Map〈java.lang.String,java.lang.Object〉
     * @author sqoy
     * @date 2022/2/18 16:06
     */
    Map<String, Object> getItemList(String PaId, String GroupId);

}
