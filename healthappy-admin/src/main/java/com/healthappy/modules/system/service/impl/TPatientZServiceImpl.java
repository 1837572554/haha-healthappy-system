package com.healthappy.modules.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.TPatientZ;
import com.healthappy.modules.system.service.TPatientZService;
import com.healthappy.modules.system.service.dto.TPatientZDto;
import com.healthappy.modules.system.service.dto.TPatientZQueryCriteria;
import com.healthappy.modules.system.service.mapper.TPatientZMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @description 职业体检人员表
 * @author sjc
 * @date 2021-09-8
 */
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class TPatientZServiceImpl extends BaseServiceImpl<TPatientZMapper, TPatientZ> implements TPatientZService {

    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(TPatientZQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<TPatientZ> page = new PageInfo<TPatientZ>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("content", generator.convert(page.getList(), TPatientZDto.class));
        map.put("totalElements", page.getTotal());

        return map;
    }

    @Override
    public List<TPatientZ> queryAll(TPatientZQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(TPatientZDto.class, criteria));
    }

    @Override
    public TPatientZ getByPaId(String paId){
        return baseMapper.selectOne(new LambdaQueryWrapper<TPatientZ>().eq(TPatientZ::getPaId,paId));
    }

    @Override
    public boolean deleteByPaId(Long id) {
        return baseMapper.deleteById(id)>0;
    }


}
