package com.healthappy.modules.system.service;

import com.healthappy.modules.system.domain.vo.AloneChargeMakeBillVo;
import com.healthappy.modules.system.domain.vo.AloneChargeVo;
import com.healthappy.modules.system.domain.vo.AloneTicketVo;
import com.healthappy.modules.system.service.dto.AlonePayCostDetailDto;
import com.healthappy.modules.system.service.dto.PayCostAloneQueryCriteria;

import java.util.Map;

/**
 * @description 个人缴费
 * @author FGQ
 * @date 2022-2-18
 */
public interface AlonePayCostService {


    Map<String,Object> getAll(PayCostAloneQueryCriteria criteria);

    AlonePayCostDetailDto detail(String paId);

    AloneTicketVo checkTicket(AloneTicketVo vo);

    void charge(AloneChargeVo vo);

    AloneTicketVo addCurrentTicket(AloneTicketVo vo);

    /**
     * 补打票据
     * @author YJ
     * @date 2022/4/15 17:18
     */
    void makeBill(AloneChargeMakeBillVo vo);
}
