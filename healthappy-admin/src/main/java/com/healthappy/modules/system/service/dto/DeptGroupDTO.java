package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 科室组合项DTO
 * @author Jevany
 * @date 2022/3/15 16:15
 */
@Data
@ApiModel("科室组合项DTO")
public class DeptGroupDTO implements Serializable {
    /** 科室编号 */
    @ApiModelProperty("科室编号")
    private String deptId;
    /** 科室名称 */
    @ApiModelProperty("科室名称")
    private String deptName;
    /** 组合编号 */
    @ApiModelProperty("组合编号")
    private String groupId;
    /** 组合名称 */
    @ApiModelProperty("组合名称")
    private String groupName;
    /** 数量 */
    @ApiModelProperty("数量")
    private Integer cnt;
}