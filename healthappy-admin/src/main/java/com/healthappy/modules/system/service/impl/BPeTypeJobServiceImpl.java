package com.healthappy.modules.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BPeTypeJob;
import com.healthappy.modules.system.service.BPeTypeJobService;
import com.healthappy.modules.system.service.dto.BPeTypeJobDto;
import com.healthappy.modules.system.service.dto.BPeTypeJobQueryCriteria;
import com.healthappy.modules.system.service.mapper.BPeTypeJobMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BPeTypeJobServiceImpl extends BaseServiceImpl<BPeTypeJobMapper, BPeTypeJob> implements BPeTypeJobService {

    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BPeTypeJobQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BPeTypeJob> page = new PageInfo<BPeTypeJob>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", generator.convert(page.getList(), BPeTypeJobDto.class));
        map.put("totalElements", page.getTotal());
        return map;
    }

    @Override
    public List<BPeTypeJob> queryAll(BPeTypeJobQueryCriteria criteria) {
         return baseMapper.selectList(QueryHelpPlus.getPredicate(BPeTypeJobDto.class, criteria));
    }
}
