package com.healthappy.modules.system.service.dto;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @description 职业史表
 * @author sjc
 * @date 2021-12-15
 */
@Data
public class TRecordDto implements Serializable {

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private Long id;


    /**
     * 体检号
     */
    @NotNull(message = "体检号不能为空")
    @ApiModelProperty("体检号")
    private String paId;

    /**
     * 起止日期
     */
    @ApiModelProperty("起止日期")
    private String startEnd;

    /**
     * 单位
     */
    @ApiModelProperty("单位")
    private String workUnit;

    /**
     * 车间
     */
    @ApiModelProperty("车间")
    private String workshop;

    /**
     * 工种
     */
    @ApiModelProperty("工种")
    private String job;

    /**
     * 危害因素
     */
    @ApiModelProperty("危害因素")
    private String poison;

    /**
     * 防护措施
     */
    @ApiModelProperty("防护措施")
    private String defend;

}
