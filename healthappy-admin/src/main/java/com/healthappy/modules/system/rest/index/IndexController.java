package com.healthappy.modules.system.rest.index;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.healthappy.annotation.AnonymousAccess;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.service.BGroupHdService;
import com.healthappy.modules.system.service.TAppointService;
import com.healthappy.modules.system.service.TPatientService;
import com.healthappy.modules.system.service.dto.AppointDateNumDTO;
import com.healthappy.modules.system.service.dto.AppointRegisterTodayDTO;
import com.healthappy.utils.R;
import com.healthappy.utils.RedisUtil;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 首页
 *
 * @author Jevany
 * @date 2022/3/11 16:58
 */
@Slf4j
@Api(tags = "首页")
@RestController
@AllArgsConstructor
@RequestMapping("/index")
public class IndexController {

	private final TPatientService patientService;
	private final TAppointService appointService;
	private final BGroupHdService groupHdService;


	@Log("今日业务量")
	@ApiOperation("今日业务量")
	@GetMapping("/getAppointRegisterNum")
	@AnonymousAccess
	public R getAppointRegisterNum() {
		AppointRegisterTodayDTO appointRegisterTodayDTO = new AppointRegisterTodayDTO();
		appointRegisterTodayDTO.setRegisterNum(
				patientService.lambdaQuery().apply(" TO_DAYS(sign_date)=TO_DAYS(now()) ").count());
		appointRegisterTodayDTO.setAppointNum(
				appointService.lambdaQuery().apply(" TO_DAYS(appoint_date)=TO_DAYS(now()) ").count());
		appointRegisterTodayDTO.setAppointSignNum(appointService.lambdaQuery()
				.apply(" TO_DAYS(appoint_date)=TO_DAYS(now()) and TO_DAYS(sign_date)= TO_DAYS(now()) ").count());
		appointRegisterTodayDTO.setOtherNum(
				appointRegisterTodayDTO.getRegisterNum() - appointRegisterTodayDTO.getAppointSignNum());
		return R.ok(appointRegisterTodayDTO);
	}

	@Log("各科室业务量")
	@ApiOperation("各科室业务量")
	@GetMapping("/getDeptBuysiness")
	@AnonymousAccess
	public R getDeptBusiness(@RequestParam String date) {
		return R.ok(groupHdService.listDeptBusiness(date));
	}

	@Log("体检计划")
	@ApiOperation("体检计划")
	@PostMapping("/physicalExaminationPlan")
	@ApiImplicitParams(@ApiImplicitParam(name = "timeList", value = "时间段", dataType = "List<Timestamp>"))
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "日期预约数", response = AppointDateNumDTO.class, responseContainer = "List")})
	@AnonymousAccess
	public R physicalExaminationPlan(@RequestBody List<String> timeList) {
		if (CollUtil.isEmpty(timeList) || timeList.size() != 2) {
			return R.error("时间段参数不正确");
		}
		return R.ok(appointService.calcAppointDateNum(timeList));
	}

	@GetMapping("/test")
	@AnonymousAccess
	public R test() {
		Long index = RedisUtil.listLeftPush("test111", "111");
		log.info(index.toString());
		index = RedisUtil.listLeftPush("test111", "222");
		log.info(index.toString());
		log.info("add success");
		return R.ok();
	}

	@GetMapping("/getTest")
	@AnonymousAccess
	public R getTest() {
		Long cnt = RedisUtil.listSize("test111");
		log.info(cnt.toString());
		Object test111 = null;
		if (cnt > 0) {
			test111 = RedisUtil.listRightPop("test111");
			log.info(Convert.toStr(test111));
		}
		return R.ok(StrUtil.toString(test111));
	}

}
