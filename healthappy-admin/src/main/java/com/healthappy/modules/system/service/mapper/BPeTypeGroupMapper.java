package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BPeTypeGroup;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 体检类型组
 * @author yanjun
 * @date 2021-09-03
 */
@Mapper
@Repository
public interface BPeTypeGroupMapper extends CoreMapper<BPeTypeGroup> {
}
