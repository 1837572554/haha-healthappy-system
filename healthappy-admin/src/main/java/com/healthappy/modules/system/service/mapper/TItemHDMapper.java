package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TItemHD;
import com.healthappy.modules.system.service.dto.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 体检人员项目相关Mapper
 * @author sjc
 * @date 2021-06-29
 */
@Mapper
@Repository
public interface TItemHDMapper extends CoreMapper<TItemHD> {

    /**
     * 根据体检号删除该人员的组合项目
     * @param paId
     * @return
     */
    boolean deleteByPaId(@Param("paId") String paId,@Param("tenantId") String tenantId);

    /**
     * 根据体检号和组合项目id进行删除
     * @param paId
     * @param groupId
     * @return
     */
    boolean deleteByPaIdAndGroupId(@Param("paId") String paId,@Param("groupId")String groupId,@Param("tenantId") String tenantId);



    List<TItemHD> all(@Param("criteria") TItemHDQueryCriteria criteria,@Param("tenantId") String tenantId);

    /**
     * 根据条件获取体检人员对应的项目结果
     * @author YJ
     * @date 2022/3/2 10:06
     * @param criteria
     * @param tenantId
     * @return java.util.List〈com.healthappy.modules.system.service.dto.DataQueryDto〉
     */
    List<DataQueryDto> getGroupPanelHd(@Param("criteria") DataQueryCriteria criteria, @Param("tenantId") String tenantId);

    /**
     * 根据体检号获取对应的组合项目条码，用于条码的生成
     * @param paId
     * @param tenantId
     * @return
     */
    List<TItemHD> getBarcodeGroup(@Param("paId") String paId,@Param("tenantId") String tenantId);

    /**
     * 更新指定条码名称对应的项目的条码
     * @param barcode 条码号
     * @param barcodeName 条码名称
     * @param type 组合项目类型
     * @return
     */
    boolean updateBarcode(@Param("paId") String paId,@Param("barcode") String barcode,@Param("barcodeName") String barcodeName,@Param("type") String type);


    /**
     * 修改TypeJ、TypeZ和deptId
     * @author YJ
     * @date 2021/12/24 16:18
     * @param itemHd
     * @return java.lang.Integer 返回受影响的行数
     */
    Integer updateItemTypeAndDeptId(@Param("itemHd") TItemHD itemHd);

    /**
     * 删除之前清条码
     * @author YJ
     * @date 2021/12/24 17:52
     * @param paId 体检号
     * @param groupIdList 组合编号列表 不能为空，空就没意义
     * @param tenantId
     * @return java.lang.Integer 返回受影响的行数
     */
    Integer delBeforeClearBarcode(@Param("paId") String paId,@Param("groupIdList") List<String> groupIdList,@Param("tenantId") String tenantId);

    /**
     * 添加新项目，去除同条码名称的Barcode
     * @author YJ
     * @date 2021/12/27 10:02
     * @param paId  体检号
     * @param tenantId 租户编号
     * @return java.lang.Integer
     */
    @SqlParser(filter = true)
    Integer addItemClearWithNameBarcode(@Param("paId") String paId,@Param("tenantId") String tenantId);

    /**
     * 添加新项目，去除同条码名称的Barcode
     * @author YJ
     * @date 2021/12/27 10:02
     * @param paId  体检号
     * @param tenantId 租户编号
     * @return
     */
    List<AloneReportPrintGroupDto> getAloneReportPrintGroup(@Param("paId") String paId,@Param("deptId") String deptId,@Param("groupId") String groupId, @Param("tenantId") String tenantId);

    /**
     * 添加新项目，去除同条码名称的Barcode
     * @author YJ
     * @date 2021/12/27 10:02
     * @param paId  体检号
     * @param tenantId 租户编号
     * @return
     */
    List<AloneReportPrintItemDto> getAloneReportPrintItem(@Param("paId") String paId, @Param("GroupId") String GroupId,@Param("tenantId") String tenantId);

    /**
     * 添加新项目，去除同条码名称的Barcode
     * @author YJ
     * @date 2021/12/27 10:02
     * @param AppointId  体检号
     * @param tenantId 租户编号
     * @return
     */
    List<GetAppointGroupDto> queryAppointGroup(@Param("AppointId") String AppointId, @Param("tenantId") String tenantId);

    /**
     * 清除同一个PaId中相同GroupId
     * @param paIds
     */
    @SqlParser(filter = true)
    void repeatGroupId(@Param("paIds") List<String> paIds,@Param("tenantId") String tenantId);
}
