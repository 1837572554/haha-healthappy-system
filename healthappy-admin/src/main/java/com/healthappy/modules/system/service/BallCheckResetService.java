package com.healthappy.modules.system.service;

import com.healthappy.modules.system.domain.vo.BallCheckUploadVo;

import java.util.List;
import java.util.Map;

/**
 * @description 团检备单 服务层
 * @author fang
 * @date 2021-06-17
 */
public interface BallCheckResetService {

	/**
	 * 上传
	 * @param registrationType 1预约 0登记
	 * @param appointDateStart
	 * @param appointDateEnd
	 * @param buildList
	 * @param fileName
	 * @return java.util.List〈java.util.Map〈java.lang.String,java.lang.Object〉〉
	 */
	List<Map<String, Object>> upload(Integer registrationType, String appointDateStart, String appointDateEnd,
			List<BallCheckUploadVo> buildList, String fileName);
}
