package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.healthappy.config.SeedIdGenerator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @description 套餐项目表
 * @author sjc
 * @date 2021-07-05
 */
@Data
@ApiModel("套餐项目表")
@TableName("B_Package_DT")
@SeedIdGenerator
public class BPackageDT implements Serializable  {
    private static final long serialVersionUID = 1L;


    /**
     *套餐id
     */
    @ApiModelProperty("套餐id")
    @TableId(type = IdType.INPUT)
    private String id;


    /**
     * 套餐编号
     */
    @ApiModelProperty("套餐编号")
    private String packageId;

    /**
     * 组合编号
     */
    @ApiModelProperty("组合编号")
    private String groupId;

    /**
     * 组合编号
     */
    @TableField(exist = false)
    @ApiModelProperty("组合名称")
    private String groupName;

    /**
     * 是否是职检项目
     */
    @ApiModelProperty("是否是职检项目")
    private String typeZ;

    /**
     * 是否是普检项目
     */
    @ApiModelProperty("是否是普检项目")
    private String typeJ;

    /**
     * 折扣
     */
    @ApiModelProperty("折扣")
    private BigDecimal discount;

    /**
     * 类型 0--基础项目,1--附加项目
     */
    @ApiModelProperty("类型 0--基础项目,1--附加项目")
    private String type;

    @ApiModelProperty("原价")
    private BigDecimal price;

    @ApiModelProperty("实际价格")
    private BigDecimal cost;


    @TableField(exist=false)
    @ApiModelProperty("组合项目的适用性别")
    private String sex;

    /** 组合项目的科室编号 */
    @TableField(exist=false)
    @ApiModelProperty("组合项目的科室编号")
    private String deptId;

    /**
     * 医疗机构ID(U_Hospital_Info的ID)
     */
    @ApiModelProperty("医疗机构ID")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;

}
