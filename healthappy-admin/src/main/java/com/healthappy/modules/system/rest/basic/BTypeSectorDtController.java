package com.healthappy.modules.system.rest.basic;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BTypeSectorDt;
import com.healthappy.modules.system.domain.vo.BTypeSectorDtVo;
import com.healthappy.modules.system.service.BTypeSectorDtService;
import com.healthappy.modules.system.service.dto.BTypeSectorDtQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 行业设置设置 控制器
 * @author FGQ
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "基础管理：行业设置")
@RestController
@AllArgsConstructor
@RequestMapping("/BTypeSectorDt")
public class BTypeSectorDtController {

    private final BTypeSectorDtService bTypeSectorDtService;

    @Log("查询行业设置")
    @ApiOperation("查询行业设置，权限码：BTypeSectorDt:list")
    @GetMapping
    public R list(BTypeSectorDtQueryCriteria criteria, Pageable pageable) {
        return R.ok(bTypeSectorDtService.queryAll(criteria, pageable));
    }

    @Log("新增|修改行业设置")
    @ApiOperation("新增|修改行业设置，权限码：BTypeSectorDt:add")
    @PostMapping
    @PreAuthorize("@el.check('BTypeSectorDt:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BTypeSectorDtVo resources) {
        if(checkNameRepeat(resources)){
            return R.error("行业名称不能重复");
        }
        return R.ok(bTypeSectorDtService.saveOrUpdate(BeanUtil.copyProperties(resources, BTypeSectorDt.class)));
    }

    @Log("删除行业设置")
    @ApiOperation("删除行业设置，权限码：BTypeSectorDt:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('BTypeSectorDt:delete')")
    @Klock
    public R remove(@RequestBody Set<Integer> ids) {
        return R.ok(bTypeSectorDtService.removeByIds(ids));
    }

    private boolean checkNameRepeat(BTypeSectorDtVo resources) {
        return bTypeSectorDtService.lambdaQuery().eq(BTypeSectorDt::getName, resources.getName())
                .ne(ObjectUtil.isNotNull(resources.getId()),BTypeSectorDt::getId,resources.getId()).count() > 0;
    }
}
