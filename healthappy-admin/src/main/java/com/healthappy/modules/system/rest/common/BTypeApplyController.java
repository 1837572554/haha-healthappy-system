package com.healthappy.modules.system.rest.common;

import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BTypeApply;
import com.healthappy.modules.system.service.BTypeApplyService;
import com.healthappy.modules.system.service.dto.BTypeApplyQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @author FGQ
 * @description 申请类型
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "默认基础设置：申请类型")
@ApiSort(value = 1)
@RestController
@AllArgsConstructor
@RequestMapping("/BTypeApply")
public class BTypeApplyController {

    private final BTypeApplyService typeApplyService;

    @Log("查询申请类型")
    @ApiOperation("查询申请类型，权限码：BTypeApply:list")
    @GetMapping("/all")
    public R list(BTypeApplyQueryCriteria criteria) {
        return R.ok(typeApplyService.list(QueryHelpPlus.getPredicate(BTypeApply.class, criteria)));
    }

    @Log("新增|申请类型")
    @ApiOperation("新增|申请类型，权限码：admin")
    @PostMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BTypeApply resources) {
        return R.ok(typeApplyService.saveOrUpdate(resources));
    }

    @Log("删除申请类型")
    @ApiOperation("删除申请类型，权限码：admin")
    @DeleteMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R remove(@RequestBody Set<Integer> ids) {
        return R.ok(typeApplyService.removeByIds(ids));
    }
}
