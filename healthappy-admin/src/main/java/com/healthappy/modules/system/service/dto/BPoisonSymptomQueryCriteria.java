package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 毒害对应症状细表
 * @author sjc
 * @date 2021-08-10
 */
@Data
public class BPoisonSymptomQueryCriteria {

    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("症状id")
    private Long symptomId;

    /**
     * 体检类别
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("type")
    private String type;

    /**
     * 危害因素id
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("危害因素id")
    private String poisonId;


}
