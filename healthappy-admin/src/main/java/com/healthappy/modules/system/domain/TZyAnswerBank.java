package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.config.SeedIdGenerator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author Jevany
 * @date 2021/12/8 18:55
 * @desc 体检者问卷答案库
 */
@Data
@SeedIdGenerator
@TableName("T_ZY_ANSWER_BANK")
@ApiModel("体检者问卷答案库")
public class TZyAnswerBank implements Serializable {

    /** 答案库编号 answer_bank_id */
    @ApiModelProperty("答案库编号 answer_bank_id")
    @TableId(type = IdType.INPUT)
    private String id;

    /** 体检号 */
    @ApiModelProperty("体检号")
    private String paId;

    /** 库编号 */
    @ApiModelProperty("库编号")
    private Integer bankId;

    /** 操作医生 */
    @ApiModelProperty("操作医生")
    private String operationDoctor;

    /** 创建日期 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(fill = FieldFill.INSERT)
    private Timestamp createTime;

    /** 医疗机构ID(U_Hospital_Info的ID) */
    @ApiModelProperty("医疗机构ID(U_Hospital_Info的ID)")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;
}
