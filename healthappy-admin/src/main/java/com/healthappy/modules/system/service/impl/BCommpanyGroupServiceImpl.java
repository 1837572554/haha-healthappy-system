package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNode;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BCommpanyGroup;
import com.healthappy.modules.system.service.BCommpanyGroupService;
import com.healthappy.modules.system.service.dto.BCommpanyGroupQueryCriteria;
import com.healthappy.modules.system.service.mapper.BCommpanyGroupMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BCommpanyGroupServiceImpl extends BaseServiceImpl<BCommpanyGroupMapper, BCommpanyGroup> implements BCommpanyGroupService {

    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BCommpanyGroupQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BCommpanyGroup> page = new PageInfo<BCommpanyGroup>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", page.getList());
        map.put("totalElements", page.getTotal());
        return map;
    }

    @Override
    public List<BCommpanyGroup> queryAll(BCommpanyGroupQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BCommpanyGroup.class, criteria));
    }
}
