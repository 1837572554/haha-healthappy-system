package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BPeTypeDT;



/**
 * @description 体检分类 服务层
 * @author yanjun
 * @date 2021年9月3日 13:09:52
 */
public interface BPeTypeDTService extends BaseService<BPeTypeDT> {
}


