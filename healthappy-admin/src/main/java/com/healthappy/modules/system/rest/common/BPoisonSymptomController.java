package com.healthappy.modules.system.rest.common;

import cn.hutool.core.collection.CollUtil;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BPoisonSymptom;
import com.healthappy.modules.system.domain.vo.BPoisonSymptomVo;
import com.healthappy.modules.system.service.BPoisonSymptomService;
import com.healthappy.modules.system.service.dto.BPoisonSymptomQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @description 毒害对应症状细表 控制器
 * @author sjc
 * @date 2021-08-10
 */
@Slf4j
@Api(tags = "默认基础数据：毒害对应症状细表")
@RestController
@Validated
@AllArgsConstructor
@RequestMapping("/BPoisonSymptom")
public class BPoisonSymptomController {

    private final BPoisonSymptomService bPoisonSymptomService;
    private final IGenerator generator;

    @Log("分页查询毒害对应症状细表")
    @ApiOperation("分页查询毒害对应症状细表，权限码：BPoisonSymptom:list")
    @GetMapping
    public R listPage(BPoisonSymptomQueryCriteria criteria,  @PageableDefault(sort = {"poison_id"}) Pageable pageable) {
        return R.ok(bPoisonSymptomService.queryAll(criteria, pageable));
    }

    @Log("新增|修改毒害对应症状细表")
    @ApiOperation("新增|修改毒害对应症状细表，权限码：admin")
    @PostMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R saveOrUpdate(@Valid @RequestBody List<BPoisonSymptomVo> resources) {
        if(CollUtil.isEmpty(resources)){
            throw new BadRequestException("参数不能为空");
        }
        String poisonId = resources.get(0).getPoisonId();
        deleteByPoId(poisonId);
        bPoisonSymptomService.saveBatch(generator.convert(resources, BPoisonSymptom.class));
        return R.ok();
    }

    @Log("根据危害因素id删除")
    @ApiOperation("根据危害因素id删除，权限码：admin")
    @DeleteMapping("/delByPoId")
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R deleteByPoId(@RequestParam("poisonId") String poisonId) {
        if(bPoisonSymptomService.deleteByPoId(poisonId))
            return R.ok();
        return R.error(500,"方法异常");
    }

    @Log("根据症状id删除")
    @ApiOperation("根据症状id删除，权限码：admin")
    @DeleteMapping("/delBySymId")
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R deleteBySymId(@RequestParam("symptomId") Long symptomId) {
        if(bPoisonSymptomService.deleteBySymId(symptomId))
            return R.ok();
        return R.error(500,"方法异常");
    }

    @Log("根据症状id集合批量删除")
    @ApiOperation("根据症状id集合批量删除，权限码：admin")
    @DeleteMapping("/delBySymIds")
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R deleteBySymIds(@RequestBody List<Long> symptomIds) {
        if(bPoisonSymptomService.deleteBySymIds(symptomIds))
            return R.ok();
        return R.error(500,"方法异常");
    }
}
