package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 从业体检行业
 * @author fang
 * @date 2021-06-21
 */
@Data
@TableName("B_Type_Sector_HD")
@ApiModel("从业体检行业")
public class BTypeSectorHd implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
     * id
     */
    @ApiModelProperty("id")
    private Long id;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    private String name;

    /**
     * 排序
     */
    @ApiModelProperty("排序")
    private Integer dispOrder;

    /**
     * 名称
     */
    @ApiModelProperty("编号")
    private String code;

    /**
     * 医疗机构id(u_hospital_info的id)
     */
    @ApiModelProperty("医疗机构id(u_hospital_info的id)")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;
}
