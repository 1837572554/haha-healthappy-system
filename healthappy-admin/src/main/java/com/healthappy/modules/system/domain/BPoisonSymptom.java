package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 毒害对应症状细表
 * @author sjc
 * @date 2021-08-10
 */
@Data
@TableName("B_Poison_Symptom")
@ApiModel("毒害对应症状细表")
public class BPoisonSymptom implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 症状id
     */
    @ApiModelProperty("症状id")
    private Long symptomId;

    /**
     * 症状名称
     */
    @TableField(exist=false)
    @ApiModelProperty("症状名称")
    private String symptomName;

    /**
     * 体检类别
     */
    @ApiModelProperty("type")
    private String type;

    /**
     * 危害因素id
     */
    @ApiModelProperty("危害因素id")
    private String poisonId;


}
