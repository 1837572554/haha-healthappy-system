package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BTypeAppoint;

/**
 * Created with IntelliJ IDEA. Created by yutang 2021/9/8 0008  9:18 Description:
 */
public interface BTypeAppointService extends BaseService<BTypeAppoint> {

}
