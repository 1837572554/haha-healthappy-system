package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.*;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * 日期预约数
 * @author Jevany
 * @date 2022/3/16 12:50
 */
@Data
@ApiModel("日期预约数")
public class AppointDateNumDTO {

    /** 日期 */
    @ApiModelProperty("日期")
    private String day;

    /** 数量 */
    @ApiModelProperty("数量")
    private Integer num;
}