package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BGroupHd;
import com.healthappy.modules.system.service.dto.DeptGroupDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 项目组合
 * @author fang
 * @date 2021-06-17
 */
@Mapper
@Repository
public interface BGroupHdMapper extends CoreMapper<BGroupHd> {

    /**
     * 获取指定日期的（项目类型：影像、B超、仪器）预约，登记的科室,项目
     * @author YJ
     * @date 2022/3/15 16:40
     * @param dateStr
     * @param tenantId
     * @return java.util.List〈com.healthappy.modules.system.service.dto.DeptBusinessDTO〉
     */
    @SqlParser(filter = true)
    List<DeptGroupDTO> listDeptBusiness(@Param("dateStr") String dateStr, @Param("tenantId") String tenantId);

    /**
     * 获取指定日期的（项目类型：影像、B超、仪器）登记的科室，项目数量
     * @author YJ
     * @date 2022/3/15 18:09
     * @param dateStr
     * @param tenantId
     * @return java.util.List〈com.healthappy.modules.system.service.dto.DeptGroupDTO〉
     */
    @SqlParser(filter = true)
    List<DeptGroupDTO> listRegister(@Param("dateStr") String dateStr, @Param("tenantId") String tenantId);

    /**
     * 获取指定日期的（项目类型：影像、B超、仪器）预约的科室，项目数量
     * @author YJ
     * @date 2022/3/15 18:09
     * @param dateStr
     * @param tenantId
     * @return java.util.List〈com.healthappy.modules.system.service.dto.DeptGroupDTO〉
     */
    @SqlParser(filter = true)
    List<DeptGroupDTO> listAppoint(@Param("dateStr") String dateStr, @Param("tenantId") String tenantId);

    /**
     * 获取指定日期的（项目类型：影像、B超、仪器）预约的科室，项目数量
     * @param dateStr
     * @param tenantId
     * @return
     */
    @SqlParser(filter = true)
    List<DeptGroupDTO> listAppointSign(@Param("dateStr") String dateStr, @Param("tenantId") String tenantId);
}
