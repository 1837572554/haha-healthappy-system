package com.healthappy.modules.system.rest.basic;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BArea;
import com.healthappy.modules.system.domain.vo.BAreaVo;
import com.healthappy.modules.system.service.BAreaService;
import com.healthappy.modules.system.service.dto.BAreaQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 区域表（阿斯利康）控制器
 * @author fang
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "基础管理：所属区域设置")
@RestController
@AllArgsConstructor
@RequestMapping("/bArea")
public class BAreaController {

    private final BAreaService bAreaService;

    @Log("查询地区管理")
    @ApiOperation("查询地区管理，权限码：bArea:list")
    @GetMapping
    @PreAuthorize("@el.check('bArea:list')")
    public R list(BAreaQueryCriteria criteria, Pageable pageable) {
        return R.ok(bAreaService.queryAll(criteria, pageable));
    }

    @Log("新增|修改地区管理")
    @ApiOperation("新增|修改地区管理，权限码：bArea:add")
    @PostMapping
    @PreAuthorize("@el.check('bArea:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BAreaVo resources) {
        if(checkNameRepeat(resources)){
            return R.error("地区名称不能重复");
        }
        if(StrUtil.isAllBlank(resources.getHiCode(),resources.getCode()) && resources.getHiCode().equals(resources.getCode())){
            return R.error("上级区域编码不能和本级编码一样");
        }
        return R.ok(bAreaService.saveOrUpdate(BeanUtil.copyProperties(resources, BArea.class)));
    }

    @Log("删除地区")
    @ApiOperation("删除地区，权限码：bArea:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('bArea:delete')")
    @Klock
    public R remove(@RequestBody Set<Integer> ids) {
        checkData(ids);
        return R.ok(bAreaService.removeByIds(ids));
    }

    @Log("查询地区管理 tree")
    @ApiOperation("查询地区管理 tree，权限码：bArea:list")
    @GetMapping("/tree")
    public R tree(BAreaQueryCriteria criteria) {
        criteria.setIsEnable("1");
        return R.ok(bAreaService.tree(criteria));
    }

    private boolean checkNameRepeat(BAreaVo resources) {
        return bAreaService.lambdaQuery().eq(BArea::getHiCode,resources.getHiCode()).eq(BArea::getName, resources.getName())
                .ne(ObjectUtil.isNotNull(resources.getId()),BArea::getId,resources.getId()).count() > 0;
    }

    private void checkData(Set<Integer> ids) {
        if (CollUtil.isNotEmpty(ids)) {
            Integer count = bAreaService.checkExistHiCode(ids);
            if (count > 0) {
                throw new BadRequestException("该数据有下级，请先删除下级");
            }
        }
    }
}
