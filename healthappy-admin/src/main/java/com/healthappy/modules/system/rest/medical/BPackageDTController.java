package com.healthappy.modules.system.rest.medical;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BPackageDT;
import com.healthappy.modules.system.domain.vo.BPackageDTVo;
import com.healthappy.modules.system.service.BPackageDTService;
import com.healthappy.modules.system.service.dto.BPackageDTQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @description 套餐项目表 控制器
 * @author sjc
 * @date 2021-07-05
 */
@Validated
@Slf4j
@Api(tags = "体检管理：套餐项目")
@RestController
@AllArgsConstructor
@RequestMapping("/BPackageDT")
public class BPackageDTController {

    private final BPackageDTService bPackageDTService;
    private final IGenerator generator;

    @Log("查询套餐项目")
    @ApiOperation("查询套餐项目，权限码：BPackageDT:list")
    @GetMapping
    public R list(BPackageDTQueryCriteria criteria,@PageableDefault(size = 2000) Pageable pageable) {
        return R.ok(bPackageDTService.queryAll(criteria,pageable));
    }

    @Log("新增|修改 一个单独的套餐项目")
    @ApiOperation("新增|修改  一个单独的套餐项目，权限码：BPackageDT:add")
    @PostMapping
    @PreAuthorize("@el.check('BPackageDT:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BPackageDTVo resources) {
        BPackageDT bPackageDT = new BPackageDT();
        BeanUtil.copyProperties(resources,bPackageDT);
        if(bPackageDTService.saveOrUpdate(bPackageDT))
            return R.ok();
        return R.error(500,"方法异常");
    }

    @Log("新增|修改 批量的套餐项目")
    @ApiOperation("新增|修改  批量的套餐项目，权限码：BPackageDT:add")
    @PostMapping("/list")
    @PreAuthorize("@el.check('BPackageDT:add')")
    @Klock
    public R save(@Valid @RequestBody List<BPackageDTVo> resources) {
        if(CollUtil.isEmpty(resources)){
            throw new BadRequestException("参数不能为空");
        }
        String packageId=resources.get(0).getPackageId();
        //先删除，后添加
        remove(packageId);
        List<BPackageDT> convert = generator.convert(resources, BPackageDT.class);

        if(bPackageDTService.saveBatch(convert))
            return R.ok();
        return R.error(500,"方法异常");
    }

    @Log("根据套餐id删除套餐项目")
    @ApiOperation("根据套餐id删除套餐项目，权限码：BPackageDT:delete")
    @DeleteMapping("{packageId}")
    @PreAuthorize("@el.check('BPackageDT:delete')")
    @Klock
    public R remove(@PathVariable String packageId) {
        LambdaQueryWrapper<BPackageDT> queryWrapper = new LambdaQueryWrapper ();
        queryWrapper.eq(BPackageDT::getPackageId,packageId);
        if(bPackageDTService.remove(queryWrapper))
            return R.ok();
        return R.error(500,"方法异常");
    }
}
