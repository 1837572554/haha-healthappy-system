package com.healthappy.modules.system.service.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.healthappy.modules.system.domain.DataRenewLog;

public interface DataRenewLogMapper extends BaseMapper<DataRenewLog> {

}
