package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.modules.system.domain.BPackageDT;
import com.healthappy.modules.system.domain.BPackageHD;
import com.healthappy.modules.system.domain.vo.BPackageDTVo;
import com.healthappy.modules.system.domain.vo.BPackageHDVo;
import com.healthappy.modules.system.service.BPackageDTService;
import com.healthappy.modules.system.service.BPackageHDService;
import com.healthappy.modules.system.service.dto.BPackageHDQueryCriteria;
import com.healthappy.modules.system.service.mapper.BPackageHDMapper;
import com.healthappy.modules.system.service.wrapper.BPackageHDWrapper;
import com.healthappy.utils.SecurityUtils;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * 套餐表服务层实现层
 * @author sjc
 * @date 2021-07-2
 */
@Service
@AllArgsConstructor//有自动注入的功能
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BPackageHDServiceImpl extends BaseServiceImpl<BPackageHDMapper, BPackageHD> implements BPackageHDService {

    private final BPackageDTService packageDTService;

    @Override
    public Map<String, Object> queryAll(BPackageHDQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        return BPackageHDWrapper.build().pageVO(new PageInfo<BPackageHD>(queryAll(criteria)));
    }

    @Override
    public List<BPackageHD> queryAll(BPackageHDQueryCriteria criteria) {
        List<String> sexList=new ArrayList<>();

        if(StrUtil.isNotBlank(criteria.getSex())){
            sexList.add("0");
            sexList.add(criteria.getSex());
            criteria.setSexList(sexList);
        }

        return baseMapper.selectList(QueryHelpPlus.getPredicate(BPackageHD.class, criteria));
    }

    @Override
    public void saveOrUpdatePackage(BPackageHDVo resources) {
        BPackageHD bPackageHD = BeanUtil.copyProperties(resources, BPackageHD.class);
        List<BPackageDTVo> bPackageDt = resources.getBPackageDt();
        if(ObjectUtil.isNull(resources.getId())){
            bPackageHD.setUpdateBy(SecurityUtils.getUserId());
            bPackageHD.setUpdateTime(new DateTime().toTimestamp());
            baseMapper.insert(bPackageHD);
        }else {
            baseMapper.updateById(bPackageHD);
            //删除 套餐中所有项目
            packageDTService.remove(Wrappers.<BPackageDT>lambdaUpdate().eq(BPackageDT::getPackageId,bPackageHD.getId()));
        }
        packageDTService.saveBatch(bPackageDt.stream().peek(x-> x.setPackageId(bPackageHD.getId())).map(k -> BeanUtil.copyProperties(k, BPackageDT.class)).collect(Collectors.toList()));
    }
}
