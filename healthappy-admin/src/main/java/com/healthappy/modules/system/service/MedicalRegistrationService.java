package com.healthappy.modules.system.service;

import com.healthappy.modules.system.service.dto.MedicalRegistrationDto;
import com.healthappy.modules.system.service.dto.RegisterProDto;

import java.util.List;

/**
 * @description 体检登记 服务层
 * @author fang
 * @date 2021-06-17
 */
public interface MedicalRegistrationService {


    /**
     * 组装页面返回的参数
     * @param id
     * @return
     */
    MedicalRegistrationDto buildBean(String id);

    /**
     * 组装项目
     * @param departmentGroupHdId
     * @return
     */
    List<RegisterProDto> buildProject(Long departmentGroupHdId);

    /**
     * 获得登记后的组合项目
     * @param paId
     * @return
     */
    List<RegisterProDto> getList(String paId);
}
