package com.healthappy.modules.system.rest.basic;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BFeeType;
import com.healthappy.modules.system.domain.vo.BFeeTypeVo;
import com.healthappy.modules.system.service.BFeeTypeService;
import com.healthappy.modules.system.service.dto.BFeeTypeQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 区域表（阿斯利康）控制器
 * @author FGQ
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "基础管理：费别设置")
@RestController
@AllArgsConstructor
@RequestMapping("/bFeeType")
public class BFeeTypeController {

    private final BFeeTypeService bFeeTypeService;

    @Log("查询费别")
    @ApiOperation("查询费别，权限码：bFeeType:list")
    @GetMapping
    public R list(BFeeTypeQueryCriteria criteria, Pageable pageable) {
        return R.ok(bFeeTypeService.queryAll(criteria, pageable));
    }

    @Log("新增|修改费别")
    @ApiOperation("新增|修改费别，权限码：bFeeType:add")
    @PostMapping
    @PreAuthorize("@el.check('bFeeType:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BFeeTypeVo resources) {
        if(checkNameRepeat(resources)){
           return R.error("费别名称不能重复");
        }
        return R.ok(bFeeTypeService.saveOrUpdate(BeanUtil.copyProperties(resources, BFeeType.class)));
    }

    @Log("删除费别")
    @ApiOperation("删除费别，权限码：bFeeType:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('bFeeType:delete')")
    @Klock
    public R remove(@RequestBody Set<Integer> ids) {
        return R.ok(bFeeTypeService.removeByIds(ids));
    }

    private boolean checkNameRepeat(BFeeTypeVo resources) {
        return  bFeeTypeService.lambdaQuery().eq(BFeeType::getName, resources.getName())
                .ne(ObjectUtil.isNotNull(resources.getId()),BFeeType::getId,resources.getId()).count() > 0;
    }
}
