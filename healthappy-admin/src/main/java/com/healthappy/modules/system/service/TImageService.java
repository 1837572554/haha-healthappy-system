package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TImage;
import com.healthappy.modules.system.domain.vo.Base64ImgVo;
import com.healthappy.modules.system.domain.vo.TImageVo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author Jevany
 * @date 2021/11/30 16:59
 * @desc
 */
public interface TImageService extends BaseService<TImage> {

    /**
     * 上传文件
     * @param multipartFile /
     * @param userName /
     * @return /
     */
    TImageVo upload(MultipartFile multipartFile, String userName);

    /**
     * 上传文件集合
     * @param multipartFile /
     * @param userName /
     * @return /
     */
    List<TImageVo> uploads(MultipartFile[] multipartFile, String userName);

    /**
     * 上传Base64对象
     * @author YJ
     * @date 2021/12/6 19:18
     * @param base64ImgVo
     * @param userName
     * @return com.healthappy.modules.system.domain.vo.TImageVo
     */
    TImageVo uploadBase64(Base64ImgVo base64ImgVo, String userName);

    /**
     * 上传Base64文件集合
     * @author YJ
     * @date 2021/12/6 19:59
     * @param base64ImgVos
     * @param userName
     * @return java.util.List〈com.healthappy.modules.system.domain.vo.TImageVo〉
     */
    List<TImageVo> uploadBase64List(List<Base64ImgVo> base64ImgVos, String userName);
}
