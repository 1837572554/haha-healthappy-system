package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author sjc
 * @date 2021-06-29
 */
@Data
public class TItemHDVo {

    /**
     * 体检号
     */
    //@NotNull(message = "体检号不能为空")
    @ApiModelProperty("体检号")
    private String paId;

    /**
     * 组合编号
     */
    @NotBlank(message = "组合编号不能为空")
    @ApiModelProperty("组合编号")
    private String groupId;

    /**
     * 科室编号
     */
    @ApiModelProperty("科室编号")
    private String deptId;

    /**
     * 组合名称
     */
    @ApiModelProperty("组合名称")
    private String groupName;

    /**
     * 结果判定
     */
    @ApiModelProperty("结果判定")
    private String resultMark;

    /**
     * 结果
     */
    @ApiModelProperty("结果")
    private String result;

    /**
     * 描述
     */
    @ApiModelProperty("描述")
    private String resultDesc;
    /**
     * 是否是职业项目
     */
    @ApiModelProperty("是否是职业项目")
    private String typeZ;

    /**
     * 是否是健康项目
     */
    @ApiModelProperty("是否是健康项目")
    private String typeJ;

    /**
     * 类型
     */
    @ApiModelProperty("类型")
    private String type;

    /**
     * 收费
     */
    @ApiModelProperty("收费")
    private BigDecimal price;

    /**
     * 折扣
     */
    @ApiModelProperty("折扣")
    private BigDecimal discount;

    /**
     * 优惠后价格
     */
    @ApiModelProperty("优惠后价格")
    private BigDecimal cost;

    /**
     * 是否附加项目
     */
    @ApiModelProperty("是否附加项目")
    private String addition;

    /**
     * 付费方式 0自费 1统收
     */
    @ApiModelProperty("付费方式 0自费 1统收")
    private String payType;

    /**
     * 发票id
     */
    @ApiModelProperty("发票id")
    private Long invoiceId;

    /**
     * 是否选检项目
     */
    @ApiModelProperty("是否选检项目")
    private String choose;


    /**
     * 复查
     */
    @ApiModelProperty("复查")
    private Integer review;

    /**
     * 项目登记医生
     */
    @ApiModelProperty("项目登记医生")
    private String registerDoc;


    /**
     * 条码名称
     */
    @ApiModelProperty("条码名称")
    private String barcodeName;

    /**
     * His收费主表的Guid
     */
    @ApiModelProperty("His收费主表的Guid")
    private Long hisFheadId;

    /**
     * 减免标志
     */
    @ApiModelProperty("减免标志")
    private String free;

    /**
     * 上机状态
     */
    @ApiModelProperty("上机状态")
    private String practice;

    /**
     * 登记状态
     */
    @ApiModelProperty("登记状态")
    private String registerState;

    /**
     * 是否退费
     */
    @ApiModelProperty("是否退费")
    private String chargeReturn;


    /**
     * 体检收费单头表主健ID
     */
    @ApiModelProperty("体检收费单头表主健ID")
    private Long fheadId;
}
