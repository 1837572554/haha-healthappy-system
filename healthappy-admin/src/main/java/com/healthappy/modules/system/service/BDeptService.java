package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BDept;
import com.healthappy.modules.system.service.dto.BDeptQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @description 部门 服务层
 * @author fang
 * @date 2021-06-17
 */
public interface BDeptService extends BaseService<BDept> {


    Map<String, Object> queryAll(BDeptQueryCriteria criteria, Pageable pageable);


    /**
     * 查询数据分页
     * @param criteria 条件
     * @return Map<String, Object>
     */
    List<BDept> queryAll(BDeptQueryCriteria criteria);


    /**
     * 获取启用科室且下级存在细项
     * @author YJ
     * @date 2021/12/20 15:45
     * @return java.util.List〈com.healthappy.modules.system.domain.BDept〉
     */
    List<BDept> getDeptEnableAndExistsItem();

    /**
     * 检测科室是否被关联细项、组合项目
     * @author YJ
     * @date 2021/12/22 11:59
     * @param ids
     * @return java.lang.String
     */
    String checkExistsAssociate(Set<Integer> ids);


}
