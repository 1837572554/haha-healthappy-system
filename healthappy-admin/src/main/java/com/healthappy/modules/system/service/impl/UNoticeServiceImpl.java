package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateTime;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.UNotice;
import com.healthappy.modules.system.domain.UNoticeDept;
import com.healthappy.modules.system.domain.vo.UNoticeVo;
import com.healthappy.modules.system.service.UNoticeDeptService;
import com.healthappy.modules.system.service.UNoticeService;
import com.healthappy.modules.system.service.dto.UNoticeQueryCriteria;
import com.healthappy.modules.system.service.mapper.UNoticeMapper;
import com.healthappy.utils.CacheConstant;
import com.healthappy.utils.SecurityUtils;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
@CacheConfig(cacheNames = CacheConstant.UNOTICE)
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class UNoticeServiceImpl extends BaseServiceImpl<UNoticeMapper, UNotice> implements UNoticeService {

    private final UNoticeDeptService uNoticeDeptService;
    private final IGenerator generator;

    @Override
    @Cacheable
    public Map<String, Object> queryAll(UNoticeQueryCriteria criteria, Pageable pageable) {

		getPage(pageable);
		PageInfo<UNotice> page = new PageInfo<UNotice>(queryAll(criteria));
		Map<String, Object> map = new LinkedHashMap<>(2);
		map.put("content", generator.convert(page.getList(), UNotice.class));
		Integer total=this.baseMapper.selectCount(QueryHelpPlus.getPredicate(UNotice.class, criteria));
		map.put("totalElements", total);
		map.put("totalPage", page.getPages());
        return map;
    }

    @Override
    @Cacheable
    public List<UNotice> queryAll(UNoticeQueryCriteria criteria) {
        return fillDeptId(baseMapper.selectList(QueryHelpPlus.getPredicate(UNotice.class, criteria)));
    }

    @Override
    @CacheEvict(allEntries = true)
    public void saveOrUpdateUnotice(UNoticeVo resources) {
        UNotice uNotice = BeanUtil.copyProperties(resources, UNotice.class);
        uNotice.setTime(new DateTime().toTimestamp());
        uNotice.setUsers(SecurityUtils.getNickName());
        if (this.saveOrUpdate(uNotice) && resources.getDeptId() != null) {
            uNoticeDeptService.deleteByUnoticeId(uNotice.getId());
            List<UNoticeDept> depts = resources.getDeptId().stream().map(s -> {
                UNoticeDept uNoticeDept = new UNoticeDept();
                uNoticeDept.setNoticeId(uNotice.getId());
                uNoticeDept.setDeptId(s);
                return uNoticeDept;
            }).collect(Collectors.toList());

            uNoticeDeptService.saveBatch(depts);
        }
    }

    @Override
    @CacheEvict(allEntries = true)
    public List<UNotice> myNotice() {
        return baseMapper.myNotice(SecurityUtils.getUserId(), SecurityUtils.getTenantId());
    }

    private List<UNotice> fillDeptId(List<UNotice> list) {
        List<String> ids = list.stream().map(UNotice::getId).collect(Collectors.toList());
        if (CollUtil.isNotEmpty(ids)) {
            List<UNoticeDept> noticeDeptList = uNoticeDeptService.lambdaQuery().in(UNoticeDept::getNoticeId, ids).list();
            if (CollUtil.isNotEmpty(noticeDeptList)) {
                return list.stream().peek(k -> k.setDeptId(noticeDeptList.stream().filter(p -> p.getNoticeId().equals(k.getId())).map(UNoticeDept::getDeptId).collect(Collectors.toList()))).collect(Collectors.toList());
            }
        }
        return list;
    }


}
