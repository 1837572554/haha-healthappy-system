package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.UNotice;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 公告表（阿斯利康）
 * @author fang
 * @date 2021-06-17
 */
@Mapper
@Repository
public interface UNoticeMapper extends CoreMapper<UNotice> {
    /**
     * 我的公告，有效范围内的公告
     * @author YJ
     * @date 2022/3/14 11:17
     * @param userId 用户Id
     * @param tenantId 租户id
     * @return java.util.List〈com.healthappy.modules.system.domain.UNotice〉
     */
    @SqlParser(filter = true)
    List<UNotice> myNotice(@Param("userId") Long userId,@Param("tenantId") String tenantId);
}
