package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @description 企业经济类型
 * @author sjc
 * @date 2021-08-30
 */
@Data
public class BTypeCompanyVo  {

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private Long id;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    @NotBlank(message = "名称不能为空")
    private String name;
    /**
     * 标准编码
     */
    @ApiModelProperty("标准编码")
    private String code;

    /**
     * 排序
     */
    @ApiModelProperty("排序")
    private Integer dispOrder;
}
