package com.healthappy.modules.system.rest.manage;

import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.service.AloneReportPrintService;
import com.healthappy.modules.system.service.dto.AloneReportPrintCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Api(tags = "体检管理：单项报告")
@RestController
@AllArgsConstructor
@RequestMapping("/AloneReportPrint")
public class AloneReportPrintController {
    private final AloneReportPrintService tAloneReportPrint;

    @Log("单项报告-人员列表查询")
    @ApiOperation("单项报告-人员列表查询，权限码：AloneReportPrint:list")
    @PostMapping
    @PreAuthorize("@el.check('AloneReportPrint:list')")
    public R list(@RequestBody AloneReportPrintCriteria criteria) {
//        if (CollUtil.isEmpty(criteria.getTimeList()) || criteria.getTimeList().size() != 2) {
//            throw new BadRequestException("时间段不能为空");
//            return R.error(ResultCode.VALIDATE_FAILED,"时间段不能为空");
//        }
//        if (ObjectUtil.isEmpty(criteria.getPageNum()) || criteria.getPageNum() <= 0) {
//            criteria.setPageNum(1);
//        }
//        if (ObjectUtil.isEmpty(criteria.getPageSize()) || criteria.getPageSize() <= 0) {
//            criteria.setPageSize(10);
//        }
        return R.ok(tAloneReportPrint.getList(criteria));
    }

    @Log("单项报告-组合项目查询")
    @ApiOperation("单项报告-组合项目查询，权限码：AloneReportPrintGroup:list")
    @GetMapping("/groupList")
    @PreAuthorize("@el.check('AloneReportPrint:list')")
    public R grouplist(@RequestParam(required = true) String paId, @RequestParam(required = false) String deptId, @RequestParam(required = false)  String groupId) {
        return R.ok(tAloneReportPrint.getGroupList(paId, deptId, groupId));
    }

    @Log("单项报告-体检项目查询")
    @ApiOperation("单项报告-体检项目查询，权限码：AloneReportPrintItem:list")
    @GetMapping("itemList")
    @PreAuthorize("@el.check('AloneReportPrint:list')")
    public R itemlist(@RequestParam(required = true) String paId, @RequestParam(required = true) String groupId) {
        return R.ok(tAloneReportPrint.getItemList(paId, groupId));
    }


}
