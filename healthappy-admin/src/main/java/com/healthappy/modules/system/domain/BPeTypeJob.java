package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.healthappy.config.databind.FieldBind;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 工种表
 * @author fang
 * @date 2021-06-21
 */
@Data
@TableName("B_Petype_Job")
public class BPeTypeJob  implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
     * id
     */
    private Long id;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    private String name;

    /**
     * 排序
     */
    @ApiModelProperty("排序")
    private Integer dispOrder;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    private String code;

    /**
     * 类型(1代表职业；2代表从业)
     */
    @ApiModelProperty("类型(1代表职业；2代表从业)")
    @FieldBind(type = "petypeJobType" , target = "typeText")
    private String type;

    @TableField(exist = false)
    private String typeText;

    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;
}
