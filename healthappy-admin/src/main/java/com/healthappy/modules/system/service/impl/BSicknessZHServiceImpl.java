package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BSicknessZH;
import com.healthappy.modules.system.service.BSicknessZHService;
import com.healthappy.modules.system.service.dto.*;
import com.healthappy.modules.system.service.mapper.BSicknessZHMapper;
import com.healthappy.utils.SecurityUtils;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author SJC
 * @date 2021-08-3
 */
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BSicknessZHServiceImpl extends BaseServiceImpl<BSicknessZHMapper, BSicknessZH> implements BSicknessZHService {


    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BSicknessZHQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BSicknessZH> page = new PageInfo<BSicknessZH>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", generator.convert(page.getList(), BSicknessZHDto.class));
        map.put("totalElements", page.getTotal());
        return map;
    }

    @Override
    public List<BSicknessZH> queryAll(BSicknessZHQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BSicknessZHDto.class, criteria));
    }


    /**
     * 根据病种组合id删除子项
     *
     * @param SicknessZhId
     * @return
     */
    @Override
    public boolean deleteBySicknessZhId(String SicknessZhId) {
        return this.lambdaUpdate().eq(BSicknessZH::getSicknessZhId, SicknessZhId).remove();
    }

    /**
     * 根据病种组合id集合批量删除子项
     *
     * @param SicknessZhIds
     * @return
     */
    @Override
    public boolean deleteBySicknessZhIds(Set<String> SicknessZhIds) {
        return this.lambdaUpdate().in(BSicknessZH::getSicknessZhId, SicknessZhIds).remove();
    }

    /**
     * 根据病种di删除子项
     *
     * @param SicknessId
     * @return
     */
    @Override
    public boolean deleteBySicknessId(String SicknessId) {
        return this.lambdaUpdate().eq(BSicknessZH::getSicknessId, SicknessId).remove();
    }

    @Override
    public List<DeptItemSicknessTreeDTO> deptItemSicknessTree(String simpleSpelling) {
        //获取当前租户下的科室、明细项、病种数据
        List<DeptItemSicknessDTO> deptItemSicknessDTOS = baseMapper.listDeptItemSickness(simpleSpelling, SecurityUtils.getTenantId());
        List<DeptItemSicknessTreeDTO> deptItemSicknessTreeList = new ArrayList<>();
        //过滤科室编号列表
//        List<String> deptIdList = deptItemSicknessDTOS.stream().map(DeptItemSicknessDTO::getDeptId).distinct().collect(Collectors.toList());


//        Map<DeptItemSicknessTreeDTO, Map<ItemSicknessDTO, Map<SicknessBaseDTO, List<DeptItemSicknessDTO>>>> collect1 = deptItemSicknessDTOS.stream()
//                .collect(Collectors.groupingBy(d -> new DeptItemSicknessTreeDTO(d.getDeptId(), d.getDeptName(), new ArrayList<>())
//                        , Collectors.groupingBy(i -> new ItemSicknessDTO(i.getItemId(), i.getItemName(), new ArrayList<>())
//                                , Collectors.groupingBy(s -> new SicknessBaseDTO(s.getSicknessId(), s.getSicknessName()))
//                        )));

        LinkedHashMap<DeptItemSicknessTreeDTO, LinkedHashMap<ItemSicknessDTO, LinkedHashMap<SicknessBaseDTO, List<DeptItemSicknessDTO>>>> collect = deptItemSicknessDTOS.stream()
                .collect(Collectors.groupingBy(d -> new DeptItemSicknessTreeDTO(d.getDeptId(), d.getDeptName(), new ArrayList<>()), LinkedHashMap::new
                        , Collectors.groupingBy(i -> new ItemSicknessDTO(i.getItemId(), i.getItemName(), new ArrayList<>()), LinkedHashMap::new
                                , Collectors.groupingBy(s -> new SicknessBaseDTO("",s.getSicknessName(),s.getSicknessId(),s.getSicknessRuleId()), LinkedHashMap::new, Collectors.toList())

                        )));


        for (Map.Entry dept : collect.entrySet()) {
            DeptItemSicknessTreeDTO deptDTO = (DeptItemSicknessTreeDTO) dept.getKey();
            Map<ItemSicknessDTO, Map<ItemSicknessDTO, List<DeptItemSicknessDTO>>> deptChilderList = (Map<ItemSicknessDTO, Map<ItemSicknessDTO, List<DeptItemSicknessDTO>>>) dept.getValue();
            for (Map.Entry deptChildren : deptChilderList.entrySet()) {
                ItemSicknessDTO itemDTO = (ItemSicknessDTO) deptChildren.getKey();
                Map<ItemSicknessDTO, List<DeptItemSicknessDTO>> itemChildrenList = (Map<ItemSicknessDTO, List<DeptItemSicknessDTO>>) deptChildren.getValue();
                for (Map.Entry sicknessChildren : itemChildrenList.entrySet()) {
                    SicknessBaseDTO sicknessDTO = (SicknessBaseDTO) sicknessChildren.getKey();
                    sicknessDTO.setKey(itemDTO.getKey()+sicknessDTO.getSicknessId()+sicknessDTO.getSicknessRuleId());
                    itemDTO.getChildren().add(sicknessDTO);
                }
                deptDTO.getChildren().add(itemDTO);
            }
            deptItemSicknessTreeList.add(deptDTO);
        }


        return deptItemSicknessTreeList;
    }

    private Map<String, String> getKeyTitle(List<DeptItemSicknessDTO> deptItemSicknessDTOS, String deptId) {
        if (CollUtil.isEmpty(deptItemSicknessDTOS) || deptItemSicknessDTOS.size() == 0 || StrUtil.isBlank(deptId)) {
            return null;
        }
        Map<String, String> deptMap = new HashMap<>();
        //科室
        List<DeptItemSicknessDTO> deptList = deptItemSicknessDTOS.stream().filter(d -> deptId.equals(d.getDeptId()))
                .collect(Collectors.toList());
//        for (DeptItemSicknessDTO dept : deptList) {
//
//        }
        return null;
    }
}
