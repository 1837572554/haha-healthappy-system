package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.healthappy.config.SeedIdGenerator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 病种建议表
 * @author fang
 * @date 2021-06-21
 */
@Data
@TableName("B_Sickness_Advice")
@ApiModel("病种建议表")
@SeedIdGenerator
public class BSicknessAdvice implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * id
     */
    @TableId(type = IdType.INPUT)
    @ApiModelProperty("id")
    private String id;

    /**
     * 病种名称id
     */
    @ApiModelProperty("病种名称id")
    private String sicknessId;

    /**
     * 建议
     */
    @ApiModelProperty("建议")
    private String jy;

    /**
     * 显示序号
     */
    @ApiModelProperty("显示序号")
    private Integer dispOrder;

    /**
     * 是否删除
     */
    @ApiModelProperty("是否删除")
    @TableField(value = "del_flag",fill = FieldFill.INSERT)
    @TableLogic
    private String delFlag;

    /**
     * 单位建议
     */
    @ApiModelProperty("单位建议")
    private String jyCompany;

    /**
     * 健康分析建议维护
     */
    @ApiModelProperty("健康分析建议维护")
    private String healthAdvice;

    /**
     * 医疗机构id(u_hospital_info的id)
     */
    @ApiModelProperty("医疗机构id(u_hospital_info的id)")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;
}
