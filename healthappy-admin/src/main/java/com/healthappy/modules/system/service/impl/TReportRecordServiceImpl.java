package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.config.databind.DataBind;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.TPatient;
import com.healthappy.modules.system.domain.TReportRecord;
import com.healthappy.modules.system.service.TPatientService;
import com.healthappy.modules.system.service.TReportRecordService;
import com.healthappy.modules.system.service.dto.ReportReleaseSubmitDTO;
import com.healthappy.modules.system.service.dto.ReportReleaseUnSubmitDTO;
import com.healthappy.modules.system.service.dto.reportReleaseReceiveDTO;
import com.healthappy.modules.system.service.mapper.TReportRecordMapper;
import com.healthappy.utils.SecurityUtils;
import com.healthappy.utils.SpringUtil;
import lombok.AllArgsConstructor;
import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

/**
 * 体检报告记录 实现类
 *
 * @author Jevany
 * @date 2022/3/7 9:49
 */
@AllArgsConstructor
@Service
public class TReportRecordServiceImpl extends BaseServiceImpl<TReportRecordMapper, TReportRecord>
		implements TReportRecordService {

	private final TPatientService tPatientService;

	@Override
	public void reportRecordAdd(ReportReleaseSubmitDTO submitDTO) {
		if (StrUtil.isBlank(submitDTO.getReleaseType())) {
			throw new BadRequestException("发布（操作）类型不能为空");
		}
		if (CollUtil.isEmpty(submitDTO.getPaIdList())) {
			throw new BadRequestException("流水号不能为空");
		}
		DataBind dataBind = SpringUtil.getBean(DataBind.class);
		BidiMap reportReleaseTypeBidiMap = new DualHashBidiMap(
				dataBind.ENUM_MAP.get("reportReleaseType")).inverseBidiMap();
		if (!reportReleaseTypeBidiMap.containsValue(submitDTO.getReleaseType())) {
			throw new BadRequestException("体检报告发布类型不存在");
		}

		/** 是否打印报告 */
		Boolean printReport = false;
		/** 是否打印报告 */
		Boolean uploadReport = false;
		if (submitDTO.getReleaseType().equals(reportReleaseTypeBidiMap.get("打印报告"))) {
			printReport = true;
		} else if (submitDTO.getReleaseType().equals(reportReleaseTypeBidiMap.get("上传报告"))) {
			uploadReport = true;
		}

		//除了上传报告，其他操作类型的报告都必须填写“报告类型”
		if (!uploadReport && StrUtil.isBlank(submitDTO.getReportType())) {
			throw new BadRequestException(
					String.valueOf(reportReleaseTypeBidiMap.getKey(submitDTO.getReleaseType())) + "，报告类型不能为空");
		}

		String tenantId = SecurityUtils.getTenantId();
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String nickName = SecurityUtils.getNickName();
		for (String paId : submitDTO.getPaIdList()) {
			TReportRecord tReportRecord = new TReportRecord();
			tReportRecord.setPaId(paId);
			tReportRecord.setReleaseType(submitDTO.getReleaseType());
			tReportRecord.setReportType(submitDTO.getReportType());
			tReportRecord.setDoctor(nickName);
			tReportRecord.setTenantId(tenantId);

			//            if (printReport) {
			//                HttpServer.PersonReport personReport=new HttpServer.PersonReport();
			//                personReport.init();
			//                personReport.setPatientId(paId);
			//                String personReport1 = HttpServer.getPersonReport(personReport);
			//            }
			//region 报告打印操作
			//修改体检者的upload_time,upload_doctor更新掉
			if (printReport) {
				tPatientService.lambdaUpdate().set(TPatient::getPrintDate, timestamp)
						.set(TPatient::getPrintDoctor, nickName).eq(TPatient::getId, paId).update();
			}
			//endregion 报告打印操作

			//region 报告上传操作
			//修改体检者的upload_time,upload_doctor更新掉
			if (uploadReport) {
				tPatientService.lambdaUpdate().set(TPatient::getUploadTime, timestamp)
						.set(TPatient::getUploadDoctor, nickName).eq(TPatient::getId, paId).update();
			}
			//endregion 报告上传操作

			baseMapper.insert(tReportRecord);
		}
	}

	@Override
	public void reportReleaseUndoSubmit(ReportReleaseUnSubmitDTO unSubmitDTO) {
		if (StrUtil.isBlank(unSubmitDTO.getReleaseType())) {
			throw new BadRequestException("发布（操作）类型不能为空");
		}
		DataBind dataBind = SpringUtil.getBean(DataBind.class);
		BidiMap reportReleaseTypeBidiMap = new DualHashBidiMap(
				dataBind.ENUM_MAP.get("reportReleaseType")).inverseBidiMap();
		String dictName = Convert.toStr(reportReleaseTypeBidiMap.getKey(unSubmitDTO.getReleaseType()));

		if (!StrUtil.containsAny(dictName, "打印报告", "上传报告")) {
			throw new BadRequestException("不支持此报告发布类型的撤销");
		}

		if (CollUtil.isNotEmpty(unSubmitDTO.getReleaseIdList())) {
			for (String releaseId : unSubmitDTO.getReleaseIdList()) {
				if (dictName.equals("打印报告")) {
					if (this.lambdaUpdate().eq(TReportRecord::getReleaseType, unSubmitDTO.getReleaseType())
							.eq(TReportRecord::getId, releaseId).isNull(TReportRecord::getUndoTime)
							.and(wp -> wp.isNull(TReportRecord::getReceiveTime)
									.isNull(TReportRecord::getUndoReceiveTime).or()
									.isNotNull(TReportRecord::getReceiveTime)
									.isNotNull(TReportRecord::getUndoReceiveTime))
							.set(TReportRecord::getUndoDoctor, SecurityUtils.getNickName())
							.set(TReportRecord::getUndoTime, new Timestamp(System.currentTimeMillis())).update()) {
						//撤销成功，修改体检表
						TReportRecord one = this.lambdaQuery().eq(TReportRecord::getId, releaseId).one();
						if (Optional.ofNullable(one).isPresent()) {
							String paId = one.getPaId();
							one = this.lambdaQuery().eq(TReportRecord::getReleaseType, unSubmitDTO.getReleaseType())
									.eq(TReportRecord::getPaId, paId).isNull(TReportRecord::getUndoTime)
									.orderByDesc(TReportRecord::getCreateTime).last("limit 1").one();
							if (Optional.ofNullable(one).isPresent()) {
								tPatientService.lambdaUpdate().set(TPatient::getPrintDoctor, one.getDoctor())
										.set(TPatient::getPrintDate, one.getCreateTime())
										.eq(TPatient::getId, one.getPaId()).update();
							} else {
								tPatientService.lambdaUpdate().set(TPatient::getPrintDoctor, null)
										.set(TPatient::getPrintDate, null).eq(TPatient::getId, paId).update();
							}
						}
					}
				} else if (dictName.equals("上传报告")) {
					if (this.lambdaUpdate().eq(TReportRecord::getReleaseType, unSubmitDTO.getReleaseType())
							.eq(TReportRecord::getId, releaseId).isNull(TReportRecord::getUndoTime)
							.set(TReportRecord::getUndoDoctor, SecurityUtils.getNickName())
							.set(TReportRecord::getUndoTime, new Timestamp(System.currentTimeMillis())).update()) {
						//撤销成功，修改体检表
						TReportRecord one = this.lambdaQuery().eq(TReportRecord::getId, releaseId).one();
						if (Optional.ofNullable(one).isPresent()) {
							String paId = one.getPaId();
							one = this.lambdaQuery().eq(TReportRecord::getReleaseType, unSubmitDTO.getReleaseType())
									.eq(TReportRecord::getPaId, one.getPaId()).isNull(TReportRecord::getUndoTime)
									.orderByDesc(TReportRecord::getCreateTime).last("limit 1").one();
							if (Optional.ofNullable(one).isPresent()) {
								tPatientService.lambdaUpdate().set(TPatient::getUploadDoctor, one.getDoctor())
										.set(TPatient::getUploadTime, one.getCreateTime())
										.eq(TPatient::getId, one.getPaId()).update();
							} else {
								tPatientService.lambdaUpdate().set(TPatient::getUploadDoctor, null)
										.set(TPatient::getUploadTime, null).eq(TPatient::getId, paId).update();
							}
						}
					}
				}
			}
		} else if (CollUtil.isNotEmpty(unSubmitDTO.getPaIdList())) {
			for (String paId : unSubmitDTO.getPaIdList()) {
				if (dictName.equals("打印报告")) {
					if (this.lambdaUpdate().eq(TReportRecord::getReleaseType, unSubmitDTO.getReleaseType())
							.eq(TReportRecord::getPaId, paId).isNull(TReportRecord::getUndoTime)
							.and(wp -> wp.isNull(TReportRecord::getReceiveTime)
									.isNull(TReportRecord::getUndoReceiveTime).or()
									.isNotNull(TReportRecord::getReceiveTime)
									.isNotNull(TReportRecord::getUndoReceiveTime))
							.set(TReportRecord::getUndoDoctor, SecurityUtils.getNickName())
							.set(TReportRecord::getUndoTime, new Timestamp(System.currentTimeMillis())).update()) {
						TReportRecord one = this.lambdaQuery()
								.eq(TReportRecord::getReleaseType, unSubmitDTO.getReleaseType())
								.eq(TReportRecord::getPaId, paId).isNull(TReportRecord::getUndoTime)
								.orderByDesc(TReportRecord::getCreateTime).last("limit 1").one();
						if (Optional.ofNullable(one).isPresent()) {
							tPatientService.lambdaUpdate().set(TPatient::getPrintDoctor, one.getDoctor())
									.set(TPatient::getPrintDate, one.getCreateTime()).eq(TPatient::getId, one.getPaId())
									.update();
						} else {
							tPatientService.lambdaUpdate().set(TPatient::getPrintDoctor, null)
									.set(TPatient::getPrintDate, null).eq(TPatient::getId, paId).update();
						}
					}
				} else if (dictName.equals("上传报告")) {
					if (this.lambdaUpdate().eq(TReportRecord::getReleaseType, unSubmitDTO.getReleaseType())
							.eq(TReportRecord::getPaId, paId).isNull(TReportRecord::getUndoTime)
							.set(TReportRecord::getUndoDoctor, SecurityUtils.getNickName())
							.set(TReportRecord::getUndoTime, new Timestamp(System.currentTimeMillis())).update()) {
						//撤销成功，修改体检表
						TReportRecord one = this.lambdaQuery()
								.eq(TReportRecord::getReleaseType, unSubmitDTO.getReleaseType())
								.eq(TReportRecord::getPaId, paId).isNull(TReportRecord::getUndoTime)
								.orderByDesc(TReportRecord::getCreateTime).last("limit 1").one();
						if (Optional.ofNullable(one).isPresent()) {
							tPatientService.lambdaUpdate().set(TPatient::getUploadDoctor, one.getDoctor())
									.set(TPatient::getUploadTime, one.getCreateTime())
									.eq(TPatient::getId, one.getPaId()).update();
						} else {
							tPatientService.lambdaUpdate().set(TPatient::getUploadDoctor, null)
									.set(TPatient::getUploadTime, null).eq(TPatient::getId, paId).update();
						}
					}
				}
			}
		}
	}


	@Override
	public void reportRecordReceive(reportReleaseReceiveDTO receiveDTO) {
		if (CollUtil.isEmpty(receiveDTO.getReleaseIdList()) && CollUtil.isEmpty(receiveDTO.getPaIdList())) {
			throw new BadRequestException("数据不能为空");
		}
		DataBind dataBind = SpringUtil.getBean(DataBind.class);
		BidiMap reportReleaseTypeBidiMap = new DualHashBidiMap(
				dataBind.ENUM_MAP.get("reportReleaseType")).inverseBidiMap();
		if (CollUtil.isNotEmpty(receiveDTO.getReleaseIdList())) {
			//走指定的领取
			for (String releaseId : receiveDTO.getReleaseIdList()) {
				this.lambdaUpdate().eq(TReportRecord::getId, releaseId)
						.eq(TReportRecord::getReleaseType, Convert.toStr(reportReleaseTypeBidiMap.get("打印报告")))
						.isNull(TReportRecord::getUndoTime).and(wp -> wp.isNull(TReportRecord::getReceiveTime).or()
								.isNotNull(TReportRecord::getReceiveTime).isNotNull(TReportRecord::getUndoReceiveTime))
						.set(TReportRecord::getReceiveDoctor, SecurityUtils.getNickName())
						.set(TReportRecord::getReceiveTime, new Timestamp(System.currentTimeMillis()))
						.set(TReportRecord::getUndoReceiveDoctor, null).set(TReportRecord::getUndoReceiveTime, null)
						.update();
			}
		} else if (CollUtil.isNotEmpty(receiveDTO.getPaIdList())) {
			for (String paId : receiveDTO.getPaIdList()) {
				this.lambdaUpdate().eq(TReportRecord::getPaId, paId)
						.eq(TReportRecord::getReleaseType, Convert.toStr(reportReleaseTypeBidiMap.get("打印报告")))
						.isNull(TReportRecord::getUndoTime).and(wp -> wp.isNull(TReportRecord::getReceiveTime).or()
								.isNotNull(TReportRecord::getReceiveTime).isNotNull(TReportRecord::getUndoReceiveTime))
						.set(TReportRecord::getReceiveDoctor, SecurityUtils.getNickName())
						.set(TReportRecord::getReceiveTime, new Timestamp(System.currentTimeMillis()))
						.set(TReportRecord::getUndoReceiveDoctor, null).set(TReportRecord::getUndoReceiveTime, null)
						.update();
			}
		}
	}


	@Override
	public void reportRecordUndoReceive(reportReleaseReceiveDTO receiveDTO) {
		if (CollUtil.isEmpty(receiveDTO.getReleaseIdList()) && CollUtil.isEmpty(receiveDTO.getPaIdList())) {
			throw new BadRequestException("数据不能为空");
		}
		DataBind dataBind = SpringUtil.getBean(DataBind.class);
		BidiMap reportReleaseTypeBidiMap = new DualHashBidiMap(
				dataBind.ENUM_MAP.get("reportReleaseType")).inverseBidiMap();
		if (CollUtil.isNotEmpty(receiveDTO.getReleaseIdList())) {
			//走指定的领取
			for (String releaseId : receiveDTO.getReleaseIdList()) {
				this.lambdaUpdate().eq(TReportRecord::getId, releaseId)
						.eq(TReportRecord::getReleaseType, Convert.toStr(reportReleaseTypeBidiMap.get("打印报告")))
						.isNull(TReportRecord::getUndoTime).isNotNull(TReportRecord::getReceiveTime)
						.isNull(TReportRecord::getUndoReceiveTime)
						.set(TReportRecord::getUndoReceiveDoctor, SecurityUtils.getNickName())
						.set(TReportRecord::getUndoReceiveTime, new Timestamp(System.currentTimeMillis())).update();
			}
		} else if (CollUtil.isNotEmpty(receiveDTO.getPaIdList())) {
			for (String paId : receiveDTO.getPaIdList()) {
				this.lambdaUpdate().eq(TReportRecord::getPaId, paId)
						.eq(TReportRecord::getReleaseType, Convert.toStr(reportReleaseTypeBidiMap.get("打印报告")))
						.isNull(TReportRecord::getUndoTime).isNotNull(TReportRecord::getReceiveTime)
						.isNull(TReportRecord::getUndoReceiveTime)
						.set(TReportRecord::getUndoReceiveDoctor, SecurityUtils.getNickName())
						.set(TReportRecord::getUndoReceiveTime, new Timestamp(System.currentTimeMillis())).update();
			}
		}
	}

	@Override
	public Map<String, Object> listReportRecord(String paId, Pageable pageable) {
		getPage(pageable);
		PageInfo<TReportRecord> page = new PageInfo<TReportRecord>(baseMapper.listReportRecord(paId));
		Map<String, Object> map = new LinkedHashMap<>(2);
		map.put("content", page.getList());
		map.put("totalElements", page.getTotal());
		return map;
	}
}