package com.healthappy.modules.system.service.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.config.databind.FieldBind;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Data
public class MedicalRegistrationDto implements Serializable {


    @ApiModelProperty("流水号")
    private String id;

    @ApiModelProperty("姓名")
    private String name;

    /**
     * 单位编号
     */
    @ApiModelProperty("单位编号")
    private String companyId;
    /**
     * 单位编号
     */
    @ApiModelProperty("单位名称")
    private String companyName;

    @ApiModelProperty("性别ID")
    @FieldBind(target = "sexName")
    private String sex;

    @ApiModelProperty("性别名称")
    @TableField(exist = false)
    private String sexName;

    @ApiModelProperty("年龄")
    private Integer age;

    @ApiModelProperty("儿童年龄")
    private String ageDate;

    @ApiModelProperty("婚姻状况主键")
    @FieldBind(target = "maritalText")
    private String marital;

    @ApiModelProperty("婚姻状况")
    @TableField(exist = false)
    private String maritalText;

    @ApiModelProperty("出生日期")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Timestamp birthday;

    @ApiModelProperty("联系电话")
    private String phone;

    @ApiModelProperty("证件号")
    private String idNo;

    @ApiModelProperty("证件类型")
    private String idType;

    @ApiModelProperty("民族代码")
    private String nation;

    @ApiModelProperty("民族")
    private String nationName;

    @ApiModelProperty("地址")
    private String address;

    @ApiModelProperty("户籍地址")
    private String birthplace;

    @ApiModelProperty("体检分类表id")
    private Long peTypeHdId;

    @ApiModelProperty("体检分类名称")
    private String peTypeHdName;

    @ApiModelProperty("体检类别表id")
    private Long peTypeDtId;

    @ApiModelProperty("体检类别名称")
    private String peTypeDtName;

    @ApiModelProperty("体检日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp peDate;

    @ApiModelProperty("工种ID")
    private Long job;

    @ApiModelProperty("工种名称")
    private String jobName;

    @ApiModelProperty("工号")
    private String code;

    @ApiModelProperty("总工龄")
    private String totalYears;

    @ApiModelProperty("缴费方式")
    private String chargeType;

    @ApiModelProperty("统收限额")
    private BigDecimal tsxe;

    @ApiModelProperty("部门id")
    private String departmentId;

    @ApiModelProperty("部门名称")
    private String departmentName;

    @ApiModelProperty("分组id")
    private String departmentGroupId;

    @ApiModelProperty("分组名称")
    private String departmentGroupName;

    @ApiModelProperty("部门/车间")
    private String workshop;

    @ApiModelProperty("备注")
    private String mark;

    @ApiModelProperty("套餐名称")
    private String packageName;

    @ApiModelProperty("套餐价格")
    private BigDecimal packagePrice;

    @ApiModelProperty("附加套餐价格")
    private BigDecimal packageAdditionPrice;

    @ApiModelProperty(value = "附加项目总价格")
    private Double additionPrice;

    @ApiModelProperty("总价格")
    private BigDecimal totalPrice;

    @ApiModelProperty("接害工龄")
    private String workYears;

    @ApiModelProperty("初检号")
    private Long historyNo;

    @ApiModelProperty("接害种类")
    private String poisonType;

    @ApiModelProperty("身份证图片")
    private String idImage;

    @ApiModelProperty("人像")
    private String portrait;

    @ApiModelProperty("毒害")
    private List<PoisonTypeDto> poisonTypeList;

    /** 毒害编号列表 */
    @ApiModelProperty("毒害编号列表")
    private List<String> poisonIdList;

    @ApiModelProperty("体检项目")
    private List<RegisterProDto> projectList;

    /**
     * 档案号，一个身份证对应一个
     */
    @ApiModelProperty("档案号，一个身份证对应一个")
    private Long recordId;

    /** 总检时间 */
    @ApiModelProperty("总检时间")
    private String conclusionDate;

    /** 审核时间 */
    @ApiModelProperty("审核时间")
    private String verifyDate;
}
