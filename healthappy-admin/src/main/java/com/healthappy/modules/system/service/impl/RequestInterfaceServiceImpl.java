package com.healthappy.modules.system.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.healthappy.modules.http.report.RequestConstant;
import com.healthappy.modules.system.service.RequestInterfaceService;
import com.healthappy.utils.R;
import com.healthappy.utils.SpringBeanFactoryUtil;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author: wukefei
 * @Date: Created in 2022/3/30 9:45
 * @Description:
 * @Version: 1.0
 *
 */
@Service
@AllArgsConstructor//有自动注入的功能
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class RequestInterfaceServiceImpl implements RequestInterfaceService {

	@ApiModelProperty("timeout   超时时长，-1表示默认超时，单位毫秒")
	private final static int TIMEOUT = -1;
	Map<String, String> map = new HashedMap();

	public  RequestInterfaceServiceImpl(){
			try {
				Class cls = RequestConstant.class;
				Field[] fields = cls.getDeclaredFields();
				for (int i = 0; i < fields.length; i++) {
					Field f = fields[i];
					map.put(f.getName(), String.valueOf(f.get(cls)));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

	@Override
	public Map<String, String> getList() {
		return map;
	}

	@Override
	public Boolean check(String key) {
		String requestMapping = map.get(key);
		String requestUrl = SpringBeanFactoryUtil.resolve("${httpServer.reportServer}") + requestMapping;
		Boolean isLive = true;
		try {
			String result = HttpUtil.post(requestUrl, "", TIMEOUT);
		} catch (Exception e) {
			isLive = false;
		}
		return isLive;
	}

	@Override
	public Boolean checkHostandPort(String host, int port) {
		if (StrUtil.isBlank(host) || port == 0) {
			String requestUrl = SpringBeanFactoryUtil.resolve("${httpServer.reportServer}");
			try {
				URL url = new URL(requestUrl);
				host = url.getHost();
				port = url.getPort();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}
		return this.isHostConnectable(host, port);
	}



	/**
	 * 使用Socket 链接判断 host地址下的port 端口是否能访问
	 * @param host
	 * @param port
	 * @return
	 */
	public boolean isHostConnectable(String host, int port) {
		Boolean isLive = true;
		Socket socket = new Socket();
		try {
			socket.connect(new InetSocketAddress(host, port), 1000);
		} catch (Exception e) {
			isLive = false;
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return isLive;
	}

	@Override
	public Boolean run() {
		String path = SpringBeanFactoryUtil.resolve("${httpServer.reportPath}");
		try {
			String run = "cmd /c "+path +"run.bat";
			Process pro = Runtime.getRuntime().exec(run);
			pro.waitFor();
		}catch (Exception e){
			e.printStackTrace();
		}
		return this.checkHostandPort(null, 0);
	}

	@Override
	public Boolean shutdwon() {
		String path = SpringBeanFactoryUtil.resolve("${httpServer.reportPath}");
		try {
			String shutdwon = "cmd /c  "+path +"shutdwon.bat";
			Process pro = Runtime.getRuntime().exec(shutdwon);
			pro.waitFor();
		}catch (Exception e){
			e.printStackTrace();
		}
		return this.checkHostandPort(null, 0)==false?true:false;
	}




	public static void main(String[] args){
		try {
			String shutdwon = "cmd /c  E:/ReportWebapi/shutdwon.bat";
			String toPath="cmd /c start cd /d E:/ReportWebapi/ ";
			String run=" ipconfig";

			List<String> list=new ArrayList<>();
//			list.add(toPath);
			list.add(run);
			ProcessBuilder builder = new ProcessBuilder(); //创建系统进程
			builder.command(list);
			Process process = builder.start();//启动进程会返回dos页面的对象
			process.destroy();
//			Process pro = Runtime.getRuntime().exec(toPath);
//			pro.waitFor();
//			String run="cmd /c dotnet HealthHappyReportGenerate.dll --urls=\"http://*:5555\" --ip=\"127.0.0.1\" --port=5555";

//			String shutdwon = "for /f \"tokens=1-5\" %a in ('netstat -ao^|findstr " + 5555 + "') do （@taskkill /F /PID %a)";
////			String run = "start \"HealthHappyReportGenerate.exe\" dotnet HealthHappyReportGenerate.dll --urls=\"http://*:5555\" --ip=\"127.0.0.1\" --port=5555 ";

		}catch (Exception e){
			e.printStackTrace();
		}

	}

}
