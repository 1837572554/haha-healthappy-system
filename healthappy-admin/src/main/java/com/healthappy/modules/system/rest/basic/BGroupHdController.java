package com.healthappy.modules.system.rest.basic;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BGroupHd;
import com.healthappy.modules.system.domain.BPackageDT;
import com.healthappy.modules.system.domain.vo.BGroupHdVo;
import com.healthappy.modules.system.service.BGroupHdService;
import com.healthappy.modules.system.service.dto.BGroupHdQueryCriteria;
import com.healthappy.utils.R;
import com.healthappy.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author fang
 * @description 组合项目设置
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "基础管理：组合项目设置")
@ApiSort(3)
@RestController
@AllArgsConstructor
@RequestMapping("/BGroupHd")
public class BGroupHdController {

    private final BGroupHdService bGroupHdService;

    @Log("组合项目信息")
    @ApiOperation("组合项目信息，权限码：BGroupHd:list")
    @GetMapping
    public R list(BGroupHdQueryCriteria criteria, Pageable pageable) {
        return R.ok(bGroupHdService.queryAll(criteria, pageable));
    }

    @Log("组合项目信息全部列表")
    @ApiOperation(value = "组合项目信息全部列表，权限码：BGroupHd:list")
    @GetMapping("/allList")
    public R allList(BGroupHdQueryCriteria criteria) {
        return R.ok(bGroupHdService.queryAll(criteria));
    }

    @Log("组合项目信息全部列表-精简")
    @ApiOperation(value = "组合项目信息全部列表-精简，权限码：BGroupHd:list")
    @GetMapping("/allListLite")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "精简组合项结果",response = BPackageDT.class)
    })
    public R allListLite(BGroupHdQueryCriteria criteria) {
        List<BGroupHd> bGroupHds = bGroupHdService.queryAll(criteria);
        List<BPackageDT> packageDTList=new ArrayList<>();
        bGroupHds.forEach( hd ->{
            BPackageDT packageDT=new BPackageDT();
            packageDT.setGroupId(Optional.ofNullable(hd.getId()).orElse(""));
            packageDT.setGroupName(Optional.ofNullable(hd.getName()).orElse(""));
            packageDT.setPrice(Optional.ofNullable(hd.getPrice()).orElse(BigDecimal.valueOf(0)));
            packageDT.setDiscount(BigDecimal.valueOf(1));
            packageDT.setDeptId(Optional.ofNullable(hd.getDeptId()).orElse(""));
            packageDTList.add(packageDT);
        });
        return R.ok(packageDTList);
    }

    @Log("查询条码组合")
    @ApiOperation("查询条码组合，权限码：BGroupHd:list")
    @GetMapping("/barcode")
    public R barcode(@RequestParam(value = "barcode", required = false) String barcode) {
        List<BGroupHd> list = bGroupHdService.lambdaQuery().select(BGroupHd::getBarcode)
                .groupBy(BGroupHd::getBarcode).isNotNull(BGroupHd::getBarcode)
                .like(StrUtil.isNotBlank(barcode), BGroupHd::getBarcode, barcode).list();

        return R.ok(CollUtil.isNotEmpty(list) ? list.stream().map(BGroupHd::getBarcode).collect(Collectors.toList()) : Collections.emptyList());
    }

    @Log("新增|修改组合项目信息")
    @ApiOperation("新增|修改组合项目信息，权限码：BGroupHd:add")
    @PostMapping
    @PreAuthorize("@el.check('BGroupHd:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BGroupHdVo resources) {
        if (checkNameRepeat(resources)) {
            return R.error("组合名称不能重复");
        }
        if (StringUtils.isBlank(resources.getIsEnable())) {
            resources.setIsEnable("1");
        }
        return R.ok(bGroupHdService.saveOrUpdate(BeanUtil.copyProperties(resources, BGroupHd.class)));
    }

    @Log("删除组合项目信息")
    @ApiOperation("删除组合项目信息，权限码：BGroupHd:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('BGroupHd:delete')")
    @Klock
    public R remove(@RequestBody Set<String> ids) {
        return R.ok(bGroupHdService.removeByIds(ids));
    }

    private boolean checkNameRepeat(BGroupHdVo resources) {
        return bGroupHdService.lambdaQuery().eq(BGroupHd::getName, resources.getName()).eq(BGroupHd::getDeptId, resources.getDeptId())
                .ne(ObjectUtil.isNotNull(resources.getId()), BGroupHd::getId, resources.getId()).count() > 0;
    }
}
