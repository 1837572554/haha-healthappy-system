package com.healthappy.modules.system.service.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author FGQ
 * @date 2020-05-14
 */
@Data
public class BCommentZDto implements Serializable {
    /**
     * id
     */
    private String id;

    /**
     * 检查结果
     */
    private String comment;

    /**
     * 建议
     */
    private String suggest;

    /**
     * 排序号
     */
    private Integer dispOrder;

    /**
     * code
     */
    private Long code;
}
