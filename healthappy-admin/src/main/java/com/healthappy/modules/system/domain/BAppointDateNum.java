package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 预约号源数量表 对象
 * @author YJ
 * @date 2022/5/7 0:11
 */
@TableName("B_Appoint_DateNum")
@Data
public class BAppointDateNum implements Serializable {
	/**
	 * 自动编号
	 */
	@ApiModelProperty(value = "自动编号")
	@TableId(type = IdType.AUTO)
	private Long id;

	/**
	 * 预约日期编号
	 */
	@ApiModelProperty(value = "预约日期编号")
	@TableField(value = "appoint_date_id")
	private String appointDateId;

	/**
	 * 单位编号
	 */
	@ApiModelProperty(value = "单位编号")
	private String companyId;

	/**
	 * 人数
	 */
	@ApiModelProperty(value = "人数")
	private Integer num;

	/**
	 * 租户编号
	 */
	@ApiModelProperty(value = "租户编号")
	@TableField(value = "tenant_id", fill = FieldFill.INSERT)
	private String tenantId;

	private static final long serialVersionUID = 1L;
}
