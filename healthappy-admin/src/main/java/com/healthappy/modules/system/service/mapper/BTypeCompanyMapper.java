package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BTypeCompany;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 企业经济类型
 * @author sjc
 * @date 2021-08-30
 */
@Mapper
@Repository
public interface BTypeCompanyMapper extends CoreMapper<BTypeCompany> {

}
