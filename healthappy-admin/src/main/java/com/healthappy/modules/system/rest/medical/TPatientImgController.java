package com.healthappy.modules.system.rest.medical;/**
 * @author Jevany
 * @date 2021/12/1 16:15
 * @desc
 */

import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.TPatientImg;
import com.healthappy.modules.system.service.TPatientImgService;
import com.healthappy.modules.system.service.dto.TPatientImgDto;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @desc: 体检图片绑定控制类
 * @author: YJ
 * @date: 2021-12-01 16:15
 **/
@Slf4j
@Api(tags = "体检管理：图片相关")
@RestController
@AllArgsConstructor
@RequestMapping("/patientImg")
public class TPatientImgController {

    private final TPatientImgService tPatientImgService;

    @Log("绑定项目图片")
    @ApiOperation("绑定项目图片，权限码：ProjectCheck:update")
    @PostMapping("/bindItemImg")
    @PreAuthorize("@el.check('ProjectCheck:update')")
    @Klock
    public R bindItemImg(@Validated @RequestBody List<TPatientImg> tPatientImgs) {
        tPatientImgService.bindItemImg(tPatientImgs);
        return R.ok();
    }


    @Log("获取绑定图片数据")
    @ApiOperation("获取绑定图片数据，权限码：ProjectCheck:list")
    @PostMapping("/getItemImg")
    @PreAuthorize("@el.check('ProjectCheck:list')")
    public R getItemImg(@Validated @RequestBody TPatientImgDto tPatientImgDto) {
        return R.ok(tPatientImgService.getItemImg(tPatientImgDto));
    }

}
