package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * 病种基础信息DTO
 * @author Jevany
 * @date 2022/3/8 14:20
 */
@Data
@AllArgsConstructor
@ApiModel("病种基础信息DTO")
public class SicknessBaseDTO  implements Serializable {
    /** 明细项id+病种id */
    @ApiModelProperty("明细项id+病种id")
    private String key;

    /** 病种名称 */
    @ApiModelProperty("病种名称")
    private String title;
    
    /** 病种id */
    @ApiModelProperty("病种id")
    private String sicknessId;

    /** 病种规则id */
    @ApiModelProperty("病种规则id")
    private String sicknessRuleId;
}