package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.healthappy.modules.system.domain.TCharge;
import com.healthappy.modules.system.domain.vo.AloneChargePrintVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface TChargeMapper extends BaseMapper<TCharge> {

    /**
     * 获取最后一条有效的缴费数据
     * @author YJ
     * @date 2022/4/15 17:32
     * @param paId
     * @param receiptType
     * @param tenantId
     * @return com.healthappy.modules.system.domain.TCharge
     */
    TCharge getLatestValidData(@Param("paId") String paId,@Param("receiptType") String receiptType, @Param("tenantId") String tenantId);


    /**
     * 获取最后一条数据到打印健康证对象
     * @author YJ
     * @date 2022/4/15 19:00
     * @param paId
     * @param receiptType
     * @param tenantId
     * @return com.healthappy.modules.system.domain.vo.AloneChargePrintVo
     */
    AloneChargePrintVo getLatestValidDataPrint(@Param("paId") String paId,@Param("receiptType") String receiptType, @Param("tenantId") String tenantId);
}