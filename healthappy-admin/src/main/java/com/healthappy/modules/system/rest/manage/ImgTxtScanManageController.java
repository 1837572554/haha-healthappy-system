package com.healthappy.modules.system.rest.manage;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.service.ImgTxtScanManageService;
import com.healthappy.modules.system.service.dto.ImgTxtScanDelDTO;
import com.healthappy.modules.system.service.dto.ImgTxtScanManageQueryCriteria;
import com.healthappy.modules.system.service.dto.ImgTxtScanQueryDTO;
import com.healthappy.modules.system.service.dto.ImgTxtScanSaveDTO;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * 图片扫描录入管理 控制器
 *
 * @author Jevany
 * @date 2022/2/15 9:06
 */
@Slf4j
@Api(tags = "体检管理：图文扫描录入")
@RestController
@AllArgsConstructor
@RequestMapping("/ImgTxtScan")
public class ImgTxtScanManageController {

    private final ImgTxtScanManageService imgTxtScanManageService;

    @Log("图文扫描录入-列表")
    @ApiOperation("图文扫描录入-列表，权限码：ImgTxtScanManage:list")
    @GetMapping
    @PreAuthorize("@el.check('ImgTxtScanManage:list')")
    public R list(ImgTxtScanManageQueryCriteria criteria) {

        if (ObjectUtil.isEmpty(criteria.getPageNum()) || criteria.getPageNum() <= 0) {
            criteria.setPageNum(1);
        }
        if (ObjectUtil.isEmpty(criteria.getPageSize()) || criteria.getPageSize() <= 0) {
            criteria.setPageSize(10);
        }
        return R.ok(imgTxtScanManageService.getList(criteria));
    }

    @Log("图文扫描录入-详细")
    @ApiOperation("图文扫描录入-详细，权限码：ImgTxtScanManage:list")
    @GetMapping("/getImgTxtScan")
    @PreAuthorize("@el.check('ImgTxtScanManage:list')")
    public R getImgTxtScan(ImgTxtScanQueryDTO imgTxtScanQueryDTO) {

        if ("4".equals(imgTxtScanQueryDTO.getImgTxtTypeId()) && StrUtil.isBlank(imgTxtScanQueryDTO.getGroupId())) {
            return R.error("项目编号不能为空");
        }
        return R.ok(imgTxtScanManageService.getImgTxtScan(imgTxtScanQueryDTO));
    }

    @Log("图文扫描录入-保存")
    @ApiOperation("图文扫描录入-保存，权限码：ImgTxtScanManage:list")
    @PostMapping("/saveImgTxtScan")
    @PreAuthorize("@el.check('ImgTxtScanManage:list')")
    @Klock
    public R saveImgTxtScan(@RequestBody ImgTxtScanSaveDTO imgTxtScanSaveDTO) {
        if ("4".equals(imgTxtScanSaveDTO.getImgTxtTypeId()) && StrUtil.isBlank(imgTxtScanSaveDTO.getGroupId())) {
            return R.error("项目编号不能为空");
        }
        imgTxtScanManageService.saveImgTxtScan(imgTxtScanSaveDTO);
        return R.ok();
    }

    @Log("图文扫描录入-删除")
    @ApiOperation("图文扫描录入-删除，权限码：ImgTxtScanManage:list")
    @DeleteMapping
    @PreAuthorize("@el.check('ImgTxtScanManage:list')")
    @Klock
    public R delImgTxtScan(@RequestBody ImgTxtScanDelDTO imgTxtScanDelDTO) {
        if ("4".equals(imgTxtScanDelDTO.getImgTxtTypeId())) {
            if(StrUtil.isBlank(imgTxtScanDelDTO.getGroupId())) {
                return R.error("项目编号不能为空");
            }
            if(StrUtil.isBlank(imgTxtScanDelDTO.getFileId())) {
                return R.error("文件编号不能为空");
            }
        }
        imgTxtScanManageService.delImgTxtScan(imgTxtScanDelDTO);
        return R.ok();
    }
}
