package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BCommentZ;
import com.healthappy.modules.system.service.dto.BCommentZQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 从业结论表 服务层
 * @author fang
 * @date 2021-06-17
 */
public interface BCommentZService extends BaseService<BCommentZ> {


    Map<String, Object> queryAll(BCommentZQueryCriteria criteria, Pageable pageable);


    /**
     * 查询数据分页
     * @param criteria 条件
     * @return Map<String, Object>
     */
    List<BCommentZ> queryAll(BCommentZQueryCriteria criteria);
}
