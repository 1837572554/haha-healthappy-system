package com.healthappy.modules.system.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author FGQ
 */
@Data
@ApiModel(value = "用户标识中间表")
public class UserTag implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("标识ID")
    private Long tagId;

    @ApiModelProperty("用户ID")
    private Long userId;

}
