package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Jevany
 * @date 2021/12/10 15:40
 * @desc 中医体制特性
 */
@Data
@TableName("B_ZY_TZ")
public class BZyTz implements Serializable {

    /** 体制特质编号 */
    @ApiModelProperty("体制特质编号")
    private String id;

     /** 体制特质名称 */
    @ApiModelProperty("体制特质名称")
    private String name;

     /** 体制特质类型编号 */
    @ApiModelProperty("体制特质类型编号")
    private String type;

    /** 体制特质内容 */
    @ApiModelProperty("体制特质内容")
    private String content;
}
