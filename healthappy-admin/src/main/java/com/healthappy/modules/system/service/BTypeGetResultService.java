package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BTypeGetResult;
import com.healthappy.modules.system.service.dto.BTypeGetResultQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 结果获取方式 服务层
 * @author sjc
 * @date 2021-08-3
 */
public interface BTypeGetResultService extends BaseService<BTypeGetResult> {

    Map<String, Object> queryAll(BTypeGetResultQueryCriteria criteria, Pageable pageable);

    List<BTypeGetResult> queryAll(BTypeGetResultQueryCriteria criteria);

    boolean deleteById(Long id);
}
