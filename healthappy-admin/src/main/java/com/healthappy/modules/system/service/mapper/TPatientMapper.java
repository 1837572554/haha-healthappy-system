package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TPatient;
import com.healthappy.modules.system.domain.vo.BatchInfoEditVO;
import com.healthappy.modules.system.domain.vo.BatchProjectAdjustVO;
import com.healthappy.modules.system.domain.vo.BatchResultEntryVO;
import com.healthappy.modules.system.service.dto.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author sjc
 * @description 体检人员相关mapper
 * @date 2021-06-28
 */
@Mapper
@Repository
public interface TPatientMapper extends CoreMapper<TPatient> {

	/**
	 * 删除体检人员数据
	 * @author YJ
	 * @date 2022/3/3 10:43
	 * @param id
	 * @param tenantId
	 * @return boolean
	 */
	@SqlParser(filter = true)
	boolean deleteById(@Param("id") String id, @Param("tenantId") String tenantId);

	List<TPatient> queryAll(@Param("criteria") TPatientQueryCriteria criteria, @Param("tenantId") String tenantId);

	/**
	 * 查询体检号的结论、建议
	 *
	 * @param id
	 * @param resultMark
	 * @param type
	 * @return java.util.List〈com.healthappy.modules.system.service.dto.PersonalReportSummaryDto〉
	 * @author YJ
	 * @date 2022/3/1 11:44
	 */
	List<PersonalReportSummaryDto> summary(@Param("id") String id, @Param("resultMark") String resultMark,
			@Param("type") Integer type);

	List<HistoricalComparisonDto> getDataPrimaryKeyByIdNo(@Param("id") String id);

	List<HistoricalComparisonDto.ItemHd> buildItemHdAndDtList(@Param("id") String id);

	List<TPatientLiteDto> getPatientLite(@Param("criteria") MedicalRegistrationQueryCriteria criteria);

	/**
	 * 项目检查界面 体检者查询
	 *
	 * @param criteria
	 * @return java.util.List〈com.healthappy.modules.system.service.dto.ProjectCheckDto〉
	 * @author YJ
	 * @date 2021/12/23 19:45
	 */
	@SqlParser(filter = true)
	List<ProjectCheckLiteDto> getProjectCheckList(@Param("criteria") ProjectCheckQueryCriteria criteria);

	/**
	 * 体检人员第一个项目 体检号，签到时间，租户号，必填
	 *
	 * @param tPatient
	 * @return java.lang.Integer
	 * @author YJ
	 * @date 2022/1/11 10:05
	 */
	@SqlParser(filter = true)
	Integer updSignDate(@Param("tPatient") TPatient tPatient);

	/**
	 * 获得综合管理列表总数
	 *
	 * @param criteria
	 * @return java.lang.Integer
	 * @author YJ
	 * @date 2022/2/8 15:58
	 */
	Integer getIntegratedManageCount(@Param("criteria") IntegratedManageQueryCriteria criteria);

	/**
	 * 获得综合管理列表总数
	 *
	 * @param criteria
	 * @return java.lang.Integer
	 * @author YJ
	 * @date 2022/2/8 15:58
	 */
	List<IntegratedManageDTO> getIntegratedManageList(@Param("criteria") IntegratedManageQueryCriteria criteria);

	/**
	 * 获取图文扫描录入管理列表
	 *
	 * @param criteria
	 * @return java.util.List〈com.healthappy.modules.system.service.dto.TPatientLiteDto〉
	 * @author YJ
	 * @date 2022/2/15 17:21
	 */
	List<TPatientLiteDto> getImgTxtScanManageList(@Param("criteria") ImgTxtScanManageQueryCriteria criteria);

	/**
	 * 获取图文扫描录入管理条数
	 *
	 * @param criteria
	 * @return java.lang.Integer
	 * @author YJ
	 * @date 2022/2/15 18:21
	 */
	Integer getImgTxtScanCount(@Param("criteria") ImgTxtScanManageQueryCriteria criteria);

	/**
	 * 获取图文扫描录入管理列表
	 *
	 * @param criteria
	 * @return java.util.List〈com.healthappy.modules.system.service.dto.TPatientLiteDto〉
	 * @author YJ
	 * @date 2022/2/15 17:21
	 */
	List<AloneReportPrintDto> getAloneReportPrintList(@Param("criteria") AloneReportPrintCriteria criteria);

	/**
	 * 获取图文扫描录入管理条数
	 *
	 * @param criteria
	 * @return java.lang.Integer
	 * @author YJ
	 * @date 2022/2/15 18:21
	 */
	Integer getAloneReportPrintCount(@Param("criteria") AloneReportPrintCriteria criteria);

	/**
	 * 查询单位职业报告人员列表
	 *
	 * @param criteria 单位职业报告管理-人员查询类
	 * @return java.util.List〈com.healthappy.modules.system.service.dto.CompanyReportPersonDTO〉
	 * @author YJ
	 * @date 2022/2/18 11:46
	 */
	List<CompanyReportPersonDTO> getCompanyOccupationReportPerson(
			@Param("criteria") CompanyReportOccupationPersonQueryCriteria criteria);

	/**
	 * 查询单位职业报告人员列表-条数
	 *
	 * @param criteria 单位职业报告管理-人员查询类
	 * @return java.lang.Integer
	 * @author YJ
	 * @date 2022/2/18 17:49
	 */
	Integer getCompanyOccupationReportPersonCount(
			@Param("criteria") CompanyReportOccupationPersonQueryCriteria criteria);

	/**
	 * 查询单位健康报告人员列表
	 *
	 * @param criteria 单位健康报告管理-人员查询类
	 * @return java.util.List〈com.healthappy.modules.system.service.dto.CompanyReportPersonDTO〉
	 * @author YJ
	 * @date 2022/2/18 11:47
	 */
	List<CompanyReportPersonDTO> getCompanyHealthReportPerson(
			@Param("criteria") CompanyReportHealthPersonQueryCriteria criteria);

	/**
	 * 查询单位健康报告人员列表-条数
	 *
	 * @param criteria 单位健康报告管理-人员查询类
	 * @return java.util.List〈com.healthappy.modules.system.service.dto.CompanyReportPersonDTO〉
	 * @author YJ
	 * @date 2022/2/18 11:47
	 */
	Integer getCompanyHealthReportPersonCount(@Param("criteria") CompanyReportHealthPersonQueryCriteria criteria);

	/**
	 * 根据报告编号 获得报告关联的人员数量
	 *
	 * @param repId
	 * @return java.lang.Integer
	 * @author YJ
	 * @date 2022/2/18 19:41
	 */
	@SqlParser(filter = true)
	Integer getPersonCount(@Param("repId") String repId);

	/**
	 * 根据报告编号 获得报告关联的人员数据列表
	 *
	 * @param criteria
	 * @return java.util.List〈com.healthappy.modules.system.service.dto.CompanyReportPersonDTO〉
	 * @author YJ
	 * @date 2022/2/18 19:46
	 */
	List<CompanyReportPersonDTO> getPersons(@Param("criteria") CompanyReportPersonQueryCriteria criteria);

	/**
	 * 获取体检报告发布列表
	 * @author YJ
	 * @date 2022/3/3 14:57
	 * @param criteria
	 * @return java.util.List〈com.healthappy.modules.system.service.dto.ReportReleaseDTO〉
	 */
	List<ReportReleaseDTO> listReportRelease(@Param("criteria") ReportReleaseQueryCriteria criteria);

	/**
	 * 批量删除体检人员数据
	 * @author YJ
	 * @date 2022/3/3 10:43
	 * @param ids
	 * @param tenantId
	 * @return boolean
	 */
	@SqlParser(filter = true)
	Boolean deleteByIds(@Param("ids") String ids, @Param("tenantId") String tenantId);

	/**
	 * 获取人员的条码列表
	 * @author YJ
	 * @date 2022/5/12 16:10
	 * @param paId 流水号
	 * @return java.util.List〈BarcodeVO〉
	 */
	List<PatientBarcodeDTO> getBarcodeByPaId(@Param("paId") String paId);

	/**
	 * 获取批量修改信息列表
	 * @author YJ
	 * @date 2022/5/13 10:33
	 * @param criteria
	 * @return java.util.List〈com.healthappy.modules.system.domain.vo.BatchInfoEditVO〉
	 */
	List<BatchInfoEditVO> listBatchInfoEdit(@Param("criteria") BatchInfoEditQueryCriteria criteria);

	/**
	 * 批量修改信息列表
	 * @author YJ
	 * @date 2022/5/13 20:03
	 * @param batchInfoEditVOList
	 */
	void batchInfoEdit(@Param("list") List<BatchInfoEditVO> batchInfoEditVOList);

	/**
	 * 获取批量项目调整信息列表
	 * @author YJ
	 * @date 2022/5/13 18:06
	 * @param criteria
	 * @return java.util.List〈com.healthappy.modules.system.domain.vo.BatchProjectAdjustVO〉
	 */
	List<BatchProjectAdjustVO> listBatchProjectAdjust(@Param("criteria") BatchProjectAdjustQueryCriteria criteria);

	/**
	 * 获取批量结果录入列表
	 * @author YJ
	 * @date 2022/5/13 18:06
	 * @param criteria
	 * @return java.util.List〈com.healthappy.modules.system.domain.vo.BatchProjectAdjustVO〉
	 */
	List<BatchResultEntryVO> listBatchResultEntry(@Param("criteria") BatchResultEntryQueryCriteria criteria);
}
