package com.healthappy.modules.system.service;

import cn.hutool.core.lang.tree.Tree;
import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BTypeComIndustry;
import com.healthappy.modules.system.service.dto.BTypeComIndustryQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 企业从事行业类型表
 * @author sjc
 * @date 2021-08-30
 */
public interface BTypeComIndustryService extends BaseService<BTypeComIndustry> {


    List<Tree<String>> queryAll(BTypeComIndustryQueryCriteria criteria);
}
