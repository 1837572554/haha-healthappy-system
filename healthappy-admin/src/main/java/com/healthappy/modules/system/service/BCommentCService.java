package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BCommentC;
import com.healthappy.modules.system.service.dto.BCommentCQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 从业结论表 服务层
 * @author fang
 * @date 2021-06-17
 */
public interface BCommentCService extends BaseService<BCommentC> {


    Map<String, Object> queryAll(BCommentCQueryCriteria criteria, Pageable pageable);


    /**
     * 查询数据分页
     * @param criteria 条件
     * @return Map<String, Object>
     */
    List<BCommentC> queryAll(BCommentCQueryCriteria criteria);
}
