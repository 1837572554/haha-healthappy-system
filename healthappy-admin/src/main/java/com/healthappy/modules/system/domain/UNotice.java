package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.config.SeedIdGenerator;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * @description 公告
 * @author fang
 * @date 2021-06-18
 */
@Data
@TableName("U_Notice")
@SeedIdGenerator
public class UNotice implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.INPUT)
    /**
     * id
     */
    private String id;

    @ApiModelProperty("标题")
    private String title;

    /**
     * 公告信息
     */
    @ApiModelProperty("公告信息")
    private String notice;

    /**
     * 发布者
     */
    @ApiModelProperty("发布者")
    private String users;

    /**
     * 发布时间
     */
    @ApiModelProperty("发布时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp time;

    /**
     * 有效时间
     */
    @NotNull(message = "有效时间不能为空")
    @ApiModelProperty("有效时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp validDate;

    /**
     * 截止日期
     */
    @NotNull(message = "截止日期不能为空")
    @ApiModelProperty("截止日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Timestamp expirationDate;

    /**
     * 被公告科室id，多个id
     */
    @ApiModelProperty("被公告的科室ID")
    @TableField(exist=false)
    private List<String> deptId;

    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;
}
