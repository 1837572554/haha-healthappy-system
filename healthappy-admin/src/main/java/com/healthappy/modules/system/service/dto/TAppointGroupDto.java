package com.healthappy.modules.system.service.dto;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 预约表
 * @author sjc
 * @date 2021-09-14
 */
@Data
public class TAppointGroupDto implements Serializable {

    /**
     * 预约号
     */
    @ApiModelProperty("预约号")
    private String appointId;



    /**
     * 组合项目ID
     */
    @ApiModelProperty("组合项目ID")
    private String groupId;


    /**
     * 付费方式 0自费,1统收
     */
    @ApiModelProperty("付费方式 0自费,1统收")
    private Integer payType;


    /**
     * 价格
     */
    @ApiModelProperty("价格")
    private Double price;


    /**
     * 其他ID，预留
     */
    @ApiModelProperty("其他ID，预留")
    private String otherId;


    /**
     * 是否放弃缴费  0否 1是 默认0
     */
    @ApiModelProperty("是否放弃缴费  0否 1是 默认0")
    private Integer discard;



    /**
     * 医疗机构ID(U_Hospital_Info的ID)
     */
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;
}
