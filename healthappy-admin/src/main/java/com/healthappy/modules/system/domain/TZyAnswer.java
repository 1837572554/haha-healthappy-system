package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Jevany
 * @date 2021/12/8 19:08
 * @desc 体检者问卷 答案
 */
@Data
@TableName("T_ZY_ANSWER")
@ApiModel("体检者问卷答案表")
public class TZyAnswer {

    /** 答案库编号 */
    @ApiModelProperty("答案库编号")
    private String answerBankId;

    /** 问题编号 */
    @ApiModelProperty("问题编号")
    private String questionId;

    /** 答案编号 */
    @ApiModelProperty("答案编号")
    public Integer answerId;

    /** 医疗机构ID(U_Hospital_Info的ID) */
    @ApiModelProperty("医疗机构ID(U_Hospital_Info的ID)")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    public String tenantId;
}
