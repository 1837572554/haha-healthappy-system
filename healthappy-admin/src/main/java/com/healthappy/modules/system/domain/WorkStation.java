package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 *  工作站代码表
 * @author FGQ
 */
@Data
@TableName("U_WorkStation")
public class WorkStation implements Serializable {

    /**
     * 工作站代码 建议区间设置大点01，别的站设06
     */
    @ApiModelProperty("工作站代码")
    private String code;

    /**
     * 租户ID
     */
    @ApiModelProperty("租户ID")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;

    /**
     * 是否启用
     */
    @ApiModelProperty("是否启用")
    private Boolean isEnable;

    /**
     * 工作站名称
     * @author YJ
     * @date 2021/11/26 15:58
     */
    private String name;
}
