package com.healthappy.modules.system.service.dto;

import com.healthappy.modules.system.domain.ZhToSickness;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;


@EqualsAndHashCode(callSuper = true)
@Data
public class ZhToSicknessDto extends ZhToSickness {

    @ApiModelProperty("病种名称")
    private String sicknessName;

    @ApiModelProperty("病种建议ID")
    private String sicknessAdviceId;

    @ApiModelProperty("病种建议")
    private String sicknessAdviceJy;
}
