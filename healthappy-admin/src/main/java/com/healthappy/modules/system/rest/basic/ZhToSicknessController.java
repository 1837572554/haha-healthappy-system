package com.healthappy.modules.system.rest.basic;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.ZhToSickness;
import com.healthappy.modules.system.domain.vo.ZhToSicknessVo;
import com.healthappy.modules.system.service.BSicknessZHService;
import com.healthappy.modules.system.service.ZhToSicknessService;
import com.healthappy.modules.system.service.dto.ZhToSicknessQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 组合判断表 控制器
 * @author sjc
 * @date 2021-08-3
 */
@Slf4j
@Api(tags = "基础设置：组合判断设置")
@RestController
@AllArgsConstructor
@RequestMapping("/ZhToSickness")
public class ZhToSicknessController {
    private final IGenerator generator;
    private final ZhToSicknessService zhToSicknessService;
    private final BSicknessZHService bSicknessZHService;

    @Log("分页查询组合判断数据")
    @ApiOperation("分页查询组合判断数据，权限码：ZhToSickness:list")
    @GetMapping
    public R listPage(ZhToSicknessQueryCriteria criteria, Pageable pageable) {
        return R.ok(zhToSicknessService.queryAll(criteria, pageable));
    }

    @Log("新增|修改组合判断表")
    @ApiOperation("新增|修改组合判断表，权限码：ZhToSickness:add")
    @PostMapping
    @PreAuthorize("@el.check('ZhToSickness:add')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody ZhToSicknessVo resources) {
        if(checkNameRepeat(resources)){
            return  R.error("组合名称不能重复");
        }
        if(CollUtil.isEmpty(resources.getBSicknessZHVoList())){
            return R.error("病种集合不能为空");
        }
        zhToSicknessService.saveOrUpdateZhToSickness(resources);
        return  R.ok();
    }

    @Log("删除组合判断")
    @ApiOperation("删除组合判断，权限码：ZhToSickness:delete")
    @DeleteMapping
    @PreAuthorize("@el.check('ZhToSickness:delete')")
    @Klock
    public R remove(@RequestBody Set<String> ids) {
        if(zhToSicknessService.removeByIds(ids))
        {
            bSicknessZHService.deleteBySicknessZhIds(ids);
            return R.ok();
        }
        return R.error(500,"方法异常");
    }

    private boolean checkNameRepeat(ZhToSicknessVo resources) {
        return zhToSicknessService.lambdaQuery().eq(ZhToSickness::getZhName, resources.getZhName())
                .ne(ObjectUtil.isNotNull(resources.getId()),ZhToSickness::getId,resources.getId()).count() > 0;
    }
}
