package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @description 申请类型
 * @author sjc
 * @date 2021-08-3
 */
@Data
public class BTypeDeviceQueryCriteria {


    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    @Query(type = Query.Type.EQUAL)
    private Long id;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    @Query(type = Query.Type.INNER_LIKE)
    private String name;

}
