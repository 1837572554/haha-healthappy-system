package com.healthappy.modules.system.rest.system;

import com.healthappy.config.databind.DataBind;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.DictDetail;
import com.healthappy.modules.system.service.DictDetailService;
import com.healthappy.modules.system.service.dto.DictDetailQueryCriteria;
import com.healthappy.utils.CacheConstant;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author hupeng
 * @date 2019-04-10
 */
@RestController
@Api(tags = "系统：字典详情管理")
@RequestMapping("/api/dictDetail")
public class DictDetailController {

    private final DictDetailService dictDetailService;
    private static final String ENTITY_NAME = "dictDetail";
    private final DataBind dataBind;

    public DictDetailController(DictDetailService dictDetailService, DataBind dataBind) {
        this.dictDetailService = dictDetailService;
        this.dataBind = dataBind;
    }

    @Log("查询字典详情")
    @ApiOperation("查询字典详情，权限码：dict:list")
    @GetMapping
    @PreAuthorize("@el.check('dict:list')")
    public R getDictDetails(DictDetailQueryCriteria criteria,
                            @PageableDefault(sort = {"sort"}, direction = Sort.Direction.ASC) Pageable pageable) {
        return R.ok(dictDetailService.queryAll(criteria, pageable));
    }

    @Log("查询多个字典详情")
    @ApiOperation("查询多个字典详情，权限码：dict:list")
    @GetMapping(value = "/map")
    @PreAuthorize("@el.check('dict:list')")
    public R getDictDetailMaps(DictDetailQueryCriteria criteria,
                               @PageableDefault(sort = {"sort"}, direction = Sort.Direction.ASC) Pageable pageable) {
        String[] names = criteria.getDictName().split(",");
        Map<String, Object> map = new HashMap<>(names.length);
        for (String name : names) {
            criteria.setDictName(name);
            map.put(name, dictDetailService.queryAll(criteria, pageable).get("content"));
        }
        return R.ok(map);
    }

    @Log("新增字典详情")
    @ApiOperation("新增字典详情，权限码：admin,dict:add")
    @PostMapping
    @PreAuthorize("@el.check('admin','dict:add')")
    @CacheEvict(cacheNames = {CacheConstant.DICT_DETAIL,CacheConstant.B_DEPT}, allEntries = true)
    @Klock
    public R create(@Validated @RequestBody DictDetail resources) {
        if (resources.getId() != null) {
            throw new BadRequestException("A new " + ENTITY_NAME + " cannot already have an ID");
        }
        resources.setDictId(resources.getDict().getId());
        if(dictDetailService.lambdaQuery().eq(DictDetail::getDictId, resources.getDictId())
                .and(wrapper->wrapper.eq(DictDetail::getLabel, resources.getLabel()).or().eq(DictDetail::getValue,resources.getValue())).count()>0){
            throw new BadRequestException("标签或值已存在");
        }
        boolean flag = dictDetailService.save(resources);
        dataBind.resetMap();
        return R.ok(flag);
    }


    @Log("修改字典详情")
    @ApiOperation("修改字典详情，权限码：admin,dict:edit")
    @PutMapping
    @PreAuthorize("@el.check('admin','dict:edit')")
    @CacheEvict(cacheNames = {CacheConstant.DICT_DETAIL,CacheConstant.B_DEPT}, allEntries = true)
    @Klock
    public R update(@Validated @RequestBody DictDetail resources) {
        if(dictDetailService.lambdaQuery().eq(DictDetail::getDictId, resources.getDictId()).ne(DictDetail::getId,resources.getId())
                .and(wrapper->wrapper.eq(DictDetail::getLabel, resources.getLabel()).or().eq(DictDetail::getValue,resources.getValue())).count()>0){
            throw new BadRequestException("标签或值已存在");
        }
        boolean flag = dictDetailService.saveOrUpdate(resources);
        dataBind.resetMap();
        return R.ok(flag);
    }

    @Log("删除字典详情")
    @ApiOperation("删除字典详情，权限码：admin,dict:delete")
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("@el.check('admin','dict:delete')")
    @CacheEvict(cacheNames = {CacheConstant.DICT_DETAIL,CacheConstant.B_DEPT}, allEntries = true)
    @Klock
    public R delete(@PathVariable Long id) {
        boolean flag = dictDetailService.removeById(id);
        dataBind.resetMap();
        return R.ok(flag);
    }
}
