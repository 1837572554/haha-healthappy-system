package com.healthappy.modules.system.rest.ballcheck;

import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.service.IsSubmitService;
import com.healthappy.modules.system.service.dto.IsSubmitQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 指引单 控制器
 * @author FGQ
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "体检流程：指引单回交")
@RestController
@AllArgsConstructor
@RequestMapping("/is-submit")
public class IsSubmitController {

    private final IsSubmitService isSubmitService;

    @Log("列表")
    @ApiOperation("列表，权限码：isSubmit:list")
    @GetMapping
    @PreAuthorize("@el.check('isSubmit:list')")
    public R listPage(IsSubmitQueryCriteria criteria, @PageableDefault(sort = {"id"}) Pageable pageable) {
        return R.ok(isSubmitService.pageAll(criteria,pageable));
    }

    @Log("详情")
    @ApiOperation("详情，权限码：isSubmit:list")
    @GetMapping("/detail/{id}")
    @PreAuthorize("@el.check('isSubmit:list')")
    public R detail(@PathVariable String id) {
        return R.ok(isSubmitService.detail(id));
    }

    @Log("回交/撤销")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "true:回交,false:撤销", dataType = "Boolean"),
            @ApiImplicitParam(name = "id", value = "流水号", dataType = "String"),
            @ApiImplicitParam(name = "itemIds", value = "组合项目组", dataType = "List"),
            @ApiImplicitParam(name = "status", value = "类型true:提交,false:取消", dataType = "Boolean"),
    })
    @ApiOperation("回交/撤销，权限码：isSubmit:upd")
    @PostMapping("{type}/{id}/{status}")
    @PreAuthorize("@el.check('isSubmit:upd')")
    @Klock
    public R backSubmitRevocation(@RequestBody Set<String> itemIds, @PathVariable Boolean type, @PathVariable String id, @PathVariable Boolean status){
        isSubmitService.backSubmitRevocation(itemIds,type,id,status);
        return R.ok();
    }
}
