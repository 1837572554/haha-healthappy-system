package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BDepartment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @description 部门表
 * @author sjc
 * @date 2021-06-24
 */
@Mapper
@Repository
public interface BDepartmentMapper extends CoreMapper<BDepartment> {

    /**
     * 获得单位下最大的科室编号，对象中只有编号
     * @param companyId 公司编号
     * @param tenantId 租户编号
     * @return 获得单位下最大的科室编号，对象中只有编号
     */
    BDepartment getMaxDepartment(@Param("companyId") String companyId, @Param("tenantId") String tenantId);
}
