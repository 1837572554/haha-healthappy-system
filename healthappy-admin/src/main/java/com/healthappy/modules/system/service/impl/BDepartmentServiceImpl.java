package com.healthappy.modules.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.BDepartment;
import com.healthappy.modules.system.service.BCompanyService;
import com.healthappy.modules.system.service.BDepartmentService;
import com.healthappy.modules.system.service.TidSeedService;
import com.healthappy.modules.system.service.dto.BDepartmentQueryCriteria;
import com.healthappy.modules.system.service.mapper.BDepartmentMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sjc
 * @date 2021-06-24
 */
@Service
@AllArgsConstructor//有自动注入的功能
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BDepartmentServiceImpl extends BaseServiceImpl<BDepartmentMapper, BDepartment> implements BDepartmentService {
//    private final BDepartmentMapper bDepartmentMapper;
    private final IGenerator generator;
    private final BCompanyService bCompanyService;
    private final TidSeedService tidSeedService;

    @Override
    public Map<String, Object> queryAll(BDepartmentQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BDepartment> page = new PageInfo<BDepartment>(queryAlls(criteria));
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("content", generator.convert(page.getList(), BDepartment.class));
        map.put("totalElements", page.getTotal());

        return map;
    }

    /**
     * 查询全部数据
     * @return Object
     */
    @Override
    public List<BDepartment> queryAlls(BDepartmentQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BDepartment.class, criteria));
//        return bDepartmentMapper.selectList(QueryHelpPlus.getPredicate(BDepartment.class, criteria));
    }

    /**
     * 判断部门是否存在
     * @param department_id
     * @return
     */
    @Override
    public boolean isExistDepartment(String department_id)
    {
        return this.getById(department_id)!=null;
    }

    /**
     * 获得单位下最大的部门编号
     * @param companyId 单位编号
     * @param tenantId  租户编号
     * @return
     */
    @Override
    public BDepartment getMaxIDDepartment(String companyId,String tenantId) {
        return baseMapper.getMaxDepartment(companyId,tenantId);
    }

    /**
     * 根据单位编号，生成部门编号
     * @param companyId 单位编号
     * @param tenantId  租户编号
     * @return
     */
    @Override
    public String generateDepartmentKey(String companyId,String tenantId){
        if(!bCompanyService.isExistCompany(companyId)){
            throw new BadRequestException("公司编号不存在");
        }
        String bDepartmentId = tidSeedService.createASeendPrimaryKeyNoWorkStation("B_Department", 6);
        return companyId+bDepartmentId;
    }

    /**
     * 是否存在此租户和单位编号
     * @param companyId
     * @param tenantId
     * @return
     */
    @Override
    public boolean isExistCompanyID(String companyId, String tenantId) {
        return baseMapper.selectCount(new LambdaQueryWrapper<BDepartment>().eq(BDepartment::getComId,companyId).eq(BDepartment::getTenantId,tenantId))>0;
    }

}
