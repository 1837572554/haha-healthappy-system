package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@TableName("B_PeType_DT")
@ApiModel(value = "体检类别",discriminator = "体检类别：小类")
public class BPeTypeDT implements Serializable {

    private static final long SerialVersionUID = 111839700929517618L;

    @Query(type = Query.Type.EQUAL)
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "主键id")
    private Long id;

    @Query(type = Query.Type.INNER_LIKE)
    @ApiModelProperty("体检类别名称")
    @NotBlank(message = "体检类别名称不能为空")
    private String name;

    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("体检类型ID")
    @NotBlank(message = "体检类型ID不能为空")
    @TableField("hd_id")
    private Long hdID;

    @TableField("disp_order")
    @ApiModelProperty("排序")
    private Integer dispOrder;


}
