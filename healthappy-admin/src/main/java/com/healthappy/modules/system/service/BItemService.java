package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BItem;
import com.healthappy.modules.system.service.dto.BItemQueryCriteria;
import com.healthappy.modules.system.service.dto.BItemTreeDto;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 体检项目 服务层
 * @author fang
 * @date 2021-06-17
 */
public interface BItemService extends BaseService<BItem> {


	Map<String, Object> queryAll(BItemQueryCriteria criteria, Pageable pageable);


	/**
	 * 查询数据分页
	 * @param criteria 条件
	 * @return Map<String, Object>
	 */
	List<BItem> queryAll(BItemQueryCriteria criteria);

	List<BItemTreeDto> getTree();

	/**
	 * 根据组合项目获得明细项
	 * @author YJ
	 * @date 2022/5/16 17:56
	 * @param groupId
	 * @return java.util.List〈com.healthappy.modules.system.domain.BItem〉
	 */
	List<BItem> listByGroupId(String groupId);
}
