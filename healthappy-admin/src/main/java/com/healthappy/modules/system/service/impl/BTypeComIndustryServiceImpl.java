package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNode;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import com.alipay.api.domain.CategoryVO;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BArea;
import com.healthappy.modules.system.domain.BGroupDT;
import com.healthappy.modules.system.domain.BTypeComIndustry;
import com.healthappy.modules.system.domain.BTypeCompany;
import com.healthappy.modules.system.service.BTypeComIndustryService;
import com.healthappy.modules.system.service.BTypeCompanyService;
import com.healthappy.modules.system.service.dto.BTypeComIndustryDto;
import com.healthappy.modules.system.service.dto.BTypeComIndustryQueryCriteria;
import com.healthappy.modules.system.service.dto.BTypeCompanyQueryCriteria;
import com.healthappy.modules.system.service.dto.TAppointDto;
import com.healthappy.modules.system.service.mapper.BTypeComIndustryMapper;
import com.healthappy.modules.system.service.mapper.BTypeCompanyMapper;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @description 企业从事行业类型表
 * @author sjc
 * @date 2021-08-30
 */
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BTypeComIndustryServiceImpl extends BaseServiceImpl<BTypeComIndustryMapper, BTypeComIndustry> implements BTypeComIndustryService {

    private final IGenerator generator;

    /**
     * 查询企业经济类型全部数据
     * @return Object
     */
    @Override
    public List<Tree<String>> queryAll(BTypeComIndustryQueryCriteria criteria) {
        List<BTypeComIndustry> categoryList = this.list();


        if(StrUtil.isNotBlank(criteria.getName()) || StrUtil.isNotBlank(criteria.getCode()) || ObjectUtil.isNotNull(criteria.getId())){
            BTypeComIndustry comIndustry = this.getOne(new LambdaQueryWrapper<BTypeComIndustry>().like(StrUtil.isNotBlank(criteria.getCode()),BTypeComIndustry::getCode,criteria.getCode())
                    .like(StrUtil.isNotBlank(criteria.getName()),BTypeComIndustry::getName, criteria.getName())
                    .last(" limit 1")
                    .eq(ObjectUtil.isNotNull(criteria.getId()),BTypeComIndustry::getId,criteria.getId()));

            ArrayList<BTypeComIndustry> list = new ArrayList<>();
            buildReverseTree(list,categoryList,comIndustry.getHiCode());
            categoryList.clear();
            categoryList = new ArrayList<>();
            categoryList.add(comIndustry);
            categoryList.addAll(list);
        }


        List<TreeNode<String>> nodeList = CollUtil.newArrayList();
        for (BTypeComIndustry area:categoryList) {
            TreeNode<String> treeNode = new TreeNode<String>(area.getCode(),area.getHiCode(),area.getName(),"0");
            Map<String,Object> map = new HashMap<>(2);
            map.put("code",area.getCode());
            map.put("hiCode",area.getHiCode());
            treeNode.setExtra(map);
            nodeList.add(treeNode);
        }
        return TreeUtil.build(nodeList,"");
    }


    //递归查询 最顶级 Tree
    private void buildReverseTree(List<BTypeComIndustry> bAreas, List<BTypeComIndustry> areaList,String hiCode) {
        Optional<BTypeComIndustry> optional = areaList.stream().filter(x -> x.getCode().equals(hiCode)).findAny();
        if(optional.isPresent()){
            BTypeComIndustry bArea = optional.get();
            bAreas.add(bArea);
            this.buildReverseTree(bAreas,areaList,bArea.getHiCode());
        }
    }

}
