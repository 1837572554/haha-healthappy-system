package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

/**
 * 体检报告发布领取、撤销DTO
 * @author Jevany
 * @date 2022/3/7 11:28
 */
@Data
@ApiModel("体检报告发布领取、撤销DTO")
public class reportReleaseReceiveDTO  implements Serializable {
    /** 发布编号集合，针对某人使用，如果有此数据，就不走流水号的数据 */
    private Set<String> releaseIdList;

    /** 流水号列表，批量操作使用 */
    private Set<String> paIdList;
}