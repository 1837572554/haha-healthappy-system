package com.healthappy.modules.system.rest.print.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Author: wukefei
 * @Date: Created in 2022/4/28 12:03
 * @Description:
 * @Version: 1.0
 *
 */
@ApiModel("")
@Data
public class BasePrintVo {
	/** 流水号列表 */
	@ApiModelProperty("流水号")
	private List<String> Ids;

	/** 用于接收的Socket的用户编号 */
	@ApiModelProperty("用于接收的Socket的用户编号")
	private String acceptSocketUserId;

}
