package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description t_phone
 * @author FGQ
 * @date 2021-10-26
 */
@Data
@ApiModel("t_photo")
@TableName("t_photo")
@RenewLog
public class TPhoto implements Serializable {

    @TableId(type = IdType.INPUT)
    /**
     * id
     */
    @ApiModelProperty("流水号")
    private String id;

    /**
     * 医疗机构id(u_hospital_info的id)
     */
    @ApiModelProperty("医疗机构id(u_hospital_info的id)")
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;

    /**
     * 身份证图片
     */
    @ApiModelProperty("身份证图片")
    private String idImage;

    /**
     * 人像
     */
    @ApiModelProperty("人像")
    private String portrait;
}
