package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.healthappy.config.SeedIdGenerator;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 综合设置
 * @author cyt
 * @date 2021-08-17
 */
@Data
@TableName("B_Set_Up")
@SeedIdGenerator
public class BSetUp  implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.INPUT)
    /**
     * id
     */
    @ApiModelProperty("id")
    private String id;

    /**
     * 设置名称
     */
    @ApiModelProperty("名称")
    private String xmlName;

    /**
     * 设置值
     */
    @ApiModelProperty("值")
    private String value;

    /**
     * 医疗机构ID
     */
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    private String tenantId;
}
