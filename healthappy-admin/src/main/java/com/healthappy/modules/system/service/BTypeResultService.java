package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BTypeResult;

/**
 * @description 结果类型 服务层
 * @author fang
 * @date 2021-06-17
 */
public interface BTypeResultService extends BaseService<BTypeResult> {


}
