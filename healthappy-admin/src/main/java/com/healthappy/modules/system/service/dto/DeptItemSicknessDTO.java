package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 科室、明细项、病种数据
 * @author Jevany
 * @date 2022/3/8 14:31
 */
@Data
@ApiModel("科室、明细项、病种数据")
public class DeptItemSicknessDTO implements Serializable {
    /** 科室编号 */
    @ApiModelProperty("科室编号")
    private String deptId;

    /** 科室名称 */
    @ApiModelProperty("科室名称")
    private String deptName;

    /** 明细项id */
    @ApiModelProperty("明细项id")
    private String itemId;

    /** 明细项名称 */
    @ApiModelProperty("明细项名称")
    private String itemName;

    /** 病种id */
    @ApiModelProperty("病种id")
    private String sicknessId;

    /** 病种名称 */
    @ApiModelProperty("病种名称")
    private String sicknessName;

    /** 病种规则id */
    @ApiModelProperty("病种规则id")
    private String sicknessRuleId;
}