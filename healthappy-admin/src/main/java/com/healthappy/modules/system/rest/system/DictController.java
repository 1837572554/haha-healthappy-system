package com.healthappy.modules.system.rest.system;

import com.healthappy.config.databind.DataBind;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.Dict;
import com.healthappy.modules.system.service.DictService;
import com.healthappy.modules.system.service.dto.DictDto;
import com.healthappy.modules.system.service.dto.DictQueryCriteria;
import com.healthappy.utils.CacheConstant;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author hupeng
 * @date 2019-04-10
 */
@Api(tags = "系统：字典管理")
@RestController
@RequestMapping("/api/dict")
public class DictController {

    private final DictService dictService;
    private final IGenerator generator;
    private final DataBind dataBind;

    private static final String ENTITY_NAME = "dict";

    public DictController(DictService dictService, IGenerator generator, DataBind dataBind) {
        this.dictService = dictService;
        this.generator = generator;
        this.dataBind = dataBind;
    }

    @Log("导出字典数据")
    @ApiOperation("导出字典数据，权限码：admin,dict:list")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('admin','dict:list')")
    @Klock
    public void download(HttpServletResponse response, DictQueryCriteria criteria) throws IOException {
        dictService.download(generator.convert(dictService.queryAll(criteria), DictDto.class), response);
    }

    @Log("查询字典")
    @ApiOperation("查询字典，权限码：admin,dict:list")
    @GetMapping(value = "/all")
    @PreAuthorize("@el.check('admin','dict:list')")
    public R all() {
        return R.ok(dictService.queryAll(new DictQueryCriteria()));
    }

    @Log("查询字典")
    @ApiOperation("查询字典，权限码：admin,dict:list")
    @GetMapping
    @PreAuthorize("@el.check('admin','dict:list')")
    public R getDicts(DictQueryCriteria resources, Pageable pageable) {
        return R.ok(dictService.queryAll(resources, pageable));
    }

    @Log("新增字典")
    @ApiOperation("新增字典，权限码：admin,dict:add")
    @PostMapping
    @PreAuthorize("@el.check('admin','dict:add')")
    @CacheEvict(cacheNames = {CacheConstant.DICT,CacheConstant.DICT_DETAIL,CacheConstant.B_DEPT}, allEntries = true)
    @Klock
    public R create(@Validated @RequestBody Dict resources) {
        if (resources.getId() != null) {
            throw new BadRequestException("A new " + ENTITY_NAME + " cannot already have an ID");
        }
        if (dictService.lambdaQuery().eq(Dict::getName, resources.getName()).count() > 0) {
            throw new BadRequestException("字典名称已存在");
        }
        boolean flag = dictService.save(resources);
        dataBind.resetMap();
        return R.ok(flag);
    }

    @Log("修改字典")
    @ApiOperation("修改字典，权限码：admin,dict:edit")
    @PutMapping
    @PreAuthorize("@el.check('admin','dict:edit')")
    @CacheEvict(cacheNames = {CacheConstant.DICT,CacheConstant.DICT_DETAIL,CacheConstant.B_DEPT}, allEntries = true)
    @Klock
    public R update(@Validated @RequestBody Dict resources) {
        if (dictService.lambdaQuery().eq(Dict::getName, resources.getName()).ne(Dict::getId, resources.getId()).count() > 0) {
            throw new BadRequestException("字典名称已存在");
        }
        boolean flag = dictService.saveOrUpdate(resources);
        dataBind.resetMap();
        return R.ok(flag);
    }

    @Log("删除字典")
    @ApiOperation("删除字典，权限码：admin,dict:delete")
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("@el.check('admin','dict:delete')")
    @CacheEvict(cacheNames = {CacheConstant.DICT,CacheConstant.DICT_DETAIL,CacheConstant.B_DEPT}, allEntries = true)
    @Klock
    public R delete(@PathVariable Long id) {
        boolean flag = dictService.removeById(id);
        dataBind.resetMap();
        return R.ok(flag);
    }


    @Log("字典树")
    @ApiOperation("字典树")
    @GetMapping("/tree")
    public R tree() {
        return R.ok(dictService.tree());
    }
}
