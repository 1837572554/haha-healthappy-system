package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BCommentZQueryCriteria {

    @Query(type = Query.Type.INNER_LIKE)
    @ApiModelProperty("建议")
    private String suggest;
}
