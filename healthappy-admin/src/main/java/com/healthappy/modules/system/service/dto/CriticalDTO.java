package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 危急值状况管理 返回类
 * @author Jevany
 * @date 2022/2/10 18:16
 */
@Data
public class CriticalDTO implements Serializable {
    
    /** 危急值自动编号 */
    @ApiModelProperty("危急值自动编号")
    private Long id;
    
    /** 流水号 */
    @ApiModelProperty("流水号")
    private String paId;
    
    /** 姓名 */
    @ApiModelProperty("姓名")
    private String name;
    
    /** 性别 */
    @ApiModelProperty("性别")
    private String sex;

    /** 年龄 */
    @ApiModelProperty("年龄")
    private Integer age;
    
    /** 身份证 */
    @ApiModelProperty("身份证")
    private String idNo;
    
    /** 电话 */
    @ApiModelProperty("电话")
    private String phone;

    /** 单位编号 */
    @ApiModelProperty("单位编号")
    private String companyId;
    
    /** 单位 */
    @ApiModelProperty("单位")
    private String companyName;

    /** 部门编号 */
    @ApiModelProperty("部门编号")
    private String deptId;

    /** 部门 */
    @ApiModelProperty("部门")
    private String deptName;

    /** 发生科室编号 */
    @ApiModelProperty("发生科室编号")
    private String occuDeptId;

    /** 发生科室 */
    @ApiModelProperty("发生科室")
    private String occuDept;

    /** 组合项目编号 */
    @ApiModelProperty("组合项目编号")
    private String groupId;

    /** 组合项目名称 */
    @ApiModelProperty("组合项目名称")
    private String groupName;

    /** 小项名称 */
    @ApiModelProperty("小项名称")
    private String itemName;

    /** 异常类型 */
    @ApiModelProperty("异常类型")
    private String affectLevel;

    /** 具体情况 */
    @ApiModelProperty("具体情况")
    private String detail;

    /** 发生时间 */
    @ApiModelProperty("发生时间")
    private String occuTime;

    /** 处理时间 */
    @ApiModelProperty("处理时间")
    private String dealTime;

    /** 处理医生 */
    @ApiModelProperty("处理医生")
    private String dealDoctor;

    /** 通知方式 */
    @ApiModelProperty("通知方式")
    private String noticeWay;

    /** 转诊科室 */
    @ApiModelProperty("转诊科室")
    private String transferDept;
}