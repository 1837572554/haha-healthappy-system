package com.healthappy.modules.system.domain.vo;

import com.healthappy.modules.system.domain.TItemSickness;
import com.healthappy.modules.system.service.dto.ProejctCheckBItemDtDto;
import com.healthappy.modules.system.service.dto.ProjectCheckBItemHdDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import java.util.List;

@Data
public class ProjectCheckVo {

    /**
     * 组合项目
     */
    @ApiModelProperty("组合项目")
    private ProjectCheckBItemHdDto checkBItemHdDto;

    /**
     * 子项及结果
     */
    @ApiModelProperty("子项及结果")
    private List<ProejctCheckBItemDtDto> checkBItemDtDtoList;

    /**
     * 项目与病种关联列表
     */
    @ApiModelProperty("关联")
    @Valid
    private List<TItemSickness> itemSicknessList;
}
