package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BWuJia;
import com.healthappy.modules.system.service.dto.BWuJiaQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 物价表服务层
 * @author sjc
 * @date 2021-08-3
 */
public interface BWuJiaService extends BaseService<BWuJia> {
    Map<String, Object> queryAll(BWuJiaQueryCriteria criteria, Pageable pageable);

    List<BWuJia> queryAll(BWuJiaQueryCriteria criteria);

    boolean deleteById(Long id);
}
