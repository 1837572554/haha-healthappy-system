package com.healthappy.modules.system.service.dto;


import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;

/**
 * @description 预约表
 * @author sjc
 * @date 2021-06-28
 */
@Data
public class TAppointQueryCriteria {

    @ApiModelProperty("单位ID")
    @Query(type = Query.Type.EQUAL)
    private String companyId;

    @ApiModelProperty("导入日期")
    @Query(type = Query.Type.BETWEEN)
    private List<Timestamp> appointDate;

    @ApiModelProperty("体检日期")
    @Query(type = Query.Type.BETWEEN)
    private List<Timestamp> signDate;

    @ApiModelProperty("部门ID")
    @Query(type = Query.Type.EQUAL)
    private String departmentId;

    @ApiModelProperty("身份证号")
    @Query(type = Query.Type.INNER_LIKE)
    private String idNo;

    @Query(type = Query.Type.INNER_LIKE)
    @ApiModelProperty("性别")
    private String name;

    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("性别(查询BSex)")
    private Long sex;

    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("体检分类(查询BPeTypeDT)")
    private Long type;

    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("体检类别(B_Pe_Type_DT)")
    private Long peType;

    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("分组ID")
    private String deptGroupId;

    @Query(type = Query.Type.BETWEEN)
    @ApiModelProperty("预约id范围，给2个参数，第一个参数是下限，第二个参数是上限")
    private List<String> id;

    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("预约类型")
    private String appointType;

    @ApiModelProperty("是否签到")
    @Query(type = Query.Type.NOT_NULL_OR_IS_NULL)
    private Boolean signDoctor;


    @Query(type = Query.Type.IN)
    @ApiModelProperty("毒害种类")
    private List<Long> poisonType;
}
