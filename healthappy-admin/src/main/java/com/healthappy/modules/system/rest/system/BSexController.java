package com.healthappy.modules.system.rest.system;

import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BSex;
import com.healthappy.modules.system.service.BSexService;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 性别 控制器
 * @author fang
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "系统设置：性别")
@RestController
@AllArgsConstructor
@RequestMapping("/BSex")
public class BSexController {

    private final BSexService sexService;

    @Log("查询性别")
    @ApiOperation("查询性别")
    @GetMapping
    public R list() {
        return R.ok(sexService.list());
    }

    @Log("新增|修改性别")
    @ApiOperation("新增|修改性别，权限码：admin")
    @PostMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R saveOrUpdate(@RequestBody BSex bSex) {
        return R.ok(sexService.saveOrUpdate(bSex));
    }


    @Log("删除性别")
    @ApiOperation("删除性别，权限码：admin")
    @DeleteMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R remove(@RequestBody Set<Integer> ids) {
        return R.ok(sexService.removeByIds(ids));
    }
}
