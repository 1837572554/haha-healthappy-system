package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.config.SeedIdGenerator;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.sql.Timestamp;

/**
 * @desc: 图片表
 * @author: YJ
 * @date: 2021-11-30 16:24
 **/
@Data
@ApiModel("图片表")
@SeedIdGenerator
@TableName("T_IMAGE")
@RenewLog
public class TImage {

    //implements Serializable

    /** 主键id-长度20 */
    @ApiModelProperty("主键id-长度20")
    @TableId(type = IdType.INPUT)
    private String id;

    /** 文件名称（上传时的文件名） */
    @ApiModelProperty("文件名称（上传时的文件名）")
    private String fileName;

    /** 服务器中文件路径 */
    @ApiModelProperty("服务器中文件路径")
    private String filePath;

    /** 图片Url */
    @ApiModelProperty("图片Url")
    private String url;

    /** 添加时间 */
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty("添加时间")
    private Timestamp createTime;

    /** 医疗机构ID(U_Hospital_Info的ID) */
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    @ApiModelProperty(value = "医疗机构ID",hidden = true)
    private String tenantId;

    /** 用户名 */
    @ApiModelProperty("用户名")
    private String userName;

    /** 文件大小 */
    @ApiModelProperty("文件大小")
    private String size;

    /** 文件的MD5值 */
    @ApiModelProperty("文件的MD5值")
    private String md5Code;
}
