package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BZyQuestion;
import com.healthappy.modules.system.domain.TPatient;
import com.healthappy.modules.system.domain.vo.TPaitentZyQuestionVo;

/**
 * @author Jevany
 * @date 2021/12/8 14:56
 * @desc 中医体质问卷问题 服务
 */
public interface BZyQuestionService extends BaseService<BZyQuestion> {

    /**
     * 获得体检者问卷数据
     * @author YJ
     * @date 2021/12/8 17:42
     * @param tPatient
     * @param groupId   组合项编号
     * @return com.healthappy.modules.system.domain.vo.TPaitentZyQuestionVo
     */
    TPaitentZyQuestionVo getQuestions(TPatient tPatient,String groupId);

    /**
     * 保存体检者问卷数据
     * @author YJ
     * @date 2021/12/8 18:52
     * @param vo TPaitentZyQuestionVo对象
     */
    TPaitentZyQuestionVo saveQuestions(TPaitentZyQuestionVo vo);
}
