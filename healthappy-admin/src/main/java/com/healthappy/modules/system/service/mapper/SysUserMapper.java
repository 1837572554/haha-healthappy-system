package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author hupeng
 * @date 2020-05-14
 */
@Repository
@Mapper
public interface SysUserMapper extends CoreMapper<User> {

    /**
     * 修改密码
     * @param username 用户名
     * @param password 密码
     * @param lastPasswordResetTime /
     */
    @Update("update `user` set password = #{password} , last_password_reset_time = #{lastPasswordResetTime} where username = #{username}")
    void updatePass(@Param("password") String password, @Param("lastPasswordResetTime") String lastPasswordResetTime, @Param("username") String username);

    /**
     * 修改邮箱
     * @param username 用户名
     * @param email 邮箱
     */
    @Update("update `user` set email = #{email} where username = #{username}")
    void updateEmail(@Param("email") String email, @Param("username") String username);

    /**
     * 根据用户名查询用户信息
     * @param userName 用户名
     */
    @SqlParser(filter = true)
    @Select("SELECT u.id,u.nick_name,u.sex,u.is_enable,u.create_time,u.phone,u.email ,u.`password` ,u.username,u.tenant_id,ua.real_name avatar FROM `user` " +
            " u LEFT JOIN user_avatar ua ON u.avatar_id = ua.id  WHERE u.username = #{username}")
    User findByName(String userName);

    /**
     * 根据用户ID查询用户信息
     * @param userId 用户ID
     */
    @Select("SELECT u.id,u.nick_name,u.sex,u.is_enable,u.create_time,u.phone,u.email ,u.`password` ,u.username,u.tenant_id,ua.real_name avatar FROM `user` " +
            " u LEFT JOIN user_avatar ua ON u.avatar_id = ua.id  WHERE u.id = #{userId}")
    User findById(@Param("userId") Long userId);

    /**
     * 获取指定用户编号是否有对应的权限
     * @param userId 用户编号
     * @param permissionName 权限名称
     * @return 有权限则大于0，反之则为0
     */
    @Select("select count(1) from users_roles ur where user_id =#{userId} and role_id in " +
            "(select role_id from roles_menus rm where menu_id in (select id from menu m where permission =#{permissionName}))")
    public Integer checkPermission(@Param("userId") Long userId, @Param("permissionName") String permissionName);

    /**
     * 获得权限对应的用户
     * @author YJ
     * @date 2022/2/14 17:38
     * @param permissionName
     * @return java.util.List〈com.healthappy.modules.system.domain.User〉
     */
    @Select("SELECT u.id ,u.username ,u.nick_name from `user` u where u.id " +
            "in (select ur.user_id from users_roles ur where role_id  in (select role_id from roles_menus rm where menu_id in (select id from menu m where permission ='Critical:notification')))")
    List<User> getPermissionUsers(@Param("permissionName") String permissionName);

    /**
     * 根据标识权限获得对应的userId,userName,tenantId数据
     * @author YJ
     * @date 2022/3/17 13:59
     * @param tagPermission
     * @return java.util.List〈com.healthappy.modules.system.domain.User〉
     */
    @SqlParser(filter = true)
    @Select("select u.id,u.username,u.tenant_id from user_tag ut \n" +
            "\tINNER JOIN `user` u on ut.user_id=u.id\n" +
            "WHERE EXISTS (select 1 from tag t where locate(',${tagPermission},',CONCAT(',',t.permission,','))>0 and t.id =ut.tag_id)")
    List<User> getUsersByTagPermission(@Param("tagPermission") String tagPermission);


    @SqlParser(filter = true)
    @Select("select * from `user` ${ew.customSqlSegment}")
    User getNotTenant(@Param(Constants.WRAPPER) Wrapper<User> wrapper);
}
