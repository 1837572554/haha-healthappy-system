package com.healthappy.modules.system.service.dto;

import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
public class AloneReportPrintCriteria {
    /**
     * 体检时间类型
     */
    @NotNull(message = "体检时间类型不能为空")
    @ApiModelProperty(value = "体检时间类型 ,页面路径：imgTxtScanManage", required = true, position = 0)
    private Integer dateType;

    /** 时间段（数组） */
    @ApiModelProperty(value = "时间段（数组） ", required = true, position = 1)
    @DateTimeFormat("yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private List<Date> timeList;

    /**
     * 体检号
     */
    @ApiModelProperty("体检号")
    private String paId;

    /**
     * 体检者姓名
     */
    @ApiModelProperty("体检者姓名")
    private String paName;

    /**
     * 单位ID
     */
    @ApiModelProperty("单位ID")
    private String companyId;

    /**
     * 部门ID
     */
    @ApiModelProperty("部门ID")
    private String departmentId;

    /** 科室id */
    @ApiModelProperty("科室id")
    private String deptId;

    /** 项目id */
    @ApiModelProperty("项目id")
    private String groupId;


//    /**
//     * 页面显示条数
//     */
//    @ApiModelProperty(value = "页面显示条数,默认为10", position = 101)
//    private Integer pageSize = 10;
//
//    /**
//     * 页数
//     */
//    @ApiModelProperty(value = "页数,默认为1", position = 102)
//    private Integer pageNum = 1;
//
//
//    /**
//     * 开始条数
//     */
//    @ApiModelProperty(value = "开始条数", hidden = true)
//    private Integer startIndex = -1;

}
