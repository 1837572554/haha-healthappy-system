package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.*;
import com.healthappy.modules.system.domain.vo.BBarcodeVo;
import com.healthappy.modules.system.service.*;
import com.healthappy.modules.system.service.dto.BBarcodeQueryCriteria;
import com.healthappy.modules.system.service.mapper.BBarcodeMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Slf4j
@Service
@AllArgsConstructor
public class BBarcodeServiceImpl extends BaseServiceImpl<BBarcodeMapper, BBarcode> implements BBarcodeService {

    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BBarcodeQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BBarcode> page = new PageInfo<BBarcode>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", generator.convert(page.getList(), BBarcode.class));
        map.put("totalElements", page.getTotal());
        return map;
    }

    @Override
    public List<BBarcode> queryAll(BBarcodeQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BBarcode.class, criteria));
    }

    @Override
    public boolean saveOrUpdate(BBarcodeVo resources) {
        BBarcode barcode = new BBarcode();
        BeanUtil.copyProperties(resources, barcode);

        if (StrUtil.isNotEmpty(barcode.getSeed())) {
            //如果种子长度+条码头长度大于条码总长度时
            if ((barcode.getSeed().length() + barcode.getBarcodeHead().length()) > barcode.getBarcodeLength()) {
                throw new BadRequestException("设定的种子长度或者条码头长度过长!");
            }
            //如果种子是非数字
            String regEx = "^-?[0-9]+$";
            Pattern pat = Pattern.compile(regEx);
            Matcher mat = pat.matcher(barcode.getSeed());
            if (!mat.find()) {
                throw new BadRequestException("条码种子的值必须为数字!");
            }
        } else {
            barcode.setSeed("1");
        }
        //一个组合项目类型只能存在一个
        List<BBarcode> list = this.lambdaQuery().eq(BBarcode::getGroupHdType, barcode.getGroupHdType()).list();
        if (CollUtil.isNotEmpty(list)) {
            throw new BadRequestException("该组合项目类型已经存在!");
        }

        return this.saveOrUpdate(barcode);
    }


    /**
     * 根据组合项目类型，获取条码号
     *
     * @param groupHdType
     * @return
     */
    @Override
    public String generateBarcode(String groupHdType) {
        BBarcode barcode = baseMapper.selectOne(new LambdaQueryWrapper<BBarcode>().eq(BBarcode::getGroupHdType, groupHdType));
        //如果没有。就采用体检号的形式
        if (barcode == null) {
            return null;
        }
        //条码总长度
        Integer length = barcode.getBarcodeLength();
        //条码头
        String head = barcode.getBarcodeHead();
        /** 条码总-条码头=填充长度 */
        Integer fillLength = length - head.length();


        String seed = "";
        //如果种子值没有，就给一个1
        if (StrUtil.isEmpty(barcode.getSeed())) {
            seed = "1";
        } else {
            seed = Long.parseLong(barcode.getSeed()) + 1 + "";
        }
        if (seed.length() < fillLength) {
            seed = StrUtil.fillBefore(seed, '0', fillLength);
        } else if (seed.length() > fillLength) {
            throw new BadRequestException("超设置-条码长度，请调整Barcode表设置");
        }

        //种子值+1并更新到数据库
        barcode.setSeed(seed);
        baseMapper.updateById(barcode);

        return head + barcode.getSeed();
    }


}
