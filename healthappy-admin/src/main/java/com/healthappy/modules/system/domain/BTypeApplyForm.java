package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 申请单类型
 * @author sjc
 * @date 2021-08-3
 */
@Data
@ApiModel("申请单类型")
@TableName("B_Type_Apply_Form")
public class BTypeApplyForm implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    @TableId(type = IdType.AUTO)
    private Long id;


    /**
     * 名称
     */
    @ApiModelProperty("名称")
    private String name;
    /**
     * 排序
     */
    @ApiModelProperty("排序")
    private Integer dispOrder;
}
