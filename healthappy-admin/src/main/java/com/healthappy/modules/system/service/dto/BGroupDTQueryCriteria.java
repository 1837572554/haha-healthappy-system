package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @description 组合项目子项
 * @author sjc
 * @date 2021-08-4
 */
@Data
public class BGroupDTQueryCriteria {


    /**
     * 组合项目id
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("组合项目id")
    private String groupId;

    /**
     * 项目编号
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("项目编号")
    private String itemId;
}
