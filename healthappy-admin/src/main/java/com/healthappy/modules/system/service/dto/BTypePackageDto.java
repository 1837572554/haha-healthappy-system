package com.healthappy.modules.system.service.dto;

import com.healthappy.modules.system.domain.BTypePackage;
import lombok.Data;


/**
 * @description 套餐类型
 * @author sjc
 * @date 2021-08-24
 */
@Data
public class BTypePackageDto extends BTypePackage {


}
