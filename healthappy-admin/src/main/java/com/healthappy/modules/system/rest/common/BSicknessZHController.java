package com.healthappy.modules.system.rest.common;

import cn.hutool.core.collection.CollUtil;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BSicknessZH;
import com.healthappy.modules.system.domain.vo.BSicknessZHVo;
import com.healthappy.modules.system.service.BSicknessZHService;
import com.healthappy.modules.system.service.dto.BSicknessZHQueryCriteria;
import com.healthappy.modules.system.service.dto.DeptItemSicknessTreeDTO;
import com.healthappy.utils.R;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * @author sjc
 * @description 组合判断子项 控制器
 * @date 2021-06-28
 */
@Validated
@Slf4j
@Api(tags = "默认基础设置：组合判断子项")
@RestController
@AllArgsConstructor
@RequestMapping("/BSicknessZH")
public class BSicknessZHController {
    private final BSicknessZHService bSicknessZHService;
    private final IGenerator generator;

    @Log("分页查询病种组合子项数据")
    @ApiOperation("分页查询病种组合子项数据，权限码：BSicknessZH:list")
    @GetMapping
    public R listPage(BSicknessZHQueryCriteria criteria, Pageable pageable) {
        return R.ok(bSicknessZHService.queryAll(criteria, pageable));
    }

    @Log("新增|修改病种组合子项表")
    @ApiOperation("新增|修改病种组合子项表，权限码：admin")
    @PostMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R saveOrUpdate(@Valid @RequestBody List<BSicknessZHVo> resources) {
        if (CollUtil.isEmpty(resources)) {
            return R.error(500, "参数不能为空");
        }
        String sicknessZhId = resources.get(0).getSicknessZhId();
        removeSicknessZhId(sicknessZhId);
        bSicknessZHService.saveBatch(generator.convert(resources, BSicknessZH.class));
        return R.ok();
    }

    @Log("根据病种id来删除病种组合子项")
    @ApiOperation("根据病种id来删除病种组合子项，权限码：admin")
    @DeleteMapping("/delSicknessId")
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R removeSicknessId(@RequestParam("sicknessId") String sicknessId) {
        if (bSicknessZHService.deleteBySicknessId(sicknessId))
            return R.ok();
        return R.error(500, "方法异常");
    }

    @Log("根据组合病种id来删除对应的所有病种组合子项")
    @ApiOperation("根据组合病种id来删除对应的所有病种组合子项，权限码：admin")
    @DeleteMapping("/delSicknessZHId")
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R removeSicknessZhId(@RequestParam("sicknessZhId") String sicknessZhId) {
        if (bSicknessZHService.deleteBySicknessZhId(sicknessZhId))
            return R.ok();
        return R.error(500, "方法异常");
    }

    @Log("获得组合判断-部门、明细项、病种树")
    @ApiOperation("获得组合判断-部门、明细项、病种树，权限码：ZhToSickness:list")
    @GetMapping("/deptItemSicknessTree")
    @PreAuthorize("@el.check('ZhToSickness:list')")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "科室、明细项、病种数据DTO",response = DeptItemSicknessTreeDTO.class,responseContainer = "List")
    })
    public R deptItemSicknessTree(@ApiParam(name = "simpleSpelling",value = "简拼") String simpleSpelling) {
        return R.ok(bSicknessZHService.deptItemSicknessTree(simpleSpelling));
    }
}
