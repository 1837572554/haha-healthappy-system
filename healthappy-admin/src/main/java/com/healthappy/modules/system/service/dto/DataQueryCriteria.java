package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class DataQueryCriteria {
    /**
     * 体检号
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("体检号")
    private String id;

    /**
     * 身份证
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("身份证")
    private String idNo;


    /**
     * 姓名
     */
    @Query(type = Query.Type.INNER_LIKE)
    @ApiModelProperty("姓名")
    private String name;

    /**
     * 体检分类表id
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("体检分类表id")
    private Long peTypeHdId;

    /**
     * 体检类别表id
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("体检类别表id")
    private Long peTypeDtId;


    /**
     * 单位id
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("单位id")
    private String companyId;

    /**
     * 部门id
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("部门id")
    private String departmentId;

    /**
     * 分组id
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("分组id")
    private String departmentGroupId;

    /**
     * 科室编号
     */
    @ApiModelProperty("科室编号")
    private String deptId;

    /**
     * 组合编号
     */
    @ApiModelProperty("组合编号")
    private String groupId;

    @ApiModelProperty("是否职业禁忌证 0否1是")
    private String pjTaboo;

    @ApiModelProperty("是否疑似职业病 0否1是")
    private String pjSuspicion;

    @ApiModelProperty("是否其他复查 0否1是")
    private String pjReview;

    @ApiModelProperty("未见异常 0否1是")
    private String pjRemove;

    @ApiModelProperty("是否其他疾病或异常 0否1是")
    private String pjUnusual;

    @ApiModelProperty("是否复查 0否1是")
    private String pjRelate;



    @ApiModelProperty("日期类型：1：登记，2：签到，3：总检,4:总审")
    private Integer dateType;

    @ApiModelProperty("开始日期")
    private String startTime;

    @ApiModelProperty("结束日期")
    private String endTime;

    @ApiModelProperty("年龄段-起始年龄")
    private Integer startAge;

    @ApiModelProperty("年龄段-结尾年龄")
    private Integer endAge;
}
