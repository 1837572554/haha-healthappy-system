package com.healthappy.modules.system.rest.manage;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.google.common.base.Joiner;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.vo.TItemHDVo;
import com.healthappy.modules.system.domain.vo.TPatientItemVO;
import com.healthappy.modules.system.domain.vo.TPatientVo;
import com.healthappy.modules.system.service.IntegratedManageService;
import com.healthappy.modules.system.service.TPatientService;
import com.healthappy.modules.system.service.dto.IntegratedManageDTO;
import com.healthappy.modules.system.service.dto.IntegratedManageQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author Jevany
 * @date 2022/2/8 10:07
 * @desc 综合管理控制器
 */
@Slf4j
@Api(tags = "体检管理：综合管理")
@RestController
@AllArgsConstructor
@RequestMapping("/IntegratedManage")
public class IntegratedManageController {

    private final IntegratedManageService integratedManageService;
    private final TPatientService tPatientService;

    @Log("综合管理数据-查询")
    @ApiOperation("综合管理数据-查询，权限码：IntegratedManage:list")
    @GetMapping
    @PreAuthorize("@el.check('IntegratedManage:list')")
    public R list(IntegratedManageQueryCriteria criteria) {
//        if (null != criteria.getDateType()) {
//            if (StrUtil.isAllBlank(criteria.getStartTime(), criteria.getEndTime())) {
//                return R.error("请选择时间");
//            }
//        }
        if (ObjectUtil.isEmpty(criteria.getPageNum()) || criteria.getPageNum() <= 0) {
            criteria.setPageNum(1);
        }
        if (ObjectUtil.isEmpty(criteria.getPageSize()) || criteria.getPageSize() <= 0) {
            criteria.setPageSize(10);
        }
        criteria.setIsExport(false);
        return R.ok(integratedManageService.getIntegratedManageList(criteria));
    }

    @Log("综合管理数据-导出")
    @ApiOperation("综合管理数据-导出，权限码：IntegratedManage:list")
    @GetMapping("/export")
    @PreAuthorize("@el.check('IntegratedManage:list')")
    @Klock
    public void export(HttpServletResponse response, IntegratedManageQueryCriteria criteria) throws IOException {
        criteria.setIsExport(true);
        Map<String, Object> map = integratedManageService.getIntegratedManageList(criteria);
        integratedManageService.export((List<IntegratedManageDTO>)map.get("content"),response);
    }

    @Log("综合管理数据-信息者修改")
    @ApiOperation("综合管理数据-信息者修改，权限码：IntegratedManage:list")
    @PostMapping("/editPatientInfo")
    @PreAuthorize("@el.check('IntegratedManage:list')")
    @Klock
    public R editPatientInfo(@RequestBody TPatientVo patientVo) {
        return R.ok(tPatientService.editPatientInfo(patientVo));
    }

    @Log("综合管理数据-项目修改")
    @ApiOperation("综合管理数据-项目修改，权限码：IntegratedManage:list")
    @PostMapping("/editPatientItem")
    @PreAuthorize("@el.check('IntegratedManage:list')")
    @Klock
    public R editPatientItem(@RequestBody TPatientItemVO itemVO) {
        if(CollUtil.isNotEmpty(itemVO.getItems())) {
            for (TItemHDVo item : itemVO.getItems()) {
                if(StrUtil.isBlank(item.getPayType())) {
                    return R.error(item.getGroupName()+"项目未设置支付类型");
                }
            }
        }
        return R.ok(tPatientService.editPatientItem(itemVO));
    }

	@Log("综合管理数据-删除信息者")
	@ApiOperation("综合管理数据-删除信息者，权限码：IntegratedManage:del")
	@DeleteMapping("/delList")
//	@PreAuthorize("@el.check('IntegratedManage:del')")
	@Klock
	public R del(@RequestBody List<String> ids) {
		String idsSting = Joiner.on(",").join(ids);
		//防止sql注入的过滤
		boolean haveSqlKey = checktHaveSqlKey(idsSting);
		if(haveSqlKey){
			return R.error("参数包含关键词");
		}
		boolean b = tPatientService.delPatientByIds(idsSting);
		if(b){
			return R.ok();
		}
		return R.error();
	}

	/**
	 * 检查参数是否有sql关键词
	 * @param param
	 * @return 有包含关键字则返回true 反之false
	 */
	public  boolean checktHaveSqlKey(String param){
		int select = param.indexOf("select");
		int create = param.indexOf("create");
		int insert = param.indexOf("insert");
		int delete = param.indexOf("delete");
		int update = param.indexOf("update");
		int drop = param.indexOf("drop");
		int alter = param.indexOf("alter");
		int truncate = param.indexOf("truncate");
		int i = select + create + insert + delete + update + drop + alter + truncate;
        //有查找到关键字，数值则大于-8
		if(i>-8) {
            return true;
        }
        return false;
	}
}
