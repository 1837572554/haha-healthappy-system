package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.healthappy.config.databind.FieldBind;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @desc: 电测听数据
 * @author: YJ
 * @date: 2021-12-02 15:26
 **/
@Data
@TableName("T_DCT_DATA")
@RenewLog
public class TDctData implements Serializable {

    /** 体检编号 */
    @ApiModelProperty("体检编号")
    private String paId;

    /** 电测听项类型ID */
    @ApiModelProperty("电测听项类型ID")
    @FieldBind(target = "dctTypeName")
    private Integer dctType;

    /** 电测听项类型名称 */
    @ApiModelProperty("电测听项类型名称")
    @TableField(exist = false)
    private String dctTypeName;

    /** 125频率数值 */
    @ApiModelProperty("125频率数值")
    private String p125;

    /** 250频率数值 */
    @ApiModelProperty("250频率数值")
    private String p250;

    /** 500频率数值 */
    @ApiModelProperty("500频率数值")
    private String p500;

    /** 750频率数值 */
    @ApiModelProperty("750频率数值")
    private String p750;

    /** 1000频率数值 */
    @ApiModelProperty("1000频率数值")
    private String p1000;

    /** 1500频率数值 */
    @ApiModelProperty("1500频率数值")
    private String p1500;

    /** 2000频率数值 */
    @ApiModelProperty("2000频率数值")
    private String p2000;

    /** 3000频率数值 */
    @ApiModelProperty("3000频率数值")
    private String p3000;

    /** 4000频率数值 */
    @ApiModelProperty("4000频率数值")
    private String p4000;

    /** 6000频率数值 */
    @ApiModelProperty("6000频率数值")
    private String p6000;

    /** 8000频率数值 */
    @ApiModelProperty("8000频率数值")
    private String p8000;

    /** 12000频率数值 */
    @ApiModelProperty("12000频率数值")
    private String p12000;

    /** 16000频率数值 */
    @ApiModelProperty("16000频率数值")
    private String p16000;

    /** 医疗机构ID(U_Hospital_Info的ID) */
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    @ApiModelProperty(value = "医疗机构ID",hidden = true)
    private String tenantId;
}
