package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BDept;
import com.healthappy.modules.system.service.dto.BDeptQueryCriteria;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 部门
 * @author fang
 * @date 2021-06-17
 */
@Mapper
@Repository
public interface BDeptMapper extends CoreMapper<BDept> {
    /**
     * 获取启用科室且下级存在细项
     * @author YJ
     * @date 2021/12/20 15:50
     * @param tenantId 租户编号
     * @return java.util.List〈com.healthappy.modules.system.domain.BDept〉
     */
    @SqlParser(filter = true)
    public List<BDept> getDeptEnableAndExistsItem(@Param("tenantId") String tenantId);

    /**
     * 获取启用科室且下级存在组合项
     * @author YJ
     * @date 2021/12/20 15:50
     * @param tenantId 租户编号
     * @return java.util.List〈com.healthappy.modules.system.domain.BDept〉
     */
    @SqlParser(filter = true)
    public List<BDept> getDeptEnableAndExistsGroup(@Param("tenantId") String tenantId);

    /**
     * 获取启用科室且下级存在组合项和细项
     * @author YJ
     * @date 2021/12/20 15:50
     * @param tenantId 租户编号
     * @return java.util.List〈com.healthappy.modules.system.domain.BDept〉
     */
    @SqlParser(filter = true)
    public List<BDept> getDeptEnableAndExistsGroupItem(@Param("tenantId") String tenantId);

    /**
     * 获取当前
     * @author YJ
     * @date 2021/12/22 18:14
     * @param criteria 查询参数
     * @param delFlag 是否显示删除项目 空为全查（不限制），0为不删除数据，1为删除的数据
     * @return java.util.List〈com.healthappy.modules.system.domain.BDept〉
     */
    List<BDept> getDepts(@Param("criteria") BDeptQueryCriteria criteria);
}
