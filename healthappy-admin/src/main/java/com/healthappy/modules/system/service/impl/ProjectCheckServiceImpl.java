package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.config.databind.DataBind;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.domain.*;
import com.healthappy.modules.system.domain.vo.GroupResultVo;
import com.healthappy.modules.system.domain.vo.ItemResultVo;
import com.healthappy.modules.system.domain.vo.ProjectCheckVo;
import com.healthappy.modules.system.service.*;
import com.healthappy.modules.system.service.dto.*;
import com.healthappy.modules.system.service.wrapper.ProjectCheckWrapper;
import com.healthappy.utils.SecurityUtils;
import com.healthappy.utils.SpringUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Slf4j
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class ProjectCheckServiceImpl implements ProjectCheckService {

	private final TPatientService patientService;

	private final TItemDTService itemDTService;

	private final BItemService itemService;

	private final TItemHDService itemHDService;

	private final IGenerator generator;

	private final BSicknessRuleService sicknessRuleService;

	private final TItemSicknessService itemSicknessService;

	private final TPhotoService photoService;

	private final TCriticalService tCriticalService;

	private final BGroupHdService bGroupHdService;


	@Override
	public Map<String, Object> queryAll(ProjectCheckQueryCriteria criteria) {
		//清除一下
		criteria.setPeDateType(null);
		criteria.setWorkstation(null);

		//      暂时注释
		//      List listVO = ProjectCheckWrapper.build().listVO(patientService.list(QueryHelpPlus.getPredicate(TPatient.class, criteria)));
		List<ProjectCheckDto> list = (List<ProjectCheckDto>) patientService.list(
				QueryHelpPlus.getPredicate(TPatient.class, criteria)).stream().map(tp -> {
			ProjectCheckDto projectCheckDto = BeanUtil.copyProperties(tp, ProjectCheckDto.class);
			if (StrUtil.isNotBlank(projectCheckDto.getPhoto())) {
				projectCheckDto.setPhoto(
						Optional.ofNullable(photoService.lambdaQuery().eq(TPhoto::getId, projectCheckDto.getId()).one())
								.map(TPhoto::getPortrait).orElse(""));
			}
			return projectCheckDto;
		}).collect(Collectors.toList());
		Map<String, Object> map = new LinkedHashMap<>(2);
		map.put("content", list);
		map.put("totalElements", list.size());
		return map;
	}

	@Override
	public ProjectCheckDto queryOne(String paId) {
		return ProjectCheckWrapper.build().entityVO(patientService.lambdaQuery().eq(TPatient::getId, paId).one());
	}


	/**
	 * 检查项目和结果查询
	 *
	 * @param criteria
	 * @return
	 */
	@Override
	public List<ProejctCheckBItemDtDto> checkupItemsAndResultsQuery(DiagnosisRelatedDtQueryCriteria criteria) {
		List<TItemHD> list = itemHDService.list(QueryHelpPlus.getPredicate(TItemHD.class, criteria));
		if (CollUtil.isEmpty(list)) {
			return Collections.emptyList();
		}

		List<ProejctCheckBItemDtDto> collect = list.stream()
				.map(dt -> itemDTService.lambdaQuery().eq(TItemDT::getGroupId, dt.getGroupId())
						.eq(TItemDT::getPaId, dt.getPaId()).list().stream().map(item -> {
							ProejctCheckBItemDtDto itemDtDto = BeanUtil.copyProperties(item,
									ProejctCheckBItemDtDto.class);
							BItem bItem = itemService.lambdaQuery().eq(BItem::getId, item.getItemId()).one();
							if (null != bItem) {
								itemDtDto.setIsSummary(Convert.toBool(bItem.getIsSummary()));
								itemDtDto.setResultType(bItem.getResultType());
								itemDtDto.setNormalResult(bItem.getResult());
								itemDtDto.setDispOrder(bItem.getDispOrder());
								itemDtDto.setCode(bItem.getCode());
								List<SicknessByItemDto> sicknessRuleList = sicknessRuleService.getByItemId(
										bItem.getId());
								itemDtDto.setSicknessRuleList(CollUtil.isEmpty(sicknessRuleList) ?
										Collections.emptyList() :
										sicknessRuleList);
								itemDtDto.setItemSicknessMatchDtoList(
										itemSicknessService.listResultSickness(item.getPaId(), item.getItemId()));
							}
							return itemDtDto;
						}).collect(Collectors.toList())).flatMap(Collection::stream).collect(Collectors.toList());
		//排序
		Collections.sort(collect);
		return collect;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updProjectCheck(ProjectCheckVo projectCheckVo) {
		if (Optional.ofNullable(projectCheckVo.getCheckBItemHdDto()).map(ProjectCheckBItemHdDto::getId).isPresent()) {
			//region 修改真实签到时间，第一个项目保存的时候进行修改
			TPatient tPatient = new TPatient();
			tPatient.setId(projectCheckVo.getCheckBItemHdDto().getPaId());
			tPatient.setSignDate(new Timestamp(System.currentTimeMillis()));
			tPatient.setTenantId(SecurityUtils.getTenantId());
			patientService.updSignDate(tPatient);
			//endregion 修改真实签到时间，第一个项目保存的时候进行修改
			itemHDService.saveOrUpdate(BeanUtil.copyProperties(projectCheckVo.getCheckBItemHdDto(), TItemHD.class));
			//保存生成的病种规则//,projectCheckVo.getItemSicknessList()
			itemSicknessService.installVo(projectCheckVo);

			//region 生成危急值
			tCriticalService.checkCritical(projectCheckVo);
			//endregion 生成危急值
		}
		if (CollUtil.isNotEmpty(projectCheckVo.getCheckBItemDtDtoList()) && projectCheckVo.getCheckBItemDtDtoList()
				.stream().allMatch(dt -> ObjectUtil.isNotNull(dt.getId()))) {
			itemDTService.saveOrUpdateBatch(generator.convert(projectCheckVo.getCheckBItemDtDtoList(), TItemDT.class));
		}
	}

	@Override
	public ItemSicknessDescDto generateResult(GroupResultVo groupResultVo) {

		TPatient patientInfo = patientService.getPatientInfo(groupResultVo.getPaId());

		BGroupHd bGroupHd = bGroupHdService.getOne(
				Wrappers.<BGroupHd>lambdaQuery().eq(BGroupHd::getId, groupResultVo.getGroupId()));

		DataBind dataBind = SpringUtil.getBean(DataBind.class);
		BidiMap groupHdTypeBidiMap = new DualHashBidiMap(dataBind.ENUM_MAP.get("groupHdType")).inverseBidiMap();


		//region 肺功能小结生成
		if (bGroupHd != null && bGroupHd.getType().equals(groupHdTypeBidiMap.get("肺功能项目").toString())) {
			return calcPFResult(groupResultVo.getItemResultVos());
		}
		//endregion 肺功能小结生成

		ItemSicknessDescDto itemSicknessDescDto = new ItemSicknessDescDto();
		itemSicknessDescDto.setMark("正常");
		List<ItemSicknessMatchDto> itemSicknessMatchDtoList = new ArrayList<>();
		List<ItemSicknessMatchDto> tmpDtos = new ArrayList<>();
		//小结内容
		String result = "";
		Integer idx = 0;
		//正常判定标识
		String[] normalMarkArray = {"", "正常", "阴性"};

		for (ItemResultVo itemResultVo : groupResultVo.getItemResultVos()) {
			if (StrUtil.isBlank(itemResultVo.getItemResult())) {
				continue;
			}
			//判断小项的结果类型
			BItem bItem = itemService.lambdaQuery().eq(BItem::getId, itemResultVo.getItemId()).one();
			//未找到项目则跳过
			if (bItem == null || StrUtil.isBlank(bItem.getId())) {
				continue;
			}
			String[] resStr = itemResultVo.getItemResult().split("、");
			for (String resultVal : resStr) {
				tmpDtos.clear();
				if ("0".equals(bItem.getResultType())) {
					//文本类型
					tmpDtos.addAll(sicknessRuleService.txtItemSicknessMath(itemResultVo.getItemId(), resultVal,
							itemResultVo.getItemMark(), patientInfo.getSex()));
				} else {
					//数值类型
					itemResultVo.setItemResult(resultVal.replace(">", "").replace("<", "").replace("=", ""));
					if (!isNumber(resultVal)) {
						continue;
					}
					tmpDtos.addAll(sicknessRuleService.numItemSicknessMath(itemResultVo.getItemId(), resultVal,
							itemResultVo.getItemMark(), patientInfo.getSex()));
				}
				//如果没有查到对应的病种，并且判定是异常时
				if (CollUtil.isEmpty(tmpDtos) && !Arrays.asList(normalMarkArray).contains(itemResultVo.getItemMark())
						&& itemResultVo.getItemMark() != null) {
					StringBuilder buf = new StringBuilder().append("(").append(++idx).append(")")
							.append(bItem.getName()).append(":").append(resultVal);
					if ("0".equals(bItem.getResultType()) && StrUtil.isNotBlank(bItem.getUnit())) {
						buf.append(" ").append(bItem.getUnit());
					}
					result += buf.append("\n").toString();
					itemSicknessDescDto.setMark("异常");
				} else {
					itemSicknessMatchDtoList.addAll(tmpDtos);
					for (ItemSicknessMatchDto tmpDto : tmpDtos) {
						StringBuilder buf = new StringBuilder().append("(").append(++idx).append(")")
								.append(tmpDto.getSicknessName());
						result += buf.append("\n").toString();
					}
					itemSicknessDescDto.setMark("异常");
				}
			}
		}

		if (StrUtil.isBlank(result)) {
			result = "未见异常" + new StringBuilder().append("\n").toString();
			itemSicknessDescDto.setMark("正常");
		}

		itemSicknessDescDto.setDesc(result);
		itemSicknessDescDto.setItemSicknessMatchDtoList(itemSicknessMatchDtoList);

		return itemSicknessDescDto;
	}

	@Override
	public List<ProjectCheckDto> getHistory(String paId) {
		//获取体检号对应的人员
		TPatient patient = patientService.lambdaQuery().eq(TPatient::getId, paId).one();
		if (!Optional.ofNullable(patient).isPresent()) {
			throw new BadRequestException("未检测对应体检者");
		}
		List<TPatient> list = patientService.lambdaQuery().eq(TPatient::getIdNo, patient.getIdNo())
				.ne(TPatient::getId, paId).isNotNull(TPatient::getSignDate).list();
		if (CollUtil.isEmpty(list)) {
			return new ArrayList<>();
		}
		return generator.convert(list, ProjectCheckDto.class);
	}

	public boolean isNumber(String str) {
		if (StrUtil.isEmpty(str)) {
			return false;
		}
		String reg = "^[0-9]+(.[0-9]+)?$";
		return str.matches(reg);
	}


	@Override
	public List<ItemSicknessMatchDto> listResultSickness(String paId, String itemId) {
		if (StrUtil.isBlank(paId) || StrUtil.isBlank(itemId)) {
			return Collections.emptyList();
		}
		return itemSicknessService.listResultSickness(paId, itemId);
	}

	/**
	 * 计算肺功能 小结
	 * 通过组合项目类型是 肺功能确认项目
	 * 通过项目编码匹配明细项 FVC、FEV1、FEV1/FVC、FVC%
	 *
	 * @param itemResultVos
	 * @return com.healthappy.modules.system.service.dto.ItemSicknessDescDto
	 * @author YJ
	 * @date 2022/3/24 20:20
	 */
	private ItemSicknessDescDto calcPFResult(List<ItemResultVo> itemResultVos) {
		ItemSicknessDescDto itemSicknessDescDto = new ItemSicknessDescDto();
		itemSicknessDescDto.setDesc("");
		itemSicknessDescDto.setMark("正常");
		itemSicknessDescDto.setItemSicknessMatchDtoList(new ArrayList<>());

		//获取项目对应的code
		for (ItemResultVo itemResultVo : itemResultVos) {
			if (StrUtil.isBlank(itemResultVo.getItemResult())) {
				itemResultVo.setItemResult("-99");
			}

			itemResultVo.setItemCode(StrUtil.blankToDefault(
					Optional.ofNullable(itemService.getById(itemResultVo.getItemId())).map(BItem::getCode).orElse(""),
					""));
		}

		Double fvc = -99D;
		Double fev1 = -99D;
		Double fev1Fvc = -99D;

		ItemResultVo fvcItem = itemResultVos.stream().filter(item -> item.getItemCode().equals("FVC")).findAny()
				.orElse(null);
		if (ObjectUtil.isNotNull(fvcItem)) {
			fvc = Double.parseDouble(fvcItem.getItemResult());
		}

		ItemResultVo fev1Item = itemResultVos.stream().filter(item -> item.getItemCode().equals("FEV1")).findAny()
				.orElse(null);
		if (ObjectUtil.isNotNull(fev1Item)) {
			fev1 = Double.parseDouble(fvcItem.getItemResult());
		}

		ItemResultVo fev1FvcItem = itemResultVos.stream().filter(item -> item.getItemCode().equals("FEV1/FVC"))
				.findAny().orElse(null);
		if (ObjectUtil.isNotNull(fev1FvcItem) && ObjectUtil.isNotNull(fev1Fvc)) {
			fev1Fvc = Double.parseDouble(fvcItem.getItemResult());
		}

		if ((fvc != -99D) && (fev1 != -99D) && (fev1Fvc != -99D)) {
			if (fev1Fvc >= 70f) {
				if (fvc >= 70f) {
					itemSicknessDescDto.setDesc("通气功能正常");
				} else if (fvc >= 50f && fvc < 70f) {
					itemSicknessDescDto.setDesc("轻度限制性通气功能障碍");
				} else if (fvc >= 30f && fvc < 50) {
					itemSicknessDescDto.setDesc("中度限制性通气功能障碍");
				} else {
					itemSicknessDescDto.setDesc("重度限制性通气功能障碍");
				}
			} else {
				if (fvc >= 70f) {
					if (fev1 >= 70f) {
						itemSicknessDescDto.setDesc("轻度阻塞型通气功能障碍");
					} else if (fev1 >= 50f && fev1 < 70f) {
						itemSicknessDescDto.setDesc("中度阻塞型通气功能障碍");
					} else if (fev1 >= 30f && fev1 < 50f) {
						itemSicknessDescDto.setDesc("重度阻塞型通气功能障碍");
					} else if (fev1 < 30f) {
						itemSicknessDescDto.setDesc("极重度阻塞型通气功能障碍");
					}
				} else if (fvc >= 50f && fvc < 70f) {
					if (fev1 >= 50f) {
						itemSicknessDescDto.setDesc("轻度混合型通气功能障碍");
					} else if (fev1 >= 30f && fev1 < 50f) {
						itemSicknessDescDto.setDesc("中度混合型通气功能障碍(以阻塞为主)");
					} else if (fev1 < 30f) {
						itemSicknessDescDto.setDesc("重度阻塞型通气功能障碍(以阻塞为主)");
					}
				} else if (fvc >= 30f && fvc < 50f) {
					if (fev1 >= 50f) {
						itemSicknessDescDto.setDesc("中度混合型通气功能障碍(以限制为主)");
					} else if (fev1 >= 30f && fev1 < 50f) {
						itemSicknessDescDto.setDesc("中度混合型通气功能障碍(以阻塞为主)");
					} else if (fev1 < 30f) {
						itemSicknessDescDto.setDesc("重度阻塞型通气功能障碍(以阻塞为主)");
					}
				} else {
					if (fev1 >= 30f) {
						itemSicknessDescDto.setDesc("重度混合型通气功能障碍(以限制为主)");
					} else if (fev1 < 30f) {
						itemSicknessDescDto.setDesc("重度阻塞型通气功能障碍");
					}
				}
			}
			if (!itemSicknessDescDto.getDesc().equals("通气功能正常")) {
				itemSicknessDescDto.setMark("异常");
			}
			return itemSicknessDescDto;
		}
		return itemSicknessDescDto;
	}
}
