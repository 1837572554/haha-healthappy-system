package com.healthappy.modules.system.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Jevany
 * @date 2022/5/7 2:47
 */
@ApiModel("预约号源DTO")
@Data
public class AppointDateDTO implements Serializable {

	/**
	 * 预约日期开始
	 */
	@ApiModelProperty(value = "预约日期开始")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@NotBlank(message = "开始日期不能为空")
	private String dateStart;

	/**
	 * 预约日期结束
	 */
	@ApiModelProperty(value = "预约日期结束")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@NotBlank(message = "结束日期不能为空")
	private String dateEnd;

	/** 预约详细数据 */
	@Valid
	@ApiModelProperty("预约详细数据")
	@NotEmpty(message = "预约详细数据不能为空")
	private List<BAppointDateNumDTO> appointDateNumDTOList;

	private static final long serialVersionUID = 1L;
}