package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @desc: Base64图片对象
 * @author: YJ
 * @date: 2021-12-06 19:14
 **/
@Data
public class Base64ImgVo {

    /** Base64图片字符串 */
    @NotBlank(message = "Base64字符串不能为空")
    @ApiModelProperty(value = "Base64图片字符串",required = true)
    private String base64String;

    /** 图片后缀名 */
    @NotBlank(message = "图片后缀不能为空")
    @ApiModelProperty(value = "图片后缀名",required = true)
    private String imageSuffix;
}
