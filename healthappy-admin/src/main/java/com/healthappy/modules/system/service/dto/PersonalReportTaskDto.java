package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class PersonalReportTaskDto implements Serializable {

    @ApiModelProperty("体检编号")
    private String id;

    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("性别")
    private String sex;

    private String sexText;

    @ApiModelProperty("年龄")
    private Integer age;

    @ApiModelProperty("套餐名称")
    private String packageName;

    @ApiModelProperty("分组id")
    private String departmentGroupId;

    @ApiModelProperty("分组")
    private String departmentGroupName;

    @ApiModelProperty("单位编号")
    private String companyId;

    @ApiModelProperty("单位")
    private String companyName;

    @ApiModelProperty("审核医生")
    private String verifyDoc;

    @ApiModelProperty("主检医生")
    private String conclusionDoc;
}
