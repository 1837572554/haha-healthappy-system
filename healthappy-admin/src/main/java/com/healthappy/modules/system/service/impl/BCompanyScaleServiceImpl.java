package com.healthappy.modules.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BCompanyScale;
import com.healthappy.modules.system.service.BCompanyScaleService;
import com.healthappy.modules.system.service.dto.BCompanyScaleDto;
import com.healthappy.modules.system.service.dto.BCompanyScaleQueryCriteria;
import com.healthappy.modules.system.service.mapper.BCompanyScaleMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
/**
 * @description 企业规模表
 * @author sjc
 * @date 2021-09-2
 */
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BCompanyScaleServiceImpl extends BaseServiceImpl<BCompanyScaleMapper, BCompanyScale> implements BCompanyScaleService {

    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BCompanyScaleQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BCompanyScale> page = new PageInfo<BCompanyScale>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("content", generator.convert(page.getList(), BCompanyScaleDto.class));
        map.put("totalElements", page.getTotal());

        return map;
    }
    /**
     * 查询企业经济类型全部数据
     * @return Object
     */
    @Override
    public List<BCompanyScale> queryAll(BCompanyScaleQueryCriteria criteria) {
        return this.list(QueryHelpPlus.getPredicate(BCompanyScaleDto.class, criteria));
    }
}
