package com.healthappy.modules.system.service.impl;

import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.BSex;
import com.healthappy.modules.system.service.BSexService;
import com.healthappy.modules.system.service.mapper.BSexMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BSexServiceImpl extends BaseServiceImpl<BSexMapper, BSex> implements BSexService {

   
}
