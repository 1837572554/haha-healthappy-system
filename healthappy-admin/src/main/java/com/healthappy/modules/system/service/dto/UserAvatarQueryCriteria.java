package com.healthappy.modules.system.service.dto;

import lombok.Data;

/**
 * @author hupeng
 * @date 2020-05-14
 */
@Data
public class UserAvatarQueryCriteria {
}
