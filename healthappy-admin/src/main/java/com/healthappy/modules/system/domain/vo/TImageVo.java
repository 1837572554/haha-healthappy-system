package com.healthappy.modules.system.domain.vo;/**
 * @author Jevany
 * @date 2021/12/1 13:02
 * @desc
 */

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

/**
 * @desc: 图片Vo
 * @author: YJ
 * @date: 2021-12-01 13:02
 **/
@Builder
@Data
public class TImageVo {
    /** 主键id-长度20 */
    @ApiModelProperty("主键id-长度20，上传成功才会有数据")
    private String id;

    /** 文件名称（上传时的文件名） */
    @ApiModelProperty("文件名称（上传时的文件名）")
    private String fileName;

    /** 图片Url */
    @ApiModelProperty("图片Url，如失败会在此处填失败原因")
    private String url;

    public TImageVo(String id,String fileName,String url){
        this.id=id;
        this.fileName=fileName;
        this.url=url;
    }
}
