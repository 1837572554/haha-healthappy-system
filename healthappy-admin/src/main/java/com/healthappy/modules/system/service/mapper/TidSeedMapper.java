package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TidSeed;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * @description ID种子
 * @author FGQ
 * @date 2021-10-29
 */
@Mapper
@Repository
public interface TidSeedMapper extends CoreMapper<TidSeed> {

    /**
     * 查询种子
     * @param workStationCode
     * @param tableName
     * @return
     */
    @Select("select * from T_Id_Seed where work_station_code = #{workStationCode} and table_name = #{tableName}")
    TidSeed selectByOne(@Param("workStationCode") String workStationCode,@Param("tableName") String tableName);

    /**
     * 查询表中Id的长度
     * @param tableName 表名
     * @return
     */
    @SqlParser(filter = true)
    @Select("select  \n" +
            "\n" +
            "\n" +
            "cast(substring(column_type,LOCATE('(',column_type)+CHAR_LENGTH('('),\n" +
            " \n" +
            "locate(')',column_type,CHAR_LENGTH(')'))-(SELECT locate('(',column_type)+ CHAR_LENGTH('('))\n" +
            " \n" +
            ") as SIGNED INTEGER)\n" +
            "\n" +
            "FROM information_schema.columns\n" +
            "\n" +
            "where table_name= #{tableName} and column_name = 'id'")
    int getTableLen(@Param("tableName") String tableName);
}
