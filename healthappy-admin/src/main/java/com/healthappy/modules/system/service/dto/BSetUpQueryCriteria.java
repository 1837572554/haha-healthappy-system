package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BSetUpQueryCriteria {

    /**
     * id
     */
    @Query(type = Query.Type.EQUAL)
    @ApiModelProperty("id")
    private String id;

    /**
     * 毒害名称
     */
    @Query(type = Query.Type.INNER_LIKE)
    @ApiModelProperty("设置名称")
    private String xmlName;
}
