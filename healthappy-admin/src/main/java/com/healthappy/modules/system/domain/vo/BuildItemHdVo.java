package com.healthappy.modules.system.domain.vo;

import com.healthappy.modules.system.domain.BGroupHd;
import com.healthappy.modules.system.domain.BItem;
import com.healthappy.modules.system.domain.BPackageDT;
import lombok.Data;

import java.util.List;

@Data
public class BuildItemHdVo {

    private List<String> groupIds;

    private List<BItem> itemList;

    private String packageId;

    private String packageName;

    private String departmentId;

    private String paId;

    private String peTypeName;
}
