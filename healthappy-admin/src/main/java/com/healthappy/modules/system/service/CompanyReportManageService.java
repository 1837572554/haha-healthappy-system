package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TCompanyRepHd;
import com.healthappy.modules.system.domain.vo.CompanyReportVO;
import com.healthappy.modules.system.service.dto.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 单位报告管理 服务接口
 * @author Jevany
 * @date 2022/2/17 15:34
 */
public interface CompanyReportManageService extends BaseService<TCompanyRepHd> {

    /**
     * 查询单位报告管理列表
     * @author YJ
     * @date 2022/2/17 15:51
     * @param criteria
     * @return com.healthappy.modules.system.service.dto.CompanyReportDTO
     */
    Map<String, Object> getList(CompanyReportQueryCriteria criteria);

    /**
     * 添加单位报告编制
     * @author YJ
     * @date 2022/2/17 17:44
     * @param companyRepHdVO
     */
    String saveReport(CompanyReportVO companyRepHdVO);

    /**
     * 保存人员
     * @author YJ
     * @date 2022/2/18 19:08
     * @param saveDTO 单位报告人员保存对象
     */
    void savePerson(CompanyReportPersonSaveDTO saveDTO);

    /**
     * 获得人员
     * @author YJ
     * @date 2022/2/18 19:40
     * @param criteria
     * @return java.util.Map〈java.lang.String,java.lang.Object〉
     */
    Map<String, Object> getPerson(CompanyReportPersonQueryCriteria criteria);

    /**
     * 获得单位职业报告人员列表
     * @author YJ
     * @date 2022/2/18 18:28
     * @param criteria 单位职业报告管理-人员查询类
     * @return java.util.Map〈java.lang.String,java.lang.Object〉单位报告管理 人员列表
     */
    Map<String, Object> getOccupationPersonList(CompanyReportOccupationPersonQueryCriteria criteria);

    /**
     * 获得单位健康报告人员列表
     * @author YJ
     * @date 2022/2/18 18:29
     * @param criteria 单位健康报告管理-人员查询类
     * @return java.util.Map〈java.lang.String,java.lang.Object〉单位报告管理 人员列表
     */
    Map<String, Object> getHealthPersonList(CompanyReportHealthPersonQueryCriteria criteria);

    /**
     * 获取病种名单
     * @author YJ
     * @date 2022/2/23 15:04
     * @return java.util.Map〈java.lang.String,java.lang.Object〉
     */
    Map<String,Object> getSicknessList(CompanyReportPersonQueryCriteria criteria);

    /**
     * 导出病种名单
     * @param all
     * @param response
     * @throws IOException
     */
    void exportSicknessList(List<CompanyRepSicknessListDTO> all, HttpServletResponse response) throws IOException;

    /**
     * 获取病种人员名单
     * @author YJ
     * @date 2022/2/23 16:19
     * @param criteria 单位报告病种人员列表查询类
     * @return java.util.Map〈java.lang.String,java.lang.Object〉
     */
    Map<String,Object> getSicknessPersonList(CompanyReportSicknessPersonListQueryCriteria criteria);

    /**
     * 导出病种人员名单
     * @author YJ
     * @date 2022/2/23 17:16
     * @param all
     * @param response
     */
    void exportSicknessPersonList(List<CompanyReportSicknessPersonListDTO> all,HttpServletResponse response) throws IOException;

    /**
     * 报告审核
     * @author YJ
     * @date 2022/2/25 17:06
     * @param verifyDTO
     * @return java.lang.Boolean
     */
    void verifyReport(CompanyReportVerifyDTO verifyDTO);

}