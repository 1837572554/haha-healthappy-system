package com.healthappy.modules.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BSetUp;
import com.healthappy.modules.system.service.BSetUpService;
import com.healthappy.modules.system.service.dto.BSetUpDto;
import com.healthappy.modules.system.service.dto.BSetUpQueryCriteria;
import com.healthappy.modules.system.service.mapper.BSetUpMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 综合设置服务层实现层
 * @author cyt
 * @date 2021-08-17
 */
@Service
@AllArgsConstructor//有自动注入的功能
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BSetUpServiceImpl  extends BaseServiceImpl<BSetUpMapper, BSetUp> implements BSetUpService {

    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BSetUpQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BSetUp> page = new PageInfo<BSetUp>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("content", generator.convert(page.getList(), BSetUpDto.class));
        map.put("totalElements", page.getTotal());

        return map;
    }

    @Override
    public List<BSetUp> queryAll(BSetUpQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BSetUpDto.class, criteria));
    }
}
