package com.healthappy.modules.system.service.impl;

import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BCompany;
import com.healthappy.modules.system.domain.BTypeCompany;
import com.healthappy.modules.system.domain.BTypeResult;
import com.healthappy.modules.system.service.BTypeCompanyService;
import com.healthappy.modules.system.service.BTypeResultService;
import com.healthappy.modules.system.service.dto.BCompanyQueryCriteria;
import com.healthappy.modules.system.service.dto.BTypeCompanyQueryCriteria;
import com.healthappy.modules.system.service.mapper.BTypeCompanyMapper;
import com.healthappy.modules.system.service.mapper.BTypeResultMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @description 企业经济类型
 * @author sjc
 * @date 2021-08-30
 */
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BTypeCompanyServiceImpl extends BaseServiceImpl<BTypeCompanyMapper, BTypeCompany> implements BTypeCompanyService {

    private final IGenerator generator;

    @Override
    public Map<String, Object> queryAll(BTypeCompanyQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BTypeCompany> page = new PageInfo<BTypeCompany>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("content", generator.convert(page.getList(), BTypeCompany.class));
        map.put("totalElements", page.getTotal());

        return map;
    }
    /**
     * 查询企业经济类型全部数据
     * @return Object
     */
    @Override
    public List<BTypeCompany> queryAll(BTypeCompanyQueryCriteria criteria) {
        return this.list(QueryHelpPlus.getPredicate(BTypeCompany.class, criteria));
    }
}
