package com.healthappy.modules.system.rest.manage;

import cn.hutool.core.util.ObjectUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.service.BAppointDateService;
import com.healthappy.modules.system.service.dto.AppointDateDTO;
import com.healthappy.modules.system.service.dto.AppointDateOneDTO;
import com.healthappy.utils.R;
import com.healthappy.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 预约号源管理
 * @author Jevany
 * @date 2022/5/6 11:28
 */
@Slf4j
@Api(tags = "体检管理：预约号源管理")
@RestController
@AllArgsConstructor
@RequestMapping("/AppointNumManage")
public class AppointNumManageController {

	private final BAppointDateService appointDateService;

	@Log("预约号源-列表查询")
	@ApiOperation("预约号源-列表查询，权限码：AppointNumManage:list")
	@GetMapping
	@PreAuthorize("@el.check('AppointNumManage:list')")
	public R list(@RequestParam Integer year, @RequestParam Integer month) {
		if (ObjectUtil.isEmpty(year) || ObjectUtil.isEmpty(month)) {
			return R.ok("请正确输入年月");
		}
		return R.ok(appointDateService.getMonthAppointDateList(year, month));
	}

	@Log("预约号源-批量新增")
	@ApiOperation("预约号源-批量新增，权限码：AppointNumManage:list")
	@PostMapping("/batchAddition")
	@PreAuthorize("@el.check('AppointNumManage:list')")
	public R batchAddition(@Validated @RequestBody AppointDateDTO appointDateDTO) {
		appointDateService.batchAddition(appointDateDTO);
		return R.ok();
	}

	@Log("预约号源-批量清除")
	@ApiOperation("预约号源-批量清除，权限码：AppointNumManage:list")
	@GetMapping("/batchClean")
	@PreAuthorize("@el.check('AppointNumManage:list')")
	public R batchClean(@RequestParam String dateStart, @RequestParam String dateEnd) {
		if (StringUtils.isBlank(dateStart)) {
			return R.error("开始时间不能为空");
		}
		if (StringUtils.isBlank(dateEnd)) {
			return R.error("结束时间不能为空");
		}
		appointDateService.batchClean(dateStart, dateEnd);
		return R.ok();
	}

	@Log("预约号源-新增或修改")
	@ApiOperation("预约号源-新增或修改，权限码：AppointNumManage:list")
	@PostMapping("/addOrUpdate")
	@PreAuthorize("@el.check('AppointNumManage:list')")
	public R addOrUpdate(@Validated @RequestBody AppointDateOneDTO appointDateOneDTO) {
		appointDateService.addOrUpdate(appointDateOneDTO);
		return R.ok();
	}

	//	@Log("test")
	//	@ApiOperation("test，权限码：AppointNumManage:list")
	//	@PostMapping("/test")
	//	@PreAuthorize("@el.check('AppointNumManage:list')")
	//	public R test(String companyId, String appointId) {
	//		appointDateService.companyCanRegister(companyId, appointId);
	//		return R.ok();
	//	}

}
