package com.healthappy.modules.system.service.impl;

import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.modules.system.domain.BTypePay;
import com.healthappy.modules.system.service.BTypePayService;
import com.healthappy.modules.system.service.mapper.BTypePayMapper;
import org.springframework.stereotype.Service;

/**
 * Created with IntelliJ IDEA. Created by yutang 2021/9/2 0002  14:41 Description:
 */
@Service
public class BTypePayServiceImpl  extends BaseServiceImpl<BTypePayMapper, BTypePay> implements BTypePayService {

}
