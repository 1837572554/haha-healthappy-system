package com.healthappy.modules.system.rest.paycost;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.healthappy.annotation.AnonymousAccess;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.TChargeCOMApplyWujia;
import com.healthappy.modules.system.domain.TChargeComApply;
import com.healthappy.modules.system.domain.vo.TChargeComApplyVo;
import com.healthappy.modules.system.service.CompanyPayCostService;
import com.healthappy.modules.system.service.PayApplyPatientService;
import com.healthappy.modules.system.service.TChargeCOMApplyWujiaService;
import com.healthappy.modules.system.service.dto.ApplyWujiaExportDTO;
import com.healthappy.modules.system.service.dto.CompanyPayApplyExportDto;
import com.healthappy.modules.system.service.dto.CompanyPayCostCriteria;
import com.healthappy.utils.R;
import com.healthappy.utils.SecurityUtils;
import com.healthappy.utils.StringUtils;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Api(tags = "缴费管理：单位缴费")
@RestController
@AllArgsConstructor
@RequestMapping("/payCostCompany")
public class CompanyPayCostController {

    private final CompanyPayCostService companyPayCostService;
    private final PayApplyPatientService payApplyPatientService;
    private final IGenerator generator;
    private final TChargeCOMApplyWujiaService wujiaService;

    @Log("缴费管理：单位缴费——查询待缴列表")
    @ApiOperation("缴费管理：单位缴费——查询待缴列表，权限码：payCostCompany:list")
    @GetMapping
    @PreAuthorize("@el.check('payCostCompany:list')")
    @ApiResponses(value = {
            @ApiResponse( code = 200,message = "",response = TChargeComApplyVo.class)
    })
    public R getList(@Validated CompanyPayCostCriteria criteria, Pageable pageable) {
        return R.ok(companyPayCostService.getList(criteria,pageable));
    }

    @Log("缴费管理：单位缴费——单位缴费申请保存")
    @ApiOperation("缴费管理：单位缴费——单位缴费申请保存，权限码：payCostCompany:insert")
    @PostMapping("/saveApply")
    @PreAuthorize("@el.check('payCostCompany:insert')")
    @Klock
    public R saveApply(@RequestBody @Validated TChargeComApply vo) {
        //判断是结账操作还是新增申请单操作 id不为空，则是结账操作
        if(!StringUtils.isBlank(vo.getId())) {
            List<TChargeComApply> applyList = companyPayCostService.list(new LambdaQueryWrapper<TChargeComApply>()
                    .eq(TChargeComApply::getId,vo.getId())
            );
            vo.setApplyTime(applyList.get(0).getApplyTime());
            vo.setPayTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        }
        else {
            vo.setApplyTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        }
        if (StringUtils.isBlank(vo.getCompanyId())) {
            throw new BadRequestException("单位ID不能为空！");
        }
        return R.ok(companyPayCostService.saveOrUpdate(generator.convert(vo, TChargeComApply.class)));
    }

    @Log("缴费管理：单位缴费——删除缴费申请")
    @ApiOperation("缴费管理：单位缴费——删除缴费申请，权限码：payCostCompany:remove")
    @DeleteMapping("/removeApply")
    @PreAuthorize("@el.check('payCostCompany:remove')")
    @Klock
    public R removeApply(@RequestBody Set<String> ids) {
        return R.ok(companyPayCostService.removeByIds(ids));
    }

    @Log("缴费管理：单位缴费——导出数据")
    @GetMapping("/export")
    @ApiOperation("缴费管理：单位缴费——导出数据，权限码：payCostCompany:export")
    @PreAuthorize("@el.check('payCostCompany:export')")
    @AnonymousAccess
    @ResponseExcel(name = "单位缴费")
    @Klock
    public List<CompanyPayApplyExportDto> list(CompanyPayCostCriteria criteria){
        List<TChargeComApply> comApplyList = companyPayCostService.getAll(criteria);
        return generator.convert(comApplyList,CompanyPayApplyExportDto.class);
    }

    @Log("缴费管理：单位缴费——保存物价")
    @PostMapping("/savePrice")
    @ApiOperation("缴费管理：单位缴费——保存物价，权限码：payCostCompany:insert")
    @PreAuthorize("@el.check('payCostCompany:insert')")
    @Klock
    public R savePrice(@RequestBody @Validated List<TChargeCOMApplyWujia> priceList){
        companyPayCostService.savePrice(priceList);
        return R.ok();
    }

    @Log("缴费管理：单位缴费——物价列表")
    @GetMapping("/priceList")
    @ApiOperation("缴费管理：单位缴费——物价列表，权限码：payCostCompany:list")
    @PreAuthorize("@el.check('payCostCompany:list')")
    @Klock
    public R<List<TChargeCOMApplyWujia>> priceList(@RequestParam("payApplyId")
                           @ApiParam(value = "缴费申请单号",required = true)String payApplyId){
        String tenantId = SecurityUtils.getTenantId();
        return R.ok(wujiaService.list(Wrappers.<TChargeCOMApplyWujia>lambdaQuery()
                .eq(TChargeCOMApplyWujia::getPayApplyId,payApplyId)
                .eq(TChargeCOMApplyWujia::getTenantId,tenantId)));
    }

    @Log("缴费管理：单位缴费——导出物价数据")
    @GetMapping("/exportPrice")
    @ApiOperation("缴费管理：单位缴费——导出物价数据，权限码：payCostCompany:export")
    @PreAuthorize("@el.check('payCostCompany:export')")
    @AnonymousAccess
    @ResponseExcel(name = "单位缴费物价")
    @Klock
    public List<ApplyWujiaExportDTO> exportPrice(@RequestParam("payApplyId")
                                                          @ApiParam(value = "缴费申请单号",required = true)String payApplyId){
        List<TChargeCOMApplyWujia> list = wujiaService.list(Wrappers.<TChargeCOMApplyWujia>lambdaQuery()
                .eq(TChargeCOMApplyWujia::getPayApplyId, payApplyId));
        return generator.convert(list, ApplyWujiaExportDTO.class);
    }
}
