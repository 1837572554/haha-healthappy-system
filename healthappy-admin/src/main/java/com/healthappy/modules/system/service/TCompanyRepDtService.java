package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.TCompanyRepDt;

/**
 * 单位报告人员表 服务
 * @author Jevany
 * @date 2022/2/18 18:55
 */
public interface TCompanyRepDtService extends BaseService<TCompanyRepDt> {
}