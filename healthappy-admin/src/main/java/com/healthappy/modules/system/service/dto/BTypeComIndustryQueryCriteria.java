package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * @description 企业从事行业类型表
 * @author sjc
 * @date 2021-08-30
 */
@Data
public class BTypeComIndustryQueryCriteria  {

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    @Query(type = Query.Type.EQUAL)
    private Long id;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    @Query(type = Query.Type.INNER_LIKE)
    private String name;
    /**
     * 标准编码
     */
    @ApiModelProperty("标准编码")
    @Query(type = Query.Type.EQUAL)
    private String code;

    /**
     * 上级类别编码
     */
    @ApiModelProperty("上级类别编码")
    @Query(type = Query.Type.EQUAL)
    private String hiCode;
}
