package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BSicknessZH;
import com.healthappy.modules.system.service.dto.BSicknessZHQueryCriteria;
import com.healthappy.modules.system.service.dto.DeptItemSicknessTreeDTO;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @description 病种组合子项
 * @author sjc
 * @date 2021-08-3
 */
public interface BSicknessZHService extends BaseService<BSicknessZH> {


    Map<String, Object> queryAll(BSicknessZHQueryCriteria criteria, Pageable pageable);

    List<BSicknessZH> queryAll(BSicknessZHQueryCriteria criteria);
    /**
     * 根据病种组合id删除子项
     * @param SicknessZhId
     * @return
     */
    boolean deleteBySicknessZhId(String SicknessZhId);
    /**
     * 根据病种组合id集合批量删除子项
     * @param SicknessZhIds
     * @return
     */
    boolean deleteBySicknessZhIds(Set<String> SicknessZhIds);

    /**
     * 根据病种di删除子项
     * @param SicknessId
     * @return
     */
    boolean deleteBySicknessId(String SicknessId);

    /**
     * 获取科室、明细项、病种 树数据
     * @author YJ
     * @date 2022/3/9 10:25
     * @param simpleSpelling 名称或简拼
     * @return java.util.List〈com.healthappy.modules.system.service.dto.DeptItemSicknessTreeDTO〉
     */
    List<DeptItemSicknessTreeDTO> deptItemSicknessTree(String simpleSpelling);
}
