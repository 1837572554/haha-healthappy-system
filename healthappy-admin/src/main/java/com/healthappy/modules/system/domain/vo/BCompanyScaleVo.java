package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @description 企业规模表
 * @author sjc
 * @date 2021-09-2
 */
@Data
public class BCompanyScaleVo  {



    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private Long id;

    /**
     * 规模类型
     */
    @ApiModelProperty("规模类型")
    @NotBlank(message = "规模类型不能为空")
    private String type;

    /**
     * 标准代码
     */
    @ApiModelProperty("标准代码")
    private String code;
}
