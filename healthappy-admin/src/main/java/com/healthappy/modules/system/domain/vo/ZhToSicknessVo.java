package com.healthappy.modules.system.domain.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;


/**
 * @description 组合判断设置
 * @author sjc
 * @date 2021-08-2
 */
@Data
public class ZhToSicknessVo {

    /**
     * 主键id（组合病种主键id）
     */
    @ApiModelProperty("主键id（组合病种主键id）")
    private Long id;

    /**
     * 病种id
     */
    @ApiModelProperty("病种id")
    @NotNull(message = "病种id不能为空")
    private String sicknessId;

    /**
     * 组合名称
     */
    @ApiModelProperty("组合名称")
    @NotNull(message = "组合名称不能为空")
    private String zhName;

    /**
     * jp
     */
    @ApiModelProperty("简拼")
    private String jp;


    @ApiModelProperty("病种组合子项集合数据")
    private List<SicknessZHVo> bSicknessZHVoList;
}
