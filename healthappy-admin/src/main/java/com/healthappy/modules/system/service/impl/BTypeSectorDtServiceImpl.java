package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BTypeSectorDt;
import com.healthappy.modules.system.service.BTypeSectorDtService;
import com.healthappy.modules.system.service.BTypeTakebloodService;
import com.healthappy.modules.system.service.dto.BTypeSectorDtDto;
import com.healthappy.modules.system.service.dto.BTypeSectorDtQueryCriteria;
import com.healthappy.modules.system.service.mapper.BTypeSectorDtMapper;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BTypeSectorDtServiceImpl extends BaseServiceImpl<BTypeSectorDtMapper, BTypeSectorDt> implements BTypeSectorDtService {

    private final IGenerator generator;

    private final BTypeTakebloodService typeTakebloodService;

    @Override
    public Map<String, Object> queryAll(BTypeSectorDtQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<BTypeSectorDt> page = new PageInfo<BTypeSectorDt>(queryAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", generator.convert(page.getList(), BTypeSectorDtDto.class));
        map.put("totalElements", page.getTotal());
        return map;
    }

    @Override
    public List<BTypeSectorDt> queryAll(BTypeSectorDtQueryCriteria criteria) {
        return baseMapper.selectList(QueryHelpPlus.getPredicate(BTypeSectorDtDto.class, criteria));
    }
}
