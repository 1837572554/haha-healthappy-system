package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 项目病种名与判定
 * @author YJ
 * @date 2022/2/22 11:46
 */
@Data
public class SicknessByItemDto implements Serializable {

    /** 病种规则 主键id */
    @ApiModelProperty("病种规则 主键id")
    private String id;

    /** 是否阳性结果 */
    @ApiModelProperty("是否阳性结果")
    private String isPositive;

    /** 病种名称 */
    @ApiModelProperty("病种名称")
    private String sicknessName;
    
    /** 判定文字 */
    @ApiModelProperty("判定文字")
    private String mark;

    /** 是否危急值 */
    @ApiModelProperty("是否危急值")
    private String isCritical;
}
