package com.healthappy.modules.system.service.dto;

import com.healthappy.annotation.Query;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
public class BCommpanyGroupQueryCriteria {

    /**
     * 集团名称
     */
    @Query(type = Query.Type.INNER_LIKE)
    private String name;
}
