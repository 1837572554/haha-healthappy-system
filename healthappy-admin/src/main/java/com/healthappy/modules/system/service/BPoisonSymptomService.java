package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BPoisonSymptom;
import com.healthappy.modules.system.service.dto.BPoisonSymptomQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @description 毒害对应症状细表 服务层
 * @author sjc
 * @date 2021-08-9
 */
public interface BPoisonSymptomService extends BaseService<BPoisonSymptom> {
    Map<String, Object> queryAll(BPoisonSymptomQueryCriteria criteria, Pageable pageable);

    List<BPoisonSymptom> queryAll(BPoisonSymptomQueryCriteria criteria);
    /**
     * 根据危害因素id删除
     * @param poisonId
     * @return
     */
    boolean deleteByPoId(String poisonId);
    /**
     * 根据危害因素id集合批量删除
     * @param poisonIds
     * @return
     */
    boolean deleteByPoIds(List<Long> poisonIds);
    /**
     * 根据症状id删除
     * @param symptomId
     * @return
     */
    boolean deleteBySymId(Long symptomId);

    /**
     * 根据症状id集合批量删除
     * @param symptomIds
     * @return
     */
    boolean deleteBySymIds(List<Long> symptomIds);
}
