package com.healthappy.modules.system.service.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @description 物价表
 * @author SJC
 * @date 2021-08-3
 */
@Data
public class BWuJiaQueryCriteria {
    /**
     *主键id
     */
    @Query(type = Query.Type.EQUAL)
    private String id;


    /**
     * 物价项目ID
     */
    @Query(type = Query.Type.EQUAL)
    private String code;


    /**
     * 物价项目名称
     */
    @Query(type = Query.Type.INNER_LIKE)
    private String itemCnname;

    /**
     * 物价项目名称简拼
     */
    @Query(type = Query.Type.INNER_LIKE)
    private String jp;
    /**
     * 价格
     */
    @Query(type = Query.Type.BETWEEN)
    private List<Double> price;
}
