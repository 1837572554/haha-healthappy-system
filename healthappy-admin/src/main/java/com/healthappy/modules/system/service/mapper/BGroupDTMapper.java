package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BGroupDT;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 项目组合子项
 * @author sjc
 * @date 2021-05-4
 */
@Mapper
@Repository
public interface BGroupDTMapper extends CoreMapper<BGroupDT> {
}
