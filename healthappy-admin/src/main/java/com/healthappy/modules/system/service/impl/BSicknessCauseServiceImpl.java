package com.healthappy.modules.system.service.impl;

import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.BSicknessCause;
import com.healthappy.modules.system.service.BSicknessCauseService;
import com.healthappy.modules.system.service.mapper.BSicknessCauseMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author FGQ
 * @date 2021-03-08
 */
@Service
@AllArgsConstructor
//@CacheConfig(cacheNames = "user")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BSicknessCauseServiceImpl extends BaseServiceImpl<BSicknessCauseMapper, BSicknessCause> implements BSicknessCauseService {


}
