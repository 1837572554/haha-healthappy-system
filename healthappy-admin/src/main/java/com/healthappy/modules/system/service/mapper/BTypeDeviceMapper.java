package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BTypeDevice;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 申请类型
 * @author sjc
 * @date 2021-08-3
 */
@Mapper
@Repository
public interface BTypeDeviceMapper extends CoreMapper<BTypeDevice> {
}
