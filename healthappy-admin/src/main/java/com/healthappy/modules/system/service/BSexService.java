package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BSex;

/**
 * @description 性别 服务层
 * @author fang
 * @date 2021-06-17
 */
public interface BSexService extends BaseService<BSex> {


}
