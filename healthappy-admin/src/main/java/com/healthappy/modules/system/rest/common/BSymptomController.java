package com.healthappy.modules.system.rest.common;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.domain.BSymptom;
import com.healthappy.modules.system.domain.vo.BSymptomVo;
import com.healthappy.modules.system.service.BSymptomService;
import com.healthappy.modules.system.service.dto.BSymptomQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @description 症状表
 * @author sjc
 * @date 2021-08-10
 */
@Slf4j
@Api(tags = "默认基础数据：症状")
@RestController
@AllArgsConstructor
@RequestMapping("/BSymptom")
public class BSymptomController {
    private final BSymptomService bSymptomService;

    @Log("分页查询症状表")
    @ApiOperation("分页查询症状表，权限码：BSymptom:list")
    @GetMapping
    public R listPage(BSymptomQueryCriteria criteria, Pageable pageable) {
        return R.ok(bSymptomService.queryAll(criteria, pageable));
    }

    @Log("新增|修改症状表")
    @ApiOperation("新增|修改症状表，权限码：admin")
    @PostMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R saveOrUpdate(@Validated @RequestBody BSymptomVo resources) {
        //如果名称重复
        if(checkBSymptomName(resources)) {
            return R.error(500,"症状名称已经存在");
        }
        BSymptom bSymptom = new BSymptom();
        BeanUtil.copyProperties(resources,bSymptom);
        if(bSymptomService.saveOrUpdate(bSymptom))
            return R.ok();
        return R.error(500,"方法异常");
    }

    @Log("删除症状表")
    @ApiOperation("删除症状表，权限码：admin")
    @DeleteMapping
    @PreAuthorize("@el.check('admin')")
    @Klock
    public R remove(@RequestBody Set<Long> ids) {
        if(bSymptomService.removeByIds(ids))
            return R.ok();
        return R.error(500,"方法异常");
    }

    /**
     * 用于判断名称是否存在
     * @param resources
     * @return
     */
    private Boolean checkBSymptomName(BSymptomVo resources) {
        //名称相同  id不同的情况下能查询到数据，那么就是名称重复
        return bSymptomService.lambdaQuery().eq(BSymptom::getName,resources.getName())
                .ne(ObjectUtil.isNotNull(resources.getId()),BSymptom::getId,resources.getId()).count() > 0;

    }
}
