package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.healthappy.modules.dataSync.config.RenewLog;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @desc: 体检项目图片关联表
 * @author: YJ
 * @date: 2021-11-30 16:50
 **/
@ApiModel("体检项目图片关联表")
@Data
@TableName("T_PATIENT_IMG")
@RenewLog
public class TPatientImg implements Serializable {

    /** 体检编号 */
    @ApiModelProperty(value = "体检编号",required = true)
    @NotBlank(message = "体检编号不能为空")
    private String paId;

    /** 组合项编号 */
    @ApiModelProperty(value = "组合项编号",required = true)
    @NotBlank(message = "组合项编号不能为空")
    private String groupId;

    /** 小项编号，可为空 */
    @ApiModelProperty("小项编号，可为空")
    private String itemId;

    /** 文件编号 */
    @ApiModelProperty(value = "文件编号",required = true)
    @NotBlank(message = "文件编号不能为空")
    private String fileId;

    /** 添加时间 */
    //jackson格式化时间的时候，默认是按照伦敦天文台的时间进行转换的，和北京时间相差8个时区
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(value = "添加时间",hidden = true)
    private Timestamp createTime;

    /** 医疗机构ID(U_Hospital_Info的ID) */
    @TableField(value = "tenant_id",fill = FieldFill.INSERT)
    @ApiModelProperty(value = "医疗机构ID",hidden = true)
    private String tenantId;
}
