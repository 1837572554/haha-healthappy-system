package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author yanjun
 * @description 项目与病种规则匹配后得到的病种结果
 * @date 2021/11/9
 */
@Data
public class ItemSicknessMatchDto  implements Serializable {
    /**
     * 小项编号
     */
    @ApiModelProperty("小项编号")
    private String itemId;

    /**
     * 病种编号
     */
    @ApiModelProperty("病种编号")
    private String sicknessId;

    /**
     * 病种名称
     */
    @ApiModelProperty("病种名称")
    private String sicknessName;

    /**
     * 是否是危急值 0不是 1是
     */
    @ApiModelProperty("是否危急值 0不是 1是")
    private String isCritical;
}
