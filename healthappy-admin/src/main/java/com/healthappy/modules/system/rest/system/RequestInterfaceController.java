package com.healthappy.modules.system.rest.system;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.http.report.RequestConstant;
import com.healthappy.modules.system.service.RequestInterfaceService;
import com.healthappy.modules.system.service.RoleService;
import com.healthappy.modules.system.service.UserService;
import com.healthappy.utils.R;
import com.healthappy.utils.SpringBeanFactoryUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.map.HashedMap;

import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.util.Map;

/**
 * @Author: wukefei
 * @Date: Created in 2022/3/29 9:38
 * @Description:
 * @Version: 1.0
 *
 */
@Api(tags = "系统：请求接口管理")
@RestController
@RequestMapping("/api/interface")
public class RequestInterfaceController {

	private final RequestInterfaceService requestInterfaceService;

	public RequestInterfaceController(RequestInterfaceService service) {
		this.requestInterfaceService = service;
	}



	@ApiOperation("获取接口列表")
	@GetMapping(value = "/getList")
	public R getList() {
		return R.ok(requestInterfaceService.getList());
	}


	@ApiOperation("根据key检查接口是否正常")
	@GetMapping(value = "/check")
	public R check(@RequestParam String key) {
		Boolean isLive = requestInterfaceService.check(key);
		if (isLive) {
			return R.ok("接口正常");
		}
		return R.error("无法访问");
	}

	/**
	 * 检查 对应host下的port端口是否能访问
	 * @param host
	 * @param port
	 * @return
	 */
	@ApiOperation("根据地址和端口检查服务是否正常")
	@GetMapping(value = "/checkHostandPort")
	public R checkHostandPort(@RequestParam(required = false) String host,@RequestParam(required = false,defaultValue = "0") int port){
			boolean hostConnectable =requestInterfaceService.checkHostandPort(host,port);
		if (hostConnectable) {
			return R.ok("接口正常");
		}
		return R.error("无法访问");
	}

	/**
	 * 启动 报告生成 服务
	 * @return
	 */
	@ApiOperation("重启报告生成api")
	@GetMapping(value = "/runReportApi")
	public R runReportApi() throws Exception{
		requestInterfaceService.shutdwon();
		Thread.sleep(1000L);
		Boolean run = requestInterfaceService.run();
		if (run) {
			return R.ok("启动成功");
		}
		return R.error("关闭失败");
	}

	/**
	 * 关闭 报告生成 服务
	 * @return
	 */
	@ApiOperation("关闭报告生成api")
	@GetMapping(value = "/shutdwonReportApi")
	public R shutdwonReportApi(){
		Boolean shutdwon = requestInterfaceService.shutdwon();
		if (shutdwon) {
			return R.ok("关闭成功");
		}
		return R.error("关闭失败");
	}
}

