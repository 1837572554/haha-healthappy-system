package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TImageDTO;
import com.healthappy.modules.system.domain.TPatientImg;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Jevany
 * @date 2021/12/1 16:55
 * @desc 体检项目图片绑定 Mapper
 */

@Mapper
@Repository
public interface TPatientImgMapper extends CoreMapper<TPatientImg> {

    /**
     * 根据体检号和组合编号，获取图片信息
     * @author YJ
     * @date 2022/2/16 14:37
     * @param paId 体检号
     * @param groupId 组合项编号
     * @return java.util.List〈com.healthappy.modules.system.domain.TImage〉
     */
    List<TImageDTO> getImagesByPaIdGroupId(@Param("paId") String paId, @Param("groupId") String groupId);

    TImageDTO getImageById(String fileId);
}