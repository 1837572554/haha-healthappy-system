package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Jevany
 * @date 2022/1/21 17:03
 * @desc 病种名单
 */
@ApiModel("病种名单")
@Data
public class SicknessConclusionNameDto implements Serializable {
	/** 病种id */
	@ApiModelProperty("病种id")
	private String sicknessId;

	/** 病种名称 */
	@ApiModelProperty("病种名称")
	private String sicknessName;

	/** 总人数 */
	@ApiModelProperty("总人数")
	private Long totalCnt;

	/** 男性人数 */
	@ApiModelProperty("男性人数")
	private Long manCnt;

	/** 女性人数 */
	@ApiModelProperty("女性人数")
	private Long womanCnt;
}