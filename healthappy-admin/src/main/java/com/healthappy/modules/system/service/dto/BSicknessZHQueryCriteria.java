package com.healthappy.modules.system.service.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.healthappy.annotation.Query;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description 病种组合子项
 * @author SJC
 * @date 2021-08-3
 */
@Data
public class BSicknessZHQueryCriteria  {

    /**
     * 病种组合id
     */
    @ApiModelProperty("病种组合id")
    @Query(type = Query.Type.EQUAL)
    private String sicknessZhId;

    /**
     * 病种id
     */
    @ApiModelProperty("病种id")
    @Query(type = Query.Type.EQUAL)
    private String sicknessId;

    /**
     * 是否包含
     */
    @ApiModelProperty("是否包含")
    @Query(type = Query.Type.EQUAL)
    private Long containsFlag;


}
