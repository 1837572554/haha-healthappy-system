package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TRecordRay;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @description 职业史放射表
 * @author sjc
 * @date 2021-12-15
 */
@Mapper
@Repository
public interface TRecordRayMapper extends CoreMapper<TRecordRay> {

}
