package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TPatientPoison;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Jevany
 * @date 2022/1/12 15:11
 * @desc
 */
@Mapper
@Repository
public interface TPatientPoisonMapper extends CoreMapper<TPatientPoison> {

}
