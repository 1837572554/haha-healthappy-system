package com.healthappy.modules.system.service.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @description 套餐项目表
 * @author sjc
 * @date 2021-07-05
 */
@Data
public class BPackageDTDto   implements Serializable {


    /**
     *套餐id
     */
    @ApiModelProperty("id")
    private String id;


    /**
     * 套餐编号
     */
    @ApiModelProperty("packageId")
    private String packageId;

    /**
     * 组合编号
     */
    @ApiModelProperty("groupId")
    private String groupId;
    /**
     * 组合名称
     */
    @ApiModelProperty("组合名称")
    private String groupName;

    @ApiModelProperty("性别")
    private String sex;

    /**
     * 是否是职检项目
     */
    @ApiModelProperty("是否是职检项目")
    private String typeZ;

    /**
     * 是否是普检项目
     */
    @ApiModelProperty("是否是普检项目")
    private String typeJ;

    /**
     * 折扣
     */
    @ApiModelProperty("折扣")
    private BigDecimal discount;

    /**
     * 类型 0--基础项目,1--附加项目
     */
    @ApiModelProperty("类型 0--基础项目,1--附加项目")
    private String type;

    @ApiModelProperty("实际价格")
    private BigDecimal price;
}
