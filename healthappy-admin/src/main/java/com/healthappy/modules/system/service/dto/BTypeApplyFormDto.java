package com.healthappy.modules.system.service.dto;

import com.healthappy.modules.system.domain.BTypeApplyForm;
import lombok.Data;

/**
 * @description 申请单类型
 * @author sjc
 * @date 2021-08-3
 */
@Data
public class BTypeApplyFormDto extends BTypeApplyForm {



}
