package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.Menu;
import com.healthappy.modules.system.service.dto.RoleMenuPidCountDTO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * @author hupeng
 * @date 2020-05-14
 */
@Repository
@Mapper
public interface MenuMapper extends CoreMapper<Menu> {


    /**
     * 根据菜单的 PID 查询
     * @param pid /
     * @return /
     */
    @Select("SELECT * from menu m where m.pid = #{pid} order by m.`sort` asc ")
    List<Menu> findByPid(@Param("pid") long pid);

    @Select("select m.* from menu m LEFT JOIN roles_menus t on m.id= t.menu_id LEFT JOIN role r on r.id = t.role_id where r.id = #{roleId} order by m.sort asc")
    Set<Menu> findMenuByRoleId(@Param("roleId") Long roleId);

    @Select("select distinct m.* from menu m LEFT JOIN roles_menus t on m.id= t.menu_id LEFT JOIN role r on r.id = t.role_id where m.permission is not null order by m.sort asc")
    Set<Menu> getAdminMenu();

    @Select("select distinct m.* from menu m LEFT JOIN roles_menus t on m.id= t.menu_id LEFT JOIN role r on r.id = t.role_id where m.type!=2 order by m.sort asc")
    List<Menu> selectAdminMenuList();

    @Select("<script>select m.* from menu m LEFT OUTER JOIN roles_menus t on m.id= t.menu_id LEFT OUTER JOIN role r on r.id = t.role_id where m.type!=2 and  r.id in <foreach collection=\"roleIds\" index=\"index\" item=\"item\" open=\"(\" separator=\",\" close=\")\">#{item}</foreach> order by m.sort asc</script>")
    List<Menu> selectListByRoles(@Param("roleIds") List<Long> roleIds);

    /**
     * 比较角色，设置的菜单Pid下的数量，是否等于真实菜单同Pid下的数量
     * @author YJ
     * @date 2022/3/4 14:58
     * @param roleId
     * @param menuPid
     * @return java.lang.Integer 相同为1，不同为0
     */
    @Select("select (case when count(1)=(select count(1) from menu m1 where m1.pid =#{menuPid}) then 1 else 0 end) as is_eq from roles_menus rm \n" +
            "\tINNER JOIN menu m on m.id =rm.menu_id \n" +
            "where role_id =#{roleId} and m.pid =#{menuPid}\n")
    Integer hasRoleMenuPidCountEqMenuPidCount(@Param("roleId") Long roleId,@Param("menuPid") Long menuPid);

    /**
     * 获取角色现有Pid下的菜单数量
     * @param roleId
     * @param menuPidList
     * @return
     */
    List<RoleMenuPidCountDTO> listRolePidMenuCount(@Param("roleId") Long roleId, @Param("menuPidList") List<Long> menuPidList);

    /**
     * 获取菜单Pid下的菜单数量
     * @author YJ
     * @date 2022/3/4 17:18
     * @param menuPidList
     * @return java.util.List〈com.healthappy.modules.system.service.dto.RoleMenuPidCountDTO〉
     */
    List<RoleMenuPidCountDTO> listPidMenuCount(@Param("menuPidList") List<Long> menuPidList);

}
