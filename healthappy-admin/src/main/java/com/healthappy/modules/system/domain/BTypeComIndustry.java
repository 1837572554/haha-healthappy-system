package com.healthappy.modules.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @description 企业从事行业类型表
 * @author sjc
 * @date 2021-08-30
 */
@Data
@ApiModel("企业从事行业类型表")
@TableName("B_Type_ComIndustry")
public class BTypeComIndustry implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 从事行业名称
     */
    @ApiModelProperty("从事行业名称")
    private String name;
    /**
     * 标准编码
     */
    @ApiModelProperty("标准编码")
    private String code;

    /**
     * 上级类别编码
     */
    @ApiModelProperty("上级类别编码")
    private String hiCode;

    /**
     * 排序
     */
    @ApiModelProperty("排序")
    private Integer dispOrder;

    @TableField(exist=false)
    @ApiModelProperty("子类型")
    private List<BTypeComIndustry> treeList = new ArrayList<>();//

}


