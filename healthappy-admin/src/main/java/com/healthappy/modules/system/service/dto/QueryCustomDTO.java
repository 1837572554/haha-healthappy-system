package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class QueryCustomDTO implements Serializable {

    private String id;

    private Long hdId;

    private Long dtId;

    @ApiModelProperty("姓名")
    private String tName;

    @ApiModelProperty("性别")
    private String sex;

    @ApiModelProperty("年龄")
    private String age;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("身份证")
    private String idNo;

    @ApiModelProperty("婚姻状况")
    private String marital;

    @ApiModelProperty("地址")
    private String address;

    @ApiModelProperty("体检分类")
    private String peTypeHd;

    @ApiModelProperty("体检类别")
    private String peTypeDt;

    @ApiModelProperty("民族")
    private String nation;

    @ApiModelProperty("工种")
    private String job;

    @ApiModelProperty("工种名称")
    private String jobName;

    @ApiModelProperty("总工龄")
    private String totalYears;

    @ApiModelProperty("工号")
    private String tCode;

    @ApiModelProperty("公司")
    private String company;

    @ApiModelProperty("部门")
    private String department;

    @ApiModelProperty("分组")
    private String departmentGroup;

    @ApiModelProperty("接害工龄")
    private String workYears;

    @ApiModelProperty("危害因素")
    private String poisonName;

    @ApiModelProperty("既往病史")
    private String bsJwbs;

    @ApiModelProperty("病名")
    private String bsBm;

    @ApiModelProperty("诊断时间")
    private String bsZdsj;

    @ApiModelProperty("诊断单位")
    private String bsZddw;

    @ApiModelProperty("是否痊愈")
    private String bsSfqy;

    @ApiModelProperty("烟史")
    private String bsYs;

    @ApiModelProperty("酒史")
    private String bsJs;

    @ApiModelProperty("职业结论")
    private String commentZ;

    @ApiModelProperty("职业建议")
    private String suggestZ;

    @ApiModelProperty("科室")
    private String deptName;

//    @ApiModelProperty("组合")
//    private String groupName;
//
//    @ApiModelProperty("组合判定")
//    private String resultDesc;
//
//    @ApiModelProperty("组合描述")
//    private String resultMark;
//
//    @ApiModelProperty("基础项目")
//    private String itemName;
//
//    @ApiModelProperty("基础判定")
//    private String itemResultMark;
}
