package com.healthappy.modules.system.service;

import com.healthappy.common.service.BaseService;
import com.healthappy.modules.system.domain.BGroupDT;
import com.healthappy.modules.system.domain.vo.BGroupDTVo;
import com.healthappy.modules.system.service.dto.BGroupDTQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @description 项目组合子项 服务层
 * @author sjc
 * @date 2021-08-4
 */
public interface BGroupDTService extends BaseService<BGroupDT> {

    Map<String, Object> queryAll(BGroupDTQueryCriteria criteria, Pageable pageable);

    List<BGroupDT> queryAll(BGroupDTQueryCriteria criteria);

    boolean removeBygroupId(String groupId);

    boolean removeByIds(Set<Integer> ids);

    void saveOrUpdate(List<BGroupDTVo> resources);
}
