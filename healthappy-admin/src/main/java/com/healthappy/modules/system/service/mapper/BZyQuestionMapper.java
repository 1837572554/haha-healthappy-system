package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BZyQuestion;
import com.healthappy.modules.system.service.dto.BZyQuestionDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Jevany
 * @date 2021/12/8 15:15
 * @desc 中医体质问题 Mapper
 */
@Mapper
@Repository
public interface BZyQuestionMapper extends CoreMapper<BZyQuestion> {

    /**
     * 获得体检者的问卷问题
     * @author YJ
     * @date 2021/12/8 16:58
     * @param paId 体检号
     * @param bankId 库编号 1：33题  2：67题
     * @param sex 性别 0不限 1男 2女
     * @param answerBankId 答案库编号
     * @return java.util.List〈com.healthappy.modules.system.domain.BZyQuestion〉
     */
    List<BZyQuestionDto> getQuestion(@Param("paId") String paId, @Param("bankId") Integer bankId, @Param("sex") String sex,@Param("answerBankId") String answerBankId);
}
