package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.pagehelper.PageInfo;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.modules.system.domain.TChargeCOMApplyWujia;
import com.healthappy.modules.system.domain.TChargeComApply;
import com.healthappy.modules.system.domain.TChargeComApplyPatient;
import com.healthappy.modules.system.domain.vo.TChargeComApplyVo;
import com.healthappy.modules.system.service.CompanyPayCostService;
import com.healthappy.modules.system.service.PayApplyPatientService;
import com.healthappy.modules.system.service.TChargeCOMApplyWujiaService;
import com.healthappy.modules.system.service.dto.CompanyPayCostCriteria;
import com.healthappy.modules.system.service.mapper.CompanyPayCostMapper;
import com.healthappy.utils.SecurityUtils;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class CompanyPayCostServiceImpl extends BaseServiceImpl<CompanyPayCostMapper, TChargeComApply> implements CompanyPayCostService {

    private final TChargeCOMApplyWujiaService wujiaService;
    private final PayApplyPatientService payApplyPatientService;

    private final IGenerator generator;

    @Override
    public Map<String, Object> getList(CompanyPayCostCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<TChargeComApply> page = new PageInfo<>(getAll(criteria));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", page.getList());
        map.put("totalElements", page.getTotal());
        return map;
    }

    @Override
    public List getAll(CompanyPayCostCriteria criteria) {
        QueryWrapper wrapper = QueryHelpPlus.getPredicate(TChargeComApply.class, criteria);
        if (ObjectUtil.isNotEmpty(criteria.getPayFlag())) {
            if (criteria.getPayFlag() == 0) {
                wrapper.ne("pay_flag", 1);
            } else {
                wrapper.eq("pay_flag", 1);
            }
        }

        List<TChargeComApply> list = baseMapper.selectList(wrapper);
        if(CollUtil.isNotEmpty(list)){
            List<TChargeComApplyVo> comApplyList = generator.convert(list, TChargeComApplyVo.class);
            comApplyList.forEach(comApply-> comApply.setPatientBind(payApplyPatientService.lambdaQuery().eq(TChargeComApplyPatient::getPayApplyId,comApply.getId()).count()>0?true:false));
            return comApplyList;
        } else {
            return new ArrayList();
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void savePrice(List<TChargeCOMApplyWujia> priceList) {
        //先删除旧的
        wujiaService.remove(Wrappers.<TChargeCOMApplyWujia>lambdaQuery()
                .eq(TChargeCOMApplyWujia::getPayApplyId,priceList.get(0).getPayApplyId()));
        String tenantId = SecurityUtils.getTenantId();
        priceList.forEach(p -> p.setTenantId(tenantId));
        wujiaService.saveBatch(priceList);
    }


}
