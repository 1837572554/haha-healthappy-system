package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Jevany
 * @date 2021/12/8 17:35
 * @desc 中医体质问卷问题
 */
@Data
public class BZyQuestionDto implements Serializable {
    /** 问题编号 */
    @ApiModelProperty("问题编号")
    private String questionId;

    /** 问题 */
    @ApiModelProperty("问题")
    private String content;

    /** 是否反向分值 0：正常 1：反向*/
    @ApiModelProperty("是否反向分值 0：正常 1：反向")
    private String change;


    /** 答案编号 1：没有 2：很少 3：有时 4：经常 5：总是 */
    @ApiModelProperty("答案编号 1：没有 2：很少 3：有时 4：经常 5：总是")
    private Integer answerId;
}
