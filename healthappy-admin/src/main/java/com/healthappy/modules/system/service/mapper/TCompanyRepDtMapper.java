package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.TCompanyRepDt;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 单位报告管理-报告与人员关联数据Mapper
 * @author Jevany
 * @date 2022/2/18 18:49
 */
@Mapper
@Repository
public interface TCompanyRepDtMapper extends CoreMapper<TCompanyRepDt> {
}
