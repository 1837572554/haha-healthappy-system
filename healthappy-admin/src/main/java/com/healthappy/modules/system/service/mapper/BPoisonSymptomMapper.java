package com.healthappy.modules.system.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.system.domain.BPoisonSymptom;
import org.apache.ibatis.annotations.Mapper;

/**
 * @description 毒害对应症状细表
 * @author sjc
 * @date 2021-08-10
 */
@Mapper
public interface BPoisonSymptomMapper extends CoreMapper<BPoisonSymptom> {



}
