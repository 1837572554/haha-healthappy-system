package com.healthappy.modules.dataSync.config;

/**
 * 这里是 拦截mybatis 的拦截器
 *
 * @Author: wukefei
 * @Date: Created in 2022/5/6 17:00
 * @Description:
 * @Version: 1.0
 *
 */
//@Intercepts({
//		@Signature(type = StatementHandler.class, method = "query", args = {Statement.class, ResultHandler.class}),
//		@Signature(type = StatementHandler.class, method = "update", args = Statement.class),
//		@Signature(type = StatementHandler.class, method = "batch", args = Statement.class)
//})
public class MybatisLogInterceptor {// implements Interceptor


	//	public static final String DELEGATE_MAPPED_STATEMENT = "delegate.mappedStatement";
	//
	//	/**
	//	 * 进行拦截的时候要执行的方法
	//	 *
	//	 * @param invocation
	//	 * @return
	//	 * @throws Throwable
	//	 */
	//	@Override
	//	public Object intercept(Invocation invocation) throws Throwable {
	//
	//
	//
	//		StatementHandler statementHandler = (StatementHandler) invocation.getTarget();
	//		//通过MetaObject优雅访问对象的属性，这里是访问statementHandler的属性;：MetaObject是Mybatis提供的一个用于方便、
	//		MetaObject metaObject = MetaObject.forObject(statementHandler,
	//				SystemMetaObject.DEFAULT_OBJECT_FACTORY,
	//				SystemMetaObject.DEFAULT_OBJECT_WRAPPER_FACTORY,
	//				new DefaultReflectorFactory());
	//
	//		//获取mappedStatement 对象
	//		MappedStatement mappedStatement = (MappedStatement) metaObject.getValue(DELEGATE_MAPPED_STATEMENT);
	//
	//		//获取对象的 注解
	//		Class<?> clazz = getTableClass(mappedStatement);
	//		if (clazz == null) {
	//			return 	invocation.proceed();
	//		}
	//		//获取表实体类上的注解
	//		RenewLog tableShard = clazz.getAnnotation(RenewLog.class);
	//		String name = clazz.getName();
	//		//注解存在 再进行拦截
	//		if (tableShard != null ) {
	//			//sql语句类型 select、DELETE、INSERT、UPDATE
	//			String sqlType = mappedStatement.getSqlCommandType().toString();
	//			//拦截 对应的操作
	//			if(sqlType.equals("UPDATE")||sqlType.equals("DELETE")||sqlType.equals("INSERT")){
	//				BoundSql boundSql = statementHandler.getBoundSql();
	//				//执行成功把  sql 记录下来
	//				Configuration configuration = mappedStatement.getConfiguration();
	//				// 把sql 中的 ？变成参数
	//				String realSql = getRealSql(configuration, boundSql);
	//				DataRenewLog log=DataRenewLog.builder()
	//						.id(UUID.randomUUID().toString().replace("-", ""))
	//						.modelName(name)
	//						.sqlString(realSql)
	//						.type(sqlType)
	//						.crateTime(LocalDateTime.now())
	//						.build();
	//				DataRenewLogMapper mapper= SpringContextHolder.getBean(DataRenewLogMapper.class);
	//				mapper.insert(log);
	//			}
	//		}
	//		return 	invocation.proceed();
	//	}
	//
	//
	//
	//
	//	/**
	//	 * 替换 占位符号
	//	 * @param configuration
	//	 * @param boundSql
	//	 * @return
	//	 */
	//	public static String getRealSql(Configuration configuration, BoundSql boundSql) {
	//		// 获取参数
	//		Object parameterObject = boundSql.getParameterObject();
	//		//获取sql中问号的基本信息
	//		List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();
	//		// sql语句中多个空格都用一个空格代替
	//		String sql = boundSql.getSql().replaceAll("[\\s]+", " ");
	//
	//		if (CollectionUtils.isNotEmpty(parameterMappings) && parameterObject != null) {
	//
	//			// 获取类型处理器注册器，类型处理器的功能是进行java类型和数据库类型的转换<br>　　　　　　　
	//			TypeHandlerRegistry typeHandlerRegistry = configuration.getTypeHandlerRegistry();
	//			// 如果根据parameterObject.getClass(）可以找到对应的类型，则替换
	//			if (typeHandlerRegistry.hasTypeHandler(parameterObject.getClass())) {
	//				sql = sql.replaceFirst("\\?", Matcher.quoteReplacement(getParameterValue(parameterObject)));
	//
	//			} else {
	//				// MetaObject主要是封装了originalObject对象，提供了get和set的方法用于获取和设置originalObject的属性值,主要支持对JavaBean、Collection、Map三种类型对象的操作
	//				MetaObject metaObject = configuration.newMetaObject(parameterObject);
	//				for (ParameterMapping parameterMapping : parameterMappings) {
	//					String propertyName = parameterMapping.getProperty();
	//					if (metaObject.hasGetter(propertyName)) {
	//						Object obj = metaObject.getValue(propertyName);
	//						sql = sql.replaceFirst("\\?", Matcher.quoteReplacement(getParameterValue(obj)));
	//					} else if (boundSql.hasAdditionalParameter(propertyName)) {
	//						// 该分支是动态sql
	//						Object obj = boundSql.getAdditionalParameter(propertyName);
	//						sql = sql.replaceFirst("\\?", Matcher.quoteReplacement(getParameterValue(obj)));
	//					} else {
	//						//打印出缺失，提醒该参数缺失并防止错位
	//						sql = sql.replaceFirst("\\?", "缺失");
	//					}
	//				}
	//			}
	//		}
	//		return sql;
	//	}
	//
	//
	//	/**
	//	 * 根据值的类型转 换成对应格式；
	//	 * @param obj
	//	 * @return
	//	 */
	//	private static String getParameterValue(Object obj) {
	//		String value = null;
	//		if (obj instanceof String) {
	//			value = "'" + obj.toString() + "'";
	//			return value;
	//		}
	//		if (obj instanceof Date) {
	//			DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, Locale.CHINA);
	//			value = "'" + formatter.format(new Date()) + "'";
	//			return value;
	//		}
	//		if (obj != null) {
	//			value = obj.toString();
	//		} else {
	//			value = "";
	//		}
	//		return value;
	//	}
	//
	//
	//	private Class<?> getTableClass(MappedStatement mappedStatement) throws ClassNotFoundException {
	//		String className = mappedStatement.getId();
	//		//获取到BaseMapper的实现类
	//		className = className.substring(0, className.lastIndexOf('.'));
	//		Class<?> clazz = Class.forName(className);
	//		if (BaseMapper.class.isAssignableFrom(clazz)) {
	//			//获取表实体类
	//			return (Class<?>) ((ParameterizedType) (clazz.getGenericInterfaces()[0])).getActualTypeArguments()[0];
	//		}
	//		return null;
	//	}
	//
	//	@Override
	//	public Object plugin(Object target) {
	//		return Plugin.wrap(target, this);
	//	}
	//
	//	@Override
	//	public void setProperties(Properties properties) {
	//	}
}
