package com.healthappy.modules.dataSync;

/**
 * @Author: wukefei
 * @Date: Created in 2022/5/5 12:20
 * @Description:
 * @Version: 1.0
 *
 */

import java.util.Arrays;
import java.util.List;

/**
 * @author: myqxin
 * @Desc:
 * @create: 2021-09-18 10:31
 **/
public class SqlKeywordConstant {

	public static final  String insert="INSERT INTO";
	public static final  String select="SELECT * FROM";
	public static final  String values=" VALUES ";

	public static final  String dropTable=" DROP TABLE IF EXISTS ";


	// 字段名称
	public static final  String columnName=" COLUMN_NAME ";
	// 数据类型
	public static final  String typeName=" TYPE_NAME ";
	// 字段长度
	public static final  String columnSize=" COLUMN_SIZE ";
	// 小数部分位数
	public static final  String decimalDigits=" DECIMAL_DIGITS ";
	//  是否可为空 1代表可空 0代表不可为空
	public static final  String nullable=" NULLABLE ";
	// 描述
	public static final  String remarks=" REMARKS ";



}



