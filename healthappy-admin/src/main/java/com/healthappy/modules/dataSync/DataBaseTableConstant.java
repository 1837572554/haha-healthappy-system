package com.healthappy.modules.dataSync;

import java.util.Arrays;
import java.util.List;

/**
 * @Author: wukefei
 * @Date: Created in 2022/5/5 12:20
 * @Description:
 * @Version: 1.0
 *
 */


public class DataBaseTableConstant {
	/**
	 * 基础数据表
	 * @return
	 */
	public static List<String> getBaseTableNames() {
		List<String>  tableNames= Arrays.asList("B_Area","B_Area_Standard","B_Barcode","B_Comment_C","B_Comment_WZ",
				"B_Comment_Z","B_Commpany_Group","B_Company","B_Company_Industry","B_Department","B_Department_Group_HD",
				"B_Department_Group_TD","B_Dept","B_Disease","B_Fee_Type","B_Group_DT","B_Group_HD","B_Group_HD_WuJia",
				"B_Item","B_Nation","B_PETYPE_DT_ToStandard","B_PETYPE_GROUP","B_PETYPE_ZYJOB_Standard","B_POISON_Standard",
				"B_Package_DT","B_Package_HD","B_PeType_DT","B_PeType_HD","B_PeType_Institution","B_PeType_Poison",
				"B_Petype_Job","B_Poison","B_Poison_Symptom","B_Province","B_Report_Config","B_Set_Up","B_Sex",
				"B_Sickness","B_Sickness_Advice","B_Sickness_Cause","B_Sickness_Rule","B_Sickness_Science","B_Sickness_ZH",
				"B_Student_Item_Level","B_Student_Item_Type","B_Student_Item_Val","B_Symptom","B_Type_Apply",
				"B_Type_Apply_Form","B_Type_Appoint","B_Type_ComIndustry","B_Type_Company","B_Type_Device",
				"B_Type_Get_Result","B_Type_Item","B_Type_Package","B_Type_Pay","B_Type_Result","B_Type_Sample",
				"B_Type_Sector_DT","B_Type_Sector_HD","B_Type_Takeblood","B_WuJia","B_ZY_ANSWER","B_ZY_QUESTION",
				"B_ZY_TZ","B_ZY_TZ_BANK","B_marital_status","U_Notice","U_Notice_Dept","U_WorkStation","Zh_To_Sickness",
				"dict","dict_detail","tag","tag_sign","tenant","user","user_avatar","user_tag","users_depts","users_roles",
				"verification_code","visits","role","roles_menus","sign","menu","tag","tag_sign","tenant","user","user_avatar",
				"user_tag","users_depts","users_roles","verification_code","visits"
		);
		return tableNames;
	}


	public static List<String> getBusinessTableNames() {
		List<String>  tableNames= Arrays.asList("T_Appoint","T_Appoint_Group","T_CHARGE","T_COMPANY_REP_DT","T_COMPANY_REP_HD",
				"T_CRITICAL","T_Charge_COM","T_Charge_COM_Apply","T_Charge_COM_Apply_Patient","T_Charge_COM_Apply_Wujia",
				"T_DCT_DATA","T_DCT_DATA_XZ","T_IMAGE","T_IMGTXTSCAN","T_Id_Seed","T_Item_DT","T_Item_HD","T_Item_Sickness",
				"T_PATIENT_IMG","T_Patient","T_Patient_Baseinfo","T_Patient_C","T_Patient_Poison","T_Patient_Z",
				"T_REPORT_RECORD","T_Record","T_Record_Ray","T_SEED","T_Wz","T_ZY_ANSWER","T_ZY_ANSWER_BANK");
		return tableNames;
	}
}



