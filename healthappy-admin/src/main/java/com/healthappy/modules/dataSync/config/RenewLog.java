package com.healthappy.modules.dataSync.config;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @Author: wukefei
 * @Date: Created in 2022/5/7 15:37
 * @Description:
 * @Version: 1.0
 *
 */
@Target({TYPE})
@Retention(RUNTIME)
public @interface RenewLog {

}
