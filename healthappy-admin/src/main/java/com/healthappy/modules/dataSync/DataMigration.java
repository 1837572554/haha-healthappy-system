package com.healthappy.modules.dataSync;

/**
 * @Author: wukefei
 * @Date: Created in 2022/5/5 12:20
 * @Description:
 * @Version: 1.0
 *
 */

import java.sql.*;
import java.util.List;

/**
 * @author: myqxin
 * @Desc:
 * @create: 2021-09-18 10:31
 **/
public class DataMigration {



	/**
	 * 数据源
	 *
	 * @return
	 */
	private Connection formConn() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			return DriverManager.getConnection("jdbc:mysql://47.97.0.227:3306/medical_sys_pro?useUnicode=true&characterEncoding=UTF-8&serverTimezone=GMT%2b8&&zeroDateTimeBehavior=convertToNull", "medical_sys_pro", "YdZ2x6ZaaidKY2YY");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 目标库
	 *
	 * @return
	 */
	private Connection toDestination() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			return DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/test?useUnicode=true&characterEncoding=UTF-8&serverTimezone=GMT%2b8&&zeroDateTimeBehavior=convertToNull", "root", "wkf@2022");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 拼装 列名
	 * @param resultSet
	 * @return
	 * @throws SQLException
	 */
	private static String getColumnNames(ResultSet resultSet) throws SQLException {
		StringBuffer columns = new StringBuffer();
		int size = resultSet.getMetaData().getColumnCount();
		String comma = "";
		for (int i = 0; i < size; i++) {
			String columnName = resultSet.getMetaData().getColumnName(i+1);
			columns.append(comma).append("`"+columnName+"`");
			comma = ",";
		}
		return columns.toString();
	}

	/**
	 * 拼装 展位符号
	 * @param resultSet
	 * @return
	 * @throws SQLException
	 */
	private static String getTempSign(ResultSet resultSet) throws SQLException {
		StringBuffer sign = new StringBuffer();
		int size = resultSet.getMetaData().getColumnCount();
		String comma = "";
		for (int i = 0; i < size; i++) {
			sign.append(comma).append("?");
			comma = ",";
		}
		return sign.toString();
	}



	public void transferData(String tableName){
		//源数据库链接
		Connection formConn=null;
		Statement statement=null;
		ResultSet resultSet=null;
		//目标数据库链接
		Connection toConn=null;
		PreparedStatement toStatement=null;

		try {
			//获取源数据库
			 formConn=formConn();
			 statement=formConn.createStatement();
			 resultSet=statement.executeQuery("select * from "+tableName);

			//结果集(列)获取到的长度
			int size = resultSet.getMetaData().getColumnCount();
			//拼接insert into
			StringBuffer sbf = new StringBuffer();
			//列名
			String columns = getColumnNames(resultSet);
			//占位符号
			String signs = getTempSign(resultSet);
			sbf.append("insert into ");
			sbf.append(tableName);
			sbf.append("("+columns+")");
			sbf.append(" values ");
			sbf.append("("+signs+")");

			System.out.println(sbf);

			//获取 目标 的数据库链接
			toConn = toDestination();
			toStatement = toConn.prepareStatement(sbf.toString());
			//取消事务(不写入日志)
			toConn.setAutoCommit(false);

			//完成条数
			int count = 0;
			int num = 0;
			long start = System.currentTimeMillis();
			while (resultSet.next()) {
				++count;
				for (int i = 1; i <= size; i++) {
					toStatement.setObject(i, resultSet.getObject(i));
				}
				//将预先语句存储起来，这里还没有向数据库插入
				toStatement.addBatch();
				// 1000条 提交一次
				if (count % 1000 == 0) {
					++num;
					toStatement.executeBatch();
//					System.out.println("第" + num + "次提交,耗时:" + (System.currentTimeMillis() - start) / 1000.0 + "s");
				}
			}
//			System.out.println(toStatement.toString());
			//没达到一千条也提交一次
			toStatement.executeBatch();
			//提交
			toConn.commit();
			//恢复事务
			toConn.setAutoCommit(true);
			System.out.println("完成 " + count + " 条数据,耗时:" + (System.currentTimeMillis() - start) / 1000.0 + "s");
		}catch (Exception e){
			e.printStackTrace();
		}finally {
			//关闭资源
			close(formConn, statement, resultSet);
			close(toConn, toStatement, null);
		}


	}

	public void close(Connection conn, Statement stmt, ResultSet rs) {

		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 重新创建 表
	 * @param tableName
	 * @param createSql
	 * @return
	 */
	public boolean reCreateTable(String tableName,String createSql){
		Connection conn=null;
		Statement statement=null;
		ResultSet resultSet=null;
		try {
			conn = toDestination();
			statement= conn.createStatement();
			statement.execute(SqlKeywordConstant.dropTable+tableName);
			int i = statement.executeUpdate(createSql);
			if (i>0){
				return true;
			}
		}catch (SQLException e){
			e.printStackTrace();
		}finally {
			close(conn, statement, resultSet);
		}
		return false;
	};

	/**
	 * 获取 表的创建sql
	 * @param tableName
	 * @return
	 */
	public String getCreateSql (String tableName)   {
		Connection formConn=null;
		Statement statement=null;
		ResultSet resultSet=null;
		String createSql="";
		try {
			 formConn=formConn();
			 statement=formConn.createStatement();;
			 resultSet=statement.executeQuery("show create table "+tableName);
			while (resultSet.next()){
				System.out.println(resultSet.getObject(1));
				createSql=resultSet.getObject(2).toString();
			}
		}catch (SQLException e){
			e.printStackTrace();
		}finally {
			close(formConn, statement, resultSet);
		}
		return createSql;
	};

	public static void main(String[] args) {
		DataMigration dataMigration = new DataMigration();
		//需要同步的表
		List<String>  tableNames= DataBaseTableConstant.getBaseTableNames();
		try {
			long start = System.currentTimeMillis();
			for (String tableName:tableNames) {
				//获取数据库 创建sql
				String createSql = dataMigration.getCreateSql(tableName);

				//先删除目标库 的表 然后重新创建表
				dataMigration.reCreateTable(tableName,createSql);

				//同步数据
				dataMigration.transferData(tableName);
				System.out.println("数据,耗时:" + (System.currentTimeMillis() - start) / 1000.0 + "s");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			//关闭链接
		}
	}
}



