package com.healthappy.modules.monitor.rest;

import com.healthappy.modules.monitor.service.VisitsService;
import com.healthappy.utils.R;
import com.healthappy.utils.RequestHolder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Zheng Jie
 * @date 2018-12-13
 */
@RestController
@RequestMapping("/api/visits")
@Api(tags = "系统:访问记录管理")
public class VisitsController {

    private final VisitsService visitsService;

    public VisitsController(VisitsService visitsService) {
        this.visitsService = visitsService;
    }

    @PostMapping
    @ApiOperation("创建访问记录")
    public R create() {
        visitsService.count(RequestHolder.getHttpServletRequest());
        return R.ok();
    }

    @GetMapping
    @ApiOperation("查询")
    public R get() {
        return R.ok(visitsService.get());
    }

    @GetMapping(value = "/chartData")
    @ApiOperation("查询图表数据")
    public R getChartData() {
        return R.ok(visitsService.getChartData());
    }
}
