package com.healthappy.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @Classname PreTenantConfigProperties
 * @Description 多租户动态配置
 * @Author Created by Lihaodong (alias:小东啊) lihaodongmail@163.com
 * @Date 2019-08-09 23:36
 * @Version 1.0
 */
@Data
@Component
@ConfigurationProperties(prefix = "tenant")
public class HealthTenantConfigProperties {

    /**
     * 多租户字段名称
     */
    private String column = "tenant_id";
    /**
     * 多租户的数据表集合
     */
    private List<String> ignoreTenantTables = new ArrayList<>();

    /**
     * 多租户的排除mapper集合
     */
    private List<String> ignoreTenantMappers = new ArrayList<>();
}
