package com.healthappy.config;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import com.healthappy.modules.system.domain.vo.UploadCacheVo;
import com.healthappy.utils.RedisUtil;
import com.healthappy.utils.SecurityConstants;
import com.healthappy.utils.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA. Created by yutang 2021/9/14 0014  9:39 Description:webSocket服务类
 */
@Slf4j
@Component
@ServerEndpoint(value = "/webSocket/{name}")
public class WebSocketServer {

	/** 在线人数 */
	private static int onlineCount = 0;

	/**
	 * 与某个客户端的连接对话，需要通过它来给客户端发送消息
	 */
	private Session session;

	/**
	 * 标识当前连接客户端的用户名
	 */
	private String name;


	/**
	 *  用于存所有的连接服务的客户端，这个对象存储是安全的
	 */
	private static ConcurrentHashMap<String, WebSocketServer> webSocketSet = new ConcurrentHashMap<>();

	@OnOpen
	public void onOpen(Session session, EndpointConfig config, @PathParam(value = "name") String name) {
		this.session = session;
		this.name = name;

		if (webSocketSet.containsKey(name)) {
			webSocketSet.remove(name);
			webSocketSet.put(name, this);
		} else {
			webSocketSet.put(name, this);
			addOnlineCount();
			log.info(name + "，已上线！");
		}

		try {
			webSocketSet.get(name).session.getBasicRemote().sendText("连接成功");
			log.info("连接成功,发送人:{}", name);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@OnClose
	public void onClose() {
		webSocketSet.remove(this.name);

		if (webSocketSet.containsKey(this.name)) {
			webSocketSet.remove(this.name);
			subOnlineCount();
			log.info(this.name + "，已下线！");
		}

	}

	@OnError
	public void onError(Session session, Throwable throwable) {
		log.error("用户" + this.name + "发生了错误，具体如下：" + throwable.getMessage());
	}

	/**
	 * 服务器接收客户端发来的消息
	 * @param message 消息
	 * @param session 会话session
	 */
	@OnMessage
	public void onMessage(String message, Session session) {
		log.info("服务器收到了用户" + name + "发来的消息：" + message);
	}

	/**
	 * 获取在线人数
	 * @return 在线人数
	 */
	public static int getOnlineCount() {
		return onlineCount;
	}

	/**
	 * 团检备单-群发
	 * @param message
	 */
	public void groupSending(String message, String userName) {
		log.info("给【{}】账号群发了团检备单消息", userName);
		sendMessage(userName, message, WebSocketConfig.BALL_CHECK_SOCKET);
	}

	/**
	 * 发送命令给客户端
	 * @author YJ
	 * @date 2022/2/14 14:56
	 * @param userName 用户名
	 * @param message 消息主体
	 * @param command 命令（指令）
	 */
	public synchronized void sendMessage(String userName, String message, String command) {
		log.info("给【{}】账号发送了{}消息:{}", userName, command, message);
		for (String name : webSocketSet.keySet()) {
			try {
				if (StrUtil.contains(name, userName)) {
					WebSocketJson socketJson = new WebSocketJson();
					socketJson.setData(message);
					socketJson.setFlag(name);
					socketJson.setComm(command);
					webSocketSet.get(name).session.getBasicRemote().sendText(new JSONObject(socketJson).toString());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 指定发送
	 * @param name
	 * @param message
	 */
	public synchronized void appointSending(String name, String message) {
		String regex = "\\{(.*?)}";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(message);
		List<String> strList = new ArrayList<>();
		while (matcher.find()) {
			strList.add(matcher.group(1));
		}
		String fileName = strList.get(0);
		String schedule = strList.get(1);
		try {
			String key = SecurityConstants.BALL_CHECK_UPLOAD + SecurityUtils.getUsername();
			List<UploadCacheVo> cacheList = RedisUtil.hasKey(key) ? RedisUtil.get(key) : new ArrayList<>();
			if (Convert.toInt(schedule) == 100) {
				cacheList = cacheList.stream().filter(vo -> !vo.getFileName().equals(fileName))
						.collect(Collectors.toList());
			} else {
				if (RedisUtil.hasKey(key) && cacheList.stream()
						.anyMatch(cacheVo -> cacheVo.getFileName().equals(fileName))) {
					cacheList.forEach(cacheVo -> {
						if (cacheVo.getFileName().equals(fileName)) {
							List<String> history = cacheVo.getHistory();
							history.add(message);
							cacheVo.setHistory(history);
						}
					});
				} else {
					UploadCacheVo cacheVo = new UploadCacheVo();
					cacheVo.setFileName(fileName);
					cacheVo.setHistory(Collections.singletonList(message));
					cacheList.add(cacheVo);
				}
			}
			RedisUtil.set(key, cacheList);
			this.groupSending(message, name);
			log.info("发送人:{},消息内容:{}", name, message);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static synchronized void subOnlineCount() {
		onlineCount--;
	}

	public static synchronized void addOnlineCount() {
		onlineCount++;
	}

	public static WebSocketServer get(String name) {
		if (webSocketSet.containsKey(name)) {
			return webSocketSet.get(name);
		}
		return null;
	}

	public static ConcurrentHashMap<String, WebSocketServer> getMap() {
		return webSocketSet;
	}

	public static boolean isOnline(String name) {
		return webSocketSet.containsKey(name);
	}

}
