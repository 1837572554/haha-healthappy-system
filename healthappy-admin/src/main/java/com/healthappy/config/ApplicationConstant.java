package com.healthappy.config;

/**
 * 应用常量类
 * @author Jevany
 * @date 2022/5/10 18:26
 */
public interface ApplicationConstant {
	/** 预约号源 开关 标示 */
	String APPOINT_SWITCH = "1";
}
