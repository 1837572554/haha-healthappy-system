package com.healthappy.config.databind;


/**
 * @author FGQ
 * Field字段
 */
public class FieldSetProperty {

    private String fieldName;

    private FieldBind fieldDict;

    public String getFieldName() {
        return this.fieldName;
    }


    public FieldBind getFieldDict() {
        return this.fieldDict;
    }

    public void setFieldName(String var1) {
        this.fieldName = var1;
    }
    public void setFieldDict(FieldBind var1) {
        this.fieldDict = var1;
    }

    public FieldSetProperty(String var1, FieldBind var3) {
        this.fieldName = var1;
        this.fieldDict = var3;
    }
}
