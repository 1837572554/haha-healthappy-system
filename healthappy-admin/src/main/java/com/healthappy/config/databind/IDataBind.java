package com.healthappy.config.databind;

import org.apache.ibatis.reflection.MetaObject;

/**
 * @author FGQ
 */
public interface IDataBind {

    void setMetaObject(FieldBind var1, Object var2, MetaObject var3);
}
