package com.healthappy.config.databind;

import java.lang.annotation.*;

/**
 * @author FGQ
 * 映射注解
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
public @interface FieldBind {

    String sharding() default "";

    String type() default "";

    String target();
}
