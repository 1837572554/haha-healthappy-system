package com.healthappy.config.databind;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import com.healthappy.modules.system.domain.vo.DictEnumVo;
import com.healthappy.modules.system.service.DictService;
import com.healthappy.utils.SpringUtil;
import org.apache.commons.collections.map.HashedMap;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @author FGQ
 * 字典实现类
 */
@Component
public class DataBind implements IDataBind, ApplicationListener<ContextRefreshedEvent> {

	public Map<String, Map<String, String>> ENUM_MAP = new ConcurrentHashMap<>();


	@Override
	public void setMetaObject(FieldBind var1, Object var2, MetaObject var3) {
		Map<String, Map<String, String>> map = getDictMap(false);
		Object originalObject = var3.getOriginalObject();
		Class<?> clz = originalObject.getClass();
		Field[] fields = ReflectUtil.getFields(clz);
		for (Field field : fields) {
			FieldBind fieldBind = field.getAnnotation(FieldBind.class);
			if (null != fieldBind) {
				String type = StrUtil.isNotBlank(fieldBind.type()) ? fieldBind.type() : field.getName();
				if (map.containsKey(type)) {
					var3.setValue(fieldBind.target(), map.get(type)
							.get(String.valueOf(ReflectUtil.getFieldValue(originalObject, field.getName()))));
				}
			}
		}
	}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		getDictMap(true);
	}


	/**
	 * 重置
	 */
	public void resetMap() {
		getDictMap(true);
	}


	/**
	 *
	 * @param isReload
	 * @return
	 */
	public Map<String, Map<String, String>> getDictMap(Boolean isReload) {

		List<DictEnumVo> list = new ArrayList<>();
		DictService dictService = SpringUtil.getBean(DictService.class);
		if (isReload) {
			list = dictService.reload();
		} else {
			list = dictService.dictConvertEnum();
		}
		Map<String, Map<String, String>> map = new HashedMap();
		if (CollUtil.isNotEmpty(list)) {
			map = list.stream().collect(Collectors.groupingBy(DictEnumVo::getName,
					Collectors.toMap(DictEnumVo::getLabel, DictEnumVo::getValue)));
		}
		if (CollUtil.isEmpty(list)) {
			list = dictService.reload();
			map = list.stream().collect(Collectors.groupingBy(DictEnumVo::getName,
					Collectors.toMap(DictEnumVo::getLabel, DictEnumVo::getValue)));
		}
		ENUM_MAP = map;
		return map;
	}

}
