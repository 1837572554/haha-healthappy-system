package com.healthappy.config.databind;

import cn.hutool.core.collection.CollUtil;
import org.apache.ibatis.executor.resultset.DefaultResultSetHandler;
import org.apache.ibatis.executor.resultset.ResultSetHandler;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.session.Configuration;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.Statement;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

/**
 * @author FGQ
 * 字典映射字段
 */
@Intercepts({@Signature(
        type = ResultSetHandler.class,
        method = "handleResultSets",
        args = {Statement.class}
)})
public class DataInterceptor implements Interceptor {

    @Autowired
    public IDataBind dataBind;

    private static Map<Class<?>, List<FieldSetProperty>> concurrentHashMap  = new ConcurrentHashMap();
    private static Set<Class<?>> copyOnWriteArraySet = new CopyOnWriteArraySet();

    public DataInterceptor(IDataBind var2) {
        this.dataBind = var2;
    }

    /**
     * 执行的代码方法
     * @param var0
     * @param var1
     * @return
     * @throws Exception
     */
    public static Object invocation(Invocation var0, BiConsumer<MetaObject, FieldSetProperty> var1) throws Exception {
        List var2 = (List)var0.proceed();
        if (var2.isEmpty()) {
            return var2;
        } else {
            DefaultResultSetHandler var3 = (DefaultResultSetHandler)var0.getTarget();
            Field var4 = var3.getClass().getDeclaredField("mappedStatement");
            var4.setAccessible(true);
            MappedStatement var5 = (MappedStatement)var4.get(var3);
            Configuration var6 = var5.getConfiguration();
            Iterator var7 = var2.iterator();

            while(var7.hasNext()) {
                Object var8 = var7.next();
                if (null != var8 && !configuration(var6, var8, var1)) {
                    break;
                }
            }

            return var2;
        }
    }

    /**
     * 对class字段的映射
     * @param var0
     * @return
     */
    public static List<Field> field(Class<?> var0) {
        if (null == var0) {
            return null;
        } else {
            Field[] var1 = var0.getDeclaredFields();
            List var2 = (List)Arrays.stream(var1).filter((var0x) -> {
                return !Modifier.isStatic(var0x.getModifiers());
            }).filter((var0x) -> {
                return !Modifier.isTransient(var0x.getModifiers());
            }).collect(Collectors.toList());
            Class var3 = var0.getSuperclass();
            if (var3.equals(Object.class)) {
                return var2;
            } else {
                List var4 = field(var3);
                Iterator var5 = var4.iterator();

                while(var5.hasNext()) {
                    Field var6 = (Field)var5.next();
                    if (!var2.stream().anyMatch((var1x) -> {
                        return var1x.equals(var6.getName());
                    })) {
                        var2.add(var6);
                    }
                }

                return var2;
            }
        }
    }

    /**
     * 给字段赋值
     * @param var3
     * @param var4
     */
    public void setValue(MetaObject var3, FieldSetProperty var4) {
        String var5 = var4.getFieldName();
        Object var6 = var3.getValue(var5);
        if (null != var6) {
            boolean var9 = true;
            if (null != dataBind) {
                FieldBind var8 = var4.getFieldDict();
                if (null != var8) {
                    var9 = false;
                    dataBind.setMetaObject(var8, var6, var3);
                }
            }
            if (var9) {
                var3.setValue(var5, var6);
            }
        }

    }

    /**
     * 将数据添加到集合
     * @param var0
     * @return
     */
    public static List<FieldSetProperty> propertyList(Class<?> var0) {
        if (copyOnWriteArraySet.contains(var0)) {
            return null;
        } else {
            Object var1 = (List)concurrentHashMap.get(var0);
            if (null == var1) {
                if (var0.isAssignableFrom(HashMap.class)) {
                    copyOnWriteArraySet.add(var0);
                } else {
                    var1 = new ArrayList();
                    List var2 = field(var0);
                    Iterator var3 = var2.iterator();

                    while(true) {
                        Field var4;
                        FieldBind var6;
                        do {
                            if (!var3.hasNext()) {
                                if (((List)var1).isEmpty()) {
                                    copyOnWriteArraySet.add(var0);
                                } else {
                                    concurrentHashMap.put(var0, (List<FieldSetProperty>) var1);
                                }

                                return (List)var1;
                            }

                            var4 = (Field)var3.next();

                            var6 =var4.getAnnotation(FieldBind.class);
                        } while(null == var6);

                        ((List)var1).add(new FieldSetProperty(var4.getName(), var6));
                    }
                }
            }

            return (List)var1;
        }
    }

    public static boolean configuration(Configuration var0, Object var1, BiConsumer<MetaObject, FieldSetProperty> var2) {
        List var3 = propertyList(var1.getClass());
        if (CollUtil.isNotEmpty(var3)) {
            MetaObject var4 = var0.newMetaObject(var1);
            var3.parallelStream().forEach((var2x) -> {
                var2.accept(var4, (FieldSetProperty) var2x);
            });
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Object intercept(Invocation var1) throws Exception {
        return invocation(var1, this::setValue);
    }

    @Override
    public Object plugin(Object var1) {
        return var1 instanceof ResultSetHandler ? Plugin.wrap(var1, this) : var1;
    }

    @Override
    public void setProperties(Properties var1) {
    }
}

