package com.healthappy.config.databind;

import com.healthappy.exception.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

/**
 * @author FGQ
 * 配置类
 */
@Lazy
@Configuration(proxyBeanMethods = false)
public class MpmAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public DataInterceptor fieldDecryptInterceptor(@Lazy @Autowired(required = false) IDataBind var2){
        if(null == var2){
            return null;
        }
        return new DataInterceptor(var2);
    }
}
