package com.healthappy.config;

/**
 * @author ：LionCity
 * @date ：Created in 2020-04-10 15:47
 * @description：自动注入时间处理
 * @modified By：
 * @version:
 */

import cn.hutool.core.annotation.AnnotationUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.service.TidSeedService;
import com.healthappy.utils.SecurityUtils;
import com.healthappy.utils.SpringUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

/**
 * 处理新增和更新的基础数据填充，
 */
@Slf4j
@Component
public class MetaHandler implements MetaObjectHandler {

    /**
     * 新增数据执行
     *
     * @param metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        Timestamp time = new Timestamp(System.currentTimeMillis());
        try {
            Class<?> aClass = metaObject.getOriginalObject().getClass();
            SeedIdGenerator idGenerator = AnnotationUtil.getAnnotation(aClass, SeedIdGenerator.class);
            if(null != idGenerator){
                TableName tableName = AnnotationUtil.getAnnotation(aClass, TableName.class);
                String lineCaseName =  null == tableName  ? StrUtil.toUnderlineCase(aClass.getSimpleName()) : tableName.value();
                this.setFieldValByName("id", SpringUtil.getBean(TidSeedService.class).createASeedPrimaryKey(lineCaseName),metaObject);
            }
            if (metaObject.hasSetter("createTime")) {
                log.debug("自动插入 createTime");
                this.setFieldValByName("createTime", time, metaObject);
            }
            if (metaObject.hasSetter("updateTime")) {
                log.debug("自动插入 updateTime");
                this.setFieldValByName("updateTime", time, metaObject);
            }
            if (metaObject.hasSetter("delFlag")) {
                log.debug("自动插入 delFlag");
                this.setFieldValByName("delFlag", "0", metaObject);
            }
            if(metaObject.hasSetter("updateBy")){
                log.debug("自动插入 updateBy");
                this.setFieldValByName("updateBy", SecurityUtils.getUserId(),metaObject);
            }
            if(metaObject.hasSetter("createBy")){
                log.debug("自动插入 createBy");
                this.setFieldValByName("createBy",SecurityUtils.getUserId(),metaObject);
            }
            if(metaObject.hasSetter("tenantId") &&  null == metaObject.getValue("tenantId")){
                log.debug("自动插入 tenantId");
//                System.out.println(SecurityUtils.getTenantId());
                this.setFieldValByName("tenantId", SecurityUtils.getTenantId(),metaObject);
            }
        } catch (Exception e) {
            log.error("插入自动注入失败:{}", e);
            throw new BadRequestException("插入自动注入失败:"+e.getMessage());
        }
    }

    /**
     * 更新数据执行
     *
     * @param metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        Timestamp time = new Timestamp(System.currentTimeMillis());
        try {
            if (metaObject.hasSetter("updateTime")) {
                log.debug("自动插入 updateTime");
                this.setFieldValByName("updateTime",time , metaObject);
            }
            if(metaObject.hasSetter("updateBy")){
                log.debug("自动插入 updateBy");
                this.setFieldValByName("updateBy", SecurityUtils.getUserId(),metaObject);
            }
        } catch (Exception e) {
            log.error("更新自动注入失败:{}", e);
            throw new BadRequestException("更新自动注入失败:"+e.getMessage());
        }
    }

}
