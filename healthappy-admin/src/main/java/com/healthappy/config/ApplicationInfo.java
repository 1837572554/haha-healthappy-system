package com.healthappy.config;

import com.healthappy.utils.IpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

/**
 * 打印信息
 * @author FGQ
 */
@Slf4j
public class ApplicationInfo {
    /**
     * 启动成功之后，打印项目信息
     */
    public static void print(ConfigurableApplicationContext context){
        ConfigurableEnvironment environment = context.getEnvironment();

        // 项目profile
        String profileActive = environment.getProperty("spring.profiles.active");
        // 项目路径
        String contextPath = "/";
        // 项目端口
        String port = environment.getProperty("server.port");

        log.info("profileActive : {}",profileActive);
        log.info("contextPath : {}",contextPath);
        log.info("port : {}",port);

        String homeUrl = "http://" + IpUtil.getLocalIP() + ":" + port + contextPath;
        String swaggerUrl = "http://" + IpUtil.getLocalIP() + ":" + port + contextPath + "doc.html";
        log.info("home:{}",homeUrl);
        log.info("docs:{}",swaggerUrl);
        log.info("启动成功");
    }
}
