package com.healthappy.config.handler;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.tenant.TenantHandler;
import com.healthappy.config.HealthTenantConfigProperties;
import com.healthappy.config.TenantConstant;
import com.healthappy.aspect.IgnoreTenantUtil;
import com.healthappy.utils.RequestHolder;
import com.healthappy.utils.SecurityUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.StringValue;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * @Classname HealthTenantHandler
 * @Description 租户处理器 -主要实现mybatis-plus https://mp.baomidou.com/guide/tenant.html
 * @Author ZhangJiaJun
 * @Date 2020-07-01 23:34
 * @Version 1.0
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class HealthTenantHandler implements TenantHandler {

    private final HealthTenantConfigProperties configProperties;

    static {
        SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);
    }

    /**
     * 租户Id
     *获取租户 ID 值表达式
     * @return
     */
    @Override
    public Expression getTenantId(boolean where) {
        return  new StringValue(StrUtil.isBlank(SecurityUtils.getTenantId()) ? TenantConstant.DEFAULT_TENANT_ID : SecurityUtils.getTenantId());
     }

    /**
     * 租户字段名
     *
     * @return
     */
    @Override
    public String getTenantIdColumn() {
        return configProperties.getColumn();
    }

    /**
     * 根据表名判断是否进行过滤
     * 忽略掉一些表：如租户表（sys_tenant）本身不需要执行这样的处理
     *
     * @param tableName 忽略租户的表
     * @return
     */
    @Override
    public boolean doTableFilter(String tableName) {
        boolean ignoreFlag = false;
        try {
            HttpServletRequest request = RequestHolder.getHttpServletRequest();
            ignoreFlag = IgnoreTenantUtil.getIgnoreTenantUtil().contains(request.getRequestURL().toString());
        } catch (Exception ignored) {
        }
        return configProperties.getIgnoreTenantTables().contains(tableName)
                || Objects.equals(SecurityUtils.getTenantId(), TenantConstant.DEFAULT_TENANT_ID) || ignoreFlag
                || IgnoreTenantUtil.ignoreTenantLogo;
    }

}
