package com.healthappy.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @desc: 图片配置类
 * @author: YJ
 * @date: 2021-12-01 09:41
 **/
//注册到Spring容器
@Component
@Data
@PropertySource("classpath:imageConfig.properties")
public class ImageConfig {
    /**
     * 图片保存根路径
     */
    @Value("${image.savePath}")
    private String savePath;
}
