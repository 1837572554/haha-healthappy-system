package com.healthappy.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * @author: ZhangHouYing
 * @date: 2019-08-24 15:44
 */
@Configuration
public class WebSocketConfig {

	/**
	 * 危急值通知 标识
	 */
	public static final String CRITICAL_NOTIFICATION = "critical";

	/** 团检备单 标识 */
	public static final String BALL_CHECK_SOCKET = "single";

	/** 打印 标识 */
	public static final String PRINT_NOTIFICATION = "print";

	/** 打印失败 标识 */
	public static final String PRINT_NOTIFICATION_FAILED = "print_failed";

	@Bean
	public ServerEndpointExporter serverEndpointExporter() {
		return new ServerEndpointExporter();
	}

}
