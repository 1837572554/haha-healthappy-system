package com.healthappy.config;

import java.lang.annotation.*;

/**
 * @author FGQ
 * 映射注解
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface SeedIdGenerator {

}
