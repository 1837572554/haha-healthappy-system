package com.healthappy.config;

import lombok.Data;

/**
 * JSON格式
 * @author FGQ
 */
@Data
public class WebSocketJson {

    private String comm;

    private String flag;

    private String data;

}
