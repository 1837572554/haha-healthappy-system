package com.healthappy.logging.rest;

import com.healthappy.logging.aop.log.Log;
import com.healthappy.logging.service.LogService;
import com.healthappy.logging.service.dto.LogQueryCriteria;
import com.healthappy.utils.R;
import com.healthappy.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author hupeng
 * @date 2018-11-24
 */
@RestController
@RequestMapping("/api/logs")
@Api(tags = "监控：日志管理")
@SuppressWarnings("unchecked")
public class LogController {

    private final LogService logService;

    public LogController(LogService logService) {
        this.logService = logService;
    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('admin','log:list')")
    public void download(HttpServletResponse response, LogQueryCriteria criteria) throws IOException {
        criteria.setLogType("INFO");
        logService.download(logService.queryAll(criteria), response);
    }

    @Log("导出错误数据")
    @ApiOperation("导出错误数据")
    @GetMapping(value = "/error/download")
    @PreAuthorize("@el.check('admin','log:list')")
    public void errorDownload(HttpServletResponse response, LogQueryCriteria criteria) throws IOException {
        criteria.setLogType("ERROR");
        logService.download(logService.queryAll(criteria), response);
    }

    @GetMapping
    @ApiOperation("日志查询")
    @PreAuthorize("@el.check('admin','log:list')")
    public R getLogs(LogQueryCriteria criteria, Pageable pageable) {
        criteria.setLogType("INFO");
        criteria.setType(0);
        return R.ok(logService.queryAll(criteria, pageable));
    }

    @GetMapping(value = "/mlogs")
    @PreAuthorize("@el.check('admin','log:list')")
    public R getApiLogs(LogQueryCriteria criteria, Pageable pageable) {
        criteria.setLogType("INFO");
        criteria.setType(1);
        return R.ok(logService.findAllByPageable(criteria.getBlurry(), pageable));
    }

    @GetMapping(value = "/user")
    @ApiOperation("用户日志查询")
    public R getUserLogs(LogQueryCriteria criteria, Pageable pageable) {
        criteria.setLogType("INFO");
        criteria.setBlurry(SecurityUtils.getUsername());
        return R.ok(logService.queryAllByUser(criteria, pageable));
    }

    @GetMapping(value = "/error")
    @ApiOperation("错误日志查询")
    @PreAuthorize("@el.check('admin','logError:list')")
    public R getErrorLogs(LogQueryCriteria criteria, Pageable pageable) {
        criteria.setLogType("ERROR");
        return R.ok(logService.queryAll(criteria, pageable));
    }

    @GetMapping(value = "/error/{id}")
    @ApiOperation("日志异常详情查询")
    @PreAuthorize("@el.check('admin','logError:detail')")
    public R getErrorLogs(@PathVariable Long id) {
        return R.ok(logService.findByErrDetail(id));
    }

    @DeleteMapping(value = "/del/error")
    @Log("删除所有ERROR日志")
    @ApiOperation("删除所有ERROR日志")
    @PreAuthorize("@el.check('admin','logError:remove')")
    public R delAllByError() {
        logService.delAllByError();
        return R.ok();
    }

    @DeleteMapping(value = "/del/info")
    @Log("删除所有INFO日志")
    @ApiOperation("删除所有INFO日志")
    @PreAuthorize("@el.check('admin','logInfo:remove')")
    public R delAllByInfo() {
        logService.delAllByInfo();
        return R.ok();
    }
}
