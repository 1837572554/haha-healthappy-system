package com.healthappy.modules.shop.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.shop.domain.Picture;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author hupeng
 * @date 2020-05-13
 */
@Repository
@Mapper
public interface PictureMapper extends CoreMapper<Picture> {

}
