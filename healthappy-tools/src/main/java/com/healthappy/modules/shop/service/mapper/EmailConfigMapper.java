package com.healthappy.modules.shop.service.mapper;

import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.shop.domain.EmailConfig;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author hupeng
 * @date 2020-05-13
 */
@Repository
@Mapper
public interface EmailConfigMapper extends CoreMapper<EmailConfig> {

}
