package com.healthappy.modules.shop.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import cn.hutool.poi.excel.StyleSet;
import com.healthappy.common.service.impl.BaseServiceImpl;
import com.healthappy.common.utils.QueryHelpPlus;
import com.healthappy.dozer.service.IGenerator;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.shop.domain.LocalStorage;
import com.healthappy.modules.shop.service.LocalStorageService;
import com.healthappy.modules.shop.service.dto.LocalStorageDto;
import com.healthappy.modules.shop.service.dto.LocalStorageQueryCriteria;
import com.healthappy.modules.shop.service.mapper.LocalStorageMapper;
import com.healthappy.utils.FileUtil;
import com.healthappy.utils.SecurityUtils;
import com.healthappy.utils.StringUtils;
import com.github.pagehelper.PageInfo;
import org.apache.poi.ss.usermodel.Font;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

/**
 * @author hupeng
 * @date 2020-05-13
 */
@Service
//@CacheConfig(cacheNames = "localStorage")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class LocalStorageServiceImpl extends BaseServiceImpl<LocalStorageMapper, LocalStorage> implements LocalStorageService {

    private final IGenerator generator;
    @Value("${file.path}")
    private String path;

    @Value("${file.localUrl}")
    private String localUrl;

    @Value("${file.maxSize}")
    private long maxSize;

    public LocalStorageServiceImpl(IGenerator generator) {
        this.generator = generator;
    }

    @Override
    //@Cacheable
    public Map<String, Object> queryAll(LocalStorageQueryCriteria criteria, Pageable pageable) {
        getPage(pageable);
        PageInfo<LocalStorage> page = new PageInfo<>(baseMapper.selectList(QueryHelpPlus.getPredicate(LocalStorage.class, criteria)));
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", generator.convert(page.getList(), LocalStorageDto.class));
        map.put("totalElements", page.getTotal());
        return map;
    }


    @Override
    //@Cacheable
    public List<LocalStorageDto> queryAll(LocalStorageQueryCriteria criteria) {
        return generator.convert(baseMapper.selectList(QueryHelpPlus.getPredicate(LocalStorage.class, criteria)), LocalStorageDto.class);
    }

    @Override
    public LocalStorageDto findById(Long id) {
        LocalStorage localStorage = this.getById(id);
        return generator.convert(localStorage, LocalStorageDto.class);
    }

    @Override
    public LocalStorageDto create(String name, MultipartFile multipartFile, boolean b) {
        FileUtil.checkSize(maxSize, multipartFile.getSize());
        String suffix = FileUtil.getExtensionName(multipartFile.getOriginalFilename());
        String type = FileUtil.getFileType(suffix);
        File file = FileUtil.upload(multipartFile, path + type + File.separator);
        if (ObjectUtil.isNull(file)) {
            throw new BadRequestException("上传失败");
        }
        try {
            name = StringUtils.isBlank(name) ? FileUtil.getFileNameNoEx(multipartFile.getOriginalFilename()) : name;
            LocalStorage localStorage = new LocalStorage(
                    file.getName(),
                    name,
                    suffix,
                    file.getPath(),
                    type,
                    FileUtil.getSize(multipartFile.getSize()),
                    SecurityUtils.getUsername()
            );
            this.save(localStorage);
            return generator.convert(localStorage, LocalStorageDto.class);
        } catch (Exception e) {
            FileUtil.del(file);
            throw e;
        }
    }


    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            LocalStorage storage = this.getById(id);
            FileUtil.del(storage.getPath());
            this.removeById(id);
        }
    }


    @Override
    public void download(List<LocalStorageDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (LocalStorageDto localStorage : all) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("文件真实的名称", localStorage.getRealName());
            map.put("文件名", localStorage.getName());
            map.put("后缀", localStorage.getSuffix());
//            map.put("路径", localStorage.getPath());
            map.put("类型", localStorage.getType());
            map.put("大小", localStorage.getSize());
            map.put("操作人", localStorage.getOperate());
            map.put("创建日期", localStorage.getCreateTime());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    @Override
    public void updateLocalStorage(LocalStorageDto resources) {
        LocalStorage localStorage = this.getById(resources.getId());
        BeanUtils.copyProperties(resources, localStorage);
        this.saveOrUpdate(localStorage);
    }

    @Override
    public String uploadExecl(List<Map<String, Object>> mapList,String fileName) {
        mapList = mapList.stream().map(map ->{
            Map<String, Object> objectMap = new LinkedHashMap<>();
            map.keySet().forEach(key ->{
                if(!StrUtil.containsAny(key,"部门ID","体检分类ID","危害因素Ids","工种ID","体检套餐ID","体检套餐价格","附加套餐ID","附加套餐价格","附加项目Ids","体检类别ID")){
                    objectMap.put(key,map.get(key));
                }
            });
            return objectMap;
        }).collect(Collectors.toList());
        ExcelWriter writer = ExcelUtil.getWriter(path + fileName + ".xlsx");
        Font font = writer.createFont();
        font.setBold(true);
        writer.getCellStyle().setFont(font);
        writer.write(mapList,true);
        writer.setColumnWidth(0,15)
                .setColumnWidth(1,10)
                .setColumnWidth(2,10)
                .setColumnWidth(3,10)
                .setColumnWidth(4,15)
                .setColumnWidth(5,20)
                .setColumnWidth(6,10)
                .setColumnWidth(7,30)
                .setColumnWidth(8,30)
                .setColumnWidth(9,10)
                .setColumnWidth(10,30)
                .setColumnWidth(11,30)
                .setColumnWidth(12,30)
                .setColumnWidth(13,30)
                .setColumnWidth(14,20)
                .setColumnWidth(15,30)
                .setColumnWidth(16,10)
                .setColumnWidth(17,10)
                .setColumnWidth(18,30)
                .setColumnWidth(19,30)
                .setColumnWidth(20,30)
                .setColumnWidth(21,20)
                .setColumnWidth(22,30)
                .setColumnWidth(23,50)
                .setRowHeight(-1,15);
        writer.close();
        return localUrl + "/file/" + fileName  + ".xlsx";
    }


    public static void main(String[] args) {

    }
}
