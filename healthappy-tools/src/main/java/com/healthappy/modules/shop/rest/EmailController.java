package com.healthappy.modules.shop.rest;

import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.shop.domain.EmailConfig;
import com.healthappy.modules.shop.domain.vo.EmailVo;
import com.healthappy.modules.shop.service.EmailConfigService;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 发送邮件
 * @author 郑杰
 * @date 2018/09/28 6:55:53
 */
@RestController
@RequestMapping("api/email")
@Api(tags = "工具：邮件管理")
public class EmailController {

    private final EmailConfigService emailService;

    public EmailController(EmailConfigService emailService) {
        this.emailService = emailService;
    }

    @GetMapping
    public R get() {
        return R.ok(emailService.find());
    }

    @Log("配置邮件")
    @PutMapping
    @ApiOperation("配置邮件")
    public R emailConfig(@Validated @RequestBody EmailConfig emailConfig) {
        emailService.update(emailConfig, emailService.find());
        return R.ok();
    }

    @Log("发送邮件")
    @PostMapping
    @ApiOperation("发送邮件")
    public R send(@Validated @RequestBody EmailVo emailVo) throws Exception {
        emailService.send(emailVo, emailService.find());
        return R.ok();
    }
}
