package com.healthappy.modules.shop.rest;

import com.healthappy.dozer.service.IGenerator;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.shop.domain.Picture;
import com.healthappy.modules.shop.service.PictureService;
import com.healthappy.modules.shop.service.dto.PictureDto;
import com.healthappy.modules.shop.service.dto.PictureQueryCriteria;
import com.healthappy.utils.R;
import com.healthappy.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author hupeng
 * @date 2018/09/20 14:13:32
 */
@RestController
@RequestMapping("/api/pictures")
@Api(tags = "工具：免费图床管理")
public class PictureController {
    private final IGenerator generator;
    private final PictureService pictureService;

    public PictureController(IGenerator generator, PictureService pictureService) {
        this.generator = generator;
        this.pictureService = pictureService;
    }

    @Log("查询图片")
    @PreAuthorize("@el.check('pictures:list')")
    @GetMapping
    @ApiOperation("查询图片")
    public R getRoles(PictureQueryCriteria criteria, Pageable pageable) {
        return R.ok(pictureService.queryAll(criteria, pageable));
    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('pictures:list')")
    public void download(HttpServletResponse response, PictureQueryCriteria criteria) throws IOException {
        pictureService.download(generator.convert(pictureService.queryAll(criteria), PictureDto.class), response);
    }

    @Log("上传图片")
    @PreAuthorize("@el.check('pictures:add')")
    @PostMapping
    @ApiOperation("上传图片")
    public R upload(@RequestParam MultipartFile file) {
        String userName = SecurityUtils.getUsername();
        Picture picture = pictureService.upload(file, userName);
        return R.ok();
    }

    @Log("同步图床数据")
    @ApiOperation("同步图床数据")
    @PostMapping(value = "/synchronize")
    public R synchronize() {
        pictureService.synchronize();
        return R.ok();
    }

    @Log("多选删除图片")
    @ApiOperation("多选删除图片")
    @PreAuthorize("@el.check('pictures:delete')")
    @DeleteMapping
    public R deleteAll(@RequestBody Long[] ids) {
        pictureService.deleteAll(ids);
        return R.ok();
    }
}
