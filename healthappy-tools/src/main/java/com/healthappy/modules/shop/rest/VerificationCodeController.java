package com.healthappy.modules.shop.rest;

import com.healthappy.modules.shop.domain.VerificationCode;
import com.healthappy.modules.shop.domain.vo.EmailVo;
import com.healthappy.modules.shop.service.EmailConfigService;
import com.healthappy.modules.shop.service.VerificationCodeService;
import com.healthappy.utils.R;
import com.healthappy.utils.ShopConstant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

/**
 * @author hupeng
 * @date 2018-12-26
 */
@RestController
@RequestMapping("/api/code")
@Api(tags = "工具：验证码管理")
public class VerificationCodeController {

    private final VerificationCodeService verificationCodeService;

    private final EmailConfigService emailService;

    public VerificationCodeController(VerificationCodeService verificationCodeService, EmailConfigService emailService) {
        this.verificationCodeService = verificationCodeService;
        this.emailService = emailService;
    }

    @PostMapping(value = "/resetEmail")
    @ApiOperation("重置邮箱，发送验证码")
    public R resetEmail(@RequestBody VerificationCode code) throws Exception {
        code.setScenes(ShopConstant.RESET_MAIL);
        EmailVo emailVo = verificationCodeService.sendEmail(code);
        emailService.send(emailVo, emailService.find());
        return R.ok();
    }

    @PostMapping(value = "/email/resetPass")
    @ApiOperation("重置密码，发送验证码")
    public R resetPass(@RequestParam String email) throws Exception {
        VerificationCode code = new VerificationCode();
        code.setType("email");
        code.setValue(email);
        code.setScenes(ShopConstant.RESET_MAIL);
        EmailVo emailVo = verificationCodeService.sendEmail(code);
        emailService.send(emailVo, emailService.find());
        return R.ok();
    }

    @GetMapping(value = "/validated")
    @ApiOperation("验证码验证")
    public R validated(VerificationCode code) {
        verificationCodeService.validated(code);
        return R.ok();
    }
}
