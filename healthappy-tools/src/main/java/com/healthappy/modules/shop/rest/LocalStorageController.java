package com.healthappy.modules.shop.rest;

import com.healthappy.dozer.service.IGenerator;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.shop.service.LocalStorageService;
import com.healthappy.modules.shop.service.dto.LocalStorageDto;
import com.healthappy.modules.shop.service.dto.LocalStorageQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * @author hupeng
 * @date 2020-05-13
 */
@AllArgsConstructor
@Api(tags = "文件管理")
@RestController
@RequestMapping("/api/localStorage")
public class LocalStorageController {

    private final LocalStorageService localStorageService;
    private final IGenerator generator;


    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('admin','localStorage:list')")
    public void download(HttpServletResponse response, LocalStorageQueryCriteria criteria) throws IOException {
        localStorageService.download(generator.convert(localStorageService.queryAll(criteria), LocalStorageDto.class), response);
    }

    @GetMapping
    @Log("查询文件")
    @ApiOperation("查询文件")
    @PreAuthorize("@el.check('admin','localStorage:list')")
    public R getLocalStorages(LocalStorageQueryCriteria criteria, Pageable pageable) {
        return R.ok(localStorageService.queryAll(criteria, pageable));
    }

    @PostMapping
    @Log("新增文件")
    @ApiOperation("新增文件")
    @PreAuthorize("@el.check('admin','localStorage:add')")
    public R create(@RequestParam String name, @RequestParam("file") MultipartFile file) {
        return R.ok(localStorageService.create(name, file, false));
    }

    @PutMapping
    @Log("修改文件")
    @ApiOperation("修改文件")
    @PreAuthorize("@el.check('admin','localStorage:edit')")
    public R update(@Validated @RequestBody LocalStorageDto resources) {
        localStorageService.updateLocalStorage(resources);
        return R.ok();
    }

    @Log("删除文件")
    @ApiOperation("删除文件")
    @PreAuthorize("@el.check('admin','localStorage:delete')")
    @DeleteMapping
    public R deleteAll(@RequestBody Long[] ids) {
        Arrays.asList(ids).forEach(id -> {
            localStorageService.removeById(id);
        });
        return R.ok();
    }
}
