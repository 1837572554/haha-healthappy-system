package com.healthappy.modules.shop.rest;

import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.shop.domain.QiniuConfig;
import com.healthappy.modules.shop.domain.QiniuContent;
import com.healthappy.modules.shop.service.QiNiuService;
import com.healthappy.modules.shop.service.dto.QiniuQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 发送邮件
 * @author 郑杰
 * @date 2018/09/28 6:55:53
 */
@Slf4j
@RestController
@RequestMapping("/api/qiNiuContent")
@Api(tags = "工具：七牛云存储管理")
public class QiniuController {

    private final QiNiuService qiNiuService;

    public QiniuController(QiNiuService qiNiuService) {
        this.qiNiuService = qiNiuService;
    }

    @GetMapping(value = "/config")
    public R get() {
        return R.ok(qiNiuService.find());
    }

    @Log("配置七牛云存储")
    @ApiOperation("配置七牛云存储")
    @PutMapping(value = "/config")
    public R emailConfig(@Validated @RequestBody QiniuConfig qiniuConfig) {

        qiNiuService.update(qiniuConfig);
        qiNiuService.update(qiniuConfig.getType());
        return R.ok();
    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    public void download(HttpServletResponse response, QiniuQueryCriteria criteria) throws IOException {
        qiNiuService.downloadList(qiNiuService.queryAll(criteria), response);
    }

    @Log("查询文件")
    @ApiOperation("查询文件")
    @GetMapping
    public R getRoles(QiniuQueryCriteria criteria, Pageable pageable) {
        return R.ok(qiNiuService.queryAll(criteria, pageable));
    }

    @Log("上传文件")
    @ApiOperation("上传文件")
    @PostMapping
    public R upload(@RequestParam MultipartFile file) {
        QiniuContent qiniuContent = qiNiuService.upload(file, qiNiuService.find());
        Map<String, Object> map = new HashMap<>(3);
        map.put("id", qiniuContent.getId());
        map.put("errno", 0);
        map.put("data", new String[]{qiniuContent.getUrl()});
        return R.ok();
    }

    @Log("同步七牛云数据")
    @ApiOperation("同步七牛云数据")
    @PostMapping(value = "/synchronize")
    public R synchronize() {

        qiNiuService.synchronize(qiNiuService.find());
        return R.ok();
    }

    @Log("下载文件")
    @ApiOperation("下载文件")
    @GetMapping(value = "/download/{id}")
    public R download(@PathVariable Long id) {

        Map<String, Object> map = new HashMap<>(1);
        map.put("url", qiNiuService.download(qiNiuService.findByContentId(id), qiNiuService.find()));
        return R.ok(map);
    }

    @Log("删除文件")
    @ApiOperation("删除文件")
    @DeleteMapping(value = "/{id}")
    public R delete(@PathVariable Long id) {

        qiNiuService.delete(qiNiuService.findByContentId(id), qiNiuService.find());
        return R.ok();
    }

    @Log("删除多张图片")
    @ApiOperation("删除多张图片")
    @DeleteMapping
    public R deleteAll(@RequestBody Long[] ids) {

        qiNiuService.deleteAll(ids, qiNiuService.find());
        return R.ok();
    }
}
