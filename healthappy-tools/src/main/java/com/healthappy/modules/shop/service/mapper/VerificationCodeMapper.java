package com.healthappy.modules.shop.service.mapper;


import com.healthappy.common.mapper.CoreMapper;
import com.healthappy.modules.shop.domain.VerificationCode;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface VerificationCodeMapper extends CoreMapper<VerificationCode> {

}
