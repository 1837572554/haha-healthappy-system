package com.healthappy.modules.utils;

import cn.hutool.core.map.MapUtil;
import lombok.experimental.UtilityClass;

import java.util.HashMap;
import java.util.Map;

@UtilityClass
public class ExpressConfig {

    /** 中国邮政 */
    private Map<Integer,String> YZPYExpType(){
        return MapUtil.of(1,"标准快递");
    }

    /** 百世快递 */
    private  Map<Integer,String> HTKYExpType(){
        return MapUtil.of(1,"标准快递");
    }

    /** 德邦快递 */
    public  Map<Integer,String> DBLExpType(){
        Map<Integer,String> map = new HashMap<>();
        map.put(1,"标准快递");
        map.put(2,"微小件特惠");
        map.put(3,"电商尊享");
        map.put(4,"特准快件");
        map.put(5,"大件快递360");
        map.put(6,"重包入户");
        map.put(7,"同城件");
        map.put(8,"重包特惠");
        map.put(9,"经济大件");
        map.put(10,"航空大件次日达");
        map.put(11,"航空大件隔日达");
        return map;
    }

    /** 申通快递 */
    public  Map<Integer,String> STOExpType(){
        Map<Integer,String> map = new HashMap<>();
        map.put(1,"标准快递");
        map.put(4,"生鲜");
        return map;
    }

    /** 圆通快递 */
    public  Map<Integer,String> YTOExpType(){
        return MapUtil.of(1,"上门揽件");
    }

    /** 韵达快递 */
    public  Map<Integer,String> YDExpType(){
        return MapUtil.of(1,"标准快递");
    }


    /** 中通快递 */
    public  Map<Integer,String> ZTOExpType(){
        Map<Integer,String> map = new HashMap<>();
        map.put(1,"普通订单");
        map.put(2,"线下订单");
        map.put(3,"COD订单");
        map.put(4,"限时物流");
        map.put(5,"快递保障订单");
        map.put(6,"星联服务");
        return map;
    }
}
