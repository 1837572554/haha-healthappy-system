package com.healthappy.annotation;


import com.healthappy.aspect.IgnoreTenantUtil;
import com.healthappy.utils.SecurityUtils;
import lombok.SneakyThrows;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;


/**
 * @author FGQ
 * @since 2020-10-16
 */
@Aspect
@Component
public class IgnoreTenantAspect {

    @Around("@annotation(ignoreTenant)")
    @SneakyThrows
    public Object aroundMethod(ProceedingJoinPoint point, IgnoreTenant ignoreTenant) {
     return  around(point,ignoreTenant);

    }

    @Around("@within(ignoreTenant)")
    @SneakyThrows
    public Object aroundType(ProceedingJoinPoint point, IgnoreTenant ignoreTenant) {
        return around(point,ignoreTenant);
    }

    @SneakyThrows
    private Object around(ProceedingJoinPoint point,IgnoreTenant ignoreTenant) {
     HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
     String url = request.getRequestURL().toString();
     IgnoreTenantUtil.getIgnoreTenantUtil().add(url);
     Object proceed = point.proceed();
     if(ignoreTenant.isStoreUser()){
         IgnoreTenantUtil.getIgnoreTenantUtil().remove(url);
     }
     IgnoreTenantUtil.ignoreTenantLogo = false;
     return proceed;
    }
}
