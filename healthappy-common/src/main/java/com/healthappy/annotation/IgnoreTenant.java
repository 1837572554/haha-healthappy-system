package com.healthappy.annotation;

import java.lang.annotation.*;

/**
 * <p>
 * 忽略租户拦截
 * </p>
 *
 * @author FGQ
 * @since 2020-10-16
 */
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface IgnoreTenant {

    boolean isStoreUser() default false;
}
