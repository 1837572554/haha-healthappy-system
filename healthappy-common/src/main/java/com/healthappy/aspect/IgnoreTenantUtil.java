package com.healthappy.aspect;

import cn.hutool.core.collection.CollUtil;

import java.util.HashSet;
import java.util.Set;

/***
 * 忽略方法中的租户
 * @author FGQ
 */
public class IgnoreTenantUtil {

    private static Set<String> ignoreTenantMethodsList;

    public static Boolean ignoreTenantLogo = false;

    private IgnoreTenantUtil(){

    }

    public static Set<String>  getIgnoreTenantUtil(){
        if(CollUtil.isEmpty(ignoreTenantMethodsList)){
            synchronized(IgnoreTenantUtil.class){
                if(CollUtil.isEmpty(ignoreTenantMethodsList)){
                    ignoreTenantMethodsList = new HashSet<>();
                }
            }
        }
        return ignoreTenantMethodsList;
    }
}
