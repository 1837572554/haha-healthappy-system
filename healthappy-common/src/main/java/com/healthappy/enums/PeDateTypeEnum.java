package com.healthappy.enums;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import com.google.common.collect.ImmutableMap;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.collections.bidimap.DualHashBidiMap;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author yanjun
 * @date 2021/11/24 20:36
 * @desc 体检时间类型Enum
 */
@Getter
@AllArgsConstructor
public enum PeDateTypeEnum {
	/** 未知 */
	UNKNOWN(0, "未知"),
	/** 登记时间 */
	REGISTRATION(1, "登记时间"),
	/** 签到时间 */
	SIGN(2, "签到时间"),
	/** 完成时间 */
	COMPLETION(3, "完成时间"),
	/** 总检时间 */
	CONCLUSION(4, "总检时间"),
	/** 审核时间 */
	VERIFY(5, "审核时间"),
	/** 打印时间 */
	PRINT(6, "打印时间"),
	/** 上传时间 */
	UPLOAD(7, "上传时间"),
	//region 危急值
	/** 发生时间 */
	OCCUR(20, "发生时间"),
	/** 处理时间 */
	HANDLE(21, "处理时间"),
	//endregion 危急值
	/** 签收时间 */
	RECEIPT(30, "签收时间");
	@EnumValue
	private Integer value;
	private String desc;

	/**
	 * 是否未知
	 *
	 * @param value
	 * @return java.lang.Boolean
	 * @author YJ
	 * @date 2021/12/23 20:39
	 */
	public static Boolean isUnknown(Integer value) {
		return UNKNOWN.getValue().equals(value);
	}

	/**
	 * 是否登记时间
	 *
	 * @param value
	 * @return java.lang.Boolean
	 * @author YJ
	 * @date 2021/12/23 20:39
	 */
	public static Boolean isRegistration(Integer value) {
		return REGISTRATION.getValue().equals(value);
	}

	/**
	 * 是否签到时间
	 *
	 * @param value
	 * @return java.lang.Boolean
	 * @author YJ
	 * @date 2021/12/23 20:39
	 */
	public static Boolean isSign(Integer value) {
		return SIGN.getValue().equals(value);
	}

	/**
	 * 是否完成时间
	 *
	 * @param value
	 * @return java.lang.Boolean
	 * @author YJ
	 * @date 2021/12/23 20:39
	 */
	public static Boolean isCompletion(Integer value) {
		return COMPLETION.getValue().equals(value);
	}

	/**
	 * 是否总检时间
	 *
	 * @param value
	 * @return java.lang.Boolean
	 * @author YJ
	 * @date 2021/12/23 20:39
	 */
	public static Boolean isConclusion(Integer value) {
		return CONCLUSION.getValue().equals(value);
	}

	/**
	 * 是否审核时间
	 *
	 * @param value
	 * @return java.lang.Boolean
	 * @author YJ
	 * @date 2021/12/23 20:39
	 */
	public static Boolean isVerify(Integer value) {
		return VERIFY.getValue().equals(value);
	}

	/**
	 * 是否打印时间
	 *
	 * @param value
	 * @return java.lang.Boolean
	 * @author YJ
	 * @date 2021/12/23 20:39
	 */
	public static Boolean isPrint(Integer value) {
		return PRINT.getValue().equals(value);
	}

	/**
	 * 是否上传时间
	 * @author YJ
	 * @date 2022/2/8 16:09
	 * @param value
	 * @return java.lang.Boolean
	 */
	public static Boolean isUpload(Integer value) {
		return UPLOAD.getValue().equals(value);
	}

	/**
	 * 是否发生时间
	 * @author YJ
	 * @date 2022年2月10日 16:04:01
	 * @param value
	 * @return java.lang.Boolean
	 */
	public static Boolean isOccur(Integer value) {
		return OCCUR.getValue().equals(value);
	}

	/**
	 * 是否处理时间
	 * @author YJ
	 * @date 2022年2月10日 16:03:57
	 * @param value
	 * @return java.lang.Boolean
	 */
	public static Boolean isHandle(Integer value) {
		return HANDLE.getValue().equals(value);
	}

	/**
	 * 是否签收时间
	 * @author YJ
	 * @date 2022/5/18 10:40
	 * @param value
	 * @return java.lang.Boolean
	 */
	public static Boolean isReceipt(Integer value) {
		return RECEIPT.getValue().equals(value);
	}


	public static PeDateTypeEnum toType(Integer value) {
		return Stream.of(PeDateTypeEnum.values()).filter(p -> p.getValue().equals(value)).findAny().orElse(UNKNOWN);
	}

	public static String getDescByVal(Integer val) {
		return toType(val).getDesc();
	}

	public static Integer getValByDesc(String desc) {
		return Stream.of(PeDateTypeEnum.values()).filter(p -> p.getDesc().equals(desc)).findAny().orElse(UNKNOWN)
				.getValue();
	}

	@JsonValue
	public Map<String, Object> toMap() {
		return ImmutableMap.<String, Object>builder().put("desc", getDesc()).put("value", getValue()).build();
	}

	public static List<Map> toList() {
		return Stream.of(PeDateTypeEnum.values()).map(p -> p.toMap()).collect(Collectors.toList());
		//        List<Map> list= new ArrayList();
		//        PeDateTypeEnum[] values = PeDateTypeEnum.values();
		//        for (PeDateTypeEnum value : values) {
		//            list.add(value.toMap());
		//        }
		//        return list;
	}

	public static String toJson() {
		return JSON.toJSON(toList()).toString();
	}


	public static void main(String[] args) {
		DualHashBidiMap bidiMap = new DualHashBidiMap(Stream.of(PeDateTypeEnum.values())
				.collect(Collectors.toMap(PeDateTypeEnum::getValue, PeDateTypeEnum::getDesc)));
		System.out.println(bidiMap.getKey("登记时间"));
		System.out.println(bidiMap.get(1));
	}
}
