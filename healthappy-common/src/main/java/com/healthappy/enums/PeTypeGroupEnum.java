package com.healthappy.enums;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonValue;
import com.google.common.collect.ImmutableMap;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.poi.util.Internal;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * @author yanjun
 * @description 体检类型分组 枚举
 * @date 2021/9/10
 */
@Getter
@AllArgsConstructor
public enum PeTypeGroupEnum {
    /**
     * 职业
     */
    PROFESSION(1, "职业"),
    /**
     * 健康
     */
    HEALTH(2, "健康"),
    /**
     * 从业
     */
    WORK(3, "从业"),
    /**
     * 驾驶员
     */
    Driver(4, "驾驶员"),
    /**
     * 儿童
     */
    CHILD(5, "儿童"),
    /**
     * 职普合一
     */
    ZPHY(6, "职普合一"),
    /**
     * 学生
     */
    STUDENT(7, "学生");

    private Integer value;
    private String desc;

    public static PeTypeGroupEnum toType(int value) {
        return Stream.of(PeTypeGroupEnum.values())
                .filter(p -> p.value == value)
                .findAny()
                .orElse(null);
    }

    public static String getDescByVal(Internal val) {
        PeTypeGroupEnum[] values = PeTypeGroupEnum.values();
        for (PeTypeGroupEnum peTypeGroupEnum : values) {
            if (peTypeGroupEnum.getValue().equals(val)) {
                return peTypeGroupEnum.getDesc();
            }
        }
        return null;
    }

    public static Integer getValByDesc(String desc) {
        PeTypeGroupEnum[] values = PeTypeGroupEnum.values();
        for (PeTypeGroupEnum peTypeGroupEnum : values) {
            if (peTypeGroupEnum.getDesc().equalsIgnoreCase(desc)) {
                return peTypeGroupEnum.getValue();
            }
        }
        return null;
    }

    @JsonValue
    public Map<String, Object> toMap() {
        return ImmutableMap.<String, Object>builder()
                .put("desc", getDesc())
                .put("value", getValue())
                .build();
    }

    public static List<Map> toList() {
        List<Map> list = new ArrayList();
        PeTypeGroupEnum[] values = PeTypeGroupEnum.values();
        for (PeTypeGroupEnum value : values) {
            System.out.println(value.toMap());
            list.add(value.toMap());
        }
        return list;
    }

    public static String toJson() {
        return JSON.toJSON(toList()).toString();
    }


}
