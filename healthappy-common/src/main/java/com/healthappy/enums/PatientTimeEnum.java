package com.healthappy.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author: FGQ
 * @Desc: 登录时间转换
 * @Date: Created in 18:11 2022/3/7
 */
@Getter
@AllArgsConstructor
public enum  PatientTimeEnum implements IEnum<Integer> {

    /***登记 */
    PE_DATE(1, "pe_date"),

    /*** 签到 */
    SIGN_DATE(2, "sign_date"),

    /*** 总检 */
    CONCLUSION_DATE(3, "conclusion_date");

    private final Integer value;
    private final String desc;

    @Override
    public Integer getId() {
        return value;
    }
}
