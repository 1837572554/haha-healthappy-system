package com.healthappy.enums;

/**
 * @Author: FGQ
 * @Desc: 基础入参Enum
 * @Date: Created in 18:21 2022/3/7
 */
public interface IEnum<T> {

    T getId();
}
