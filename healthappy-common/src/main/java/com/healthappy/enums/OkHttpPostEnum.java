package com.healthappy.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * @author Jevany
 * @date 2022/5/19 19:03
 */
@Getter
@AllArgsConstructor
public enum OkHttpPostEnum implements IEnum<Integer> {
	/** 默认 */
	DEFAULT(0, "default"),
	/** json */
	JSON(1, "json"),
	/** multipart/form-data */
	MULTIPART_FORM(2, "multipart/form-data");


	@EnumValue
	private Integer value;
	private String desc;

	@Override
	public Integer getId() {
		return value;
	}
}
