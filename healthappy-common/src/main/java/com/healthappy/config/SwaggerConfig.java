package com.healthappy.config;

import com.fasterxml.classmate.TypeResolver;
import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import com.google.common.base.Predicates;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.Ordered;
import org.springframework.data.domain.Pageable;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.AlternateTypeRule;
import springfox.documentation.schema.AlternateTypeRuleConvention;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static springfox.documentation.builders.PathSelectors.regex;
import static springfox.documentation.schema.AlternateTypeRules.newRule;


/**
 * api页面 /doc.html
 * @Author hupeng <610796224@qq.com>
 * @Date 2019/1/9
 **/

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Value("${jwt.header}")
    private String tokenHeader;

    @Value("${jwt.token-start-with}")
    private String tokenStartWith;

    @Value("${swagger.enabled}")
    private Boolean enabled;

    @Value("${swagger.title}")
    private String title;

    @Value("${swagger.version}")
    private String version;

    @Value("${swagger.serverUrl}")
    private String serverUrl;

    @Bean
    @SuppressWarnings("all")
    public Docket createRestApi() {
//        ParameterBuilder ticketPar = new ParameterBuilder();
//        List<Parameter> pars = new ArrayList<>();
//        ticketPar.name(tokenHeader).description("token")
//                .modelRef(new ModelRef("string"))
//                .parameterType("header")
//                .defaultValue(tokenStartWith + " ")
//                .required(true)
//                .build();
//        pars.add(ticketPar.build());
        return new Docket(DocumentationType.SWAGGER_2)
                .enable(enabled)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.healthappy.modules"))
                .paths(Predicates.not(PathSelectors.regex("/error.*")))
                .build()
                .directModelSubstitute(Timestamp.class, Date.class)

                //.globalOperationParameters(pars)
                .securitySchemes(securitySchemes())
                .securityContexts(securityContexts());
    }

	@Bean(value = "sysRestApi")
	public Docket sysRestApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("系统")
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.healthappy.modules.system.rest.system"))
				.paths(PathSelectors.any())
				.build();
	}

	@Bean(value = "baseRestApi")
	public Docket baseRestApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("基础管理")
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.healthappy.modules.system.rest.basic"))
				.paths(PathSelectors.any())
				.build();
	}

	@Bean(value = "physicaExamlRestApi")
	public Docket physicaExamlRestApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("体检管理")
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.healthappy.modules.system.rest.manage"))
				.paths(PathSelectors.any())
				.build();
	}


	@Bean(value = "physicaExamlFlowRestApi")
	public Docket physicaExamlFlowRestApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("体检流程")
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.healthappy.modules.system.rest.projectcheck"))
				.paths(PathSelectors.any())
				.build();
	}

	@Bean(value = "dataQueryRestApi")
	public Docket dataQueryRestApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("数据查询")
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.healthappy.modules.system.rest.dataquery"))
				.paths(PathSelectors.any())
				.build();
	}

	@Bean(value = "commonRestApi")
	public Docket commonRestApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("默认基础数据")
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.healthappy.modules.system.rest.common"))
				.paths(PathSelectors.any())
				.build();
	}

	@Bean(value = "toolRestApi")
	public Docket toolRestApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("工具")
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.healthappy.modules.shop.rest"))
				.paths(PathSelectors.any())
				.build();
	}



    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(title)
                .termsOfServiceUrl(serverUrl)
                .description(title)
                .version(version)
                .contact(new Contact("haha_healthappy", "http://doc.hahahealth.cn/docs/#/", "xxxxx@163.com"))
                .build();
    }

    private List<ApiKey> securitySchemes() {
        List<ApiKey> apiKeyList = new ArrayList();
        apiKeyList.add(new ApiKey("Authorization", "Authorization", "header"));
        return apiKeyList;
    }

    private List<SecurityContext> securityContexts() {
        List<SecurityContext> securityContexts = new ArrayList<>();
        securityContexts.add(
                SecurityContext.builder()
                        .securityReferences(defaultAuth())
                        .forPaths(regex("^(?!auth).*$"))
                        .build());
        return securityContexts;
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        List<SecurityReference> securityReferences = new ArrayList<>();
        securityReferences.add(new SecurityReference("Authorization", authorizationScopes));
        return securityReferences;
    }

}

/**
 *  将Pageable转换展示在swagger中
 */
@Configuration
@EnableSwagger2
@EnableKnife4j
@Import(BeanValidatorPluginsConfiguration.class)
class SwaggerDataConfig {

    @Bean
    public AlternateTypeRuleConvention pageableConvention(final TypeResolver resolver) {
        return new AlternateTypeRuleConvention() {
            @Override
            public int getOrder() {
                return Ordered.HIGHEST_PRECEDENCE;
            }

            @Override
            public List<AlternateTypeRule> rules() {
                return newArrayList(newRule(resolver.resolve(Pageable.class), resolver.resolve(Page.class)));
            }
        };
    }

    @ApiModel
    @Data
    private static class Page {
        @ApiModelProperty("页码 (0..N)")
        private Integer page;

        @ApiModelProperty("每页显示的数目")
        private Integer size;

        @ApiModelProperty("以下列格式排序标准：property[,asc | desc]。 默认排序顺序为升序。 支持多种排序条件：如：id,asc")
        private List<String> sort;
    }
}
