package com.healthappy.utils;

public interface SecurityConstants {

    String BALL_CHECK_UPLOAD = "ball_check_upload_";

    /**
     * 用户缓存
     */
    String AUTH_USER_TENANT_ID = "auth_user_tenant_id_";
    /**
     * 登录来源
     */
    String SOURCE_PC = "PC";

    /** 登录来源 */
    String SOURCE_MINI = "MINI";


    /**
     * 开发环境
     */
    String DEV_CODE = "dev";
    /**
     * 生产环境
     */
    String PROD_CODE = "prod";

    /**
     * 指引单回交-弃检标识
     */
    String IS_SUBMIT_RESULT = "本人放弃检查";
}
