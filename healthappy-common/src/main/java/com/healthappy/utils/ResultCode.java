package com.healthappy.utils;

import com.baomidou.mybatisplus.extension.api.IErrorCode;
import lombok.AllArgsConstructor;

/**
 * @Description: 结果状态码
 * @Author: ZhangJiaJun
 * @Date: 2020/4/1 13:41
 */
@AllArgsConstructor
public enum ResultCode implements IErrorCode {

    /**
     * 操作成功
     */
    SUCCESS(200, "操作成功"),
    /**
     * 操作失败
     */
    ERROR(500, "操作失败"),
    /**
     * 参数检验失败
     */
    VALIDATE_FAILED(400, "参数检验失败"),
    /**
     * 暂未登录或token已经过期
     */
    UNAUTHORIZED(401, "暂未登录或token已经过期"),
    /**
     * 没有相关权限
     */
    FORBIDDEN(403, "没有相关权限"),
    /**
     * 业务异常
     */
    BIZ_ERROR(590, "业务异常");

    /**
     * 状态码
     */
    private long code;
    /**
     * 消息
     */
    private String msg;

    @Override
    public long getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }
}
