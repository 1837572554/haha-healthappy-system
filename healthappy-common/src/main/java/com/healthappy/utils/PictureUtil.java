package com.healthappy.utils;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.*;

/**
 * @desc: 图片帮助类
 * @author: YJ
 * @date: 2021-12-03 16:38
 **/
public class PictureUtil {

    /**
     * Base64图片保存到本地
     * @author YJ
     * @date 2021/12/3 16:45
     * @param savePath 文件保存的全路径，路径+名称  D:/home/lab/test/1111.jpg
     * @param base64String Base64字符串
     */
    public static boolean base64StrToImage(String savePath,String base64String) {
        if(StringUtils.isBlank(base64String)){
            return false;
        }
        int index = base64String.indexOf(";base64,");
        if (index != -1) {
            base64String = base64String.substring(index + 8);
        }
        //前台在用Ajax传base64值的时候会把base64中的+换成空格，所以需要替换回来
        base64String = base64String.replaceAll(" ","+");

        BASE64Decoder decoder = new BASE64Decoder();

        byte[] imageByte = null;
        try {
            imageByte = decoder.decodeBuffer(base64String);
            for (int i = 0; i < imageByte.length; ++i) {
                if (imageByte[i] < 0) {
                    // 调整异常数据
                    imageByte[i] += 256;
                }
            }
            //文件夹不存在则自动创建
            File tempFile = new File(savePath);
            if (!tempFile.getParentFile().exists()) {
                tempFile.getParentFile().mkdirs();
            }

            OutputStream out = new FileOutputStream(savePath);
            out.write(imageByte);
            out.flush();
            out.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 图片转base64字符串
     * @param imgFile 图片路径
     * @return
     */
    public static String imageToBase64Str(String imgFile) {
        InputStream inputStream = null;
        byte[] data = null;
        try {
            inputStream = new FileInputStream(imgFile);
            data = new byte[inputStream.available()];
            inputStream.read(data);
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 加密
        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(data);
    }
}
