package com.healthappy.utils;

import lombok.experimental.UtilityClass;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 常用静态常量
 * @author Zheng Jie
 * @date 2018-12-26
 */
@UtilityClass
public class ShopConstant {

    public static final String RESET_PASS = "重置密码";

    public static final String RESET_MAIL = "重置邮箱";

    public static String TID_SEED_ID = "tid_create_id_";

    //面单打印模板内容(html格式)
    private final String PRINT_TEMPLATE = "print_template_";


    public String PrintTemplate(String orderNo){
       return RedisUtil.get(PRINT_TEMPLATE + orderNo);
    }

    public void PrintTemplate(String orderNo,String printTemplate){
        RedisUtil.set(PRINT_TEMPLATE + orderNo,printTemplate);
    }

    public List<String> PrintTemplate(Set<String> orderNo){
         return orderNo.stream().map(ShopConstant::PrintTemplate).collect(Collectors.toList());
    }

    /**
     * 常用接口
     */
    public static class Url {
        //免费图床
        public static final String SM_MS_URL = "https://sm.ms/api";

        // IP归属地查询
        public static final String IP_URL = "http://whois.pconline.com.cn/ipJson.jsp?ip=%s&json=true";
    }
}
