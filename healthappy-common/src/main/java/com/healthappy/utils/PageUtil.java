package com.healthappy.utils;

import cn.hutool.core.util.ObjectUtil;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 分页工具
 *
 * @author Zheng Jie
 * @date 2018-12-10
 */
public class PageUtil extends cn.hutool.core.util.PageUtil {

    /**
     * List 分页
     */
    public static List toPage(int page, int size, List list) {
        int fromIndex = page * size;
        int toIndex = page * size + size;
        if (fromIndex > list.size()) {
            return new ArrayList();
        } else if (toIndex >= list.size()) {
            return list.subList(fromIndex, list.size());
        } else {
            return list.subList(fromIndex, toIndex);
        }
    }

    /**
     * 计算列表开始，结束位置
     *
     * @param page     当前页值
     * @param size     每页条数
     * @param listSize 列表总数
     * @return java.lang.Integer 数组，0：开始位置，1：结束位置，2：当前页数，3：总页数，4：页面条数
     * @author YJ
     * @date 2022/1/19 18:11
     */
    public static Integer[] toStartEnd(Integer page, Integer size, Integer listSize) {
        if (ObjectUtil.isEmpty(page) || page <= 0) {
            page = 1;
        }
        if (ObjectUtil.isEmpty(size) || size <= 0) {
            size = 10;
        }
        if (listSize <= 0) {
            return new Integer[]{0, 0, 1, 0, size};
        }

        /** 总页数 */
        Integer totalPage = (listSize + size - 1) / size;
        if (page > totalPage) {
            page = totalPage;
        }
        /** 开始 */
        Integer startIndex = (page - 1) * size;
        /** 结束 */
        Integer endIndex = startIndex + size;
        if (totalPage.equals(page)) {
            endIndex = listSize;
        }
        return new Integer[]{startIndex, endIndex, page, totalPage, size};
    }

    /**
     * Page 数据处理，预防redis反序列化报错
     */
    public static Map<String, Object> toPage(Page page) {
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", page.getContent());
        map.put("totalElements", page.getTotalElements());
        return map;
    }


    /**
     * 自定义分页
     */
    public static Map<String, Object> toPage(Object object, Object totalElements) {
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", object);
        map.put("totalElements", totalElements);
        return map;
    }

}
