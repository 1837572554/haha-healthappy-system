package com.healthappy.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.stereotype.Service;

/**
 * @author  FGQ
 */
@Slf4j
@Service
public class SpringBeanFactoryUtil implements BeanFactoryAware {
    private static BeanFactory beanFactory;

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }

    /**
     * 动态解析yml的值。
     * @param value ${}格式
     * @return 若是解析失败或者未查找到，均返回null
     */
    public static String resolve(String value) {
        try {
            if (beanFactory != null && beanFactory instanceof ConfigurableBeanFactory) {
                return ((ConfigurableBeanFactory) beanFactory).resolveEmbeddedValue(value);
            }
        }catch (Exception e){
            log.error("",e);
        }

        return null;
    }
}
