package com.healthappy.utils;

import cn.hutool.core.util.StrUtil;
import com.healthappy.exception.BadRequestException;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author FGQ
 */
@UtilityClass
public class ValidatorUtils {

    private final Validator validator =  Validation.buildDefaultValidatorFactory()
            .getValidator();

    public void beanValidate(@Valid Object obj) throws BadRequestException {

        if (obj instanceof Collection){
            ValidCollection validCollection = new ValidCollection();
            validCollection.setList((Collection) obj);
            validate(validCollection);
        }else {
            validate(obj);
        }
    }

    public void validate(@Valid Object obj) {
        String msg = "";
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(obj);
        for (ConstraintViolation<Object> c : constraintViolations) {
            if(StrUtil.isNotBlank(msg)){
                break;
            }
            msg = c.getMessage();
        }
        Assert.isTrue(ObjectUtils.isEmpty(constraintViolations), msg);
    }

    /**
     * 导入时验证。若不存在，抛出异常
     * @param addDtos
     */
    public void verifyImportParam(List<?> addDtos) {
        ArrayList<String> list = new ArrayList<>();
        StringBuilder sb = null;

        Validator validator =  Validation.buildDefaultValidatorFactory()
                .getValidator();
        for (int i = 1; i <= addDtos.size(); i++) {
            sb = new StringBuilder();
            Object item = addDtos.get(i - 1);
            Set<ConstraintViolation<Object>> constraintViolations = validator.validate(item);
            for (ConstraintViolation<Object> c : constraintViolations) {
                sb.append(c.getPropertyPath().toString()).append(":").append(c.getMessage()).append(";");
            }
            if (!StringUtils.isEmpty(sb.toString())) {
                list.add("第" + i + "条：" + sb.toString());
            }
        }
        Assert.isTrue(ObjectUtils.isEmpty(list), list.toString());
    }

    private static class ValidCollection<T> {
        @Valid
        private Collection<T> list;

        public Collection<T> getList() {
            return list;
        }

        public void setList(Collection<T> list) {
            this.list = list;
        }
    }
}