package com.healthappy.utils;

/**
 * @Author: FGQ
 * @Desc: 字典枚举
 * @Date: Created in 20:04 2022/3/10
 */
public interface CacheConstant {

	/* --系统管理-- */

	/** 字典 */
	String DICT = "dict";

	/** 字典详情 */
	String DICT_DETAIL = "dictDetail";

	/** 团检备单导入记录 */
	String FILE_IMPORT_RECORD = "fileImportRecord";

	/** 菜单 */
	String MENU = "menu";

	/** 用户 */
	String USER = "user";

	/** 角色 */
	String ROLE = "role";

	/** 标识 */
	String TAG = "tag";

	/** 租户 */
	String TENANT = "tenant";

	/** 公告设置 */
	String UNOTICE = "UNotice";

	/** 公告设置部门 */
	String U_NOTICE_DEPT = "UNoticeDept";

	/* --基础数据管理-- */

	/** 科室设置 */
	String B_DEPT = "BDept";

	/** 体检项目设置 */
	String B_ITEM = "BItem";

	/** 菜单 */
	String USER_MENU = "userMenu";

	/** 预约号源数据 */
	String APPOINT_DATE_NUM = "AppointDateNum";
}
