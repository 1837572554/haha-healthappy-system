package com.healthappy.utils;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ReflectUtil;

import java.util.ArrayList;
import java.util.List;


/**
 * @desc: Bean拷贝工具类
 * @author: YJ
 * @date: 2021-12-07 18:34
 **/
public class BeanCopyUtil {

    /**
     * 拷贝List对象 （eg：List<TestDTO> dtoList = copy(poList,TestDTO.class);）
     * @author YJ
     * @date 2021/12/7 18:58
     * @param sourceList 原List对象
     * @param beanClass 需要转换的对象类
     * @return java.util.List〈T〉
     */
    public static <T> List<T> copyList(Object sourceList, Class<T> beanClass){
        List<Object> sList = (List<Object>) sourceList;
        List<T> tList = new ArrayList<T>();
        for (Object t : sList) {
            T target = ReflectUtil.newInstance(beanClass);
            BeanUtil.copyProperties(t, target);
            tList.add(target);
        }
        return (List<T>) tList;
    }
}
