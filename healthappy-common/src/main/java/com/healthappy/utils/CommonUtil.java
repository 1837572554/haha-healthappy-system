package com.healthappy.utils;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONObject;
import lombok.experimental.UtilityClass;

import java.util.*;
import java.util.stream.Collectors;

@UtilityClass
public class CommonUtil {


    /** string 转 List */
    public List<String> stringConverList(String value){
        return Convert.toList(String.class,value.split(","));
    }

    /** List 转 String */
    public String listConverString(List<String> str){
        return CollUtil.join(str,",");
    }

    /** 不同排列组合 */
    public List<String> descartes(List<String>... lists) {
        List<String> tempList = new ArrayList<>();
        for (List<String> list : lists) {
            if (tempList.isEmpty()) {
                tempList = list;
            } else {
                //java8新特性，stream流
                tempList = tempList.stream().flatMap(item -> list.stream().map(item2 -> item + " " + item2)).collect(Collectors.toList());
            }
        }
        return tempList;
    }

    /** 不同排列组合 */
    public List<String> descartes(List<List<String>> lists) {
        List<String> tempList = new ArrayList<>();
        for (List<String> list : lists) {
            if (tempList.isEmpty()) {
                tempList = list;
            } else {
                //java8新特性，stream流
                tempList = tempList.stream().flatMap(item -> list.stream().map(item2 -> item + " " + item2)).collect(Collectors.toList());
            }
        }
        return tempList;
    }

    /** 获取距离现在时间的 */
    public String distanceDate(Date endDate){
        DateTime date = DateUtil.date();
        long second = DateUtil.between(endDate, date , DateUnit.SECOND);
        if(second > 60){
            long minute = DateUtil.between(endDate, date, DateUnit.MINUTE);
            if(minute > 60){
                long hour = DateUtil.between(endDate, date, DateUnit.HOUR);
                if(hour > 24){
                    long day =  DateUtil.betweenDay(endDate, date, false);
                    if(day > DateUtil.endOfMonth(date).dayOfMonth()){
                        long month = DateUtil.betweenMonth(endDate, date, false);
                        if(month > 12){
                            return DateUtil.betweenYear(endDate, date, false) + "年前";
                        }
                        return month + "月前";
                    }
                    return day + "天前";
                }
                return hour + "小时前";
            }
            return minute + "分钟前";
        }
        return second+"秒前";
    }

    /**
     * 使用 Map按value进行排序
     * @param map
     * @return
     */
    public  <K, V> LinkedHashMap<K, V> sortMapByValueDesc(Map<K, V> map){
        List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
        list.sort(Comparator.comparing(o -> o.getValue().toString()));
        return list.stream().collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (key1, key2) -> key2, LinkedHashMap::new));
    }
}
