package com.healthappy.utils;

import com.baomidou.mybatisplus.extension.api.IErrorCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class R<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 是否成功
     */
    private boolean success;

    /**
     * 状态码
     */
    private long code;

    /**
     * 提示信息
     */
    private String msg;

    /**
     * 返回数据
     */
    private T data;

    /**
     * 成功返回结果
     *
     * @return com.healthappy.eyesight.common.utils.R<T>
     * @Date 2020/4/1 2:01 下午
     * @Author ZhangJiaJun
     */
    public static <T> R<T> ok() {
        return new R<>(true, ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMsg(), null);
    }

    /**
     * 成功返回结果
     *
     * @param data 返回数据
     * @return com.healthappy.eyesight.common.utils.R<T>
     * @Date 2020/4/1 2:01 下午
     * @Author ZhangJiaJun
     */
    public static <T> R<T> ok(T data) {
        return new R<>(true, ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMsg(), data);
    }

    /**
     * 成功返回结果
     *
     * @param data 返回数据
     * @param msg  错误消息
     * @return com.healthappy.eyesight.common.utils.R<T>
     * @Date 2020/4/1 2:02 下午
     * @Author ZhangJiaJun
     */
    public static <T> R<T> ok(T data, String msg) {
        return new R<>(true, ResultCode.SUCCESS.getCode(), msg, data);
    }

    /**
     * 失败返回结果
     *
     * @param errorCode 错误码
     * @return com.healthappy.eyesight.common.utils.R<T>
     * @Date 2020/4/1 2:02 下午
     * @Author ZhangJiaJun
     */
    public static <T> R<T> error(IErrorCode errorCode) {
        return new R<>(false, errorCode.getCode(), errorCode.getMsg(), null);
    }

    /**
     * 失败返回结果
     *
     * @param msg 错误消息
     * @return com.healthappy.eyesight.common.utils.R<T>
     * @Date 2020/4/1 2:02 下午
     * @Author ZhangJiaJun
     */
    public static <T> R<T> error(String msg) {
        return new R<>(false, ResultCode.ERROR.getCode(), msg, null);
    }

    /**
     * 失败返回结果
     *
     * @return com.healthappy.eyesight.common.utils.R<T>
     * @Date 2020/4/1 2:02 下午
     * @Author ZhangJiaJun
     */
    public static <T> R<T> error() {
        return error(ResultCode.ERROR);
    }

    /**
     * 失败返回结果
     *
     * @param code 错误码
     * @param msg  错误消息
     * @return com.healthappy.eyesight.common.utils.R<T>
     * @Date 2020/4/1 3:16 下午
     * @Author ZhangJiaJun
     */
    public static <T> R<T> error(int code, String msg) {
        return new R<>(false, code, msg, null);
    }


    /**
     * 参数验证失败返回结果
     *
     * @return com.healthappy.eyesight.common.utils.R<T>
     * @Date 2020/4/1 2:02 下午
     * @Author ZhangJiaJun
     */
    public static <T> R<T> validateFailed() {
        return error(ResultCode.VALIDATE_FAILED);
    }

    /**
     * 参数验证失败返回结果
     *
     * @param msg 错误消息
     * @return com.healthappy.eyesight.common.utils.R<T>
     * @Date 2020/4/1 2:02 下午
     * @Author ZhangJiaJun
     */
    public static <T> R<T> validateFailed(String msg) {
        return new R<>(false, ResultCode.VALIDATE_FAILED.getCode(), msg, null);
    }

    /**
     * 未登录返回结果
     *
     * @param data 返回数据
     * @return com.healthappy.eyesight.common.utils.R<T>
     * @Date 2020/4/1 2:02 下午
     * @Author ZhangJiaJun
     */
    public static <T> R<T> unauthorized(T data) {
        return new R<>(false, ResultCode.UNAUTHORIZED.getCode(), ResultCode.UNAUTHORIZED.getMsg(), data);
    }

    /**
     * 未授权返回结果
     *
     * @param data 数据
     * @return com.healthappy.eyesight.common.utils.R<T>
     * @Date 2020/4/1 2:02 下午
     * @Author ZhangJiaJun
     */
    public static <T> R<T> forbidden(T data) {
        return new R<>(false, ResultCode.FORBIDDEN.getCode(), ResultCode.FORBIDDEN.getMsg(), data);
    }

    /**
     * 业务异常返回结果
     *
     * @param msg 错误消息
     * @return com.healthappy.eyesight.common.utils.R<T>
     * @Date 2020/4/1 2:03 下午
     * @Author ZhangJiaJun
     */
    public static <T> R<T> bizError(String msg) {
        return new R<>(false, ResultCode.BIZ_ERROR.getCode(), msg, null);
    }
}

