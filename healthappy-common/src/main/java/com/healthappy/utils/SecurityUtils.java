package com.healthappy.utils;

import cn.hutool.core.bean.BeanPath;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.healthappy.exception.BadRequestException;
import lombok.experimental.UtilityClass;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 获取当前登录的用户
 * @author Zheng Jie
 * @date 2019-01-17
 */
@UtilityClass
public class SecurityUtils {

    public static JwtUser getUserDetails() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            throw new BadRequestException(HttpStatus.UNAUTHORIZED, "当前登录状态过期");
        }
        if (authentication.getPrincipal() instanceof UserDetails) {
            UserDetails userDetails = (UserDetails)authentication.getPrincipal();
            Object o =  RedisUtil.get(SecurityConstants.AUTH_USER_TENANT_ID+ userDetails.getUsername());
            com.alibaba.fastjson.JSONObject parseObject = JSON.parseObject(JSON.toJSONString(o));
            return com.alibaba.fastjson.JSONObject.parseObject(parseObject.toJSONString(),JwtUser.class);
        }
        throw new BadRequestException(HttpStatus.UNAUTHORIZED, "找不到当前登录的信息");
    }

    public static List<String> authorities(){
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            throw new BadRequestException(HttpStatus.UNAUTHORIZED, "当前登录状态过期");
        }
        if (authentication.getPrincipal() instanceof UserDetails) {
            UserDetails userDetails = (UserDetails)authentication.getPrincipal();
            Object o =  RedisUtil.get(SecurityConstants.AUTH_USER_TENANT_ID+ userDetails.getUsername());
            com.alibaba.fastjson.JSONObject parseObject = JSON.parseObject(JSON.toJSONString(o));
            return parseObject.getJSONArray("authorities").stream().map(k-> JSON.parseObject(k.toString()).getString("authority")).collect(Collectors.toList());
        }
        throw new BadRequestException(HttpStatus.UNAUTHORIZED, "找不到当前登录的信息");
    }

    /**
     * 获得用户昵称（用户名称）
     * @author YJ
     * @date 2022/1/26 16:57
     * @return java.lang.String
     */
    public static String getNickName(){
        return getUserDetails().getNickName();
    }

    /**
     * 获取TenantId
     */
    public  String getTenantId() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            return null;
        }
        if (authentication.getPrincipal() instanceof UserDetails) {
            UserDetails userDetails = (UserDetails) authentication.getPrincipal();
            String key = SecurityConstants.AUTH_USER_TENANT_ID+userDetails.getUsername();
            if(RedisUtil.hasKey(key)){
                Object o =  RedisUtil.get(key);
                return com.alibaba.fastjson.JSONObject.parseObject(JSON.toJSONString(o), com.alibaba.fastjson.JSONObject.class).getString("tenantId");
            }
        }
        return null;
    }

    /**
     * 获取系统用户名称（登录名）
     * @return 系统用户名称
     */
    public static String getUsername() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            throw new BadRequestException(HttpStatus.UNAUTHORIZED, "当前登录状态过期");
        }
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return userDetails.getUsername();
    }

    /**
     * 获取系统用户id
     * @return 系统用户id
     */
    public static Long getUserId() {
        return getUserDetails().getId();
    }
}
