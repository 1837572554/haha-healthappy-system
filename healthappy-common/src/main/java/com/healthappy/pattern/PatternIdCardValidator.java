package com.healthappy.pattern;

import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.IdcardUtil;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PatternIdCardValidator implements ConstraintValidator<PatternIdCard, String> {


    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return IdcardUtil.isValidCard(value);
    }
}
