package com.healthappy.pattern;

import org.hibernate.validator.internal.constraintvalidators.bv.PatternValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 验证身份证
 * @author FGQ
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Constraint(validatedBy = {PatternIdCardValidator.class})
public @interface PatternIdCard {

    String regexp() default "";

    String message() default "身份证号不正确";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
