package com.healthappy.modules.system.service;

import com.healthappy.modules.system.service.dto.BusinessWorkloadDistinctDto;
import com.healthappy.modules.system.service.dto.DoctorPerformanceDto;
import com.healthappy.modules.system.service.dto.DoctorWorkloadDistinctDto;
import com.healthappy.modules.system.service.dto.DoctorWorkloadQueryCriteria;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author Jevany
 * @date 2021/11/26 14:20
 * @desc 医生工作量 接口
 */
public interface DoctorWorkloadService {
    /**
     * 获得医生检查工作量
     *
     * @param criteria 医生工作量统计查询类
     * @return java.util.List〈com.healthappy.modules.system.service.dto.DoctorCheckWorkloadDto〉
     * @author YJ
     * @date 2021/11/29 16:31
     */
    Map<String, Object> getDoctorCheckWorkload(DoctorWorkloadQueryCriteria criteria);

    /**
     * 获得医生业务工作量
     *
     * @param criteria 医生工作量统计查询类
     * @return java.util.List〈com.healthappy.modules.system.service.dto.DoctorBusinessWorkloadDto〉
     * @author YJ
     * @date 2021/11/29 16:48
     */
    Map<String, Object> getDoctorBusinessWorkload(DoctorWorkloadQueryCriteria criteria);

    /**
     * 获得医生绩效分
     *
     * @param criteria 医生工作量统计查询类
     * @return java.util.List〈com.healthappy.modules.system.service.dto.DoctorPerformanceDto〉
     * @author YJ
     * @date 2021/11/29 16:50
     */
    Map<String, Object> getDoctorPerformance(DoctorWorkloadQueryCriteria criteria);

    /**
     * 医生检查工作量导出Excel
     * @author YJ
     * @date 2022/1/25 10:06
     * @param all
     * @param response
     */
    void doctorCheckWorkloadExcel(List<DoctorWorkloadDistinctDto> all, HttpServletResponse response) throws IOException;

    /**
     * 医生业务工作量导出Excel
     * @author YJ
     * @date 2022/1/25 10:07
     * @param all
     * @param response
     */
    void doctorBusinessWorkloadExcel(List<BusinessWorkloadDistinctDto> all, HttpServletResponse response) throws IOException;

    /**
     * 医生绩效分导出Excel
     * @author YJ
     * @date 2022/1/25 10:08
     * @param all
     * @param response
     */
    void doctorPerformanceExcel(List<DoctorPerformanceDto> all, HttpServletResponse response) throws IOException;

}
