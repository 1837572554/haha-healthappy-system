package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.modules.system.service.dto.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description
 * @author FGQ
 * @date 2021-06-17
 */
@Mapper
@Repository
public interface RegistrationMapper {


    /**
     * 条码
     * @param id
     * @return
     */
    @SqlParser(filter = true)
    @Select("SELECT GROUP_CONCAT(DISTINCT replace(tih.group_name,' ','')) group_name,\n" +
            "IF(ISNULL(tih.barcode) or tih.barcode = 'NULL','123456789',tih.barcode)as barcode,\n" +
            "tp.`name` as user_name,tp.id as id,\n" +
            "tih.barcode_name as barcode_name,\n" +
            "(SELECT dd.`value` FROM dict di\n" +
            "LEFT JOIN dict_detail  dd on di.id = dd.dict_id\n" +
            "WHERE di.`Name` = 'sex' and dd.label = tp.sex) as user_sex,\n" +
            "tp.age as user_age\n" +
            "FROM T_Item_HD tih\n" +
            "LEFT JOIN T_Patient as tp on tih.pa_id = tp.id\n" +
            "WHERE tih.pa_id = #{id}\n" +
            "group by tih.barcode")
    List<BarCodeDto> barCode(@Param("id") String id);

    @SqlParser(filter = true)
    @Select("SELECT \n" +
            "tp.id id,\n" +
            "tp.name name,\n" +
            "(SELECT dd.`value` FROM dict di LEFT JOIN dict_detail  dd on di.id = dd.dict_id WHERE di.`Name` = 'sex' and dd.label = tp.sex) sex,\n" +
            "tp.age age,\n" +
            "(SELECT dd.`value` FROM dict di LEFT JOIN dict_detail  dd on di.id = dd.dict_id WHERE di.`Name` = 'marital' and dd.label = tp.marital) marital,\n" +
            "(SELECT bc.`name` FROM B_Company bc WHERE bc.id = tp.company_id)  company_name,\n" +
            "(SELECT bd.`name` FROM B_Department bd WHERE bd.id = tp.department_id) department_name,\n" +
            "tp.phone phone,\n" +
            "tp.pe_date pe_date,\n" +
            "t.`name` tenant_name,\n" +
            "t.tel_phone tel_phone,\n" +
            "IF(ISNULL(tih.barcode) or tih.barcode = 'NULL','123456789',tih.barcode)as barcode,\n" +
            "tih.group_name group_name,\n" +
            "bgh.instruction instruction,\n" +
            "bgh.eat eat,\n" +
            "tpo.id_image id_image,\n" +
            "tpo.portrait portrait,\n"+
            "tpz.poison_type poison,\n"+
            "(IF(tih.type_z = 1, 2, 1)) type\n" +
            "FROM T_Patient tp \n" +
            "LEFT JOIN T_Item_HD tih ON tih.pa_id = tp.id \n" +
            "LEFT JOIN t_photo tpo on tpo.id = tp.id\n"+
            "LEFT JOIN T_Patient_Z tpz on tpz.pa_id = tp.id\n"+
            "LEFT JOIN B_Group_HD bgh ON bgh.id = tih.group_id\n" +
            "LEFT JOIN tenant t ON t.tenant_id = tp.tenant_id\n" +
            "WHERE tp.id = #{id}")
    List<GuideSheetDto> guideSheet(String id);


    @SqlParser(filter = true)
    @Select("select poison_name from B_Poison where id = #{id}")
    String getByPaIdPoison(String id);

    @SqlParser(filter = true)
    @Select("SELECT \n" +
            "tp.id id,\n" +
            "tp.`name` name,\n" +
            "tp.id_no id_no,\n" +
            "tp.phone phone,\n" +
            "tpz.poison_type poison_type,\n"+
            "(SELECT IF(ISNULL(tih.barcode) or tih.barcode = 'NULL','123456789',tih.barcode)as barcode FROM T_Item_HD tih where tih.pa_id = tp.id limit 0,1) barcode,\n" +
            "t.`name` tenant_name,\n" +
            "(SELECT dd.`value` FROM dict di LEFT JOIN dict_detail  dd on di.id = dd.dict_id WHERE di.`Name` = 'sex' and dd.label = tp.sex) sex,\n" +
            "(SELECT dd.`value` FROM dict di LEFT JOIN dict_detail  dd on di.id = dd.dict_id WHERE di.`Name` = 'marital' and dd.label = tp.marital) marital,\n" +
            "(SELECT bc.`name` FROM B_Company bc WHERE bc.id = tp.company_id)  company_name,\n" +
            "(SELECT bd.`name` FROM B_Department bd WHERE bd.id = tp.department_id) department_name,\n" +
            "bpd.`name` as pe_type_dt_name,\n" +
            "tp.total_years total_years,\n" +
            "tpz.work_years work_years,\n" +
            "bpj.`name` job_name,\n" +
            "twz.bs_jwbs bs_jwbs,\n" +
            "twz.bs_zddw bs_zddw,\n" +
            "twz.bs_bm bs_bm,\n" +
            "twz.bs_zdsj bs_zdsj,\n" +
            "twz.bs_sfqy bs_sfqy,\n" +
            "twz.bs_cc bs_cc,\n" +
            "twz.bs_jq bs_jq,\n" +
            "twz.bs_zq bs_zq,\n" +
            "twz.bs_mcyj bs_mcyj,\n" +
            "twz.bs_xyzn bs_xyzn,\n" +
            "twz.bs_zc bs_zc,\n" +
            "twz.bs_sc bs_sc,\n" +
            "twz.bs_yct bs_yct,\n" +
            "twz.bs_ys bs_ys,\n" +
            "twz.bs_qt bs_qt,\n" +
            "twz.ys_yl ys_yl,\n" +
            "twz.ys_ns ys_ns,\n" +
            "twz.js_jl js_jl,\n" +
            "twz.js_ns js_ns\n" +
            "FROM T_Patient tp\n" +
            "LEFT JOIN tenant t ON t.tenant_id = tp.tenant_id\n" +
            "LEFT JOIN B_PeType_DT bpd ON bpd.id = tp.pe_type_dt_id\n" +
            "LEFT JOIN T_Patient_Z tpz on tpz.pa_id = tp.id\n" +
            "LEFT JOIN B_Petype_Job bpj on bpj.id = tp.job\n" +
            "LEFT JOIN T_Wz twz on twz.pa_id = tp.id\n" +
            "WHERE tp.id = #{id}")
    InformationSheetDto informationSheet(@Param("id") String id);


    @SqlParser(filter = true)
    @Select("select * from T_Record where pa_id = #{id}")
    List<InformationSheetZybDto> informationSheetItemList(@Param("id") String id);

    @Select("SELECT \n" +
            "tp.id `code`,\n" +
            "tp.`name` `name`,\n" +
            "CONCAT((select dd.value from dict_detail dd WHERE dd.dict_id = (select d.id from dict d where d.name = 'sex') and dd.label = tp.sex),\n" +
            "'(',tp.age,')岁') sex_and_age,\n" +
            "bc.`name` company,\n" +
            "bd.`name` dept,\n" +
            "tp.phone tel,\n" +
            "tp.report_date time,\n" +
            "t.`name` tenant,\n" +
            "t.img logo,\n" +
            "t.address address,\n" +
            "t.tel_phone tenant_tel\n" +
            "from T_Patient tp \n" +
            "LEFT JOIN B_Company bc on bc.id = tp.company_id\n" +
            "LEFT JOIN B_Department bd on bd.id = tp.department_id\n" +
            "LEFT JOIN tenant t on tp.tenant_id = t.tenant_id\n" +
            "WHERE tp.id = #{id}")
    @SqlParser(filter = true)
    List<HealthyMedicalHomePageDto> healthyMedicalHomePage(@Param("id") String id);


    @Select("SELECT \n" +
            "tih.group_name,\n" +
            "(IF(tih.result is not null,'已检','未检')) state,\n" +
            "DATE_FORMAT(tp.pe_date, '%Y-%m-%d') pe_date \n" +
            "FROM T_Patient tp\n" +
            "LEFT JOIN T_Item_HD tih on tp.id = tih.pa_id\n" +
            "LEFT JOIN B_Group_HD bgh on bgh.id = tih.group_id\n" +
            "WHERE tp.id = #{id} \n" +
            "and ${s} EXISTS (\n" +
            "select * from dict_detail dd WHERE dd.dict_id = (select d.id from dict d where d.name = 'groupHdType') and dd.`value` = '检验项目' and dd.label = bgh.type \n" +
            ")")
    @SqlParser(filter = true)
    List<HealthyItemResultDto> healthyItem(@Param("id") String id,@Param("s") String s);
}
