package com.healthappy.modules.system.rest;

import cn.hutool.core.util.StrUtil;
import com.healthappy.annotation.AnonymousAccess;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.service.DeptWorkloadService;
import com.healthappy.modules.system.service.dto.DeptWorkloadQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description 科室工作量统计
 * @author FGQ
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "统计报表：科室工作量统计")
@RestController
@AllArgsConstructor
@RequestMapping("/statistic")
public class DeptWorkloadController {

    private final  DeptWorkloadService deptWorkloadService;

    @Log("科室工作量统计")
    @ApiOperation("科室工作量统计")
    @GetMapping("/dept-workload")
    @PreAuthorize("@el.check('statistic:deptWorkload')")
    public R deptWorkload(DeptWorkloadQueryCriteria criteria) {
        log.info("------科室工作量统计------");
        if(null != criteria.getDateType()){
            if(StrUtil.isAllBlank(criteria.getStartTime(),criteria.getEndTime())){
                return R.error("请选择时间");
            }
        }
        return R.ok(deptWorkloadService.deptWorkload(criteria));
    }
}
