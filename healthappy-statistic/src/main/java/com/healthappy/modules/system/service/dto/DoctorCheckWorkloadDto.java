package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @desc: 医生检查工作量统计 Dto
 * @author: YJ
 * @date: 2021-11-27 10:32
 **/
@Data
public class DoctorCheckWorkloadDto {

    /**
     * 体检医生
     */
    @ApiModelProperty("体检医生")
    private String doctorName;

    /**
     * 医生类型
     */
    @ApiModelProperty("医生类型")
    private String doctorType;

    /**
     * 科室名称
     */
    @ApiModelProperty("科室名称")
    private String deptType;

    /**
     * groupName
     */
    @ApiModelProperty("项目名称")
    private String groupName;

    /**
     * 项次数
     */
    @ApiModelProperty("项次数")
    private Long itemCount;

    /**
     * 体检分类名称
     */
    @ApiModelProperty("体检分类名称")
    private String peTypeHdName;

    /**
     * 体检分类次数
     */
    @ApiModelProperty("体检分类次数")
    private Long peTypeHdCount;

    /**
     * 检查权重
     */
    @ApiModelProperty("检查权重")
    private Double checkWeight;

    /**
     * 得分
     */
    @ApiModelProperty("得分")
    private Double score;

    /**
     * 总分
     */
    @ApiModelProperty("总分")
    private Double totalScore;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remark;


}
