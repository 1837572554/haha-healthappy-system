package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class TypeNumberDto {

    @ApiModelProperty("分类")
    private String fl;

    @ApiModelProperty("类别")
    private String lb;

    @ApiModelProperty("分类人数")
    private Long flrs;

    @ApiModelProperty("类别人数")
    private Long lbrs;

    private String flId;

    private String lbId;
}
