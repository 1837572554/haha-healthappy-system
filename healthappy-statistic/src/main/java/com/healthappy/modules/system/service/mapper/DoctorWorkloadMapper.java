package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.modules.system.service.dto.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Jevany
 * @date 2021/11/29 16:59
 * @desc 医生工作量统计Mapper
 */
@Mapper
@Repository
public interface DoctorWorkloadMapper {

     /**
      * 获取医生基础数据
      * @param criteria
      * @return
      */
     @SqlParser(filter = true)
     List<CheckWorkloadDto> obtainBasicDoctorData(@Param("criteria") DoctorWorkloadQueryCriteria criteria);


     //region 检查工作量
     /**
      * 获得医生工作量的总数据
      * @author YJ
      * @date 2022/1/17 16:49
      * @param criteria
      * @return java.util.List〈com.healthappy.modules.system.service.dto.DoctorWorkloadTotalDataDto〉
      */
     @SqlParser(filter = true)
     List<DoctorWorkloadTotalDataDto> doctorWorkloadTotalData(@Param("criteria") DoctorWorkloadQueryCriteria criteria);

     /**
      * 获得Distinct后的医生工作量数据
      * @author YJ
      * @date 2022/1/17 18:46
      * @param criteria
      * @return java.util.List〈com.healthappy.modules.system.service.dto.DoctorWorkloadDistinctDto〉
      */
     @SqlParser(filter = true)
     List<DoctorWorkloadDistinctDto> doctorWorkloadDistinct(@Param("criteria") DoctorWorkloadQueryCriteria criteria);

     //endregion 检查工作量

     //region 业务工作量

     //region 总检工作量


     /**
      * 总检医生总数据
      * @author YJ
      * @date 2022/1/18 18:47
      * @param criteria
      * @return java.util.List〈com.healthappy.modules.system.service.dto.BusinessWorkloadTotalDataDto〉
      */
     @SqlParser(filter = true)
     List<BusinessWorkloadTotalDataDto> conclusionBusinessWorkloadTotalData(@Param("criteria") DoctorWorkloadQueryCriteria criteria);

     /**
      * 总检医生Distinct数据
      * @author YJ
      * @date 2022/1/19 11:41
      * @param criteria
      * @return java.util.List〈com.healthappy.modules.system.service.dto.BusinessWorkloadDistinctDto〉
      */
     @SqlParser(filter = true)
     List<BusinessWorkloadDistinctDto> conclusionBusinessWorkloadDistinct(@Param("criteria") DoctorWorkloadQueryCriteria criteria);

     //endregion 总检工作量

     //region 审核工作量

     /**
      * 审核医生总数据
      * @author YJ
      * @date 2022/1/18 18:47
      * @param criteria
      * @return java.util.List〈com.healthappy.modules.system.service.dto.BusinessWorkloadTotalDataDto〉
      */
     @SqlParser(filter = true)
     List<BusinessWorkloadTotalDataDto> verifyBusinessWorkloadTotalData(@Param("criteria") DoctorWorkloadQueryCriteria criteria);

     /**
      * 审核医生Distinct数据
      * @author YJ
      * @date 2022/1/19 11:41
      * @param criteria
      * @return java.util.List〈com.healthappy.modules.system.service.dto.BusinessWorkloadDistinctDto〉
      */
     @SqlParser(filter = true)
     List<BusinessWorkloadDistinctDto> verifyBusinessWorkloadDistinct(@Param("criteria") DoctorWorkloadQueryCriteria criteria);



     //endregion 审核工作量

     //region 登记工作量

     /**
      * 登记医生总数据
      * @author YJ
      * @date 2022/1/18 18:47
      * @param criteria
      * @return java.util.List〈com.healthappy.modules.system.service.dto.BusinessWorkloadTotalDataDto〉
      */
     @SqlParser(filter = true)
     List<BusinessWorkloadTotalDataDto> registerBusinessWorkloadTotalData(@Param("criteria") DoctorWorkloadQueryCriteria criteria);

     /**
      * 登记医生Distinct数据
      * @author YJ
      * @date 2022/1/19 11:41
      * @param criteria
      * @return java.util.List〈com.healthappy.modules.system.service.dto.BusinessWorkloadDistinctDto〉
      */
     @SqlParser(filter = true)
     List<BusinessWorkloadDistinctDto> registerBusinessWorkloadDistinct(@Param("criteria") DoctorWorkloadQueryCriteria criteria);



     //endregion 登记工作量

     //region 预约工作量

     /**
      * 预约医生总数据
      * @author YJ
      * @date 2022/1/18 18:47
      * @param criteria
      * @return java.util.List〈com.healthappy.modules.system.service.dto.BusinessWorkloadTotalDataDto〉
      */
     @SqlParser(filter = true)
     List<BusinessWorkloadTotalDataDto> appointBusinessWorkloadTotalData(@Param("criteria") DoctorWorkloadQueryCriteria criteria);

     /**
      * 预约医生Distinct数据
      * @author YJ
      * @date 2022/1/19 11:41
      * @param criteria
      * @return java.util.List〈com.healthappy.modules.system.service.dto.BusinessWorkloadDistinctDto〉
      */
     @SqlParser(filter = true)
     List<BusinessWorkloadDistinctDto> appointBusinessWorkloadDistinct(@Param("criteria") DoctorWorkloadQueryCriteria criteria);



     //endregion 预约工作量




     //endregion 业务工作量

     //region 绩效分
     @SqlParser(filter = true)
     List<DoctorPerformanceDto> performanceDoctorWorkload(@Param("criteria") DoctorWorkloadQueryCriteria criteria);


     /**
      * 计算绩效分总数
      * @author YJ
      * @date 2022/1/20 15:43
      * @param criteria
      * @return java.lang.Integer
      */
     @SqlParser(filter = true)
     Integer performanceDoctorWorkloadCount(@Param("criteria") DoctorWorkloadQueryCriteria criteria);
     //endregion 绩效分
}
