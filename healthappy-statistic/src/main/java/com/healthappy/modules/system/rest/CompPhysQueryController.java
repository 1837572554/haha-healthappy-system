package com.healthappy.modules.system.rest;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.service.CompPhysQueryService;
import com.healthappy.modules.system.service.dto.CompPhysQueryCriteria;
import com.healthappy.modules.system.service.dto.CompanyGroupDto;
import com.healthappy.modules.system.service.dto.CompanyPackageDto;
import com.healthappy.utils.R;
import com.healthappy.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author Jevany
 * @date 2022/1/5 10:40
 * @desc 单位体检情况查询 控制器
 */
@Slf4j
@Api(tags = "统计报表：单位体检情况")
@RestController
@AllArgsConstructor
@RequestMapping("/statistic")
public class CompPhysQueryController {

    private final CompPhysQueryService compPhysQueryService;

    @Log("单位体检情况查询")
    @ApiOperation("单位体检情况查询，权限码：TPatient:list")
    @GetMapping("/companyPhysicalExamination")
    @PreAuthorize("@el.check('statistic:com:list')")
    @ApiResponses(value = {
            @ApiResponse(code = 200,message = "单位套餐项目情况",response = CompanyPackageDto.class),
            @ApiResponse(code = 201,message = "单位项目使用情况",response = CompanyGroupDto.class)
    })
    public R companyPhysicalExamination(CompPhysQueryCriteria criteria){
        if(null != criteria.getDateType()){
            if(StrUtil.isAllBlank(criteria.getStartTime(),criteria.getEndTime())){
                return R.error("请选择时间");
            }
        }
        if(ObjectUtil.isEmpty(criteria.getType())){
            return R.error("请选择接口类型");
        }
        if(StrUtil.isBlank(criteria.getTenantId())){
            criteria.setTenantId(SecurityUtils.getTenantId());
        }
        if(criteria.getIsExport().equals(false)){
            if (ObjectUtil.isEmpty(criteria.getPageNum()) || criteria.getPageNum() <= 0) {
                criteria.setPageNum(1);
            }
            if (ObjectUtil.isEmpty(criteria.getPageSize()) || criteria.getPageSize() <= 0) {
                criteria.setPageSize(10);
            }
        }

        if(criteria.getType().equals(1)){
            return R.ok(compPhysQueryService.getCompanyPackageList(criteria));
        }
        else if(criteria.getType().equals(2)){
            return R.ok(compPhysQueryService.getCompanyGroupList(criteria));
        }
        else{
            return R.error("接口类型错误");
        }
    }


    @Log("导出单位体检情况Excel")
    @ApiOperation("导出单位体检情况Excel，权限码：TPatient:list")
    @GetMapping("/companyPhysicalExaminationExcel")
    @PreAuthorize("@el.check('statistic:com:upload')")
    public void companyPhysicalExaminationExcel(HttpServletResponse response, CompPhysQueryCriteria criteria) throws IOException {
        if(null != criteria.getDateType()){
            if(StrUtil.isAllBlank(criteria.getStartTime(),criteria.getEndTime())){
                throw new BadRequestException("请选择时间");
            }
        }
        if(ObjectUtil.isEmpty(criteria.getType())){
            throw new BadRequestException("请选择接口类型");
        }
        if(StrUtil.isBlank(criteria.getTenantId())){
            criteria.setTenantId(SecurityUtils.getTenantId());
        }
        criteria.setIsExport(true);

        if(criteria.getType().equals(1)){
            Map<String, Object> companyPackageList = compPhysQueryService.getCompanyPackageList(criteria);
            compPhysQueryService.companyPackageListDown((List<CompanyPackageDto>)companyPackageList.get("content"),response);
        }
        else if(criteria.getType().equals(2)){
            Map<String, Object> companyGroupList = compPhysQueryService.getCompanyGroupList(criteria);
            compPhysQueryService.companyGroupListDown((List<CompanyGroupDto>)companyGroupList.get("content"),response);
        }
        else{
            throw new BadRequestException("接口类型错误");
        }
    }

}
