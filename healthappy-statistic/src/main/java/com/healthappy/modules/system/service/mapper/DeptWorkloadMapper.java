package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.modules.system.service.dto.DeptWorkloadDto;
import com.healthappy.modules.system.service.dto.DeptWorkloadQueryCriteria;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description
 * @author FGQ
 * @date 2021-06-17
 */
@Mapper
@Repository
public interface DeptWorkloadMapper {


    /**
     * 获取科室和体检项目
     * @return
     */
    @SqlParser(filter = true)
    List<DeptWorkloadDto> getDeptAndItemHdList(@Param("criteria") DeptWorkloadQueryCriteria criteria);

    /**
     * 根据分组ID获取登记的计算数据
     * @param criteria
     * @param groupId
     * @return
     */
    @SqlParser(filter = true)
    DeptWorkloadDto getByItemHd(@Param("groupId") String groupId, @Param("criteria") DeptWorkloadQueryCriteria criteria);
}
