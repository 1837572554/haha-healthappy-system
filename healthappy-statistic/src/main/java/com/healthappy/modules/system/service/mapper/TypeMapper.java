package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.modules.system.service.dto.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description
 * @author FGQ
 * @date 2021-06-17
 */
@Mapper
@Repository
public interface TypeMapper {

    /**
     * 分类集合查询
     * @param criteria
     * @return
     */
    @SqlParser(filter = true)
    List<TypeDto> getTypeList(@Param("criteria") TypeQueryCriteria criteria);

    /**
     * 类别查询集合
     * @param criteria
     * @return
     */
    @SqlParser(filter = true)
    List<CategoryDto> getCategoryList(@Param("criteria") TypeQueryCriteria criteria);


    /**
     * 获取分类和类别
     * @return
     */
    @SqlParser(filter = true)
    List<TypeNumberDto> getTypeCategoryList(@Param("criteria") TypeQueryCriteria criteria);

    /**
     * 获取体检者
     * @param criteria
     * @return
     */
    @SqlParser(filter = true)
    List<TypeNumberPatientDto> getPatientList(@Param("criteria") TypeQueryCriteria criteria);
}
