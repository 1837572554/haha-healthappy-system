package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Jevany
 * @date 2022/1/18 18:33
 * @desc
 */
@ApiModel("医生业务工作量统计")
@Data
public class BusinessWorkloadDistinctDto extends BusinessWorkloadTotalDataDto {

    /** 医生类型名称 */
    @ApiModelProperty("医生类型名称")
    private String doctorType;

    /** 业务名称 */
    @ApiModelProperty("业务名称")
    private String businessName;

    /** 科室名称 */
    @ApiModelProperty("科室名称")
    private String deptName;

    /** 组合项名称 */
    @ApiModelProperty("组合项名称")
    private String groupName;

    /** 组合项次数 */
    @ApiModelProperty("组合项次数")
    private Long groupCnt;

    /** 组合项权重 */
    @ApiModelProperty("组合项权重")
    private Double checkWeight;

    /** 得分 权重*项次数 */
    @ApiModelProperty("得分 权重*项次数")
    private Double score;

    /** 总分 人员总得分*/
    @ApiModelProperty("总分 人员总得分")
    private Double totalScore;
}
