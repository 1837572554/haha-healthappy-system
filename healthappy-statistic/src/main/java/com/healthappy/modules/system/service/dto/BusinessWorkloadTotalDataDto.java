package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Jevany
 * @date 2022/1/18 18:17
 * @desc 业务工作量总数据
 */
@Data
public class BusinessWorkloadTotalDataDto {

    /** 医生名称 */
    @ApiModelProperty("医生名称")
    private String doctor;

    /** 科室编号 */
    @ApiModelProperty("科室编号")
    private String deptId;

    /** 组合项编号 */
    @ApiModelProperty("组合项编号")
    private String groupId;
}
