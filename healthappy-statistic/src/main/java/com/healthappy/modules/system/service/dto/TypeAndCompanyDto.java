package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class TypeAndCompanyDto {

    @ApiModelProperty("分类")
    private String fl;

    @ApiModelProperty("类别")
    private String lb;

    private String flId;

    private String lbId;

    @ApiModelProperty("分类单位数量")
    private Long typeCompanyAmount;

    @ApiModelProperty("类别单位数量")
    private Long typeDtCompanyAmount;
}
