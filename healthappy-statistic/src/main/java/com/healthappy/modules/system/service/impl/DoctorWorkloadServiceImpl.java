package com.healthappy.modules.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.healthappy.exception.BadRequestException;
import com.healthappy.modules.system.service.DoctorWorkloadService;
import com.healthappy.modules.system.service.dto.*;
import com.healthappy.modules.system.service.mapper.DoctorWorkloadMapper;
import com.healthappy.utils.FileUtil;
import com.healthappy.utils.PageUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @desc: 医生工作量服务接口 实现类
 * @author: YJ
 * @date: 2021-11-29 16:52
 **/

@Service
//@DS("click")
@AllArgsConstructor
public class DoctorWorkloadServiceImpl implements DoctorWorkloadService {

    private final DoctorWorkloadMapper doctorWorkloadMapper;

    /**
     * 获得医生检查工作量
     *
     * @param criteria 医生工作量统计查询类
     * @return java.util.List〈com.healthappy.modules.system.service.dto.DoctorCheckWorkloadDto〉
     * @author YJ
     * @date 2021/11/29 16:31
     */
    @Override
    public Map<String, Object> getDoctorCheckWorkload(DoctorWorkloadQueryCriteria criteria) {
        /** 总数据 */
        List<DoctorWorkloadTotalDataDto> totalData = doctorWorkloadMapper.doctorWorkloadTotalData(criteria);

        /** 过滤后的数据 */
        List<DoctorWorkloadDistinctDto> doctorWorkloadDistinctDtos = doctorWorkloadMapper.doctorWorkloadDistinct(criteria);

        //总条数
        Integer totalCount = doctorWorkloadDistinctDtos.size();

        Integer[] pageBase = null;
        if (criteria.getIsExport().equals(false)) {
            //计算分页-页数
            pageBase = PageUtil.toStartEnd(criteria.getPageNum(), criteria.getPageSize(), totalCount);
            //获得分页数据
            doctorWorkloadDistinctDtos = doctorWorkloadDistinctDtos.subList(pageBase[0], pageBase[1]);
        }

        doctorWorkloadDistinctDtos.forEach(d -> {
            //体检分类次数
            d.setPeTypeHdCnt(totalData.stream().filter(td -> td.getDeptId().equals(d.getDeptId()) && td.getDoctor().equals(d.getDoctor()) && td.getGroupId().equals(d.getGroupId()) && td.getPeTypeHdId().equals(d.getPeTypeHdId())).count());
            if (ObjectUtil.isNull(d.getPeTypeHdCnt())) {
                d.setPeTypeHdCnt(0L);
            }
            //项次数
            d.setGroupCnt(totalData.stream().filter(td -> td.getDeptId().equals(d.getDeptId()) && td.getDoctor().equals(d.getDoctor()) && td.getGroupId().equals(d.getGroupId())).count());
            if (ObjectUtil.isNull(d.getGroupCnt())) {
                d.setGroupCnt(0L);
            }
            //得分
            d.setScore(d.getCheckWeight() * d.getGroupCnt());
        });

        Map<String, Double> mapScore = doctorWorkloadDistinctDtos.stream().collect(Collectors.toMap(DoctorWorkloadDistinctDto::getDoctor, DoctorWorkloadDistinctDto::getScore, Double::sum));
        doctorWorkloadDistinctDtos.forEach(d -> {
            d.setTotalScore(mapScore.get(d.getDoctor()));
        });

        if (criteria.getIsExport().equals(false)) {
            Map<String, Object> map = new LinkedHashMap<>(5);
            map.put("content", doctorWorkloadDistinctDtos);
            map.put("totalElements", totalCount);
            map.put("totalPage", pageBase[3]);
            map.put("currentPage", pageBase[2]);
            map.put("pageSize", criteria.getPageSize());
            return map;
        } else {
            Map<String, Object> map = new LinkedHashMap<>(1);
            map.put("content", doctorWorkloadDistinctDtos);
            return map;
        }
    }

    /**
     * 获得医生业务工作量
     *
     * @param criteria 医生工作量统计查询类
     * @return java.util.List〈com.healthappy.modules.system.service.dto.DoctorBusinessWorkloadDto〉
     * @author YJ
     * @date 2021/11/29 16:48
     */
    @Override
    public Map<String, Object> getDoctorBusinessWorkload(DoctorWorkloadQueryCriteria criteria) {
        List<BusinessWorkloadDistinctDto> totalData = new ArrayList<>();
        switch (criteria.getBusinessType()) {
            case 0:
                /** 总检 */
                totalData.addAll(this.getConclusionBusinessWorkload(criteria));
                /** 审核 */
                totalData.addAll(this.getVerifyBusinessWorkload(criteria));
                /** 登记 */
                totalData.addAll(this.getRegisterBusinessWorkload(criteria));
                /** 预约 */
                totalData.addAll(this.getAppointBusinessWorkload(criteria));
                break;
            case 1:
                /** 总检 */
                totalData.addAll(this.getConclusionBusinessWorkload(criteria));
                break;
            case 2:
                /** 审核 */
                totalData.addAll(this.getVerifyBusinessWorkload(criteria));
                break;
            case 3:
                /** 登记 */
                totalData.addAll(this.getRegisterBusinessWorkload(criteria));
                break;
            case 4:
                /** 预约 */
                totalData.addAll(this.getAppointBusinessWorkload(criteria));
                break;
            default:
                throw new BadRequestException("业务类型不支持");
        }

        //总条数
        Integer totalCount = totalData.size();

        Integer[] pageBase = null;
        if (criteria.getIsExport().equals(false)) {
            //计算分页-页数
            pageBase = PageUtil.toStartEnd(criteria.getPageNum(), criteria.getPageSize(), totalCount);
            //获得分页数据
            totalData = totalData.subList(pageBase[0], pageBase[1]);
        }

        if (criteria.getIsExport().equals(false)) {

            Map<String, Object> map = new LinkedHashMap<>(5);
            map.put("content", totalData);
            map.put("totalElements", totalCount);
            map.put("totalPage", pageBase[3]);
            map.put("currentPage", pageBase[2]);
            map.put("pageSize", criteria.getPageSize());

            return map;
        } else {
            Map<String, Object> map = new LinkedHashMap<>(1);
            map.put("content", totalData);
            return map;
        }


//        /** 总检 */
//        List<BusinessWorkloadDistinctDto> conclusionBusinessWorkload = this.getConclusionBusinessWorkload(criteria);
//        /** 审核 */
//        List<BusinessWorkloadDistinctDto> verifyBusinessWorkload = this.getVerifyBusinessWorkload(criteria);
//        /** 登记 */
//        List<BusinessWorkloadDistinctDto> registerBusinessWorkload = this.getRegisterBusinessWorkload(criteria);
//        /** 预约 */
//        List<BusinessWorkloadDistinctDto> appointBusinessWorkload = this.getAppointBusinessWorkload(criteria);

//        return null;
    }

    //region 医生工作量 相关方法

    /**
     * 总检业务工作量统计数据
     *
     * @param criteria
     * @return java.util.List〈com.healthappy.modules.system.service.dto.BusinessWorkloadDistinctDto〉
     * @author YJ
     * @date 2022/1/18 19:30
     */
    public List<BusinessWorkloadDistinctDto> getConclusionBusinessWorkload(DoctorWorkloadQueryCriteria criteria) {
        /** 总数据 */
        List<BusinessWorkloadTotalDataDto> totalData = doctorWorkloadMapper.conclusionBusinessWorkloadTotalData(criteria);
        /** 过滤后的数据 */
        List<BusinessWorkloadDistinctDto> businessWorkloadDistincts = doctorWorkloadMapper.conclusionBusinessWorkloadDistinct(criteria);

        businessWorkloadDistincts.forEach(d -> {
            d.setBusinessName("总检");

            /** 组合项次数统计 */
            d.setGroupCnt(totalData.stream().filter(td -> td.getDoctor().equals(d.getDoctor())
                    && td.getDeptId().equals(d.getDeptId()) && td.getGroupId().equals(d.getGroupId())).count());

            /** 得分 */
            d.setScore(d.getCheckWeight() * d.getGroupCnt());
        });

        Map<String, Double> mapScore = businessWorkloadDistincts.stream().collect(Collectors.toMap(BusinessWorkloadDistinctDto::getDoctor, BusinessWorkloadDistinctDto::getScore, Double::sum));
        businessWorkloadDistincts.forEach(d -> {
            d.setTotalScore(mapScore.get(d.getDoctor()));
        });

        return businessWorkloadDistincts;
    }

    /**
     * 审核业务工作量统计数据
     *
     * @param criteria
     * @return java.util.List〈com.healthappy.modules.system.service.dto.BusinessWorkloadDistinctDto〉
     * @author YJ
     * @date 2022/1/18 19:30
     */
    public List<BusinessWorkloadDistinctDto> getVerifyBusinessWorkload(DoctorWorkloadQueryCriteria criteria) {
        /** 总数据 */
        List<BusinessWorkloadTotalDataDto> totalData = doctorWorkloadMapper.verifyBusinessWorkloadTotalData(criteria);
        /** 过滤后的数据 */
        List<BusinessWorkloadDistinctDto> businessWorkloadDistincts = doctorWorkloadMapper.verifyBusinessWorkloadDistinct(criteria);

        businessWorkloadDistincts.forEach(d -> {
            d.setBusinessName("审核");

            /** 组合项次数统计 */
            d.setGroupCnt(totalData.stream().filter(td -> td.getDoctor().equals(d.getDoctor())
                    && td.getDeptId().equals(d.getDeptId()) && td.getGroupId().equals(d.getGroupId())).count());

            /** 得分 */
            d.setScore(d.getCheckWeight() * d.getGroupCnt());
        });

        Map<String, Double> mapScore = businessWorkloadDistincts.stream().collect(Collectors.toMap(BusinessWorkloadDistinctDto::getDoctor, BusinessWorkloadDistinctDto::getScore, Double::sum));
        businessWorkloadDistincts.forEach(d -> {
            d.setTotalScore(mapScore.get(d.getDoctor()));
        });

        return businessWorkloadDistincts;
    }

    /**
     * 登记业务工作量统计数据
     *
     * @param criteria
     * @return java.util.List〈com.healthappy.modules.system.service.dto.BusinessWorkloadDistinctDto〉
     * @author YJ
     * @date 2022/1/18 19:30
     */
    public List<BusinessWorkloadDistinctDto> getRegisterBusinessWorkload(DoctorWorkloadQueryCriteria criteria) {
        /** 总数据 */
        List<BusinessWorkloadTotalDataDto> totalData = doctorWorkloadMapper.registerBusinessWorkloadTotalData(criteria);
        /** 过滤后的数据 */
        List<BusinessWorkloadDistinctDto> businessWorkloadDistincts = doctorWorkloadMapper.registerBusinessWorkloadDistinct(criteria);

        businessWorkloadDistincts.forEach(d -> {
            d.setBusinessName("登记");

            /** 组合项次数统计 */
            d.setGroupCnt(totalData.stream().filter(td -> td.getDoctor().equals(d.getDoctor())
                    && td.getDeptId().equals(d.getDeptId()) && td.getGroupId().equals(d.getGroupId())).count());

            /** 得分 */
            d.setScore(d.getCheckWeight() * d.getGroupCnt());
        });

        Map<String, Double> mapScore = businessWorkloadDistincts.stream().collect(Collectors.toMap(BusinessWorkloadDistinctDto::getDoctor, BusinessWorkloadDistinctDto::getScore, Double::sum));
        businessWorkloadDistincts.forEach(d -> {
            d.setTotalScore(mapScore.get(d.getDoctor()));
        });

        return businessWorkloadDistincts;
    }

    /**
     * 登记业务工作量统计数据
     *
     * @param criteria
     * @return java.util.List〈com.healthappy.modules.system.service.dto.BusinessWorkloadDistinctDto〉
     * @author YJ
     * @date 2022/1/18 19:30
     */
    public List<BusinessWorkloadDistinctDto> getAppointBusinessWorkload(DoctorWorkloadQueryCriteria criteria) {
        /** 总数据 */
        List<BusinessWorkloadTotalDataDto> totalData = doctorWorkloadMapper.appointBusinessWorkloadTotalData(criteria);
        /** 过滤后的数据 */
        List<BusinessWorkloadDistinctDto> businessWorkloadDistincts = doctorWorkloadMapper.appointBusinessWorkloadDistinct(criteria);

        businessWorkloadDistincts.forEach(d -> {
            d.setBusinessName("预约");

            /** 组合项次数统计 */
            d.setGroupCnt(totalData.stream().filter(td -> td.getDoctor().equals(d.getDoctor())
                    && td.getDeptId().equals(d.getDeptId()) && td.getGroupId().equals(d.getGroupId())).count());

            /** 得分 */
            d.setScore(d.getCheckWeight() * d.getGroupCnt());
        });

        Map<String, Double> mapScore = businessWorkloadDistincts.stream().collect(Collectors.toMap(BusinessWorkloadDistinctDto::getDoctor, BusinessWorkloadDistinctDto::getScore, Double::sum));
        businessWorkloadDistincts.forEach(d -> {
            d.setTotalScore(mapScore.get(d.getDoctor()));
        });

        return businessWorkloadDistincts;
    }

    //endregion 医生工作量 相关方法


    /**
     * 获得医生绩效分
     *
     * @param criteria 医生工作量统计查询类
     * @return java.util.List〈com.healthappy.modules.system.service.dto.DoctorPerformanceDto〉
     * @author YJ
     * @date 2021/11/29 16:50
     */
    @Override
    public Map<String, Object> getDoctorPerformance(DoctorWorkloadQueryCriteria criteria) {

        if (criteria.getIsExport().equals(false)) {
            /** 总条数 */
            Integer totalCnt = doctorWorkloadMapper.performanceDoctorWorkloadCount(criteria);

            Integer[] pageInfo = PageUtil.toStartEnd(criteria.getPageNum(), criteria.getPageSize(), totalCnt);

            criteria.setStartIndex(pageInfo[0]);
            List<DoctorPerformanceDto> doctorPerformanceDtos = doctorWorkloadMapper.performanceDoctorWorkload(criteria);

            Map<String, Object> map = new LinkedHashMap<>(5);
            map.put("content", doctorPerformanceDtos);
            map.put("totalElements", totalCnt);
            map.put("totalPage", pageInfo[3]);
            map.put("currentPage", pageInfo[2]);
            map.put("pageSize", criteria.getPageSize());

            return map;
        } else {
            criteria.setStartIndex(-1);
            List<DoctorPerformanceDto> doctorPerformanceDtos = doctorWorkloadMapper.performanceDoctorWorkload(criteria);
            Map<String, Object> map = new LinkedHashMap<>(1);
            map.put("content", doctorPerformanceDtos);

            return map;
        }


    }

    /**
     * 医生检查工作量导出Excel
     *
     * @param all
     * @param response
     * @author YJ
     * @date 2022/1/25 10:06
     */
    @Override
    public void doctorCheckWorkloadExcel(List<DoctorWorkloadDistinctDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        Integer idx = 0;
        for (DoctorWorkloadDistinctDto doctorWorkloadDistinctDto : all) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("序号", ++idx);
            map.put("体检医生", doctorWorkloadDistinctDto.getDoctor());
            map.put("医生类型", doctorWorkloadDistinctDto.getDoctorType());
            map.put("科室名称", doctorWorkloadDistinctDto.getDeptName());
            map.put("项目名称", doctorWorkloadDistinctDto.getGroupName());
            map.put("项次数", doctorWorkloadDistinctDto.getGroupCnt());
            map.put("体检分类", doctorWorkloadDistinctDto.getPeTypeHdName());
            map.put("分类次数", doctorWorkloadDistinctDto.getPeTypeHdCnt());
            map.put("检查权重", doctorWorkloadDistinctDto.getCheckWeight());
            map.put("得分", doctorWorkloadDistinctDto.getScore());
            map.put("总得分", doctorWorkloadDistinctDto.getTotalScore());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    /**
     * 医生业务工作量导出Excel
     *
     * @param all
     * @param response
     * @author YJ
     * @date 2022/1/25 10:07
     */
    @Override
    public void doctorBusinessWorkloadExcel(List<BusinessWorkloadDistinctDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        Integer idx = 0;
        for (BusinessWorkloadDistinctDto dto : all) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("序号", ++idx);
            map.put("医生名称", dto.getDoctor());
            map.put("医生类型", dto.getDoctorType());
            map.put("体检业务", dto.getBusinessName());
            map.put("科室名称", dto.getDeptName());
            map.put("项目名称", dto.getGroupName());
            map.put("完成人数", dto.getGroupCnt());
            map.put("权重", dto.getCheckWeight());
            map.put("权重得分", dto.getScore());
            map.put("总分", dto.getTotalScore());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    /**
     * 医生绩效分导出Excel
     * @param all
     * @param response
     * @author YJ
     * @date 2022/1/25 10:08
     */
    @Override
    public void doctorPerformanceExcel(List<DoctorPerformanceDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        Integer idx = 0;
        for (DoctorPerformanceDto dto : all) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("序号", ++idx);
            map.put("姓名", dto.getDoctor());
            map.put("检查工作量", dto.getCheckWorkload());
            map.put("总检工作量", dto.getConclusionWorkload());
            map.put("审核工作量", dto.getVerifyWorkload());
            map.put("总工作量", dto.getTotalWorkload());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}
