package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Data
public class TypeQueryCriteria {

    @ApiModelProperty("事件类型，1：登记时间,2：签到时间,3：总检时间")
    private Integer dateType;

    @ApiModelProperty("开始时间")
    private String startTime;

    @ApiModelProperty("结束时间")
    private String endTime;

    @ApiModelProperty("单位ID")
    private String companyId;

    @ApiModelProperty("集团ID")
    private String parentId;

    @ApiModelProperty("类别ID")
    private String peTypeDtId;

    @NotBlank(message = "租户ID不能为空")
    @ApiModelProperty(value = "租户ID",required = true)
    private String tenantId;

    @NotNull
    @ApiModelProperty(value = "1:单位及体检分类统计,2:单位及体检类别统计,3:体检分类统计[分类及人数],4:体检分类统计[分类及单位]",required = true)
    private Integer type;
}
