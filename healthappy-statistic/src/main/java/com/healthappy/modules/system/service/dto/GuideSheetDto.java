package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GuideSheetDto {

    @ApiModelProperty("体检编号")
    private String id;

    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("性别")
    private String sex;

    @ApiModelProperty("年龄")
    private Integer age;

    @ApiModelProperty("婚否")
    private String marital;

    @ApiModelProperty("单位")
    private String companyName;

    @ApiModelProperty("部门")
    private String departmentName;

    @ApiModelProperty("联系方式")
    private String phone;

    @ApiModelProperty("体检日期")
    private String peDate;

    @ApiModelProperty("医院名称")
    private String tenantName;

    @ApiModelProperty("医院电话")
    private String telPhone;

    @ApiModelProperty("条码")
    private String barcode;

    @ApiModelProperty("毒害")
    private String poison;

    @ApiModelProperty("身份证图片")
    private String idImage;

    @ApiModelProperty("人像")
    private String portrait;

    @ApiModelProperty("1：健康指引单，2：职业指引单")
    private Integer type;

    @ApiModelProperty("项目名称")
    private String groupName;

    @ApiModelProperty("提示信息")
    private String instruction;

    @ApiModelProperty("是否空腹")
    private Boolean eat;
}
