package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @desc: 医生工作量-绩效分 Dto
 * @author: YJ
 * @date: 2021-11-27 13:24
 **/
@ApiModel("绩效分")
@Data
public class DoctorPerformanceDto {
    /**
     * 医生姓名
     */
    @ApiModelProperty("医生姓名")
    private  String doctor;

    /**
     * 总检工作量
     */
    @ApiModelProperty("检查工作量")
    private Double checkWorkload;

    /**
     * 总检工作量
     */
    @ApiModelProperty("总检工作量")
    private Double conclusionWorkload;

    /**
     * 审核工作量
     */
    @ApiModelProperty("审核工作量")
    private Double verifyWorkload;

    /**
     * 总工作量
     */
    @ApiModelProperty("总工作量")
    private Double totalWorkload;
}
