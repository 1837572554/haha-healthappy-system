package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BarCodeDto {

    @ApiModelProperty("流水号")
    private String id;

    @ApiModelProperty("分组项目")
    private String groupName;

    @ApiModelProperty("条码")
    private String barcode;

    @ApiModelProperty("条码名称")
    private String barcodeName;

    @ApiModelProperty("姓名")
    private String userName;

    @ApiModelProperty("性别")
    private String userSex;

    @ApiModelProperty("年龄")
    private Integer userAge;
}
