package com.healthappy.modules.system.rest;

import cn.hutool.json.JSONObject;
import com.healthappy.annotation.AnonymousAccess;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.common.Base64ConvertFile;
import com.healthappy.modules.common.enums.ReportEnum;
import com.healthappy.modules.system.service.RegistrationService;
import com.healthappy.utils.R;
import com.healthappy.utils.RedisUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description 体检登记 打印
 * @author FGQ
 * @date 2021-09-06
 */
@Slf4j
@Api(tags = "体检管理：体检登记")
@RestController
@AllArgsConstructor
@RequestMapping("/statistic/medicalRegistration")
public class RegistrationController {

    private final RegistrationService registrationService;

    @Log("条码")
    @ApiOperation(value = "条码",hidden = true)
    @GetMapping("/barCode")
    @AnonymousAccess
    public R barCode(@RequestParam("id") String id){
        return R.ok(registrationService.barCode(id));
    }

    @Log("获取条码文件")
    @ApiOperation("获取条码文件")
    @GetMapping("/filePath/barCode")
    @PreAuthorize("@el.check('MedicalRegistration:list')")
    public R filePathBarCode(@RequestParam("id") String id){
        return R.ok(Base64ConvertFile.returnFile(Base64ConvertFile.parameterJoin(ReportEnum.BAR_CODE.getDesc(),new JSONObject().put("id",id))));
    }

    @Log("指引单")
    @ApiOperation(value = "指引单",hidden = true)
    @GetMapping("/guideSheet")
    @AnonymousAccess
    public R guideSheet(@RequestParam("id") String id){
        return R.ok(registrationService.guideSheet(id));
    }


    @Log("指引单文件")
    @ApiOperation("指引单文件")
    @GetMapping("/filePath/guideSheet")
    @PreAuthorize("@el.check('MedicalRegistration:list')")
    public R fileGuideSheet(@RequestParam("id") String id){
        return R.ok(Base64ConvertFile.returnFile(Base64ConvertFile.parameterJoin(ReportEnum.BAR_CODE.getDesc(),new JSONObject().put("id",id))));
    }


    @Log("指引单项目列表")
    @ApiOperation(value = "指引单项目列表",hidden = true)
    @GetMapping("/guideSheet/item")
    @AnonymousAccess
    public R guideSheet(@RequestParam("id") String id
    ,@RequestParam("type") Integer type){
        if(type == 1){
            if(!RedisUtil.hasKey(Base64ConvertFile.GUIDE_SHEET+id)){
                registrationService.guideSheet(id);
            }
            return R.ok(RedisUtil.get(Base64ConvertFile.GUIDE_SHEET+id));
        }
        if(!RedisUtil.hasKey(Base64ConvertFile.NON_GUIDE_SHEET+id)){
            registrationService.guideSheet(id);
        }
        return R.ok(RedisUtil.get(Base64ConvertFile.NON_GUIDE_SHEET+id));
    }

    @Log("信息表")
    @ApiOperation(value = "信息表",hidden = true)
    @GetMapping("/informationSheet")
    @AnonymousAccess
    public R informationSheet(@RequestParam("id") String id){
        return R.ok(registrationService.informationSheet(id));
    }

    @Log("信息表-职业病史")
    @ApiOperation(value = "信息表-职业病史",hidden = true)
    @GetMapping("/informationSheet/itemList")
    @AnonymousAccess
    public R informationSheetItemList(@RequestParam("id") String id){
        return R.ok(registrationService.informationSheetItemList(id));
    }

    @Log("信息表文件")
    @ApiOperation(value = "信息表文件")
    @PreAuthorize("@el.check('MedicalRegistration:list')")
    @GetMapping("/filePath/informationSheet")
    public R fileInformationSheet(@RequestParam("id") String id){
        return R.ok(Base64ConvertFile.returnFile(Base64ConvertFile.parameterJoin(ReportEnum.INFORMATION_SHEET.getDesc(),new JSONObject().put("id",id))));
    }

    @Log("健康体检-首页")
    @ApiOperation(value = "健康体检-首页",hidden = true)
    @GetMapping("/healthyMedical/homePage")
    @AnonymousAccess
    public R healthyMedicalHomePage(@RequestParam("id") String id){
        return R.ok(registrationService.healthyMedicalHomePage(id));
    }

    /**
     * 体检项目完成情况
     * @param id 流水号 paId
     * @param type 是否是检验项目
     * @return
     */
    @Log("健康体检-体检项目完成情况")
    @ApiOperation(value = "健康体检-体检项目完成情况",hidden = true)
    @GetMapping("/healthyMedical/item")
    @AnonymousAccess
    public R healthyItem(@RequestParam("id") String id,@RequestParam("type") Boolean type){
        return R.ok(registrationService.healthyItem(id,type));
    }
}
