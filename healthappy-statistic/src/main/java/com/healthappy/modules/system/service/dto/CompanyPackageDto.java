package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Jevany
 * @date 2022/1/5 11:06
 * @desc 单位套餐项目情况
 */
@Data
public class CompanyPackageDto {

    /** 单位情况 */
    @ApiModelProperty("单位情况")
    private String companyName;

    /** 体检分类 */
    @ApiModelProperty("体检分类")
    private String peTypeHdName;

    /** 单位分组名称 */
    @ApiModelProperty("单位分组名称")
    private String deptGroupName;

    /** 使用套餐情况 */
    @ApiModelProperty("使用套餐情况")
    private String subdtPackage;

    /** 套餐名称 */
    @ApiModelProperty("套餐名称")
    private String packageName;

    /** 项目名称 */
    @ApiModelProperty("项目名称")
    private String groupName;

    /** 使用人数 */
    @ApiModelProperty("使用人数")
    private Integer numberOfPeople;
}
