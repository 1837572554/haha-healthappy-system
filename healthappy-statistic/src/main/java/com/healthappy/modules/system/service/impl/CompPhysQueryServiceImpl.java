package com.healthappy.modules.system.service.impl;

import com.healthappy.modules.system.service.CompPhysQueryService;
import com.healthappy.modules.system.service.dto.CompPhysQueryCriteria;
import com.healthappy.modules.system.service.dto.CompanyGroupDto;
import com.healthappy.modules.system.service.dto.CompanyPackageDto;
import com.healthappy.modules.system.service.mapper.CompPhysQueryMapper;
import com.healthappy.utils.FileUtil;
import com.healthappy.utils.PageUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * @author Jevany
 * @date 2022/1/5 11:23
 * @desc
 */
@AllArgsConstructor
@Service
public class CompPhysQueryServiceImpl implements CompPhysQueryService {

    private final CompPhysQueryMapper compPhysQueryMapper;

    @Override
    public Map<String, Object> getCompanyPackageList(CompPhysQueryCriteria criteria) {
        List<CompanyPackageDto> companyPackageList = compPhysQueryMapper.getCompanyPackageList(criteria);
        //总条数
        Integer totalCount=companyPackageList.size();

        Integer[] pageBase=null;
        if(criteria.getIsExport().equals(false)){
            //计算分页-页数
            pageBase = PageUtil.toStartEnd(criteria.getPageNum(), criteria.getPageSize(), totalCount);

            Map<String, Object> map = new LinkedHashMap<>(5);
            map.put("content", companyPackageList.subList(pageBase[0], pageBase[1]));
            map.put("totalElements", totalCount);
            map.put("totalPage", pageBase[3]);
            map.put("currentPage", pageBase[2]);
            map.put("pageSize", criteria.getPageSize());
            return map;
        } else {
            Map<String, Object> map = new LinkedHashMap<>(1);
            map.put("content", companyPackageList);
            return map;
        }
    }

    @Override
    public Map<String, Object> getCompanyGroupList(CompPhysQueryCriteria criteria) {
        List<CompanyGroupDto> companyGroupList = compPhysQueryMapper.getCompanyGroupList(criteria);
        //总条数
        Integer totalCount=companyGroupList.size();
        Integer[] pageBase=null;
        if(criteria.getIsExport().equals(false)){
            //计算分页-页数
            pageBase = PageUtil.toStartEnd(criteria.getPageNum(), criteria.getPageSize(), totalCount);

            Map<String, Object> map = new LinkedHashMap<>(5);
            map.put("content", companyGroupList.subList(pageBase[0], pageBase[1]));
            map.put("totalElements", totalCount);
            map.put("totalPage", pageBase[3]);
            map.put("currentPage", pageBase[2]);
            map.put("pageSize", criteria.getPageSize());
            return map;
        } else {
            Map<String, Object> map = new LinkedHashMap<>(1);
            map.put("content", companyGroupList);
            return map;
        }
    }

    @Override
    public void companyPackageListDown(List<CompanyPackageDto> all, HttpServletResponse response) throws IOException {
        List<Map<String,Object>> list=new ArrayList<>();
        Integer idx=0;
        for (CompanyPackageDto dto : all) {
            Map<String,Object> map=new HashMap<>();
            map.put("序号",++idx);
            map.put("单位名称",dto.getCompanyName());
            map.put("体检分类",dto.getPeTypeHdName());
            map.put("分组",dto.getDeptGroupName());
            map.put("套餐名称",dto.getPackageName());
            map.put("使用套餐情况",dto.getSubdtPackage());
            map.put("项目名称",dto.getGroupName());
            map.put("使用人数",dto.getNumberOfPeople());
            list.add(map);
        }
        FileUtil.downloadExcel(list,response);
    }

    @Override
    public void companyGroupListDown(List<CompanyGroupDto> all, HttpServletResponse response) throws IOException {
        List<Map<String,Object>> list=new ArrayList<>();
        Integer idx=0;
        for (CompanyGroupDto dto : all) {
            Map<String,Object> map=new HashMap<>();
            map.put("序号",++idx);
            map.put("科室",dto.getDeptName());
            map.put("项目",dto.getGroupName());
            map.put("使用人数",dto.getNumberPeople());
            list.add(map);
        }
        FileUtil.downloadExcel(list,response);
    }
}