package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.*;
import lombok.Data;

/**
 * @author Jevany
 * @date 2022/1/17 18:12
 * @desc
 */
@ApiModel("医生工作量统计")
@Data
public class DoctorWorkloadDistinctDto extends DoctorWorkloadTotalDataDto {
    /** 科室名称 */
    @ApiModelProperty("科室名称")
    private String deptName;

    /** 医生类型 1医生 2护士 */
    @ApiModelProperty("医生类型 1医生 2护士")
    private String doctorType;

    /** 组合项名称 */
    @ApiModelProperty("组合项名称")
    private String groupName;

    /** 组合项次数 */
    @ApiModelProperty("组合项次数")
    private Long groupCnt;
    
    /** 体检分类名称 */
    @ApiModelProperty("体检分类名称")
    private String peTypeHdName;
    
    /** 体检分类次数 */
    @ApiModelProperty("体检分类次数")
    private Long peTypeHdCnt;

    /** 检查权重 */
    @ApiModelProperty("检查权重")
    private Double checkWeight;

    /** 得分 权重*项次数 */
    @ApiModelProperty("得分 权重*项次数")
    private Double score;

    /** 总分 人员总得分*/
    @ApiModelProperty("总分 人员总得分")
    private Double totalScore;
}
