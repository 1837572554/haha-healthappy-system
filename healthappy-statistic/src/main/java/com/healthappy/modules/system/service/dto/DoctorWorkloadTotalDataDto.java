package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.*;
import lombok.Data;

/**
 * @author Jevany
 * @date 2022/1/17 16:34
 * @desc 医生工作总数据 后面做统计用
 */
@Data
public class DoctorWorkloadTotalDataDto {
    /** 科室编号 */
    @ApiModelProperty("科室编号")
    private String deptId;

    /** 组合编号 */
    @ApiModelProperty("组合编号")
    private String groupId;

    /** 项目检查医生 */
    @ApiModelProperty("项目检查医生")
    private String doctor;

    /** 医生类型编号 1医生 2护士*/
    @ApiModelProperty("医生类型编号 1医生 2护士")
    private String doctorTypeId;

    /** 体检分类编号 */
    @ApiModelProperty("体检分类编号")
    private String peTypeHdId;
}
