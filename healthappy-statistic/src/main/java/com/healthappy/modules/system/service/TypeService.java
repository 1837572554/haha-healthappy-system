package com.healthappy.modules.system.service;

import com.healthappy.modules.system.service.dto.*;

import java.util.List;

/**
 * @author FGQ
 */
public interface TypeService {

    /**
     * 分类统计
     * @param criteria
     * @return
     */
    List<TypeDto> classifiedStatistic(TypeQueryCriteria criteria);

    /**
     * 类别统计
     * @param criteria
     * @return
     */
    List<CategoryDto> categoryStatistic(TypeQueryCriteria criteria);

    /**
     * 体检分类统计[分类及人数]
     * @param criteria
     * @return
     */
    List<TypeNumberDto> typeNumber(TypeQueryCriteria criteria);

    /**
     * 体检分类统计[分类及单位]
     * @param criteria
     * @return
     */
    List<TypeAndCompanyDto> typeAndCompany(TypeQueryCriteria criteria);
}
