package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GuideSheetItemDto {

    @ApiModelProperty("项目名称")
    private String groupName;

    @ApiModelProperty("提示信息")
    private String instruction;
}
