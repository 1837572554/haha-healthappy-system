package com.healthappy.modules.system.service;

import com.healthappy.modules.system.service.dto.CompPhysQueryCriteria;
import com.healthappy.modules.system.service.dto.CompanyGroupDto;
import com.healthappy.modules.system.service.dto.CompanyPackageDto;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author Jevany
 * @date 2022/1/5 11:05
 * @desc 单位情况查询服务接口
 */
public interface CompPhysQueryService {

    /**
     * 获得单位套餐项目情况
     * @author YJ
     * @date 2022/1/5 11:18
     * @param criteria
     * @return java.util.List〈com.healthappy.modules.system.service.dto.CompanyPackageDto〉
     */
    Map<String, Object> getCompanyPackageList(CompPhysQueryCriteria criteria);


    /**
     * 获得单位项目使用情况
     * @author YJ
     * @date 2022/1/5 11:23
     * @param criteria
     * @return java.util.List〈com.healthappy.modules.system.service.dto.CompanyGroupDto〉
     */
    Map<String, Object> getCompanyGroupList(CompPhysQueryCriteria criteria);


    /**
     * 下载单位套餐项目情况
     * @author YJ
     * @date 2022/1/25 17:46
     * @param all
     * @param response
     */
    void companyPackageListDown(List<CompanyPackageDto> all, HttpServletResponse response) throws IOException;

    /**
     * 下载单位项目使用情况
     * @author YJ
     * @date 2022/1/25 17:46
     * @param all
     * @param response
     */
    void companyGroupListDown(List<CompanyGroupDto> all, HttpServletResponse response) throws IOException;

}