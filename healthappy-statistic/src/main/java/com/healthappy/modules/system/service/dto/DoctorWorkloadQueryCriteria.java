package com.healthappy.modules.system.service.dto;/**
 * @author Jevany
 * @date 2021/11/26 16:56
 * @desc
 */

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @desc: 医生工作量统计查询类
 * @author: YJ
 * @date: 2021-11-26 16:56
 **/
@Data
public class DoctorWorkloadQueryCriteria {

    @NotNull(message = "请求类型不能为空")
    @ApiModelProperty(value="请求类型 1:检查工作量,2:业务工作量,3:绩效分",required = true,position = 0,example = "1")
    private Integer type;

    @NotNull(message = "时间类型不能为空")
    @ApiModelProperty(value = "时间类型 1，登记日期；2，签到日期；3，完成日期 ",required = true,position = 1,example = "1")
    private Integer dateType;

    @NotBlank(message = "开始时间不能为空")
    @ApiModelProperty(value = "开始时间 如：2021-01-01 00:00:00",required = true,position = 2,example = "2021-01-01 00:00:00")
    private String startTime;

    @NotBlank(message = "结束时间不能为空")
    @ApiModelProperty(value = "结束时间 如：2021-12-01 23:59:59",required = true,position = 3,example = "2021-12-01 23:59:59")
    private String endTime;

    @ApiModelProperty("科室ID")
    private String deptId;

    @ApiModelProperty("医生姓名")
    private String doctor;

    @ApiModelProperty(value = "医生类型(1,医生；2，护士；)")
    private String doctorType;

    @ApiModelProperty("分类ID")
    private String peTypeHdId;

    /** 业务类型 业务类型 0：全部 1：总检 2：审核 3：批量登记 4：批量预约*/
    @ApiModelProperty("业务类型 0：全部 1：总检 2：审核 3：批量登记 4：批量预约 如空默认为全部")
    private Integer businessType=0;

    @ApiModelProperty(value = "租户ID",hidden = true)
    private String tenantId;

    /**
     * 页面显示条数
     */
    @ApiModelProperty("页面显示条数,默认为10")
    private Integer pageSize = 10;

    /**
     * 页数
     */
    @ApiModelProperty("页数,默认为1")
    private Integer pageNum = 1;

    /** 开始条数 */
    @ApiModelProperty(value = "开始条数",hidden = true)
    private Integer startIndex=-1;

    /**
     * 是否导出，如果为True，分页功能不启用，默认False
     */
    @ApiModelProperty(value = "是否导出，如果为True，分页功能不启用，默认False",hidden = true)
    private Boolean isExport = false;
}
