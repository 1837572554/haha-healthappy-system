package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 健康报告-首页
 * @author FGQ
 */
@Data
public class HealthyMedicalHomePageDto {

    @ApiModelProperty("体检编号")
    private String code;

    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("年龄")
    private String sexAndAge;

    @ApiModelProperty("单位")
    private String company;

    @ApiModelProperty("部门")
    private String dept;

    @ApiModelProperty("联系电话")
    private String tel;

    @ApiModelProperty("报告日期")
    private String time;

    @ApiModelProperty("医院")
    private String tenant;

    @ApiModelProperty("图标")
    private String logo;

    @ApiModelProperty("地址")
    private String address;

    @ApiModelProperty("医院电话")
    private String tenantTel;
}
