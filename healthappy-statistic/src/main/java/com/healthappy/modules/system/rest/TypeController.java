package com.healthappy.modules.system.rest;

import com.healthappy.annotation.AnonymousAccess;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.service.TypeService;
import com.healthappy.modules.system.service.dto.TypeQueryCriteria;
import com.healthappy.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description 分类统计
 * @author FGQ
 * @date 2021-06-17
 */
@Slf4j
@Api(tags = "统计报表：分类统计")
@RestController
@AllArgsConstructor
@RequestMapping("/statistic")
public class TypeController {

    private final TypeService typeService;


    @Log("分类统计")
    @ApiOperation("分类统计")
    @GetMapping("/type")
    @PreAuthorize("@el.check('statistic:type:list')")
    public R type(TypeQueryCriteria criteria) {
        log.info("------分类统计------");
        if(null != criteria.getDateType()){
            if(null == criteria.getStartTime() || null == criteria.getEndTime()){
                return R.error("请选择时间");
            }
        }
        switch (criteria.getType()){
            case 1:
                //分类统计
                return R.ok(typeService.classifiedStatistic(criteria));
            case 2:
                //类别统计
                return R.ok(typeService.categoryStatistic(criteria));
            case 3:
                //分类及人数
                return R.ok(typeService.typeNumber(criteria));
            case 4:
                //分类及单位
                return R.ok(typeService.typeAndCompany(criteria));
            default:
                return R.error("参数不合法");
        }
    }
}
