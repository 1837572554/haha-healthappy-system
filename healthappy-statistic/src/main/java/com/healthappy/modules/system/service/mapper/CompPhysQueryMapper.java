package com.healthappy.modules.system.service.mapper;

import com.baomidou.mybatisplus.annotation.SqlParser;
import com.healthappy.modules.system.service.dto.CompPhysQueryCriteria;
import com.healthappy.modules.system.service.dto.CompanyGroupDto;
import com.healthappy.modules.system.service.dto.CompanyPackageDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Jevany
 * @date 2022/1/5 11:45
 * @desc 单位体检情况查询 数据查询接口
 */
@Mapper
@Repository
public interface CompPhysQueryMapper {

    /**
     * 获得单位套餐项目情况
     * @author YJ
     * @date 2022/1/5 11:47
     * @param criteria
     * @return java.util.List〈com.healthappy.modules.system.service.dto.CompanyPackageDto〉
     */
    @SqlParser(filter = true)
    List<CompanyPackageDto> getCompanyPackageList(@Param("criteria") CompPhysQueryCriteria criteria);

    /**
     * 获得单位项目使用情况
     * @author YJ
     * @date 2022/1/5 11:47
     * @param criteria
     * @return java.util.List〈com.healthappy.modules.system.service.dto.CompanyGroupDto〉
     */
    @SqlParser(filter = true)
    List<CompanyGroupDto> getCompanyGroupList(@Param("criteria") CompPhysQueryCriteria criteria);
}
