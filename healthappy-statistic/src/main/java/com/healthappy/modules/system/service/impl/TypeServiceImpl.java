package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.healthappy.modules.system.service.TypeService;
import com.healthappy.modules.system.service.dto.*;
import com.healthappy.modules.system.service.mapper.TypeMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author FGQ
 */
@Service
//@DS("click")
@AllArgsConstructor
public class TypeServiceImpl implements TypeService {

    private final TypeMapper typeMapper;

    @Override
    public List<TypeDto> classifiedStatistic(TypeQueryCriteria criteria) {
        return typeMapper.getTypeList(criteria);
    }

    @Override
    public List<CategoryDto> categoryStatistic(TypeQueryCriteria criteria) {
        return typeMapper.getCategoryList(criteria);
    }

    @Override
    public List<TypeNumberDto> typeNumber(TypeQueryCriteria criteria) {
        List<TypeNumberDto> categoryList = typeMapper.getTypeCategoryList(criteria);
        if(CollUtil.isEmpty(categoryList)){
            return Collections.emptyList();
        }
        List<TypeNumberPatientDto> patientList = typeMapper.getPatientList(criteria);
        return categoryList.stream().peek(e -> {
            if(CollUtil.isEmpty(patientList)){
                e.setFlrs(0L);
                e.setLbrs(0L);
            }else {
                e.setLbrs(patientList.stream()
                        .filter(f->StrUtil.isNotBlank(f.getPeTypeDtId()) && StrUtil.isNotBlank(f.getPeTypeHdId()) && StrUtil.isNotBlank(f.getCompanyId()))
                        .filter(pa->pa.getPeTypeDtId().equals(e.getLbId()) && pa.getPeTypeHdId().equals(e.getFlId())).count());
                e.setFlrs(patientList.stream()
                        .filter(f->StrUtil.isNotBlank(f.getPeTypeHdId()) && StrUtil.isNotBlank(f.getCompanyId()))
                        .filter(pa->pa.getPeTypeHdId().equals(e.getFlId())).count());
            }
        }).collect(Collectors.toList());
    }

    @Override
    public List<TypeAndCompanyDto> typeAndCompany(TypeQueryCriteria criteria) {
        List<TypeAndCompanyDto> categoryList = typeMapper.getTypeCategoryList(criteria).stream().map(
                e-> BeanUtil.copyProperties(e,TypeAndCompanyDto.class)
        ).collect(Collectors.toList());
        if(CollUtil.isEmpty(categoryList)){
            return Collections.emptyList();
        }
        List<TypeNumberPatientDto> patientList = typeMapper.getPatientList(criteria);
        return categoryList.stream().peek(e -> {
            if(CollUtil.isEmpty(patientList)){
                e.setTypeCompanyAmount(0L);
                e.setTypeDtCompanyAmount(0L);
            }else {
                e.setTypeCompanyAmount(Convert.toLong(patientList.parallelStream()
                        .filter(f -> StrUtil.isNotBlank(f.getPeTypeHdId()) && StrUtil.isNotBlank(f.getCompanyId()))
                        .filter(pa->pa.getPeTypeHdId().equals(e.getFlId())).collect(Collectors.groupingBy(TypeNumberPatientDto::getCompanyId)).size()));
                e.setTypeDtCompanyAmount(Convert.toLong(patientList.parallelStream().
                        filter(f -> StrUtil.isNotBlank(f.getPeTypeDtId()) && StrUtil.isNotBlank(f.getPeTypeHdId()) && StrUtil.isNotBlank(f.getCompanyId())).
                        filter(pa->pa.getPeTypeDtId().equals(e.getLbId()) && pa.getPeTypeHdId().equals(e.getFlId())).collect(Collectors.groupingBy(TypeNumberPatientDto::getCompanyId)).size()));
            }
        }).collect(Collectors.toList());
    }
}
