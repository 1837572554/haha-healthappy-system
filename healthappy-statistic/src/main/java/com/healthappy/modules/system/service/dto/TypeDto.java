package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author FGQ
 */
@Data
public class TypeDto {

    @ApiModelProperty("单位")
    private String name;

    @ApiModelProperty("总人数")
    private Long totalNum;

    @ApiModelProperty("职业体检")
    private Long zytj;

    @ApiModelProperty("健康体检")
    private Long jktj;

    @ApiModelProperty("职普合一")
    private Long zphy;

    @ApiModelProperty("儿童体检")
    private Long rttj;

    @ApiModelProperty("职业体检复查")
    private Long zytjfc;

    @ApiModelProperty("健康体检复查")
    private Long jktjfc;

    @ApiModelProperty("儿童体检复查")
    private Long rttjfc;

    @ApiModelProperty("放射体检")
    private Long fstj;

    @ApiModelProperty("放射体检复查")
    private Long fstjfc;

    @ApiModelProperty("驾驶员体检")
    private Long jsytj;

    @ApiModelProperty("征兵体检")
    private Long zbtj;

    @ApiModelProperty("公务员体检")
    private Long gwytj;
}
