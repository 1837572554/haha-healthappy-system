package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**

/**
 * @desc: 医生业务工作量 Dto
 * @author: YJ
 * @date: 2021-11-27 10:49
 **/
@Data
public class DoctorBusinessWorkloadDto {
    /**
     * 体检医生
     */
    @ApiModelProperty("体检医生")
    private String doctorName;

    /**
     * 医生类型
     */
    @ApiModelProperty("医生类型")
    private String doctorType;

    /**
     * 体检业务[登记、总检、审核、打印]
     */
    @ApiModelProperty("体检业务[登记、总检、审核、打印]")
    private String businessType;

    /**
     * 科室名称
     */
    @ApiModelProperty("科室名称")
    private String deptName;

    /**
     * 项目名称
     */
    @ApiModelProperty("项目名称")
    private String groupName;

    /**
     * 项目名称
     */
    @ApiModelProperty("完成人数")
    private Long completeNum;

    /**
     * 业务权重
     */
    @ApiModelProperty("业务权重")
    private Double businessWeight;

    /**
     * 得分
     */
    @ApiModelProperty("得分")
    private Double score;

    /**
     * 总分
     */
    @ApiModelProperty("总分")
    private Double totalScore;
}
