package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author FGQ
 * 类别
 */
@Data
public class CategoryDto {

    @ApiModelProperty("单位名称")
    private String name;

    @ApiModelProperty("在岗期间")
    private Long zgqj;

    @ApiModelProperty("上岗前")
    private Long sgq;

    @ApiModelProperty("VIP体检")
    private Long viptj;

    @ApiModelProperty("学生体检")
    private Long xstj;

    @ApiModelProperty("离岗时")
    private Long lgs;

    @ApiModelProperty("离岗后")
    private Long lgh;

    @ApiModelProperty("应急")
    private Long yj;

    @ApiModelProperty("常规体检")
    private Long cgtj;

    @ApiModelProperty("入职体检")
    private Long rztj;
}
