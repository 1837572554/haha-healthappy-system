package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CategoryStatisticDto {

    @ApiModelProperty("单位名称")
    private String name;
}

