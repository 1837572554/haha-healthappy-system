package com.healthappy.modules.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.healthappy.modules.common.Base64ConvertFile;
import com.healthappy.modules.system.service.RegistrationService;
import com.healthappy.modules.system.service.dto.*;
import com.healthappy.modules.system.service.mapper.RegistrationMapper;
import com.healthappy.utils.RedisUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author FGQ
 */
@Service
@AllArgsConstructor
public class RegistrationServiceImpl implements RegistrationService {

    private final RegistrationMapper registrationMapper;

    @Override
    public List<BarCodeDto> barCode(String id) {
        return registrationMapper.barCode(id);
    }

    @Override
    public List<GuideSheetDto> guideSheet(String id) {
        List<GuideSheetDto> guideSheetDto = registrationMapper.guideSheet(id);
        if(CollUtil.isEmpty(guideSheetDto)){
            return null;
        }
        GuideSheetDto sheetDto = guideSheetDto.get(0);
        List<GuideSheetItemDto> fastingItemList = new ArrayList<>();
        List<GuideSheetItemDto> nonFastingItemList = new ArrayList<>();
        for (GuideSheetDto dto:guideSheetDto) {
            GuideSheetItemDto item = new GuideSheetItemDto();
            item.setGroupName(dto.getGroupName());
            item.setInstruction(dto.getInstruction());
            if(dto.getEat()!= null && dto.getEat()){
                nonFastingItemList.add(item);
            }else {
                fastingItemList.add(item);
            }
        }
        RedisUtil.set(Base64ConvertFile.GUIDE_SHEET + id,fastingItemList);
        RedisUtil.set(Base64ConvertFile.NON_GUIDE_SHEET + id,nonFastingItemList);

        sheetDto.setPoison(buildPoison(sheetDto.getPoison()));

        sheetDto.setPeDate(StrUtil.isNotBlank(sheetDto.getPeDate()) ? DateTime.of(sheetDto.getPeDate(),"yyyy-MM-dd").toString("yyyy年M月dd日") : null);
        return Collections.singletonList(sheetDto);
    }

    private String buildPoison(String poisonType) {
        String poison = "";
        if(StrUtil.isBlank(poisonType)){
            return poison;
        }
        List<String> list = Stream.of(poisonType.split("、")).collect(Collectors.toList());
        if(CollUtil.isNotEmpty(list)){
            List<String> poisons = new ArrayList<>();
            for (String str:list) {
                poisons.add(registrationMapper.getByPaIdPoison(str));
            }
            poison = String.join("、", poisons);
        }
        return poison;
    }

    @Override
    public List<InformationSheetDto> informationSheet(String id) {
        InformationSheetDto informationSheetDto = registrationMapper.informationSheet(id);
        if(null == informationSheetDto){
            return Collections.emptyList();
        }
        informationSheetDto.setPoisonType(buildPoison(informationSheetDto.getPoisonType()));
        informationSheetDto.setBsZdsj(StrUtil.isNotBlank(informationSheetDto.getBsZdsj()) ? DateTime.of(informationSheetDto.getBsZdsj(),"yyyy-MM-dd").toString("yyyy年M月dd日") : null);
        return Collections.singletonList(informationSheetDto);
    }

    @Override
    public List<InformationSheetZybDto> informationSheetItemList(String id) {
        return registrationMapper.informationSheetItemList(id).stream().peek(e -> {
            String[] split = e.getStartEnd().split("-");
            e.setStartTime(split[0]);
            e.setEndTime(split[1]);
        }).collect(Collectors.toList());
    }

    @Override
    public List<HealthyMedicalHomePageDto> healthyMedicalHomePage(String id) {
        return registrationMapper.healthyMedicalHomePage(id);
    }

    @Override
    public List<HealthyItemResultDto> healthyItem(String id, Boolean type) {
        List<HealthyItemResultDto> resultDtoList = registrationMapper.healthyItem(id, "NOT");
        if(type){
            AtomicInteger integer = CollUtil.isEmpty(resultDtoList) ? new AtomicInteger(0) : new AtomicInteger(resultDtoList.size());
            List<HealthyItemResultDto> dtoList = registrationMapper.healthyItem(id, "");
            resultDtoList = CollUtil.isEmpty(dtoList) ? Collections.emptyList() : dtoList.stream().peek(k->k.setGroupName(StrUtil.format("({})",integer.incrementAndGet()) + k.getGroupName())).collect(Collectors.toList());
        }else {
            AtomicInteger integer = new AtomicInteger(0);
            resultDtoList = CollUtil.isEmpty(resultDtoList) ? Collections.emptyList() :
                    resultDtoList.stream().peek(k->k.setGroupName(StrUtil.format("({})",integer.incrementAndGet()) + k.getGroupName())).collect(Collectors.toList());
        }
        return resultDtoList;
    }
}
