package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CheckWorkloadDto {

    @ApiModelProperty("分组ID")
    private String groupId;

    @ApiModelProperty("分组名称")
    private String groupName;

    @ApiModelProperty("医生")
    private String doctor;

    @ApiModelProperty("类型")
    private String doctorType;

    @ApiModelProperty("部门名称")
    private String deptName;

    @ApiModelProperty("检查权重")
    private Integer checkWeight;

    @ApiModelProperty("得分")
    private Double score;

    @ApiModelProperty("项目次数")
    private Long groupCount;

    @ApiModelProperty("总分")
    private Double totalScore;

    @ApiModelProperty("分类")
    private String hdType;

    @ApiModelProperty("分类次数")
    private Long hdCount;

    @ApiModelProperty("备注")
    private String remark;
}
