package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author Jevany
 * @date 2022/1/5 10:46
 * @desc 单位体检查询
 */
@Data
@ApiModel("单位体检查询")
public class CompPhysQueryCriteria {

    @NotBlank
    @ApiModelProperty("1:单位套餐项目情况,2:单位项目使用情况")
    private Integer type;

    @ApiModelProperty(value = "时间类型(1，登记日期；2，签到日期；4，总检日期)")
    private Integer dateType;

    @ApiModelProperty("开始时间")
    private String startTime;

    @ApiModelProperty("结束时间")
    private String endTime;

    @ApiModelProperty("分类ID")
    private String peTypeHdId;

    @ApiModelProperty("单位编号")
    private String companyId;

    @ApiModelProperty("租户编号")
    private String tenantId;


    /**
     * 页面显示条数
     */
    @ApiModelProperty("页面显示条数,默认为10")
    private Integer pageSize = 10;

    /**
     * 页数
     */
    @ApiModelProperty("页数,默认为1")
    private Integer pageNum = 1;

    /** 开始条数 */
    @ApiModelProperty(value = "开始条数",hidden = true)
    private Integer startIndex=-1;

    /**
     * 是否导出，如果为True，分页功能不启用，默认False
     */
    @ApiModelProperty(value = "是否导出，如果为True，分页功能不启用，默认False",hidden = true)
    private Boolean isExport = false;
}
