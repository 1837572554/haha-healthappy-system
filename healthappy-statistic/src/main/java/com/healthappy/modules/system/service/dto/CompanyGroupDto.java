package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Jevany
 * @date 2022/1/5 11:20
 * @desc 单位项目使用情况
 */
@Data
public class CompanyGroupDto {

    /** 科室名称 */
    @ApiModelProperty("科室名称")
    private String deptName;

    /** 组合项目名称 */
    @ApiModelProperty("组合项目名称")
    private String groupName;

    /** 使用人数 */
    @ApiModelProperty("使用人数")
    private Integer numberPeople;
}
