package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author FGQ
 * 部门
 */
@Data
public class DeptWorkloadDto {

    @ApiModelProperty("体检科室")
    private String name;

    @ApiModelProperty("体检项目")
    private String groupName;

    @ApiModelProperty("原价")
    private BigDecimal price;

    @ApiModelProperty("实际价格")
    private BigDecimal cost;

    @ApiModelProperty("分组ID")
    private String groupId;

    @ApiModelProperty("登记人数")
    private Long patientNum;

    @ApiModelProperty("已检人数")
    private Long checkedNum;

    @ApiModelProperty("已检金额")
    private BigDecimal checkedAmount;

    @ApiModelProperty("未检人数")
    private Long uncheckedNum;

    @ApiModelProperty("未检金额")
    private BigDecimal uncheckedAmount;

    @ApiModelProperty("弃检人数")
    private Long refuseCheckNum;

    @ApiModelProperty("弃检金额")
    private BigDecimal refuseCheckAmount;

    @ApiModelProperty("正常人数")
    private Long correctNum;

    @ApiModelProperty("异常人数")
    private Long notCorrectNum;
}
