package com.healthappy.modules.system.rest;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.healthappy.exception.BadRequestException;
import com.healthappy.logging.aop.log.Log;
import com.healthappy.modules.system.service.DoctorWorkloadService;
import com.healthappy.modules.system.service.dto.BusinessWorkloadDistinctDto;
import com.healthappy.modules.system.service.dto.DoctorPerformanceDto;
import com.healthappy.modules.system.service.dto.DoctorWorkloadDistinctDto;
import com.healthappy.modules.system.service.dto.DoctorWorkloadQueryCriteria;
import com.healthappy.utils.R;
import com.healthappy.utils.SecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @desc: 医生工作量统计
 * @author: YJ
 * @date: 2021-11-26 14:11
 **/
@Slf4j
@Api(tags = "统计报表：医生工作量统计")
@RestController
@AllArgsConstructor
@RequestMapping("/statistic")
public class DoctorWorkloadController {

    private final DoctorWorkloadService doctorWorkloadService;

    @Log("医生检查工作量统计")
    @ApiOperation("医生检查工作量统计，权限码：statistic:dcl:list")
    @GetMapping("/doctorCheckWorkload")
    @PreAuthorize("@el.check('statistic:dcl:list')")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "医生工作量统计", response = DoctorWorkloadDistinctDto.class, responseContainer = "Map"),
            @ApiResponse(code = 201, message = "医生业务工作量统计", response = BusinessWorkloadDistinctDto.class, responseContainer = "Map"),
            @ApiResponse(code = 202, message = "绩效分", response = DoctorPerformanceDto.class, responseContainer = "Map")
    })
    public R getDoctorCheckWorkload(DoctorWorkloadQueryCriteria criteria) {
        log.info("------医生工作量统计------");
        if (null != criteria.getDateType()) {
            if (StrUtil.isAllBlank(criteria.getStartTime(), criteria.getEndTime())) {
                return R.error("请选择时间");
            }
        }
        if (StrUtil.isBlank(criteria.getTenantId())) {
            criteria.setTenantId(SecurityUtils.getTenantId());
        }
        if (ObjectUtil.isEmpty(criteria.getPageNum()) || criteria.getPageNum() <= 0) {
            criteria.setPageNum(1);
        }
        if (ObjectUtil.isEmpty(criteria.getPageSize()) || criteria.getPageSize() <= 0) {
            criteria.setPageSize(10);
        }

        switch (criteria.getType()) {
            case 1:
                return R.ok(doctorWorkloadService.getDoctorCheckWorkload(criteria));
            case 2:
                if (ObjectUtil.isNull(criteria.getBusinessType())) {
                    return R.error("业务类型不能为空");
                }
                return R.ok(doctorWorkloadService.getDoctorBusinessWorkload(criteria));
            case 3:
                return R.ok(doctorWorkloadService.getDoctorPerformance(criteria));
            default:
                return R.error("参数有误");
        }
    }

    @Log("导出医生工作量统计")
    @ApiOperation("导出医生工作量统计，权限码：statistic:dcl:upload")
    @GetMapping(value = "/doctorCheckDownload")
    @PreAuthorize("@el.check('statistic:dcl:upload')")
    public void doctorCheckDownload(HttpServletResponse response, DoctorWorkloadQueryCriteria criteria) throws IOException {
        log.info("------医生工作量统计导出Excel------");
        if (null != criteria.getDateType()) {
            if (StrUtil.isAllBlank(criteria.getStartTime(), criteria.getEndTime())) {
                throw new BadRequestException("请选择时间");
            }
        }
        if (StrUtil.isBlank(criteria.getTenantId())) {
            criteria.setTenantId(SecurityUtils.getTenantId());
        }
        criteria.setIsExport(true);

        switch (criteria.getType()) {
            case 1:
                Map<String, Object> doctorCheckWorkload = doctorWorkloadService.getDoctorCheckWorkload(criteria);
                doctorWorkloadService.doctorCheckWorkloadExcel((List<DoctorWorkloadDistinctDto>) doctorCheckWorkload.get("content"), response);
            case 2:
                if (ObjectUtil.isNull(criteria.getBusinessType())) {
                    throw new BadRequestException("业务类型不能为空");
                }
                Map<String, Object> doctorBusinessWorkload = doctorWorkloadService.getDoctorBusinessWorkload(criteria);
                doctorWorkloadService.doctorBusinessWorkloadExcel((List<BusinessWorkloadDistinctDto>) doctorBusinessWorkload.get("content"), response);
            case 3:
                Map<String, Object> doctorPerformance = doctorWorkloadService.getDoctorPerformance(criteria);
                doctorWorkloadService.doctorPerformanceExcel((List<DoctorPerformanceDto>) doctorPerformance.get("content"), response);
            default:
                throw new BadRequestException("参数有误");
        }
    }

}
