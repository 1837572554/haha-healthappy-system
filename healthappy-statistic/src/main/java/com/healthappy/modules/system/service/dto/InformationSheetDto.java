package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 信息表
 */
@Data
public class InformationSheetDto {

    @ApiModelProperty("流水号")
    private String id;

    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("性别")
    private String sex;

    @ApiModelProperty("身份证号")
    private String idNo;

    @ApiModelProperty("联系方式")
    private String phone;

    @ApiModelProperty("单位")
    private String companyName;

    @ApiModelProperty("婚否")
    private String marital;

    @ApiModelProperty("部门")
    private String departmentName;

    @ApiModelProperty("条码")
    private String barcode;

    @ApiModelProperty("医院名称")
    private String tenantName;

    @ApiModelProperty("体检类别")
    private String peTypeDtName;

    @ApiModelProperty("总工龄")
    private String totalYears;

    @ApiModelProperty("接害工龄")
    private String workYears;

    @ApiModelProperty("毒害")
    private String poisonType;

    @ApiModelProperty("工种")
    private String jobName;

    @ApiModelProperty("既往病史")
    private String bsJwbs;

    @ApiModelProperty("病名")
    private String bsBm;

    @ApiModelProperty("诊断时间")
    private String bsZdsj;

    @ApiModelProperty("诊断单位")
    private String bsZddw;

    @ApiModelProperty("是否痊愈")
    private String bsSfqy;

    @ApiModelProperty("初潮")
    private String bsCc;

    @ApiModelProperty("经期")
    private String bsJq;

    @ApiModelProperty("周期")
    private String bsZq;

    @ApiModelProperty("末次月经")
    private String bsMcyj;

    @ApiModelProperty("现有子女")
    private String bsXyzn;

    @ApiModelProperty("流产")
    private String bsLc;

    @ApiModelProperty("早产")
    private String bsZc;

    @ApiModelProperty("死产")
    private String bsSc;

    @ApiModelProperty("异常胎")
    private String bsYct;

    @ApiModelProperty("烟史")
    private String bsYs;

    @ApiModelProperty("酒史")
    private String bsJs;

    @ApiModelProperty("其他")
    private String bsQt;

    @ApiModelProperty("烟史-烟龄，每年吸的数量")
    private String ysYl;

    @ApiModelProperty("烟史-每天吸的数量")
    private String ysNs;

    @ApiModelProperty("酒史-酒龄,每年喝的数量")
    private String jsJl;

    @ApiModelProperty("酒史-每天喝的数量")
    private String jsNs;
}
