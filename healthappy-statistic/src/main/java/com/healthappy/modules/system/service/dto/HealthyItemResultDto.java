package com.healthappy.modules.system.service.dto;

import lombok.Data;

/**
 * @author 项目结果
 */
@Data
public class HealthyItemResultDto {

    private String groupName;

    private String state;

    private String peDate;
}
