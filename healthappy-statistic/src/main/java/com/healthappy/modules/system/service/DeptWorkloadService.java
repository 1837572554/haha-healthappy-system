package com.healthappy.modules.system.service;

import com.healthappy.modules.system.service.dto.DeptWorkloadDto;
import com.healthappy.modules.system.service.dto.DeptWorkloadQueryCriteria;

import java.util.List;

/**
 * @author FGQ
 */
public interface DeptWorkloadService {

    /**
     * 工作量
     * @return
     * @param criteria
     */
    List<DeptWorkloadDto> deptWorkload(DeptWorkloadQueryCriteria criteria);
}
