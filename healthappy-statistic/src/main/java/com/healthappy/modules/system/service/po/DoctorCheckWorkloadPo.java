package com.healthappy.modules.system.service.po;

import lombok.Data;

/**
 * @desc: 医生检查工作量Po
 * @author: YJ
 * @date: 2021-11-29 20:18
 **/
@Data
public class DoctorCheckWorkloadPo {
    private String userName;

    private String userTypeName;
    private String groupId;
    private String groupName;
    private String deptName;
    private String patientId;

    private String peTypeHdId;
    private String peTypeHdName;
    private Double checkWeight;
}
