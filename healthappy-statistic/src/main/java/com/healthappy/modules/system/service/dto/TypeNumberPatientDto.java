package com.healthappy.modules.system.service.dto;

import lombok.Data;

@Data
public class TypeNumberPatientDto {

    private String id;

    private String peTypeHdId;

    private String peTypeDtId;

    private String companyId;
}
