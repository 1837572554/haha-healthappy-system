package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class DeptWorkloadQueryCriteria {


    @ApiModelProperty("事件类型，1：登记时间,2：签到时间,3：完成时间")
    private Integer dateType;

    @ApiModelProperty("开始时间")
    private String startTime;

    @ApiModelProperty("结束时间")
    private String endTime;

    @ApiModelProperty("单位ID")
    private String companyId;

    @ApiModelProperty("科室ID")
    private String deptId;

    @ApiModelProperty("项目ID")
    private String groupId;

    @NotBlank(message = "租户ID不能为空")
    @ApiModelProperty(value = "租户ID",required = true)
    private String tenantId;
}
