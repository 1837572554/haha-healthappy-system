package com.healthappy.modules.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.healthappy.modules.system.service.DeptWorkloadService;
import com.healthappy.modules.system.service.dto.DeptWorkloadDto;
import com.healthappy.modules.system.service.dto.DeptWorkloadQueryCriteria;
import com.healthappy.modules.system.service.mapper.DeptWorkloadMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author FGQ
 */
@Service
//@DS("click")
@AllArgsConstructor
public class DeptWorkloadServiceImpl implements DeptWorkloadService {

    private final DeptWorkloadMapper workloadMapper;

    @Override
    public List<DeptWorkloadDto> deptWorkload(DeptWorkloadQueryCriteria criteria) {
        List<DeptWorkloadDto> workloadDtoList = workloadMapper.getDeptAndItemHdList(criteria);
        if(CollUtil.isEmpty(workloadDtoList)){
            return Collections.emptyList();
        }
        return workloadDtoList.parallelStream().peek(vo -> BeanUtil.copyProperties(workloadMapper.getByItemHd(vo.getGroupId(),criteria), vo, "name", "groupName", "price", "cost","groupId")).collect(Collectors.toList());
    }
}
