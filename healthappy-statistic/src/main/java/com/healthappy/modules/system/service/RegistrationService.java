package com.healthappy.modules.system.service;

import com.healthappy.modules.system.service.dto.*;

import java.util.List;

/**
 * @author FGQ
 */
public interface RegistrationService {

    List<BarCodeDto> barCode(String id);

    List<GuideSheetDto> guideSheet(String id);

    List<InformationSheetDto> informationSheet(String id);

    List<InformationSheetZybDto> informationSheetItemList(String id);

    List<HealthyMedicalHomePageDto> healthyMedicalHomePage(String id);

    List<HealthyItemResultDto> healthyItem(String id, Boolean type);
}
