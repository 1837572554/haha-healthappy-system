package com.healthappy.modules.system.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class InformationSheetZybDto {

    @ApiModelProperty("开始时间")
    private String startTime;

    @ApiModelProperty("结束时间")
    private String endTime;

    @ApiModelProperty("起止时间")
    private String startEnd;

    @ApiModelProperty("单位")
    private String workUnit;

    @ApiModelProperty("车间")
    private String workshop;

    @ApiModelProperty("工种")
    private String job;

    @ApiModelProperty("危害因素")
    private String poison;

    @ApiModelProperty("防护措施")
    private String defend;
}
