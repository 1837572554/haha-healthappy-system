package com.healthappy.modules.common;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson.JSONObject;
import com.healthappy.utils.SpringBeanFactoryUtil;
import com.healthappy.utils.SpringUtil;
import lombok.extern.java.Log;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Log
@Component
public class Base64ConvertFile {

    public final static String GUIDE_SHEET = "guide:sheet:item:";

    //非空腹项目
    public final static String NON_GUIDE_SHEET = "guide:sheet:non:item:";

    private final static String URL = "http://localhost:8085/jmreport/exportPdf";


    public static String returnFile(Map<String,Object> map){
        RestTemplate restTemplate = SpringUtil.getBean(RestTemplate.class);
        HttpEntity<Map<String,Object>> httpEntity = new HttpEntity<>(map);
        BufferedOutputStream bos = null;
        FileOutputStream fos = null;
        String name = null;
        try {
         log.info("请求jmreport参数:"+ map);
         String apiResult =  restTemplate.postForObject(URL,httpEntity,String.class);
         log.info("请求jmreport:"+ apiResult);
         JSONObject jsonObject = JSONObject.parseObject(apiResult);
         Object code = jsonObject.get("code");
         Object message = jsonObject.get("message");
        if(null != code && !"".equals(String.valueOf(code)) && "200".equals(String.valueOf(code))){
            JSONObject result = jsonObject.getJSONObject("result");
            //文件byte64字符串
            String file = result.getString("file");
            //文件名称
            String[] splitName = result.getString("name").split("\\.");
            name = splitName[0] + "-" + DateUtil.current(true)+"."+ splitName[1];
            //转换成byte
            byte[] buffer = Base64Utils.decodeFromString(file);
            String fileName = SpringBeanFactoryUtil.resolve("${file.path}") + name;
            File dest = new File(fileName).getCanonicalFile();
            FileUtil.mkdir(dest.getParentFile());
            fos = new FileOutputStream(dest);
            bos = new BufferedOutputStream(fos);
            bos.write(buffer);
            }else {
                log.info("调用失败:"+ message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return "/file/"+ name;
    }

    public static Map<String, Object> parameterJoin(String key, cn.hutool.json.JSONObject object) {
        Map<String,Object> map = new HashMap<>(2);
        map.put("excelConfigId",key);
        map.put("queryParam",object);
        return map;
    }
}
