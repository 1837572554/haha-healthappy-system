package com.healthappy.modules.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author FGQ
 */
@Getter
@AllArgsConstructor
public enum ReportEnum {

    /***体检登记-条码*/
    BAR_CODE("barCode", "631364086721728512"),
    /***体检登记-指引单*/
    GUIDE_SHEET("guideSheet", "630551864357474304"),
    /***体检登记-信息单*/
    INFORMATION_SHEET("informationSheet", "633111470652837888"),;


    private final String value;
    private final String desc;

}
